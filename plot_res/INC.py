import numpy as np
import sys
import matplotlib.pyplot as plt
import math

from NRIP import read_summer_out_file
from NRIP import read_summer_out_file as rsof
from node_com import calc_node_com

# Intra&Inter node communication (CBN)

def plot_inter_node_communication(info, cells, num_nodes):
    
    fig = plt.figure(figsize=(16,8))
    axVol = fig.add_subplot(121)
    axMes = fig.add_subplot(122)
 
    Np = [8, 16, 32, 64, 128]
    for method in info.keys():
        Np = info[method]["Np"]
    Np = [64, 128,192,256]
    
    numM = len(info.keys())
    metNum = -1

    done=False
    for method in info.keys():
        metNum += 1
        comVol = []
        numMes = []

        X = []
        x = 0
        for n in Np:
            if n in info[method][cells].keys():
                if num_nodes!=-1:
                    comTab = info[method][cells][n][num_nodes]["ComTab"]
                else:
                    comTab = info[method][cells][n]["ComTab"]
                vol = 0
                mes = 0
                if not done:
                    print(comTab)
                if num_nodes!=-1:
                    ranksPerNode = int(math.ceil(float(n)/float(num_nodes)))
                    #ranksPerNode = info[method][cells][n][num_nodes]["ranksPerNode"][0]
                else:
                    ranksPerNode = 64

                if num_nodes!=-1:
                    interval=[]
                    for j in range(num_nodes):
                        interval.append((j+1)*ranksPerNode)
                    interval=np.array(interval)
                    for indC,com in enumerate(comTab):
                        if indC > n:
                            break
                        for indN, val in enumerate(com):
                            signsC=np.sign(interval-indC)
                            signsN=np.sign(interval-indN)
                            if not np.array_equal(signsC,signsN):
                                if val > 0:
                                    vol += val
                                    mes += 1
                else:
                    nodeCom = calc_node_com(comTab, ranksPerNode)

                    vol = 0
                    mes = 0
                    for nodeii in nodeCom:
                        vol += nodeii[1]
                        mes += nodeii[3]
                                
                done=True

                numMes.append(mes)
                comVol.append(vol)
                X.append(x)
                x += 1
                if not done:
                    print(method,n,num_nodes,cells)
                done=True
                
            else:
                numMes.append(10)
                comVol.append(1000)
        
        XX = (numM + 1)*np.array(X)
        axVol.bar( XX + metNum, comVol, label=method)
        axVol.xaxis.set_ticks(XX)
        axVol.set_xticklabels(Np)

        XX = (numM +1)*np.array(X)
        axMes.bar(XX + metNum, numMes, label=method)
        axMes.xaxis.set_ticks(XX)
        axMes.set_xticklabels(Np)
    
    axVol.set_xlabel("Number of MPI-processes",fontsize=20)
    axVol.set_ylabel("Inter-nodes communication volume", fontsize=20)
    axVol.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+ ") cells, Nodes: " +str(num_nodes))
    axVol.legend()

    axMes.set_xlabel("Number of MPI-processes",fontsize=20)
    axMes.set_ylabel("Number of messages sent between nodes", fontsize=20)
    axMes.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+ ") cells, Nodes: " +str(num_nodes))
    axMes.legend()



def main():
    info={}
    nodes = [1,2,3,4]
    for name in sys.argv[1:]:
        if "slowq" not in name:
            #read_summer_out_file(name,info)
            rsof(name,info)
            nnode=0
        else:
            rsof(name,info)
            nnode=-1
    nnode = -1
    for cells in [100000, 1000000, 10000000]:
        if nnode==-1:
            plot_inter_node_communication(info, cells, nnode)
        else:
            for node in nodes:
                plot_inter_node_communication(info, cells, node)

    plt.show()  

if __name__ == "__main__":
    main()

