import numpy as np
import sys
import matplotlib.pyplot as plt

from RIP import read_summer_out_file

# Operation Execution Time Plot (OETP)


def spmv_plot(info, op, nd):

    col=[u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22']
    fig = plt.figure(figsize=(16,8))
    #fig2 = plt.figure(figsize=(16,8))
    
    ax = fig.add_subplot(111)
    #ax2 = fig2.add_subplot(111)
    
    cells_l = [100000,1000000]
    Np = [8, 16, 32, 64, 128]
    nodes = [2, 4]
    numM = len(info.keys())
    cellNum = -1

    metNumD = {"Zoltan" : 2, "Metis" : 0, "Scotch" : 1, "MetisCN": 3, "MetisNC": 4, "ScotchCN":5, "ScotchNC": 6, "ZoltanCN": 7}
    cellNumD = ["-","--","-."]
    
    for cells in cells_l:
        cellNum += 1
        
        for method in info.keys():

            metNum = metNumD[method]
            
            spmv_t = []
            X = []
            
            x = 0
            for n in Np:
                
                if op in info[method][cells][n][nd].keys():
                    spmv = info[method][cells][n][nd][op]
                
                    spmv_t.append(max(spmv))
                else:
                    spmv_t.append(1)
                x += 1
                    
                X.append(x)
                
            
            XX = (numM + 1)*np.array(X)

            ax.plot(XX, spmv_t, cellNumD[cellNum], label=method+"_"+str(cells), color=col[metNum])
            ax.xaxis.set_ticks(XX)
            ax.set_xticklabels(Np)
            
    ax.set_xlabel("Number of MPI-processes",fontsize=20)
    if op == "SpMV" or op == "avgSpMV":
        ax.set_ylabel("Matrix-vector execution time (s)",fontsize=20)
        ax.set_yscale('log')
    if op == "avgCopyOwnerToAll" or op == "copyOwnerToAll":
        ax.set_ylabel("copyOwnerToAll execution time (s)",fontsize=20)
    ax.set_title("Nodes: " +str(nd))
    ax.legend()
    
    
def main():
    info = {}
    nodes= [2, 4]
    for name in sys.argv[1:]:
        read_summer_out_file(name, info)
    for nd in nodes:
        spmv_plot(info,"avgSpMV", nd)
        spmv_plot(info,"avgCopyOwnerToAll", nd)
    plt.show()

if __name__ == "__main__":
    main()
