import numpy as np
import sys
import matplotlib.pyplot as plt

from NRIP import read_summer_out_file

# Norne Operation Execution Time Plot (NOETP)


def spmv_plot_ranks(info, op, nd, gridSizes,Np):

    col=[u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#bcbd44', u'#9215bd']
    fig = plt.figure(figsize=(16,8))
    #fig2 = plt.figure(figsize=(16,8))
    
    # ax = fig.add_subplot(121)
    # ax2 = fig2.add_subplot(122)
    
    numM = len(info.keys())

    metNumD = {"Metis" : 0, "Metis-CN": 1, "Metis-NC": 2, "Metis-W" : 3, "Metis-W-CN": 4, "Metis-W-NC": 5, "Metis-WT-CN": 6, "Metis-WT-NC": 7, "CpGrid": 8, "CpGrid-CN": 9, "Metis-WT": 10}
    gsNumD = ["-","--","-*-"]
    
    for ind,gS in enumerate(gridSizes):
        ax = fig.add_subplot(1,len(gridSizes),ind+1)
        for method in info.keys():
            metNum = metNumD[method]
            
            spmv_t = []
            X = []
            
            x = 0
            for n in Np:
                if op in info[method][gS][n][nd].keys():
                    spmv = info[method][gS][n][nd][op]
                
                    spmv_t.append(max(spmv))
                else:
                    spmv_t.append(1)
                x += 1
                    
                X.append(x)
                
            
            XX = (numM + 1)*np.array(X)
            
            ax.plot(XX, spmv_t, gsNumD[ind], label=method+"_"+str(gS), color=col[metNum])
            ax.xaxis.set_ticks(XX)
            ax.set_xticklabels(Np)
            
        ax.set_xlabel("Number of MPI-processes",fontsize=20)
        if op == "SpMV" or op == "avgSpMV":
            ax.set_ylabel("Matrix-vector execution time (s)",fontsize=20)
            ax.set_yscale('log')
        if op == "avgCopyOwnerToAll" or op == "copyOwnerToAll":
            ax.set_ylabel("copyOwnerToAll execution time (s)",fontsize=20)
        ax.set_title("Nodes: " +str(nd))
        ax.legend()
    
def spmv_plot_nodes(info, op, n, gridSizes,nd):

    col=[u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#bcbd44', u'#9215bd']
    fig = plt.figure(figsize=(16,8))
    #fig2 = plt.figure(figsize=(16,8))
    
    # ax = fig.add_subplot(121)
    # ax2 = fig2.add_subplot(122)
    
    numM = len(info.keys())

    metNumD = {"Metis" : 0, "Metis-CN": 1, "Metis-NC": 2, "Metis-W" : 3, "Metis-W-CN": 4, "Metis-W-NC": 5, "Metis-WT-CN": 6, "Metis-WT-NC": 7, "CpGrid": 8, "CpGrid-CN": 9, "Metis-WT": 10}
    gsNumD = ["-","--","-*-"]
    
    for ind,gS in enumerate(gridSizes):
        ax = fig.add_subplot(1,len(gridSizes),ind+1)
        for method in info.keys():
            metNum = metNumD[method]
            
            spmv_t = []
            X = []
            
            x = 0
            for node in nd:
                if op in info[method][gS][n][node].keys():
                    spmv = info[method][gS][n][node][op]
                
                    spmv_t.append(max(spmv))
                else:
                    spmv_t.append(1)
                x += 1
                    
                X.append(x)
                
            
            XX = (numM + 1)*np.array(X)
            
            ax.plot(XX, spmv_t, gsNumD[ind], label=method+"_"+str(gS), color=col[metNum])
            ax.xaxis.set_ticks(XX)
            ax.set_xticklabels(nd)
            
        ax.set_xlabel("Number of Nodes",fontsize=20)
        if op == "SpMV" or op == "avgSpMV":
            ax.set_ylabel("Matrix-vector execution time (s)",fontsize=20)
            ax.set_yscale('log')
        if op == "avgCopyOwnerToAll" or op == "copyOwnerToAll":
            ax.set_ylabel("copyOwnerToAll execution time (s)",fontsize=20)
        ax.set_title("Ranks: " +str(n))
        ax.legend()
    
def main():
    info = {}

    for name in sys.argv[1:]:
        read_summer_out_file(name, info)


    methods = info.keys()
    gridSizes = info[methods[0]].keys()
    
    ranks = []
    for ra in info[methods[0]][gridSizes[0]].keys():
        ranks.append(ra)
    ranks.sort()
    nodes =  []
    for nod in info[methods[0]][gridSizes[0]][ranks[0]].keys():
        nodes.append(nod)
    nodes.sort()
    if len(ranks) ==1:
        spmv_plot_nodes(info,"avgSpMV", ranks[0],gridSizes,nodes)
        spmv_plot_nodes(info,"avgCopyOwnerToAll", ranks[0],gridSizes,nodes)
    else:
        for nd in nodes:
            spmv_plot_ranks(info,"avgSpMV", nd, gridSizes,ranks)
            spmv_plot_ranks(info,"avgCopyOwnerToAll", nd, gridSizes,ranks)
    plt.show()

if __name__ == "__main__":
    main()
