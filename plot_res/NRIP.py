import numpy as np
import sys
import matplotlib.pyplot as plt

# Norne Read Info and Plot (NRIP)

def parse_fname(name, info):

    fname = name.split("/")[-1]
    method = fname.split("_")[0]
    num_nodes = int(fname.split("_")[2].split("-")[0])    
    rankPnode = int(fname.split("_")[1].split("-")[0])
    if "size" in fname:
        sizeGrid  = fname.split("_")[3].split("-")[0]
    else:
        X = int(fname.split("_")[3].split("-")[0])
        Y= int(fname.split("_")[4].split("-")[0])
        Z = int(fname.split("_")[5].split("-")[0])
        sizeGrid = X*Y*Z

    mpiSize = num_nodes*rankPnode



    if method not in info.keys():
        info[method] = {}
    if sizeGrid not in info[method].keys():
        info[method][sizeGrid] = {}
    if rankPnode not in info[method][sizeGrid].keys():
        info[method][sizeGrid][rankPnode] = {}
    if num_nodes not in info[method][sizeGrid][rankPnode].keys():
        info[method][sizeGrid][rankPnode][num_nodes] = []
        
    print (method, rankPnode, num_nodes, sizeGrid)
    return method, rankPnode, num_nodes, sizeGrid

def read_summer_out_file(name, info):


    method, rankPnode, num_nodes, sizeGrid = parse_fname(name, info)
    
    f = open (name, "r")
    lines = f.readlines()
    f.close()

    finfo = {}
    

    for line in lines:
    
        line_list = line.split()
        #print (line_list)
        
        if "before" in line_list:
            if "Interior" not in finfo.keys():
                finfo["Interior"] = {}
            finfo["Interior"][int(line_list[3])] = int(line.split(":")[1].strip())

        if "After" in line_list:
            if "Cells" not in finfo.keys():
                finfo["Cells"] = {}
            try:
                finfo["Cells"][int(line_list[3])] = int(line_list[5])
            except:
                print("problem",name)
                #finfo["Cells"][int(line_list[3])] = int(line_list[5])

        if "Edge-cut:" in line_list:
            finfo["Edge-cut"] = int(line_list[1])
            
        if "ghost" in line_list:
            cti = line.split(":")[1].split()
            if "ComTab" not in finfo.keys():
                finfo["ComTab"] = []
            ct = []
            try:
                if len(cti) == rankPnode*num_nodes:
                    for cc in cti:
                        ct.append(int(cc))
                else:
                    for i in range(int(rankPnode*num_nodes)):
                    
                        ct.append(int(cti[i]))
            
                finfo["ComTab"].append(ct)
            except:
                print("problem", name)
            
        if "Matrix-vector" in line_list and line_list[0]!="Average":
            if "SpMV" not in finfo.keys():
                finfo["SpMV"] = []
            finfo["SpMV"].append(float(line_list[5]))

        if "Matrix-vector" in line_list and line_list[0]=="Average":
            if "avgSpMV" not in finfo.keys():
                finfo["avgSpMV"] = []
            finfo["avgSpMV"].append(float(line_list[6]))

        if "copyOwnerToAll" in line_list and line_list[0]!="Average":
            if "copyOwnerToAll" not in finfo.keys():
                finfo["copyOwnerToAll"] = []
            finfo["copyOwnerToAll"].append(float(line_list[4]))

        if "copyOwnertoAll" in line_list and line_list[0]=="Average":
            if "avgCopyOwnerToAll" not in finfo.keys():
                finfo["avgCopyOwnerToAll"] = []
            finfo["avgCopyOwnerToAll"].append(float(line_list[5]))
        if "Number" in line_list:
            if "ranksPerNode" not in finfo.keys():
                finfo["ranksPerNode"] = []
            finfo["ranksPerNode"].append(int(line.split(":")[1]))


        info[method][sizeGrid][rankPnode][num_nodes] = finfo
        #info[method]["Np"].append(rankPnode)
        
def com_vol_plot(info,sGrid,nd,Np):

    fig = plt.figure(figsize=(16,8))
    fig2 = plt.figure(figsize=(16,8))
    
    ax = fig.add_subplot(111)
    ax2 = fig2.add_subplot(111)
    
    
    numM = len(info.keys())
    metNum = -1
    for method in info.keys():
        metNum += 1
        comVol = []
        numMes = []
        X = []
        x = 0
        for n in Np:    
            if n in info[method][sGrid].keys():
                comTab = info[method][sGrid][n][nd]["ComTab"]

                vol = 0
                mes = 0
                for indC,com in enumerate(comTab):
                    if indC > n:
                        break
                    vol += sum(com)
                    for val in com:
                        if val>0:
                            mes += 1
                numMes.append(mes)
                comVol.append(vol)
                X.append(x)
                x += 1
            else:
                numMes.append(0)
                comVol.append(0)
                X.append(x)
                x += 1
            
        XX = (numM + 1)*np.array(X)
        ax.bar(XX + metNum, comVol, label=method)
        ax.xaxis.set_ticks(XX)
        ax.set_xticklabels(Np)

        XX = (numM + 1)*np.array(X)
        ax2.bar(XX + metNum, numMes, label=method)
        ax2.xaxis.set_ticks(XX)
        ax2.set_xticklabels(Np)
        
    ax.set_xlabel("Number of MPI-processes",fontsize=20)
    ax.set_ylabel("Communication volume",fontsize=20)
    ax.set_title("Size: " +str(sGrid) + ", Nodes: " +str(nd))
    ax.legend()

    ax2.set_xlabel("Number of MPI-processes",fontsize=20)
    ax2.set_ylabel("Number of mesages",fontsize=20)
    ax2.set_title("Size: " + str(sGrid) + ", Nodes: " +str(nd)) 
    ax2.legend()

def balance_plot(info, sGrid, nd, Np):

    col=[u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22']
    fig = plt.figure(figsize=(16,8))

    ax = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    
    numM = len(info.keys())
    metNum = -1

    
    for method in info.keys():
        metNum += 1
        
        interiorBal = []
        totBal = []

        extraCells = []
        X = []
        x = 0

        for n in Np:
            if n in info[method][sGrid].keys():
                inter = info[method][sGrid][n][nd]["Interior"]
                tot = info[method][sGrid][n][nd]["Cells"]

                interiorBal.append( float(n*max(inter.values()))/float(sum(inter.values())))
                totBal.append( float(n*max(tot.values()))/float(sum(tot.values())) ) 
                extraCells.append(sum(tot.values()) - sum(inter.values()))
            
                X.append(x)
                x += 1
            else:
                interiorBal.append( 0)
                totBal.append( 0 )
                extraCells.append( 0 )
            
                X.append(x)
                x += 1

        ax.plot(X, interiorBal, "--", label=method+"_interior",color=col[metNum])
        ax.plot(X, totBal,label=method+"_total",color=col[metNum])
        ax.xaxis.set_ticks(X)
        ax.set_xticklabels(Np)

        XX = (numM+1)*np.array(X)
        ax2.bar(XX + metNum, extraCells, label=method)
        ax2.xaxis.set_ticks(XX)   
        ax2.set_xticklabels(Np)
        
    ax.set_xlabel("Number of MPI-processes",fontsize=20)
    ax.set_ylabel("Inbalance factor",fontsize=20)
    ax.set_title("Size: " + str(sGrid) + ", Nodes: " +str(nd))
    ax.legend()
    
    ax2.set_title("Size: " + str(sGrid) + ", Nodes: " +str(nd))
    ax2.set_xlabel("Number of MPI-processes",fontsize=20)
    ax2.set_ylabel("Extra cells",fontsize=20)
    ax2.legend()

def main():
    info = {}
    for name in sys.argv[1:]:
        read_summer_out_file(name, info)
    methods = info.keys()
    gridSizes = info[methods[0]].keys()
    
    ranks = []
    for ra in info[methods[0]][gridSizes[0]].keys():
        ranks.append(ra)
    ranks.sort()
    nodes =  []
    for nod in info[methods[0]][gridSizes[0]][ranks[0]].keys():
        nodes.append(nod)
    nodes.sort()

    for gS in gridSizes:
        for nd in nodes:
            com_vol_plot(info,gS, nd, ranks)
            balance_plot(info,gS, nd, ranks)
    plt.show()  
    
if __name__ == "__main__":
    main()
