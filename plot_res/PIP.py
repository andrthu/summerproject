import numpy as np
import sys
import matplotlib.pyplot as plt

from RIP import read_summer_out_file

# Partition Info Plot (PIP)


def partition_info_plot(info, cells):

    col=[u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22']
    fig = plt.figure(figsize=(16,8))
    fig2 = plt.figure(figsize=(16,8))
    fig3 = plt.figure(figsize=(16,8))
    fig4 = plt.figure(figsize=(16,8))
    
    axCV = fig3.add_subplot(111)
    axM  = fig4.add_subplot(111)
    
    axC1 = fig.add_subplot(121)
    axC2 = fig.add_subplot(122)
    axM1 = fig2.add_subplot(121)
    axM2 = fig2.add_subplot(122)
    
    Np = [8, 16, 32, 64, 128]
    for method in info.keys():
        Np = info[method]["Np"]

    
    numM = len(info.keys())
    metNum = -1

    
    for method in info.keys():
        metNum += 1
        
        comVol =    []
        avgComVol = []
        maxComVol = []
        minComVol = []
        
        numMes = []
        maxMes = []
        minMes = []
        avgMes = []
        
        X = []
        x = 0
        
        for n in Np:
            comTab = info[method][cells][n]["ComTab"]

            locVol = []
            locMes = []
            for com in comTab:
                locVol.append(sum(com))
                mes = 0
                for val in com:
                    if val>0:
                        mes += 1
                locMes.append(mes)
                        
            numMes.append(sum(locMes))
            maxMes.append(max(locMes))
            minMes.append(min(locMes))
            avgMes.append(sum(locMes)/n)
            
            comVol.append(sum(locVol))
            avgComVol.append(sum(locVol)/n)
            maxComVol.append(max(locVol))
            minComVol.append(min(locVol))
            
            X.append(x)
            x += 1
            
        XX = (numM + 1)*np.array(X)

        axCV.bar(XX + metNum, comVol, label=method, color=col[metNum])
        axCV.xaxis.set_ticks(XX)
        axCV.set_xticklabels(Np)

        axM.bar(XX + metNum, numMes, label=method, color=col[metNum])
        axM.xaxis.set_ticks(XX)
        axM.set_xticklabels(Np)

        axC1.plot(XX, avgComVol, label=method, color=col[metNum])
        axC1.xaxis.set_ticks(XX)
        axC1.set_xticklabels(Np)

        axC2.plot(XX, maxComVol, label=method+"_max", color=col[metNum])
        axC2.plot(XX, minComVol, "--", label=method+"_min", color=col[metNum])
        axC2.xaxis.set_ticks(XX)
        axC2.set_xticklabels(Np)

        axM1.plot(XX, avgMes, label=method, color=col[metNum])
        axM1.xaxis.set_ticks(XX)
        axM1.set_xticklabels(Np)

        axM2.plot(XX, maxMes, label=method+"_max", color=col[metNum])
        axM2.plot(XX, minMes, "--", label=method+"_min", color=col[metNum])
        axM2.xaxis.set_ticks(XX)
        axM2.set_xticklabels(Np)

        

    axCV.set_xlabel("Number of MPI-processes",fontsize=20)
    axCV.set_ylabel("Total communication volume",fontsize=20)
    axCV.legend()
    axCV.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")

    axM.set_xlabel("Number of MPI-processes",fontsize=20)
    axM.set_ylabel("Total number of messages",fontsize=20)
    axM.legend()
    axM.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")
    
    axC1.set_xlabel("Number of MPI-processes",fontsize=20)
    axC1.set_ylabel("Average communication volume",fontsize=20)
    axC1.legend()
    axC1.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")
    
    axC2.set_xlabel("Number of MPI-processes",fontsize=20)
    axC2.legend()
    axC2.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")
    
    axM1.set_xlabel("Number of MPI-processes",fontsize=20)
    axM1.set_ylabel("Average number of messages",fontsize=20)
    axM1.legend()
    axM1.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")
    
    
    axM2.set_xlabel("Number of MPI-processes",fontsize=20)
    axM2.set_ylabel("Number of messages",fontsize=20)
    axM2.legend()
    axM2.set_title("Size: 10^("+str(np.log(cells)/np.log(10))+") cells")
    
    
def main():
    info = {}
    for name in sys.argv[1:]:
        read_summer_out_file(name, info)

    for cells in [1000000]:
        partition_info_plot(info, cells)
    plt.show()

if __name__ == "__main__":
    main()
