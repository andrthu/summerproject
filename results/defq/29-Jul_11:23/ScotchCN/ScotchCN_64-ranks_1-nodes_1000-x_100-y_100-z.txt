Number of cores/ranks per node is: 64
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 155703
Cell on rank 1 before loadbalancing: 156950
Cell on rank 2 before loadbalancing: 155812
Cell on rank 3 before loadbalancing: 155678
Cell on rank 4 before loadbalancing: 155183
Cell on rank 5 before loadbalancing: 157112
Cell on rank 6 before loadbalancing: 154762
Cell on rank 7 before loadbalancing: 154814
Cell on rank 8 before loadbalancing: 157014
Cell on rank 9 before loadbalancing: 155509
Cell on rank 10 before loadbalancing: 156323
Cell on rank 11 before loadbalancing: 155698
Cell on rank 12 before loadbalancing: 155618
Cell on rank 13 before loadbalancing: 154953
Cell on rank 14 before loadbalancing: 156300
Cell on rank 15 before loadbalancing: 155954
Cell on rank 16 before loadbalancing: 157203
Cell on rank 17 before loadbalancing: 157704
Cell on rank 18 before loadbalancing: 157591
Cell on rank 19 before loadbalancing: 156238
Cell on rank 20 before loadbalancing: 155601
Cell on rank 21 before loadbalancing: 156948
Cell on rank 22 before loadbalancing: 156684
Cell on rank 23 before loadbalancing: 156229
Cell on rank 24 before loadbalancing: 156757
Cell on rank 25 before loadbalancing: 155704
Cell on rank 26 before loadbalancing: 155431
Cell on rank 27 before loadbalancing: 156081
Cell on rank 28 before loadbalancing: 155558
Cell on rank 29 before loadbalancing: 154860
Cell on rank 30 before loadbalancing: 157154
Cell on rank 31 before loadbalancing: 157437
Cell on rank 32 before loadbalancing: 156072
Cell on rank 33 before loadbalancing: 156027
Cell on rank 34 before loadbalancing: 156878
Cell on rank 35 before loadbalancing: 157559
Cell on rank 36 before loadbalancing: 157427
Cell on rank 37 before loadbalancing: 157449
Cell on rank 38 before loadbalancing: 157358
Cell on rank 39 before loadbalancing: 157166
Cell on rank 40 before loadbalancing: 157561
Cell on rank 41 before loadbalancing: 156767
Cell on rank 42 before loadbalancing: 157271
Cell on rank 43 before loadbalancing: 157811
Cell on rank 44 before loadbalancing: 155321
Cell on rank 45 before loadbalancing: 155784
Cell on rank 46 before loadbalancing: 155987
Cell on rank 47 before loadbalancing: 156681
Cell on rank 48 before loadbalancing: 155121
Cell on rank 49 before loadbalancing: 155417
Cell on rank 50 before loadbalancing: 155913
Cell on rank 51 before loadbalancing: 156948
Cell on rank 52 before loadbalancing: 156534
Cell on rank 53 before loadbalancing: 155853
Cell on rank 54 before loadbalancing: 156393
Cell on rank 55 before loadbalancing: 156445
Cell on rank 56 before loadbalancing: 155117
Cell on rank 57 before loadbalancing: 154696
Cell on rank 58 before loadbalancing: 155244
Cell on rank 59 before loadbalancing: 156788
Cell on rank 60 before loadbalancing: 156763
Cell on rank 61 before loadbalancing: 155622
Cell on rank 62 before loadbalancing: 155291
Cell on rank 63 before loadbalancing: 156173


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	2438	2151	307	1858	0	0	1341	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1679	42	0	0	
From rank 1 to: 	2438	0	288	2089	675	2132	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	563	2047	0	0	
From rank 2 to: 	2150	294	0	2638	0	0	238	1412	0	0	15	748	949	2005	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	311	2094	2629	0	0	201	2588	379	0	0	909	0	1630	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	1860	678	0	0	0	2525	228	2522	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2245	0	0	0	1123	0	0	0	
From rank 5 to: 	0	2131	0	206	2524	0	2062	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	724	1679	0	0	28	853	0	0	
From rank 6 to: 	0	0	235	2581	216	2069	0	2322	0	0	1897	730	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	1342	49	1415	376	2523	70	2322	0	0	0	0	2160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	2436	160	1602	0	1053	820	1478	0	0	62	1816	728	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	2437	0	1918	321	0	0	3267	0	0	0	1676	394	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	15	902	0	0	1898	0	158	1914	0	2250	1919	699	703	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	763	0	0	0	727	2160	1600	319	2249	0	13	2172	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	948	1626	0	0	0	0	0	0	1919	14	0	1979	1870	1219	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	2012	0	0	0	0	0	1057	0	706	2186	1972	0	326	1988	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	850	3264	706	0	1909	321	0	2089	0	0	385	0	529	1911	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	1479	0	0	0	1214	1982	2077	0	0	0	0	0	2374	861	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2363	651	2210	0	0	2312	0	263	2262	0	0	0	0	452	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2361	0	2195	0	0	331	553	2593	1978	129	0	0	0	0	170	910	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	53	1676	0	0	0	0	382	0	646	2197	0	2506	365	2536	206	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	1811	397	0	0	0	0	0	0	2209	0	2500	0	2236	34	706	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	740	0	0	0	0	0	532	2370	0	0	360	2234	0	2264	2020	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1891	870	0	341	2538	29	2260	0	312	1963	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2311	553	206	691	2021	311	0	2487	0	0	0	0	0	0	1929	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2593	0	0	160	1969	2487	0	0	0	0	0	0	0	526	1896	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	263	1991	0	0	0	0	0	0	0	2690	2198	367	128	491	286	2604	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2263	126	0	0	0	0	0	0	2694	0	417	1877	0	0	2099	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2195	416	0	2597	0	2717	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	363	1873	2591	0	2927	524	409	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	123	0	0	2928	0	2647	2069	521	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	472	0	2714	514	2650	0	0	2142	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	446	164	0	0	0	0	1929	528	268	2100	0	378	2070	0	0	2275	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	911	0	0	0	0	5	1897	2607	146	36	0	532	2140	2278	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2796	0	1849	49	0	2525	606	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2798	0	2310	625	0	0	251	2394	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2316	0	2264	453	2448	163	634	0	0	0	1775	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1855	618	2263	0	2662	30	487	0	0	0	1462	584	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	49	0	463	2669	0	2259	2019	0	0	0	920	361	0	0	109	2525	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2446	35	2257	0	1059	1894	0	0	0	393	0	0	2283	189	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2511	252	175	485	2020	1051	0	2426	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	600	2388	646	0	0	1895	2424	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1986	2083	385	135	2094	0	161	0	0	0	423	0	0	1220	1306	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1981	0	387	1925	2558	772	749	0	0	0	278	0	0	0	1965	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1461	903	0	0	0	2085	391	0	2298	0	266	226	2577	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1774	577	355	379	0	0	388	1922	2308	0	0	0	1734	68	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	134	2555	0	0	0	2590	1653	28	0	0	2317	695	0	0	282	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2088	786	277	0	2590	0	303	2555	0	0	0	1967	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	98	2285	0	0	0	748	220	1734	1650	288	0	2729	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2513	202	0	0	166	0	2579	78	23	2559	2729	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2543	357	2252	0	2399	0	181	0	0	2014	314	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2545	0	2348	110	2392	571	0	264	0	0	179	2315	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	279	0	0	2315	0	0	0	360	2348	0	2221	435	0	2188	324	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	437	0	0	0	668	1966	0	0	2258	114	2221	0	0	97	544	2503	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2394	420	0	0	2310	1447	800	0	0	0	346	0	0	1874	262	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2402	577	0	109	2316	0	0	2543	0	0	1079	140	0	0	143	1919	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1245	1962	0	0	281	0	0	0	0	0	2184	554	1447	0	0	2158	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1307	0	0	0	0	0	0	0	173	274	337	2504	815	2545	2139	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	2245	747	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2531	0	1595	1907	78	590	0	
From rank 57 to: 	0	0	0	0	0	1679	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2530	0	2091	991	568	1817	281	538	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2016	178	0	0	0	1075	0	0	0	2087	0	2286	0	319	0	2371	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	314	2309	0	0	356	150	0	0	1595	981	2289	0	0	0	2116	412	
From rank 60 to: 	1678	554	0	0	1120	33	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1908	568	0	0	0	2543	2047	315	
From rank 61 to: 	41	2052	0	0	0	862	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	1825	318	0	2540	0	117	2006	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1874	132	0	0	606	267	0	2111	2053	113	0	2592	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	255	1919	0	0	0	539	2364	417	320	1994	2594	0	

===================================================================================
=   BAD TERMINATION OF ONE OF YOUR APPLICATION PROCESSES
=   PID 123022 RUNNING AT n001
=   EXIT CODE: 136
=   CLEANING UP REMAINING PROCESSES
=   YOU CAN IGNORE THE BELOW CLEANUP MESSAGES
===================================================================================
YOUR APPLICATION TERMINATED WITH THE EXIT STRING: Floating point exception (signal 8)
This typically refers to a problem with your application.
Please see the FAQ page for debugging suggestions
