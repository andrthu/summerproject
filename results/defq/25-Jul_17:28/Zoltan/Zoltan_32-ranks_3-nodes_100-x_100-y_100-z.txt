Number of cores/ranks per node is: 10
Zoltan partitioner
Cell on rank 0 before loadbalancing: 31281
Cell on rank 1 before loadbalancing: 31280
Cell on rank 2 before loadbalancing: 31236
Cell on rank 3 before loadbalancing: 31326
Cell on rank 4 before loadbalancing: 31239
Cell on rank 5 before loadbalancing: 31238
Cell on rank 6 before loadbalancing: 31170
Cell on rank 7 before loadbalancing: 31308
Cell on rank 8 before loadbalancing: 31267
Cell on rank 9 before loadbalancing: 31304
Cell on rank 10 before loadbalancing: 31290
Cell on rank 11 before loadbalancing: 31291
Cell on rank 12 before loadbalancing: 31336
Cell on rank 13 before loadbalancing: 31334
Cell on rank 14 before loadbalancing: 31088
Cell on rank 15 before loadbalancing: 31089
Cell on rank 16 before loadbalancing: 31390
Cell on rank 17 before loadbalancing: 31391
Cell on rank 18 before loadbalancing: 31436
Cell on rank 19 before loadbalancing: 31173
Cell on rank 20 before loadbalancing: 31238
Cell on rank 21 before loadbalancing: 31237
Cell on rank 22 before loadbalancing: 30793
Cell on rank 23 before loadbalancing: 31302
Cell on rank 24 before loadbalancing: 31286
Cell on rank 25 before loadbalancing: 31286
Cell on rank 26 before loadbalancing: 31109
Cell on rank 27 before loadbalancing: 31306
Cell on rank 28 before loadbalancing: 31263
Cell on rank 29 before loadbalancing: 31263
Cell on rank 30 before loadbalancing: 31212
Cell on rank 31 before loadbalancing: 31238
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	1098	1146	369	0	0	0	418	0	0	0	0	0	244	705	385	0	0	0	0	0	0	0	811	36	220	0	0	0	0	0	0	
From rank 1 to: 	1111	0	259	1119	326	490	0	679	0	0	0	0	341	80	0	344	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	1168	267	0	1192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	423	360	0	0	0	0	0	0	0	0	
From rank 3 to: 	357	1113	1209	0	457	59	0	448	0	0	0	0	0	0	0	0	279	0	0	0	0	0	500	223	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	313	0	457	0	1685	635	547	0	0	0	0	0	0	0	0	124	0	0	176	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	512	0	59	1699	0	500	996	0	0	0	971	197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	631	493	0	1000	0	1006	0	255	0	0	0	0	16	288	277	1007	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	426	698	0	436	553	990	1002	0	0	64	93	719	221	252	0	0	288	432	0	95	0	0	0	253	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1874	1045	136	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	0	683	193	
From rank 9 to: 	0	0	0	0	0	0	1009	64	1873	0	258	1045	0	895	0	0	0	125	64	0	0	0	0	0	126	0	24	0	616	0	34	530	
From rank 10 to: 	0	0	0	0	0	0	0	93	1053	274	0	1264	557	124	0	417	0	38	0	0	0	0	0	16	2	0	0	0	3	0	0	0	
From rank 11 to: 	0	0	0	0	0	980	260	726	125	1032	1254	0	707	340	0	0	0	0	0	0	0	0	0	113	60	0	0	0	0	0	0	0	
From rank 12 to: 	0	337	0	0	0	197	0	219	0	0	546	728	0	1196	3	1244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	236	89	0	0	0	0	0	259	40	864	124	339	1189	0	1079	115	0	0	0	0	0	0	0	19	432	198	312	0	19	0	0	0	
From rank 14 to: 	709	0	0	0	0	0	0	0	0	0	0	0	3	1103	0	1524	0	0	0	0	0	0	0	0	74	1283	0	0	0	0	0	0	
From rank 15 to: 	385	344	0	0	0	0	0	0	0	0	454	0	1258	116	1516	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	280	124	0	19	298	0	0	0	0	0	0	0	0	0	1185	608	1008	697	29	449	280	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	284	432	0	130	37	0	0	0	0	0	1190	0	845	177	544	307	0	735	0	0	0	0	325	950	0	407	
From rank 18 to: 	0	0	0	0	0	0	267	0	0	64	0	0	0	0	0	0	632	874	0	1464	116	0	0	0	0	0	0	0	0	27	0	1103	
From rank 19 to: 	0	0	0	0	176	0	1009	97	0	0	0	0	0	0	0	0	1008	176	1496	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	679	562	116	0	0	1736	467	8	0	0	0	224	0	189	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	305	0	0	1710	0	923	778	445	0	0	588	0	181	0	0	
From rank 22 to: 	0	0	423	486	0	0	0	0	0	0	0	0	0	0	0	0	448	0	0	0	462	907	0	1285	230	0	0	0	0	0	0	0	
From rank 23 to: 	815	0	368	206	0	0	0	269	0	0	21	110	0	19	0	0	275	708	0	0	8	779	1286	0	1331	190	0	0	43	99	0	0	
From rank 24 to: 	37	0	0	0	0	0	0	0	0	128	2	63	0	433	74	0	0	0	0	0	0	446	230	1329	0	1248	766	747	309	206	0	0	
From rank 25 to: 	225	0	0	0	0	0	0	0	0	0	0	0	0	195	1298	0	0	0	0	0	0	0	0	198	1245	0	1116	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	24	0	0	0	318	0	0	0	0	0	0	0	0	0	0	769	1100	0	1374	788	247	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	581	0	0	743	0	1346	0	212	511	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	170	618	4	0	0	18	0	0	0	329	0	0	0	0	0	43	309	0	796	223	0	1468	804	132	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	962	27	0	189	164	0	93	216	0	245	511	1485	0	303	789	
From rank 30 to: 	0	0	0	0	0	0	0	0	680	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	817	307	0	1234	
From rank 31 to: 	0	0	0	0	0	0	0	0	212	530	0	0	0	0	0	0	0	382	1109	0	0	0	0	0	0	0	0	0	130	788	1244	0	
loadb
After loadbalancing process 25 has 35563 cells.
After loadbalancing process 12 has 35806 cells.
After loadbalancing process 10 has 35131 cells.
After loadbalancing process 15 has 35162 cells.
After loadbalancing process 27 has 34909 cells.
After loadbalancing process 18 has 35983 cells.
After loadbalancing process 14 has 35784 cells.
After loadbalancing process 16 has 36367 cells.
After loadbalancing process 20 has 35219 cells.
After loadbalancing process 22 has 35034 cells.
After loadbalancing process 19 has 35135 cells.
After loadbalancing process 11 has 36888 cells.
After loadbalancing process 8 has 35425 cells.
After loadbalancing process 23 has 37829 cells.
After loadbalancing process 24 has 37304 cells.
After loadbalancing process 3 has 35971 cells.
After loadbalancing process 7 has 37830 cells.
After loadbalancing process 26 has 35729 cells.
After loadbalancing process 21 has 36200 cells.
After loadbalancing process 4 has 35176 cells.
After loadbalancing process 29 has 36247 cells.
After loadbalancing process 31 has 35633 cells.
After loadbalancing process 30 has 34284 cells.
After loadbalancing process 0 has 36713 cells.
After loadbalancing process 28 has 36177 cells.
After loadbalancing process 5 has 36172 cells.
After loadbalancing process 6 has 36143 cells.
After loadbalancing process 1 has 36029 cells.
After loadbalancing process 13 has 36648 cells.
After loadbalancing process 9 has 37967 cells.
After loadbalancing process 17 has 37754 cells.
After loadbalancing process 2 has 34646 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	1098	1146	369	0	0	0	418	0	0	0	0	0	244	705	385	0	0	0	0	0	0	0	811	36	220	0	0	0	0	0	0	
Rank 1's ghost cells:	1111	0	259	1119	326	490	0	679	0	0	0	0	341	80	0	344	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1168	267	0	1192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	423	360	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	357	1113	1209	0	457	59	0	448	0	0	0	0	0	0	0	0	279	0	0	0	0	0	500	223	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	313	0	457	0	1685	635	547	0	0	0	0	0	0	0	0	124	0	0	176	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	512	0	59	1699	0	500	996	0	0	0	971	197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	631	493	0	1000	0	1006	0	255	0	0	0	0	16	288	277	1007	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	426	698	0	436	553	990	1002	0	0	64	93	719	221	252	0	0	288	432	0	95	0	0	0	253	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1874	1045	136	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	0	683	193	
Rank 9's ghost cells:	0	0	0	0	0	0	1009	64	1873	0	258	1045	0	895	0	0	0	125	64	0	0	0	0	0	126	0	24	0	616	0	34	530	
Rank 10's ghost cells:	0	0	0	0	0	0	0	93	1053	274	0	1264	557	124	0	417	0	38	0	0	0	0	0	16	2	0	0	0	3	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	980	260	726	125	1032	1254	0	707	340	0	0	0	0	0	0	0	0	0	113	60	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	337	0	0	0	197	0	219	0	0	546	728	0	1196	3	1244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	236	89	0	0	0	0	0	259	40	864	124	339	1189	0	1079	115	0	0	0	0	0	0	0	19	432	198	312	0	19	0	0	0	
Rank 14's ghost cells:	709	0	0	0	0	0	0	0	0	0	0	0	3	1103	0	1524	0	0	0	0	0	0	0	0	74	1283	0	0	0	0	0	0	
Rank 15's ghost cells:	385	344	0	0	0	0	0	0	0	0	454	0	1258	116	1516	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	280	124	0	19	298	0	0	0	0	0	0	0	0	0	1185	608	1008	697	29	449	280	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	284	432	0	130	37	0	0	0	0	0	1190	0	845	177	544	307	0	735	0	0	0	0	325	950	0	407	
Rank 18's ghost cells:	0	0	0	0	0	0	267	0	0	64	0	0	0	0	0	0	632	874	0	1464	116	0	0	0	0	0	0	0	0	27	0	1103	
Rank 19's ghost cells:	0	0	0	0	176	0	1009	97	0	0	0	0	0	0	0	0	1008	176	1496	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	679	562	116	0	0	1736	467	8	0	0	0	224	0	189	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	305	0	0	1710	0	923	778	445	0	0	588	0	181	0	0	
Rank 22's ghost cells:	0	0	423	486	0	0	0	0	0	0	0	0	0	0	0	0	448	0	0	0	462	907	0	1285	230	0	0	0	0	0	0	0	
Rank 23's ghost cells:	815	0	368	206	0	0	0	269	0	0	21	110	0	19	0	0	275	708	0	0	8	779	1286	0	1331	190	0	0	43	99	0	0	
Rank 24's ghost cells:	37	0	0	0	0	0	0	0	0	128	2	63	0	433	74	0	0	0	0	0	0	446	230	1329	0	1248	766	747	309	206	0	0	
Rank 25's ghost cells:	225	0	0	0	0	0	0	0	0	0	0	0	0	195	1298	0	0	0	0	0	0	0	0	198	1245	0	1116	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	24	0	0	0	318	0	0	0	0	0	0	0	0	0	0	769	1100	0	1374	788	247	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	581	0	0	743	0	1346	0	212	511	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	170	618	4	0	0	18	0	0	0	329	0	0	0	0	0	43	309	0	796	223	0	1468	804	132	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	962	27	0	189	164	0	93	216	0	245	511	1485	0	303	789	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	680	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	817	307	0	1234	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	212	530	0	0	0	0	0	0	0	382	1109	0	0	0	0	0	0	0	0	0	130	788	1244	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          56.1849          0.22492
    2          32.8955         0.585485
    3           23.648         0.718884
    4          20.9849         0.887386
    5          18.3689          0.87534
    6          14.5454         0.791849
    7          13.1724         0.905603
    8          12.3575         0.938137
    9          10.1705         0.823022
   10          9.57508         0.941457
   11          9.25808         0.966894
   12           7.7927         0.841718
   13          7.37006         0.945766
   14          7.32556         0.993962
   15          6.33056         0.864174
   16          6.07268         0.959265
   17          6.00595         0.989012
   18          5.33528         0.888331
   19           5.1315         0.961805
   20          5.03552         0.981297
   21          4.55093         0.903766
   22          4.51289         0.991641
   23          4.35213         0.964378
   24          3.98884         0.916526
   25          3.96765         0.994688
   26          3.71247         0.935685
   27            3.419         0.920948
   28          3.39652         0.993427
   29          3.15899         0.930066
   30          2.95806         0.936396
   31            2.991          1.01113
   32          2.87275         0.960467
   33           2.8945          1.00757
   34          3.11737            1.077
   35          3.15026          1.01055
   36          3.10957         0.987082
   37          2.81662         0.905792
   38          2.25193         0.799514
   39           1.8087         0.803179
   40          1.48693         0.822098
   41          1.25887         0.846623
   42          1.16941         0.928938
   43          1.04171         0.890795
   44         0.885793         0.850329
   45          0.77356         0.873296
   46         0.655942         0.847953
   47         0.555843         0.847396
   48         0.480208         0.863928
   49         0.397002         0.826729
   50         0.330817         0.833287
   51          0.28187         0.852044
   52         0.235201         0.834429
   53         0.202022         0.858935
   54         0.172134         0.852053
   55         0.143406         0.833108
   56         0.123689         0.862507
   57         0.103883         0.839878
   58        0.0846473          0.81483
   59        0.0716487         0.846438
   60        0.0585903         0.817744
   61        0.0477715         0.815347
   62         0.039379         0.824321
   63        0.0319124         0.810392
   64        0.0263122         0.824513
   65        0.0218217         0.829339
=== rate=0.866081, T=5.57925, TIT=0.0858346, IT=65

 Elapsed time: 5.57925
Rank 0: Matrix-vector product took 0.0296857 seconds
Rank 1: Matrix-vector product took 0.0287734 seconds
Rank 2: Matrix-vector product took 0.0281057 seconds
Rank 3: Matrix-vector product took 0.0291226 seconds
Rank 4: Matrix-vector product took 0.0285949 seconds
Rank 5: Matrix-vector product took 0.0292872 seconds
Rank 6: Matrix-vector product took 0.029152 seconds
Rank 7: Matrix-vector product took 0.0304711 seconds
Rank 8: Matrix-vector product took 0.0287574 seconds
Rank 9: Matrix-vector product took 0.0304523 seconds
Rank 10: Matrix-vector product took 0.0283025 seconds
Rank 11: Matrix-vector product took 0.0297519 seconds
Rank 12: Matrix-vector product took 0.0288844 seconds
Rank 13: Matrix-vector product took 0.0295999 seconds
Rank 14: Matrix-vector product took 0.0289418 seconds
Rank 15: Matrix-vector product took 0.0282836 seconds
Rank 16: Matrix-vector product took 0.0292874 seconds
Rank 17: Matrix-vector product took 0.0307191 seconds
Rank 18: Matrix-vector product took 0.0289823 seconds
Rank 19: Matrix-vector product took 0.0284633 seconds
Rank 20: Matrix-vector product took 0.0284776 seconds
Rank 21: Matrix-vector product took 0.0290438 seconds
Rank 22: Matrix-vector product took 0.0282575 seconds
Rank 23: Matrix-vector product took 0.0305282 seconds
Rank 24: Matrix-vector product took 0.0300485 seconds
Rank 25: Matrix-vector product took 0.0286411 seconds
Rank 26: Matrix-vector product took 0.0287486 seconds
Rank 27: Matrix-vector product took 0.0282516 seconds
Rank 28: Matrix-vector product took 0.0291999 seconds
Rank 29: Matrix-vector product took 0.0291306 seconds
Rank 30: Matrix-vector product took 0.0278508 seconds
Rank 31: Matrix-vector product took 0.0288834 seconds
Average time for Matrix-vector product is 0.0290837 seconds

Rank 0: copyOwnerToAll took 0.000755879 seconds
Rank 1: copyOwnerToAll took 0.00062834 seconds
Rank 2: copyOwnerToAll took 0.000796599 seconds
Rank 3: copyOwnerToAll took 0.000751559 seconds
Rank 4: copyOwnerToAll took 0.00043468 seconds
Rank 5: copyOwnerToAll took 0.00047395 seconds
Rank 6: copyOwnerToAll took 0.00059001 seconds
Rank 7: copyOwnerToAll took 0.000753019 seconds
Rank 8: copyOwnerToAll took 0.00065762 seconds
Rank 9: copyOwnerToAll took 0.000730119 seconds
Rank 10: copyOwnerToAll took 0.000823211 seconds
Rank 11: copyOwnerToAll took 0.000792921 seconds
Rank 12: copyOwnerToAll took 0.000636971 seconds
Rank 13: copyOwnerToAll took 0.000803531 seconds
Rank 14: copyOwnerToAll took 0.000685111 seconds
Rank 15: copyOwnerToAll took 0.000583301 seconds
Rank 16: copyOwnerToAll took 0.000804571 seconds
Rank 17: copyOwnerToAll took 0.000807021 seconds
Rank 18: copyOwnerToAll took 0.000613891 seconds
Rank 19: copyOwnerToAll took 0.000597911 seconds
Rank 20: copyOwnerToAll took 0.00059546 seconds
Rank 21: copyOwnerToAll took 0.0005943 seconds
Rank 22: copyOwnerToAll took 0.00063298 seconds
Rank 23: copyOwnerToAll took 0.0009243 seconds
Rank 24: copyOwnerToAll took 0.00068415 seconds
Rank 25: copyOwnerToAll took 0.00069881 seconds
Rank 26: copyOwnerToAll took 0.00065681 seconds
Rank 27: copyOwnerToAll took 0.00040193 seconds
Rank 28: copyOwnerToAll took 0.00071175 seconds
Rank 29: copyOwnerToAll took 0.00071239 seconds
Rank 30: copyOwnerToAll took 0.00062744 seconds
Rank 31: copyOwnerToAll took 0.00056002 seconds
Average time for copyOwnertoAll is 0.000672517 seconds
Number of cores/ranks per node is: 10
Zoltan partitioner
Cell on rank 0 before loadbalancing: 31281
Cell on rank 1 before loadbalancing: 31280
Cell on rank 2 before loadbalancing: 31236
Cell on rank 3 before loadbalancing: 31326
Cell on rank 4 before loadbalancing: 31239
Cell on rank 5 before loadbalancing: 31238
Cell on rank 6 before loadbalancing: 31170
Cell on rank 7 before loadbalancing: 31308
Cell on rank 8 before loadbalancing: 31267
Cell on rank 9 before loadbalancing: 31304
Cell on rank 10 before loadbalancing: 31290
Cell on rank 11 before loadbalancing: 31291
Cell on rank 12 before loadbalancing: 31336
Cell on rank 13 before loadbalancing: 31334
Cell on rank 14 before loadbalancing: 31088
Cell on rank 15 before loadbalancing: 31089
Cell on rank 16 before loadbalancing: 31390
Cell on rank 17 before loadbalancing: 31391
Cell on rank 18 before loadbalancing: 31436
Cell on rank 19 before loadbalancing: 31173
Cell on rank 20 before loadbalancing: 31238
Cell on rank 21 before loadbalancing: 31237
Cell on rank 22 before loadbalancing: 30793
Cell on rank 23 before loadbalancing: 31302
Cell on rank 24 before loadbalancing: 31286
Cell on rank 25 before loadbalancing: 31286
Cell on rank 26 before loadbalancing: 31109
Cell on rank 27 before loadbalancing: 31306
Cell on rank 28 before loadbalancing: 31263
Cell on rank 29 before loadbalancing: 31263
Cell on rank 30 before loadbalancing: 31212
Cell on rank 31 before loadbalancing: 31238
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	1098	1146	369	0	0	0	418	0	0	0	0	0	244	705	385	0	0	0	0	0	0	0	811	36	220	0	0	0	0	0	0	
From rank 1 to: 	1111	0	259	1119	326	490	0	679	0	0	0	0	341	80	0	344	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	1168	267	0	1192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	423	360	0	0	0	0	0	0	0	0	
From rank 3 to: 	357	1113	1209	0	457	59	0	448	0	0	0	0	0	0	0	0	279	0	0	0	0	0	500	223	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	313	0	457	0	1685	635	547	0	0	0	0	0	0	0	0	124	0	0	176	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	512	0	59	1699	0	500	996	0	0	0	971	197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	631	493	0	1000	0	1006	0	255	0	0	0	0	16	288	277	1007	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	426	698	0	436	553	990	1002	0	0	64	93	719	221	252	0	0	288	432	0	95	0	0	0	253	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1874	1045	136	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	0	683	193	
From rank 9 to: 	0	0	0	0	0	0	1009	64	1873	0	258	1045	0	895	0	0	0	125	64	0	0	0	0	0	126	0	24	0	616	0	34	530	
From rank 10 to: 	0	0	0	0	0	0	0	93	1053	274	0	1264	557	124	0	417	0	38	0	0	0	0	0	16	2	0	0	0	3	0	0	0	
From rank 11 to: 	0	0	0	0	0	980	260	726	125	1032	1254	0	707	340	0	0	0	0	0	0	0	0	0	113	60	0	0	0	0	0	0	0	
From rank 12 to: 	0	337	0	0	0	197	0	219	0	0	546	728	0	1196	3	1244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	236	89	0	0	0	0	0	259	40	864	124	339	1189	0	1079	115	0	0	0	0	0	0	0	19	432	198	312	0	19	0	0	0	
From rank 14 to: 	709	0	0	0	0	0	0	0	0	0	0	0	3	1103	0	1524	0	0	0	0	0	0	0	0	74	1283	0	0	0	0	0	0	
From rank 15 to: 	385	344	0	0	0	0	0	0	0	0	454	0	1258	116	1516	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	280	124	0	19	298	0	0	0	0	0	0	0	0	0	1185	608	1008	697	29	449	280	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	284	432	0	130	37	0	0	0	0	0	1190	0	845	177	544	307	0	735	0	0	0	0	325	950	0	407	
From rank 18 to: 	0	0	0	0	0	0	267	0	0	64	0	0	0	0	0	0	632	874	0	1464	116	0	0	0	0	0	0	0	0	27	0	1103	
From rank 19 to: 	0	0	0	0	176	0	1009	97	0	0	0	0	0	0	0	0	1008	176	1496	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	679	562	116	0	0	1736	467	8	0	0	0	224	0	189	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	305	0	0	1710	0	923	778	445	0	0	588	0	181	0	0	
From rank 22 to: 	0	0	423	486	0	0	0	0	0	0	0	0	0	0	0	0	448	0	0	0	462	907	0	1285	230	0	0	0	0	0	0	0	
From rank 23 to: 	815	0	368	206	0	0	0	269	0	0	21	110	0	19	0	0	275	708	0	0	8	779	1286	0	1331	190	0	0	43	99	0	0	
From rank 24 to: 	37	0	0	0	0	0	0	0	0	128	2	63	0	433	74	0	0	0	0	0	0	446	230	1329	0	1248	766	747	309	206	0	0	
From rank 25 to: 	225	0	0	0	0	0	0	0	0	0	0	0	0	195	1298	0	0	0	0	0	0	0	0	198	1245	0	1116	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	24	0	0	0	318	0	0	0	0	0	0	0	0	0	0	769	1100	0	1374	788	247	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	581	0	0	743	0	1346	0	212	511	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	170	618	4	0	0	18	0	0	0	329	0	0	0	0	0	43	309	0	796	223	0	1468	804	132	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	962	27	0	189	164	0	93	216	0	245	511	1485	0	303	789	
From rank 30 to: 	0	0	0	0	0	0	0	0	680	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	817	307	0	1234	
From rank 31 to: 	0	0	0	0	0	0	0	0	212	530	0	0	0	0	0	0	0	382	1109	0	0	0	0	0	0	0	0	0	130	788	1244	0	
loadb
After loadbalancing process 2 has 34646 cells.
After loadbalancing process 4 has 35176 cells.
After loadbalancing process 1 has 36029 cells.
After loadbalancing process 0 has 36713 cells.
After loadbalancing process 27 has 34909 cells.
After loadbalancing process 19 has 35135 cells.
After loadbalancing process 16 has 36367 cells.
After loadbalancing process 20 has 35219 cells.
After loadbalancing process 3 has 35971 cells.
After loadbalancing process 23 has 37829 cells.
After loadbalancing process 21 has 36200 cells.
After loadbalancing process 29 has 36247 cells.
After loadbalancing process 25 has 35563 cells.
After loadbalancing process 22 has 35034 cells.
After loadbalancing process 11 has 36888 cells.
After loadbalancing process 30 has 34284 cells.
After loadbalancing process 12 has 35806 cells.
After loadbalancing process 17 has 37754 cells.
After loadbalancing process 7 has 37830 cells.
After loadbalancing process 28 has 36177 cells.
After loadbalancing process 26 has 35729 cells.
After loadbalancing process 5 has 36172 cells.
After loadbalancing process 31 has 35633 cells.
After loadbalancing process 8 has 35425 cells.
After loadbalancing process 10 has 35131 cells.
After loadbalancing process 24 has 37304 cells.
After loadbalancing process 6 has 36143 cells.
After loadbalancing process 15 has 35162 cells.
After loadbalancing process 14 has 35784 cells.
After loadbalancing process 13 has 36648 cells.
After loadbalancing process 9 has 37967 cells.
After loadbalancing process 18 has 35983 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	1098	1146	369	0	0	0	418	0	0	0	0	0	244	705	385	0	0	0	0	0	0	0	811	36	220	0	0	0	0	0	0	
Rank 1's ghost cells:	1111	0	259	1119	326	490	0	679	0	0	0	0	341	80	0	344	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1168	267	0	1192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	423	360	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	357	1113	1209	0	457	59	0	448	0	0	0	0	0	0	0	0	279	0	0	0	0	0	500	223	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	313	0	457	0	1685	635	547	0	0	0	0	0	0	0	0	124	0	0	176	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	512	0	59	1699	0	500	996	0	0	0	971	197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	631	493	0	1000	0	1006	0	255	0	0	0	0	16	288	277	1007	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	426	698	0	436	553	990	1002	0	0	64	93	719	221	252	0	0	288	432	0	95	0	0	0	253	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1874	1045	136	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	0	683	193	
Rank 9's ghost cells:	0	0	0	0	0	0	1009	64	1873	0	258	1045	0	895	0	0	0	125	64	0	0	0	0	0	126	0	24	0	616	0	34	530	
Rank 10's ghost cells:	0	0	0	0	0	0	0	93	1053	274	0	1264	557	124	0	417	0	38	0	0	0	0	0	16	2	0	0	0	3	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	980	260	726	125	1032	1254	0	707	340	0	0	0	0	0	0	0	0	0	113	60	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	337	0	0	0	197	0	219	0	0	546	728	0	1196	3	1244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	236	89	0	0	0	0	0	259	40	864	124	339	1189	0	1079	115	0	0	0	0	0	0	0	19	432	198	312	0	19	0	0	0	
Rank 14's ghost cells:	709	0	0	0	0	0	0	0	0	0	0	0	3	1103	0	1524	0	0	0	0	0	0	0	0	74	1283	0	0	0	0	0	0	
Rank 15's ghost cells:	385	344	0	0	0	0	0	0	0	0	454	0	1258	116	1516	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	280	124	0	19	298	0	0	0	0	0	0	0	0	0	1185	608	1008	697	29	449	280	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	284	432	0	130	37	0	0	0	0	0	1190	0	845	177	544	307	0	735	0	0	0	0	325	950	0	407	
Rank 18's ghost cells:	0	0	0	0	0	0	267	0	0	64	0	0	0	0	0	0	632	874	0	1464	116	0	0	0	0	0	0	0	0	27	0	1103	
Rank 19's ghost cells:	0	0	0	0	176	0	1009	97	0	0	0	0	0	0	0	0	1008	176	1496	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	679	562	116	0	0	1736	467	8	0	0	0	224	0	189	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	305	0	0	1710	0	923	778	445	0	0	588	0	181	0	0	
Rank 22's ghost cells:	0	0	423	486	0	0	0	0	0	0	0	0	0	0	0	0	448	0	0	0	462	907	0	1285	230	0	0	0	0	0	0	0	
Rank 23's ghost cells:	815	0	368	206	0	0	0	269	0	0	21	110	0	19	0	0	275	708	0	0	8	779	1286	0	1331	190	0	0	43	99	0	0	
Rank 24's ghost cells:	37	0	0	0	0	0	0	0	0	128	2	63	0	433	74	0	0	0	0	0	0	446	230	1329	0	1248	766	747	309	206	0	0	
Rank 25's ghost cells:	225	0	0	0	0	0	0	0	0	0	0	0	0	195	1298	0	0	0	0	0	0	0	0	198	1245	0	1116	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	24	0	0	0	318	0	0	0	0	0	0	0	0	0	0	769	1100	0	1374	788	247	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	581	0	0	743	0	1346	0	212	511	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	170	618	4	0	0	18	0	0	0	329	0	0	0	0	0	43	309	0	796	223	0	1468	804	132	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	962	27	0	189	164	0	93	216	0	245	511	1485	0	303	789	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	680	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	817	307	0	1234	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	212	530	0	0	0	0	0	0	0	382	1109	0	0	0	0	0	0	0	0	0	130	788	1244	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          56.1849          0.22492
    2          32.8955         0.585485
    3           23.648         0.718884
    4          20.9849         0.887386
    5          18.3689          0.87534
    6          14.5454         0.791849
    7          13.1724         0.905603
    8          12.3575         0.938137
    9          10.1705         0.823022
   10          9.57508         0.941457
   11          9.25808         0.966894
   12           7.7927         0.841718
   13          7.37006         0.945766
   14          7.32556         0.993962
   15          6.33056         0.864174
   16          6.07268         0.959265
   17          6.00595         0.989012
   18          5.33528         0.888331
   19           5.1315         0.961805
   20          5.03552         0.981297
   21          4.55093         0.903766
   22          4.51289         0.991641
   23          4.35213         0.964378
   24          3.98884         0.916526
   25          3.96765         0.994688
   26          3.71247         0.935685
   27            3.419         0.920948
   28          3.39652         0.993427
   29          3.15899         0.930066
   30          2.95806         0.936396
   31            2.991          1.01113
   32          2.87275         0.960467
   33           2.8945          1.00757
   34          3.11737            1.077
   35          3.15026          1.01055
   36          3.10957         0.987082
   37          2.81662         0.905792
   38          2.25193         0.799514
   39           1.8087         0.803179
   40          1.48693         0.822098
   41          1.25887         0.846623
   42          1.16941         0.928938
   43          1.04171         0.890795
   44         0.885793         0.850329
   45          0.77356         0.873296
   46         0.655942         0.847953
   47         0.555843         0.847396
   48         0.480208         0.863928
   49         0.397002         0.826729
   50         0.330817         0.833287
   51          0.28187         0.852044
   52         0.235201         0.834429
   53         0.202022         0.858935
   54         0.172134         0.852053
   55         0.143406         0.833108
   56         0.123689         0.862507
   57         0.103883         0.839878
   58        0.0846473          0.81483
   59        0.0716487         0.846438
   60        0.0585903         0.817744
   61        0.0477715         0.815347
   62         0.039379         0.824321
   63        0.0319124         0.810392
   64        0.0263122         0.824513
   65        0.0218217         0.829339
=== rate=0.866081, T=5.55077, TIT=0.0853964, IT=65

 Elapsed time: 5.55077
Rank 0: Matrix-vector product took 0.02963 seconds
Rank 1: Matrix-vector product took 0.0290256 seconds
Rank 2: Matrix-vector product took 0.0280338 seconds
Rank 3: Matrix-vector product took 0.0290347 seconds
Rank 4: Matrix-vector product took 0.0284144 seconds
Rank 5: Matrix-vector product took 0.0292291 seconds
Rank 6: Matrix-vector product took 0.0292101 seconds
Rank 7: Matrix-vector product took 0.0305157 seconds
Rank 8: Matrix-vector product took 0.0286547 seconds
Rank 9: Matrix-vector product took 0.0304764 seconds
Rank 10: Matrix-vector product took 0.0283777 seconds
Rank 11: Matrix-vector product took 0.0297449 seconds
Rank 12: Matrix-vector product took 0.0289446 seconds
Rank 13: Matrix-vector product took 0.0297163 seconds
Rank 14: Matrix-vector product took 0.0288615 seconds
Rank 15: Matrix-vector product took 0.0281943 seconds
Rank 16: Matrix-vector product took 0.029315 seconds
Rank 17: Matrix-vector product took 0.0304759 seconds
Rank 18: Matrix-vector product took 0.0289922 seconds
Rank 19: Matrix-vector product took 0.0284123 seconds
Rank 20: Matrix-vector product took 0.0286712 seconds
Rank 21: Matrix-vector product took 0.0292615 seconds
Rank 22: Matrix-vector product took 0.0284018 seconds
Rank 23: Matrix-vector product took 0.0304281 seconds
Rank 24: Matrix-vector product took 0.0302972 seconds
Rank 25: Matrix-vector product took 0.0287115 seconds
Rank 26: Matrix-vector product took 0.0288233 seconds
Rank 27: Matrix-vector product took 0.0281656 seconds
Rank 28: Matrix-vector product took 0.0291271 seconds
Rank 29: Matrix-vector product took 0.0293357 seconds
Rank 30: Matrix-vector product took 0.0276515 seconds
Rank 31: Matrix-vector product took 0.0287779 seconds
Average time for Matrix-vector product is 0.029091 seconds

Rank 0: copyOwnerToAll took 0.00064163 seconds
Rank 1: copyOwnerToAll took 0.00056521 seconds
Rank 2: copyOwnerToAll took 0.000702529 seconds
Rank 3: copyOwnerToAll took 0.00062335 seconds
Rank 4: copyOwnerToAll took 0.00059764 seconds
Rank 5: copyOwnerToAll took 0.00063866 seconds
Rank 6: copyOwnerToAll took 0.00064192 seconds
Rank 7: copyOwnerToAll took 0.000877149 seconds
Rank 8: copyOwnerToAll took 0.00062338 seconds
Rank 9: copyOwnerToAll took 0.000750109 seconds
Rank 10: copyOwnerToAll took 0.00079766 seconds
Rank 11: copyOwnerToAll took 0.00077458 seconds
Rank 12: copyOwnerToAll took 0.00081787 seconds
Rank 13: copyOwnerToAll took 0.00078278 seconds
Rank 14: copyOwnerToAll took 0.00053351 seconds
Rank 15: copyOwnerToAll took 0.00051825 seconds
Rank 16: copyOwnerToAll took 0.00081736 seconds
Rank 17: copyOwnerToAll took 0.00079796 seconds
Rank 18: copyOwnerToAll took 0.000685 seconds
Rank 19: copyOwnerToAll took 0.00076813 seconds
Rank 20: copyOwnerToAll took 0.00061917 seconds
Rank 21: copyOwnerToAll took 0.00053185 seconds
Rank 22: copyOwnerToAll took 0.00061848 seconds
Rank 23: copyOwnerToAll took 0.00087677 seconds
Rank 24: copyOwnerToAll took 0.00066395 seconds
Rank 25: copyOwnerToAll took 0.00065868 seconds
Rank 26: copyOwnerToAll took 0.00064938 seconds
Rank 27: copyOwnerToAll took 0.00039263 seconds
Rank 28: copyOwnerToAll took 0.00069366 seconds
Rank 29: copyOwnerToAll took 0.00063058 seconds
Rank 30: copyOwnerToAll took 0.0004795 seconds
Rank 31: copyOwnerToAll took 0.00061148 seconds
Average time for copyOwnertoAll is 0.00066815 seconds
