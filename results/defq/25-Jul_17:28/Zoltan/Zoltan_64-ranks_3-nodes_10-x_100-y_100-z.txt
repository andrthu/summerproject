Number of cores/ranks per node is: 21
Zoltan partitioner
Cell on rank 0 before loadbalancing: 1544
Cell on rank 1 before loadbalancing: 1543
Cell on rank 2 before loadbalancing: 1516
Cell on rank 3 before loadbalancing: 1569
Cell on rank 4 before loadbalancing: 1560
Cell on rank 5 before loadbalancing: 1533
Cell on rank 6 before loadbalancing: 1540
Cell on rank 7 before loadbalancing: 1539
Cell on rank 8 before loadbalancing: 1571
Cell on rank 9 before loadbalancing: 1572
Cell on rank 10 before loadbalancing: 1565
Cell on rank 11 before loadbalancing: 1565
Cell on rank 12 before loadbalancing: 1571
Cell on rank 13 before loadbalancing: 1567
Cell on rank 14 before loadbalancing: 1568
Cell on rank 15 before loadbalancing: 1568
Cell on rank 16 before loadbalancing: 1563
Cell on rank 17 before loadbalancing: 1563
Cell on rank 18 before loadbalancing: 1563
Cell on rank 19 before loadbalancing: 1563
Cell on rank 20 before loadbalancing: 1574
Cell on rank 21 before loadbalancing: 1573
Cell on rank 22 before loadbalancing: 1534
Cell on rank 23 before loadbalancing: 1571
Cell on rank 24 before loadbalancing: 1556
Cell on rank 25 before loadbalancing: 1556
Cell on rank 26 before loadbalancing: 1555
Cell on rank 27 before loadbalancing: 1556
Cell on rank 28 before loadbalancing: 1539
Cell on rank 29 before loadbalancing: 1540
Cell on rank 30 before loadbalancing: 1539
Cell on rank 31 before loadbalancing: 1539
Cell on rank 32 before loadbalancing: 1573
Cell on rank 33 before loadbalancing: 1574
Cell on rank 34 before loadbalancing: 1568
Cell on rank 35 before loadbalancing: 1568
Cell on rank 36 before loadbalancing: 1570
Cell on rank 37 before loadbalancing: 1571
Cell on rank 38 before loadbalancing: 1578
Cell on rank 39 before loadbalancing: 1564
Cell on rank 40 before loadbalancing: 1573
Cell on rank 41 before loadbalancing: 1572
Cell on rank 42 before loadbalancing: 1572
Cell on rank 43 before loadbalancing: 1571
Cell on rank 44 before loadbalancing: 1574
Cell on rank 45 before loadbalancing: 1574
Cell on rank 46 before loadbalancing: 1559
Cell on rank 47 before loadbalancing: 1559
Cell on rank 48 before loadbalancing: 1570
Cell on rank 49 before loadbalancing: 1570
Cell on rank 50 before loadbalancing: 1570
Cell on rank 51 before loadbalancing: 1571
Cell on rank 52 before loadbalancing: 1571
Cell on rank 53 before loadbalancing: 1570
Cell on rank 54 before loadbalancing: 1569
Cell on rank 55 before loadbalancing: 1570
Cell on rank 56 before loadbalancing: 1576
Cell on rank 57 before loadbalancing: 1575
Cell on rank 58 before loadbalancing: 1572
Cell on rank 59 before loadbalancing: 1573
Cell on rank 60 before loadbalancing: 1565
Cell on rank 61 before loadbalancing: 1565
Cell on rank 62 before loadbalancing: 1561
Cell on rank 63 before loadbalancing: 1557
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	146	17	139	147	0	0	30	36	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	148	0	100	0	0	0	0	0	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	12	100	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	139	0	120	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	109	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	142	0	0	0	0	107	0	117	10	0	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	106	0	126	38	0	0	56	0	171	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	0	0	66	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	134	0	193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	162	0	0	0	0	0	5	0	0	0	0	0	0	63	19	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	30	0	0	110	116	46	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	39	140	0	0	10	0	0	0	0	148	74	9	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	149	0	13	138	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	20	0	0	0	126	46	0	0	79	9	0	135	87	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	9	138	139	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	180	0	0	0	71	84	0	0	50	48	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	56	140	58	0	101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	48	100	0	204	0	0	0	0	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	91	0	209	0	0	0	0	0	54	0	14	0	0	0	0	0	0	0	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	152	0	39	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	29	135	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	159	0	0	163	0	0	0	109	0	0	0	0	36	61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	79	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	39	158	220	0	0	52	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	54	0	0	0	0	0	116	138	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	52	123	0	0	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	0	0	0	0	137	0	0	140	0	0	0	0	48	0	100	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	3	50	29	147	140	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	171	150	101	30	4	0	109	15	6	0	0	0	0	43	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	30	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	175	0	0	0	0	0	0	65	21	0	0	0	0	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	133	55	112	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	44	0	0	0	16	26	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	97	0	137	0	0	0	0	0	33	117	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	31	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	0	0	48	70	33	0	55	0	0	185	0	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	72	0	0	0	0	0	0	4	0	117	0	184	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	115	0	0	0	0	12	
From rank 30 to: 	0	0	0	0	0	61	0	0	0	0	0	0	55	0	0	117	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	48	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	0	109	65	0	0	111	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	22	0	33	0	0	0	0	0	100	2	229	30	0	188	119	0	12	0	0	0	8	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	118	0	0	0	0	100	0	95	41	0	0	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	83	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	105	0	172	0	0	0	0	0	0	0	0	82	0	0	0	0	0	0	0	72	60	54	0	0	0	0	0	71	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	222	41	183	0	12	0	0	0	0	20	0	25	141	28	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	0	11	0	211	0	81	103	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	25	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	204	0	0	79	31	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	63	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	55	0	8	0	0	0	0	182	54	0	0	0	0	0	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	38	0	0	19	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	77	80	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	103	37	0	0	0	185	17	77	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	0	19	51	72	0	0	193	0	69	35	0	147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	72	0	203	0	74	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	75	42	203	0	5	75	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	75	148	0	0	0	0	0	0	0	5	0	139	175	42	0	0	0	0	10	9	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	9	0	0	24	0	0	0	0	0	155	73	61	140	0	0	83	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	0	0	141	0	0	0	0	122	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	136	0	42	83	142	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	182	8	77	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	176	0	181	20	0	0	0	100	61	0	3	0	0	2	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	179	0	109	0	0	0	66	6	0	0	0	0	154	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	77	18	107	0	0	53	0	105	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	78	0	0	0	0	0	0	0	0	0	10	0	122	0	0	0	0	0	0	193	25	0	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	11	0	0	0	0	0	0	53	197	0	140	139	0	0	0	0	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	23	143	0	144	0	0	0	0	126	6	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	66	101	0	142	143	0	35	0	15	0	21	71	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	6	0	0	0	0	35	0	142	46	0	0	68	0	85	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	151	0	204	146	0	0	0	92	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	29	0	0	0	0	0	0	0	0	0	44	0	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	0	0	15	58	193	0	141	0	0	7	128	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	134	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	136	0	0	0	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	78	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	21	0	0	0	0	0	112	78	0	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	151	0	0	0	6	75	69	0	0	0	115	0	97	74	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	128	0	0	0	0	0	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	72	97	0	192	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	31	0	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	83	99	124	0	0	73	187	0	
loadb
After loadbalancing process 46 has 2009 cells.
After loadbalancing process 47 has 1962 cells.
After loadbalancing process 1 has 1932 cells.
After loadbalancing process 18 has 1865 cells.
After loadbalancing process 11 has 1991 cells.
After loadbalancing process 2 has 1863 cells.
After loadbalancing process 20 has 1976 cells.
After loadbalancing process 19 has 2082 cells.
After loadbalancing process 8 has 1991 cells.
After loadbalancing process 21 has 1965 cells.
After loadbalancing process 36 has 2151 cells.
After loadbalancing process 49 has 2113 cells.
After loadbalancing process 37 has 2091 cells.
After loadbalancing process 51 has 1931 cells.
After loadbalancing process 22 has 2062 cells.
After loadbalancing process 17 has 2091 cells.
After loadbalancing process 23 has 2115 cells.
After loadbalancing process 48 has 1837 cells.
After loadbalancing process 50 has 2095 cells.
After loadbalancing process 40 has 1992 cells.
After loadbalancing process 42 has 2079 cells.
After loadbalancing process 28 has 2072 cells.
After loadbalancing process 43 has 2001 cells.
After loadbalancing process 9 has 1929 cells.
After loadbalancing process 0 has 2079 cells.
After loadbalancing process 39 has 2113 cells.
After loadbalancing process 35 has 2240 cells.
After loadbalancing process 13 has 1922 cells.
After loadbalancing process 41 has 2169 cells.
After loadbalancing process 25 has 2072 cells.
After loadbalancing process 6 has 2116 cells.
After loadbalancing process 7 has 2116 cells.
After loadbalancing process 30 has 2052 cells.
After loadbalancing process 16 has 1980 cells.
After loadbalancing process 38 has 2109 cells.
After loadbalancing process 24 has 2185 cells.
After loadbalancing process 32 has 2329 cells.
After loadbalancing process 14 has 1984 cells.
After loadbalancing process 12 has 2141 cells.
After loadbalancing process 56 has 2019 cells.
After loadbalancing process 10 has 2123 cells.
After loadbalancing process 55 has 2274 cells.
After loadbalancing process 31 has 2134 cells.
After loadbalancing process 15 has 2053 cells.
After loadbalancing process 58 has 2309 cells.
After loadbalancing process 4 has 2064 cells.
After loadbalancing process 61 has 2154 cells.
After loadbalancing process 5 has 2177 cells.
After loadbalancing process 27 has 2106 cells.
After loadbalancing process 63 has 2194 cells.
After loadbalancing process 59 has 1990 cells.
After loadbalancing process 54 has 2075 cells.
After loadbalancing process 33 has 2177 cells.
After loadbalancing process 3 has 2081 cells.
After loadbalancing process 26 has 2091 cells.
After loadbalancing process 45 has 2119 cells.
After loadbalancing process 52 has 1999 cells.
After loadbalancing process 53 has 2180 cells.
After loadbalancing process 62 has 2153 cells.
After loadbalancing process 57 has 2192 cells.
After loadbalancing process 29 has 2130 cells.
After loadbalancing process 34 has 2186 cells.
After loadbalancing process 44 has 2177 cells.
After loadbalancing process 60 has 2087 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	146	17	139	147	0	0	30	36	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	148	0	100	0	0	0	0	0	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	12	100	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	139	0	120	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	109	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	142	0	0	0	0	107	0	117	10	0	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	106	0	126	38	0	0	56	0	171	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	0	0	66	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	134	0	193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	162	0	0	0	0	0	5	0	0	0	0	0	0	63	19	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	30	0	0	110	116	46	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	39	140	0	0	10	0	0	0	0	148	74	9	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	149	0	13	138	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	20	0	0	0	126	46	0	0	79	9	0	135	87	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	9	138	139	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	180	0	0	0	71	84	0	0	50	48	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	56	140	58	0	101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	48	100	0	204	0	0	0	0	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	91	0	209	0	0	0	0	0	54	0	14	0	0	0	0	0	0	0	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	152	0	39	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	29	135	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	159	0	0	163	0	0	0	109	0	0	0	0	36	61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	79	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	39	158	220	0	0	52	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	54	0	0	0	0	0	116	138	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	52	123	0	0	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	0	0	0	0	137	0	0	140	0	0	0	0	48	0	100	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	3	50	29	147	140	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	171	150	101	30	4	0	109	15	6	0	0	0	0	43	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	30	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	175	0	0	0	0	0	0	65	21	0	0	0	0	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	133	55	112	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	44	0	0	0	16	26	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	97	0	137	0	0	0	0	0	33	117	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	31	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	0	0	48	70	33	0	55	0	0	185	0	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	72	0	0	0	0	0	0	4	0	117	0	184	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	115	0	0	0	0	12	
Rank 30's ghost cells:	0	0	0	0	0	61	0	0	0	0	0	0	55	0	0	117	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	48	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	0	109	65	0	0	111	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	22	0	33	0	0	0	0	0	100	2	229	30	0	188	119	0	12	0	0	0	8	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	118	0	0	0	0	100	0	95	41	0	0	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	83	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	105	0	172	0	0	0	0	0	0	0	0	82	0	0	0	0	0	0	0	72	60	54	0	0	0	0	0	71	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	222	41	183	0	12	0	0	0	0	20	0	25	141	28	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	0	11	0	211	0	81	103	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	25	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	204	0	0	79	31	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	63	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	55	0	8	0	0	0	0	182	54	0	0	0	0	0	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	38	0	0	19	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	77	80	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	103	37	0	0	0	185	17	77	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	0	19	51	72	0	0	193	0	69	35	0	147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	72	0	203	0	74	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	75	42	203	0	5	75	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	75	148	0	0	0	0	0	0	0	5	0	139	175	42	0	0	0	0	10	9	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	9	0	0	24	0	0	0	0	0	155	73	61	140	0	0	83	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	0	0	141	0	0	0	0	122	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	136	0	42	83	142	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	182	8	77	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	176	0	181	20	0	0	0	100	61	0	3	0	0	2	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	179	0	109	0	0	0	66	6	0	0	0	0	154	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	77	18	107	0	0	53	0	105	0	0	0	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	78	0	0	0	0	0	0	0	0	0	10	0	122	0	0	0	0	0	0	193	25	0	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	11	0	0	0	0	0	0	53	197	0	140	139	0	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	23	143	0	144	0	0	0	0	126	6	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	66	101	0	142	143	0	35	0	15	0	21	71	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	6	0	0	0	0	35	0	142	46	0	0	68	0	85	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	151	0	204	146	0	0	0	92	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	29	0	0	0	0	0	0	0	0	0	44	0	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	0	0	15	58	193	0	141	0	0	7	128	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	134	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	136	0	0	0	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	78	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	21	0	0	0	0	0	112	78	0	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	151	0	0	0	6	75	69	0	0	0	115	0	97	74	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	128	0	0	0	0	0	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	72	97	0	192	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	31	0	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	83	99	124	0	0	73	187	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          40.7219         0.254115
    2          25.1991         0.618809
    3          22.7729         0.903718
    4          22.9052          1.00581
    5          12.5095         0.546142
    6          6.80289          0.54382
    7          5.04994         0.742323
    8           3.1653         0.626799
    9          1.80224         0.569375
   10          1.23684         0.686277
   11          0.80101         0.647627
   12         0.522745         0.652607
   13         0.356496         0.681969
   14         0.223124         0.625881
   15         0.171521         0.768724
   16         0.110758         0.645743
   17        0.0738204         0.666499
   18        0.0512829         0.694698
   19        0.0320719         0.625391
   20        0.0208997         0.651652
   21         0.014426         0.690247
=== rate=0.641726, T=0.159978, TIT=0.00761799, IT=21

 Elapsed time: 0.159978
Rank 0: Matrix-vector product took 0.00161776 seconds
Rank 1: Matrix-vector product took 0.00151106 seconds
Rank 2: Matrix-vector product took 0.00145691 seconds
Rank 3: Matrix-vector product took 0.00161551 seconds
Rank 4: Matrix-vector product took 0.00162133 seconds
Rank 5: Matrix-vector product took 0.00167994 seconds
Rank 6: Matrix-vector product took 0.00163772 seconds
Rank 7: Matrix-vector product took 0.00164804 seconds
Rank 8: Matrix-vector product took 0.0015566 seconds
Rank 9: Matrix-vector product took 0.00149683 seconds
Rank 10: Matrix-vector product took 0.00165974 seconds
Rank 11: Matrix-vector product took 0.00160293 seconds
Rank 12: Matrix-vector product took 0.00165761 seconds
Rank 13: Matrix-vector product took 0.0014902 seconds
Rank 14: Matrix-vector product took 0.00155072 seconds
Rank 15: Matrix-vector product took 0.00159644 seconds
Rank 16: Matrix-vector product took 0.00153509 seconds
Rank 17: Matrix-vector product took 0.00161855 seconds
Rank 18: Matrix-vector product took 0.00144852 seconds
Rank 19: Matrix-vector product took 0.00162416 seconds
Rank 20: Matrix-vector product took 0.0015601 seconds
Rank 21: Matrix-vector product took 0.00151824 seconds
Rank 22: Matrix-vector product took 0.00158498 seconds
Rank 23: Matrix-vector product took 0.00163571 seconds
Rank 24: Matrix-vector product took 0.00167309 seconds
Rank 25: Matrix-vector product took 0.00159952 seconds
Rank 26: Matrix-vector product took 0.00160466 seconds
Rank 27: Matrix-vector product took 0.00162027 seconds
Rank 28: Matrix-vector product took 0.00160441 seconds
Rank 29: Matrix-vector product took 0.0016488 seconds
Rank 30: Matrix-vector product took 0.00158334 seconds
Rank 31: Matrix-vector product took 0.00163532 seconds
Rank 32: Matrix-vector product took 0.00176984 seconds
Rank 33: Matrix-vector product took 0.00166505 seconds
Rank 34: Matrix-vector product took 0.00167048 seconds
Rank 35: Matrix-vector product took 0.00171681 seconds
Rank 36: Matrix-vector product took 0.0016386 seconds
Rank 37: Matrix-vector product took 0.00161484 seconds
Rank 38: Matrix-vector product took 0.00163449 seconds
Rank 39: Matrix-vector product took 0.00163277 seconds
Rank 40: Matrix-vector product took 0.00154588 seconds
Rank 41: Matrix-vector product took 0.00165512 seconds
Rank 42: Matrix-vector product took 0.00158952 seconds
Rank 43: Matrix-vector product took 0.00152761 seconds
Rank 44: Matrix-vector product took 0.00167133 seconds
Rank 45: Matrix-vector product took 0.00162212 seconds
Rank 46: Matrix-vector product took 0.00154476 seconds
Rank 47: Matrix-vector product took 0.00152169 seconds
Rank 48: Matrix-vector product took 0.00141409 seconds
Rank 49: Matrix-vector product took 0.00162083 seconds
Rank 50: Matrix-vector product took 0.00162259 seconds
Rank 51: Matrix-vector product took 0.00148544 seconds
Rank 52: Matrix-vector product took 0.00154399 seconds
Rank 53: Matrix-vector product took 0.00213487 seconds
Rank 54: Matrix-vector product took 0.00159222 seconds
Rank 55: Matrix-vector product took 0.00171654 seconds
Rank 56: Matrix-vector product took 0.00155376 seconds
Rank 57: Matrix-vector product took 0.00166632 seconds
Rank 58: Matrix-vector product took 0.00174401 seconds
Rank 59: Matrix-vector product took 0.00199988 seconds
Rank 60: Matrix-vector product took 0.00160406 seconds
Rank 61: Matrix-vector product took 0.00164431 seconds
Rank 62: Matrix-vector product took 0.00165841 seconds
Rank 63: Matrix-vector product took 0.00168858 seconds
Average time for Matrix-vector product is 0.00161883 seconds

Rank 0: copyOwnerToAll took 8.634e-05 seconds
Rank 1: copyOwnerToAll took 9.094e-05 seconds
Rank 2: copyOwnerToAll took 0.00020458 seconds
Rank 3: copyOwnerToAll took 0.00018913 seconds
Rank 4: copyOwnerToAll took 8.987e-05 seconds
Rank 5: copyOwnerToAll took 0.00017209 seconds
Rank 6: copyOwnerToAll took 0.00020079 seconds
Rank 7: copyOwnerToAll took 0.0001751 seconds
Rank 8: copyOwnerToAll took 7.78e-05 seconds
Rank 9: copyOwnerToAll took 6.962e-05 seconds
Rank 10: copyOwnerToAll took 9.578e-05 seconds
Rank 11: copyOwnerToAll took 6.927e-05 seconds
Rank 12: copyOwnerToAll took 0.00014093 seconds
Rank 13: copyOwnerToAll took 7.976e-05 seconds
Rank 14: copyOwnerToAll took 7.015e-05 seconds
Rank 15: copyOwnerToAll took 0.00017766 seconds
Rank 16: copyOwnerToAll took 0.00015439 seconds
Rank 17: copyOwnerToAll took 0.0001297 seconds
Rank 18: copyOwnerToAll took 0.00014446 seconds
Rank 19: copyOwnerToAll took 0.00016132 seconds
Rank 20: copyOwnerToAll took 0.00014212 seconds
Rank 21: copyOwnerToAll took 0.00019302 seconds
Rank 22: copyOwnerToAll took 0.00016386 seconds
Rank 23: copyOwnerToAll took 0.00017237 seconds
Rank 24: copyOwnerToAll took 0.0001785 seconds
Rank 25: copyOwnerToAll took 0.00018032 seconds
Rank 26: copyOwnerToAll took 0.00019762 seconds
Rank 27: copyOwnerToAll took 0.0002176 seconds
Rank 28: copyOwnerToAll took 0.00015584 seconds
Rank 29: copyOwnerToAll took 0.00023275 seconds
Rank 30: copyOwnerToAll took 0.00021248 seconds
Rank 31: copyOwnerToAll took 0.00025565 seconds
Rank 32: copyOwnerToAll took 0.00032179 seconds
Rank 33: copyOwnerToAll took 0.00024231 seconds
Rank 34: copyOwnerToAll took 0.00023391 seconds
Rank 35: copyOwnerToAll took 0.00029583 seconds
Rank 36: copyOwnerToAll took 0.00022752 seconds
Rank 37: copyOwnerToAll took 0.00021032 seconds
Rank 38: copyOwnerToAll took 0.00022669 seconds
Rank 39: copyOwnerToAll took 0.00024542 seconds
Rank 40: copyOwnerToAll took 0.00013754 seconds
Rank 41: copyOwnerToAll took 0.00024939 seconds
Rank 42: copyOwnerToAll took 0.000267489 seconds
Rank 43: copyOwnerToAll took 0.000269139 seconds
Rank 44: copyOwnerToAll took 0.000271959 seconds
Rank 45: copyOwnerToAll took 0.000306089 seconds
Rank 46: copyOwnerToAll took 6.046e-05 seconds
Rank 47: copyOwnerToAll took 7.909e-05 seconds
Rank 48: copyOwnerToAll took 4.607e-05 seconds
Rank 49: copyOwnerToAll took 8.222e-05 seconds
Rank 50: copyOwnerToAll took 7.049e-05 seconds
Rank 51: copyOwnerToAll took 7.282e-05 seconds
Rank 52: copyOwnerToAll took 0.000181829 seconds
Rank 53: copyOwnerToAll took 0.000189229 seconds
Rank 54: copyOwnerToAll took 0.000162719 seconds
Rank 55: copyOwnerToAll took 0.000101119 seconds
Rank 56: copyOwnerToAll took 0.000229049 seconds
Rank 57: copyOwnerToAll took 0.000191339 seconds
Rank 58: copyOwnerToAll took 0.000240729 seconds
Rank 59: copyOwnerToAll took 0.000208559 seconds
Rank 60: copyOwnerToAll took 0.000226589 seconds
Rank 61: copyOwnerToAll took 0.000170589 seconds
Rank 62: copyOwnerToAll took 0.000367019 seconds
Rank 63: copyOwnerToAll took 0.00032694 seconds
Average time for copyOwnertoAll is 0.000178032 seconds
