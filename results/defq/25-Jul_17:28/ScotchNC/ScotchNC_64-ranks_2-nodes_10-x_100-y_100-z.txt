Number of cores/ranks per node is: 32
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 50000
Cell on rank 1 before loadbalancing: 50000
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	123	110	37	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	123	0	0	98	10	104	0	0	130	0	10	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	110	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	29	98	130	0	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	10	0	146	0	122	33	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	104	0	0	123	0	109	0	0	0	0	0	127	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	33	112	0	148	0	0	0	0	8	122	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	112	0	148	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	130	0	0	0	0	0	0	0	114	100	0	90	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	114	0	26	100	0	0	95	0	0	0	0	0	90	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	110	20	0	0	0	0	0	0	100	16	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	100	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	68	78	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	0	
From rank 12 to: 	0	20	0	0	0	122	8	0	80	0	0	0	0	124	100	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	24	120	0	0	0	0	0	124	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	45	95	0	0	100	0	0	117	0	0	0	0	48	52	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	40	90	117	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	73	0	50	70	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	107	33	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	73	33	127	0	0	0	0	0	0	0	0	0	90	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	90	0	0	0	0	48	0	0	0	0	0	0	143	0	0	0	93	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	52	0	50	0	0	0	137	0	120	100	0	83	0	0	0	0	0	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	80	0	0	0	120	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	140	0	0	0	0	0	100	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	120	0	0	50	105	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	40	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	83	0	0	55	0	40	54	0	0	100	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	68	0	0	0	0	0	0	0	0	0	0	0	0	120	40	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	64	0	78	0	0	0	0	0	0	0	0	100	0	0	0	0	54	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	120	38	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	120	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	40	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	101	0	0	38	72	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	70	0	25	0	0	0	25	0	0	93	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	110	20	0	0	0	120	90	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	0	0	110	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	60	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	140	0	0	110	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	110	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	98	0	0	0	0	0	0	80	90	0	0	0	0	0	0	0	0	40	0	30	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	40	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	90	22	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	
From rank 38 to: 	60	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	90	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	0	98	22	120	0	20	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	0	0	0	0	0	20	0	96	116	0	67	0	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	130	0	0	0	0	0	0	96	0	19	145	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	19	0	110	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	67	0	97	0	0	140	117	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	0	83	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	90	80	0	0	0	117	0	0	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	24	83	106	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	50	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	72	0	0	120	0	0	0	0	110	80	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	100	48	0	120	30	0	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	170	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	72	58	170	0	0	0	0	0	0	0	0	0	50	170	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	123	12	115	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	123	0	120	0	0	0	0	0	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	30	0	0	7	120	0	130	0	66	0	16	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	130	0	0	98	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	80	37	0	0	0	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	66	98	110	0	0	104	0	0	0	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	80	0	0	120	50	0	70	32	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	16	0	37	104	122	0	98	0	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	50	0	0	0	0	0	0	50	98	0	70	0	120	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	0	0	0	0	0	0	0	70	0	0	90	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	50	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	170	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	118	90	170	0	
loadb
After loadbalancing process 7 has 1830 cells.
After loadbalancing process 40 has 2019 cells.
After loadbalancing process 50 has 1818 cells.
After loadbalancing process 48 has 2046 cells.
After loadbalancing process 51 has 2082 cells.
After loadbalancing process 63 has 1987 cells.
After loadbalancing process 44 has 2007 cells.
After loadbalancing process 46 has 2028 cells.
After loadbalancing process 33 has 1903 cells.
After loadbalancing process 47 has 2059 cells.
After loadbalancing process 42 has 1917 cells.
After loadbalancing process 5 has 2060 cells.
After loadbalancing process 6 has 1985 cells.
After loadbalancing process 18 has 1814 cells.
After loadbalancing process 14 has 2044 cells.
After loadbalancing process 49 has 1974 cells.
After loadbalancing process 61 has 1916 cells.
After loadbalancing process 43 has 1802 cells.
After loadbalancing process 54 has 2044 cells.
After loadbalancing process 53 has 1912 cells.
After loadbalancing process 45 has 1920 cells.
After loadbalancing process 15 has 1964 cells.
After loadbalancing process 62 has 1990 cells.
After loadbalancing process 21 has 2143 cells.
After loadbalancing process 59 has 2054 cells.
After loadbalancing process 60 has 2055 cells.
After loadbalancing process 9 has 2062 cells.
After loadbalancing process 35 has 1920 cells.
After loadbalancing process 13 has 1934 cells.
After loadbalancing process 41 has 1967 cells.
After loadbalancing process 23 has 1942 cells.
After loadbalancing process 58 has 2072 cells.
After loadbalancing process 19 has 1942 cells.
After loadbalancing process 17 has 1921 cells.
After loadbalancing process 32 has 2046 cells.
After loadbalancing process 31 has 2053 cells.
After loadbalancing process 20 has 2021 cells.
After loadbalancing process 3 has 1972 cells.
After loadbalancing process 12 has 2068 cells.
After loadbalancing process 56 has 2043 cells.
After loadbalancing process 8 has 2052 cells.
After loadbalancing process 25 has 2004 cells.
After loadbalancing process 36 has 2050 cells.
After loadbalancing process 1 has 2072 cells.
After loadbalancing process 28 has 1888 cells.
After loadbalancing process 4 has 1990 cells.
After loadbalancing process 16 has 2019 cells.
After loadbalancing process 26 has 2144 cells.
After loadbalancing process 34 has 2060 cells.
After loadbalancing process 27 has 2013 cells.
After loadbalancing process 22 has 1948 cells.
After loadbalancing process 0 has 2048 cells.
After loadbalancing process 11 has 2059 cells.
After loadbalancing process 2 has 1930 cells.
After loadbalancing process 24 has 2017 cells.
After loadbalancing process 52 has 1940 cells.
After loadbalancing process 57 has 2085 cells.
After loadbalancing process 29 has 1925 cells.
After loadbalancing process 30 has 2011 cells.
After loadbalancing process 10 has 2088 cells.
After loadbalancing process 39 has 2050 cells.
After loadbalancing process 55 has 2079 cells.
After loadbalancing process 37 has 2050 cells.
After loadbalancing process 38 has 2070 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	123	110	37	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	123	0	0	98	10	104	0	0	130	0	10	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	110	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	29	98	130	0	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	10	0	146	0	122	33	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	104	0	0	123	0	109	0	0	0	0	0	127	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	33	112	0	148	0	0	0	0	8	122	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	112	0	148	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	130	0	0	0	0	0	0	0	114	100	0	90	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	114	0	26	100	0	0	95	0	0	0	0	0	90	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	110	20	0	0	0	0	0	0	100	16	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	100	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	68	78	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	20	0	0	0	122	8	0	80	0	0	0	0	124	100	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	24	120	0	0	0	0	0	124	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	45	95	0	0	100	0	0	117	0	0	0	0	48	52	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	40	90	117	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	73	0	50	70	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	107	33	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	73	33	127	0	0	0	0	0	0	0	0	0	90	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	90	0	0	0	0	48	0	0	0	0	0	0	143	0	0	0	93	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	52	0	50	0	0	0	137	0	120	100	0	83	0	0	0	0	0	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	80	0	0	0	120	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	140	0	0	0	0	0	100	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	120	0	0	50	105	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	40	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	83	0	0	55	0	40	54	0	0	100	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	68	0	0	0	0	0	0	0	0	0	0	0	0	120	40	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	64	0	78	0	0	0	0	0	0	0	0	100	0	0	0	0	54	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	120	38	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	120	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	40	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	101	0	0	38	72	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	70	0	25	0	0	0	25	0	0	93	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	110	20	0	0	0	120	90	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	0	0	110	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	60	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	140	0	0	110	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	110	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	98	0	0	0	0	0	0	80	90	0	0	0	0	0	0	0	0	40	0	30	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	40	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	90	22	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	
Rank 38's ghost cells:	60	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	90	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	0	98	22	120	0	20	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	0	0	0	0	0	20	0	96	116	0	67	0	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	130	0	0	0	0	0	0	96	0	19	145	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	19	0	110	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	67	0	97	0	0	140	117	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	0	83	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	90	80	0	0	0	117	0	0	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	24	83	106	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	50	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	72	0	0	120	0	0	0	0	110	80	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	100	48	0	120	30	0	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	170	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	72	58	170	0	0	0	0	0	0	0	0	0	50	170	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	123	12	115	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	123	0	120	0	0	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	30	0	0	7	120	0	130	0	66	0	16	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	130	0	0	98	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	80	37	0	0	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	66	98	110	0	0	104	0	0	0	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	80	0	0	120	50	0	70	32	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	16	0	37	104	122	0	98	0	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	50	0	0	0	0	0	0	50	98	0	70	0	120	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	0	0	0	0	0	0	0	70	0	0	90	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	50	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	170	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	118	90	170	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          39.5585         0.246856
    2          24.2102          0.61201
    3          21.7681          0.89913
    4          20.8142         0.956179
    5          10.6771         0.512973
    6          5.47319         0.512608
    7          4.26414         0.779095
    8           2.6034         0.610535
    9          1.31849         0.506449
   10         0.941949         0.714415
   11          0.63881         0.678179
   12         0.353448         0.553292
   13         0.243741         0.689608
   14         0.163741         0.671784
   15         0.102728         0.627382
   16        0.0618463         0.602038
   17        0.0461287          0.74586
   18        0.0286899         0.621954
   19        0.0167953         0.585407
   20        0.0111473         0.663719
=== rate=0.61961, T=0.119748, TIT=0.00598742, IT=20

 Elapsed time: 0.119748
Rank 0: Matrix-vector product took 0.00159071 seconds
Rank 1: Matrix-vector product took 0.00160764 seconds
Rank 2: Matrix-vector product took 0.00149965 seconds
Rank 3: Matrix-vector product took 0.00152687 seconds
Rank 4: Matrix-vector product took 0.00154 seconds
Rank 5: Matrix-vector product took 0.0016054 seconds
Rank 6: Matrix-vector product took 0.0015394 seconds
Rank 7: Matrix-vector product took 0.00142439 seconds
Rank 8: Matrix-vector product took 0.00159549 seconds
Rank 9: Matrix-vector product took 0.0016032 seconds
Rank 10: Matrix-vector product took 0.00162103 seconds
Rank 11: Matrix-vector product took 0.0016009 seconds
Rank 12: Matrix-vector product took 0.00160651 seconds
Rank 13: Matrix-vector product took 0.00150335 seconds
Rank 14: Matrix-vector product took 0.00158612 seconds
Rank 15: Matrix-vector product took 0.00153113 seconds
Rank 16: Matrix-vector product took 0.0015693 seconds
Rank 17: Matrix-vector product took 0.00149165 seconds
Rank 18: Matrix-vector product took 0.00140911 seconds
Rank 19: Matrix-vector product took 0.00150314 seconds
Rank 20: Matrix-vector product took 0.0015619 seconds
Rank 21: Matrix-vector product took 0.0016528 seconds
Rank 22: Matrix-vector product took 0.00151407 seconds
Rank 23: Matrix-vector product took 0.0015163 seconds
Rank 24: Matrix-vector product took 0.00156849 seconds
Rank 25: Matrix-vector product took 0.00155348 seconds
Rank 26: Matrix-vector product took 0.00165732 seconds
Rank 27: Matrix-vector product took 0.00156165 seconds
Rank 28: Matrix-vector product took 0.00146128 seconds
Rank 29: Matrix-vector product took 0.00149288 seconds
Rank 30: Matrix-vector product took 0.00155089 seconds
Rank 31: Matrix-vector product took 0.00157782 seconds
Rank 32: Matrix-vector product took 0.00157972 seconds
Rank 33: Matrix-vector product took 0.0014747 seconds
Rank 34: Matrix-vector product took 0.00159677 seconds
Rank 35: Matrix-vector product took 0.00148941 seconds
Rank 36: Matrix-vector product took 0.00158708 seconds
Rank 37: Matrix-vector product took 0.00159098 seconds
Rank 38: Matrix-vector product took 0.0016105 seconds
Rank 39: Matrix-vector product took 0.00159125 seconds
Rank 40: Matrix-vector product took 0.00156271 seconds
Rank 41: Matrix-vector product took 0.00152295 seconds
Rank 42: Matrix-vector product took 0.00149193 seconds
Rank 43: Matrix-vector product took 0.00140333 seconds
Rank 44: Matrix-vector product took 0.0015547 seconds
Rank 45: Matrix-vector product took 0.0014889 seconds
Rank 46: Matrix-vector product took 0.00157861 seconds
Rank 47: Matrix-vector product took 0.00160071 seconds
Rank 48: Matrix-vector product took 0.00158796 seconds
Rank 49: Matrix-vector product took 0.00154666 seconds
Rank 50: Matrix-vector product took 0.00140001 seconds
Rank 51: Matrix-vector product took 0.00162453 seconds
Rank 52: Matrix-vector product took 0.00151306 seconds
Rank 53: Matrix-vector product took 0.00148984 seconds
Rank 54: Matrix-vector product took 0.00158442 seconds
Rank 55: Matrix-vector product took 0.00161097 seconds
Rank 56: Matrix-vector product took 0.00159668 seconds
Rank 57: Matrix-vector product took 0.0016053 seconds
Rank 58: Matrix-vector product took 0.00160673 seconds
Rank 59: Matrix-vector product took 0.00159288 seconds
Rank 60: Matrix-vector product took 0.00158481 seconds
Rank 61: Matrix-vector product took 0.00147518 seconds
Rank 62: Matrix-vector product took 0.00154164 seconds
Rank 63: Matrix-vector product took 0.00153724 seconds
Average time for Matrix-vector product is 0.00155072 seconds

Rank 0: copyOwnerToAll took 0.00019552 seconds
Rank 1: copyOwnerToAll took 7.815e-05 seconds
Rank 2: copyOwnerToAll took 0.00018347 seconds
Rank 3: copyOwnerToAll took 6.871e-05 seconds
Rank 4: copyOwnerToAll took 7.923e-05 seconds
Rank 5: copyOwnerToAll took 7.596e-05 seconds
Rank 6: copyOwnerToAll took 6.47e-05 seconds
Rank 7: copyOwnerToAll took 4.662e-05 seconds
Rank 8: copyOwnerToAll took 7.302e-05 seconds
Rank 9: copyOwnerToAll took 7.714e-05 seconds
Rank 10: copyOwnerToAll took 0.00013054 seconds
Rank 11: copyOwnerToAll took 0.00018328 seconds
Rank 12: copyOwnerToAll took 8.581e-05 seconds
Rank 13: copyOwnerToAll took 6.316e-05 seconds
Rank 14: copyOwnerToAll took 7.815e-05 seconds
Rank 15: copyOwnerToAll took 6.763e-05 seconds
Rank 16: copyOwnerToAll took 7.708e-05 seconds
Rank 17: copyOwnerToAll took 5.77e-05 seconds
Rank 18: copyOwnerToAll took 4.47e-05 seconds
Rank 19: copyOwnerToAll took 7.066e-05 seconds
Rank 20: copyOwnerToAll took 6.115e-05 seconds
Rank 21: copyOwnerToAll took 9.967e-05 seconds
Rank 22: copyOwnerToAll took 6.278e-05 seconds
Rank 23: copyOwnerToAll took 7.725e-05 seconds
Rank 24: copyOwnerToAll took 0.00015242 seconds
Rank 25: copyOwnerToAll took 0.00010974 seconds
Rank 26: copyOwnerToAll took 0.00012415 seconds
Rank 27: copyOwnerToAll took 0.00010527 seconds
Rank 28: copyOwnerToAll took 6.443e-05 seconds
Rank 29: copyOwnerToAll took 0.00015884 seconds
Rank 30: copyOwnerToAll took 9.828e-05 seconds
Rank 31: copyOwnerToAll took 7.845e-05 seconds
Rank 32: copyOwnerToAll took 0.00013666 seconds
Rank 33: copyOwnerToAll took 4.495e-05 seconds
Rank 34: copyOwnerToAll took 0.00013622 seconds
Rank 35: copyOwnerToAll took 9.369e-05 seconds
Rank 36: copyOwnerToAll took 0.000174601 seconds
Rank 37: copyOwnerToAll took 0.0001496 seconds
Rank 38: copyOwnerToAll took 0.00016943 seconds
Rank 39: copyOwnerToAll took 8.007e-05 seconds
Rank 40: copyOwnerToAll took 7.242e-05 seconds
Rank 41: copyOwnerToAll took 0.00010525 seconds
Rank 42: copyOwnerToAll took 6.11e-05 seconds
Rank 43: copyOwnerToAll took 6.115e-05 seconds
Rank 44: copyOwnerToAll took 6.821e-05 seconds
Rank 45: copyOwnerToAll took 4.841e-05 seconds
Rank 46: copyOwnerToAll took 8.606e-05 seconds
Rank 47: copyOwnerToAll took 0.00013659 seconds
Rank 48: copyOwnerToAll took 6.008e-05 seconds
Rank 49: copyOwnerToAll took 7.079e-05 seconds
Rank 50: copyOwnerToAll took 5.029e-05 seconds
Rank 51: copyOwnerToAll took 7.616e-05 seconds
Rank 52: copyOwnerToAll took 7.906e-05 seconds
Rank 53: copyOwnerToAll took 6.964e-05 seconds
Rank 54: copyOwnerToAll took 9.016e-05 seconds
Rank 55: copyOwnerToAll took 0.00012084 seconds
Rank 56: copyOwnerToAll took 0.00014341 seconds
Rank 57: copyOwnerToAll took 0.00013568 seconds
Rank 58: copyOwnerToAll took 0.00014399 seconds
Rank 59: copyOwnerToAll took 7.318e-05 seconds
Rank 60: copyOwnerToAll took 6.922e-05 seconds
Rank 61: copyOwnerToAll took 5.466e-05 seconds
Rank 62: copyOwnerToAll took 8.354e-05 seconds
Rank 63: copyOwnerToAll took 6.832e-05 seconds
Average time for copyOwnertoAll is 9.38608e-05 seconds
