Number of cores/ranks per node is: 16
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	140	103	0	0	105	26	0	0	0	0	70	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	140	0	10	100	0	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	104	10	0	150	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	44	56	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	100	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	148	0	100	0	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	105	0	0	0	148	0	110	0	0	50	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	26	114	0	0	0	110	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	100	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	140	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	100	52	0	0	140	0	20	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	100	30	0	110	0	0	0	130	0	0	0	0	0	0	0	0	0	0	100	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	70	0	0	0	0	80	0	0	0	90	110	0	85	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	95	0	60	0	0	0	0	0	0	0	0	85	0	150	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	90	0	0	0	0	0	0	0	0	0	150	0	104	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	136	16	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	40	112	0	150	0	0	0	0	0	0	0	0	0	0	0	0	80	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	92	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	120	80	110	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	20	116	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	120	0	0	10	0	130	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	120	0	112	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	110	0	120	0	10	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	10	0	120	121	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	132	0	0	0	120	120	0	0	120	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	0	0	116	16	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	130	116	0	120	0	0	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	20	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	10	120	0	120	0	110	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	131	0	126	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	30	0	0	0	30	80	0	0	0	0	0	0	0	0	0	0	110	0	130	0	120	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	10	0	114	16	0	0	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	126	4	120	0	0	0	0	0	96	10	0	0	0	0	0	0	0	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	112	11	0	0	0	0	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	127	0	0	100	0	0	31	106	23	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	100	143	0	0	0	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	0	0	20	0	0	92	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	0	0	160	0	109	0	0	0	0	0	120	38	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	10	0	0	0	0	160	0	114	7	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	100	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	113	0	104	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	0	108	7	119	0	0	0	0	40	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	20	0	0	0	0	0	0	0	108	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	130	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	30	120	12	110	0	0	0	80	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	50	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	50	0	0	0	0	0	0	0	0	0	0	0	17	0	0	0	0	127	0	0	0	0	0	0	0	100	0	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	0	0	92	0	0	0	80	40	93	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	30	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	134	0	112	0	0	0	0	0	0	0	0	0	0	115	21	
From rank 49 to: 	0	0	44	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	100	24	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	60	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	116	30	0	110	0	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	24	116	0	90	0	0	0	0	0	38	0	0	0	0	102	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	90	0	100	140	10	0	0	77	63	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	0	30	140	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	110	0	0	0	114	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	136	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	140	0	0	110	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	16	94	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	110	110	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	0	0	0	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	107	10	0	140	0	13	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	77	0	0	0	0	107	0	140	0	0	0	120	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	63	120	0	0	110	10	140	0	0	0	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	108	0	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	140	0	12	101	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	108	12	0	140	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	0	96	0	0	0	0	0	19	120	0	0	101	140	0	
loadb
After loadbalancing process 7 has 1740 cells.
After loadbalancing process 42 has 1950 cells.
After loadbalancing process 20 has 1790 cells.
After loadbalancing process 6 has 1975 cells.
After loadbalancing process 22 has 1942 cells.
After loadbalancing process 21 has 1930 cells.
After loadbalancing process 4 has 1895 cells.
After loadbalancing process 23 has 2056 cells.
After loadbalancing process 5 has 2073 cells.
After loadbalancing process 17 has 2076 cells.
After loadbalancing process 24 has 1911 cells.
After loadbalancing process 25 has 2027 cells.
After loadbalancing process 18 has 1928 cells.
After loadbalancing process 11 has 2080 cells.
After loadbalancing process 30 has 2026 cells.
After loadbalancing process 16 has 2115 cells.
After loadbalancing process 60 has 1795 cells.
After loadbalancing process 1 has 1940 cells.
After loadbalancing process 9 has 1947 cells.
After loadbalancing process 61 has 1953 cells.
After loadbalancing process 59 has 2112 cells.
After loadbalancing process 41 has 1820 cells.
After loadbalancing process 48 has 1942 cells.
After loadbalancing process 62 has 1965 cells.
After loadbalancing process 63 has 2087 cells.
After loadbalancing process 51 has 2072 cells.
After loadbalancing process 0 has 2104 cells.
After loadbalancing process 19 has 2022 cells.
After loadbalancing process 26 has 2032 cells.
After loadbalancing process 3 has 1930 cells.
After loadbalancing process 47 has 1960 cells.
After loadbalancing process 49 has 1968 cells.
After loadbalancing process 32 has 1937 cells.
After loadbalancing process 53 has 2111 cells.
After loadbalancing process 34 has 1912 cells.
After loadbalancing process 56 has 1950 cells.
After loadbalancing process 57 has 1988 cells.
After loadbalancing process 12 has 2090 cells.
After loadbalancing process 58 has 2073 cells.
After loadbalancing process 35 has 2069 cells.
After loadbalancing process 40 has 1931 cells.
After loadbalancing process 43 has 2050 cells.
After loadbalancing process 33 has 2053 cells.
After loadbalancing process 38 has 2094 cells.
After loadbalancing process 15 has 2110 cells.
After loadbalancing process 28 has 1945 cells.
After loadbalancing process 29 has 2040 cells.
After loadbalancing process 39 has 2024 cells.
After loadbalancing process 37 has 2058 cells.
After loadbalancing process 52 has 2090 cells.After loadbalancing process 45 has 2046 cells.

After loadbalancing process 46 has 2050 cells.
After loadbalancing process 36 has 2100 cells.
After loadbalancing process 31 has 2069 cells.
After loadbalancing process 2 has 2078 cells.
After loadbalancing process 27 has 1906 cells.
After loadbalancing process 44 has 1920 cells.
After loadbalancing process 54 has 2060 cells.
After loadbalancing process 10 has 2070 cells.
After loadbalancing process 8 has 1960 cells.
After loadbalancing process 50 has 2046 cells.
After loadbalancing process 14 has 2056 cells.
After loadbalancing process 13 has 2138 cells.
After loadbalancing process 55 has 2070 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	140	103	0	0	105	26	0	0	0	0	70	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	140	0	10	100	0	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	104	10	0	150	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	44	56	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	100	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	148	0	100	0	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	105	0	0	0	148	0	110	0	0	50	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	26	114	0	0	0	110	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	100	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	140	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	100	52	0	0	140	0	20	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	100	30	0	110	0	0	0	130	0	0	0	0	0	0	0	0	0	0	100	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	70	0	0	0	0	80	0	0	0	90	110	0	85	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	95	0	60	0	0	0	0	0	0	0	0	85	0	150	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	90	0	0	0	0	0	0	0	0	0	150	0	104	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	136	16	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	40	112	0	150	0	0	0	0	0	0	0	0	0	0	0	0	80	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	92	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	120	80	110	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	20	116	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	120	0	0	10	0	130	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	120	0	112	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	110	0	120	0	10	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	10	0	120	121	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	132	0	0	0	120	120	0	0	120	0	0	0	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	0	0	116	16	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	130	116	0	120	0	0	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	20	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	10	120	0	120	0	110	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	131	0	126	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	30	0	0	0	30	80	0	0	0	0	0	0	0	0	0	0	110	0	130	0	120	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	10	0	114	16	0	0	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	126	4	120	0	0	0	0	0	96	10	0	0	0	0	0	0	0	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	112	11	0	0	0	0	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	127	0	0	100	0	0	31	106	23	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	100	143	0	0	0	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	0	0	20	0	0	92	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	0	0	160	0	109	0	0	0	0	0	120	38	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	10	0	0	0	0	160	0	114	7	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	100	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	113	0	104	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	0	108	7	119	0	0	0	0	40	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	20	0	0	0	0	0	0	0	108	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	130	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	30	120	12	110	0	0	0	80	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	50	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	50	0	0	0	0	0	0	0	0	0	0	0	17	0	0	0	0	127	0	0	0	0	0	0	0	100	0	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	0	0	92	0	0	0	80	40	93	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	30	120	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	134	0	112	0	0	0	0	0	0	0	0	0	0	115	21	
Rank 49's ghost cells:	0	0	44	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	100	24	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	60	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	116	30	0	110	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	24	116	0	90	0	0	0	0	0	38	0	0	0	0	102	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	90	0	100	140	10	0	0	77	63	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	0	30	140	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	110	0	0	0	114	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	136	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	140	0	0	110	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	16	94	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	110	110	0	0	0	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	0	0	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	107	10	0	140	0	13	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	77	0	0	0	0	107	0	140	0	0	0	120	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	63	120	0	0	110	10	140	0	0	0	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	108	0	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	140	0	12	101	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	108	12	0	140	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	0	96	0	0	0	0	0	19	120	0	0	101	140	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.7627         0.241889
    2          23.7942         0.613842
    3          21.3248         0.896221
    4          19.5913         0.918706
    5          10.0073         0.510804
    6          5.44176          0.54378
    7           4.2369          0.77859
    8          2.51535         0.593677
    9          1.30827         0.520115
   10         0.885653         0.676964
   11         0.613388         0.692582
   12         0.348868         0.568756
   13         0.240788         0.690198
   14         0.157252         0.653073
   15         0.106371         0.676435
   16        0.0645225          0.60658
   17        0.0466332         0.722743
   18        0.0303558         0.650949
   19        0.0165762         0.546061
   20        0.0112534         0.678889
=== rate=0.619904, T=0.137256, TIT=0.00686281, IT=20

 Elapsed time: 0.137256
Rank 0: Matrix-vector product took 0.0016414 seconds
Rank 1: Matrix-vector product took 0.00150179 seconds
Rank 2: Matrix-vector product took 0.00162218 seconds
Rank 3: Matrix-vector product took 0.00150313 seconds
Rank 4: Matrix-vector product took 0.00147878 seconds
Rank 5: Matrix-vector product took 0.0016164 seconds
Rank 6: Matrix-vector product took 0.00153297 seconds
Rank 7: Matrix-vector product took 0.00134061 seconds
Rank 8: Matrix-vector product took 0.00152158 seconds
Rank 9: Matrix-vector product took 0.00151091 seconds
Rank 10: Matrix-vector product took 0.00161345 seconds
Rank 11: Matrix-vector product took 0.00162849 seconds
Rank 12: Matrix-vector product took 0.0016237 seconds
Rank 13: Matrix-vector product took 0.00334096 seconds
Rank 14: Matrix-vector product took 0.00160063 seconds
Rank 15: Matrix-vector product took 0.00163433 seconds
Rank 16: Matrix-vector product took 0.00188197 seconds
Rank 17: Matrix-vector product took 0.00163581 seconds
Rank 18: Matrix-vector product took 0.00149857 seconds
Rank 19: Matrix-vector product took 0.00156717 seconds
Rank 20: Matrix-vector product took 0.00140142 seconds
Rank 21: Matrix-vector product took 0.00150919 seconds
Rank 22: Matrix-vector product took 0.00152713 seconds
Rank 23: Matrix-vector product took 0.00159681 seconds
Rank 24: Matrix-vector product took 0.00148305 seconds
Rank 25: Matrix-vector product took 0.00157195 seconds
Rank 26: Matrix-vector product took 0.0015755 seconds
Rank 27: Matrix-vector product took 0.00150103 seconds
Rank 28: Matrix-vector product took 0.00151141 seconds
Rank 29: Matrix-vector product took 0.00159479 seconds
Rank 30: Matrix-vector product took 0.00162388 seconds
Rank 31: Matrix-vector product took 0.0016162 seconds
Rank 32: Matrix-vector product took 0.00150765 seconds
Rank 33: Matrix-vector product took 0.00159719 seconds
Rank 34: Matrix-vector product took 0.00148794 seconds
Rank 35: Matrix-vector product took 0.0016084 seconds
Rank 36: Matrix-vector product took 0.00162395 seconds
Rank 37: Matrix-vector product took 0.00159461 seconds
Rank 38: Matrix-vector product took 0.00162559 seconds
Rank 39: Matrix-vector product took 0.00157458 seconds
Rank 40: Matrix-vector product took 0.00150114 seconds
Rank 41: Matrix-vector product took 0.00141 seconds
Rank 42: Matrix-vector product took 0.00151643 seconds
Rank 43: Matrix-vector product took 0.00159478 seconds
Rank 44: Matrix-vector product took 0.00148457 seconds
Rank 45: Matrix-vector product took 0.00158778 seconds
Rank 46: Matrix-vector product took 0.00159128 seconds
Rank 47: Matrix-vector product took 0.00151871 seconds
Rank 48: Matrix-vector product took 0.00151948 seconds
Rank 49: Matrix-vector product took 0.00156114 seconds
Rank 50: Matrix-vector product took 0.00160666 seconds
Rank 51: Matrix-vector product took 0.0016078 seconds
Rank 52: Matrix-vector product took 0.0016471 seconds
Rank 53: Matrix-vector product took 0.00162305 seconds
Rank 54: Matrix-vector product took 0.0016057 seconds
Rank 55: Matrix-vector product took 0.00161843 seconds
Rank 56: Matrix-vector product took 0.00151857 seconds
Rank 57: Matrix-vector product took 0.00154944 seconds
Rank 58: Matrix-vector product took 0.00161033 seconds
Rank 59: Matrix-vector product took 0.00162728 seconds
Rank 60: Matrix-vector product took 0.00140428 seconds
Rank 61: Matrix-vector product took 0.0015177 seconds
Rank 62: Matrix-vector product took 0.00152401 seconds
Rank 63: Matrix-vector product took 0.00162345 seconds
Average time for Matrix-vector product is 0.00159056 seconds

Rank 0: copyOwnerToAll took 7.99e-05 seconds
Rank 1: copyOwnerToAll took 6.341e-05 seconds
Rank 2: copyOwnerToAll took 0.000169639 seconds
Rank 3: copyOwnerToAll took 0.000157909 seconds
Rank 4: copyOwnerToAll took 6.976e-05 seconds
Rank 5: copyOwnerToAll took 7.621e-05 seconds
Rank 6: copyOwnerToAll took 5.789e-05 seconds
Rank 7: copyOwnerToAll took 3.886e-05 seconds
Rank 8: copyOwnerToAll took 0.00014704 seconds
Rank 9: copyOwnerToAll took 7.223e-05 seconds
Rank 10: copyOwnerToAll took 0.000157669 seconds
Rank 11: copyOwnerToAll took 8.82e-05 seconds
Rank 12: copyOwnerToAll took 7.621e-05 seconds
Rank 13: copyOwnerToAll took 0.000171059 seconds
Rank 14: copyOwnerToAll took 0.000227889 seconds
Rank 15: copyOwnerToAll took 0.000227609 seconds
Rank 16: copyOwnerToAll took 0.00016172 seconds
Rank 17: copyOwnerToAll took 7.75e-05 seconds
Rank 18: copyOwnerToAll took 7.884e-05 seconds
Rank 19: copyOwnerToAll took 0.00016979 seconds
Rank 20: copyOwnerToAll took 3.637e-05 seconds
Rank 21: copyOwnerToAll took 7.008e-05 seconds
Rank 22: copyOwnerToAll took 5.67e-05 seconds
Rank 23: copyOwnerToAll took 7.502e-05 seconds
Rank 24: copyOwnerToAll took 8.104e-05 seconds
Rank 25: copyOwnerToAll took 7.364e-05 seconds
Rank 26: copyOwnerToAll took 0.00015985 seconds
Rank 27: copyOwnerToAll took 9.832e-05 seconds
Rank 28: copyOwnerToAll took 0.00014965 seconds
Rank 29: copyOwnerToAll took 0.00015517 seconds
Rank 30: copyOwnerToAll took 9.45e-05 seconds
Rank 31: copyOwnerToAll took 0.00017825 seconds
Rank 32: copyOwnerToAll took 6.285e-05 seconds
Rank 33: copyOwnerToAll took 7.635e-05 seconds
Rank 34: copyOwnerToAll took 9.842e-05 seconds
Rank 35: copyOwnerToAll took 0.00013836 seconds
Rank 36: copyOwnerToAll took 0.0001214 seconds
Rank 37: copyOwnerToAll took 0.00019218 seconds
Rank 38: copyOwnerToAll took 0.00014718 seconds
Rank 39: copyOwnerToAll took 0.00010163 seconds
Rank 40: copyOwnerToAll took 5.504e-05 seconds
Rank 41: copyOwnerToAll took 5.24e-05 seconds
Rank 42: copyOwnerToAll took 5.455e-05 seconds
Rank 43: copyOwnerToAll took 8.064e-05 seconds
Rank 44: copyOwnerToAll took 0.00012626 seconds
Rank 45: copyOwnerToAll took 0.00018163 seconds
Rank 46: copyOwnerToAll took 0.00011882 seconds
Rank 47: copyOwnerToAll took 6.46e-05 seconds
Rank 48: copyOwnerToAll took 8.099e-05 seconds
Rank 49: copyOwnerToAll took 0.00013336 seconds
Rank 50: copyOwnerToAll took 0.00015913 seconds
Rank 51: copyOwnerToAll took 9.134e-05 seconds
Rank 52: copyOwnerToAll took 8.483e-05 seconds
Rank 53: copyOwnerToAll took 0.00016993 seconds
Rank 54: copyOwnerToAll took 0.00016294 seconds
Rank 55: copyOwnerToAll took 0.00016287 seconds
Rank 56: copyOwnerToAll took 0.00011412 seconds
Rank 57: copyOwnerToAll took 8.828e-05 seconds
Rank 58: copyOwnerToAll took 6.942e-05 seconds
Rank 59: copyOwnerToAll took 0.00011467 seconds
Rank 60: copyOwnerToAll took 5.049e-05 seconds
Rank 61: copyOwnerToAll took 6.251e-05 seconds
Rank 62: copyOwnerToAll took 6.049e-05 seconds
Rank 63: copyOwnerToAll took 7.166e-05 seconds
Average time for copyOwnertoAll is 0.000108551 seconds
