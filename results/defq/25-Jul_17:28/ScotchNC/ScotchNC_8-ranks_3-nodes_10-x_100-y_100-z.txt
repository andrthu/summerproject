Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 33500
Cell on rank 1 before loadbalancing: 33180
Cell on rank 2 before loadbalancing: 33320
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	350	0	340	0	0	0	0	
From rank 1 to: 	350	0	310	110	0	0	0	190	
From rank 2 to: 	0	310	0	0	0	0	0	370	
From rank 3 to: 	340	110	0	0	200	270	0	180	
From rank 4 to: 	0	0	0	210	0	400	330	120	
From rank 5 to: 	0	0	0	270	400	0	0	0	
From rank 6 to: 	0	0	0	0	330	0	0	500	
From rank 7 to: 	0	190	370	180	130	0	500	0	
loadb
After loadbalancing process 5 has 11670 cells.
After loadbalancing process 1 has 12130 cells.
After loadbalancing process 0 has 11850 cells.
After loadbalancing process 6 has 17490 cells.
After loadbalancing process 2 has 11850 cells.
After loadbalancing process 4 has 12110 cells.
After loadbalancing process 3 has 12230 cells.
After loadbalancing process 7 has 18030 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	350	0	340	0	0	0	0	
Rank 1's ghost cells:	350	0	310	110	0	0	0	190	
Rank 2's ghost cells:	0	310	0	0	0	0	0	370	
Rank 3's ghost cells:	340	110	0	0	200	270	0	180	
Rank 4's ghost cells:	0	0	0	210	0	400	330	120	
Rank 5's ghost cells:	0	0	0	270	400	0	0	0	
Rank 6's ghost cells:	0	0	0	0	330	0	0	500	
Rank 7's ghost cells:	0	190	370	180	130	0	500	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          34.6543         0.216252
    2          20.8497         0.601647
    3           19.887         0.953826
    4          13.3184         0.669706
    5          6.62708         0.497588
    6           4.5085         0.680314
    7          2.52825         0.560776
    8          1.67479         0.662431
    9          1.05833         0.631916
   10         0.590901         0.558334
   11         0.447852         0.757913
   12         0.245925         0.549121
   13         0.163522         0.664928
   14        0.0966653         0.591144
   15        0.0610686         0.631753
   16         0.038527          0.63088
   17        0.0235698         0.611775
   18         0.014944         0.634029
=== rate=0.597163, T=0.90643, TIT=0.0503572, IT=18

 Elapsed time: 0.90643
Rank 0: Matrix-vector product took 0.0094783 seconds
Rank 1: Matrix-vector product took 0.00964969 seconds
Rank 2: Matrix-vector product took 0.00942861 seconds
Rank 3: Matrix-vector product took 0.0097936 seconds
Rank 4: Matrix-vector product took 0.0097231 seconds
Rank 5: Matrix-vector product took 0.00935419 seconds
Rank 6: Matrix-vector product took 0.0142691 seconds
Rank 7: Matrix-vector product took 0.0146284 seconds
Average time for Matrix-vector product is 0.0107906 seconds

Rank 0: copyOwnerToAll took 0.00013612 seconds
Rank 1: copyOwnerToAll took 0.00016701 seconds
Rank 2: copyOwnerToAll took 0.00018231 seconds
Rank 3: copyOwnerToAll took 0.000248161 seconds
Rank 4: copyOwnerToAll took 0.00020833 seconds
Rank 5: copyOwnerToAll took 0.00016147 seconds
Rank 6: copyOwnerToAll took 0.0001411 seconds
Rank 7: copyOwnerToAll took 0.0002221 seconds
Average time for copyOwnertoAll is 0.000183325 seconds
