Number of cores/ranks per node is: 4
Bad partitioner
 Cell on rank 0 before loadbalancing: 6240
Cell on rank 1 before loadbalancing: 6250
Cell on rank 2 before loadbalancing: 6240
Cell on rank 3 before loadbalancing: 6210
Cell on rank 4 before loadbalancing: 6240
Cell on rank 5 before loadbalancing: 6260
Cell on rank 6 before loadbalancing: 6260
Cell on rank 7 before loadbalancing: 6270
Cell on rank 8 before loadbalancing: 6310
Cell on rank 9 before loadbalancing: 6230
Cell on rank 10 before loadbalancing: 6245
Cell on rank 11 before loadbalancing: 6255
Cell on rank 12 before loadbalancing: 6280
Cell on rank 13 before loadbalancing: 6230
Cell on rank 14 before loadbalancing: 6260
Cell on rank 15 before loadbalancing: 6220
Edge-cut: 7351
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	310	190	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	310	0	70	200	0	0	0	0	0	0	240	60	0	0	0	0	
From rank 2 to: 	190	60	0	300	0	0	0	160	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	200	300	0	200	0	0	170	0	0	0	160	0	0	0	0	
From rank 4 to: 	0	0	0	210	0	190	0	160	0	0	0	60	160	0	270	0	
From rank 5 to: 	0	0	0	0	190	0	170	160	0	0	0	0	210	0	0	0	
From rank 6 to: 	0	0	0	0	0	170	0	340	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	150	170	160	150	340	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	220	210	60	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	220	0	0	200	0	0	100	200	
From rank 10 to: 	0	240	0	0	0	0	0	0	210	0	0	290	0	0	0	0	
From rank 11 to: 	0	70	0	160	60	0	0	0	60	200	290	0	0	0	220	0	
From rank 12 to: 	0	0	0	0	150	210	0	0	0	0	0	0	0	260	160	60	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	260	0	0	210	
From rank 14 to: 	0	0	0	0	270	0	0	0	0	90	0	220	160	0	0	230	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	200	0	0	60	210	230	0	

Edge-cut for node partition: 2205

loadb
After loadbalancing process 1 has 6740 cells.
After loadbalancing process 14 has 6700 cells.
After loadbalancing process 5 has 6800 cells.
After loadbalancing process 0 has 6920 cells.
After loadbalancing process 15 has 7230 cells.
After loadbalancing process 4 has 7240 cells.
After loadbalancing process 3 has 6950 cells.
After loadbalancing process 11 has 6770 cells.
After loadbalancing process 7 has 6985 cells.
After loadbalancing process 10 has 6990 cells.
After loadbalancing process 6 has 6950 cells.
After loadbalancing process 2 has 7130 cells.
After loadbalancing process 13 has 7120 cells.
After loadbalancing process 8 has 7315 cells.
After loadbalancing process 12 has 7240 cells.
After loadbalancing process 9 has 7290 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	0	0	0	0	0	200	0	0	0	0	0	0	60	210	230	
Rank 1's ghost cells:	0	0	310	190	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	310	0	70	200	0	0	240	60	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	190	60	0	300	0	0	0	0	0	0	0	160	0	0	0	
Rank 4's ghost cells:	0	0	200	300	0	0	0	0	160	200	0	0	170	0	0	0	
Rank 5's ghost cells:	0	0	0	0	0	0	220	210	60	0	0	0	0	0	0	0	
Rank 6's ghost cells:	200	0	0	0	0	220	0	0	200	0	0	0	0	0	0	100	
Rank 7's ghost cells:	0	0	240	0	0	210	0	0	290	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	70	0	160	60	200	290	0	60	0	0	0	0	0	220	
Rank 9's ghost cells:	0	0	0	0	210	0	0	0	60	0	190	0	160	160	0	270	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	190	0	170	160	210	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	170	0	340	0	0	0	
Rank 12's ghost cells:	0	0	0	150	170	0	0	0	0	160	150	340	0	0	0	0	
Rank 13's ghost cells:	60	0	0	0	0	0	0	0	0	150	210	0	0	0	260	160	
Rank 14's ghost cells:	210	0	0	0	0	0	0	0	0	0	0	0	0	260	0	0	
Rank 15's ghost cells:	230	0	0	0	0	0	90	0	220	270	0	0	0	160	0	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          36.2595         0.226269
    2          21.9419         0.605136
    3          20.7306         0.944792
    4          16.5229         0.797031
    5          7.99402         0.483813
    6          4.76535         0.596115
    7          3.17368         0.665992
    8           2.1167         0.666953
    9           1.1826         0.558702
   10           0.7568         0.639944
   11         0.563561         0.744663
   12          0.25481         0.452143
   13         0.178507          0.70055
   14         0.123583         0.692312
   15        0.0631971         0.511375
   16        0.0462988          0.73261
   17        0.0299656         0.647223
   18        0.0167118         0.557699
   19        0.0118532         0.709273
=== rate=0.606151, T=0.362793, TIT=0.0190943, IT=19

 Elapsed time: 0.362793
Rank 0: Matrix-vector product took 0.0054787 seconds
Rank 1: Matrix-vector product took 0.00532705 seconds
Rank 2: Matrix-vector product took 0.00561999 seconds
Rank 3: Matrix-vector product took 0.00549534 seconds
Rank 4: Matrix-vector product took 0.00571402 seconds
Rank 5: Matrix-vector product took 0.00537729 seconds
Rank 6: Matrix-vector product took 0.00553127 seconds
Rank 7: Matrix-vector product took 0.00552533 seconds
Rank 8: Matrix-vector product took 0.00582689 seconds
Rank 9: Matrix-vector product took 0.00577738 seconds
Rank 10: Matrix-vector product took 0.00556201 seconds
Rank 11: Matrix-vector product took 0.00551331 seconds
Rank 12: Matrix-vector product took 0.00575176 seconds
Rank 13: Matrix-vector product took 0.00566406 seconds
Rank 14: Matrix-vector product took 0.00553653 seconds
Rank 15: Matrix-vector product took 0.00570764 seconds
Average time for Matrix-vector product is 0.00558804 seconds

Rank 0: copyOwnerToAll took 0.00022511 seconds
Rank 1: copyOwnerToAll took 7.027e-05 seconds
Rank 2: copyOwnerToAll took 0.0002117 seconds
Rank 3: copyOwnerToAll took 0.00015555 seconds
Rank 4: copyOwnerToAll took 0.00018744 seconds
Rank 5: copyOwnerToAll took 0.00015189 seconds
Rank 6: copyOwnerToAll took 0.00019176 seconds
Rank 7: copyOwnerToAll took 0.00021055 seconds
Rank 8: copyOwnerToAll took 0.000224929 seconds
Rank 9: copyOwnerToAll took 0.000208399 seconds
Rank 10: copyOwnerToAll took 0.000167979 seconds
Rank 11: copyOwnerToAll took 0.00015893 seconds
Rank 12: copyOwnerToAll took 0.000206191 seconds
Rank 13: copyOwnerToAll took 0.000212651 seconds
Rank 14: copyOwnerToAll took 0.000163301 seconds
Rank 15: copyOwnerToAll took 0.000222981 seconds
Average time for copyOwnertoAll is 0.000185602 seconds
