Number of cores/ranks per node is: 10
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 3100
Cell on rank 1 before loadbalancing: 3100
Cell on rank 2 before loadbalancing: 3130
Cell on rank 3 before loadbalancing: 3130
Cell on rank 4 before loadbalancing: 3150
Cell on rank 5 before loadbalancing: 3140
Cell on rank 6 before loadbalancing: 3140
Cell on rank 7 before loadbalancing: 3130
Cell on rank 8 before loadbalancing: 3124
Cell on rank 9 before loadbalancing: 3156
Cell on rank 10 before loadbalancing: 3156
Cell on rank 11 before loadbalancing: 3110
Cell on rank 12 before loadbalancing: 3130
Cell on rank 13 before loadbalancing: 3110
Cell on rank 14 before loadbalancing: 3124
Cell on rank 15 before loadbalancing: 3156
Cell on rank 16 before loadbalancing: 3156
Cell on rank 17 before loadbalancing: 3120
Cell on rank 18 before loadbalancing: 3094
Cell on rank 19 before loadbalancing: 3110
Cell on rank 20 before loadbalancing: 3102
Cell on rank 21 before loadbalancing: 3156
Cell on rank 22 before loadbalancing: 3140
Cell on rank 23 before loadbalancing: 3156
Cell on rank 24 before loadbalancing: 3100
Cell on rank 25 before loadbalancing: 3110
Cell on rank 26 before loadbalancing: 3130
Cell on rank 27 before loadbalancing: 3100
Cell on rank 28 before loadbalancing: 3110
Cell on rank 29 before loadbalancing: 3120
Cell on rank 30 before loadbalancing: 3100
Cell on rank 31 before loadbalancing: 3110
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	200	0	150	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	200	0	150	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	150	0	200	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	150	20	200	0	0	210	10	0	100	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	200	40	130	0	0	70	0	0	0	0	0	100	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	210	200	0	110	0	30	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	190	0	0	20	30	110	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	130	0	190	0	0	0	0	0	0	0	0	0	60	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	100	0	20	0	0	0	190	140	90	0	0	100	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	170	10	0	0	0	0	190	0	0	0	0	0	0	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	70	160	0	0	140	0	0	188	0	0	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	90	0	188	0	0	0	105	0	0	0	0	0	0	0	0	42	0	0	0	0	0	80	135	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	170	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	170	0	52	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	100	0	0	105	150	42	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	42	138	0	0	0	138	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	100	0	0	70	0	0	0	0	0	0	0	0	0	188	25	135	10	0	140	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	188	0	162	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	25	160	0	180	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	135	0	180	0	230	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	230	0	250	136	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	44	110	0	0	90	170	0	0	0	0	
From rank 22 to: 	0	0	0	0	100	0	0	0	0	0	60	0	0	0	0	0	140	0	0	0	136	48	0	166	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	90	40	0	0	0	0	0	0	0	0	0	110	166	0	0	0	40	0	0	0	130	70	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	60	100	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	120	0	110	0	0	100	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	40	50	120	0	190	0	0	0	170	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	100	0	190	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	175	40	135	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	80	190	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	175	0	95	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	145	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	40	95	0	185	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	100	170	0	145	0	185	0	

Edge-cut for node partition: 4910

loadb
After loadbalancing process 29 has 3560 cells.
After loadbalancing process 26 has 3440 cells.
After loadbalancing process 14 has 3640 cells.
After loadbalancing process 15 has 3470 cells.
After loadbalancing process 30 has 3745 cells.
After loadbalancing process 1 has 3460 cells.
After loadbalancing process 0 has 3640 cells.
After loadbalancing process 2 has 3650 cells.
After loadbalancing process 16 has 3644 cells.
After loadbalancing process 3 has 3830 cells.
After loadbalancing process 12 has 3850 cells.
After loadbalancing process 4 has 3680 cells.
After loadbalancing process 19 has 3570 cells.
After loadbalancing process 11 has 3800 cells.
After loadbalancing process 10 has 3728 cells.
After loadbalancing process 6 has 3776 cells.
After loadbalancing process 7 has 3620 cells.
After loadbalancing process 13 has 3806 cells.
After loadbalancing process 8 has 3459 cells.
After loadbalancing process 28 has 3790 cells.
After loadbalancing process 22 has 3864 cells.
After loadbalancing process 27 has 3620 cells.
After loadbalancing process 5 has 3664 cells.
After loadbalancing process 17 has 3824 cells.
After loadbalancing process 31 has 3780 cells.
After loadbalancing process 23 has 3750 cells.
After loadbalancing process 9 has 3655 cells.
After loadbalancing process 20 has 3695 cells.
After loadbalancing process 24 has 3820 cells.
After loadbalancing process 21 has 3660 cells.
After loadbalancing process 25 has 3790 cells.
After loadbalancing process 18 has 3802 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	200	0	150	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	200	0	150	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	150	0	200	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	150	20	200	0	10	10	0	0	0	0	0	0	210	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	190	0	0	20	0	0	0	0	0	0	0	30	110	0	0	0	0	0	0	0	0	190	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	170	10	0	0	0	0	0	0	0	0	0	190	0	0	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	100	150	42	170	0	0	0	0	0	0	105	0	0	0	0	0	0	85	0	
Rank 7's ghost cells:	0	0	0	0	0	0	0	0	162	0	0	0	0	0	0	0	0	188	0	0	0	150	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	160	0	180	0	0	0	0	0	0	0	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	180	0	230	0	0	0	0	0	0	135	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	230	0	0	0	0	0	0	0	10	0	0	0	0	0	0	250	136	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	40	0	0	0	0	0	0	0	200	0	0	0	0	100	0	0	0	130	70	0	0	110	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	210	110	0	0	0	0	0	0	200	0	30	0	0	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	100	0	190	100	0	0	0	0	0	20	0	0	0	42	0	0	0	0	0	140	90	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	0	
Rank 15's ghost cells:	0	0	0	0	0	0	52	0	0	0	0	0	0	0	170	0	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	138	170	0	0	0	0	0	0	42	0	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	188	25	135	10	100	0	0	0	0	0	0	0	0	0	70	0	0	0	140	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	90	40	110	166	0	0	40	0	0	70	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	0	0	110	0	0	175	135	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	40	0	0	0	145	0	0	0	0	0	0	95	185	
Rank 21's ghost cells:	0	0	0	0	190	0	0	150	0	0	0	130	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	70	160	140	0	0	0	0	90	0	0	0	0	188	0	60	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	105	0	0	0	0	0	0	90	0	0	0	0	42	0	135	0	188	0	0	0	0	0	0	0	80	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	250	0	0	0	0	0	0	0	110	0	0	0	0	0	0	44	0	0	90	170	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	136	100	0	0	0	0	0	140	166	0	0	0	60	0	48	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	60	100	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	180	0	120	0	0	100	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	0	90	0	50	120	0	190	0	170	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	100	0	190	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	85	0	0	0	0	0	0	0	190	0	0	0	0	175	95	0	0	80	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	145	185	0	0	0	0	0	0	100	170	0	0	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.7113         0.235328
    2          22.8793         0.606697
    3          21.2363         0.928187
    4          18.8526         0.887756
    5          9.33787         0.495308
    6          5.18155         0.554896
    7          3.77698         0.728929
    8           2.4464         0.647715
    9          1.24621         0.509403
   10         0.810029         0.649997
   11         0.597681         0.737852
   12         0.293215         0.490588
   13         0.207648         0.708174
   14         0.140923         0.678664
   15        0.0816142         0.579141
   16        0.0550921         0.675031
   17        0.0377169         0.684616
   18        0.0243994          0.64691
   19        0.0155704         0.638148
=== rate=0.614916, T=0.240348, TIT=0.0126499, IT=19

 Elapsed time: 0.240348
Rank 0: Matrix-vector product took 0.00286857 seconds
Rank 1: Matrix-vector product took 0.00285768 seconds
Rank 2: Matrix-vector product took 0.00287589 seconds
Rank 3: Matrix-vector product took 0.00301155 seconds
Rank 4: Matrix-vector product took 0.00289498 seconds
Rank 5: Matrix-vector product took 0.0028805 seconds
Rank 6: Matrix-vector product took 0.00296627 seconds
Rank 7: Matrix-vector product took 0.00283333 seconds
Rank 8: Matrix-vector product took 0.00273337 seconds
Rank 9: Matrix-vector product took 0.00286739 seconds
Rank 10: Matrix-vector product took 0.00293949 seconds
Rank 11: Matrix-vector product took 0.00300281 seconds
Rank 12: Matrix-vector product took 0.00301359 seconds
Rank 13: Matrix-vector product took 0.00299622 seconds
Rank 14: Matrix-vector product took 0.00287503 seconds
Rank 15: Matrix-vector product took 0.00273769 seconds
Rank 16: Matrix-vector product took 0.00288887 seconds
Rank 17: Matrix-vector product took 0.00300199 seconds
Rank 18: Matrix-vector product took 0.00299281 seconds
Rank 19: Matrix-vector product took 0.00281093 seconds
Rank 20: Matrix-vector product took 0.00290675 seconds
Rank 21: Matrix-vector product took 0.00289391 seconds
Rank 22: Matrix-vector product took 0.00306015 seconds
Rank 23: Matrix-vector product took 0.0029488 seconds
Rank 24: Matrix-vector product took 0.00301996 seconds
Rank 25: Matrix-vector product took 0.00297398 seconds
Rank 26: Matrix-vector product took 0.00272289 seconds
Rank 27: Matrix-vector product took 0.00283517 seconds
Rank 28: Matrix-vector product took 0.00298531 seconds
Rank 29: Matrix-vector product took 0.00279908 seconds
Rank 30: Matrix-vector product took 0.00293538 seconds
Rank 31: Matrix-vector product took 0.00293967 seconds
Average time for Matrix-vector product is 0.00290844 seconds

Rank 0: copyOwnerToAll took 6.486e-05 seconds
Rank 1: copyOwnerToAll took 6.49e-05 seconds
Rank 2: copyOwnerToAll took 7.364e-05 seconds
Rank 3: copyOwnerToAll took 0.000176579 seconds
Rank 4: copyOwnerToAll took 0.000195549 seconds
Rank 5: copyOwnerToAll took 0.0001139 seconds
Rank 6: copyOwnerToAll took 0.000211539 seconds
Rank 7: copyOwnerToAll took 0.00014754 seconds
Rank 8: copyOwnerToAll took 0.00010556 seconds
Rank 9: copyOwnerToAll took 0.000220519 seconds
Rank 10: copyOwnerToAll took 0.000277111 seconds
Rank 11: copyOwnerToAll took 0.00019121 seconds
Rank 12: copyOwnerToAll took 0.000231171 seconds
Rank 13: copyOwnerToAll took 0.00019659 seconds
Rank 14: copyOwnerToAll took 0.000227711 seconds
Rank 15: copyOwnerToAll took 0.000240201 seconds
Rank 16: copyOwnerToAll took 0.00017864 seconds
Rank 17: copyOwnerToAll took 0.00020353 seconds
Rank 18: copyOwnerToAll took 0.000213521 seconds
Rank 19: copyOwnerToAll took 0.0002047 seconds
Rank 20: copyOwnerToAll took 0.00021142 seconds
Rank 21: copyOwnerToAll took 0.00017419 seconds
Rank 22: copyOwnerToAll took 0.00021093 seconds
Rank 23: copyOwnerToAll took 0.0001986 seconds
Rank 24: copyOwnerToAll took 0.0002441 seconds
Rank 25: copyOwnerToAll took 0.00024417 seconds
Rank 26: copyOwnerToAll took 0.00012656 seconds
Rank 27: copyOwnerToAll took 0.00022415 seconds
Rank 28: copyOwnerToAll took 0.00020063 seconds
Rank 29: copyOwnerToAll took 8.178e-05 seconds
Rank 30: copyOwnerToAll took 0.000198639 seconds
Rank 31: copyOwnerToAll took 0.000215519 seconds
Average time for copyOwnertoAll is 0.000183427 seconds
