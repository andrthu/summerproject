Number of cores/ranks per node is: 5
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 33380
Cell on rank 1 before loadbalancing: 33313
Cell on rank 2 before loadbalancing: 33307
Edge-cut: 2027
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	207	160	290	0	0	0	0	0	0	60	0	0	140	0	0	
From rank 1 to: 	207	0	270	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	160	270	0	0	0	0	0	0	0	0	0	0	200	130	0	0	
From rank 3 to: 	290	0	0	0	100	200	0	0	0	0	160	0	0	0	0	0	
From rank 4 to: 	0	0	0	100	0	310	0	0	0	160	190	0	0	0	0	0	
From rank 5 to: 	0	0	0	200	310	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	0	0	200	240	210	0	60	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	0	200	0	230	0	0	200	0	0	282	0	
From rank 8 to: 	0	0	0	0	0	0	240	220	0	0	0	0	0	0	58	0	
From rank 9 to: 	0	0	0	0	160	0	210	0	0	0	150	150	0	0	0	0	
From rank 10 to: 	60	0	0	160	200	0	0	0	0	140	0	300	0	110	0	0	
From rank 11 to: 	0	0	0	0	0	0	70	200	0	150	300	0	0	190	60	0	
From rank 12 to: 	0	0	200	0	0	0	0	0	0	0	0	0	0	336	64	200	
From rank 13 to: 	140	0	140	0	0	0	0	0	0	0	110	180	326	0	260	0	
From rank 14 to: 	0	0	0	0	0	0	0	282	58	0	0	60	70	250	0	320	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	200	0	320	0	
loadb
After loadbalancing process 1 has 6050 cells.
After loadbalancing process 5 has 6050 cells.
After loadbalancing process 8 has 6051 cells.
After loadbalancing process 9 has 6220 cells.
After loadbalancing process 6 has 6290 cells.
After loadbalancing process 3 has 6336 cells.
After loadbalancing process 15 has 8830 cells.
After loadbalancing process 2 has 6300 cells.
After loadbalancing process 7 has 6432 cells.
After loadbalancing process 0 has 6438 cells.
After loadbalancing process 14 has 9353 cells.
After loadbalancing process 12 has 9136 cells.
After loadbalancing process 4 has 6320 cells.
After loadbalancing process 13 has 9504 cells.
After loadbalancing process 11 has 6530 cells.
After loadbalancing process 10 has 6540 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	207	160	290	0	0	0	0	0	0	60	0	0	140	0	0	
Rank 1's ghost cells:	207	0	270	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	160	270	0	0	0	0	0	0	0	0	0	0	200	130	0	0	
Rank 3's ghost cells:	290	0	0	0	100	200	0	0	0	0	160	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	100	0	310	0	0	0	160	190	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	200	310	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	0	0	200	240	210	0	60	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	200	0	230	0	0	200	0	0	282	0	
Rank 8's ghost cells:	0	0	0	0	0	0	240	220	0	0	0	0	0	0	58	0	
Rank 9's ghost cells:	0	0	0	0	160	0	210	0	0	0	150	150	0	0	0	0	
Rank 10's ghost cells:	60	0	0	160	200	0	0	0	0	140	0	300	0	110	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	70	200	0	150	300	0	0	190	60	0	
Rank 12's ghost cells:	0	0	200	0	0	0	0	0	0	0	0	0	0	336	64	200	
Rank 13's ghost cells:	140	0	140	0	0	0	0	0	0	0	110	180	326	0	260	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	282	58	0	0	60	70	250	0	320	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	200	0	320	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          36.2649         0.226303
    2          21.9057         0.604046
    3          20.6415         0.942292
    4          16.4702         0.797914
    5           8.0864         0.490972
    6          4.84434         0.599073
    7          3.21034         0.662699
    8          2.16144         0.673274
    9          1.17707         0.544577
   10         0.759688         0.645406
   11         0.582364         0.766583
   12         0.272961         0.468713
   13         0.198499         0.727205
   14         0.136977         0.690066
   15         0.069349         0.506281
   16        0.0475479         0.685632
   17        0.0299995         0.630933
   18        0.0168402         0.561349
   19        0.0121534         0.721689
=== rate=0.60695, T=0.520699, TIT=0.0274052, IT=19

 Elapsed time: 0.520699
Rank 0: Matrix-vector product took 0.00515046 seconds
Rank 1: Matrix-vector product took 0.00476127 seconds
Rank 2: Matrix-vector product took 0.00505221 seconds
Rank 3: Matrix-vector product took 0.0049752 seconds
Rank 4: Matrix-vector product took 0.00506611 seconds
Rank 5: Matrix-vector product took 0.00481161 seconds
Rank 6: Matrix-vector product took 0.00499335 seconds
Rank 7: Matrix-vector product took 0.00520965 seconds
Rank 8: Matrix-vector product took 0.00478817 seconds
Rank 9: Matrix-vector product took 0.00499262 seconds
Rank 10: Matrix-vector product took 0.00528796 seconds
Rank 11: Matrix-vector product took 0.00527263 seconds
Rank 12: Matrix-vector product took 0.00743471 seconds
Rank 13: Matrix-vector product took 0.00770243 seconds
Rank 14: Matrix-vector product took 0.00757176 seconds
Rank 15: Matrix-vector product took 0.00706927 seconds
Average time for Matrix-vector product is 0.00563371 seconds

Rank 0: copyOwnerToAll took 0.000207229 seconds
Rank 1: copyOwnerToAll took 7.886e-05 seconds
Rank 2: copyOwnerToAll took 0.000199089 seconds
Rank 3: copyOwnerToAll took 0.00013692 seconds
Rank 4: copyOwnerToAll took 0.000221929 seconds
Rank 5: copyOwnerToAll took 0.000238411 seconds
Rank 6: copyOwnerToAll took 0.000182801 seconds
Rank 7: copyOwnerToAll took 0.000196831 seconds
Rank 8: copyOwnerToAll took 0.000171991 seconds
Rank 9: copyOwnerToAll took 0.000204231 seconds
Rank 10: copyOwnerToAll took 0.00019139 seconds
Rank 11: copyOwnerToAll took 0.00022769 seconds
Rank 12: copyOwnerToAll took 0.0001514 seconds
Rank 13: copyOwnerToAll took 0.0001731 seconds
Rank 14: copyOwnerToAll took 0.00021279 seconds
Rank 15: copyOwnerToAll took 0.000146 seconds
Average time for copyOwnertoAll is 0.000183791 seconds
