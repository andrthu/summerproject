Number of cores/ranks per node is: 32
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	160	70	0	90	112	0	0	0	0	0	0	0	0	50	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	160	0	80	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	70	80	0	180	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	170	0	0	50	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	90	0	0	0	0	104	0	46	140	90	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	122	0	70	50	104	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	90	0	100	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	46	50	160	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	140	0	0	90	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	90	0	0	0	90	0	136	0	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	0	136	0	148	0	30	40	0	0	0	0	0	0	0	0	0	0	0	0	0	30	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	148	0	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	60	0	0	0	0	0	50	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	114	0	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	10	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	30	126	114	0	130	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	
From rank 14 to: 	50	0	0	0	30	0	0	0	0	90	40	0	0	130	0	154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	50	90	0	0	0	0	0	0	0	0	0	0	106	22	154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	70	0	14	0	100	0	80	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	35	100	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	35	0	165	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	165	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	14	0	200	0	0	154	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	0	50	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	82	50	0	156	50	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	156	0	0	0	0	0	0	0	50	70	60	60	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	60	0	0	110	47	118	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	20	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	90	0	108	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	90	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	0	0	0	0	0	118	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	82	70	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	0	0	130	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	30	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	70	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	90	60	0	0	80	23	0	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	100	117	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	160	64	0	50	10	70	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	110	160	0	36	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	36	0	120	130	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	50	60	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	130	0	0	170	0	0	0	0	0	50	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	0	0	170	0	0	162	0	0	0	0	80	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	50	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	162	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	48	0	0	0	0	90	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	120	0	59	0	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	48	110	0	80	0	0	0	0	0	130	0	72	0	0	0	30	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	70	50	0	0	0	0	120	80	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	150	105	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	20	0	0	0	59	0	75	150	0	25	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	25	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	46	0	0	0	130	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	100	0	0	114	0	0	0	0	30	0	120	30	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	130	0	47	47	0	34	0	100	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	47	0	150	0	0	0	0	0	0	0	0	0	110	20	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	80	0	0	0	0	0	0	50	150	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	40	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	100	74	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	40	0	0	160	0	0	56	0	0	110	33	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	60	0	90	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	98	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	30	0	0	0	0	0	0	100	0	0	84	46	98	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	160	0	0	0	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	40	40	0	68	100	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	120	0	0	0	30	0	140	160	0	0	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	50	10	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	160	50	137	0	0	0	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	131	0	0	0	0	0	0	0	0	40	160	0	0	149	58	0	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	120	0	0	0	0	0	0	0	0	0	149	0	140	0	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	78	0	0	50	130	0	160	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	160	0	
loadb
After loadbalancing process 63 has 1830 cells.
After loadbalancing process 37 has 2020 cells.
After loadbalancing process 43 has 2055 cells.
After loadbalancing process 46 has 1810 cells.
After loadbalancing process 50 has 1968 cells.
After loadbalancing process 61 has 1985 cells.
After loadbalancing process 44 has 1895 cells.
After loadbalancing process 48 has 2069 cells.
After loadbalancing process 49 has 2058 cells.
After loadbalancing process 60 has 2083 cells.
After loadbalancing process 62 has 1983 cells.
After loadbalancing process 47 has 1950 cells.
After loadbalancing process 34 has 2040 cells.
After loadbalancing process 40 has 1934 cells.
After loadbalancing process 45 has 2115 cells.
After loadbalancing process 19 has 1813 cells.
After loadbalancing process 41 has 2050 cells.
After loadbalancing process 30 has 2060 cells.
After loadbalancing process 36 has 2040 cells.
After loadbalancing process 57 has 1974 cells.
After loadbalancing process 42 has 2080 cells.
After loadbalancing process 56 has 1930 cells.
After loadbalancing process 28 has 2060 cells.
After loadbalancing process 32 has 2080 cells.
After loadbalancing process 58 has 2025 cells.
After loadbalancing process 39 has 1940 cells.
After loadbalancing process 51 has 1929 cells.
After loadbalancing process 53 has 2149 cells.
After loadbalancing process 35 has 2050 cells.
After loadbalancing process 38 has 1920 cells.
After loadbalancing process 59 has 2090 cells.
After loadbalancing process 33 has 2102 cells.
After loadbalancing process 55 has 2052 cells.
After loadbalancing process 2 has 1970 cells.
After loadbalancing process 7 has 2099 cells.
After loadbalancing process 9 has 2030 cells.
After loadbalancing process 4 has 2050 cells.
After loadbalancing process 13 has 2100 cells.
After loadbalancing process 20 has 2030 cells.
After loadbalancing process 25 has 2078 cells.
After loadbalancing process 26 has 1964 cells.
After loadbalancing process 0 has 2082 cells.
After loadbalancing process 6 has 1909 cells.
After loadbalancing process 10 has 2104 cells.
After loadbalancing process 16 has 2032 cells.
After loadbalancing process 21 has 1948 cells.
After loadbalancing process 23 has 2086 cells.
After loadbalancing process 3 has 1880 cells.
After loadbalancing process 8 has 2062 cells.
After loadbalancing process 11 has 2084 cells.
After loadbalancing process 15 has 1974 cells.
After loadbalancing process 14 has 2062 cells.
After loadbalancing process 1 has 1900 cells.
After loadbalancing process 31 has 2057 cells.
After loadbalancing process 12 has 1900 cells.
After loadbalancing process 52 has 2150 cells.
After loadbalancing process 17 has 1885 cells.
After loadbalancing process 18 has 2022 cells.
After loadbalancing process 54 has 2050 cells.
After loadbalancing process 24 has 2061 cells.
After loadbalancing process 29 has 2076 cells.
After loadbalancing process 5 has 2034 cells.
After loadbalancing process 22 has 2080 cells.
After loadbalancing process 27 has 1933 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	160	70	0	90	112	0	0	0	0	0	0	0	0	50	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	160	0	80	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	70	80	0	180	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	170	0	0	50	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	90	0	0	0	0	104	0	46	140	90	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	122	0	70	50	104	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	90	0	100	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	46	50	160	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	140	0	0	90	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	90	0	0	0	90	0	136	0	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	136	0	148	0	30	40	0	0	0	0	0	0	0	0	0	0	0	0	0	30	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	148	0	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	60	0	0	0	0	0	50	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	114	0	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	10	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	30	126	114	0	130	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	
Rank 14's ghost cells:	50	0	0	0	30	0	0	0	0	90	40	0	0	130	0	154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	50	90	0	0	0	0	0	0	0	0	0	0	106	22	154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	70	0	14	0	100	0	80	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	35	100	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	35	0	165	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	165	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	14	0	200	0	0	154	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	0	50	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	82	50	0	156	50	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	156	0	0	0	0	0	0	0	50	70	60	60	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	60	0	0	110	47	118	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	20	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	90	0	108	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	90	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	0	0	0	0	0	118	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	82	70	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	0	0	0	130	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	30	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	70	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	90	60	0	0	80	23	0	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	100	117	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	160	64	0	50	10	70	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	110	160	0	36	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	36	0	120	130	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	50	60	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	130	0	0	170	0	0	0	0	0	50	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	0	0	170	0	0	162	0	0	0	0	80	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	50	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	162	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	48	0	0	0	0	90	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	110	120	0	59	0	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	48	110	0	80	0	0	0	0	0	130	0	72	0	0	0	30	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	70	50	0	0	0	0	120	80	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	150	105	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	20	0	0	0	59	0	75	150	0	25	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	25	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	46	0	0	0	130	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	100	0	0	114	0	0	0	0	30	0	120	30	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	130	0	47	47	0	34	0	100	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	47	0	150	0	0	0	0	0	0	0	0	0	110	20	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	80	0	0	0	0	0	0	50	150	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	40	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	100	74	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	40	0	0	160	0	0	56	0	0	110	33	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	60	0	90	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	98	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	30	0	0	0	0	0	0	100	0	0	84	46	98	0	0	0	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	160	0	0	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	40	40	0	68	100	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	120	0	0	0	30	0	140	160	0	0	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	50	10	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	160	50	137	0	0	0	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	131	0	0	0	0	0	0	0	0	40	160	0	0	149	58	0	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	120	0	0	0	0	0	0	0	0	0	149	0	140	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	78	0	0	50	130	0	160	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	160	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          40.1258         0.250395
    2          24.5553         0.611958
    3           22.025         0.896958
    4          21.4644         0.974545
    5          11.0453         0.514587
    6          5.43117         0.491718
    7          4.26777         0.785791
    8           2.5953         0.608117
    9          1.29743         0.499914
   10          0.95911          0.73924
   11         0.626972         0.653702
   12         0.351149         0.560071
   13         0.243358         0.693034
   14         0.164176         0.674627
   15         0.100169         0.610129
   16        0.0628145         0.627088
   17        0.0455823         0.725666
   18        0.0279854         0.613952
   19        0.0175742         0.627978
   20        0.0115017         0.654467
=== rate=0.620581, T=0.127538, TIT=0.00637688, IT=20

 Elapsed time: 0.127538
Rank 0: Matrix-vector product took 0.00163062 seconds
Rank 1: Matrix-vector product took 0.00149245 seconds
Rank 2: Matrix-vector product took 0.00153518 seconds
Rank 3: Matrix-vector product took 0.00146592 seconds
Rank 4: Matrix-vector product took 0.00160813 seconds
Rank 5: Matrix-vector product took 0.00158189 seconds
Rank 6: Matrix-vector product took 0.00150587 seconds
Rank 7: Matrix-vector product took 0.00164263 seconds
Rank 8: Matrix-vector product took 0.00160676 seconds
Rank 9: Matrix-vector product took 0.00159042 seconds
Rank 10: Matrix-vector product took 0.00163166 seconds
Rank 11: Matrix-vector product took 0.00163037 seconds
Rank 12: Matrix-vector product took 0.0014805 seconds
Rank 13: Matrix-vector product took 0.00165094 seconds
Rank 14: Matrix-vector product took 0.00161363 seconds
Rank 15: Matrix-vector product took 0.0015402 seconds
Rank 16: Matrix-vector product took 0.00159185 seconds
Rank 17: Matrix-vector product took 0.00146828 seconds
Rank 18: Matrix-vector product took 0.00157105 seconds
Rank 19: Matrix-vector product took 0.00141899 seconds
Rank 20: Matrix-vector product took 0.00159843 seconds
Rank 21: Matrix-vector product took 0.00153118 seconds
Rank 22: Matrix-vector product took 0.00162597 seconds
Rank 23: Matrix-vector product took 0.0016299 seconds
Rank 24: Matrix-vector product took 0.00161092 seconds
Rank 25: Matrix-vector product took 0.0016254 seconds
Rank 26: Matrix-vector product took 0.00153808 seconds
Rank 27: Matrix-vector product took 0.00152066 seconds
Rank 28: Matrix-vector product took 0.00160392 seconds
Rank 29: Matrix-vector product took 0.00161359 seconds
Rank 30: Matrix-vector product took 0.00162745 seconds
Rank 31: Matrix-vector product took 0.00161709 seconds
Rank 32: Matrix-vector product took 0.00161024 seconds
Rank 33: Matrix-vector product took 0.00162175 seconds
Rank 34: Matrix-vector product took 0.00160101 seconds
Rank 35: Matrix-vector product took 0.00158809 seconds
Rank 36: Matrix-vector product took 0.00158353 seconds
Rank 37: Matrix-vector product took 0.00157367 seconds
Rank 38: Matrix-vector product took 0.00149553 seconds
Rank 39: Matrix-vector product took 0.00150547 seconds
Rank 40: Matrix-vector product took 0.00149147 seconds
Rank 41: Matrix-vector product took 0.00159071 seconds
Rank 42: Matrix-vector product took 0.00161353 seconds
Rank 43: Matrix-vector product took 0.00159295 seconds
Rank 44: Matrix-vector product took 0.00146794 seconds
Rank 45: Matrix-vector product took 0.00163769 seconds
Rank 46: Matrix-vector product took 0.00140518 seconds
Rank 47: Matrix-vector product took 0.00151064 seconds
Rank 48: Matrix-vector product took 0.00159806 seconds
Rank 49: Matrix-vector product took 0.00159065 seconds
Rank 50: Matrix-vector product took 0.00151346 seconds
Rank 51: Matrix-vector product took 0.0014945 seconds
Rank 52: Matrix-vector product took 0.00165436 seconds
Rank 53: Matrix-vector product took 0.00166306 seconds
Rank 54: Matrix-vector product took 0.0015943 seconds
Rank 55: Matrix-vector product took 0.00158933 seconds
Rank 56: Matrix-vector product took 0.00150313 seconds
Rank 57: Matrix-vector product took 0.00152028 seconds
Rank 58: Matrix-vector product took 0.00156446 seconds
Rank 59: Matrix-vector product took 0.00160915 seconds
Rank 60: Matrix-vector product took 0.00160321 seconds
Rank 61: Matrix-vector product took 0.00153339 seconds
Rank 62: Matrix-vector product took 0.00152367 seconds
Rank 63: Matrix-vector product took 0.00142274 seconds
Average time for Matrix-vector product is 0.00156667 seconds

Rank 0: copyOwnerToAll took 8.47e-05 seconds
Rank 1: copyOwnerToAll took 4.118e-05 seconds
Rank 2: copyOwnerToAll took 6.359e-05 seconds
Rank 3: copyOwnerToAll took 6.928e-05 seconds
Rank 4: copyOwnerToAll took 7.839e-05 seconds
Rank 5: copyOwnerToAll took 8.193e-05 seconds
Rank 6: copyOwnerToAll took 9.649e-05 seconds
Rank 7: copyOwnerToAll took 8.688e-05 seconds
Rank 8: copyOwnerToAll took 6.726e-05 seconds
Rank 9: copyOwnerToAll took 6.193e-05 seconds
Rank 10: copyOwnerToAll took 0.00014509 seconds
Rank 11: copyOwnerToAll took 0.00015536 seconds
Rank 12: copyOwnerToAll took 0.00015623 seconds
Rank 13: copyOwnerToAll took 0.00016386 seconds
Rank 14: copyOwnerToAll took 8.251e-05 seconds
Rank 15: copyOwnerToAll took 7.362e-05 seconds
Rank 16: copyOwnerToAll took 0.00013184 seconds
Rank 17: copyOwnerToAll took 5.603e-05 seconds
Rank 18: copyOwnerToAll took 6.28e-05 seconds
Rank 19: copyOwnerToAll took 4.051e-05 seconds
Rank 20: copyOwnerToAll took 6.664e-05 seconds
Rank 21: copyOwnerToAll took 0.00011284 seconds
Rank 22: copyOwnerToAll took 6.683e-05 seconds
Rank 23: copyOwnerToAll took 0.00014054 seconds
Rank 24: copyOwnerToAll took 8.057e-05 seconds
Rank 25: copyOwnerToAll took 0.00014217 seconds
Rank 26: copyOwnerToAll took 6.285e-05 seconds
Rank 27: copyOwnerToAll took 9.472e-05 seconds
Rank 28: copyOwnerToAll took 7.586e-05 seconds
Rank 29: copyOwnerToAll took 0.00015872 seconds
Rank 30: copyOwnerToAll took 8.811e-05 seconds
Rank 31: copyOwnerToAll took 0.00016691 seconds
Rank 32: copyOwnerToAll took 0.00017942 seconds
Rank 33: copyOwnerToAll took 0.0001244 seconds
Rank 34: copyOwnerToAll took 6.422e-05 seconds
Rank 35: copyOwnerToAll took 9.06e-05 seconds
Rank 36: copyOwnerToAll took 9.827e-05 seconds
Rank 37: copyOwnerToAll took 7.878e-05 seconds
Rank 38: copyOwnerToAll took 0.00013249 seconds
Rank 39: copyOwnerToAll took 7.161e-05 seconds
Rank 40: copyOwnerToAll took 0.00012809 seconds
Rank 41: copyOwnerToAll took 8.441e-05 seconds
Rank 42: copyOwnerToAll took 8.538e-05 seconds
Rank 43: copyOwnerToAll took 8.464e-05 seconds
Rank 44: copyOwnerToAll took 5.708e-05 seconds
Rank 45: copyOwnerToAll took 7.628e-05 seconds
Rank 46: copyOwnerToAll took 6.121e-05 seconds
Rank 47: copyOwnerToAll took 9.163e-05 seconds
Rank 48: copyOwnerToAll took 7.334e-05 seconds
Rank 49: copyOwnerToAll took 8.812e-05 seconds
Rank 50: copyOwnerToAll took 6.504e-05 seconds
Rank 51: copyOwnerToAll took 7.615e-05 seconds
Rank 52: copyOwnerToAll took 0.00016003 seconds
Rank 53: copyOwnerToAll took 0.00011834 seconds
Rank 54: copyOwnerToAll took 0.00013595 seconds
Rank 55: copyOwnerToAll took 0.00010031 seconds
Rank 56: copyOwnerToAll took 9.449e-05 seconds
Rank 57: copyOwnerToAll took 9.638e-05 seconds
Rank 58: copyOwnerToAll took 9.968e-05 seconds
Rank 59: copyOwnerToAll took 0.00015498 seconds
Rank 60: copyOwnerToAll took 7.718e-05 seconds
Rank 61: copyOwnerToAll took 6.31e-05 seconds
Rank 62: copyOwnerToAll took 7.261e-05 seconds
Rank 63: copyOwnerToAll took 5.908e-05 seconds
Average time for copyOwnertoAll is 9.53052e-05 seconds
