Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 25009
Cell on rank 1 before loadbalancing: 25005
Cell on rank 2 before loadbalancing: 24980
Cell on rank 3 before loadbalancing: 25006
Edge-cut: 2729
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	320	0	250	0	0	160	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	320	0	180	130	0	0	40	255	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	180	0	260	0	0	0	36	0	0	0	0	80	340	0	0	
From rank 3 to: 	240	130	260	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	200	290	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	200	0	80	170	50	310	0	0	0	0	0	0	
From rank 6 to: 	150	40	0	0	290	80	0	250	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	255	36	0	0	180	260	0	220	0	0	0	84	0	0	0	
From rank 8 to: 	0	0	0	0	0	50	0	220	0	260	200	0	280	0	10	0	
From rank 9 to: 	0	0	0	0	0	310	0	0	260	0	40	210	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	190	40	0	260	0	0	250	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	210	260	0	0	0	0	0	
From rank 12 to: 	0	0	80	0	0	0	0	80	280	0	0	0	0	310	290	0	
From rank 13 to: 	0	0	340	0	0	0	0	0	0	0	0	0	310	0	36	254	
From rank 14 to: 	0	0	0	0	0	0	0	0	10	0	250	0	290	26	0	230	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	254	230	0	
loadb
After loadbalancing process 11 has 6655 cells.
After loadbalancing process 4 has 6756 cells.
After loadbalancing process 3 has 6910 cells.
After loadbalancing process 15 has 6673 cells.
After loadbalancing process 12 has 7255 cells.
After loadbalancing process 7 has 7260 cells.
After loadbalancing process 8 has 7323 cells.
After loadbalancing process 0 has 6944 cells.
After loadbalancing process 1 has 7136 cells.
After loadbalancing process 9 has 7127 cells.
After loadbalancing process 10 has 6925 cells.
After loadbalancing process 5 has 7060 cells.
After loadbalancing process 6 has 7074 cells.
After loadbalancing process 14 has 7120 cells.
After loadbalancing process 13 has 7228 cells.
After loadbalancing process 2 has 7200 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	320	0	250	0	0	160	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	320	0	180	130	0	0	40	255	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	180	0	260	0	0	0	36	0	0	0	0	80	340	0	0	
Rank 3's ghost cells:	240	130	260	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	200	290	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	200	0	80	170	50	310	0	0	0	0	0	0	
Rank 6's ghost cells:	150	40	0	0	290	80	0	250	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	255	36	0	0	180	260	0	220	0	0	0	84	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	50	0	220	0	260	200	0	280	0	10	0	
Rank 9's ghost cells:	0	0	0	0	0	310	0	0	260	0	40	210	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	190	40	0	260	0	0	250	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	210	260	0	0	0	0	0	
Rank 12's ghost cells:	0	0	80	0	0	0	0	80	280	0	0	0	0	310	290	0	
Rank 13's ghost cells:	0	0	340	0	0	0	0	0	0	0	0	0	310	0	36	254	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	10	0	250	0	290	26	0	230	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	254	230	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          36.4949         0.227738
    2          22.0498         0.604187
    3          20.8495         0.945564
    4          16.9942         0.815091
    5          8.28315          0.48741
    6          4.91697         0.593611
    7          3.29697         0.670529
    8          2.19594         0.666049
    9          1.18052          0.53759
   10         0.776837         0.658049
   11         0.600035         0.772407
   12         0.282789         0.471288
   13         0.204798         0.724209
   14         0.139588         0.681586
   15        0.0726565         0.520508
   16        0.0521565          0.71785
   17        0.0340028         0.651939
   18         0.019336         0.568659
   19        0.0144609         0.747875
=== rate=0.612528, T=0.347932, TIT=0.0183122, IT=19

 Elapsed time: 0.347932
Rank 0: Matrix-vector product took 0.00565932 seconds
Rank 1: Matrix-vector product took 0.00579855 seconds
Rank 2: Matrix-vector product took 0.00583206 seconds
Rank 3: Matrix-vector product took 0.00560679 seconds
Rank 4: Matrix-vector product took 0.00552572 seconds
Rank 5: Matrix-vector product took 0.00581649 seconds
Rank 6: Matrix-vector product took 0.00575861 seconds
Rank 7: Matrix-vector product took 0.00582496 seconds
Rank 8: Matrix-vector product took 0.00599041 seconds
Rank 9: Matrix-vector product took 0.00581865 seconds
Rank 10: Matrix-vector product took 0.00566054 seconds
Rank 11: Matrix-vector product took 0.00532903 seconds
Rank 12: Matrix-vector product took 0.00589588 seconds
Rank 13: Matrix-vector product took 0.00592481 seconds
Rank 14: Matrix-vector product took 0.00583227 seconds
Rank 15: Matrix-vector product took 0.00539905 seconds
Average time for Matrix-vector product is 0.00572957 seconds

Rank 0: copyOwnerToAll took 0.00015229 seconds
Rank 1: copyOwnerToAll took 0.00025357 seconds
Rank 2: copyOwnerToAll took 0.00020582 seconds
Rank 3: copyOwnerToAll took 9.139e-05 seconds
Rank 4: copyOwnerToAll took 8.803e-05 seconds
Rank 5: copyOwnerToAll took 0.00014808 seconds
Rank 6: copyOwnerToAll took 0.00019313 seconds
Rank 7: copyOwnerToAll took 0.00023342 seconds
Rank 8: copyOwnerToAll took 0.00022747 seconds
Rank 9: copyOwnerToAll took 0.00021626 seconds
Rank 10: copyOwnerToAll took 0.00014187 seconds
Rank 11: copyOwnerToAll took 6.191e-05 seconds
Rank 12: copyOwnerToAll took 0.0001698 seconds
Rank 13: copyOwnerToAll took 0.00014789 seconds
Rank 14: copyOwnerToAll took 0.00015286 seconds
Rank 15: copyOwnerToAll took 9.427e-05 seconds
Average time for copyOwnertoAll is 0.000161129 seconds
