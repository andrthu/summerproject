Number of cores/ranks per node is: 16
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 31235
Cell on rank 1 before loadbalancing: 31253
Cell on rank 2 before loadbalancing: 31225
Cell on rank 3 before loadbalancing: 31265
Cell on rank 4 before loadbalancing: 31254
Cell on rank 5 before loadbalancing: 31252
Cell on rank 6 before loadbalancing: 31246
Cell on rank 7 before loadbalancing: 31254
Cell on rank 8 before loadbalancing: 31243
Cell on rank 9 before loadbalancing: 31252
Cell on rank 10 before loadbalancing: 31249
Cell on rank 11 before loadbalancing: 31253
Cell on rank 12 before loadbalancing: 31251
Cell on rank 13 before loadbalancing: 31253
Cell on rank 14 before loadbalancing: 31266
Cell on rank 15 before loadbalancing: 31244
Cell on rank 16 before loadbalancing: 31259
Cell on rank 17 before loadbalancing: 31258
Cell on rank 18 before loadbalancing: 31254
Cell on rank 19 before loadbalancing: 31253
Cell on rank 20 before loadbalancing: 31251
Cell on rank 21 before loadbalancing: 31248
Cell on rank 22 before loadbalancing: 31243
Cell on rank 23 before loadbalancing: 31243
Cell on rank 24 before loadbalancing: 31243
Cell on rank 25 before loadbalancing: 31248
Cell on rank 26 before loadbalancing: 31237
Cell on rank 27 before loadbalancing: 31252
Cell on rank 28 before loadbalancing: 31243
Cell on rank 29 before loadbalancing: 31244
Cell on rank 30 before loadbalancing: 31256
Cell on rank 31 before loadbalancing: 31273
Edge-cut: 88461
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
From rank 3 to: 	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
From rank 4 to: 	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
From rank 13 to: 	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
From rank 18 to: 	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
From rank 19 to: 	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
From rank 20 to: 	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
From rank 28 to: 	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
From rank 29 to: 	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	

Edge-cut for node partition: 10625

loadb
After loadbalancing process 23 has 34387 cells.
After loadbalancing process 11 has 35522 cells.
After loadbalancing process 24 has 34091 cells.
After loadbalancing process 17 has 35264 cells.
After loadbalancing process 16 has 35836 cells.
After loadbalancing process 9 has 36514 cells.
After loadbalancing process 8 has 34244 cells.
After loadbalancing process 15 has 35510 cells.
After loadbalancing process 25 has 35306 cells.
After loadbalancing process 31 has 35632 cells.
After loadbalancing process 27 has 35111 cells.
After loadbalancing process 14 has 35764 cells.
After loadbalancing process 30 has 36207 cells.
After loadbalancing process 5 has 34508 cells.
After loadbalancing process 22 has 34778 cells.
After loadbalancing process 4 has 34875 cells.
After loadbalancing process 10 has 36748 cells.
After loadbalancing process 18 has 36516 cells.
After loadbalancing process 21 has 35883 cells.
After loadbalancing process 19 has 37123 cells.
After loadbalancing process 28 has 36787 cells.
After loadbalancing process 20 has 36699 cells.
After loadbalancing process 26 has 35803 cells.
After loadbalancing process 6 has 35494 cells.
After loadbalancing process 29 has 37047 cells.
After loadbalancing process 1 has 36077 cells.
After loadbalancing process 13 has 36803 cells.
After loadbalancing process 7 has 35703 cells.
After loadbalancing process 12 has 36860 cells.
After loadbalancing process 0 has 35550 cells.
After loadbalancing process 2 has 36770 cells.
After loadbalancing process 3 has 36762 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
Rank 3's ghost cells:	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
Rank 4's ghost cells:	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
Rank 13's ghost cells:	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
Rank 18's ghost cells:	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
Rank 19's ghost cells:	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
Rank 20's ghost cells:	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
Rank 28's ghost cells:	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
Rank 29's ghost cells:	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.9384         0.223933
    2          32.6586         0.583831
    3           23.167         0.709368
    4          19.9002         0.858992
    5          17.8022         0.894575
    6          14.3405         0.805546
    7          12.4425         0.867647
    8          11.9818         0.962969
    9          10.0942          0.84246
   10          8.90709         0.882401
   11          8.83377         0.991768
   12          7.84531         0.888104
   13          6.87091         0.875799
   14          6.92244           1.0075
   15          6.45689         0.932747
   16          5.73027         0.887466
   17          5.66022         0.987776
   18          5.37372         0.949384
   19          4.84508         0.901626
   20          4.80741         0.992225
   21          4.56438         0.949446
   22          4.15488         0.910285
   23          4.14187         0.996869
   24          3.90939          0.94387
   25          3.56422         0.911708
   26          3.52144         0.987996
   27          3.32898         0.945347
   28          3.04565         0.914891
   29          3.03015         0.994909
   30          2.92174         0.964224
   31          2.80743         0.960876
   32          2.89603          1.03156
   33          2.98469          1.03062
   34          3.04402          1.01988
   35           3.1376          1.03074
   36          2.85616         0.910302
   37          2.32291         0.813296
   38           1.8459         0.794651
   39          1.43591         0.777892
   40          1.20325         0.837966
   41          1.11945         0.930355
   42          1.00095         0.894149
   43         0.872088          0.87126
   44         0.759537         0.870941
   45         0.642011         0.845267
   46         0.544124          0.84753
   47         0.464557          0.85377
   48         0.386473         0.831918
   49          0.31789         0.822541
   50         0.263593         0.829195
   51         0.220329         0.835867
   52         0.187773          0.85224
   53         0.161491         0.860035
   54         0.135465         0.838838
   55         0.115667         0.853852
   56        0.0970093         0.838694
   57        0.0778381         0.802378
   58        0.0646127          0.83009
   59        0.0518633         0.802681
   60        0.0412151         0.794688
   61        0.0331721         0.804852
   62        0.0257889         0.777429
   63         0.019849         0.769671
=== rate=0.860841, T=5.3177, TIT=0.0844079, IT=63

 Elapsed time: 5.3177
Rank 0: Matrix-vector product took 0.0289154 seconds
Rank 1: Matrix-vector product took 0.0292949 seconds
Rank 2: Matrix-vector product took 0.029893 seconds
Rank 3: Matrix-vector product took 0.0304313 seconds
Rank 4: Matrix-vector product took 0.0286393 seconds
Rank 5: Matrix-vector product took 0.0281803 seconds
Rank 6: Matrix-vector product took 0.0289267 seconds
Rank 7: Matrix-vector product took 0.0292443 seconds
Rank 8: Matrix-vector product took 0.0279471 seconds
Rank 9: Matrix-vector product took 0.0297737 seconds
Rank 10: Matrix-vector product took 0.0298998 seconds
Rank 11: Matrix-vector product took 0.0289002 seconds
Rank 12: Matrix-vector product took 0.0298898 seconds
Rank 13: Matrix-vector product took 0.0299503 seconds
Rank 14: Matrix-vector product took 0.0292693 seconds
Rank 15: Matrix-vector product took 0.0287725 seconds
Rank 16: Matrix-vector product took 0.0290735 seconds
Rank 17: Matrix-vector product took 0.0287147 seconds
Rank 18: Matrix-vector product took 0.0301247 seconds
Rank 19: Matrix-vector product took 0.0303032 seconds
Rank 20: Matrix-vector product took 0.0299634 seconds
Rank 21: Matrix-vector product took 0.0296599 seconds
Rank 22: Matrix-vector product took 0.0283429 seconds
Rank 23: Matrix-vector product took 0.0278983 seconds
Rank 24: Matrix-vector product took 0.0278822 seconds
Rank 25: Matrix-vector product took 0.0287191 seconds
Rank 26: Matrix-vector product took 0.0292255 seconds
Rank 27: Matrix-vector product took 0.0285776 seconds
Rank 28: Matrix-vector product took 0.0297059 seconds
Rank 29: Matrix-vector product took 0.030158 seconds
Rank 30: Matrix-vector product took 0.0296595 seconds
Rank 31: Matrix-vector product took 0.0290877 seconds
Average time for Matrix-vector product is 0.0292195 seconds

Rank 0: copyOwnerToAll took 0.00046513 seconds
Rank 1: copyOwnerToAll took 0.00051865 seconds
Rank 2: copyOwnerToAll took 0.00055187 seconds
Rank 3: copyOwnerToAll took 0.00073213 seconds
Rank 4: copyOwnerToAll took 0.000344 seconds
Rank 5: copyOwnerToAll took 0.00034992 seconds
Rank 6: copyOwnerToAll took 0.00047218 seconds
Rank 7: copyOwnerToAll took 0.00038179 seconds
Rank 8: copyOwnerToAll took 0.00028001 seconds
Rank 9: copyOwnerToAll took 0.00038755 seconds
Rank 10: copyOwnerToAll took 0.00039961 seconds
Rank 11: copyOwnerToAll took 0.00041575 seconds
Rank 12: copyOwnerToAll took 0.00073373 seconds
Rank 13: copyOwnerToAll took 0.00074584 seconds
Rank 14: copyOwnerToAll took 0.00075154 seconds
Rank 15: copyOwnerToAll took 0.00055248 seconds
Rank 16: copyOwnerToAll took 0.000360311 seconds
Rank 17: copyOwnerToAll took 0.000339001 seconds
Rank 18: copyOwnerToAll took 0.000510321 seconds
Rank 19: copyOwnerToAll took 0.000592541 seconds
Rank 20: copyOwnerToAll took 0.000551691 seconds
Rank 21: copyOwnerToAll took 0.000514971 seconds
Rank 22: copyOwnerToAll took 0.000381651 seconds
Rank 23: copyOwnerToAll took 0.000407531 seconds
Rank 24: copyOwnerToAll took 0.000570091 seconds
Rank 25: copyOwnerToAll took 0.000596101 seconds
Rank 26: copyOwnerToAll took 0.000605471 seconds
Rank 27: copyOwnerToAll took 0.000412021 seconds
Rank 28: copyOwnerToAll took 0.000811921 seconds
Rank 29: copyOwnerToAll took 0.000633861 seconds
Rank 30: copyOwnerToAll took 0.000646681 seconds
Rank 31: copyOwnerToAll took 0.000651351 seconds
Average time for copyOwnertoAll is 0.000520866 seconds
Number of cores/ranks per node is: 16
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 31235
Cell on rank 1 before loadbalancing: 31253
Cell on rank 2 before loadbalancing: 31225
Cell on rank 3 before loadbalancing: 31265
Cell on rank 4 before loadbalancing: 31254
Cell on rank 5 before loadbalancing: 31252
Cell on rank 6 before loadbalancing: 31246
Cell on rank 7 before loadbalancing: 31254
Cell on rank 8 before loadbalancing: 31243
Cell on rank 9 before loadbalancing: 31252
Cell on rank 10 before loadbalancing: 31249
Cell on rank 11 before loadbalancing: 31253
Cell on rank 12 before loadbalancing: 31251
Cell on rank 13 before loadbalancing: 31253
Cell on rank 14 before loadbalancing: 31266
Cell on rank 15 before loadbalancing: 31244
Cell on rank 16 before loadbalancing: 31259
Cell on rank 17 before loadbalancing: 31258
Cell on rank 18 before loadbalancing: 31254
Cell on rank 19 before loadbalancing: 31253
Cell on rank 20 before loadbalancing: 31251
Cell on rank 21 before loadbalancing: 31248
Cell on rank 22 before loadbalancing: 31243
Cell on rank 23 before loadbalancing: 31243
Cell on rank 24 before loadbalancing: 31243
Cell on rank 25 before loadbalancing: 31248
Cell on rank 26 before loadbalancing: 31237
Cell on rank 27 before loadbalancing: 31252
Cell on rank 28 before loadbalancing: 31243
Cell on rank 29 before loadbalancing: 31244
Cell on rank 30 before loadbalancing: 31256
Cell on rank 31 before loadbalancing: 31273
Edge-cut: 88461
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
From rank 3 to: 	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
From rank 4 to: 	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
From rank 13 to: 	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
From rank 18 to: 	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
From rank 19 to: 	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
From rank 20 to: 	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
From rank 28 to: 	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
From rank 29 to: 	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	

Edge-cut for node partition: 10625

loadb
After loadbalancing process 23 has 34387 cells.
After loadbalancing process 8 has 34244 cells.
After loadbalancing process 17 has 35264 cells.
After loadbalancing process 22 has 34778 cells.
After loadbalancing process 16 has 35836 cells.
After loadbalancing process 27 has 35111 cells.
After loadbalancing process 24 has 34091 cells.
After loadbalancing process 4 has 34875 cells.
After loadbalancing process 10 has 36748 cells.
After loadbalancing process 18 has 36516 cells.
After loadbalancing process 11 has 35522 cells.
After loadbalancing process 30 has 36207 cells.
After loadbalancing process 9 has 36514 cells.
After loadbalancing process 21 has 35883 cells.
After loadbalancing process 20 has 36699 cells.
After loadbalancing process 5 has 34508 cells.
After loadbalancing process 7 has 35703 cells.
After loadbalancing process 25 has 35306 cells.
After loadbalancing process 14 has 35764 cells.
After loadbalancing process 6 has 35494 cells.
After loadbalancing process 12 has 36860 cells.
After loadbalancing process 19 has 37123 cells.
After loadbalancing process 29 has 37047 cells.
After loadbalancing process 3 has 36762 cells.
After loadbalancing process 0 has 35550 cells.
After loadbalancing process 31 has 35632 cells.
After loadbalancing process 26 has 35803 cells.
After loadbalancing process 28 has 36787 cells.
After loadbalancing process 2 has 36770 cells.
After loadbalancing process 13 has 36803 cells.
After loadbalancing process 15 has 35510 cells.
After loadbalancing process 1 has 36077 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
Rank 3's ghost cells:	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
Rank 4's ghost cells:	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
Rank 13's ghost cells:	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
Rank 18's ghost cells:	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
Rank 19's ghost cells:	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
Rank 20's ghost cells:	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
Rank 28's ghost cells:	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
Rank 29's ghost cells:	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.9384         0.223933
    2          32.6586         0.583831
    3           23.167         0.709368
    4          19.9002         0.858992
    5          17.8022         0.894575
    6          14.3405         0.805546
    7          12.4425         0.867647
    8          11.9818         0.962969
    9          10.0942          0.84246
   10          8.90709         0.882401
   11          8.83377         0.991768
   12          7.84531         0.888104
   13          6.87091         0.875799
   14          6.92244           1.0075
   15          6.45689         0.932747
   16          5.73027         0.887466
   17          5.66022         0.987776
   18          5.37372         0.949384
   19          4.84508         0.901626
   20          4.80741         0.992225
   21          4.56438         0.949446
   22          4.15488         0.910285
   23          4.14187         0.996869
   24          3.90939          0.94387
   25          3.56422         0.911708
   26          3.52144         0.987996
   27          3.32898         0.945347
   28          3.04565         0.914891
   29          3.03015         0.994909
   30          2.92174         0.964224
   31          2.80743         0.960876
   32          2.89603          1.03156
   33          2.98469          1.03062
   34          3.04402          1.01988
   35           3.1376          1.03074
   36          2.85616         0.910302
   37          2.32291         0.813296
   38           1.8459         0.794651
   39          1.43591         0.777892
   40          1.20325         0.837966
   41          1.11945         0.930355
   42          1.00095         0.894149
   43         0.872088          0.87126
   44         0.759537         0.870941
   45         0.642011         0.845267
   46         0.544124          0.84753
   47         0.464557          0.85377
   48         0.386473         0.831918
   49          0.31789         0.822541
   50         0.263593         0.829195
   51         0.220329         0.835867
   52         0.187773          0.85224
   53         0.161491         0.860035
   54         0.135465         0.838838
   55         0.115667         0.853852
   56        0.0970093         0.838694
   57        0.0778381         0.802378
   58        0.0646127          0.83009
   59        0.0518633         0.802681
   60        0.0412151         0.794688
   61        0.0331721         0.804852
   62        0.0257889         0.777429
   63         0.019849         0.769671
=== rate=0.860841, T=5.37496, TIT=0.0853168, IT=63

 Elapsed time: 5.37496
Rank 0: Matrix-vector product took 0.02905 seconds
Rank 1: Matrix-vector product took 0.0293014 seconds
Rank 2: Matrix-vector product took 0.0300546 seconds
Rank 3: Matrix-vector product took 0.0298468 seconds
Rank 4: Matrix-vector product took 0.0283458 seconds
Rank 5: Matrix-vector product took 0.0279133 seconds
Rank 6: Matrix-vector product took 0.0284806 seconds
Rank 7: Matrix-vector product took 0.0292408 seconds
Rank 8: Matrix-vector product took 0.0277737 seconds
Rank 9: Matrix-vector product took 0.0293136 seconds
Rank 10: Matrix-vector product took 0.0298584 seconds
Rank 11: Matrix-vector product took 0.0286382 seconds
Rank 12: Matrix-vector product took 0.0299696 seconds
Rank 13: Matrix-vector product took 0.0296017 seconds
Rank 14: Matrix-vector product took 0.0322157 seconds
Rank 15: Matrix-vector product took 0.0289263 seconds
Rank 16: Matrix-vector product took 0.0290459 seconds
Rank 17: Matrix-vector product took 0.0286645 seconds
Rank 18: Matrix-vector product took 0.0296641 seconds
Rank 19: Matrix-vector product took 0.0300387 seconds
Rank 20: Matrix-vector product took 0.0325904 seconds
Rank 21: Matrix-vector product took 0.0288657 seconds
Rank 22: Matrix-vector product took 0.0283029 seconds
Rank 23: Matrix-vector product took 0.0280101 seconds
Rank 24: Matrix-vector product took 0.0313549 seconds
Rank 25: Matrix-vector product took 0.0286444 seconds
Rank 26: Matrix-vector product took 0.029057 seconds
Rank 27: Matrix-vector product took 0.0280912 seconds
Rank 28: Matrix-vector product took 0.0299102 seconds
Rank 29: Matrix-vector product took 0.0302536 seconds
Rank 30: Matrix-vector product took 0.0293573 seconds
Rank 31: Matrix-vector product took 0.0289514 seconds
Average time for Matrix-vector product is 0.0293542 seconds

Rank 0: copyOwnerToAll took 0.00048915 seconds
Rank 1: copyOwnerToAll took 0.00053918 seconds
Rank 2: copyOwnerToAll took 0.00057047 seconds
Rank 3: copyOwnerToAll took 0.00060862 seconds
Rank 4: copyOwnerToAll took 0.00036812 seconds
Rank 5: copyOwnerToAll took 0.00037665 seconds
Rank 6: copyOwnerToAll took 0.00037928 seconds
Rank 7: copyOwnerToAll took 0.00041402 seconds
Rank 8: copyOwnerToAll took 0.00027205 seconds
Rank 9: copyOwnerToAll took 0.00038884 seconds
Rank 10: copyOwnerToAll took 0.00041903 seconds
Rank 11: copyOwnerToAll took 0.00057318 seconds
Rank 12: copyOwnerToAll took 0.00052625 seconds
Rank 13: copyOwnerToAll took 0.00055975 seconds
Rank 14: copyOwnerToAll took 0.00055605 seconds
Rank 15: copyOwnerToAll took 0.0005496 seconds
Rank 16: copyOwnerToAll took 0.00035992 seconds
Rank 17: copyOwnerToAll took 0.00034007 seconds
Rank 18: copyOwnerToAll took 0.00048077 seconds
Rank 19: copyOwnerToAll took 0.00059789 seconds
Rank 20: copyOwnerToAll took 0.00058199 seconds
Rank 21: copyOwnerToAll took 0.00052009 seconds
Rank 22: copyOwnerToAll took 0.00040599 seconds
Rank 23: copyOwnerToAll took 0.00039361 seconds
Rank 24: copyOwnerToAll took 0.00033564 seconds
Rank 25: copyOwnerToAll took 0.00037781 seconds
Rank 26: copyOwnerToAll took 0.00049021 seconds
Rank 27: copyOwnerToAll took 0.00053017 seconds
Rank 28: copyOwnerToAll took 0.00061238 seconds
Rank 29: copyOwnerToAll took 0.00060544 seconds
Rank 30: copyOwnerToAll took 0.00045506 seconds
Rank 31: copyOwnerToAll took 0.00046115 seconds
Average time for copyOwnertoAll is 0.000473076 seconds
