Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	490	0	470	0	0	0	260	
From rank 1 to: 	490	0	0	0	0	0	0	260	
From rank 2 to: 	0	0	0	500	250	0	0	0	
From rank 3 to: 	470	0	500	0	0	210	0	60	
From rank 4 to: 	0	0	250	0	0	500	0	0	
From rank 5 to: 	0	0	0	210	500	0	190	330	
From rank 6 to: 	0	0	0	0	0	190	0	490	
From rank 7 to: 	260	260	0	60	0	340	480	0	
loadb
After loadbalancing process 4 has 13250 cells.
After loadbalancing process 6 has 13280 cells.
After loadbalancing process 2 has 13268 cells.
After loadbalancing process 1 has 13230 cells.
After loadbalancing process 5 has 13730 cells.
After loadbalancing process 0 has 13710 cells.
After loadbalancing process 7 has 14000 cells.
After loadbalancing process 3 has 13552 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	490	0	470	0	0	0	260	
Rank 1's ghost cells:	490	0	0	0	0	0	0	260	
Rank 2's ghost cells:	0	0	0	500	250	0	0	0	
Rank 3's ghost cells:	470	0	500	0	0	210	0	60	
Rank 4's ghost cells:	0	0	250	0	0	500	0	0	
Rank 5's ghost cells:	0	0	0	210	500	0	190	330	
Rank 6's ghost cells:	0	0	0	0	0	190	0	490	
Rank 7's ghost cells:	260	260	0	60	0	340	480	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          34.6499         0.216224
    2          20.8835         0.602699
    3          19.8922         0.952533
    4          13.2091         0.664033
    5          6.44425         0.487865
    6          4.40435         0.683454
    7          2.51401         0.570802
    8           1.6521         0.657155
    9          1.08546         0.657018
   10         0.637538         0.587346
   11         0.508636         0.797813
   12         0.235426         0.462857
   13         0.166593         0.707623
   14         0.107898         0.647672
   15        0.0602376         0.558285
   16        0.0425449         0.706286
   17        0.0280253         0.658722
   18        0.0162784         0.580846
   19        0.0115844         0.711646
=== rate=0.60542, T=0.600803, TIT=0.0316212, IT=19

 Elapsed time: 0.600803
Rank 0: Matrix-vector product took 0.0111199 seconds
Rank 1: Matrix-vector product took 0.0106572 seconds
Rank 2: Matrix-vector product took 0.0106393 seconds
Rank 3: Matrix-vector product took 0.0108665 seconds
Rank 4: Matrix-vector product took 0.0107494 seconds
Rank 5: Matrix-vector product took 0.0110333 seconds
Rank 6: Matrix-vector product took 0.0106825 seconds
Rank 7: Matrix-vector product took 0.0111074 seconds
Average time for Matrix-vector product is 0.0108569 seconds

Rank 0: copyOwnerToAll took 0.00017555 seconds
Rank 1: copyOwnerToAll took 0.00017276 seconds
Rank 2: copyOwnerToAll took 0.00010531 seconds
Rank 3: copyOwnerToAll took 0.00019044 seconds
Rank 4: copyOwnerToAll took 0.00011624 seconds
Rank 5: copyOwnerToAll took 0.00024271 seconds
Rank 6: copyOwnerToAll took 0.000155181 seconds
Rank 7: copyOwnerToAll took 0.000225421 seconds
Average time for copyOwnertoAll is 0.000172951 seconds
