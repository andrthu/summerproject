Number of cores/ranks per node is: 16
METIS partitioner
Cell on rank 0 before loadbalancing: 31235
Cell on rank 1 before loadbalancing: 31253
Cell on rank 2 before loadbalancing: 31225
Cell on rank 3 before loadbalancing: 31265
Cell on rank 4 before loadbalancing: 31254
Cell on rank 5 before loadbalancing: 31252
Cell on rank 6 before loadbalancing: 31246
Cell on rank 7 before loadbalancing: 31254
Cell on rank 8 before loadbalancing: 31243
Cell on rank 9 before loadbalancing: 31252
Cell on rank 10 before loadbalancing: 31249
Cell on rank 11 before loadbalancing: 31253
Cell on rank 12 before loadbalancing: 31251
Cell on rank 13 before loadbalancing: 31253
Cell on rank 14 before loadbalancing: 31266
Cell on rank 15 before loadbalancing: 31244
Cell on rank 16 before loadbalancing: 31259
Cell on rank 17 before loadbalancing: 31258
Cell on rank 18 before loadbalancing: 31254
Cell on rank 19 before loadbalancing: 31253
Cell on rank 20 before loadbalancing: 31251
Cell on rank 21 before loadbalancing: 31248
Cell on rank 22 before loadbalancing: 31243
Cell on rank 23 before loadbalancing: 31243
Cell on rank 24 before loadbalancing: 31243
Cell on rank 25 before loadbalancing: 31248
Cell on rank 26 before loadbalancing: 31237
Cell on rank 27 before loadbalancing: 31252
Cell on rank 28 before loadbalancing: 31243
Cell on rank 29 before loadbalancing: 31244
Cell on rank 30 before loadbalancing: 31256
Cell on rank 31 before loadbalancing: 31273
Edge-cut: 88461
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
From rank 3 to: 	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
From rank 4 to: 	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
From rank 13 to: 	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
From rank 18 to: 	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
From rank 19 to: 	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
From rank 20 to: 	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
From rank 28 to: 	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
From rank 29 to: 	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
loadb
After loadbalancing process 23 has 34387 cells.
After loadbalancing process 8 has 34244 cells.
After loadbalancing process 24 has 34091 cells.
After loadbalancing process 22 has 34778 cells.
After loadbalancing process 17 has 35264 cells.
After loadbalancing process 21 has 35883 cells.
After loadbalancing process 5 has 34508 cells.
After loadbalancing process 10 has 36748 cells.
After loadbalancing process 4 has 34875 cells.
After loadbalancing process 11 has 35522 cells.
After loadbalancing process 16 has 35836 cells.
After loadbalancing process 9 has 36514 cells.
After loadbalancing process 25 has 35306 cells.
After loadbalancing process 26 has 35803 cells.
After loadbalancing process 31 has 35632 cells.
After loadbalancing process 19 has 37123 cells.
After loadbalancing process 1 has 36077 cells.
After loadbalancing process 6 has 35494 cells.
After loadbalancing process 0 has 35550 cells.
After loadbalancing process 12 has 36860 cells.
After loadbalancing process 18 has 36516 cells.
After loadbalancing process 30 has 36207 cells.
After loadbalancing process 15 has 35510 cells.
After loadbalancing process 14 has 35764 cells.
After loadbalancing process 28 has 36787 cells.
After loadbalancing process 27 has 35111 cells.
After loadbalancing process 29 has 37047 cells.
After loadbalancing process 2 has 36770 cells.
After loadbalancing process 7 has 35703 cells.
After loadbalancing process 13 has 36803 cells.
After loadbalancing process 3 has 36762 cells.
After loadbalancing process 20 has 36699 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
Rank 3's ghost cells:	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
Rank 4's ghost cells:	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
Rank 13's ghost cells:	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
Rank 18's ghost cells:	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
Rank 19's ghost cells:	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
Rank 20's ghost cells:	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
Rank 28's ghost cells:	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
Rank 29's ghost cells:	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.9384         0.223933
    2          32.6586         0.583831
    3           23.167         0.709368
    4          19.9002         0.858992
    5          17.8022         0.894575
    6          14.3405         0.805546
    7          12.4425         0.867647
    8          11.9818         0.962969
    9          10.0942          0.84246
   10          8.90709         0.882401
   11          8.83377         0.991768
   12          7.84531         0.888104
   13          6.87091         0.875799
   14          6.92244           1.0075
   15          6.45689         0.932747
   16          5.73027         0.887466
   17          5.66022         0.987776
   18          5.37372         0.949384
   19          4.84508         0.901626
   20          4.80741         0.992225
   21          4.56438         0.949446
   22          4.15488         0.910285
   23          4.14187         0.996869
   24          3.90939          0.94387
   25          3.56422         0.911708
   26          3.52144         0.987996
   27          3.32898         0.945347
   28          3.04565         0.914891
   29          3.03015         0.994909
   30          2.92174         0.964224
   31          2.80743         0.960876
   32          2.89603          1.03156
   33          2.98469          1.03062
   34          3.04402          1.01988
   35           3.1376          1.03074
   36          2.85616         0.910302
   37          2.32291         0.813296
   38           1.8459         0.794651
   39          1.43591         0.777892
   40          1.20325         0.837966
   41          1.11945         0.930355
   42          1.00095         0.894149
   43         0.872088          0.87126
   44         0.759537         0.870941
   45         0.642011         0.845267
   46         0.544124          0.84753
   47         0.464557          0.85377
   48         0.386473         0.831918
   49          0.31789         0.822541
   50         0.263593         0.829195
   51         0.220329         0.835867
   52         0.187773          0.85224
   53         0.161491         0.860035
   54         0.135465         0.838838
   55         0.115667         0.853852
   56        0.0970093         0.838694
   57        0.0778381         0.802378
   58        0.0646127          0.83009
   59        0.0518633         0.802681
   60        0.0412151         0.794688
   61        0.0331721         0.804852
   62        0.0257889         0.777429
   63         0.019849         0.769671
=== rate=0.860841, T=5.39971, TIT=0.0857097, IT=63

 Elapsed time: 5.39971
Rank 0: Matrix-vector product took 0.0289938 seconds
Rank 1: Matrix-vector product took 0.0294534 seconds
Rank 2: Matrix-vector product took 0.0298261 seconds
Rank 3: Matrix-vector product took 0.0300512 seconds
Rank 4: Matrix-vector product took 0.0282982 seconds
Rank 5: Matrix-vector product took 0.0281988 seconds
Rank 6: Matrix-vector product took 0.0288599 seconds
Rank 7: Matrix-vector product took 0.0289749 seconds
Rank 8: Matrix-vector product took 0.0278946 seconds
Rank 9: Matrix-vector product took 0.0292202 seconds
Rank 10: Matrix-vector product took 0.0299082 seconds
Rank 11: Matrix-vector product took 0.0288363 seconds
Rank 12: Matrix-vector product took 0.030247 seconds
Rank 13: Matrix-vector product took 0.0300265 seconds
Rank 14: Matrix-vector product took 0.0291898 seconds
Rank 15: Matrix-vector product took 0.0288808 seconds
Rank 16: Matrix-vector product took 0.0290814 seconds
Rank 17: Matrix-vector product took 0.0292535 seconds
Rank 18: Matrix-vector product took 0.0295019 seconds
Rank 19: Matrix-vector product took 0.0302655 seconds
Rank 20: Matrix-vector product took 0.0296075 seconds
Rank 21: Matrix-vector product took 0.0293531 seconds
Rank 22: Matrix-vector product took 0.0282398 seconds
Rank 23: Matrix-vector product took 0.0280137 seconds
Rank 24: Matrix-vector product took 0.027811 seconds
Rank 25: Matrix-vector product took 0.0286457 seconds
Rank 26: Matrix-vector product took 0.0290772 seconds
Rank 27: Matrix-vector product took 0.0284144 seconds
Rank 28: Matrix-vector product took 0.0300065 seconds
Rank 29: Matrix-vector product took 0.030164 seconds
Rank 30: Matrix-vector product took 0.0294304 seconds
Rank 31: Matrix-vector product took 0.0289652 seconds
Average time for Matrix-vector product is 0.0291466 seconds

Rank 0: copyOwnerToAll took 0.000463799 seconds
Rank 1: copyOwnerToAll took 0.000566199 seconds
Rank 2: copyOwnerToAll took 0.000570789 seconds
Rank 3: copyOwnerToAll took 0.000574709 seconds
Rank 4: copyOwnerToAll took 0.000358579 seconds
Rank 5: copyOwnerToAll took 0.000380469 seconds
Rank 6: copyOwnerToAll took 0.000390259 seconds
Rank 7: copyOwnerToAll took 0.000406149 seconds
Rank 8: copyOwnerToAll took 0.000280709 seconds
Rank 9: copyOwnerToAll took 0.000482289 seconds
Rank 10: copyOwnerToAll took 0.000407639 seconds
Rank 11: copyOwnerToAll took 0.000411789 seconds
Rank 12: copyOwnerToAll took 0.000568659 seconds
Rank 13: copyOwnerToAll took 0.000585349 seconds
Rank 14: copyOwnerToAll took 0.000532199 seconds
Rank 15: copyOwnerToAll took 0.000540979 seconds
Rank 16: copyOwnerToAll took 0.00035389 seconds
Rank 17: copyOwnerToAll took 0.00035463 seconds
Rank 18: copyOwnerToAll took 0.00057048 seconds
Rank 19: copyOwnerToAll took 0.00063453 seconds
Rank 20: copyOwnerToAll took 0.00053011 seconds
Rank 21: copyOwnerToAll took 0.00055492 seconds
Rank 22: copyOwnerToAll took 0.00042268 seconds
Rank 23: copyOwnerToAll took 0.00037302 seconds
Rank 24: copyOwnerToAll took 0.00033748 seconds
Rank 25: copyOwnerToAll took 0.00033843 seconds
Rank 26: copyOwnerToAll took 0.00045768 seconds
Rank 27: copyOwnerToAll took 0.00039276 seconds
Rank 28: copyOwnerToAll took 0.00059727 seconds
Rank 29: copyOwnerToAll took 0.00059734 seconds
Rank 30: copyOwnerToAll took 0.00048598 seconds
Rank 31: copyOwnerToAll took 0.00049285 seconds
Average time for copyOwnertoAll is 0.000469207 seconds
Number of cores/ranks per node is: 16
METIS partitioner
Cell on rank 0 before loadbalancing: 31235
Cell on rank 1 before loadbalancing: 31253
Cell on rank 2 before loadbalancing: 31225
Cell on rank 3 before loadbalancing: 31265
Cell on rank 4 before loadbalancing: 31254
Cell on rank 5 before loadbalancing: 31252
Cell on rank 6 before loadbalancing: 31246
Cell on rank 7 before loadbalancing: 31254
Cell on rank 8 before loadbalancing: 31243
Cell on rank 9 before loadbalancing: 31252
Cell on rank 10 before loadbalancing: 31249
Cell on rank 11 before loadbalancing: 31253
Cell on rank 12 before loadbalancing: 31251
Cell on rank 13 before loadbalancing: 31253
Cell on rank 14 before loadbalancing: 31266
Cell on rank 15 before loadbalancing: 31244
Cell on rank 16 before loadbalancing: 31259
Cell on rank 17 before loadbalancing: 31258
Cell on rank 18 before loadbalancing: 31254
Cell on rank 19 before loadbalancing: 31253
Cell on rank 20 before loadbalancing: 31251
Cell on rank 21 before loadbalancing: 31248
Cell on rank 22 before loadbalancing: 31243
Cell on rank 23 before loadbalancing: 31243
Cell on rank 24 before loadbalancing: 31243
Cell on rank 25 before loadbalancing: 31248
Cell on rank 26 before loadbalancing: 31237
Cell on rank 27 before loadbalancing: 31252
Cell on rank 28 before loadbalancing: 31243
Cell on rank 29 before loadbalancing: 31244
Cell on rank 30 before loadbalancing: 31256
Cell on rank 31 before loadbalancing: 31273
Edge-cut: 88461
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
From rank 3 to: 	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
From rank 4 to: 	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
From rank 13 to: 	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
From rank 18 to: 	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
From rank 19 to: 	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
From rank 20 to: 	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
From rank 28 to: 	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
From rank 29 to: 	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
loadb
After loadbalancing process 23 has 34387 cells.
After loadbalancing process 24 has 34091 cells.
After loadbalancing process 8 has 34244 cells.
After loadbalancing process 9 has 36514 cells.
After loadbalancing process 5 has 34508 cells.
After loadbalancing process 4 has 34875 cells.
After loadbalancing process 7 has 35703 cells.
After loadbalancing process 22 has 34778 cells.
After loadbalancing process 25 has 35306 cells.
After loadbalancing process 17 has 35264 cells.
After loadbalancing process 26 has 35803 cells.
After loadbalancing process 0 has 35550 cells.
After loadbalancing process 27 has 35111 cells.
After loadbalancing process 16 has 35836 cells.
After loadbalancing process 31 has 35632 cells.
After loadbalancing process 18 has 36516 cells.
After loadbalancing process 28 has 36787 cells.
After loadbalancing process 14 has 35764 cells.
After loadbalancing process 10 has 36748 cells.
After loadbalancing process 6 has 35494 cells.
After loadbalancing process 30 has 36207 cells.
After loadbalancing process 3 has 36762 cells.
After loadbalancing process 15 has 35510 cells.
After loadbalancing process 13 has 36803 cells.
After loadbalancing process 21 has 35883 cells.
After loadbalancing process 12 has 36860 cells.
After loadbalancing process 2 has 36770 cells.
After loadbalancing process 29 has 37047 cells.
After loadbalancing process 20 has 36699 cells.
After loadbalancing process 1 has 36077 cells.
After loadbalancing process 19 has 37123 cells.
After loadbalancing process 11 has 35522 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	677	40	964	1266	0	0	146	0	0	0	0	0	0	0	0	0	0	0	0	501	721	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	672	0	1145	199	321	945	1	0	0	0	0	0	0	0	0	0	0	0	0	15	1488	38	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	39	1146	0	436	108	107	1045	148	0	0	0	0	0	1138	0	0	0	0	724	206	244	0	0	0	0	0	0	0	0	204	0	0	
Rank 3's ghost cells:	957	199	436	0	15	0	0	951	0	0	148	0	1182	154	0	0	0	0	0	954	0	175	0	0	0	0	0	0	300	26	0	0	
Rank 4's ghost cells:	1263	325	111	15	0	754	65	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	951	102	0	765	0	1283	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	1	1052	0	63	1293	0	575	0	0	418	709	0	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	154	0	144	953	1092	150	578	0	0	0	1237	59	58	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1422	0	947	0	0	0	632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	1434	0	900	548	408	198	1052	722	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	152	0	0	418	1258	0	906	0	1230	1137	398	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	716	59	947	535	1201	0	56	755	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	1179	0	0	0	58	0	402	1149	61	0	531	1293	31	0	0	0	0	0	0	0	0	0	0	0	0	905	0	0	0	
Rank 13's ghost cells:	0	0	1137	154	0	0	161	18	0	188	387	755	521	0	69	923	0	0	0	1	0	0	0	0	0	0	0	0	53	1183	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	1065	0	0	1294	73	0	599	0	0	0	0	0	0	0	0	0	0	855	0	544	68	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	624	697	0	0	35	923	595	0	0	0	0	0	0	0	0	0	0	0	460	708	0	224	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	1262	332	0	0	840	119	0	0	0	0	0	0	1033	303	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	688	0	69	803	0	240	37	1262	0	0	0	0	0	0	0	907	
Rank 18's ghost cells:	0	0	724	0	0	0	0	0	0	0	0	0	0	0	0	0	1282	69	0	558	736	70	689	0	0	0	0	0	0	677	457	0	
Rank 19's ghost cells:	0	15	224	954	0	0	0	0	0	0	0	0	0	1	0	0	332	802	551	0	235	1062	0	0	0	0	0	0	808	362	150	374	
Rank 20's ghost cells:	536	1487	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	739	235	0	1084	1087	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	721	32	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	231	73	1072	1059	0	194	1063	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	849	38	684	0	1087	178	0	699	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	1262	0	0	0	1071	699	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	713	787	0	160	0	28	1160	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	0	338	1425	131	151	1187	115	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	851	456	0	0	0	0	0	0	0	0	781	344	0	642	1107	332	0	53	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	708	0	0	0	0	0	0	0	0	0	1446	642	0	0	863	200	0	
Rank 28's ghost cells:	0	0	0	319	0	0	0	0	0	0	0	0	905	53	509	0	0	0	0	808	0	0	0	0	160	127	1123	0	0	548	120	872	
Rank 29's ghost cells:	0	0	191	22	0	0	0	0	0	0	0	0	0	1183	61	232	0	0	677	356	0	0	0	0	0	151	332	879	554	0	1165	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1011	0	463	158	0	0	0	0	28	1181	0	200	123	1179	0	608	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	293	907	0	363	0	0	0	0	1148	107	53	0	883	0	605	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.9384         0.223933
    2          32.6586         0.583831
    3           23.167         0.709368
    4          19.9002         0.858992
    5          17.8022         0.894575
    6          14.3405         0.805546
    7          12.4425         0.867647
    8          11.9818         0.962969
    9          10.0942          0.84246
   10          8.90709         0.882401
   11          8.83377         0.991768
   12          7.84531         0.888104
   13          6.87091         0.875799
   14          6.92244           1.0075
   15          6.45689         0.932747
   16          5.73027         0.887466
   17          5.66022         0.987776
   18          5.37372         0.949384
   19          4.84508         0.901626
   20          4.80741         0.992225
   21          4.56438         0.949446
   22          4.15488         0.910285
   23          4.14187         0.996869
   24          3.90939          0.94387
   25          3.56422         0.911708
   26          3.52144         0.987996
   27          3.32898         0.945347
   28          3.04565         0.914891
   29          3.03015         0.994909
   30          2.92174         0.964224
   31          2.80743         0.960876
   32          2.89603          1.03156
   33          2.98469          1.03062
   34          3.04402          1.01988
   35           3.1376          1.03074
   36          2.85616         0.910302
   37          2.32291         0.813296
   38           1.8459         0.794651
   39          1.43591         0.777892
   40          1.20325         0.837966
   41          1.11945         0.930355
   42          1.00095         0.894149
   43         0.872088          0.87126
   44         0.759537         0.870941
   45         0.642011         0.845267
   46         0.544124          0.84753
   47         0.464557          0.85377
   48         0.386473         0.831918
   49          0.31789         0.822541
   50         0.263593         0.829195
   51         0.220329         0.835867
   52         0.187773          0.85224
   53         0.161491         0.860035
   54         0.135465         0.838838
   55         0.115667         0.853852
   56        0.0970093         0.838694
   57        0.0778381         0.802378
   58        0.0646127          0.83009
   59        0.0518633         0.802681
   60        0.0412151         0.794688
   61        0.0331721         0.804852
   62        0.0257889         0.777429
   63         0.019849         0.769671
=== rate=0.860841, T=5.41517, TIT=0.0859551, IT=63

 Elapsed time: 5.41517
Rank 0: Matrix-vector product took 0.0290998 seconds
Rank 1: Matrix-vector product took 0.0292795 seconds
Rank 2: Matrix-vector product took 0.0298443 seconds
Rank 3: Matrix-vector product took 0.0295092 seconds
Rank 4: Matrix-vector product took 0.0284096 seconds
Rank 5: Matrix-vector product took 0.0282636 seconds
Rank 6: Matrix-vector product took 0.0290995 seconds
Rank 7: Matrix-vector product took 0.0290785 seconds
Rank 8: Matrix-vector product took 0.0278315 seconds
Rank 9: Matrix-vector product took 0.0294581 seconds
Rank 10: Matrix-vector product took 0.0299007 seconds
Rank 11: Matrix-vector product took 0.0289366 seconds
Rank 12: Matrix-vector product took 0.0299483 seconds
Rank 13: Matrix-vector product took 0.0299408 seconds
Rank 14: Matrix-vector product took 0.0291877 seconds
Rank 15: Matrix-vector product took 0.0290621 seconds
Rank 16: Matrix-vector product took 0.0294349 seconds
Rank 17: Matrix-vector product took 0.0289507 seconds
Rank 18: Matrix-vector product took 0.0297242 seconds
Rank 19: Matrix-vector product took 0.0301879 seconds
Rank 20: Matrix-vector product took 0.0298042 seconds
Rank 21: Matrix-vector product took 0.0290747 seconds
Rank 22: Matrix-vector product took 0.0282886 seconds
Rank 23: Matrix-vector product took 0.028123 seconds
Rank 24: Matrix-vector product took 0.0278049 seconds
Rank 25: Matrix-vector product took 0.0282815 seconds
Rank 26: Matrix-vector product took 0.0290634 seconds
Rank 27: Matrix-vector product took 0.028779 seconds
Rank 28: Matrix-vector product took 0.0299599 seconds
Rank 29: Matrix-vector product took 0.0298501 seconds
Rank 30: Matrix-vector product took 0.0296852 seconds
Rank 31: Matrix-vector product took 0.0293204 seconds
Average time for Matrix-vector product is 0.029162 seconds

Rank 0: copyOwnerToAll took 0.00045373 seconds
Rank 1: copyOwnerToAll took 0.0005451 seconds
Rank 2: copyOwnerToAll took 0.00056355 seconds
Rank 3: copyOwnerToAll took 0.00065862 seconds
Rank 4: copyOwnerToAll took 0.00040298 seconds
Rank 5: copyOwnerToAll took 0.00034946 seconds
Rank 6: copyOwnerToAll took 0.00047301 seconds
Rank 7: copyOwnerToAll took 0.00041479 seconds
Rank 8: copyOwnerToAll took 0.00032831 seconds
Rank 9: copyOwnerToAll took 0.00038195 seconds
Rank 10: copyOwnerToAll took 0.00041762 seconds
Rank 11: copyOwnerToAll took 0.00039973 seconds
Rank 12: copyOwnerToAll took 0.00049934 seconds
Rank 13: copyOwnerToAll took 0.00058925 seconds
Rank 14: copyOwnerToAll took 0.00059333 seconds
Rank 15: copyOwnerToAll took 0.00059855 seconds
Rank 16: copyOwnerToAll took 0.00044648 seconds
Rank 17: copyOwnerToAll took 0.00035884 seconds
Rank 18: copyOwnerToAll took 0.00051851 seconds
Rank 19: copyOwnerToAll took 0.00060292 seconds
Rank 20: copyOwnerToAll took 0.00055264 seconds
Rank 21: copyOwnerToAll took 0.00054672 seconds
Rank 22: copyOwnerToAll took 0.0003703 seconds
Rank 23: copyOwnerToAll took 0.00030926 seconds
Rank 24: copyOwnerToAll took 0.00030523 seconds
Rank 25: copyOwnerToAll took 0.00042896 seconds
Rank 26: copyOwnerToAll took 0.0004781 seconds
Rank 27: copyOwnerToAll took 0.00044476 seconds
Rank 28: copyOwnerToAll took 0.00058734 seconds
Rank 29: copyOwnerToAll took 0.00065059 seconds
Rank 30: copyOwnerToAll took 0.00048499 seconds
Rank 31: copyOwnerToAll took 0.00048078 seconds
Average time for copyOwnertoAll is 0.000476117 seconds
