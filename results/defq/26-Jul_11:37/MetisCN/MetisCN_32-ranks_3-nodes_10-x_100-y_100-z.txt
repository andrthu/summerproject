Number of cores/ranks per node is: 10
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 3140
Cell on rank 1 before loadbalancing: 3130
Cell on rank 2 before loadbalancing: 3151
Cell on rank 3 before loadbalancing: 3100
Cell on rank 4 before loadbalancing: 3120
Cell on rank 5 before loadbalancing: 3130
Cell on rank 6 before loadbalancing: 3120
Cell on rank 7 before loadbalancing: 3129
Cell on rank 8 before loadbalancing: 3150
Cell on rank 9 before loadbalancing: 3150
Cell on rank 10 before loadbalancing: 3110
Cell on rank 11 before loadbalancing: 3110
Cell on rank 12 before loadbalancing: 3110
Cell on rank 13 before loadbalancing: 3120
Cell on rank 14 before loadbalancing: 3120
Cell on rank 15 before loadbalancing: 3120
Cell on rank 16 before loadbalancing: 3150
Cell on rank 17 before loadbalancing: 3130
Cell on rank 18 before loadbalancing: 3100
Cell on rank 19 before loadbalancing: 3140
Cell on rank 20 before loadbalancing: 3100
Cell on rank 21 before loadbalancing: 3100
Cell on rank 22 before loadbalancing: 3120
Cell on rank 23 before loadbalancing: 3130
Cell on rank 24 before loadbalancing: 3100
Cell on rank 25 before loadbalancing: 3130
Cell on rank 26 before loadbalancing: 3120
Cell on rank 27 before loadbalancing: 3130
Cell on rank 28 before loadbalancing: 3095
Cell on rank 29 before loadbalancing: 3152
Cell on rank 30 before loadbalancing: 3153
Cell on rank 31 before loadbalancing: 3140
Edge-cut: 11945
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	230	0	90	230	0	0	70	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	230	0	0	140	0	0	0	0	0	0	0	0	0	0	70	170	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	0	0	217	0	0	0	140	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	90	140	217	0	0	0	0	143	0	0	0	90	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	230	0	0	0	0	270	0	100	0	0	0	0	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	260	0	130	40	0	0	0	0	0	0	0	0	30	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	130	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	70	0	140	140	100	40	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	200	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	200	0	70	40	0	200	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	140	80	0	260	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	140	90	0	0	0	0	0	40	250	0	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	190	170	120	0	0	0	0	0	0	0	0	0	50	140	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	200	0	0	190	0	10	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	
From rank 14 to: 	0	70	0	50	0	0	0	0	0	130	0	200	170	10	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	170	0	0	0	0	0	0	0	0	0	0	120	0	120	0	0	60	0	0	0	0	0	80	0	0	140	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	200	80	230	0	40	0	90	0	0	0	0	0	0	0	0	
From rank 17 to: 	150	70	0	0	20	0	0	0	0	0	0	0	0	0	0	60	210	0	0	0	0	0	0	210	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	180	0	290	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	230	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	120	70	0	0	0	0	0	0	0	120	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	290	0	220	0	0	60	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	200	0	0	30	90	0	0	120	110	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	90	200	0	0	70	60	190	0	0	0	110	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	270	0	110	0	230	10	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	50	170	0	0	0	0	0	0	0	0	0	0	270	0	90	60	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	130	0	0	0	0	0	0	40	110	0	90	0	250	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	110	60	250	0	0	0	220	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	40	90	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	0	0	240	0	130	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	20	0	0	220	40	130	0	210	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	110	0	0	0	0	0	90	0	210	0	

Edge-cut for node partition: 2691

loadb
After loadbalancing process 8 has 3752 cells.
After loadbalancing process 7 has 3465 cells.
After loadbalancing process 15 has 3450 cells.
After loadbalancing process 20 has 3640 cells.
After loadbalancing process 22 has 3648 cells.
After loadbalancing process 14 has 3670 cells.
After loadbalancing process 13 has 3840 cells.
After loadbalancing process 24 has 3809 cells.
After loadbalancing process 11 has 3910 cells.
After loadbalancing process 3 has 3720 cells.
After loadbalancing process 9 has 3893 cells.
After loadbalancing process 25 has 3490 cells.
After loadbalancing process 27 has 3590 cells.
After loadbalancing process 4 has 3770 cells.
After loadbalancing process 6 has 3860 cells.
After loadbalancing process 5 has 3880 cells.
After loadbalancing process 1 has 3780 cells.
After loadbalancing process 30 has 3690 cells.
After loadbalancing process 28 has 3830 cells.
After loadbalancing process 31 has 3810 cells.
After loadbalancing process 10 has 3670 cells.
After loadbalancing process 0 has 3630 cells.
After loadbalancing process 2 has 3930 cells.
After loadbalancing process 19 has 3650 cells.
After loadbalancing process 18 has 3850 cells.
After loadbalancing process 23 has 3830 cells.
After loadbalancing process 29 has 3780 cells.
After loadbalancing process 17 has 3910 cells.
After loadbalancing process 16 has 3870 cells.
After loadbalancing process 12 has 3810 cells.
After loadbalancing process 26 has 3780 cells.
After loadbalancing process 21 has 3710 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	120	70	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	220	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	110	0	200	0	0	30	90	0	0	120	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	70	190	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	90	200	0	0	60	0	0	0	0	0	0	0	0	0	80	
Rank 3's ghost cells:	0	0	0	0	270	0	110	0	230	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	270	0	90	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	170	0	
Rank 5's ghost cells:	0	40	110	0	90	0	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	130	
Rank 6's ghost cells:	0	90	0	110	60	250	0	0	0	220	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	0	0	240	40	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	230	0	0	0	240	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	120	0	20	0	0	220	40	130	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	120	110	0	0	0	0	0	90	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	230	230	0	0	0	0	150	0	0	0	0	90	70	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	230	0	0	0	0	70	0	70	0	0	0	0	140	0	0	0	0	0	0	0	170	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	230	0	0	270	0	0	100	20	0	0	0	0	0	100	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	260	0	130	0	30	0	0	80	0	0	0	40	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	50	0	0	130	0	200	170	10	120	
Rank 17's ghost cells:	0	0	90	0	0	0	0	0	0	0	0	0	0	100	20	0	0	0	200	80	230	40	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	210	0	0	0	0	0	0	0	0	150	70	20	0	0	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	60	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	180	290	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	230	0	190	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	220	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	290	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	140	0	0	0	140	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	90	140	0	0	0	50	0	0	0	0	0	217	0	143	0	0	0	90	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	70	0	100	40	190	0	0	0	0	0	0	140	140	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	140	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	200	0	70	40	0	200	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	80	0	260	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	140	90	0	0	40	250	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	50	140	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	190	120	
Rank 30's ghost cells:	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	0	0	0	200	0	0	190	0	0	
Rank 31's ghost cells:	0	0	80	0	0	140	0	0	0	0	0	0	170	0	0	0	120	0	60	0	0	0	0	0	0	0	0	0	0	120	0	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.2595         0.238749
    2          23.2513         0.607726
    3          21.4194         0.921213
    4          19.1822         0.895553
    5          9.52149         0.496371
    6          5.11798         0.537519
    7          3.80244         0.742957
    8          2.31287         0.608259
    9          1.17643         0.508644
   10         0.850506         0.722957
   11         0.611516         0.719003
   12         0.304165         0.497395
   13         0.212237         0.697771
   14         0.142571         0.671753
   15        0.0795842         0.558207
   16        0.0563408         0.707939
   17        0.0356846         0.633372
   18         0.020865         0.584707
   19        0.0140274          0.67229
=== rate=0.611548, T=0.214057, TIT=0.0112661, IT=19

 Elapsed time: 0.214057
Rank 0: Matrix-vector product took 0.00290991 seconds
Rank 1: Matrix-vector product took 0.00297158 seconds
Rank 2: Matrix-vector product took 0.0030671 seconds
Rank 3: Matrix-vector product took 0.00289807 seconds
Rank 4: Matrix-vector product took 0.00293623 seconds
Rank 5: Matrix-vector product took 0.00302959 seconds
Rank 6: Matrix-vector product took 0.00302168 seconds
Rank 7: Matrix-vector product took 0.0026874 seconds
Rank 8: Matrix-vector product took 0.00299549 seconds
Rank 9: Matrix-vector product took 0.00305476 seconds
Rank 10: Matrix-vector product took 0.00292948 seconds
Rank 11: Matrix-vector product took 0.00306757 seconds
Rank 12: Matrix-vector product took 0.00307513 seconds
Rank 13: Matrix-vector product took 0.00304121 seconds
Rank 14: Matrix-vector product took 0.00285733 seconds
Rank 15: Matrix-vector product took 0.0027036 seconds
Rank 16: Matrix-vector product took 0.0030391 seconds
Rank 17: Matrix-vector product took 0.00305591 seconds
Rank 18: Matrix-vector product took 0.00301461 seconds
Rank 19: Matrix-vector product took 0.00283532 seconds
Rank 20: Matrix-vector product took 0.00292221 seconds
Rank 21: Matrix-vector product took 0.00290131 seconds
Rank 22: Matrix-vector product took 0.00283214 seconds
Rank 23: Matrix-vector product took 0.00297826 seconds
Rank 24: Matrix-vector product took 0.00302563 seconds
Rank 25: Matrix-vector product took 0.0054644 seconds
Rank 26: Matrix-vector product took 0.00293437 seconds
Rank 27: Matrix-vector product took 0.00279366 seconds
Rank 28: Matrix-vector product took 0.00296787 seconds
Rank 29: Matrix-vector product took 0.00298328 seconds
Rank 30: Matrix-vector product took 0.00290873 seconds
Rank 31: Matrix-vector product took 0.00298852 seconds
Average time for Matrix-vector product is 0.00302786 seconds

Rank 0: copyOwnerToAll took 0.00020358 seconds
Rank 1: copyOwnerToAll took 0.00020358 seconds
Rank 2: copyOwnerToAll took 0.00020358 seconds
Rank 3: copyOwnerToAll took 0.00020358 seconds
Rank 4: copyOwnerToAll took 0.00020358 seconds
Rank 5: copyOwnerToAll took 0.00020358 seconds
Rank 6: copyOwnerToAll took 0.00020358 seconds
Rank 7: copyOwnerToAll took 0.00020358 seconds
Rank 8: copyOwnerToAll took 0.00020358 seconds
Rank 9: copyOwnerToAll took 0.00020358 seconds
Rank 10: copyOwnerToAll took 0.00020358 seconds
Rank 11: copyOwnerToAll took 0.00020358 seconds
Rank 12: copyOwnerToAll took 0.00020358 seconds
Rank 13: copyOwnerToAll took 0.00020358 seconds
Rank 14: copyOwnerToAll took 0.00020358 seconds
Rank 15: copyOwnerToAll took 0.00020358 seconds
Rank 16: copyOwnerToAll took 0.00020358 seconds
Rank 17: copyOwnerToAll took 0.00020358 seconds
Rank 18: copyOwnerToAll took 0.00020358 seconds
Rank 19: copyOwnerToAll took 0.00020358 seconds
Rank 20: copyOwnerToAll took 0.00020358 seconds
Rank 21: copyOwnerToAll took 0.00020358 seconds
Rank 22: copyOwnerToAll took 0.00020358 seconds
Rank 23: copyOwnerToAll took 0.00020358 seconds
Rank 24: copyOwnerToAll took 0.00020358 seconds
Rank 25: copyOwnerToAll took 0.00020358 seconds
Rank 26: copyOwnerToAll took 0.00020358 seconds
Rank 27: copyOwnerToAll took 0.00020358 seconds
Rank 28: copyOwnerToAll took 0.00020358 seconds
Rank 29: copyOwnerToAll took 0.00020358 seconds
Rank 30: copyOwnerToAll took 0.00020358 seconds
Rank 31: copyOwnerToAll took 0.00020358 seconds
Average time for copyOwnertoAll is 0.000248495 seconds
