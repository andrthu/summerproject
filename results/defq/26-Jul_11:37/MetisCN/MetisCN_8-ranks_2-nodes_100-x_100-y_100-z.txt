Number of cores/ranks per node is: 4
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 125002
Cell on rank 1 before loadbalancing: 124995
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 125000
Cell on rank 4 before loadbalancing: 125002
Cell on rank 5 before loadbalancing: 125004
Cell on rank 6 before loadbalancing: 124995
Cell on rank 7 before loadbalancing: 124998
Edge-cut: 44997
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4686	0	0	0	0	2634	0	
From rank 1 to: 	4686	0	1927	2846	0	659	2063	0	
From rank 2 to: 	0	1931	0	4643	2358	2471	0	0	
From rank 3 to: 	0	2855	4641	0	0	0	0	0	
From rank 4 to: 	0	0	2361	0	0	4601	0	0	
From rank 5 to: 	0	692	2462	0	4598	0	2547	2656	
From rank 6 to: 	2625	2075	0	0	0	2550	0	4739	
From rank 7 to: 	0	0	0	0	0	2690	4738	0	

Edge-cut for node partition: 10200

loadb
After loadbalancing process 7 has 132426 cells.
After loadbalancing process 3 has 132496 cells.
After loadbalancing process 6 has 136984 cells.
After loadbalancing process 0 has 132322 cells.
After loadbalancing process 4 has 131964 cells.
After loadbalancing process 2 has 136407 cells.
After loadbalancing process 5 has 137959 cells.
After loadbalancing process 1 has 137176 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4686	0	0	0	0	2634	0	
Rank 1's ghost cells:	4686	0	1927	2846	0	659	2063	0	
Rank 2's ghost cells:	0	1931	0	4643	2358	2471	0	0	
Rank 3's ghost cells:	0	2855	4641	0	0	0	0	0	
Rank 4's ghost cells:	0	0	2361	0	0	4601	0	0	
Rank 5's ghost cells:	0	692	2462	0	4598	0	2547	2656	
Rank 6's ghost cells:	2625	2075	0	0	0	2550	0	4739	
Rank 7's ghost cells:	0	0	0	0	0	2690	4738	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7694          0.21525
    2          31.0392         0.577265
    3          21.8254         0.703156
    4          18.0322         0.826203
    5          16.6646         0.924157
    6          14.0991         0.846049
    7          11.4744         0.813842
    8          10.8675         0.947112
    9          10.0682         0.926446
   10          8.33076         0.827433
   11          7.82907         0.939778
   12          7.83876          1.00124
   13          6.66097         0.849748
   14          5.99701         0.900321
   15          6.15707          1.02669
   16           5.6756         0.921801
   17          5.02639         0.885615
   18          5.09697          1.01404
   19          4.81509         0.944698
   20          4.30564         0.894197
   21          4.30625          1.00014
   22          4.06637         0.944295
   23          3.71177         0.912798
   24          3.71072         0.999717
   25          3.47652         0.936885
   26          3.14708         0.905239
   27          3.16041          1.00424
   28          2.96968         0.939651
   29          2.73196         0.919949
   30          2.80115          1.02533
   31          2.80685          1.00204
   32           2.8251           1.0065
   33          3.11008          1.10087
   34          3.11887          1.00283
   35          2.65059         0.849856
   36          2.12673          0.80236
   37          1.61891         0.761222
   38            1.302         0.804245
   39          1.18462         0.909842
   40           1.0602         0.894971
   41         0.934567         0.881503
   42         0.812852         0.869763
   43         0.658791         0.810469
   44         0.570956         0.866671
   45         0.500396         0.876419
   46         0.386851         0.773089
   47            0.326         0.842702
   48         0.282079         0.865272
   49          0.22281         0.789886
   50         0.196633         0.882514
   51         0.166235         0.845409
   52         0.135335          0.81412
   53         0.118351         0.874502
   54        0.0962141         0.812955
   55        0.0790824         0.821942
   56        0.0646433         0.817417
   57        0.0522044         0.807577
   58        0.0416891         0.798575
   59        0.0322266         0.773022
   60        0.0254921         0.791026
   61        0.0188471         0.739333
=== rate=0.855895, T=18.7003, TIT=0.306562, IT=61

 Elapsed time: 18.7003
Rank 0: Matrix-vector product took 0.1081 seconds
Rank 1: Matrix-vector product took 0.113338 seconds
Rank 2: Matrix-vector product took 0.112339 seconds
Rank 3: Matrix-vector product took 0.107158 seconds
Rank 4: Matrix-vector product took 0.107532 seconds
Rank 5: Matrix-vector product took 0.111977 seconds
Rank 6: Matrix-vector product took 0.111135 seconds
Rank 7: Matrix-vector product took 0.107136 seconds
Average time for Matrix-vector product is 0.10984 seconds

Rank 0: copyOwnerToAll took 0.000862261 seconds
Rank 1: copyOwnerToAll took 0.000862261 seconds
Rank 2: copyOwnerToAll took 0.000862261 seconds
Rank 3: copyOwnerToAll took 0.000862261 seconds
Rank 4: copyOwnerToAll took 0.000862261 seconds
Rank 5: copyOwnerToAll took 0.000862261 seconds
Rank 6: copyOwnerToAll took 0.000862261 seconds
Rank 7: copyOwnerToAll took 0.000862261 seconds
Average time for copyOwnertoAll is 0.000874308 seconds
Number of cores/ranks per node is: 4
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 125002
Cell on rank 1 before loadbalancing: 124995
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 125000
Cell on rank 4 before loadbalancing: 125002
Cell on rank 5 before loadbalancing: 125004
Cell on rank 6 before loadbalancing: 124995
Cell on rank 7 before loadbalancing: 124998
Edge-cut: 44997
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4686	0	0	0	0	2634	0	
From rank 1 to: 	4686	0	1927	2846	0	659	2063	0	
From rank 2 to: 	0	1931	0	4643	2358	2471	0	0	
From rank 3 to: 	0	2855	4641	0	0	0	0	0	
From rank 4 to: 	0	0	2361	0	0	4601	0	0	
From rank 5 to: 	0	692	2462	0	4598	0	2547	2656	
From rank 6 to: 	2625	2075	0	0	0	2550	0	4739	
From rank 7 to: 	0	0	0	0	0	2690	4738	0	

Edge-cut for node partition: 10200

loadb
After loadbalancing process 3 has 132496 cells.
After loadbalancing process 0 has 132322 cells.
After loadbalancing process 7 has 132426 cells.
After loadbalancing process 6 has 136984 cells.
After loadbalancing process 4 has 131964 cells.
After loadbalancing process 1 has 137176 cells.
After loadbalancing process 2 has 136407 cells.
After loadbalancing process 5 has 137959 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4686	0	0	0	0	2634	0	
Rank 1's ghost cells:	4686	0	1927	2846	0	659	2063	0	
Rank 2's ghost cells:	0	1931	0	4643	2358	2471	0	0	
Rank 3's ghost cells:	0	2855	4641	0	0	0	0	0	
Rank 4's ghost cells:	0	0	2361	0	0	4601	0	0	
Rank 5's ghost cells:	0	692	2462	0	4598	0	2547	2656	
Rank 6's ghost cells:	2625	2075	0	0	0	2550	0	4739	
Rank 7's ghost cells:	0	0	0	0	0	2690	4738	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7694          0.21525
    2          31.0392         0.577265
    3          21.8254         0.703156
    4          18.0322         0.826203
    5          16.6646         0.924157
    6          14.0991         0.846049
    7          11.4744         0.813842
    8          10.8675         0.947112
    9          10.0682         0.926446
   10          8.33076         0.827433
   11          7.82907         0.939778
   12          7.83876          1.00124
   13          6.66097         0.849748
   14          5.99701         0.900321
   15          6.15707          1.02669
   16           5.6756         0.921801
   17          5.02639         0.885615
   18          5.09697          1.01404
   19          4.81509         0.944698
   20          4.30564         0.894197
   21          4.30625          1.00014
   22          4.06637         0.944295
   23          3.71177         0.912798
   24          3.71072         0.999717
   25          3.47652         0.936885
   26          3.14708         0.905239
   27          3.16041          1.00424
   28          2.96968         0.939651
   29          2.73196         0.919949
   30          2.80115          1.02533
   31          2.80685          1.00204
   32           2.8251           1.0065
   33          3.11008          1.10087
   34          3.11887          1.00283
   35          2.65059         0.849856
   36          2.12673          0.80236
   37          1.61891         0.761222
   38            1.302         0.804245
   39          1.18462         0.909842
   40           1.0602         0.894971
   41         0.934567         0.881503
   42         0.812852         0.869763
   43         0.658791         0.810469
   44         0.570956         0.866671
   45         0.500396         0.876419
   46         0.386851         0.773089
   47            0.326         0.842702
   48         0.282079         0.865272
   49          0.22281         0.789886
   50         0.196633         0.882514
   51         0.166235         0.845409
   52         0.135335          0.81412
   53         0.118351         0.874502
   54        0.0962141         0.812955
   55        0.0790824         0.821942
   56        0.0646433         0.817417
   57        0.0522044         0.807577
   58        0.0416891         0.798575
   59        0.0322266         0.773022
   60        0.0254921         0.791026
   61        0.0188471         0.739333
=== rate=0.855895, T=18.7139, TIT=0.306785, IT=61

 Elapsed time: 18.7139
Rank 0: Matrix-vector product took 0.107544 seconds
Rank 1: Matrix-vector product took 0.111322 seconds
Rank 2: Matrix-vector product took 0.110523 seconds
Rank 3: Matrix-vector product took 0.108017 seconds
Rank 4: Matrix-vector product took 0.108259 seconds
Rank 5: Matrix-vector product took 0.111819 seconds
Rank 6: Matrix-vector product took 0.111288 seconds
Rank 7: Matrix-vector product took 0.107427 seconds
Average time for Matrix-vector product is 0.109525 seconds

Rank 0: copyOwnerToAll took 0.000880141 seconds
Rank 1: copyOwnerToAll took 0.000880141 seconds
Rank 2: copyOwnerToAll took 0.000880141 seconds
Rank 3: copyOwnerToAll took 0.000880141 seconds
Rank 4: copyOwnerToAll took 0.000880141 seconds
Rank 5: copyOwnerToAll took 0.000880141 seconds
Rank 6: copyOwnerToAll took 0.000880141 seconds
Rank 7: copyOwnerToAll took 0.000880141 seconds
Average time for copyOwnertoAll is 0.000900049 seconds
