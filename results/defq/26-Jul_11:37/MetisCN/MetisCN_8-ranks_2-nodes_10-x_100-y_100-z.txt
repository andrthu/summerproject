Number of cores/ranks per node is: 4
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 12560
Cell on rank 1 before loadbalancing: 12450
Cell on rank 2 before loadbalancing: 12510
Cell on rank 3 before loadbalancing: 12460
Cell on rank 4 before loadbalancing: 12515
Cell on rank 5 before loadbalancing: 12519
Cell on rank 6 before loadbalancing: 12516
Cell on rank 7 before loadbalancing: 12470
Edge-cut: 5028
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	500	0	120	220	250	0	0	
From rank 1 to: 	500	0	290	120	0	0	0	0	
From rank 2 to: 	0	290	0	460	0	0	0	0	
From rank 3 to: 	120	110	460	0	0	120	0	460	
From rank 4 to: 	220	0	0	0	0	500	0	0	
From rank 5 to: 	250	0	0	120	500	0	500	30	
From rank 6 to: 	0	0	0	0	0	510	0	570	
From rank 7 to: 	0	0	0	460	0	30	570	0	

Edge-cut for node partition: 1050

loadb
After loadbalancing process 0 has 13235 cells.
After loadbalancing process 2 has 13596 cells.
After loadbalancing process 5 has 13360 cells.
After loadbalancing process 6 has 13260 cells.
After loadbalancing process 4 has 13650 cells.
After loadbalancing process 1 has 13919 cells.
After loadbalancing process 3 has 13530 cells.
After loadbalancing process 7 has 13730 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	500	0	0	220	0	0	0	
Rank 1's ghost cells:	500	0	500	30	250	0	0	120	
Rank 2's ghost cells:	0	510	0	570	0	0	0	0	
Rank 3's ghost cells:	0	30	570	0	0	0	0	460	
Rank 4's ghost cells:	220	250	0	0	0	500	0	120	
Rank 5's ghost cells:	0	0	0	0	500	0	290	120	
Rank 6's ghost cells:	0	0	0	0	0	290	0	460	
Rank 7's ghost cells:	0	120	0	460	120	110	460	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.2619         0.220043
    2          21.2498         0.602629
    3          20.2984         0.955227
    4          14.6407         0.721273
    5          7.20376         0.492037
    6          4.62385         0.641866
    7          2.78843         0.603055
    8          1.87644         0.672935
    9           1.1169         0.595223
   10         0.675283         0.604606
   11         0.527061         0.780504
   12         0.248429         0.471347
   13         0.176785         0.711612
   14         0.121141         0.685245
   15        0.0640366         0.528613
   16        0.0480621         0.750541
   17        0.0314837         0.655063
   18        0.0181848         0.577593
   19        0.0131051         0.720662
=== rate=0.609363, T=0.613181, TIT=0.0322727, IT=19

 Elapsed time: 0.613181
Rank 0: Matrix-vector product took 0.0107554 seconds
Rank 1: Matrix-vector product took 0.011044 seconds
Rank 2: Matrix-vector product took 0.0109143 seconds
Rank 3: Matrix-vector product took 0.0109157 seconds
Rank 4: Matrix-vector product took 0.0108955 seconds
Rank 5: Matrix-vector product took 0.010711 seconds
Rank 6: Matrix-vector product took 0.0107407 seconds
Rank 7: Matrix-vector product took 0.0108597 seconds
Average time for Matrix-vector product is 0.0108545 seconds

Rank 0: copyOwnerToAll took 0.00016588 seconds
Rank 1: copyOwnerToAll took 0.00016588 seconds
Rank 2: copyOwnerToAll took 0.00016588 seconds
Rank 3: copyOwnerToAll took 0.00016588 seconds
Rank 4: copyOwnerToAll took 0.00016588 seconds
Rank 5: copyOwnerToAll took 0.00016588 seconds
Rank 6: copyOwnerToAll took 0.00016588 seconds
Rank 7: copyOwnerToAll took 0.00016588 seconds
Average time for copyOwnertoAll is 0.000185607 seconds
