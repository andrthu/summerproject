Number of cores/ranks per node is: 4
Zoltan partitioner
Cell on rank 0 before loadbalancing: 6241
Cell on rank 1 before loadbalancing: 6241
Cell on rank 2 before loadbalancing: 6260
Cell on rank 3 before loadbalancing: 6260
Cell on rank 4 before loadbalancing: 6201
Cell on rank 5 before loadbalancing: 6251
Cell on rank 6 before loadbalancing: 6275
Cell on rank 7 before loadbalancing: 6271
Cell on rank 8 before loadbalancing: 6292
Cell on rank 9 before loadbalancing: 6290
Cell on rank 10 before loadbalancing: 6151
Cell on rank 11 before loadbalancing: 6267
Cell on rank 12 before loadbalancing: 6293
Cell on rank 13 before loadbalancing: 6293
Cell on rank 14 before loadbalancing: 6164
Cell on rank 15 before loadbalancing: 6250
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	249	0	255	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	249	0	180	22	240	50	0	0	6	0	0	0	0	0	0	0	
From rank 2 to: 	0	180	0	358	0	268	0	0	101	0	130	0	0	50	0	0	
From rank 3 to: 	255	15	358	0	0	0	0	0	195	0	0	0	0	0	0	0	
From rank 4 to: 	0	240	0	0	0	288	30	240	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	50	269	0	278	0	180	0	0	0	0	0	110	210	0	0	
From rank 6 to: 	0	0	0	0	30	180	0	271	0	0	0	0	50	0	0	220	
From rank 7 to: 	0	0	0	0	240	0	271	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	6	101	193	0	0	0	0	0	322	210	10	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	319	0	50	265	0	0	0	0	
From rank 10 to: 	0	0	140	0	0	0	0	0	210	50	0	330	52	347	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	8	277	330	0	187	0	0	0	
From rank 12 to: 	0	0	0	0	0	110	50	0	0	0	52	187	0	330	310	322	
From rank 13 to: 	0	0	40	0	0	210	0	0	0	0	357	0	328	0	0	64	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	316	0	0	240	
From rank 15 to: 	0	0	0	0	0	0	220	0	0	0	0	0	313	74	236	0	
loadb
After loadbalancing process 3 has 7083 cells.
After loadbalancing process 0 has 6745 cells.
After loadbalancing process 1 has 6988 cells.
After loadbalancing process 7 has 6782 cells.
After loadbalancing process 4 has 6999 cells.
After loadbalancing process 6 has 7026 cells.
After loadbalancing process 15 has 7093 cells.
After loadbalancing process 14 has 6720 cells.
After loadbalancing process 8 has 7134 cells.
After loadbalancing process 11 has 7069 cells.
After loadbalancing process 2 has 7347 cells.
After loadbalancing process 9 has 6924 cells.
After loadbalancing process 10 has 7280 cells.
After loadbalancing process 13 has 7292 cells.
After loadbalancing process 5 has 7348 cells.
After loadbalancing process 12 has 7654 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	249	0	255	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	249	0	180	22	240	50	0	0	6	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	180	0	358	0	268	0	0	101	0	130	0	0	50	0	0	
Rank 3's ghost cells:	255	15	358	0	0	0	0	0	195	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	240	0	0	0	288	30	240	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	50	269	0	278	0	180	0	0	0	0	0	110	210	0	0	
Rank 6's ghost cells:	0	0	0	0	30	180	0	271	0	0	0	0	50	0	0	220	
Rank 7's ghost cells:	0	0	0	0	240	0	271	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	6	101	193	0	0	0	0	0	322	210	10	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	319	0	50	265	0	0	0	0	
Rank 10's ghost cells:	0	0	140	0	0	0	0	0	210	50	0	330	52	347	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	8	277	330	0	187	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	110	50	0	0	0	52	187	0	330	310	322	
Rank 13's ghost cells:	0	0	40	0	0	210	0	0	0	0	357	0	328	0	0	64	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	316	0	0	240	
Rank 15's ghost cells:	0	0	0	0	0	0	220	0	0	0	0	0	313	74	236	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          36.1575         0.225632
    2          21.9863         0.608071
    3          20.8451         0.948093
    4          16.9822         0.814685
    5          8.16826          0.48099
    6          5.02105         0.614703
    7          3.43081         0.683286
    8          2.30626         0.672221
    9          1.29696         0.562366
   10         0.890591         0.686674
   11         0.657145         0.737876
   12         0.313716         0.477392
   13         0.241073         0.768443
   14         0.154519         0.640966
   15        0.0899453         0.582097
   16        0.0662488         0.736546
   17        0.0420023         0.634008
   18        0.0259836         0.618623
   19        0.0182029         0.700556
   20        0.0105503         0.579595
=== rate=0.617907, T=0.364603, TIT=0.0182302, IT=20

 Elapsed time: 0.364603
Rank 0: Matrix-vector product took 0.00531236 seconds
Rank 1: Matrix-vector product took 0.00550783 seconds
Rank 2: Matrix-vector product took 0.00576076 seconds
Rank 3: Matrix-vector product took 0.00556397 seconds
Rank 4: Matrix-vector product took 0.00568854 seconds
Rank 5: Matrix-vector product took 0.00577155 seconds
Rank 6: Matrix-vector product took 0.00555323 seconds
Rank 7: Matrix-vector product took 0.0053738 seconds
Rank 8: Matrix-vector product took 0.00561257 seconds
Rank 9: Matrix-vector product took 0.00550942 seconds
Rank 10: Matrix-vector product took 0.00573699 seconds
Rank 11: Matrix-vector product took 0.00569972 seconds
Rank 12: Matrix-vector product took 0.00598326 seconds
Rank 13: Matrix-vector product took 0.00575618 seconds
Rank 14: Matrix-vector product took 0.00527441 seconds
Rank 15: Matrix-vector product took 0.00559338 seconds
Average time for Matrix-vector product is 0.00560612 seconds

max verdi: 0.00023566
max verdi: 0.00024608
max verdi: 0.000207291
max verdi: 0.000243531
max verdi: 0.000224411
max verdi: 0.00024946
max verdi: 0.000238741
max verdi: 0.000259631
max verdi: 0.000243201
max verdi: 0.00023654
Rank 0: copyOwnerToAll took 0.000207291 seconds
Rank 1: copyOwnerToAll took 0.000207291 seconds
Rank 2: copyOwnerToAll took 0.000207291 seconds
Rank 3: copyOwnerToAll took 0.000207291 seconds
Rank 4: copyOwnerToAll took 0.000207291 seconds
Rank 5: copyOwnerToAll took 0.000207291 seconds
Rank 6: copyOwnerToAll took 0.000207291 seconds
Rank 7: copyOwnerToAll took 0.000207291 seconds
Rank 8: copyOwnerToAll took 0.000207291 seconds
Rank 9: copyOwnerToAll took 0.000207291 seconds
Rank 10: copyOwnerToAll took 0.000207291 seconds
Rank 11: copyOwnerToAll took 0.000207291 seconds
Rank 12: copyOwnerToAll took 0.000207291 seconds
Rank 13: copyOwnerToAll took 0.000207291 seconds
Rank 14: copyOwnerToAll took 0.000207291 seconds
Rank 15: copyOwnerToAll took 0.000207291 seconds
Average time for copyOwnertoAll is 0.000238455 seconds
