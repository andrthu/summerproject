Number of cores/ranks per node is: 2
Zoltan partitioner
Cell on rank 0 before loadbalancing: 125058
Cell on rank 1 before loadbalancing: 125058
Cell on rank 2 before loadbalancing: 124311
Cell on rank 3 before loadbalancing: 125573
Cell on rank 4 before loadbalancing: 123907
Cell on rank 5 before loadbalancing: 126093
Cell on rank 6 before loadbalancing: 125000
Cell on rank 7 before loadbalancing: 125000
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2607	413	3199	0	0	1869	4	
From rank 1 to: 	2606	0	2163	0	2005	79	670	0	
From rank 2 to: 	413	2163	0	2437	1	2620	52	0	
From rank 3 to: 	3217	0	2433	0	0	167	496	2424	
From rank 4 to: 	0	2005	1	0	0	2619	3092	8	
From rank 5 to: 	0	94	2626	194	2597	0	501	2000	
From rank 6 to: 	1849	674	49	489	3089	499	0	3088	
From rank 7 to: 	4	0	0	2433	8	2004	3092	0	
loadb
After loadbalancing process 1 has 132581 cells.
After loadbalancing process 0 has 133150 cells.
After loadbalancing process 2 has 131997 cells.
After loadbalancing process 4 has 131632 cells.
After loadbalancing process 7 has 132541 cells.
After loadbalancing process 6 has 134737 cells.
After loadbalancing process 3 has 134310 cells.
After loadbalancing process 5 has 134105 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2607	413	3199	0	0	1869	4	
Rank 1's ghost cells:	2606	0	2163	0	2005	79	670	0	
Rank 2's ghost cells:	413	2163	0	2437	1	2620	52	0	
Rank 3's ghost cells:	3217	0	2433	0	0	167	496	2424	
Rank 4's ghost cells:	0	2005	1	0	0	2619	3092	8	
Rank 5's ghost cells:	0	94	2626	194	2597	0	501	2000	
Rank 6's ghost cells:	1849	674	49	489	3089	499	0	3088	
Rank 7's ghost cells:	4	0	0	2433	8	2004	3092	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.5063         0.214197
    2          30.8205         0.576017
    3          21.7868         0.706893
    4          18.2993         0.839927
    5          17.2308         0.941608
    6          14.3344         0.831905
    7          11.7989         0.823116
    8          11.5319         0.977373
    9          10.2253         0.886696
   10          8.65106         0.846046
   11          8.62697         0.997216
   12           7.8463         0.909509
   13          6.67133         0.850252
   14          6.77432          1.01544
   15          6.35782         0.938518
   16          5.42335          0.85302
   17          5.45523          1.00588
   18          5.24795         0.962003
   19          4.55393         0.867754
   20          4.56976          1.00347
   21          4.43481          0.97047
   22          3.95211         0.891155
   23          3.96739          1.00387
   24          3.74109         0.942959
   25          3.38773         0.905547
   26          3.41123          1.00694
   27          3.18751         0.934418
   28          2.93268         0.920054
   29          3.01518          1.02813
   30          2.89787         0.961092
   31          2.76071         0.952669
   32          2.99635          1.08535
   33          3.10547          1.03642
   34          3.05737         0.984513
   35          2.87775         0.941249
   36          2.27778         0.791514
   37          1.84602         0.810447
   38           1.6806         0.910395
   39          1.44005         0.856863
   40          1.21934         0.846736
   41          1.09073         0.894522
   42         0.984439         0.902552
   43         0.848154         0.861561
   44         0.686712         0.809655
   45         0.600512         0.874473
   46         0.524136         0.872817
   47         0.410257          0.78273
   48         0.354538         0.864185
   49         0.302471          0.85314
   50         0.242403         0.801409
   51         0.214734         0.885855
   52         0.179045         0.833799
   53         0.145636         0.813406
   54         0.125618         0.862544
   55         0.101929         0.811422
   56        0.0842308         0.826369
   57        0.0686153         0.814611
   58         0.055813         0.813419
   59        0.0443024         0.793764
   60        0.0355693         0.802875
   61        0.0287301         0.807721
   62         0.022152          0.77104
=== rate=0.860285, T=18.5666, TIT=0.299461, IT=62

 Elapsed time: 18.5666
Rank 0: Matrix-vector product took 0.108322 seconds
Rank 1: Matrix-vector product took 0.107267 seconds
Rank 2: Matrix-vector product took 0.107593 seconds
Rank 3: Matrix-vector product took 0.109378 seconds
Rank 4: Matrix-vector product took 0.106923 seconds
Rank 5: Matrix-vector product took 0.10988 seconds
Rank 6: Matrix-vector product took 0.109269 seconds
Rank 7: Matrix-vector product took 0.10866 seconds
Average time for Matrix-vector product is 0.108412 seconds

max verdi: 0.000831203
max verdi: 0.000755442
max verdi: 0.000752313
max verdi: 0.000754092
max verdi: 0.000762432
max verdi: 0.000728542
max verdi: 0.000736632
max verdi: 0.000728312
max verdi: 0.000738142
max verdi: 0.000735292
Rank 0: copyOwnerToAll took 0.000728312 seconds
Rank 1: copyOwnerToAll took 0.000728312 seconds
Rank 2: copyOwnerToAll took 0.000728312 seconds
Rank 3: copyOwnerToAll took 0.000728312 seconds
Rank 4: copyOwnerToAll took 0.000728312 seconds
Rank 5: copyOwnerToAll took 0.000728312 seconds
Rank 6: copyOwnerToAll took 0.000728312 seconds
Rank 7: copyOwnerToAll took 0.000728312 seconds
Average time for copyOwnertoAll is 0.00075224 seconds
Number of cores/ranks per node is: 2
Zoltan partitioner
Cell on rank 0 before loadbalancing: 125058
Cell on rank 1 before loadbalancing: 125058
Cell on rank 2 before loadbalancing: 124311
Cell on rank 3 before loadbalancing: 125573
Cell on rank 4 before loadbalancing: 123907
Cell on rank 5 before loadbalancing: 126093
Cell on rank 6 before loadbalancing: 125000
Cell on rank 7 before loadbalancing: 125000
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2607	413	3199	0	0	1869	4	
From rank 1 to: 	2606	0	2163	0	2005	79	670	0	
From rank 2 to: 	413	2163	0	2437	1	2620	52	0	
From rank 3 to: 	3217	0	2433	0	0	167	496	2424	
From rank 4 to: 	0	2005	1	0	0	2619	3092	8	
From rank 5 to: 	0	94	2626	194	2597	0	501	2000	
From rank 6 to: 	1849	674	49	489	3089	499	0	3088	
From rank 7 to: 	4	0	0	2433	8	2004	3092	0	
loadb
After loadbalancing process 1 has 132581 cells.
After loadbalancing process 2 has 131997 cells.
After loadbalancing process 5 has 134105 cells.
After loadbalancing process 0 has 133150 cells.
After loadbalancing process 4 has 131632 cells.
After loadbalancing process 7 has 132541 cells.
After loadbalancing process 3 has 134310 cells.
After loadbalancing process 6 has 134737 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2607	413	3199	0	0	1869	4	
Rank 1's ghost cells:	2606	0	2163	0	2005	79	670	0	
Rank 2's ghost cells:	413	2163	0	2437	1	2620	52	0	
Rank 3's ghost cells:	3217	0	2433	0	0	167	496	2424	
Rank 4's ghost cells:	0	2005	1	0	0	2619	3092	8	
Rank 5's ghost cells:	0	94	2626	194	2597	0	501	2000	
Rank 6's ghost cells:	1849	674	49	489	3089	499	0	3088	
Rank 7's ghost cells:	4	0	0	2433	8	2004	3092	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.5063         0.214197
    2          30.8205         0.576017
    3          21.7868         0.706893
    4          18.2993         0.839927
    5          17.2308         0.941608
    6          14.3344         0.831905
    7          11.7989         0.823116
    8          11.5319         0.977373
    9          10.2253         0.886696
   10          8.65106         0.846046
   11          8.62697         0.997216
   12           7.8463         0.909509
   13          6.67133         0.850252
   14          6.77432          1.01544
   15          6.35782         0.938518
   16          5.42335          0.85302
   17          5.45523          1.00588
   18          5.24795         0.962003
   19          4.55393         0.867754
   20          4.56976          1.00347
   21          4.43481          0.97047
   22          3.95211         0.891155
   23          3.96739          1.00387
   24          3.74109         0.942959
   25          3.38773         0.905547
   26          3.41123          1.00694
   27          3.18751         0.934418
   28          2.93268         0.920054
   29          3.01518          1.02813
   30          2.89787         0.961092
   31          2.76071         0.952669
   32          2.99635          1.08535
   33          3.10547          1.03642
   34          3.05737         0.984513
   35          2.87775         0.941249
   36          2.27778         0.791514
   37          1.84602         0.810447
   38           1.6806         0.910395
   39          1.44005         0.856863
   40          1.21934         0.846736
   41          1.09073         0.894522
   42         0.984439         0.902552
   43         0.848154         0.861561
   44         0.686712         0.809655
   45         0.600512         0.874473
   46         0.524136         0.872817
   47         0.410257          0.78273
   48         0.354538         0.864185
   49         0.302471          0.85314
   50         0.242403         0.801409
   51         0.214734         0.885855
   52         0.179045         0.833799
   53         0.145636         0.813406
   54         0.125618         0.862544
   55         0.101929         0.811422
   56        0.0842308         0.826369
   57        0.0686153         0.814611
   58         0.055813         0.813419
   59        0.0443024         0.793764
   60        0.0355693         0.802875
   61        0.0287301         0.807721
   62         0.022152          0.77104
=== rate=0.860285, T=18.4408, TIT=0.297432, IT=62

 Elapsed time: 18.4408
Rank 0: Matrix-vector product took 0.108129 seconds
Rank 1: Matrix-vector product took 0.107709 seconds
Rank 2: Matrix-vector product took 0.107277 seconds
Rank 3: Matrix-vector product took 0.109224 seconds
Rank 4: Matrix-vector product took 0.107054 seconds
Rank 5: Matrix-vector product took 0.109047 seconds
Rank 6: Matrix-vector product took 0.109428 seconds
Rank 7: Matrix-vector product took 0.107696 seconds
Average time for Matrix-vector product is 0.108195 seconds

max verdi: 0.000829483
max verdi: 0.000771582
max verdi: 0.000741692
max verdi: 0.000752762
max verdi: 0.000732642
max verdi: 0.000767323
max verdi: 0.000738472
max verdi: 0.000744303
max verdi: 0.000748612
max verdi: 0.000735732
Rank 0: copyOwnerToAll took 0.000732642 seconds
Rank 1: copyOwnerToAll took 0.000732642 seconds
Rank 2: copyOwnerToAll took 0.000732642 seconds
Rank 3: copyOwnerToAll took 0.000732642 seconds
Rank 4: copyOwnerToAll took 0.000732642 seconds
Rank 5: copyOwnerToAll took 0.000732642 seconds
Rank 6: copyOwnerToAll took 0.000732642 seconds
Rank 7: copyOwnerToAll took 0.000732642 seconds
Average time for copyOwnertoAll is 0.00075626 seconds
