Number of cores/ranks per node is: 10
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 31562
Cell on rank 1 before loadbalancing: 31191
Cell on rank 2 before loadbalancing: 31163
Cell on rank 3 before loadbalancing: 30902
Cell on rank 4 before loadbalancing: 31395
Cell on rank 5 before loadbalancing: 31135
Cell on rank 6 before loadbalancing: 31027
Cell on rank 7 before loadbalancing: 30938
Cell on rank 8 before loadbalancing: 31255
Cell on rank 9 before loadbalancing: 31562
Cell on rank 10 before loadbalancing: 31263
Cell on rank 11 before loadbalancing: 31042
Cell on rank 12 before loadbalancing: 31545
Cell on rank 13 before loadbalancing: 30976
Cell on rank 14 before loadbalancing: 31459
Cell on rank 15 before loadbalancing: 31176
Cell on rank 16 before loadbalancing: 31003
Cell on rank 17 before loadbalancing: 31375
Cell on rank 18 before loadbalancing: 31545
Cell on rank 19 before loadbalancing: 31602
Cell on rank 20 before loadbalancing: 31450
Cell on rank 21 before loadbalancing: 31312
Cell on rank 22 before loadbalancing: 30931
Cell on rank 23 before loadbalancing: 30920
Cell on rank 24 before loadbalancing: 31372
Cell on rank 25 before loadbalancing: 31561
Cell on rank 26 before loadbalancing: 30938
Cell on rank 27 before loadbalancing: 31020
Cell on rank 28 before loadbalancing: 31562
Cell on rank 29 before loadbalancing: 31250
Cell on rank 30 before loadbalancing: 31367
Cell on rank 31 before loadbalancing: 31201
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	992	313	409	0	289	150	0	0	92	8	0	148	0	585	613	0	0	0	3	666	467	0	0	103	359	0	0	0	0	0	0	
From rank 1 to: 	992	0	318	733	0	0	494	0	0	0	0	0	450	624	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	314	316	0	1065	0	495	788	0	0	0	0	0	0	0	0	0	0	0	141	0	623	244	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	409	749	1065	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	728	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	901	56	1143	462	12	37	0	0	0	0	0	0	0	816	469	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	285	0	490	0	906	0	1071	336	7	391	292	0	0	0	0	0	0	0	651	634	238	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	150	494	785	0	52	1073	0	794	0	0	326	0	312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	1143	317	794	0	9	0	660	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	462	7	0	9	0	819	579	678	0	0	0	0	0	0	0	216	0	0	0	0	0	0	0	0	1066	0	0	0	
From rank 9 to: 	86	0	0	0	6	392	0	0	819	0	612	507	263	0	1112	0	0	0	0	243	47	0	0	0	0	0	0	0	366	917	0	0	
From rank 10 to: 	8	0	0	0	37	293	319	657	574	612	0	1121	700	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	679	514	1121	0	559	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	138	460	0	0	0	0	312	0	0	272	701	557	0	948	854	44	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	624	0	0	0	0	0	0	0	0	0	0	956	0	234	942	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	596	0	0	0	0	0	0	0	0	1112	0	0	846	233	0	979	0	0	0	0	198	0	0	0	279	50	638	0	0	347	0	0	
From rank 15 to: 	613	106	0	0	0	0	0	0	0	0	0	0	45	948	972	0	0	0	0	0	0	0	0	0	0	247	751	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1081	691	332	0	0	0	0	0	0	0	0	0	0	545	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1081	0	637	496	10	0	0	1109	125	0	0	0	0	0	598	0	
From rank 18 to: 	0	0	142	0	807	644	0	0	0	0	0	0	0	0	0	0	687	633	0	719	563	0	0	78	0	0	0	0	0	0	0	0	
From rank 19 to: 	3	0	0	0	465	634	0	0	204	239	0	0	0	0	0	0	331	511	720	0	503	0	0	98	59	0	0	0	446	454	496	0	
From rank 20 to: 	672	0	623	0	0	236	0	0	0	43	0	0	0	0	202	0	0	10	562	502	0	827	201	838	392	1	0	0	0	120	0	0	
From rank 21 to: 	458	0	238	728	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	827	0	1033	47	11	331	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	1041	0	1064	73	738	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1108	88	101	834	46	1071	0	756	86	0	0	0	0	0	0	
From rank 24 to: 	110	0	0	0	0	0	0	0	0	0	0	0	0	0	276	0	0	139	0	58	393	14	77	753	0	993	340	592	0	710	483	0	
From rank 25 to: 	357	0	0	0	0	0	0	0	0	0	0	0	0	0	54	246	0	0	0	0	1	320	737	86	982	0	470	506	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	638	740	0	0	0	0	0	0	0	0	341	466	0	1323	0	690	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	582	527	1324	0	0	252	35	536	
From rank 28 to: 	0	0	0	0	0	0	0	0	1063	363	0	0	0	0	0	0	0	0	0	446	0	0	0	0	0	0	0	0	0	1027	450	1071	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	917	0	0	0	0	376	0	0	0	0	473	121	0	0	0	723	0	690	251	1028	0	455	408	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	532	598	0	496	0	0	0	0	483	0	0	35	453	448	0	957	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	536	1064	415	960	0	

Edge-cut for node partition: 42563

loadb
After loadbalancing process 7 has 35010 cells.
After loadbalancing process 10 has 34916 cells.
After loadbalancing process 15 has 34985 cells.
After loadbalancing process 2 has 33732 cells.
After loadbalancing process 8 has 36310 cells.
After loadbalancing process 16 has 34064 cells.
After loadbalancing process 30 has 35982 cells.
After loadbalancing process 26 has 35091 cells.
After loadbalancing process 3 has 36737 cells.
After loadbalancing process 18 has 34276 cells.
After loadbalancing process 21 has 36759 cells.
After loadbalancing process 31 has 34176 cells.After loadbalancing process 11 has 33853 cells.

After loadbalancing process 25 has 33861 cells.
After loadbalancing process 5 has 35818 cells.After loadbalancing process 29 has 33652 cells.

After loadbalancing process 17 has 35320 cells.
After loadbalancing process 13 has 35831 cells.
After loadbalancing process 22 has 35149 cells.
After loadbalancing process 9 has 35136 cells.
After loadbalancing process 0 has 35013 cells.
After loadbalancing process 23 has 35291 cells.
After loadbalancing process 28 has 34858 cells.
After loadbalancing process 4 has 35431 cells.
After loadbalancing process 12 has 33915 cells.
After loadbalancing process 20 has 35369 cells.
After loadbalancing process 27 has 35584 cells.
After loadbalancing process 19 has 36692 cells.
After loadbalancing process 6 has 36765 cells.
After loadbalancing process 14 has 36679 cells.
After loadbalancing process 1 has 36932 cells.
After loadbalancing process 24 has 36436 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	0	0	0	0	0	0	0	0	0	494	0	0	312	0	0	0	0	0	0	0	150	785	52	1073	794	0	326	0	0	0	0	
Rank 1's ghost cells:	0	0	0	1112	0	0	243	0	0	0	0	0	507	263	47	0	0	0	0	917	0	86	0	6	392	0	819	612	0	0	366	0	
Rank 2's ghost cells:	0	0	0	234	0	0	0	0	0	0	624	0	0	956	0	0	0	0	0	0	0	0	0	0	0	0	0	0	942	0	0	0	
Rank 3's ghost cells:	0	1112	233	0	0	0	0	0	279	638	0	0	0	846	198	0	0	50	0	347	0	596	0	0	0	0	0	0	979	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	637	496	1109	125	0	0	0	0	0	10	0	0	0	0	0	598	0	0	0	0	0	0	0	0	1081	0	0	
Rank 5's ghost cells:	0	0	0	0	633	0	719	78	0	0	0	0	0	0	563	0	0	0	0	0	0	0	142	807	644	0	0	0	0	687	0	0	
Rank 6's ghost cells:	0	239	0	0	511	720	0	98	59	0	0	0	0	0	503	0	0	0	0	454	496	3	0	465	634	0	204	0	0	331	446	0	
Rank 7's ghost cells:	0	0	0	0	1108	88	101	0	756	0	0	0	0	0	834	46	1071	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	276	139	0	58	753	0	340	0	0	0	0	393	14	77	993	592	710	483	110	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	638	0	0	0	0	341	0	0	0	0	0	0	0	0	466	1323	690	0	0	0	0	0	0	0	0	740	0	0	0	
Rank 10's ghost cells:	494	0	624	0	0	0	0	0	0	0	0	733	0	450	0	0	0	0	0	0	0	992	318	0	0	0	0	0	114	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	749	0	0	0	0	728	0	0	0	0	0	409	1065	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	514	0	0	0	0	0	0	0	0	0	0	0	559	0	0	0	0	0	0	0	0	0	0	0	0	679	1121	0	0	0	0	
Rank 13's ghost cells:	312	272	948	854	0	0	0	0	0	0	460	0	557	0	0	0	0	0	0	0	0	138	0	0	0	0	0	701	44	0	0	0	
Rank 14's ghost cells:	0	43	0	202	10	562	502	838	392	0	0	0	0	0	0	827	201	1	0	120	0	672	623	0	236	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	47	11	0	0	728	0	0	827	0	1033	331	0	0	0	458	238	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	1064	73	0	0	0	0	0	217	1041	0	738	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	54	0	0	0	86	982	470	0	0	0	0	1	320	737	0	506	0	0	357	0	0	0	0	0	0	246	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	582	1324	0	0	0	0	0	0	0	527	0	252	35	0	0	0	0	0	0	0	0	0	0	536	
Rank 19's ghost cells:	0	917	0	376	0	0	473	0	723	690	0	0	0	0	121	0	0	0	251	0	455	0	0	0	0	0	0	0	0	0	1028	408	
Rank 20's ghost cells:	0	0	0	0	598	0	496	0	483	0	0	0	0	0	0	0	0	0	35	448	0	0	0	0	0	0	0	0	0	532	453	957	
Rank 21's ghost cells:	150	92	0	585	0	0	3	0	103	0	992	409	0	148	666	467	0	359	0	0	0	0	313	0	289	0	0	8	613	0	0	0	
Rank 22's ghost cells:	788	0	0	0	0	141	0	0	0	0	316	1065	0	0	623	244	0	0	0	0	0	314	0	0	495	0	0	0	0	0	0	0	
Rank 23's ghost cells:	56	12	0	0	0	816	469	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	901	1143	462	37	0	0	0	0	
Rank 24's ghost cells:	1071	391	0	0	0	651	634	0	0	0	0	0	0	0	238	0	0	0	0	0	0	285	490	906	0	336	7	292	0	0	0	0	
Rank 25's ghost cells:	794	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1143	317	0	9	660	0	0	0	0	
Rank 26's ghost cells:	0	819	0	0	0	0	216	0	0	0	0	0	678	0	0	0	0	0	0	0	0	0	0	462	7	9	0	579	0	0	1066	0	
Rank 27's ghost cells:	319	612	0	0	0	0	0	0	0	0	0	0	1121	700	0	0	0	0	0	0	0	8	0	37	293	657	574	0	0	0	0	0	
Rank 28's ghost cells:	0	0	948	972	0	0	0	0	0	751	106	0	0	45	0	0	0	247	0	0	0	613	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	1081	691	332	0	0	0	0	0	0	0	0	0	0	0	0	0	545	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	363	0	0	0	0	446	0	0	0	0	0	0	0	0	0	0	0	0	1027	450	0	0	0	0	0	1063	0	0	0	0	1071	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	536	415	960	0	0	0	0	0	0	0	0	0	1064	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.7478          0.22317
    2          32.4582         0.582232
    3          23.1823         0.714221
    4          20.4082         0.880335
    5          18.2515         0.894323
    6          14.2673         0.781704
    7          12.4661         0.873756
    8          12.0986          0.97052
    9          10.1491         0.838866
   10          9.01325         0.888082
   11          8.97421         0.995668
   12          7.83371         0.872913
   13          6.87521         0.877644
   14          7.00483          1.01885
   15          6.45557         0.921589
   16          5.70308         0.883435
   17          5.75579          1.00924
   18           5.4667         0.949773
   19          4.81646         0.881055
   20          4.84127          1.00515
   21          4.63032         0.956428
   22          4.26378         0.920838
   23          4.27146           1.0018
   24          3.98041         0.931861
   25          3.69512         0.928327
   26          3.68139         0.996284
   27          3.35995         0.912683
   28          3.18349         0.947484
   29          3.14473         0.987823
   30          2.89017         0.919054
   31          2.88269         0.997411
   32          2.93198           1.0171
   33          2.89777         0.988332
   34          3.09015          1.06639
   35          3.20306          1.03654
   36          2.92801         0.914131
   37          2.54052         0.867662
   38          2.00393         0.788787
   39          1.56313         0.780031
   40          1.32736         0.849168
   41          1.17638         0.886253
   42          1.06841         0.908221
   43         0.954155         0.893061
   44         0.798496         0.836862
   45         0.689458         0.863446
   46         0.592698         0.859658
   47         0.488674          0.82449
   48         0.417434         0.854219
   49         0.345245         0.827065
   50         0.285802         0.827824
   51         0.247712         0.866725
   52         0.208587         0.842054
   53         0.176716         0.847208
   54         0.152956         0.865547
   55         0.125555         0.820854
   56         0.106098         0.845037
   57          0.08906         0.839409
   58        0.0726814         0.816095
   59        0.0606544         0.834523
   60        0.0492635         0.812201
   61        0.0392423         0.796579
   62        0.0310885         0.792221
   63        0.0244247         0.785649
=== rate=0.86368, T=5.31627, TIT=0.0843853, IT=63

 Elapsed time: 5.31627
Rank 0: Matrix-vector product took 0.0285112 seconds
Rank 1: Matrix-vector product took 0.0298021 seconds
Rank 2: Matrix-vector product took 0.0271739 seconds
Rank 3: Matrix-vector product took 0.0296577 seconds
Rank 4: Matrix-vector product took 0.0287807 seconds
Rank 5: Matrix-vector product took 0.0287025 seconds
Rank 6: Matrix-vector product took 0.0296886 seconds
Rank 7: Matrix-vector product took 0.0285082 seconds
Rank 8: Matrix-vector product took 0.0294406 seconds
Rank 9: Matrix-vector product took 0.0282674 seconds
Rank 10: Matrix-vector product took 0.0280902 seconds
Rank 11: Matrix-vector product took 0.0272422 seconds
Rank 12: Matrix-vector product took 0.027614 seconds
Rank 13: Matrix-vector product took 0.0289686 seconds
Rank 14: Matrix-vector product took 0.0294029 seconds
Rank 15: Matrix-vector product took 0.028011 seconds
Rank 16: Matrix-vector product took 0.0272745 seconds
Rank 17: Matrix-vector product took 0.0283637 seconds
Rank 18: Matrix-vector product took 0.027456 seconds
Rank 19: Matrix-vector product took 0.0294865 seconds
Rank 20: Matrix-vector product took 0.028846 seconds
Rank 21: Matrix-vector product took 0.0294388 seconds
Rank 22: Matrix-vector product took 0.0283926 seconds
Rank 23: Matrix-vector product took 0.0329606 seconds
Rank 24: Matrix-vector product took 0.0298239 seconds
Rank 25: Matrix-vector product took 0.0277188 seconds
Rank 26: Matrix-vector product took 0.0281631 seconds
Rank 27: Matrix-vector product took 0.0286483 seconds
Rank 28: Matrix-vector product took 0.0285724 seconds
Rank 29: Matrix-vector product took 0.0269712 seconds
Rank 30: Matrix-vector product took 0.0289208 seconds
Rank 31: Matrix-vector product took 0.0276377 seconds
Average time for Matrix-vector product is 0.0286418 seconds

Rank 0: copyOwnerToAll took 0.00062064 seconds
Rank 1: copyOwnerToAll took 0.00062064 seconds
Rank 2: copyOwnerToAll took 0.00062064 seconds
Rank 3: copyOwnerToAll took 0.00062064 seconds
Rank 4: copyOwnerToAll took 0.00062064 seconds
Rank 5: copyOwnerToAll took 0.00062064 seconds
Rank 6: copyOwnerToAll took 0.00062064 seconds
Rank 7: copyOwnerToAll took 0.00062064 seconds
Rank 8: copyOwnerToAll took 0.00062064 seconds
Rank 9: copyOwnerToAll took 0.00062064 seconds
Rank 10: copyOwnerToAll took 0.00062064 seconds
Rank 11: copyOwnerToAll took 0.00062064 seconds
Rank 12: copyOwnerToAll took 0.00062064 seconds
Rank 13: copyOwnerToAll took 0.00062064 seconds
Rank 14: copyOwnerToAll took 0.00062064 seconds
Rank 15: copyOwnerToAll took 0.00062064 seconds
Rank 16: copyOwnerToAll took 0.00062064 seconds
Rank 17: copyOwnerToAll took 0.00062064 seconds
Rank 18: copyOwnerToAll took 0.00062064 seconds
Rank 19: copyOwnerToAll took 0.00062064 seconds
Rank 20: copyOwnerToAll took 0.00062064 seconds
Rank 21: copyOwnerToAll took 0.00062064 seconds
Rank 22: copyOwnerToAll took 0.00062064 seconds
Rank 23: copyOwnerToAll took 0.00062064 seconds
Rank 24: copyOwnerToAll took 0.00062064 seconds
Rank 25: copyOwnerToAll took 0.00062064 seconds
Rank 26: copyOwnerToAll took 0.00062064 seconds
Rank 27: copyOwnerToAll took 0.00062064 seconds
Rank 28: copyOwnerToAll took 0.00062064 seconds
Rank 29: copyOwnerToAll took 0.00062064 seconds
Rank 30: copyOwnerToAll took 0.00062064 seconds
Rank 31: copyOwnerToAll took 0.00062064 seconds
Average time for copyOwnertoAll is 0.000651152 seconds
Number of cores/ranks per node is: 10
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 31562
Cell on rank 1 before loadbalancing: 31191
Cell on rank 2 before loadbalancing: 31163
Cell on rank 3 before loadbalancing: 30902
Cell on rank 4 before loadbalancing: 31395
Cell on rank 5 before loadbalancing: 31135
Cell on rank 6 before loadbalancing: 31027
Cell on rank 7 before loadbalancing: 30938
Cell on rank 8 before loadbalancing: 31255
Cell on rank 9 before loadbalancing: 31562
Cell on rank 10 before loadbalancing: 31263
Cell on rank 11 before loadbalancing: 31042
Cell on rank 12 before loadbalancing: 31545
Cell on rank 13 before loadbalancing: 30976
Cell on rank 14 before loadbalancing: 31459
Cell on rank 15 before loadbalancing: 31176
Cell on rank 16 before loadbalancing: 31003
Cell on rank 17 before loadbalancing: 31375
Cell on rank 18 before loadbalancing: 31545
Cell on rank 19 before loadbalancing: 31602
Cell on rank 20 before loadbalancing: 31450
Cell on rank 21 before loadbalancing: 31312
Cell on rank 22 before loadbalancing: 30931
Cell on rank 23 before loadbalancing: 30920
Cell on rank 24 before loadbalancing: 31372
Cell on rank 25 before loadbalancing: 31561
Cell on rank 26 before loadbalancing: 30938
Cell on rank 27 before loadbalancing: 31020
Cell on rank 28 before loadbalancing: 31562
Cell on rank 29 before loadbalancing: 31250
Cell on rank 30 before loadbalancing: 31367
Cell on rank 31 before loadbalancing: 31201
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	992	313	409	0	289	150	0	0	92	8	0	148	0	585	613	0	0	0	3	666	467	0	0	103	359	0	0	0	0	0	0	
From rank 1 to: 	992	0	318	733	0	0	494	0	0	0	0	0	450	624	0	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	314	316	0	1065	0	495	788	0	0	0	0	0	0	0	0	0	0	0	141	0	623	244	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	409	749	1065	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	728	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	901	56	1143	462	12	37	0	0	0	0	0	0	0	816	469	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	285	0	490	0	906	0	1071	336	7	391	292	0	0	0	0	0	0	0	651	634	238	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	150	494	785	0	52	1073	0	794	0	0	326	0	312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	1143	317	794	0	9	0	660	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	462	7	0	9	0	819	579	678	0	0	0	0	0	0	0	216	0	0	0	0	0	0	0	0	1066	0	0	0	
From rank 9 to: 	86	0	0	0	6	392	0	0	819	0	612	507	263	0	1112	0	0	0	0	243	47	0	0	0	0	0	0	0	366	917	0	0	
From rank 10 to: 	8	0	0	0	37	293	319	657	574	612	0	1121	700	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	679	514	1121	0	559	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	138	460	0	0	0	0	312	0	0	272	701	557	0	948	854	44	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	624	0	0	0	0	0	0	0	0	0	0	956	0	234	942	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	596	0	0	0	0	0	0	0	0	1112	0	0	846	233	0	979	0	0	0	0	198	0	0	0	279	50	638	0	0	347	0	0	
From rank 15 to: 	613	106	0	0	0	0	0	0	0	0	0	0	45	948	972	0	0	0	0	0	0	0	0	0	0	247	751	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1081	691	332	0	0	0	0	0	0	0	0	0	0	545	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1081	0	637	496	10	0	0	1109	125	0	0	0	0	0	598	0	
From rank 18 to: 	0	0	142	0	807	644	0	0	0	0	0	0	0	0	0	0	687	633	0	719	563	0	0	78	0	0	0	0	0	0	0	0	
From rank 19 to: 	3	0	0	0	465	634	0	0	204	239	0	0	0	0	0	0	331	511	720	0	503	0	0	98	59	0	0	0	446	454	496	0	
From rank 20 to: 	672	0	623	0	0	236	0	0	0	43	0	0	0	0	202	0	0	10	562	502	0	827	201	838	392	1	0	0	0	120	0	0	
From rank 21 to: 	458	0	238	728	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	827	0	1033	47	11	331	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	1041	0	1064	73	738	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1108	88	101	834	46	1071	0	756	86	0	0	0	0	0	0	
From rank 24 to: 	110	0	0	0	0	0	0	0	0	0	0	0	0	0	276	0	0	139	0	58	393	14	77	753	0	993	340	592	0	710	483	0	
From rank 25 to: 	357	0	0	0	0	0	0	0	0	0	0	0	0	0	54	246	0	0	0	0	1	320	737	86	982	0	470	506	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	638	740	0	0	0	0	0	0	0	0	341	466	0	1323	0	690	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	582	527	1324	0	0	252	35	536	
From rank 28 to: 	0	0	0	0	0	0	0	0	1063	363	0	0	0	0	0	0	0	0	0	446	0	0	0	0	0	0	0	0	0	1027	450	1071	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	917	0	0	0	0	376	0	0	0	0	473	121	0	0	0	723	0	690	251	1028	0	455	408	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	532	598	0	496	0	0	0	0	483	0	0	35	453	448	0	957	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	536	1064	415	960	0	

Edge-cut for node partition: 42563

loadb
After loadbalancing process 31 has 34176 cells.
After loadbalancing process 2 has 33732 cells.
After loadbalancing process 4 has 35431 cells.
After loadbalancing process 18 has 34276 cells.
After loadbalancing process 23 has 35291 cells.
After loadbalancing process 19 has 36692 cells.
After loadbalancing process 5 has 35818 cells.
After loadbalancing process 30 has 35982 cells.
After loadbalancing process 22 has 35149 cells.
After loadbalancing process 9 has 35136 cells.
After loadbalancing process 16 has 34064 cells.
After loadbalancing process 7 has 35010 cells.
After loadbalancing process 29 has 33652 cells.
After loadbalancing process 0 has 35013 cells.
After loadbalancing process 13 has 35831 cells.
After loadbalancing process 1 has 36932 cells.
After loadbalancing process 6 has 36765 cells.
After loadbalancing process 3 has 36737 cells.
After loadbalancing process 8 has 36310 cells.
After loadbalancing process 25 has 33861 cells.
After loadbalancing process 20 has 35369 cells.
After loadbalancing process 12 has 33915 cells.
After loadbalancing process 26 has 35091 cells.
After loadbalancing process 11 has 33853 cells.
After loadbalancing process 27 has 35584 cells.After loadbalancing process 28 has 34858 cells.

After loadbalancing process 10 has 34916 cells.
After loadbalancing process 14 has 36679 cells.
After loadbalancing process 24 has 36436 cells.
After loadbalancing process 17 has 35320 cells.
After loadbalancing process 15 has 34985 cells.
After loadbalancing process 21 has 36759 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	0	0	0	0	0	0	0	0	0	494	0	0	312	0	0	0	0	0	0	0	150	785	52	1073	794	0	326	0	0	0	0	
Rank 1's ghost cells:	0	0	0	1112	0	0	243	0	0	0	0	0	507	263	47	0	0	0	0	917	0	86	0	6	392	0	819	612	0	0	366	0	
Rank 2's ghost cells:	0	0	0	234	0	0	0	0	0	0	624	0	0	956	0	0	0	0	0	0	0	0	0	0	0	0	0	0	942	0	0	0	
Rank 3's ghost cells:	0	1112	233	0	0	0	0	0	279	638	0	0	0	846	198	0	0	50	0	347	0	596	0	0	0	0	0	0	979	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	637	496	1109	125	0	0	0	0	0	10	0	0	0	0	0	598	0	0	0	0	0	0	0	0	1081	0	0	
Rank 5's ghost cells:	0	0	0	0	633	0	719	78	0	0	0	0	0	0	563	0	0	0	0	0	0	0	142	807	644	0	0	0	0	687	0	0	
Rank 6's ghost cells:	0	239	0	0	511	720	0	98	59	0	0	0	0	0	503	0	0	0	0	454	496	3	0	465	634	0	204	0	0	331	446	0	
Rank 7's ghost cells:	0	0	0	0	1108	88	101	0	756	0	0	0	0	0	834	46	1071	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	276	139	0	58	753	0	340	0	0	0	0	393	14	77	993	592	710	483	110	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	638	0	0	0	0	341	0	0	0	0	0	0	0	0	466	1323	690	0	0	0	0	0	0	0	0	740	0	0	0	
Rank 10's ghost cells:	494	0	624	0	0	0	0	0	0	0	0	733	0	450	0	0	0	0	0	0	0	992	318	0	0	0	0	0	114	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	749	0	0	0	0	728	0	0	0	0	0	409	1065	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	514	0	0	0	0	0	0	0	0	0	0	0	559	0	0	0	0	0	0	0	0	0	0	0	0	679	1121	0	0	0	0	
Rank 13's ghost cells:	312	272	948	854	0	0	0	0	0	0	460	0	557	0	0	0	0	0	0	0	0	138	0	0	0	0	0	701	44	0	0	0	
Rank 14's ghost cells:	0	43	0	202	10	562	502	838	392	0	0	0	0	0	0	827	201	1	0	120	0	672	623	0	236	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	47	11	0	0	728	0	0	827	0	1033	331	0	0	0	458	238	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	1064	73	0	0	0	0	0	217	1041	0	738	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	54	0	0	0	86	982	470	0	0	0	0	1	320	737	0	506	0	0	357	0	0	0	0	0	0	246	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	582	1324	0	0	0	0	0	0	0	527	0	252	35	0	0	0	0	0	0	0	0	0	0	536	
Rank 19's ghost cells:	0	917	0	376	0	0	473	0	723	690	0	0	0	0	121	0	0	0	251	0	455	0	0	0	0	0	0	0	0	0	1028	408	
Rank 20's ghost cells:	0	0	0	0	598	0	496	0	483	0	0	0	0	0	0	0	0	0	35	448	0	0	0	0	0	0	0	0	0	532	453	957	
Rank 21's ghost cells:	150	92	0	585	0	0	3	0	103	0	992	409	0	148	666	467	0	359	0	0	0	0	313	0	289	0	0	8	613	0	0	0	
Rank 22's ghost cells:	788	0	0	0	0	141	0	0	0	0	316	1065	0	0	623	244	0	0	0	0	0	314	0	0	495	0	0	0	0	0	0	0	
Rank 23's ghost cells:	56	12	0	0	0	816	469	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	901	1143	462	37	0	0	0	0	
Rank 24's ghost cells:	1071	391	0	0	0	651	634	0	0	0	0	0	0	0	238	0	0	0	0	0	0	285	490	906	0	336	7	292	0	0	0	0	
Rank 25's ghost cells:	794	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1143	317	0	9	660	0	0	0	0	
Rank 26's ghost cells:	0	819	0	0	0	0	216	0	0	0	0	0	678	0	0	0	0	0	0	0	0	0	0	462	7	9	0	579	0	0	1066	0	
Rank 27's ghost cells:	319	612	0	0	0	0	0	0	0	0	0	0	1121	700	0	0	0	0	0	0	0	8	0	37	293	657	574	0	0	0	0	0	
Rank 28's ghost cells:	0	0	948	972	0	0	0	0	0	751	106	0	0	45	0	0	0	247	0	0	0	613	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	1081	691	332	0	0	0	0	0	0	0	0	0	0	0	0	0	545	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	363	0	0	0	0	446	0	0	0	0	0	0	0	0	0	0	0	0	1027	450	0	0	0	0	0	1063	0	0	0	0	1071	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	536	415	960	0	0	0	0	0	0	0	0	0	1064	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.7478          0.22317
    2          32.4582         0.582232
    3          23.1823         0.714221
    4          20.4082         0.880335
    5          18.2515         0.894323
    6          14.2673         0.781704
    7          12.4661         0.873756
    8          12.0986          0.97052
    9          10.1491         0.838866
   10          9.01325         0.888082
   11          8.97421         0.995668
   12          7.83371         0.872913
   13          6.87521         0.877644
   14          7.00483          1.01885
   15          6.45557         0.921589
   16          5.70308         0.883435
   17          5.75579          1.00924
   18           5.4667         0.949773
   19          4.81646         0.881055
   20          4.84127          1.00515
   21          4.63032         0.956428
   22          4.26378         0.920838
   23          4.27146           1.0018
   24          3.98041         0.931861
   25          3.69512         0.928327
   26          3.68139         0.996284
   27          3.35995         0.912683
   28          3.18349         0.947484
   29          3.14473         0.987823
   30          2.89017         0.919054
   31          2.88269         0.997411
   32          2.93198           1.0171
   33          2.89777         0.988332
   34          3.09015          1.06639
   35          3.20306          1.03654
   36          2.92801         0.914131
   37          2.54052         0.867662
   38          2.00393         0.788787
   39          1.56313         0.780031
   40          1.32736         0.849168
   41          1.17638         0.886253
   42          1.06841         0.908221
   43         0.954155         0.893061
   44         0.798496         0.836862
   45         0.689458         0.863446
   46         0.592698         0.859658
   47         0.488674          0.82449
   48         0.417434         0.854219
   49         0.345245         0.827065
   50         0.285802         0.827824
   51         0.247712         0.866725
   52         0.208587         0.842054
   53         0.176716         0.847208
   54         0.152956         0.865547
   55         0.125555         0.820854
   56         0.106098         0.845037
   57          0.08906         0.839409
   58        0.0726814         0.816095
   59        0.0606544         0.834523
   60        0.0492635         0.812201
   61        0.0392423         0.796579
   62        0.0310885         0.792221
   63        0.0244247         0.785649
=== rate=0.86368, T=5.46005, TIT=0.0866674, IT=63

 Elapsed time: 5.46005
Rank 0: Matrix-vector product took 0.0280662 seconds
Rank 1: Matrix-vector product took 0.0298919 seconds
Rank 2: Matrix-vector product took 0.0271009 seconds
Rank 3: Matrix-vector product took 0.0298708 seconds
Rank 4: Matrix-vector product took 0.0285101 seconds
Rank 5: Matrix-vector product took 0.0286758 seconds
Rank 6: Matrix-vector product took 0.0295268 seconds
Rank 7: Matrix-vector product took 0.0281117 seconds
Rank 8: Matrix-vector product took 0.0291657 seconds
Rank 9: Matrix-vector product took 0.0282701 seconds
Rank 10: Matrix-vector product took 0.0280827 seconds
Rank 11: Matrix-vector product took 0.0274273 seconds
Rank 12: Matrix-vector product took 0.0273058 seconds
Rank 13: Matrix-vector product took 0.0289536 seconds
Rank 14: Matrix-vector product took 0.0294532 seconds
Rank 15: Matrix-vector product took 0.0282756 seconds
Rank 16: Matrix-vector product took 0.0272824 seconds
Rank 17: Matrix-vector product took 0.0285435 seconds
Rank 18: Matrix-vector product took 0.0273426 seconds
Rank 19: Matrix-vector product took 0.0293987 seconds
Rank 20: Matrix-vector product took 0.0363753 seconds
Rank 21: Matrix-vector product took 0.029443 seconds
Rank 22: Matrix-vector product took 0.0284387 seconds
Rank 23: Matrix-vector product took 0.0283774 seconds
Rank 24: Matrix-vector product took 0.0293724 seconds
Rank 25: Matrix-vector product took 0.0272682 seconds
Rank 26: Matrix-vector product took 0.0283573 seconds
Rank 27: Matrix-vector product took 0.0286521 seconds
Rank 28: Matrix-vector product took 0.0279942 seconds
Rank 29: Matrix-vector product took 0.0272676 seconds
Rank 30: Matrix-vector product took 0.0289482 seconds
Rank 31: Matrix-vector product took 0.0275894 seconds
Average time for Matrix-vector product is 0.0286668 seconds

Rank 0: copyOwnerToAll took 0.000672099 seconds
Rank 1: copyOwnerToAll took 0.000672099 seconds
Rank 2: copyOwnerToAll took 0.000672099 seconds
Rank 3: copyOwnerToAll took 0.000672099 seconds
Rank 4: copyOwnerToAll took 0.000672099 seconds
Rank 5: copyOwnerToAll took 0.000672099 seconds
Rank 6: copyOwnerToAll took 0.000672099 seconds
Rank 7: copyOwnerToAll took 0.000672099 seconds
Rank 8: copyOwnerToAll took 0.000672099 seconds
Rank 9: copyOwnerToAll took 0.000672099 seconds
Rank 10: copyOwnerToAll took 0.000672099 seconds
Rank 11: copyOwnerToAll took 0.000672099 seconds
Rank 12: copyOwnerToAll took 0.000672099 seconds
Rank 13: copyOwnerToAll took 0.000672099 seconds
Rank 14: copyOwnerToAll took 0.000672099 seconds
Rank 15: copyOwnerToAll took 0.000672099 seconds
Rank 16: copyOwnerToAll took 0.000672099 seconds
Rank 17: copyOwnerToAll took 0.000672099 seconds
Rank 18: copyOwnerToAll took 0.000672099 seconds
Rank 19: copyOwnerToAll took 0.000672099 seconds
Rank 20: copyOwnerToAll took 0.000672099 seconds
Rank 21: copyOwnerToAll took 0.000672099 seconds
Rank 22: copyOwnerToAll took 0.000672099 seconds
Rank 23: copyOwnerToAll took 0.000672099 seconds
Rank 24: copyOwnerToAll took 0.000672099 seconds
Rank 25: copyOwnerToAll took 0.000672099 seconds
Rank 26: copyOwnerToAll took 0.000672099 seconds
Rank 27: copyOwnerToAll took 0.000672099 seconds
Rank 28: copyOwnerToAll took 0.000672099 seconds
Rank 29: copyOwnerToAll took 0.000672099 seconds
Rank 30: copyOwnerToAll took 0.000672099 seconds
Rank 31: copyOwnerToAll took 0.000672099 seconds
Average time for copyOwnertoAll is 0.000713182 seconds
