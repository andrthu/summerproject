Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 500003
Cell on rank 1 before loadbalancing: 499997
Edge-cut: 12571
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4274	2547	2780	0	2061	0	0	
From rank 1 to: 	4275	0	0	0	0	2735	0	0	
From rank 2 to: 	2548	0	0	4422	0	0	0	0	
From rank 3 to: 	2855	0	4373	0	0	609	2686	2609	
From rank 4 to: 	0	0	0	0	0	4105	0	2962	
From rank 5 to: 	2059	2735	0	629	4102	0	0	2315	
From rank 6 to: 	0	0	0	2685	0	0	0	4350	
From rank 7 to: 	0	0	0	2619	2962	2262	4348	0	
loadb
After loadbalancing process 6 has 132037 cells.
After loadbalancing process 2 has 131968 cells.
After loadbalancing process 4 has 132067 cells.
After loadbalancing process 1 has 132014 cells.
After loadbalancing process 3 has 138129 cells.
After loadbalancing process 5 has 136837 cells.
After loadbalancing process 0 has 136666 cells.
After loadbalancing process 7 has 137189 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4274	2547	2780	0	2061	0	0	
Rank 1's ghost cells:	4275	0	0	0	0	2735	0	0	
Rank 2's ghost cells:	2548	0	0	4422	0	0	0	0	
Rank 3's ghost cells:	2855	0	4373	0	0	609	2686	2609	
Rank 4's ghost cells:	0	0	0	0	0	4105	0	2962	
Rank 5's ghost cells:	2059	2735	0	629	4102	0	0	2315	
Rank 6's ghost cells:	0	0	0	2685	0	0	0	4350	
Rank 7's ghost cells:	0	0	0	2619	2962	2262	4348	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.8818           0.2157
    2          31.1027         0.577239
    3          21.9294         0.705065
    4          18.2099         0.830386
    5           16.803         0.922743
    6          14.1154         0.840049
    7          11.5343         0.817145
    8          11.0118         0.954697
    9          10.1182         0.918849
   10          8.37795         0.828011
   11          7.97213         0.951561
   12          7.87624         0.987972
   13          6.63379         0.842253
   14          6.18364         0.932143
   15           6.2947          1.01796
   16          5.55844         0.883034
   17          5.07167         0.912427
   18          5.17093          1.01957
   19          4.72993         0.914715
   20          4.33591         0.916696
   21          4.38478          1.01127
   22          4.03823         0.920966
   23          3.75599         0.930109
   24          3.77638          1.00543
   25          3.45545         0.915017
   26          3.17364         0.918445
   27          3.20719          1.01057
   28          2.96534         0.924592
   29          2.76254         0.931608
   30          2.84722          1.03065
   31          2.79065         0.980133
   32          2.82157          1.01108
   33          3.09786          1.09792
   34          3.05272         0.985429
   35           2.6748         0.876203
   36          2.21673         0.828746
   37          1.69406         0.764216
   38          1.38658         0.818494
   39          1.25056         0.901906
   40          1.08821         0.870177
   41         0.988298         0.908185
   42         0.891655         0.902213
   43         0.704554         0.790164
   44         0.592575         0.841065
   45         0.515377         0.869725
   46         0.400029         0.776186
   47         0.334212         0.835469
   48         0.290101         0.868015
   49         0.234283         0.807593
   50         0.203162         0.867162
   51         0.175237          0.86255
   52         0.142375         0.812473
   53         0.121403         0.852694
   54        0.0987969         0.813795
   55        0.0819642         0.829623
   56        0.0677273         0.826304
   57         0.055071         0.813129
   58        0.0438771         0.796737
   59        0.0352199         0.802695
   60        0.0276784         0.785875
   61        0.0205196         0.741357
=== rate=0.857089, T=18.4982, TIT=0.30325, IT=61

 Elapsed time: 18.4982
Rank 0: Matrix-vector product took 0.111456 seconds
Rank 1: Matrix-vector product took 0.107181 seconds
Rank 2: Matrix-vector product took 0.107564 seconds
Rank 3: Matrix-vector product took 0.112021 seconds
Rank 4: Matrix-vector product took 0.107236 seconds
Rank 5: Matrix-vector product took 0.11111 seconds
Rank 6: Matrix-vector product took 0.107406 seconds
Rank 7: Matrix-vector product took 0.111225 seconds
Average time for Matrix-vector product is 0.1094 seconds

Rank 0: copyOwnerToAll took 0.000859799 seconds
Rank 1: copyOwnerToAll took 0.000859799 seconds
Rank 2: copyOwnerToAll took 0.000859799 seconds
Rank 3: copyOwnerToAll took 0.000859799 seconds
Rank 4: copyOwnerToAll took 0.000859799 seconds
Rank 5: copyOwnerToAll took 0.000859799 seconds
Rank 6: copyOwnerToAll took 0.000859799 seconds
Rank 7: copyOwnerToAll took 0.000859799 seconds
Average time for copyOwnertoAll is 0.000872445 seconds
Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 500003
Cell on rank 1 before loadbalancing: 499997
Edge-cut: 12571
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4274	2547	2780	0	2061	0	0	
From rank 1 to: 	4275	0	0	0	0	2735	0	0	
From rank 2 to: 	2548	0	0	4422	0	0	0	0	
From rank 3 to: 	2855	0	4373	0	0	609	2686	2609	
From rank 4 to: 	0	0	0	0	0	4105	0	2962	
From rank 5 to: 	2059	2735	0	629	4102	0	0	2315	
From rank 6 to: 	0	0	0	2685	0	0	0	4350	
From rank 7 to: 	0	0	0	2619	2962	2262	4348	0	
loadb
After loadbalancing process 2 has 131968 cells.
After loadbalancing process 1 has 132014 cells.
After loadbalancing process 6 has 132037 cells.
After loadbalancing process 0 has 136666 cells.
After loadbalancing process 4 has 132067 cells.
After loadbalancing process 3 has 138129 cells.
After loadbalancing process 7 has 137189 cells.
After loadbalancing process 5 has 136837 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4274	2547	2780	0	2061	0	0	
Rank 1's ghost cells:	4275	0	0	0	0	2735	0	0	
Rank 2's ghost cells:	2548	0	0	4422	0	0	0	0	
Rank 3's ghost cells:	2855	0	4373	0	0	609	2686	2609	
Rank 4's ghost cells:	0	0	0	0	0	4105	0	2962	
Rank 5's ghost cells:	2059	2735	0	629	4102	0	0	2315	
Rank 6's ghost cells:	0	0	0	2685	0	0	0	4350	
Rank 7's ghost cells:	0	0	0	2619	2962	2262	4348	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.8818           0.2157
    2          31.1027         0.577239
    3          21.9294         0.705065
    4          18.2099         0.830386
    5           16.803         0.922743
    6          14.1154         0.840049
    7          11.5343         0.817145
    8          11.0118         0.954697
    9          10.1182         0.918849
   10          8.37795         0.828011
   11          7.97213         0.951561
   12          7.87624         0.987972
   13          6.63379         0.842253
   14          6.18364         0.932143
   15           6.2947          1.01796
   16          5.55844         0.883034
   17          5.07167         0.912427
   18          5.17093          1.01957
   19          4.72993         0.914715
   20          4.33591         0.916696
   21          4.38478          1.01127
   22          4.03823         0.920966
   23          3.75599         0.930109
   24          3.77638          1.00543
   25          3.45545         0.915017
   26          3.17364         0.918445
   27          3.20719          1.01057
   28          2.96534         0.924592
   29          2.76254         0.931608
   30          2.84722          1.03065
   31          2.79065         0.980133
   32          2.82157          1.01108
   33          3.09786          1.09792
   34          3.05272         0.985429
   35           2.6748         0.876203
   36          2.21673         0.828746
   37          1.69406         0.764216
   38          1.38658         0.818494
   39          1.25056         0.901906
   40          1.08821         0.870177
   41         0.988298         0.908185
   42         0.891655         0.902213
   43         0.704554         0.790164
   44         0.592575         0.841065
   45         0.515377         0.869725
   46         0.400029         0.776186
   47         0.334212         0.835469
   48         0.290101         0.868015
   49         0.234283         0.807593
   50         0.203162         0.867162
   51         0.175237          0.86255
   52         0.142375         0.812473
   53         0.121403         0.852694
   54        0.0987969         0.813795
   55        0.0819642         0.829623
   56        0.0677273         0.826304
   57         0.055071         0.813129
   58        0.0438771         0.796737
   59        0.0352199         0.802695
   60        0.0276784         0.785875
   61        0.0205196         0.741357
=== rate=0.857089, T=18.6099, TIT=0.305081, IT=61

 Elapsed time: 18.6099
Rank 0: Matrix-vector product took 0.111133 seconds
Rank 1: Matrix-vector product took 0.107234 seconds
Rank 2: Matrix-vector product took 0.107177 seconds
Rank 3: Matrix-vector product took 0.111789 seconds
Rank 4: Matrix-vector product took 0.108604 seconds
Rank 5: Matrix-vector product took 0.112031 seconds
Rank 6: Matrix-vector product took 0.107077 seconds
Rank 7: Matrix-vector product took 0.111276 seconds
Average time for Matrix-vector product is 0.10954 seconds

Rank 0: copyOwnerToAll took 0.000898079 seconds
Rank 1: copyOwnerToAll took 0.000898079 seconds
Rank 2: copyOwnerToAll took 0.000898079 seconds
Rank 3: copyOwnerToAll took 0.000898079 seconds
Rank 4: copyOwnerToAll took 0.000898079 seconds
Rank 5: copyOwnerToAll took 0.000898079 seconds
Rank 6: copyOwnerToAll took 0.000898079 seconds
Rank 7: copyOwnerToAll took 0.000898079 seconds
Average time for copyOwnertoAll is 0.000913552 seconds
