Number of cores/ranks per node is: 2
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 25009
Cell on rank 1 before loadbalancing: 25005
Cell on rank 2 before loadbalancing: 24980
Cell on rank 3 before loadbalancing: 25006
Edge-cut: 2729
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	500	0	210	0	0	0	0	
From rank 1 to: 	500	0	191	70	0	0	220	200	
From rank 2 to: 	0	201	0	515	570	0	84	0	
From rank 3 to: 	210	70	520	0	0	0	0	0	
From rank 4 to: 	0	0	570	0	0	470	310	0	
From rank 5 to: 	0	0	0	0	470	0	230	0	
From rank 6 to: 	0	230	80	0	310	230	0	480	
From rank 7 to: 	0	200	0	0	0	0	490	0	
loadb
After loadbalancing process 4 has 13840 cells.
After loadbalancing process 5 has 13190 cells.
After loadbalancing process 3 has 13298 cells.
After loadbalancing process 2 has 13877 cells.
After loadbalancing process 0 has 13248 cells.
After loadbalancing process 6 has 13836 cells.
After loadbalancing process 7 has 13190 cells.
After loadbalancing process 1 has 13652 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	500	0	210	0	0	0	0	
Rank 1's ghost cells:	500	0	191	70	0	0	220	200	
Rank 2's ghost cells:	0	201	0	515	570	0	84	0	
Rank 3's ghost cells:	210	70	520	0	0	0	0	0	
Rank 4's ghost cells:	0	0	570	0	0	470	310	0	
Rank 5's ghost cells:	0	0	0	0	470	0	230	0	
Rank 6's ghost cells:	0	230	80	0	310	230	0	480	
Rank 7's ghost cells:	0	200	0	0	0	0	490	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.3154         0.220377
    2          21.2511         0.601752
    3          20.3236         0.956352
    4          14.8936         0.732823
    5          7.37967         0.495494
    6           4.6989         0.636735
    7          2.83801         0.603975
    8          1.96582         0.692675
    9          1.13557         0.577657
   10         0.721019         0.634939
   11         0.573967          0.79605
   12         0.263849         0.459694
   13         0.193324         0.732707
   14         0.129174         0.668172
   15        0.0659357         0.510442
   16        0.0493437         0.748361
   17        0.0330467         0.669724
   18        0.0181432         0.549018
   19        0.0137501         0.757863
=== rate=0.610906, T=0.602147, TIT=0.031692, IT=19

 Elapsed time: 0.602147
Rank 0: Matrix-vector product took 0.0106952 seconds
Rank 1: Matrix-vector product took 0.0107866 seconds
Rank 2: Matrix-vector product took 0.0112213 seconds
Rank 3: Matrix-vector product took 0.0105242 seconds
Rank 4: Matrix-vector product took 0.011213 seconds
Rank 5: Matrix-vector product took 0.0104576 seconds
Rank 6: Matrix-vector product took 0.0112032 seconds
Rank 7: Matrix-vector product took 0.0104624 seconds
Average time for Matrix-vector product is 0.0108204 seconds

Rank 0: copyOwnerToAll took 0.00019815 seconds
Rank 1: copyOwnerToAll took 0.00019815 seconds
Rank 2: copyOwnerToAll took 0.00019815 seconds
Rank 3: copyOwnerToAll took 0.00019815 seconds
Rank 4: copyOwnerToAll took 0.00019815 seconds
Rank 5: copyOwnerToAll took 0.00019815 seconds
Rank 6: copyOwnerToAll took 0.00019815 seconds
Rank 7: copyOwnerToAll took 0.00019815 seconds
Average time for copyOwnertoAll is 0.000250122 seconds
