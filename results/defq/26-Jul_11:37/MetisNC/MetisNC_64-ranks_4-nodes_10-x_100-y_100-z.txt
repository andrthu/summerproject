Number of cores/ranks per node is: 16
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 25009
Cell on rank 1 before loadbalancing: 25005
Cell on rank 2 before loadbalancing: 24980
Cell on rank 3 before loadbalancing: 25006
Edge-cut: 2729
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	100	120	0	60	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	
From rank 1 to: 	100	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	90	0	0	0	0	0	0	0	0	
From rank 2 to: 	110	100	0	130	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	130	0	100	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	60	0	90	100	0	150	0	70	0	0	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	150	0	85	100	0	0	0	70	0	0	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	85	0	103	0	0	0	0	0	140	25	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	115	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	50	0	0	0	70	90	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	110	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	110	0	10	100	0	0	60	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	100	10	0	0	0	140	20	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	80	70	0	0	0	100	150	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	110	0	0	0	0	0	0	0	0	10	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	140	0	0	0	0	0	100	0	60	130	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	75	25	0	0	70	0	100	0	60	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	70	0	0	110	130	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	120	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	25	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	90	0	0	0	80	0	0	0	110	0	50	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	80	0	200	0	0	30	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	90	0	0	0	30	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	30	0	90	30	0	160	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	70	0	80	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	30	0	0	0	0	0	10	110	0	0	0	0	0	0	0	0	0	0	0	120	4	58	80	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	130	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	30	0	140	0	4	0	0	130	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	60	160	122	0	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	72	0	72	16	0	160	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	50	0	0	0	0	0	0	0	0	0	0	160	0	10	80	0	0	0	0	0	0	0	0	0	0	140	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	160	10	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	30	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	100	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	84	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	100	0	0	0	110	20	70	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	50	100	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	40	0	80	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	80	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	90	0	0	0	0	0	123	0	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	82	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	80	82	0	127	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	120	0	0	113	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	110	0	0	0	0	0	0	0	200	0	0	0	0	0	90	0	0	0	30	0	0	0	0	74	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	75	110	0	0	70	30	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	75	0	145	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	90	0	0	0	0	0	0	0	0	0	120	140	0	0	0	0	0	0	50	0	90	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	123	0	0	0	0	0	0	0	0	143	0	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	25	0	85	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	146	0	130	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	90	0	0	130	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	55	0	90	0	90	30	0	0	32	88	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	100	0	0	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	70	0	0	0	20	120	0	0	0	0	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	160	0	50	100	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	100	0	0	100	0	0	100	0	60	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	40	0	100	0	0	0	0	0	110	90	0	0	0	0	0	30	70	0	0	0	0	0	0	
From rank 52 to: 	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	50	88	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	90	0	0	0	0	0	0	200	0	30	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	100	0	50	80	0	120	0	0	0	30	0	0	0	90	
From rank 55 to: 	70	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	98	0	112	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	80	0	0	0	0	84	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	153	53	42	0	0	0	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	157	0	0	146	0	0	0	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	47	0	0	119	0	0	114	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	40	0	42	136	113	0	0	0	50	116	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	100	60	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	0	180	0	0	60	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	50	100	0	0	90	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	90	0	0	0	0	116	70	60	94	0	
loadb
After loadbalancing process 8 has 1810 cells.
After loadbalancing process 10 has 1998 cells.
After loadbalancing process 37 has 1836 cells.
After loadbalancing process 3 has 1882 cells.
After loadbalancing process 39 has 1956 cells.
After loadbalancing process 9 has 1910 cells.
After loadbalancing process 38 has 2048 cells.
After loadbalancing process 21 has 1800 cells.
After loadbalancing process 20 has 1960 cells.
After loadbalancing process 60 has 1910 cells.
After loadbalancing process 63 has 2040 cells.
After loadbalancing process 4 has 2140 cells.
After loadbalancing process 36 has 1935 cells.
After loadbalancing process 34 has 1890 cells.
After loadbalancing process 11 has 2070 cells.
After loadbalancing process 5 has 2056 cells.
After loadbalancing process 62 has 1914 cells.
After loadbalancing process 61 has 1990 cells.
After loadbalancing process 2 has 1990 cells.
After loadbalancing process 15 has 1990 cells.
After loadbalancing process 53 has 2020 cells.
After loadbalancing process 32 has 2050 cells.
After loadbalancing process 23 has 1900 cells.
After loadbalancing process 27 has 2054 cells.
After loadbalancing process 17 has 2070 cells.
After loadbalancing process 44 has 1942 cells.
After loadbalancing process 22 has 2100 cells.
After loadbalancing process 25 has 1970 cells.
After loadbalancing process 30 has 2070 cells.
After loadbalancing process 24 has 2086 cells.
After loadbalancing process 14 has 2020 cells.
After loadbalancing process 26 has 2034 cells.
After loadbalancing process 28 has 2080 cells.
After loadbalancing process 52 has 1916 cells.
After loadbalancing process 0 has 2070 cells.
After loadbalancing process 55 has 2050 cells.
After loadbalancing process 57 has 2030 cells.
After loadbalancing process 58 has 1930 cells.
After loadbalancing process 54 has 2052 cells.
After loadbalancing process 41 has 2099 cells.
After loadbalancing process 47 has 2081 cells.
After loadbalancing process 48 has 2080 cells.
After loadbalancing process 7 has 2096 cells.
After loadbalancing process 6 has 2105 cells.
After loadbalancing process 42 has 2080 cells.
After loadbalancing process 1 has 1910 cells.
After loadbalancing process 50 has 2040 cells.
After loadbalancing process 12 has 1890 cells.
After loadbalancing process 33 has 2048 cells.
After loadbalancing process 59 has 2118 cells.
After loadbalancing process 46 has 2050 cells.
After loadbalancing process 51 has 2040 cells.
After loadbalancing process 13 has 2100 cells.
After loadbalancing process 19 has 1910 cells.
After loadbalancing process 49 has 2076 cells.
After loadbalancing process 31 has 2030 cells.
After loadbalancing process 16 has 2073 cells.
After loadbalancing process 29 has 2090 cells.
After loadbalancing process 56 has 2100 cells.
After loadbalancing process 35 has 1890 cells.
After loadbalancing process 40 has 2139 cells.
After loadbalancing process 45 has 2028 cells.
After loadbalancing process 43 has 2069 cells.
After loadbalancing process 18 has 2107 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	100	120	0	60	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	100	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	90	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	110	100	0	130	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	130	0	100	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	60	0	90	100	0	150	0	70	0	0	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	150	0	85	100	0	0	0	70	0	0	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	85	0	103	0	0	0	0	0	140	25	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	115	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	50	0	0	0	70	90	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	110	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	110	0	10	100	0	0	60	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	100	10	0	0	0	140	20	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	80	70	0	0	0	100	150	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	110	0	0	0	0	0	0	0	0	10	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	140	0	0	0	0	0	100	0	60	130	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	75	25	0	0	70	0	100	0	60	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	70	0	0	110	130	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	120	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	25	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	90	0	0	0	80	0	0	0	110	0	50	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	80	0	200	0	0	30	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	90	0	0	0	30	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	30	0	90	30	0	160	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	70	0	80	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	30	0	0	0	0	0	10	110	0	0	0	0	0	0	0	0	0	0	0	120	4	58	80	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	130	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	30	0	140	0	4	0	0	130	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	60	160	122	0	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	72	0	72	16	0	160	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	50	0	0	0	0	0	0	0	0	0	0	160	0	10	80	0	0	0	0	0	0	0	0	0	0	140	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	160	10	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	30	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	100	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	84	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	100	0	0	0	110	20	70	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	50	100	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	40	0	80	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	80	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	90	0	0	0	0	0	123	0	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	82	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	80	82	0	127	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	120	0	0	113	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	110	0	0	0	0	0	0	0	200	0	0	0	0	0	90	0	0	0	30	0	0	0	0	74	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	75	110	0	0	70	30	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	75	0	145	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	90	0	0	0	0	0	0	0	0	0	120	140	0	0	0	0	0	0	50	0	90	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	123	0	0	0	0	0	0	0	0	143	0	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	25	0	85	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	146	0	130	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	90	0	0	130	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	55	0	90	0	90	30	0	0	32	88	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	100	0	0	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	70	0	0	0	20	120	0	0	0	0	0	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	160	0	50	100	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	100	0	0	100	0	0	100	0	60	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	40	0	100	0	0	0	0	0	110	90	0	0	0	0	0	30	70	0	0	0	0	0	0	
Rank 52's ghost cells:	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	50	88	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	90	0	0	0	0	0	0	200	0	30	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	100	0	50	80	0	120	0	0	0	30	0	0	0	90	
Rank 55's ghost cells:	70	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	98	0	112	0	0	0	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	80	0	0	0	0	84	0	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	153	53	42	0	0	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	80	0	0	0	0	157	0	0	146	0	0	0	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	47	0	0	119	0	0	114	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	40	0	42	136	113	0	0	0	50	116	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	100	60	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	0	180	0	0	60	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	50	100	0	0	90	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	90	0	0	0	0	116	70	60	94	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          40.3265         0.251648
    2          24.6316         0.610805
    3          22.1826         0.900576
    4          21.6423         0.975642
    5          11.2654         0.520525
    6          5.64624         0.501204
    7          4.35458         0.771236
    8          2.62563         0.602957
    9          1.31987         0.502688
   10         0.971811         0.736292
   11         0.658203         0.677296
   12         0.376043         0.571317
   13         0.261944          0.69658
   14         0.175982         0.671832
   15         0.117003         0.664857
   16        0.0715813         0.611789
   17        0.0492799         0.688447
   18          0.03163         0.641844
   19        0.0195161         0.617011
   20        0.0127361         0.652596
=== rate=0.623752, T=0.147527, TIT=0.00737635, IT=20

 Elapsed time: 0.147527
Rank 0: Matrix-vector product took 0.00164233 seconds
Rank 1: Matrix-vector product took 0.00146647 seconds
Rank 2: Matrix-vector product took 0.00153342 seconds
Rank 3: Matrix-vector product took 0.00144879 seconds
Rank 4: Matrix-vector product took 0.00165542 seconds
Rank 5: Matrix-vector product took 0.00158515 seconds
Rank 6: Matrix-vector product took 0.00161935 seconds
Rank 7: Matrix-vector product took 0.00161883 seconds
Rank 8: Matrix-vector product took 0.0014003 seconds
Rank 9: Matrix-vector product took 0.0014689 seconds
Rank 10: Matrix-vector product took 0.00153557 seconds
Rank 11: Matrix-vector product took 0.0016069 seconds
Rank 12: Matrix-vector product took 0.00144316 seconds
Rank 13: Matrix-vector product took 0.00161514 seconds
Rank 14: Matrix-vector product took 0.00156023 seconds
Rank 15: Matrix-vector product took 0.00152625 seconds
Rank 16: Matrix-vector product took 0.00161989 seconds
Rank 17: Matrix-vector product took 0.00161148 seconds
Rank 18: Matrix-vector product took 0.00163688 seconds
Rank 19: Matrix-vector product took 0.00147058 seconds
Rank 20: Matrix-vector product took 0.00151974 seconds
Rank 21: Matrix-vector product took 0.00140108 seconds
Rank 22: Matrix-vector product took 0.00161317 seconds
Rank 23: Matrix-vector product took 0.00146531 seconds
Rank 24: Matrix-vector product took 0.00161255 seconds
Rank 25: Matrix-vector product took 0.00152317 seconds
Rank 26: Matrix-vector product took 0.00157508 seconds
Rank 27: Matrix-vector product took 0.00158301 seconds
Rank 28: Matrix-vector product took 0.00159917 seconds
Rank 29: Matrix-vector product took 0.00162039 seconds
Rank 30: Matrix-vector product took 0.00159129 seconds
Rank 31: Matrix-vector product took 0.00156545 seconds
Rank 32: Matrix-vector product took 0.00159632 seconds
Rank 33: Matrix-vector product took 0.00160317 seconds
Rank 34: Matrix-vector product took 0.00146753 seconds
Rank 35: Matrix-vector product took 0.00145857 seconds
Rank 36: Matrix-vector product took 0.00149423 seconds
Rank 37: Matrix-vector product took 0.00140806 seconds
Rank 38: Matrix-vector product took 0.00159235 seconds
Rank 39: Matrix-vector product took 0.00151596 seconds
Rank 40: Matrix-vector product took 0.00164714 seconds
Rank 41: Matrix-vector product took 0.00162589 seconds
Rank 42: Matrix-vector product took 0.00161415 seconds
Rank 43: Matrix-vector product took 0.0015865 seconds
Rank 44: Matrix-vector product took 0.00149005 seconds
Rank 45: Matrix-vector product took 0.00155907 seconds
Rank 46: Matrix-vector product took 0.00158334 seconds
Rank 47: Matrix-vector product took 0.00164214 seconds
Rank 48: Matrix-vector product took 0.00163017 seconds
Rank 49: Matrix-vector product took 0.00161289 seconds
Rank 50: Matrix-vector product took 0.00156858 seconds
Rank 51: Matrix-vector product took 0.00157022 seconds
Rank 52: Matrix-vector product took 0.00147339 seconds
Rank 53: Matrix-vector product took 0.00154522 seconds
Rank 54: Matrix-vector product took 0.00161768 seconds
Rank 55: Matrix-vector product took 0.00160837 seconds
Rank 56: Matrix-vector product took 0.00161649 seconds
Rank 57: Matrix-vector product took 0.00156306 seconds
Rank 58: Matrix-vector product took 0.00147755 seconds
Rank 59: Matrix-vector product took 0.00162675 seconds
Rank 60: Matrix-vector product took 0.00147101 seconds
Rank 61: Matrix-vector product took 0.00151891 seconds
Rank 62: Matrix-vector product took 0.00151577 seconds
Rank 63: Matrix-vector product took 0.00162714 seconds
Average time for Matrix-vector product is 0.00155722 seconds

Rank 0: copyOwnerToAll took 0.000199931 seconds
Rank 1: copyOwnerToAll took 0.000199931 seconds
Rank 2: copyOwnerToAll took 0.000199931 seconds
Rank 3: copyOwnerToAll took 0.000199931 seconds
Rank 4: copyOwnerToAll took 0.000199931 seconds
Rank 5: copyOwnerToAll took 0.000199931 seconds
Rank 6: copyOwnerToAll took 0.000199931 seconds
Rank 7: copyOwnerToAll took 0.000199931 seconds
Rank 8: copyOwnerToAll took 0.000199931 seconds
Rank 9: copyOwnerToAll took 0.000199931 seconds
Rank 10: copyOwnerToAll took 0.000199931 seconds
Rank 11: copyOwnerToAll took 0.000199931 seconds
Rank 12: copyOwnerToAll took 0.000199931 seconds
Rank 13: copyOwnerToAll took 0.000199931 seconds
Rank 14: copyOwnerToAll took 0.000199931 seconds
Rank 15: copyOwnerToAll took 0.000199931 seconds
Rank 16: copyOwnerToAll took 0.000199931 seconds
Rank 17: copyOwnerToAll took 0.000199931 seconds
Rank 18: copyOwnerToAll took 0.000199931 seconds
Rank 19: copyOwnerToAll took 0.000199931 seconds
Rank 20: copyOwnerToAll took 0.000199931 seconds
Rank 21: copyOwnerToAll took 0.000199931 seconds
Rank 22: copyOwnerToAll took 0.000199931 seconds
Rank 23: copyOwnerToAll took 0.000199931 seconds
Rank 24: copyOwnerToAll took 0.000199931 seconds
Rank 25: copyOwnerToAll took 0.000199931 seconds
Rank 26: copyOwnerToAll took 0.000199931 seconds
Rank 27: copyOwnerToAll took 0.000199931 seconds
Rank 28: copyOwnerToAll took 0.000199931 seconds
Rank 29: copyOwnerToAll took 0.000199931 seconds
Rank 30: copyOwnerToAll took 0.000199931 seconds
Rank 31: copyOwnerToAll took 0.000199931 seconds
Rank 32: copyOwnerToAll took 0.000199931 seconds
Rank 33: copyOwnerToAll took 0.000199931 seconds
Rank 34: copyOwnerToAll took 0.000199931 seconds
Rank 35: copyOwnerToAll took 0.000199931 seconds
Rank 36: copyOwnerToAll took 0.000199931 seconds
Rank 37: copyOwnerToAll took 0.000199931 seconds
Rank 38: copyOwnerToAll took 0.000199931 seconds
Rank 39: copyOwnerToAll took 0.000199931 seconds
Rank 40: copyOwnerToAll took 0.000199931 seconds
Rank 41: copyOwnerToAll took 0.000199931 seconds
Rank 42: copyOwnerToAll took 0.000199931 seconds
Rank 43: copyOwnerToAll took 0.000199931 seconds
Rank 44: copyOwnerToAll took 0.000199931 seconds
Rank 45: copyOwnerToAll took 0.000199931 seconds
Rank 46: copyOwnerToAll took 0.000199931 seconds
Rank 47: copyOwnerToAll took 0.000199931 seconds
Rank 48: copyOwnerToAll took 0.000199931 seconds
Rank 49: copyOwnerToAll took 0.000199931 seconds
Rank 50: copyOwnerToAll took 0.000199931 seconds
Rank 51: copyOwnerToAll took 0.000199931 seconds
Rank 52: copyOwnerToAll took 0.000199931 seconds
Rank 53: copyOwnerToAll took 0.000199931 seconds
Rank 54: copyOwnerToAll took 0.000199931 seconds
Rank 55: copyOwnerToAll took 0.000199931 seconds
Rank 56: copyOwnerToAll took 0.000199931 seconds
Rank 57: copyOwnerToAll took 0.000199931 seconds
Rank 58: copyOwnerToAll took 0.000199931 seconds
Rank 59: copyOwnerToAll took 0.000199931 seconds
Rank 60: copyOwnerToAll took 0.000199931 seconds
Rank 61: copyOwnerToAll took 0.000199931 seconds
Rank 62: copyOwnerToAll took 0.000199931 seconds
Rank 63: copyOwnerToAll took 0.000199931 seconds
Average time for copyOwnertoAll is 0.000234182 seconds
