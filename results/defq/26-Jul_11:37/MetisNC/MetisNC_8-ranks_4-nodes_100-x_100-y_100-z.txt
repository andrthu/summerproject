Number of cores/ranks per node is: 2
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4715	2482	2548	2232	0	329	0	
From rank 1 to: 	4715	0	0	0	2731	0	0	0	
From rank 2 to: 	2489	0	0	4812	0	0	0	0	
From rank 3 to: 	2528	0	4812	0	100	0	2287	2519	
From rank 4 to: 	2270	2727	0	100	0	4679	2275	0	
From rank 5 to: 	0	0	0	0	4678	0	2633	0	
From rank 6 to: 	328	0	0	2279	2281	2628	0	4796	
From rank 7 to: 	0	0	0	2520	0	0	4797	0	
loadb
After loadbalancing process 2 has 132303 cells.
After loadbalancing process 1 has 132444 cells.
After loadbalancing process 5 has 132304 cells.
After loadbalancing process 7 has 132327 cells.
After loadbalancing process 4 has 137055 cells.
After loadbalancing process 3 has 137240 cells.
After loadbalancing process 0 has 137307 cells.
After loadbalancing process 6 has 137310 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4715	2482	2548	2232	0	329	0	
Rank 1's ghost cells:	4715	0	0	0	2731	0	0	0	
Rank 2's ghost cells:	2489	0	0	4812	0	0	0	0	
Rank 3's ghost cells:	2528	0	4812	0	100	0	2287	2519	
Rank 4's ghost cells:	2270	2727	0	100	0	4679	2275	0	
Rank 5's ghost cells:	0	0	0	0	4678	0	2633	0	
Rank 6's ghost cells:	328	0	0	2279	2281	2628	0	4796	
Rank 7's ghost cells:	0	0	0	2520	0	0	4797	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7147         0.215031
    2          30.9928         0.576989
    3          21.7144         0.700628
    4          17.6966         0.814969
    5          16.2034         0.915625
    6          14.0522         0.867235
    7          11.4589         0.815455
    8          10.7347         0.936797
    9          10.0456          0.93581
   10          8.28021         0.824261
   11          7.67645         0.927085
   12           7.8733          1.02564
   13           6.8261         0.866994
   14          6.05938         0.887679
   15          6.24886          1.03127
   16          5.70459           0.9129
   17          4.98095         0.873148
   18          5.04911          1.01369
   19          4.84242         0.959063
   20          4.28128          0.88412
   21          4.19785         0.980513
   22          4.04839         0.964397
   23          3.67342         0.907378
   24          3.59945         0.979862
   25          3.47175         0.964525
   26          3.12478         0.900058
   27          3.06724         0.981585
   28          2.97216         0.969002
   29          2.72136         0.915616
   30           2.7264          1.00185
   31          2.79016          1.02339
   32          2.82853          1.01375
   33          3.04876          1.07786
   34          3.10159          1.01733
   35          2.58569         0.833667
   36          1.99884          0.77304
   37          1.55357         0.777233
   38          1.25968         0.810833
   39          1.14446          0.90853
   40          1.06046         0.926603
   41         0.911914         0.859923
   42          0.76415         0.837963
   43         0.642808         0.841206
   44         0.559007         0.869634
   45          0.46491         0.831671
   46         0.375753         0.808227
   47         0.318972         0.848888
   48         0.262614         0.823313
   49         0.217918         0.829803
   50         0.189141         0.867946
   51          0.15585         0.823988
   52           0.1346         0.863652
   53         0.110933          0.82417
   54        0.0897209         0.808781
   55        0.0780688         0.870129
   56        0.0602546         0.771814
   57        0.0494155         0.820112
   58        0.0396409         0.802195
   59        0.0300407          0.75782
   60        0.0239644         0.797731
=== rate=0.857103, T=18.0913, TIT=0.301521, IT=60

 Elapsed time: 18.0913
Rank 0: Matrix-vector product took 0.111232 seconds
Rank 1: Matrix-vector product took 0.107305 seconds
Rank 2: Matrix-vector product took 0.107335 seconds
Rank 3: Matrix-vector product took 0.111886 seconds
Rank 4: Matrix-vector product took 0.111248 seconds
Rank 5: Matrix-vector product took 0.108169 seconds
Rank 6: Matrix-vector product took 0.111976 seconds
Rank 7: Matrix-vector product took 0.108426 seconds
Average time for Matrix-vector product is 0.109697 seconds

Rank 0: copyOwnerToAll took 0.000862659 seconds
Rank 1: copyOwnerToAll took 0.000862659 seconds
Rank 2: copyOwnerToAll took 0.000862659 seconds
Rank 3: copyOwnerToAll took 0.000862659 seconds
Rank 4: copyOwnerToAll took 0.000862659 seconds
Rank 5: copyOwnerToAll took 0.000862659 seconds
Rank 6: copyOwnerToAll took 0.000862659 seconds
Rank 7: copyOwnerToAll took 0.000862659 seconds
Average time for copyOwnertoAll is 0.000887357 seconds
Number of cores/ranks per node is: 2
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4715	2482	2548	2232	0	329	0	
From rank 1 to: 	4715	0	0	0	2731	0	0	0	
From rank 2 to: 	2489	0	0	4812	0	0	0	0	
From rank 3 to: 	2528	0	4812	0	100	0	2287	2519	
From rank 4 to: 	2270	2727	0	100	0	4679	2275	0	
From rank 5 to: 	0	0	0	0	4678	0	2633	0	
From rank 6 to: 	328	0	0	2279	2281	2628	0	4796	
From rank 7 to: 	0	0	0	2520	0	0	4797	0	
loadb
After loadbalancing process 2 has 132303 cells.
After loadbalancing process 5 has 132304 cells.
After loadbalancing process 7 has 132327 cells.
After loadbalancing process 4 has 137055 cells.
After loadbalancing process 1 has 132444 cells.
After loadbalancing process 6 has 137310 cells.
After loadbalancing process 3 has 137240 cells.
After loadbalancing process 0 has 137307 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4715	2482	2548	2232	0	329	0	
Rank 1's ghost cells:	4715	0	0	0	2731	0	0	0	
Rank 2's ghost cells:	2489	0	0	4812	0	0	0	0	
Rank 3's ghost cells:	2528	0	4812	0	100	0	2287	2519	
Rank 4's ghost cells:	2270	2727	0	100	0	4679	2275	0	
Rank 5's ghost cells:	0	0	0	0	4678	0	2633	0	
Rank 6's ghost cells:	328	0	0	2279	2281	2628	0	4796	
Rank 7's ghost cells:	0	0	0	2520	0	0	4797	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7147         0.215031
    2          30.9928         0.576989
    3          21.7144         0.700628
    4          17.6966         0.814969
    5          16.2034         0.915625
    6          14.0522         0.867235
    7          11.4589         0.815455
    8          10.7347         0.936797
    9          10.0456          0.93581
   10          8.28021         0.824261
   11          7.67645         0.927085
   12           7.8733          1.02564
   13           6.8261         0.866994
   14          6.05938         0.887679
   15          6.24886          1.03127
   16          5.70459           0.9129
   17          4.98095         0.873148
   18          5.04911          1.01369
   19          4.84242         0.959063
   20          4.28128          0.88412
   21          4.19785         0.980513
   22          4.04839         0.964397
   23          3.67342         0.907378
   24          3.59945         0.979862
   25          3.47175         0.964525
   26          3.12478         0.900058
   27          3.06724         0.981585
   28          2.97216         0.969002
   29          2.72136         0.915616
   30           2.7264          1.00185
   31          2.79016          1.02339
   32          2.82853          1.01375
   33          3.04876          1.07786
   34          3.10159          1.01733
   35          2.58569         0.833667
   36          1.99884          0.77304
   37          1.55357         0.777233
   38          1.25968         0.810833
   39          1.14446          0.90853
   40          1.06046         0.926603
   41         0.911914         0.859923
   42          0.76415         0.837963
   43         0.642808         0.841206
   44         0.559007         0.869634
   45          0.46491         0.831671
   46         0.375753         0.808227
   47         0.318972         0.848888
   48         0.262614         0.823313
   49         0.217918         0.829803
   50         0.189141         0.867946
   51          0.15585         0.823988
   52           0.1346         0.863652
   53         0.110933          0.82417
   54        0.0897209         0.808781
   55        0.0780688         0.870129
   56        0.0602546         0.771814
   57        0.0494155         0.820112
   58        0.0396409         0.802195
   59        0.0300407          0.75782
   60        0.0239644         0.797731
=== rate=0.857103, T=18.1762, TIT=0.302936, IT=60

 Elapsed time: 18.1762
Rank 0: Matrix-vector product took 0.1125 seconds
Rank 1: Matrix-vector product took 0.107533 seconds
Rank 2: Matrix-vector product took 0.107253 seconds
Rank 3: Matrix-vector product took 0.111255 seconds
Rank 4: Matrix-vector product took 0.111337 seconds
Rank 5: Matrix-vector product took 0.107342 seconds
Rank 6: Matrix-vector product took 0.111471 seconds
Rank 7: Matrix-vector product took 0.107463 seconds
Average time for Matrix-vector product is 0.109519 seconds

max verdi: 0.000963428
max verdi: 0.000909289
max verdi: 0.000899619
max verdi: 0.000917848
max verdi: 0.000912292
max verdi: 0.000889459
max verdi: 0.000884048
max verdi: 0.000906849
max verdi: 0.000922158
max verdi: 0.000899029
Rank 0: copyOwnerToAll took 0.000884048 seconds
Rank 1: copyOwnerToAll took 0.000884048 seconds
Rank 2: copyOwnerToAll took 0.000884048 seconds
Rank 3: copyOwnerToAll took 0.000884048 seconds
Rank 4: copyOwnerToAll took 0.000884048 seconds
Rank 5: copyOwnerToAll took 0.000884048 seconds
Rank 6: copyOwnerToAll took 0.000884048 seconds
Rank 7: copyOwnerToAll took 0.000884048 seconds
Average time for copyOwnertoAll is 0.000910402 seconds
