Number of cores/ranks per node is: 16
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 500003
Cell on rank 1 before loadbalancing: 499997
Edge-cut: 12571
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	838	851	0	0	0	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	0	0	1217	225	
From rank 1 to: 	835	0	133	1390	0	0	1147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	49	0	0	0	0	90	1225	
From rank 2 to: 	853	131	0	807	238	743	398	0	0	0	0	0	97	227	264	1029	0	0	0	0	138	149	0	0	973	4	0	0	0	0	0	0	
From rank 3 to: 	0	1375	815	0	913	0	142	0	0	0	0	0	0	0	926	0	0	0	0	0	352	0	0	0	58	1307	0	0	0	0	0	209	
From rank 4 to: 	0	0	256	913	0	514	702	996	0	0	0	0	0	1063	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	764	0	517	0	531	1147	0	0	0	0	1012	420	0	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	1189	1133	395	138	722	567	0	1505	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	988	1147	1525	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	600	983	854	140	705	229	229	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	599	0	829	404	907	0	0	333	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	1029	865	0	1957	0	0	0	0	0	0	0	0	0	0	555	514	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	856	404	1971	0	0	0	1189	776	0	0	0	0	0	128	909	699	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	100	0	0	1026	0	0	141	909	0	0	0	543	0	1197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	227	0	1093	422	0	0	704	0	0	0	543	0	964	531	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	262	942	205	0	0	0	235	0	0	1187	0	971	0	751	0	0	0	0	879	67	1	66	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	1033	0	0	53	0	0	223	334	0	786	1213	538	751	0	0	0	0	0	4	880	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1605	765	1206	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1598	0	1034	423	200	291	1066	1387	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	765	1001	0	586	265	751	0	0	317	0	1175	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1216	432	581	0	496	0	0	82	0	0	137	1169	0	0	0	0	
From rank 20 to: 	0	0	146	350	0	0	0	0	0	0	0	0	0	0	877	4	0	200	257	513	0	595	113	943	367	1191	60	260	0	0	0	0	
From rank 21 to: 	0	0	161	0	0	0	0	0	0	0	0	128	0	0	73	880	0	286	751	0	587	0	1312	104	1116	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	555	899	0	0	1	0	0	1066	0	0	114	1309	0	743	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	514	699	0	0	88	0	0	1374	0	82	969	104	732	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	121	2	972	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	0	380	1111	0	0	0	571	1374	9	0	0	781	0	
From rank 25 to: 	0	40	4	1318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1205	0	0	0	572	0	267	995	33	87	293	1218	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1176	140	60	0	0	0	1355	267	0	630	44	766	103	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1169	260	0	0	0	6	988	630	0	1042	79	0	42	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	47	1062	0	666	30	1294	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	766	75	665	0	1578	304	
From rank 30 to: 	1214	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	803	304	100	0	30	1589	0	645	
From rank 31 to: 	218	1224	0	209	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1199	0	42	1289	307	637	0	
loadb
After loadbalancing process 16 has 34828 cells.
After loadbalancing process 7 has 34915 cells.
After loadbalancing process 28 has 34380 cells.
After loadbalancing process 29 has 34737 cells.
After loadbalancing process 4 has 35897 cells.
After loadbalancing process 10 has 36170 cells.
After loadbalancing process 9 has 34327 cells.
After loadbalancing process 8 has 34990 cells.
After loadbalancing process 31 has 36376 cells.
After loadbalancing process 13 has 35732 cells.
After loadbalancing process 5 has 35698 cells.
After loadbalancing process 6 has 36895 cells.
After loadbalancing process 0 has 35710 cells.
After loadbalancing process 30 has 36014 cells.
After loadbalancing process 11 has 38177 cells.
After loadbalancing process 27 has 35459 cells.
After loadbalancing process 19 has 35358 cells.
After loadbalancing process 17 has 37242 cells.
After loadbalancing process 26 has 35795 cells.
After loadbalancing process 12 has 35163 cells.
After loadbalancing process 22 has 35947 cells.
After loadbalancing process 3 has 37340 cells.
After loadbalancing process 2 has 37299 cells.
After loadbalancing process 1 has 36117 cells.
After loadbalancing process 21 has 36657 cells.
After loadbalancing process 24 has 36947 cells.
After loadbalancing process 14 has 36825 cells.
After loadbalancing process 23 has 35809 cells.
After loadbalancing process 15 has 37059 cells.
After loadbalancing process 25 has 37282 cells.
After loadbalancing process 20 has 37125 cells.
After loadbalancing process 18 has 36106 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	838	851	0	0	0	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	0	0	1217	225	
Rank 1's ghost cells:	835	0	133	1390	0	0	1147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	49	0	0	0	0	90	1225	
Rank 2's ghost cells:	853	131	0	807	238	743	398	0	0	0	0	0	97	227	264	1029	0	0	0	0	138	149	0	0	973	4	0	0	0	0	0	0	
Rank 3's ghost cells:	0	1375	815	0	913	0	142	0	0	0	0	0	0	0	926	0	0	0	0	0	352	0	0	0	58	1307	0	0	0	0	0	209	
Rank 4's ghost cells:	0	0	256	913	0	514	702	996	0	0	0	0	0	1063	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	764	0	517	0	531	1147	0	0	0	0	1012	420	0	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	1189	1133	395	138	722	567	0	1505	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	988	1147	1525	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	600	983	854	140	705	229	229	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	599	0	829	404	907	0	0	333	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	1029	865	0	1957	0	0	0	0	0	0	0	0	0	0	555	514	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	856	404	1971	0	0	0	1189	776	0	0	0	0	0	128	909	699	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	100	0	0	1026	0	0	141	909	0	0	0	543	0	1197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	227	0	1093	422	0	0	704	0	0	0	543	0	964	531	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	262	942	205	0	0	0	235	0	0	1187	0	971	0	751	0	0	0	0	879	67	1	66	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	1033	0	0	53	0	0	223	334	0	786	1213	538	751	0	0	0	0	0	4	880	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1605	765	1206	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1598	0	1034	423	200	291	1066	1387	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	765	1001	0	586	265	751	0	0	317	0	1175	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1216	432	581	0	496	0	0	82	0	0	137	1169	0	0	0	0	
Rank 20's ghost cells:	0	0	146	350	0	0	0	0	0	0	0	0	0	0	877	4	0	200	257	513	0	595	113	943	367	1191	60	260	0	0	0	0	
Rank 21's ghost cells:	0	0	161	0	0	0	0	0	0	0	0	128	0	0	73	880	0	286	751	0	587	0	1312	104	1116	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	555	899	0	0	1	0	0	1066	0	0	114	1309	0	743	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	514	699	0	0	88	0	0	1374	0	82	969	104	732	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	121	2	972	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	0	380	1111	0	0	0	571	1374	9	0	0	781	0	
Rank 25's ghost cells:	0	40	4	1318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1205	0	0	0	572	0	267	995	33	87	293	1218	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1176	140	60	0	0	0	1355	267	0	630	44	766	103	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1169	260	0	0	0	6	988	630	0	1042	79	0	42	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	47	1062	0	666	30	1294	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	766	75	665	0	1578	304	
Rank 30's ghost cells:	1214	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	803	304	100	0	30	1589	0	645	
Rank 31's ghost cells:	218	1224	0	209	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1199	0	42	1289	307	637	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          56.0599         0.224419
    2          32.7469         0.584141
    3          23.2217         0.709126
    4          19.9933         0.860978
    5          17.8311         0.891851
    6          14.3608         0.805381
    7          12.5456         0.873597
    8          12.0967         0.964221
    9           10.159         0.839815
   10          9.05802         0.891627
   11          8.99103         0.992604
   12          7.91137         0.879919
   13          6.94441         0.877776
   14          7.02636           1.0118
   15          6.52103          0.92808
   16          5.79454         0.888594
   17          5.71986         0.987112
   18          5.39325         0.942898
   19           4.8709         0.903147
   20          4.80513         0.986498
   21          4.54486         0.945835
   22          4.16959          0.91743
   23          4.15714         0.997015
   24           3.9001         0.938169
   25           3.5768         0.917104
   26          3.55597         0.994177
   27          3.36837         0.947243
   28          3.08362         0.915463
   29          3.06773         0.994848
   30           2.9473         0.960742
   31          2.82306         0.957847
   32          2.90628          1.02948
   33          2.95668          1.01734
   34          3.02719          1.02384
   35          3.15651          1.04272
   36           2.8939         0.916801
   37          2.38698         0.824833
   38          1.91731         0.803236
   39          1.50532         0.785121
   40          1.26703         0.841703
   41          1.16509         0.919542
   42          1.04742         0.899007
   43         0.927301         0.885316
   44         0.792518          0.85465
   45         0.660575         0.833515
   46         0.573394         0.868023
   47         0.497601         0.867816
   48         0.406383         0.816685
   49         0.338331         0.832542
   50         0.282566         0.835177
   51         0.238531         0.844158
   52         0.206824         0.867073
   53         0.175942         0.850689
   54         0.145606         0.827578
   55          0.12379         0.850167
   56         0.102848         0.830828
   57        0.0836158         0.813005
   58        0.0695053         0.831246
   59        0.0559034         0.804305
   60        0.0453745         0.811658
   61         0.036831         0.811713
   62        0.0290095         0.787636
   63        0.0230101         0.793193
=== rate=0.862863, T=5.40716, TIT=0.0858279, IT=63

 Elapsed time: 5.40716
Rank 0: Matrix-vector product took 0.028795 seconds
Rank 1: Matrix-vector product took 0.029168 seconds
Rank 2: Matrix-vector product took 0.0300302 seconds
Rank 3: Matrix-vector product took 0.0298967 seconds
Rank 4: Matrix-vector product took 0.0288597 seconds
Rank 5: Matrix-vector product took 0.0290062 seconds
Rank 6: Matrix-vector product took 0.0294431 seconds
Rank 7: Matrix-vector product took 0.0279579 seconds
Rank 8: Matrix-vector product took 0.0280639 seconds
Rank 9: Matrix-vector product took 0.0275956 seconds
Rank 10: Matrix-vector product took 0.028839 seconds
Rank 11: Matrix-vector product took 0.0305105 seconds
Rank 12: Matrix-vector product took 0.0287582 seconds
Rank 13: Matrix-vector product took 0.0288848 seconds
Rank 14: Matrix-vector product took 0.0293918 seconds
Rank 15: Matrix-vector product took 0.0296594 seconds
Rank 16: Matrix-vector product took 0.0278885 seconds
Rank 17: Matrix-vector product took 0.0297175 seconds
Rank 18: Matrix-vector product took 0.0291118 seconds
Rank 19: Matrix-vector product took 0.028464 seconds
Rank 20: Matrix-vector product took 0.029804 seconds
Rank 21: Matrix-vector product took 0.0296573 seconds
Rank 22: Matrix-vector product took 0.0288804 seconds
Rank 23: Matrix-vector product took 0.0293717 seconds
Rank 24: Matrix-vector product took 0.0298887 seconds
Rank 25: Matrix-vector product took 0.0300771 seconds
Rank 26: Matrix-vector product took 0.0287319 seconds
Rank 27: Matrix-vector product took 0.0289226 seconds
Rank 28: Matrix-vector product took 0.027618 seconds
Rank 29: Matrix-vector product took 0.0282248 seconds
Rank 30: Matrix-vector product took 0.0289422 seconds
Rank 31: Matrix-vector product took 0.0294548 seconds
Average time for Matrix-vector product is 0.0290505 seconds

Rank 0: copyOwnerToAll took 0.000531719 seconds
Rank 1: copyOwnerToAll took 0.000531719 seconds
Rank 2: copyOwnerToAll took 0.000531719 seconds
Rank 3: copyOwnerToAll took 0.000531719 seconds
Rank 4: copyOwnerToAll took 0.000531719 seconds
Rank 5: copyOwnerToAll took 0.000531719 seconds
Rank 6: copyOwnerToAll took 0.000531719 seconds
Rank 7: copyOwnerToAll took 0.000531719 seconds
Rank 8: copyOwnerToAll took 0.000531719 seconds
Rank 9: copyOwnerToAll took 0.000531719 seconds
Rank 10: copyOwnerToAll took 0.000531719 seconds
Rank 11: copyOwnerToAll took 0.000531719 seconds
Rank 12: copyOwnerToAll took 0.000531719 seconds
Rank 13: copyOwnerToAll took 0.000531719 seconds
Rank 14: copyOwnerToAll took 0.000531719 seconds
Rank 15: copyOwnerToAll took 0.000531719 seconds
Rank 16: copyOwnerToAll took 0.000531719 seconds
Rank 17: copyOwnerToAll took 0.000531719 seconds
Rank 18: copyOwnerToAll took 0.000531719 seconds
Rank 19: copyOwnerToAll took 0.000531719 seconds
Rank 20: copyOwnerToAll took 0.000531719 seconds
Rank 21: copyOwnerToAll took 0.000531719 seconds
Rank 22: copyOwnerToAll took 0.000531719 seconds
Rank 23: copyOwnerToAll took 0.000531719 seconds
Rank 24: copyOwnerToAll took 0.000531719 seconds
Rank 25: copyOwnerToAll took 0.000531719 seconds
Rank 26: copyOwnerToAll took 0.000531719 seconds
Rank 27: copyOwnerToAll took 0.000531719 seconds
Rank 28: copyOwnerToAll took 0.000531719 seconds
Rank 29: copyOwnerToAll took 0.000531719 seconds
Rank 30: copyOwnerToAll took 0.000531719 seconds
Rank 31: copyOwnerToAll took 0.000531719 seconds
Average time for copyOwnertoAll is 0.000560839 seconds
Number of cores/ranks per node is: 16
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 500003
Cell on rank 1 before loadbalancing: 499997
Edge-cut: 12571
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	838	851	0	0	0	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	0	0	1217	225	
From rank 1 to: 	835	0	133	1390	0	0	1147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	49	0	0	0	0	90	1225	
From rank 2 to: 	853	131	0	807	238	743	398	0	0	0	0	0	97	227	264	1029	0	0	0	0	138	149	0	0	973	4	0	0	0	0	0	0	
From rank 3 to: 	0	1375	815	0	913	0	142	0	0	0	0	0	0	0	926	0	0	0	0	0	352	0	0	0	58	1307	0	0	0	0	0	209	
From rank 4 to: 	0	0	256	913	0	514	702	996	0	0	0	0	0	1063	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	764	0	517	0	531	1147	0	0	0	0	1012	420	0	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	1189	1133	395	138	722	567	0	1505	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	988	1147	1525	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	600	983	854	140	705	229	229	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	599	0	829	404	907	0	0	333	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	1029	865	0	1957	0	0	0	0	0	0	0	0	0	0	555	514	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	856	404	1971	0	0	0	1189	776	0	0	0	0	0	128	909	699	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	100	0	0	1026	0	0	141	909	0	0	0	543	0	1197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	227	0	1093	422	0	0	704	0	0	0	543	0	964	531	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	262	942	205	0	0	0	235	0	0	1187	0	971	0	751	0	0	0	0	879	67	1	66	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	1033	0	0	53	0	0	223	334	0	786	1213	538	751	0	0	0	0	0	4	880	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1605	765	1206	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1598	0	1034	423	200	291	1066	1387	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	765	1001	0	586	265	751	0	0	317	0	1175	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1216	432	581	0	496	0	0	82	0	0	137	1169	0	0	0	0	
From rank 20 to: 	0	0	146	350	0	0	0	0	0	0	0	0	0	0	877	4	0	200	257	513	0	595	113	943	367	1191	60	260	0	0	0	0	
From rank 21 to: 	0	0	161	0	0	0	0	0	0	0	0	128	0	0	73	880	0	286	751	0	587	0	1312	104	1116	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	555	899	0	0	1	0	0	1066	0	0	114	1309	0	743	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	514	699	0	0	88	0	0	1374	0	82	969	104	732	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	121	2	972	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	0	380	1111	0	0	0	571	1374	9	0	0	781	0	
From rank 25 to: 	0	40	4	1318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1205	0	0	0	572	0	267	995	33	87	293	1218	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1176	140	60	0	0	0	1355	267	0	630	44	766	103	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1169	260	0	0	0	6	988	630	0	1042	79	0	42	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	47	1062	0	666	30	1294	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	766	75	665	0	1578	304	
From rank 30 to: 	1214	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	803	304	100	0	30	1589	0	645	
From rank 31 to: 	218	1224	0	209	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1199	0	42	1289	307	637	0	
loadb
After loadbalancing process 16 has 34828 cells.
After loadbalancing process 7 has 34915 cells.
After loadbalancing process 28 has 34380 cells.
After loadbalancing process 5 has 35698 cells.
After loadbalancing process 4 has 35897 cells.
After loadbalancing process 12 has 35163 cells.
After loadbalancing process 6 has 36895 cells.
After loadbalancing process 13 has 35732 cells.
After loadbalancing process 30 has 36014 cells.
After loadbalancing process 1 has 36117 cells.
After loadbalancing process 0 has 35710 cells.
After loadbalancing process 31 has 36376 cells.
After loadbalancing process 19 has 35358 cells.
After loadbalancing process 18 has 36106 cells.
After loadbalancing process 9 has 34327 cells.
After loadbalancing process 17 has 37242 cells.
After loadbalancing process 26 has 35795 cells.
After loadbalancing process 3 has 37340 cells.
After loadbalancing process 2 has 37299 cells.
After loadbalancing process 15 has 37059 cells.
After loadbalancing process 8 has 34990 cells.
After loadbalancing process 22 has 35947 cells.
After loadbalancing process 10 has 36170 cells.
After loadbalancing process 23 has 35809 cells.
After loadbalancing process 24 has 36947 cells.
After loadbalancing process 21 has 36657 cells.
After loadbalancing process 25 has 37282 cells.
After loadbalancing process 14 has 36825 cells.
After loadbalancing process 20 has 37125 cells.
After loadbalancing process 11 has 38177 cells.
After loadbalancing process 29 has 34737 cells.
After loadbalancing process 27 has 35459 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	838	851	0	0	0	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	0	0	1217	225	
Rank 1's ghost cells:	835	0	133	1390	0	0	1147	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	49	0	0	0	0	90	1225	
Rank 2's ghost cells:	853	131	0	807	238	743	398	0	0	0	0	0	97	227	264	1029	0	0	0	0	138	149	0	0	973	4	0	0	0	0	0	0	
Rank 3's ghost cells:	0	1375	815	0	913	0	142	0	0	0	0	0	0	0	926	0	0	0	0	0	352	0	0	0	58	1307	0	0	0	0	0	209	
Rank 4's ghost cells:	0	0	256	913	0	514	702	996	0	0	0	0	0	1063	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	764	0	517	0	531	1147	0	0	0	0	1012	420	0	53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	1189	1133	395	138	722	567	0	1505	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	988	1147	1525	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	600	983	854	140	705	229	229	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	599	0	829	404	907	0	0	333	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	1029	865	0	1957	0	0	0	0	0	0	0	0	0	0	555	514	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	856	404	1971	0	0	0	1189	776	0	0	0	0	0	128	909	699	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	100	0	0	1026	0	0	141	909	0	0	0	543	0	1197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	227	0	1093	422	0	0	704	0	0	0	543	0	964	531	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	262	942	205	0	0	0	235	0	0	1187	0	971	0	751	0	0	0	0	879	67	1	66	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	1033	0	0	53	0	0	223	334	0	786	1213	538	751	0	0	0	0	0	4	880	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1605	765	1206	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1598	0	1034	423	200	291	1066	1387	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	765	1001	0	586	265	751	0	0	317	0	1175	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1216	432	581	0	496	0	0	82	0	0	137	1169	0	0	0	0	
Rank 20's ghost cells:	0	0	146	350	0	0	0	0	0	0	0	0	0	0	877	4	0	200	257	513	0	595	113	943	367	1191	60	260	0	0	0	0	
Rank 21's ghost cells:	0	0	161	0	0	0	0	0	0	0	0	128	0	0	73	880	0	286	751	0	587	0	1312	104	1116	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	555	899	0	0	1	0	0	1066	0	0	114	1309	0	743	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	514	699	0	0	88	0	0	1374	0	82	969	104	732	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	121	2	972	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	0	380	1111	0	0	0	571	1374	9	0	0	781	0	
Rank 25's ghost cells:	0	40	4	1318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1205	0	0	0	572	0	267	995	33	87	293	1218	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1176	140	60	0	0	0	1355	267	0	630	44	766	103	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1169	260	0	0	0	6	988	630	0	1042	79	0	42	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	47	1062	0	666	30	1294	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	87	766	75	665	0	1578	304	
Rank 30's ghost cells:	1214	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	803	304	100	0	30	1589	0	645	
Rank 31's ghost cells:	218	1224	0	209	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1199	0	42	1289	307	637	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          56.0599         0.224419
    2          32.7469         0.584141
    3          23.2217         0.709126
    4          19.9933         0.860978
    5          17.8311         0.891851
    6          14.3608         0.805381
    7          12.5456         0.873597
    8          12.0967         0.964221
    9           10.159         0.839815
   10          9.05802         0.891627
   11          8.99103         0.992604
   12          7.91137         0.879919
   13          6.94441         0.877776
   14          7.02636           1.0118
   15          6.52103          0.92808
   16          5.79454         0.888594
   17          5.71986         0.987112
   18          5.39325         0.942898
   19           4.8709         0.903147
   20          4.80513         0.986498
   21          4.54486         0.945835
   22          4.16959          0.91743
   23          4.15714         0.997015
   24           3.9001         0.938169
   25           3.5768         0.917104
   26          3.55597         0.994177
   27          3.36837         0.947243
   28          3.08362         0.915463
   29          3.06773         0.994848
   30           2.9473         0.960742
   31          2.82306         0.957847
   32          2.90628          1.02948
   33          2.95668          1.01734
   34          3.02719          1.02384
   35          3.15651          1.04272
   36           2.8939         0.916801
   37          2.38698         0.824833
   38          1.91731         0.803236
   39          1.50532         0.785121
   40          1.26703         0.841703
   41          1.16509         0.919542
   42          1.04742         0.899007
   43         0.927301         0.885316
   44         0.792518          0.85465
   45         0.660575         0.833515
   46         0.573394         0.868023
   47         0.497601         0.867816
   48         0.406383         0.816685
   49         0.338331         0.832542
   50         0.282566         0.835177
   51         0.238531         0.844158
   52         0.206824         0.867073
   53         0.175942         0.850689
   54         0.145606         0.827578
   55          0.12379         0.850167
   56         0.102848         0.830828
   57        0.0836158         0.813005
   58        0.0695053         0.831246
   59        0.0559034         0.804305
   60        0.0453745         0.811658
   61         0.036831         0.811713
   62        0.0290095         0.787636
   63        0.0230101         0.793193
=== rate=0.862863, T=5.35771, TIT=0.0850431, IT=63

 Elapsed time: 5.35771
Rank 0: Matrix-vector product took 0.0288689 seconds
Rank 1: Matrix-vector product took 0.0290051 seconds
Rank 2: Matrix-vector product took 0.0298242 seconds
Rank 3: Matrix-vector product took 0.0299616 seconds
Rank 4: Matrix-vector product took 0.0287566 seconds
Rank 5: Matrix-vector product took 0.0289627 seconds
Rank 6: Matrix-vector product took 0.0295964 seconds
Rank 7: Matrix-vector product took 0.0280629 seconds
Rank 8: Matrix-vector product took 0.0281681 seconds
Rank 9: Matrix-vector product took 0.02795 seconds
Rank 10: Matrix-vector product took 0.0292525 seconds
Rank 11: Matrix-vector product took 0.0305526 seconds
Rank 12: Matrix-vector product took 0.0281752 seconds
Rank 13: Matrix-vector product took 0.0286119 seconds
Rank 14: Matrix-vector product took 0.0295739 seconds
Rank 15: Matrix-vector product took 0.0296316 seconds
Rank 16: Matrix-vector product took 0.0279317 seconds
Rank 17: Matrix-vector product took 0.0298829 seconds
Rank 18: Matrix-vector product took 0.0293085 seconds
Rank 19: Matrix-vector product took 0.0284022 seconds
Rank 20: Matrix-vector product took 0.0297178 seconds
Rank 21: Matrix-vector product took 0.0295958 seconds
Rank 22: Matrix-vector product took 0.0289723 seconds
Rank 23: Matrix-vector product took 0.0291061 seconds
Rank 24: Matrix-vector product took 0.0297174 seconds
Rank 25: Matrix-vector product took 0.030003 seconds
Rank 26: Matrix-vector product took 0.0289931 seconds
Rank 27: Matrix-vector product took 0.0286535 seconds
Rank 28: Matrix-vector product took 0.0281515 seconds
Rank 29: Matrix-vector product took 0.0282262 seconds
Rank 30: Matrix-vector product took 0.0288489 seconds
Rank 31: Matrix-vector product took 0.0295188 seconds
Average time for Matrix-vector product is 0.029062 seconds

Rank 0: copyOwnerToAll took 0.000529269 seconds
Rank 1: copyOwnerToAll took 0.000529269 seconds
Rank 2: copyOwnerToAll took 0.000529269 seconds
Rank 3: copyOwnerToAll took 0.000529269 seconds
Rank 4: copyOwnerToAll took 0.000529269 seconds
Rank 5: copyOwnerToAll took 0.000529269 seconds
Rank 6: copyOwnerToAll took 0.000529269 seconds
Rank 7: copyOwnerToAll took 0.000529269 seconds
Rank 8: copyOwnerToAll took 0.000529269 seconds
Rank 9: copyOwnerToAll took 0.000529269 seconds
Rank 10: copyOwnerToAll took 0.000529269 seconds
Rank 11: copyOwnerToAll took 0.000529269 seconds
Rank 12: copyOwnerToAll took 0.000529269 seconds
Rank 13: copyOwnerToAll took 0.000529269 seconds
Rank 14: copyOwnerToAll took 0.000529269 seconds
Rank 15: copyOwnerToAll took 0.000529269 seconds
Rank 16: copyOwnerToAll took 0.000529269 seconds
Rank 17: copyOwnerToAll took 0.000529269 seconds
Rank 18: copyOwnerToAll took 0.000529269 seconds
Rank 19: copyOwnerToAll took 0.000529269 seconds
Rank 20: copyOwnerToAll took 0.000529269 seconds
Rank 21: copyOwnerToAll took 0.000529269 seconds
Rank 22: copyOwnerToAll took 0.000529269 seconds
Rank 23: copyOwnerToAll took 0.000529269 seconds
Rank 24: copyOwnerToAll took 0.000529269 seconds
Rank 25: copyOwnerToAll took 0.000529269 seconds
Rank 26: copyOwnerToAll took 0.000529269 seconds
Rank 27: copyOwnerToAll took 0.000529269 seconds
Rank 28: copyOwnerToAll took 0.000529269 seconds
Rank 29: copyOwnerToAll took 0.000529269 seconds
Rank 30: copyOwnerToAll took 0.000529269 seconds
Rank 31: copyOwnerToAll took 0.000529269 seconds
Average time for copyOwnertoAll is 0.000550157 seconds
