Number of cores/ranks per node is: 8
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	606	264	1004	33	0	917	151	1275	9	0	0	77	179	0	0	0	0	74	846	0	0	0	0	275	0	0	0	0	0	0	0	
From rank 1 to: 	606	0	1193	18	0	135	0	1168	198	881	0	0	313	0	0	0	0	0	1015	27	0	0	0	0	0	54	0	0	0	0	0	0	
From rank 2 to: 	264	1190	0	743	0	1069	0	66	0	0	0	0	983	266	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	999	22	737	0	1238	59	171	26	0	0	0	0	4	851	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	33	0	0	1227	0	691	1071	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	136	1064	69	690	0	176	1316	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	922	0	0	181	1064	168	0	594	0	0	0	0	0	0	0	0	1247	7	0	275	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	148	1167	66	25	87	1328	593	0	0	0	0	0	0	0	0	0	88	1039	332	38	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	1275	198	0	0	0	0	0	0	0	821	1369	0	423	981	6	67	0	0	0	0	0	0	0	0	920	65	0	0	0	0	0	0	
From rank 9 to: 	8	870	0	0	0	0	0	0	817	0	121	1157	1033	0	199	0	0	0	100	0	0	0	0	0	13	1255	0	43	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	1370	112	0	689	0	20	30	1009	0	0	0	0	0	0	0	0	37	33	1051	69	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	1160	698	0	0	0	1111	27	0	0	0	0	0	0	0	0	0	0	159	1189	0	0	0	0	
From rank 12 to: 	76	314	985	4	0	0	0	0	432	1029	0	0	0	689	1207	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	186	0	262	850	0	0	0	0	981	0	25	0	685	0	92	1222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	6	189	25	1111	1207	102	0	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	55	0	1008	28	21	1214	690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	1243	88	0	0	0	0	0	0	0	0	0	645	86	1161	0	0	6	1135	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	7	1039	0	0	0	0	0	0	0	0	646	0	1289	64	265	0	1114	89	0	0	0	0	0	0	0	0	
From rank 18 to: 	67	1023	0	0	0	0	0	347	0	100	0	0	0	0	0	0	99	1293	0	573	1081	0	0	0	44	1057	0	0	0	0	12	0	
From rank 19 to: 	864	27	0	0	0	0	284	38	0	0	0	0	0	0	0	0	1160	64	583	0	903	0	0	250	1192	0	0	0	0	0	0	57	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	229	1081	896	0	1713	385	1010	22	21	0	0	0	0	364	1085	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1728	0	1108	136	0	0	0	0	0	0	972	118	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	8	1115	0	0	385	1101	0	720	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1135	84	0	243	1019	137	717	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	274	0	0	0	0	0	0	0	920	14	42	0	0	0	0	0	0	0	43	1192	29	0	0	0	0	583	977	0	0	46	0	1361	
From rank 25 to: 	0	54	0	0	0	0	0	0	55	1241	31	0	0	0	0	0	0	0	1063	0	21	0	0	0	581	0	113	1202	106	38	952	292	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	1051	159	0	0	0	0	0	0	0	0	0	0	0	0	976	117	0	626	0	1304	0	40	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	44	65	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	1202	624	0	1099	241	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	1083	0	680	1072	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	38	1282	239	680	0	285	1120	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	364	953	0	0	0	966	0	0	1070	274	0	773	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	57	1079	112	0	0	1351	294	40	0	0	1118	771	0	
loadb
After loadbalancing process 5 has 34718 cells.
After loadbalancing process 4 has 34353 cells.
After loadbalancing process 28 has 34190 cells.
After loadbalancing process 22 has 34586 cells.
After loadbalancing process 29 has 34945 cells.
After loadbalancing process 14 has 34573 cells.
After loadbalancing process 3 has 35355 cells.
After loadbalancing process 2 has 35824 cells.
After loadbalancing process 31 has 36076 cells.
After loadbalancing process 7 has 36161 cells.
After loadbalancing process 0 has 36954 cells.
After loadbalancing process 16 has 35621 cells.
After loadbalancing process 26 has 35519 cells.
After loadbalancing process 6 has 35707 cells.
After loadbalancing process 24 has 36735 cells.
After loadbalancing process 23 has 34581 cells.
After loadbalancing process 19 has 36668 cells.
After loadbalancing process 27 has 35721 cells.
After loadbalancing process 1 has 36862 cells.
After loadbalancing process 30 has 35663 cells.
After loadbalancing process 15 has 34269 cells.
After loadbalancing process 17 has 35769 cells.
After loadbalancing process 11 has 35591 cells.
After loadbalancing process 21 has 35311 cells.
After loadbalancing process 12 has 35999 cells.
After loadbalancing process 25 has 36994 cells.
After loadbalancing process 9 has 36871 cells.
After loadbalancing process 18 has 36941 cells.
After loadbalancing process 13 has 35553 cells.
After loadbalancing process 8 has 37374 cells.
After loadbalancing process 10 has 35672 cells.
After loadbalancing process 20 has 38047 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	606	264	1004	33	0	917	151	1275	9	0	0	77	179	0	0	0	0	74	846	0	0	0	0	275	0	0	0	0	0	0	0	
Rank 1's ghost cells:	606	0	1193	18	0	135	0	1168	198	881	0	0	313	0	0	0	0	0	1015	27	0	0	0	0	0	54	0	0	0	0	0	0	
Rank 2's ghost cells:	264	1190	0	743	0	1069	0	66	0	0	0	0	983	266	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	999	22	737	0	1238	59	171	26	0	0	0	0	4	851	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	33	0	0	1227	0	691	1071	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	136	1064	69	690	0	176	1316	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	922	0	0	181	1064	168	0	594	0	0	0	0	0	0	0	0	1247	7	0	275	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	148	1167	66	25	87	1328	593	0	0	0	0	0	0	0	0	0	88	1039	332	38	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	1275	198	0	0	0	0	0	0	0	821	1369	0	423	981	6	67	0	0	0	0	0	0	0	0	920	65	0	0	0	0	0	0	
Rank 9's ghost cells:	8	870	0	0	0	0	0	0	817	0	121	1157	1033	0	199	0	0	0	100	0	0	0	0	0	13	1255	0	43	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	1370	112	0	689	0	20	30	1009	0	0	0	0	0	0	0	0	37	33	1051	69	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	1160	698	0	0	0	1111	27	0	0	0	0	0	0	0	0	0	0	159	1189	0	0	0	0	
Rank 12's ghost cells:	76	314	985	4	0	0	0	0	432	1029	0	0	0	689	1207	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	186	0	262	850	0	0	0	0	981	0	25	0	685	0	92	1222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	6	189	25	1111	1207	102	0	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	55	0	1008	28	21	1214	690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	1243	88	0	0	0	0	0	0	0	0	0	645	86	1161	0	0	6	1135	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	7	1039	0	0	0	0	0	0	0	0	646	0	1289	64	265	0	1114	89	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	67	1023	0	0	0	0	0	347	0	100	0	0	0	0	0	0	99	1293	0	573	1081	0	0	0	44	1057	0	0	0	0	12	0	
Rank 19's ghost cells:	864	27	0	0	0	0	284	38	0	0	0	0	0	0	0	0	1160	64	583	0	903	0	0	250	1192	0	0	0	0	0	0	57	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	229	1081	896	0	1713	385	1010	22	21	0	0	0	0	364	1085	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1728	0	1108	136	0	0	0	0	0	0	972	118	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	8	1115	0	0	385	1101	0	720	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1135	84	0	243	1019	137	717	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	274	0	0	0	0	0	0	0	920	14	42	0	0	0	0	0	0	0	43	1192	29	0	0	0	0	583	977	0	0	46	0	1361	
Rank 25's ghost cells:	0	54	0	0	0	0	0	0	55	1241	31	0	0	0	0	0	0	0	1063	0	21	0	0	0	581	0	113	1202	106	38	952	292	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	1051	159	0	0	0	0	0	0	0	0	0	0	0	0	976	117	0	626	0	1304	0	40	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	44	65	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	1202	624	0	1099	241	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	1083	0	680	1072	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	38	1282	239	680	0	285	1120	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	364	953	0	0	0	966	0	0	1070	274	0	773	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	57	1079	112	0	0	1351	294	40	0	0	1118	771	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.5991         0.222575
    2          32.4192         0.583089
    3          22.9029         0.706461
    4          19.4622         0.849767
    5          17.5881         0.903708
    6          14.3411         0.815384
    7          12.3313         0.859863
    8          11.8484         0.960838
    9          10.0568         0.848788
   10          8.77091         0.872138
   11          8.77546          1.00052
   12          7.87184         0.897028
   13          6.84242         0.869228
   14          6.91259          1.01026
   15          6.38164          0.92319
   16          5.69316         0.892116
   17          5.71093          1.00312
   18          5.35984         0.938522
   19          4.90286         0.914741
   20          4.92884           1.0053
   21          4.55154         0.923451
   22          4.32709         0.950686
   23          4.27167         0.987193
   24          3.85933         0.903472
   25          3.77807         0.978944
   26          3.62261         0.958852
   27          3.25298         0.897967
   28          3.21086         0.987052
   29          3.06989         0.956094
   30          2.81569         0.917198
   31          2.86249          1.01662
   32          2.84584         0.994186
   33          2.83831         0.997352
   34           3.0489           1.0742
   35          3.12153          1.02382
   36          2.91942         0.935255
   37          2.55415         0.874882
   38          1.96138         0.767919
   39          1.50136         0.765461
   40          1.29253         0.860903
   41          1.16046         0.897826
   42          1.05628         0.910224
   43         0.919247         0.870268
   44         0.770695         0.838398
   45          0.67875         0.880699
   46         0.581819         0.857192
   47         0.471439         0.810284
   48         0.405846         0.860867
   49         0.336197         0.828386
   50         0.265407         0.789438
   51         0.230532         0.868599
   52         0.199779           0.8666
   53         0.169728         0.849578
   54         0.147088         0.866608
   55         0.123632         0.840533
   56         0.102971         0.832882
   57        0.0855264         0.830587
   58        0.0698087         0.816224
   59         0.056981         0.816245
   60        0.0465685         0.817264
   61        0.0370534         0.795675
   62        0.0293408         0.791852
   63        0.0232457         0.792264
=== rate=0.863002, T=5.34855, TIT=0.0848977, IT=63

 Elapsed time: 5.34855
Rank 0: Matrix-vector product took 0.0306147 seconds
Rank 1: Matrix-vector product took 0.0296986 seconds
Rank 2: Matrix-vector product took 0.0287748 seconds
Rank 3: Matrix-vector product took 0.0284584 seconds
Rank 4: Matrix-vector product took 0.0277789 seconds
Rank 5: Matrix-vector product took 0.0278823 seconds
Rank 6: Matrix-vector product took 0.0289412 seconds
Rank 7: Matrix-vector product took 0.0293769 seconds
Rank 8: Matrix-vector product took 0.0300542 seconds
Rank 9: Matrix-vector product took 0.0297889 seconds
Rank 10: Matrix-vector product took 0.0289593 seconds
Rank 11: Matrix-vector product took 0.0289017 seconds
Rank 12: Matrix-vector product took 0.0289462 seconds
Rank 13: Matrix-vector product took 0.028911 seconds
Rank 14: Matrix-vector product took 0.0278568 seconds
Rank 15: Matrix-vector product took 0.0278928 seconds
Rank 16: Matrix-vector product took 0.0289331 seconds
Rank 17: Matrix-vector product took 0.0289988 seconds
Rank 18: Matrix-vector product took 0.0295944 seconds
Rank 19: Matrix-vector product took 0.0296919 seconds
Rank 20: Matrix-vector product took 0.0304064 seconds
Rank 21: Matrix-vector product took 0.028276 seconds
Rank 22: Matrix-vector product took 0.027891 seconds
Rank 23: Matrix-vector product took 0.0278485 seconds
Rank 24: Matrix-vector product took 0.0297398 seconds
Rank 25: Matrix-vector product took 0.0299525 seconds
Rank 26: Matrix-vector product took 0.0286122 seconds
Rank 27: Matrix-vector product took 0.0287314 seconds
Rank 28: Matrix-vector product took 0.028029 seconds
Rank 29: Matrix-vector product took 0.0282368 seconds
Rank 30: Matrix-vector product took 0.0286071 seconds
Rank 31: Matrix-vector product took 0.0291298 seconds
Average time for Matrix-vector product is 0.0289224 seconds

Rank 0: copyOwnerToAll took 0.000655819 seconds
Rank 1: copyOwnerToAll took 0.000655819 seconds
Rank 2: copyOwnerToAll took 0.000655819 seconds
Rank 3: copyOwnerToAll took 0.000655819 seconds
Rank 4: copyOwnerToAll took 0.000655819 seconds
Rank 5: copyOwnerToAll took 0.000655819 seconds
Rank 6: copyOwnerToAll took 0.000655819 seconds
Rank 7: copyOwnerToAll took 0.000655819 seconds
Rank 8: copyOwnerToAll took 0.000655819 seconds
Rank 9: copyOwnerToAll took 0.000655819 seconds
Rank 10: copyOwnerToAll took 0.000655819 seconds
Rank 11: copyOwnerToAll took 0.000655819 seconds
Rank 12: copyOwnerToAll took 0.000655819 seconds
Rank 13: copyOwnerToAll took 0.000655819 seconds
Rank 14: copyOwnerToAll took 0.000655819 seconds
Rank 15: copyOwnerToAll took 0.000655819 seconds
Rank 16: copyOwnerToAll took 0.000655819 seconds
Rank 17: copyOwnerToAll took 0.000655819 seconds
Rank 18: copyOwnerToAll took 0.000655819 seconds
Rank 19: copyOwnerToAll took 0.000655819 seconds
Rank 20: copyOwnerToAll took 0.000655819 seconds
Rank 21: copyOwnerToAll took 0.000655819 seconds
Rank 22: copyOwnerToAll took 0.000655819 seconds
Rank 23: copyOwnerToAll took 0.000655819 seconds
Rank 24: copyOwnerToAll took 0.000655819 seconds
Rank 25: copyOwnerToAll took 0.000655819 seconds
Rank 26: copyOwnerToAll took 0.000655819 seconds
Rank 27: copyOwnerToAll took 0.000655819 seconds
Rank 28: copyOwnerToAll took 0.000655819 seconds
Rank 29: copyOwnerToAll took 0.000655819 seconds
Rank 30: copyOwnerToAll took 0.000655819 seconds
Rank 31: copyOwnerToAll took 0.000655819 seconds
Average time for copyOwnertoAll is 0.000693495 seconds
Number of cores/ranks per node is: 8
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	606	264	1004	33	0	917	151	1275	9	0	0	77	179	0	0	0	0	74	846	0	0	0	0	275	0	0	0	0	0	0	0	
From rank 1 to: 	606	0	1193	18	0	135	0	1168	198	881	0	0	313	0	0	0	0	0	1015	27	0	0	0	0	0	54	0	0	0	0	0	0	
From rank 2 to: 	264	1190	0	743	0	1069	0	66	0	0	0	0	983	266	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	999	22	737	0	1238	59	171	26	0	0	0	0	4	851	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	33	0	0	1227	0	691	1071	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	136	1064	69	690	0	176	1316	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	922	0	0	181	1064	168	0	594	0	0	0	0	0	0	0	0	1247	7	0	275	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	148	1167	66	25	87	1328	593	0	0	0	0	0	0	0	0	0	88	1039	332	38	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	1275	198	0	0	0	0	0	0	0	821	1369	0	423	981	6	67	0	0	0	0	0	0	0	0	920	65	0	0	0	0	0	0	
From rank 9 to: 	8	870	0	0	0	0	0	0	817	0	121	1157	1033	0	199	0	0	0	100	0	0	0	0	0	13	1255	0	43	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	1370	112	0	689	0	20	30	1009	0	0	0	0	0	0	0	0	37	33	1051	69	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	1160	698	0	0	0	1111	27	0	0	0	0	0	0	0	0	0	0	159	1189	0	0	0	0	
From rank 12 to: 	76	314	985	4	0	0	0	0	432	1029	0	0	0	689	1207	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	186	0	262	850	0	0	0	0	981	0	25	0	685	0	92	1222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	6	189	25	1111	1207	102	0	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	55	0	1008	28	21	1214	690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	1243	88	0	0	0	0	0	0	0	0	0	645	86	1161	0	0	6	1135	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	7	1039	0	0	0	0	0	0	0	0	646	0	1289	64	265	0	1114	89	0	0	0	0	0	0	0	0	
From rank 18 to: 	67	1023	0	0	0	0	0	347	0	100	0	0	0	0	0	0	99	1293	0	573	1081	0	0	0	44	1057	0	0	0	0	12	0	
From rank 19 to: 	864	27	0	0	0	0	284	38	0	0	0	0	0	0	0	0	1160	64	583	0	903	0	0	250	1192	0	0	0	0	0	0	57	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	229	1081	896	0	1713	385	1010	22	21	0	0	0	0	364	1085	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1728	0	1108	136	0	0	0	0	0	0	972	118	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	8	1115	0	0	385	1101	0	720	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1135	84	0	243	1019	137	717	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	274	0	0	0	0	0	0	0	920	14	42	0	0	0	0	0	0	0	43	1192	29	0	0	0	0	583	977	0	0	46	0	1361	
From rank 25 to: 	0	54	0	0	0	0	0	0	55	1241	31	0	0	0	0	0	0	0	1063	0	21	0	0	0	581	0	113	1202	106	38	952	292	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	1051	159	0	0	0	0	0	0	0	0	0	0	0	0	976	117	0	626	0	1304	0	40	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	44	65	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	1202	624	0	1099	241	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	1083	0	680	1072	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	38	1282	239	680	0	285	1120	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	364	953	0	0	0	966	0	0	1070	274	0	773	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	57	1079	112	0	0	1351	294	40	0	0	1118	771	0	
loadb
After loadbalancing process 22 has 34586 cells.
After loadbalancing process 23 has 34581 cells.
After loadbalancing process 14 has 34573 cells.
After loadbalancing process 28 has 34190 cells.
After loadbalancing process 12 has 35999 cells.
After loadbalancing process 4 has 34353 cells.
After loadbalancing process 20 has 38047 cells.
After loadbalancing process 16 has 35621 cells.
After loadbalancing process 29 has 34945 cells.
After loadbalancing process 11 has 35591 cells.
After loadbalancing process 15 has 34269 cells.
After loadbalancing process 6 has 35707 cells.
After loadbalancing process 5 has 34718 cells.
After loadbalancing process 21 has 35311 cells.
After loadbalancing process 19 has 36668 cells.
After loadbalancing process 18 has 36941 cells.
After loadbalancing process 13 has 35553 cells.
After loadbalancing process 2 has 35824 cells.
After loadbalancing process 31 has 36076 cells.
After loadbalancing process 8 has 37374 cells.
After loadbalancing process 3 has 35355 cells.
After loadbalancing process 17 has 35769 cells.
After loadbalancing process 30 has 35663 cells.
After loadbalancing process 9 has 36871 cells.
After loadbalancing process 25 has 36994 cells.
After loadbalancing process 10 has 35672 cells.
After loadbalancing process 27 has 35721 cells.
After loadbalancing process 0 has 36954 cells.
After loadbalancing process 26 has 35519 cells.
After loadbalancing process 24 has 36735 cells.
After loadbalancing process 1 has 36862 cells.
After loadbalancing process 7 has 36161 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	606	264	1004	33	0	917	151	1275	9	0	0	77	179	0	0	0	0	74	846	0	0	0	0	275	0	0	0	0	0	0	0	
Rank 1's ghost cells:	606	0	1193	18	0	135	0	1168	198	881	0	0	313	0	0	0	0	0	1015	27	0	0	0	0	0	54	0	0	0	0	0	0	
Rank 2's ghost cells:	264	1190	0	743	0	1069	0	66	0	0	0	0	983	266	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	999	22	737	0	1238	59	171	26	0	0	0	0	4	851	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	33	0	0	1227	0	691	1071	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	136	1064	69	690	0	176	1316	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	922	0	0	181	1064	168	0	594	0	0	0	0	0	0	0	0	1247	7	0	275	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	148	1167	66	25	87	1328	593	0	0	0	0	0	0	0	0	0	88	1039	332	38	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	1275	198	0	0	0	0	0	0	0	821	1369	0	423	981	6	67	0	0	0	0	0	0	0	0	920	65	0	0	0	0	0	0	
Rank 9's ghost cells:	8	870	0	0	0	0	0	0	817	0	121	1157	1033	0	199	0	0	0	100	0	0	0	0	0	13	1255	0	43	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	1370	112	0	689	0	20	30	1009	0	0	0	0	0	0	0	0	37	33	1051	69	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	1160	698	0	0	0	1111	27	0	0	0	0	0	0	0	0	0	0	159	1189	0	0	0	0	
Rank 12's ghost cells:	76	314	985	4	0	0	0	0	432	1029	0	0	0	689	1207	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	186	0	262	850	0	0	0	0	981	0	25	0	685	0	92	1222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	6	189	25	1111	1207	102	0	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	55	0	1008	28	21	1214	690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	1243	88	0	0	0	0	0	0	0	0	0	645	86	1161	0	0	6	1135	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	7	1039	0	0	0	0	0	0	0	0	646	0	1289	64	265	0	1114	89	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	67	1023	0	0	0	0	0	347	0	100	0	0	0	0	0	0	99	1293	0	573	1081	0	0	0	44	1057	0	0	0	0	12	0	
Rank 19's ghost cells:	864	27	0	0	0	0	284	38	0	0	0	0	0	0	0	0	1160	64	583	0	903	0	0	250	1192	0	0	0	0	0	0	57	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	229	1081	896	0	1713	385	1010	22	21	0	0	0	0	364	1085	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1728	0	1108	136	0	0	0	0	0	0	972	118	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	8	1115	0	0	385	1101	0	720	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1135	84	0	243	1019	137	717	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	274	0	0	0	0	0	0	0	920	14	42	0	0	0	0	0	0	0	43	1192	29	0	0	0	0	583	977	0	0	46	0	1361	
Rank 25's ghost cells:	0	54	0	0	0	0	0	0	55	1241	31	0	0	0	0	0	0	0	1063	0	21	0	0	0	581	0	113	1202	106	38	952	292	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	1051	159	0	0	0	0	0	0	0	0	0	0	0	0	976	117	0	626	0	1304	0	40	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	44	65	1189	0	0	0	0	0	0	0	0	0	0	0	0	0	1202	624	0	1099	241	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	1083	0	680	1072	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	38	1282	239	680	0	285	1120	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	364	953	0	0	0	966	0	0	1070	274	0	773	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	57	1079	112	0	0	1351	294	40	0	0	1118	771	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.5991         0.222575
    2          32.4192         0.583089
    3          22.9029         0.706461
    4          19.4622         0.849767
    5          17.5881         0.903708
    6          14.3411         0.815384
    7          12.3313         0.859863
    8          11.8484         0.960838
    9          10.0568         0.848788
   10          8.77091         0.872138
   11          8.77546          1.00052
   12          7.87184         0.897028
   13          6.84242         0.869228
   14          6.91259          1.01026
   15          6.38164          0.92319
   16          5.69316         0.892116
   17          5.71093          1.00312
   18          5.35984         0.938522
   19          4.90286         0.914741
   20          4.92884           1.0053
   21          4.55154         0.923451
   22          4.32709         0.950686
   23          4.27167         0.987193
   24          3.85933         0.903472
   25          3.77807         0.978944
   26          3.62261         0.958852
   27          3.25298         0.897967
   28          3.21086         0.987052
   29          3.06989         0.956094
   30          2.81569         0.917198
   31          2.86249          1.01662
   32          2.84584         0.994186
   33          2.83831         0.997352
   34           3.0489           1.0742
   35          3.12153          1.02382
   36          2.91942         0.935255
   37          2.55415         0.874882
   38          1.96138         0.767919
   39          1.50136         0.765461
   40          1.29253         0.860903
   41          1.16046         0.897826
   42          1.05628         0.910224
   43         0.919247         0.870268
   44         0.770695         0.838398
   45          0.67875         0.880699
   46         0.581819         0.857192
   47         0.471439         0.810284
   48         0.405846         0.860867
   49         0.336197         0.828386
   50         0.265407         0.789438
   51         0.230532         0.868599
   52         0.199779           0.8666
   53         0.169728         0.849578
   54         0.147088         0.866608
   55         0.123632         0.840533
   56         0.102971         0.832882
   57        0.0855264         0.830587
   58        0.0698087         0.816224
   59         0.056981         0.816245
   60        0.0465685         0.817264
   61        0.0370534         0.795675
   62        0.0293408         0.791852
   63        0.0232457         0.792264
=== rate=0.863002, T=5.34797, TIT=0.0848884, IT=63

 Elapsed time: 5.34797
Rank 0: Matrix-vector product took 0.0297414 seconds
Rank 1: Matrix-vector product took 0.0298372 seconds
Rank 2: Matrix-vector product took 0.0287917 seconds
Rank 3: Matrix-vector product took 0.0284503 seconds
Rank 4: Matrix-vector product took 0.0276612 seconds
Rank 5: Matrix-vector product took 0.0278891 seconds
Rank 6: Matrix-vector product took 0.0287035 seconds
Rank 7: Matrix-vector product took 0.0291615 seconds
Rank 8: Matrix-vector product took 0.0299331 seconds
Rank 9: Matrix-vector product took 0.0295527 seconds
Rank 10: Matrix-vector product took 0.0288457 seconds
Rank 11: Matrix-vector product took 0.0292259 seconds
Rank 12: Matrix-vector product took 0.0289781 seconds
Rank 13: Matrix-vector product took 0.0286636 seconds
Rank 14: Matrix-vector product took 0.0281738 seconds
Rank 15: Matrix-vector product took 0.0276395 seconds
Rank 16: Matrix-vector product took 0.0286746 seconds
Rank 17: Matrix-vector product took 0.0287488 seconds
Rank 18: Matrix-vector product took 0.0296766 seconds
Rank 19: Matrix-vector product took 0.0294935 seconds
Rank 20: Matrix-vector product took 0.0304438 seconds
Rank 21: Matrix-vector product took 0.0282282 seconds
Rank 22: Matrix-vector product took 0.0277513 seconds
Rank 23: Matrix-vector product took 0.0278777 seconds
Rank 24: Matrix-vector product took 0.0297784 seconds
Rank 25: Matrix-vector product took 0.0300586 seconds
Rank 26: Matrix-vector product took 0.0285353 seconds
Rank 27: Matrix-vector product took 0.0286987 seconds
Rank 28: Matrix-vector product took 0.0276518 seconds
Rank 29: Matrix-vector product took 0.0286115 seconds
Rank 30: Matrix-vector product took 0.0286672 seconds
Rank 31: Matrix-vector product took 0.0290636 seconds
Average time for Matrix-vector product is 0.0288502 seconds

max verdi: 0.000728639
max verdi: 0.0006249
max verdi: 0.00062373
max verdi: 0.000610569
max verdi: 0.00062512
max verdi: 0.000616949
max verdi: 0.000638879
max verdi: 0.00060075
max verdi: 0.000597619
max verdi: 0.00060149
Rank 0: copyOwnerToAll took 0.000597619 seconds
Rank 1: copyOwnerToAll took 0.000597619 seconds
Rank 2: copyOwnerToAll took 0.000597619 seconds
Rank 3: copyOwnerToAll took 0.000597619 seconds
Rank 4: copyOwnerToAll took 0.000597619 seconds
Rank 5: copyOwnerToAll took 0.000597619 seconds
Rank 6: copyOwnerToAll took 0.000597619 seconds
Rank 7: copyOwnerToAll took 0.000597619 seconds
Rank 8: copyOwnerToAll took 0.000597619 seconds
Rank 9: copyOwnerToAll took 0.000597619 seconds
Rank 10: copyOwnerToAll took 0.000597619 seconds
Rank 11: copyOwnerToAll took 0.000597619 seconds
Rank 12: copyOwnerToAll took 0.000597619 seconds
Rank 13: copyOwnerToAll took 0.000597619 seconds
Rank 14: copyOwnerToAll took 0.000597619 seconds
Rank 15: copyOwnerToAll took 0.000597619 seconds
Rank 16: copyOwnerToAll took 0.000597619 seconds
Rank 17: copyOwnerToAll took 0.000597619 seconds
Rank 18: copyOwnerToAll took 0.000597619 seconds
Rank 19: copyOwnerToAll took 0.000597619 seconds
Rank 20: copyOwnerToAll took 0.000597619 seconds
Rank 21: copyOwnerToAll took 0.000597619 seconds
Rank 22: copyOwnerToAll took 0.000597619 seconds
Rank 23: copyOwnerToAll took 0.000597619 seconds
Rank 24: copyOwnerToAll took 0.000597619 seconds
Rank 25: copyOwnerToAll took 0.000597619 seconds
Rank 26: copyOwnerToAll took 0.000597619 seconds
Rank 27: copyOwnerToAll took 0.000597619 seconds
Rank 28: copyOwnerToAll took 0.000597619 seconds
Rank 29: copyOwnerToAll took 0.000597619 seconds
Rank 30: copyOwnerToAll took 0.000597619 seconds
Rank 31: copyOwnerToAll took 0.000597619 seconds
Average time for copyOwnertoAll is 0.000626864 seconds
