Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 333080
Cell on rank 1 before loadbalancing: 333118
Cell on rank 2 before loadbalancing: 333802
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	1539	1775	1297	0	2429	0	0	
From rank 1 to: 	1535	0	2094	0	0	1074	54	2299	
From rank 2 to: 	1775	2094	0	330	0	234	2484	72	
From rank 3 to: 	1298	0	336	0	1474	1853	2252	0	
From rank 4 to: 	0	0	0	1474	0	3007	476	1775	
From rank 5 to: 	2447	1066	234	1860	2998	0	649	1224	
From rank 6 to: 	0	54	2485	2249	479	633	0	3514	
From rank 7 to: 	0	2299	72	0	1774	1220	3514	0	
loadb
After loadbalancing process 1 has 117082 cells.
After loadbalancing process 2 has 118766 cells.
After loadbalancing process 0 has 118317 cells.
After loadbalancing process 6 has 176700 cells.
After loadbalancing process 4 has 117350 cells.
After loadbalancing process 3 has 119328 cells.
After loadbalancing process 7 has 175395 cells.
After loadbalancing process 5 has 120863 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	1539	1775	1297	0	2429	0	0	
Rank 1's ghost cells:	1535	0	2094	0	0	1074	54	2299	
Rank 2's ghost cells:	1775	2094	0	330	0	234	2484	72	
Rank 3's ghost cells:	1298	0	336	0	1474	1853	2252	0	
Rank 4's ghost cells:	0	0	0	1474	0	3007	476	1775	
Rank 5's ghost cells:	2447	1066	234	1860	2998	0	649	1224	
Rank 6's ghost cells:	0	54	2485	2249	479	633	0	3514	
Rank 7's ghost cells:	0	2299	72	0	1774	1220	3514	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.2607         0.213213
    2          30.6717         0.575878
    3           21.593         0.704004
    4          17.8002         0.824353
    5          16.5586         0.930248
    6          14.0952         0.851227
    7          11.4471         0.812132
    8          10.9562         0.957117
    9          10.1951         0.930525
   10          8.38047         0.822013
   11          7.94429         0.947953
   12          7.84315         0.987269
   13           6.5562         0.835913
   14          6.06681         0.925355
   15          6.30698          1.03959
   16           5.5665         0.882594
   17          5.03411         0.904358
   18          5.20619          1.03418
   19          4.66979         0.896968
   20          4.29878         0.920552
   21          4.37394          1.01748
   22          3.95907          0.90515
   23          3.70861         0.936738
   24          3.73158          1.00619
   25          3.40088         0.911378
   26          3.26142         0.958991
   27          3.26015         0.999611
   28           2.9201         0.895696
   29          2.84199         0.973252
   30          2.90231          1.02122
   31          2.74288         0.945068
   32          2.87823          1.04935
   33           3.1546          1.09602
   34          2.99368         0.948988
   35          2.67858         0.894747
   36          2.18249         0.814794
   37          1.67622         0.768031
   38          1.48917         0.888407
   39          1.31304         0.881729
   40          1.07289         0.817102
   41         0.986576          0.91955
   42         0.889739         0.901846
   43         0.682751          0.76736
   44         0.592482         0.867787
   45         0.530693         0.895711
   46         0.409579         0.771782
   47         0.353423         0.862892
   48         0.304379         0.861234
   49         0.237852         0.781432
   50         0.210906         0.886713
   51         0.175646         0.832815
   52         0.140004          0.79708
   53          0.12615         0.901046
   54        0.0995837         0.789408
   55        0.0839743         0.843253
   56        0.0712072         0.847965
   57        0.0560222         0.786749
   58        0.0468203         0.835745
   59        0.0368915         0.787939
   60        0.0293753          0.79626
   61        0.0229656         0.781801
=== rate=0.858673, T=25.4276, TIT=0.416846, IT=61

 Elapsed time: 25.4276
Rank 0: Matrix-vector product took 0.0958768 seconds
Rank 1: Matrix-vector product took 0.0954971 seconds
Rank 2: Matrix-vector product took 0.0969431 seconds
Rank 3: Matrix-vector product took 0.0969645 seconds
Rank 4: Matrix-vector product took 0.0954106 seconds
Rank 5: Matrix-vector product took 0.0982516 seconds
Rank 6: Matrix-vector product took 0.144235 seconds
Rank 7: Matrix-vector product took 0.142949 seconds
Average time for Matrix-vector product is 0.108266 seconds

Rank 0: copyOwnerToAll took 0.000794919 seconds
Rank 1: copyOwnerToAll took 0.000794919 seconds
Rank 2: copyOwnerToAll took 0.000794919 seconds
Rank 3: copyOwnerToAll took 0.000794919 seconds
Rank 4: copyOwnerToAll took 0.000794919 seconds
Rank 5: copyOwnerToAll took 0.000794919 seconds
Rank 6: copyOwnerToAll took 0.000794919 seconds
Rank 7: copyOwnerToAll took 0.000794919 seconds
Average time for copyOwnertoAll is 0.000808996 seconds
Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 333080
Cell on rank 1 before loadbalancing: 333118
Cell on rank 2 before loadbalancing: 333802
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	1539	1775	1297	0	2429	0	0	
From rank 1 to: 	1535	0	2094	0	0	1074	54	2299	
From rank 2 to: 	1775	2094	0	330	0	234	2484	72	
From rank 3 to: 	1298	0	336	0	1474	1853	2252	0	
From rank 4 to: 	0	0	0	1474	0	3007	476	1775	
From rank 5 to: 	2447	1066	234	1860	2998	0	649	1224	
From rank 6 to: 	0	54	2485	2249	479	633	0	3514	
From rank 7 to: 	0	2299	72	0	1774	1220	3514	0	
loadb
After loadbalancing process 3 has 119328 cells.
After loadbalancing process 2 has 118766 cells.
After loadbalancing process 4 has 117350 cells.
After loadbalancing process 1 has 117082 cells.
After loadbalancing process 5 has 120863 cells.
After loadbalancing process 0 has 118317 cells.
After loadbalancing process 6 has 176700 cells.
After loadbalancing process 7 has 175395 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	1539	1775	1297	0	2429	0	0	
Rank 1's ghost cells:	1535	0	2094	0	0	1074	54	2299	
Rank 2's ghost cells:	1775	2094	0	330	0	234	2484	72	
Rank 3's ghost cells:	1298	0	336	0	1474	1853	2252	0	
Rank 4's ghost cells:	0	0	0	1474	0	3007	476	1775	
Rank 5's ghost cells:	2447	1066	234	1860	2998	0	649	1224	
Rank 6's ghost cells:	0	54	2485	2249	479	633	0	3514	
Rank 7's ghost cells:	0	2299	72	0	1774	1220	3514	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.2607         0.213213
    2          30.6717         0.575878
    3           21.593         0.704004
    4          17.8002         0.824353
    5          16.5586         0.930248
    6          14.0952         0.851227
    7          11.4471         0.812132
    8          10.9562         0.957117
    9          10.1951         0.930525
   10          8.38047         0.822013
   11          7.94429         0.947953
   12          7.84315         0.987269
   13           6.5562         0.835913
   14          6.06681         0.925355
   15          6.30698          1.03959
   16           5.5665         0.882594
   17          5.03411         0.904358
   18          5.20619          1.03418
   19          4.66979         0.896968
   20          4.29878         0.920552
   21          4.37394          1.01748
   22          3.95907          0.90515
   23          3.70861         0.936738
   24          3.73158          1.00619
   25          3.40088         0.911378
   26          3.26142         0.958991
   27          3.26015         0.999611
   28           2.9201         0.895696
   29          2.84199         0.973252
   30          2.90231          1.02122
   31          2.74288         0.945068
   32          2.87823          1.04935
   33           3.1546          1.09602
   34          2.99368         0.948988
   35          2.67858         0.894747
   36          2.18249         0.814794
   37          1.67622         0.768031
   38          1.48917         0.888407
   39          1.31304         0.881729
   40          1.07289         0.817102
   41         0.986576          0.91955
   42         0.889739         0.901846
   43         0.682751          0.76736
   44         0.592482         0.867787
   45         0.530693         0.895711
   46         0.409579         0.771782
   47         0.353423         0.862892
   48         0.304379         0.861234
   49         0.237852         0.781432
   50         0.210906         0.886713
   51         0.175646         0.832815
   52         0.140004          0.79708
   53          0.12615         0.901046
   54        0.0995837         0.789408
   55        0.0839743         0.843253
   56        0.0712072         0.847965
   57        0.0560222         0.786749
   58        0.0468203         0.835745
   59        0.0368915         0.787939
   60        0.0293753          0.79626
   61        0.0229656         0.781801
=== rate=0.858673, T=25.5408, TIT=0.418701, IT=61

 Elapsed time: 25.5408
Rank 0: Matrix-vector product took 0.0971709 seconds
Rank 1: Matrix-vector product took 0.095572 seconds
Rank 2: Matrix-vector product took 0.0967495 seconds
Rank 3: Matrix-vector product took 0.0969072 seconds
Rank 4: Matrix-vector product took 0.0954975 seconds
Rank 5: Matrix-vector product took 0.0980657 seconds
Rank 6: Matrix-vector product took 0.144339 seconds
Rank 7: Matrix-vector product took 0.150508 seconds
Average time for Matrix-vector product is 0.109351 seconds

Rank 0: copyOwnerToAll took 0.00076846 seconds
Rank 1: copyOwnerToAll took 0.00076846 seconds
Rank 2: copyOwnerToAll took 0.00076846 seconds
Rank 3: copyOwnerToAll took 0.00076846 seconds
Rank 4: copyOwnerToAll took 0.00076846 seconds
Rank 5: copyOwnerToAll took 0.00076846 seconds
Rank 6: copyOwnerToAll took 0.00076846 seconds
Rank 7: copyOwnerToAll took 0.00076846 seconds
Average time for copyOwnertoAll is 0.000788661 seconds
