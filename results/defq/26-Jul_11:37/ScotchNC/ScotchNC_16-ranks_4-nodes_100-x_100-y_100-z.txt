Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 247808
Cell on rank 1 before loadbalancing: 250352
Cell on rank 2 before loadbalancing: 252288
Cell on rank 3 before loadbalancing: 249552
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2247	575	551	85	0	977	1327	0	0	0	913	0	0	386	0	
From rank 1 to: 	2247	0	607	698	0	0	0	0	0	0	0	1249	0	0	0	0	
From rank 2 to: 	594	629	0	2315	1270	6	23	0	1246	674	0	463	0	0	0	0	
From rank 3 to: 	552	698	2304	0	137	960	137	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	85	0	1276	147	0	2077	1395	0	0	651	0	113	900	1398	130	0	
From rank 5 to: 	0	0	6	960	2082	0	1404	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	962	0	23	137	1398	1401	0	2045	0	0	0	0	192	0	829	0	
From rank 7 to: 	1327	0	0	0	0	0	2045	0	0	0	0	0	0	0	1358	0	
From rank 8 to: 	0	0	1248	0	0	0	0	0	0	2266	822	637	0	0	0	0	
From rank 9 to: 	0	0	689	0	651	0	0	0	2256	0	497	629	1012	1323	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	822	497	0	2405	134	0	0	1075	
From rank 11 to: 	913	1249	459	0	115	0	0	0	655	631	2400	0	176	0	965	173	
From rank 12 to: 	0	0	0	0	889	0	192	0	0	1012	134	168	0	2108	1266	1361	
From rank 13 to: 	0	0	0	0	1397	0	0	0	0	1323	0	0	2108	0	0	0	
From rank 14 to: 	400	0	0	0	117	0	844	1358	0	0	0	965	1264	0	0	2181	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	1075	169	1365	0	2180	0	
loadb
After loadbalancing process 7 has 66925 cells.
After loadbalancing process 5 has 66938 cells.
After loadbalancing process 13 has 67827 cells.
After loadbalancing process 2 has 69156 cells.
After loadbalancing process 8 has 67709 cells.
After loadbalancing process 0 has 68972 cells.
After loadbalancing process 4 has 70920 cells.
After loadbalancing process 1 has 67037 cells.
After loadbalancing process 6 has 69910 cells.
After loadbalancing process 3 has 66513 cells.
After loadbalancing process 10 has 68313 cells.
After loadbalancing process 9 has 69549 cells.
After loadbalancing process 15 has 67757 cells.
After loadbalancing process 12 has 68942 cells.
After loadbalancing process 14 has 68902 cells.
After loadbalancing process 11 has 71416 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2247	575	551	85	0	977	1327	0	0	0	913	0	0	386	0	
Rank 1's ghost cells:	2247	0	607	698	0	0	0	0	0	0	0	1249	0	0	0	0	
Rank 2's ghost cells:	594	629	0	2315	1270	6	23	0	1246	674	0	463	0	0	0	0	
Rank 3's ghost cells:	552	698	2304	0	137	960	137	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	85	0	1276	147	0	2077	1395	0	0	651	0	113	900	1398	130	0	
Rank 5's ghost cells:	0	0	6	960	2082	0	1404	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	962	0	23	137	1398	1401	0	2045	0	0	0	0	192	0	829	0	
Rank 7's ghost cells:	1327	0	0	0	0	0	2045	0	0	0	0	0	0	0	1358	0	
Rank 8's ghost cells:	0	0	1248	0	0	0	0	0	0	2266	822	637	0	0	0	0	
Rank 9's ghost cells:	0	0	689	0	651	0	0	0	2256	0	497	629	1012	1323	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	822	497	0	2405	134	0	0	1075	
Rank 11's ghost cells:	913	1249	459	0	115	0	0	0	655	631	2400	0	176	0	965	173	
Rank 12's ghost cells:	0	0	0	0	889	0	192	0	0	1012	134	168	0	2108	1266	1361	
Rank 13's ghost cells:	0	0	0	0	1397	0	0	0	0	1323	0	0	2108	0	0	0	
Rank 14's ghost cells:	400	0	0	0	117	0	844	1358	0	0	0	965	1264	0	0	2181	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	1075	169	1365	0	2180	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.2139         0.217029
    2          31.3935         0.579066
    3          22.0768         0.703229
    4          18.3615         0.831712
    5          16.8328         0.916741
    6          14.0497         0.834664
    7           11.672         0.830765
    8          11.2319          0.96229
    9           10.042         0.894062
   10          8.32751         0.829268
   11          8.09068         0.971561
   12          7.93465         0.980714
   13          6.59362         0.830991
   14          6.16597         0.935142
   15          6.32092          1.02513
   16           5.5786         0.882562
   17          5.00755         0.897636
   18          5.11132          1.02072
   19          4.79578         0.938266
   20          4.35193         0.907449
   21          4.36841          1.00379
   22           4.0786         0.933659
   23          3.75529          0.92073
   24           3.7416         0.996354
   25          3.45775         0.924136
   26          3.21544         0.929925
   27          3.24828          1.01021
   28          2.97582          0.91612
   29          2.78871         0.937125
   30          2.82758          1.01394
   31          2.73914         0.968722
   32          2.80269           1.0232
   33          3.06974          1.09528
   34          3.04375         0.991535
   35          2.77571         0.911938
   36          2.37055         0.854034
   37          1.85274         0.781564
   38          1.54452         0.833644
   39          1.38217         0.894883
   40          1.19762         0.866478
   41          1.07262         0.895627
   42         0.943153         0.879299
   43         0.764226         0.810288
   44         0.653962         0.855718
   45         0.561079         0.857969
   46         0.455911         0.812561
   47         0.394167          0.86457
   48         0.329948         0.837077
   49         0.263774         0.799441
   50         0.232053          0.87974
   51         0.194184         0.836808
   52         0.160249         0.825246
   53         0.138139         0.862027
   54         0.112448          0.81402
   55        0.0949225         0.844146
   56        0.0776382         0.817912
   57        0.0642353         0.827367
   58        0.0518538         0.807248
   59        0.0407984         0.786797
   60         0.032845         0.805054
   61        0.0248163         0.755558
=== rate=0.859764, T=9.72358, TIT=0.159403, IT=61

 Elapsed time: 9.72358
Rank 0: Matrix-vector product took 0.0559282 seconds
Rank 1: Matrix-vector product took 0.0548343 seconds
Rank 2: Matrix-vector product took 0.0558293 seconds
Rank 3: Matrix-vector product took 0.0536839 seconds
Rank 4: Matrix-vector product took 0.0573016 seconds
Rank 5: Matrix-vector product took 0.0541059 seconds
Rank 6: Matrix-vector product took 0.0566251 seconds
Rank 7: Matrix-vector product took 0.0548023 seconds
Rank 8: Matrix-vector product took 0.0547783 seconds
Rank 9: Matrix-vector product took 0.0570122 seconds
Rank 10: Matrix-vector product took 0.0552731 seconds
Rank 11: Matrix-vector product took 0.0579172 seconds
Rank 12: Matrix-vector product took 0.0558472 seconds
Rank 13: Matrix-vector product took 0.0549091 seconds
Rank 14: Matrix-vector product took 0.0556292 seconds
Rank 15: Matrix-vector product took 0.0548656 seconds
Average time for Matrix-vector product is 0.0555839 seconds

Rank 0: copyOwnerToAll took 0.00069579 seconds
Rank 1: copyOwnerToAll took 0.00069579 seconds
Rank 2: copyOwnerToAll took 0.00069579 seconds
Rank 3: copyOwnerToAll took 0.00069579 seconds
Rank 4: copyOwnerToAll took 0.00069579 seconds
Rank 5: copyOwnerToAll took 0.00069579 seconds
Rank 6: copyOwnerToAll took 0.00069579 seconds
Rank 7: copyOwnerToAll took 0.00069579 seconds
Rank 8: copyOwnerToAll took 0.00069579 seconds
Rank 9: copyOwnerToAll took 0.00069579 seconds
Rank 10: copyOwnerToAll took 0.00069579 seconds
Rank 11: copyOwnerToAll took 0.00069579 seconds
Rank 12: copyOwnerToAll took 0.00069579 seconds
Rank 13: copyOwnerToAll took 0.00069579 seconds
Rank 14: copyOwnerToAll took 0.00069579 seconds
Rank 15: copyOwnerToAll took 0.00069579 seconds
Average time for copyOwnertoAll is 0.000724607 seconds
Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 247808
Cell on rank 1 before loadbalancing: 250352
Cell on rank 2 before loadbalancing: 252288
Cell on rank 3 before loadbalancing: 249552
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2247	575	551	85	0	977	1327	0	0	0	913	0	0	386	0	
From rank 1 to: 	2247	0	607	698	0	0	0	0	0	0	0	1249	0	0	0	0	
From rank 2 to: 	594	629	0	2315	1270	6	23	0	1246	674	0	463	0	0	0	0	
From rank 3 to: 	552	698	2304	0	137	960	137	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	85	0	1276	147	0	2077	1395	0	0	651	0	113	900	1398	130	0	
From rank 5 to: 	0	0	6	960	2082	0	1404	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	962	0	23	137	1398	1401	0	2045	0	0	0	0	192	0	829	0	
From rank 7 to: 	1327	0	0	0	0	0	2045	0	0	0	0	0	0	0	1358	0	
From rank 8 to: 	0	0	1248	0	0	0	0	0	0	2266	822	637	0	0	0	0	
From rank 9 to: 	0	0	689	0	651	0	0	0	2256	0	497	629	1012	1323	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	822	497	0	2405	134	0	0	1075	
From rank 11 to: 	913	1249	459	0	115	0	0	0	655	631	2400	0	176	0	965	173	
From rank 12 to: 	0	0	0	0	889	0	192	0	0	1012	134	168	0	2108	1266	1361	
From rank 13 to: 	0	0	0	0	1397	0	0	0	0	1323	0	0	2108	0	0	0	
From rank 14 to: 	400	0	0	0	117	0	844	1358	0	0	0	965	1264	0	0	2181	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	1075	169	1365	0	2180	0	
loadb
After loadbalancing process 7 has 66925 cells.
After loadbalancing process 5 has 66938 cells.
After loadbalancing process 8 has 67709 cells.
After loadbalancing process 3 has 66513 cells.
After loadbalancing process 13 has 67827 cells.
After loadbalancing process 1 has 67037 cells.
After loadbalancing process 0 has 68972 cells.
After loadbalancing process 2 has 69156 cells.
After loadbalancing process 6 has 69910 cells.
After loadbalancing process 10 has 68313 cells.
After loadbalancing process 15 has 67757 cells.
After loadbalancing process 12 has 68942 cells.
After loadbalancing process 9 has 69549 cells.
After loadbalancing process 14 has 68902 cells.
After loadbalancing process 4 has 70920 cells.
After loadbalancing process 11 has 71416 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2247	575	551	85	0	977	1327	0	0	0	913	0	0	386	0	
Rank 1's ghost cells:	2247	0	607	698	0	0	0	0	0	0	0	1249	0	0	0	0	
Rank 2's ghost cells:	594	629	0	2315	1270	6	23	0	1246	674	0	463	0	0	0	0	
Rank 3's ghost cells:	552	698	2304	0	137	960	137	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	85	0	1276	147	0	2077	1395	0	0	651	0	113	900	1398	130	0	
Rank 5's ghost cells:	0	0	6	960	2082	0	1404	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	962	0	23	137	1398	1401	0	2045	0	0	0	0	192	0	829	0	
Rank 7's ghost cells:	1327	0	0	0	0	0	2045	0	0	0	0	0	0	0	1358	0	
Rank 8's ghost cells:	0	0	1248	0	0	0	0	0	0	2266	822	637	0	0	0	0	
Rank 9's ghost cells:	0	0	689	0	651	0	0	0	2256	0	497	629	1012	1323	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	822	497	0	2405	134	0	0	1075	
Rank 11's ghost cells:	913	1249	459	0	115	0	0	0	655	631	2400	0	176	0	965	173	
Rank 12's ghost cells:	0	0	0	0	889	0	192	0	0	1012	134	168	0	2108	1266	1361	
Rank 13's ghost cells:	0	0	0	0	1397	0	0	0	0	1323	0	0	2108	0	0	0	
Rank 14's ghost cells:	400	0	0	0	117	0	844	1358	0	0	0	965	1264	0	0	2181	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	1075	169	1365	0	2180	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.2139         0.217029
    2          31.3935         0.579066
    3          22.0768         0.703229
    4          18.3615         0.831712
    5          16.8328         0.916741
    6          14.0497         0.834664
    7           11.672         0.830765
    8          11.2319          0.96229
    9           10.042         0.894062
   10          8.32751         0.829268
   11          8.09068         0.971561
   12          7.93465         0.980714
   13          6.59362         0.830991
   14          6.16597         0.935142
   15          6.32092          1.02513
   16           5.5786         0.882562
   17          5.00755         0.897636
   18          5.11132          1.02072
   19          4.79578         0.938266
   20          4.35193         0.907449
   21          4.36841          1.00379
   22           4.0786         0.933659
   23          3.75529          0.92073
   24           3.7416         0.996354
   25          3.45775         0.924136
   26          3.21544         0.929925
   27          3.24828          1.01021
   28          2.97582          0.91612
   29          2.78871         0.937125
   30          2.82758          1.01394
   31          2.73914         0.968722
   32          2.80269           1.0232
   33          3.06974          1.09528
   34          3.04375         0.991535
   35          2.77571         0.911938
   36          2.37055         0.854034
   37          1.85274         0.781564
   38          1.54452         0.833644
   39          1.38217         0.894883
   40          1.19762         0.866478
   41          1.07262         0.895627
   42         0.943153         0.879299
   43         0.764226         0.810288
   44         0.653962         0.855718
   45         0.561079         0.857969
   46         0.455911         0.812561
   47         0.394167          0.86457
   48         0.329948         0.837077
   49         0.263774         0.799441
   50         0.232053          0.87974
   51         0.194184         0.836808
   52         0.160249         0.825246
   53         0.138139         0.862027
   54         0.112448          0.81402
   55        0.0949225         0.844146
   56        0.0776382         0.817912
   57        0.0642353         0.827367
   58        0.0518538         0.807248
   59        0.0407984         0.786797
   60         0.032845         0.805054
   61        0.0248163         0.755558
=== rate=0.859764, T=9.71816, TIT=0.159314, IT=61

 Elapsed time: 9.71816
Rank 0: Matrix-vector product took 0.0559087 seconds
Rank 1: Matrix-vector product took 0.0542806 seconds
Rank 2: Matrix-vector product took 0.0558564 seconds
Rank 3: Matrix-vector product took 0.0544643 seconds
Rank 4: Matrix-vector product took 0.0571373 seconds
Rank 5: Matrix-vector product took 0.0542508 seconds
Rank 6: Matrix-vector product took 0.0564885 seconds
Rank 7: Matrix-vector product took 0.0548353 seconds
Rank 8: Matrix-vector product took 0.0548525 seconds
Rank 9: Matrix-vector product took 0.0567725 seconds
Rank 10: Matrix-vector product took 0.0551901 seconds
Rank 11: Matrix-vector product took 0.057643 seconds
Rank 12: Matrix-vector product took 0.0558257 seconds
Rank 13: Matrix-vector product took 0.0551492 seconds
Rank 14: Matrix-vector product took 0.0564603 seconds
Rank 15: Matrix-vector product took 0.0548314 seconds
Average time for Matrix-vector product is 0.0556217 seconds

Rank 0: copyOwnerToAll took 0.000679511 seconds
Rank 1: copyOwnerToAll took 0.000679511 seconds
Rank 2: copyOwnerToAll took 0.000679511 seconds
Rank 3: copyOwnerToAll took 0.000679511 seconds
Rank 4: copyOwnerToAll took 0.000679511 seconds
Rank 5: copyOwnerToAll took 0.000679511 seconds
Rank 6: copyOwnerToAll took 0.000679511 seconds
Rank 7: copyOwnerToAll took 0.000679511 seconds
Rank 8: copyOwnerToAll took 0.000679511 seconds
Rank 9: copyOwnerToAll took 0.000679511 seconds
Rank 10: copyOwnerToAll took 0.000679511 seconds
Rank 11: copyOwnerToAll took 0.000679511 seconds
Rank 12: copyOwnerToAll took 0.000679511 seconds
Rank 13: copyOwnerToAll took 0.000679511 seconds
Rank 14: copyOwnerToAll took 0.000679511 seconds
Rank 15: copyOwnerToAll took 0.000679511 seconds
Average time for copyOwnertoAll is 0.000698491 seconds
