Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	260	234	0	0	0	0	0	0	0	0	0	0	280	0	0	
From rank 1 to: 	260	0	46	240	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	234	46	0	250	0	0	30	220	0	0	0	0	230	10	0	0	
From rank 3 to: 	0	250	250	0	0	0	220	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	240	0	247	7	0	240	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	240	0	250	10	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	30	220	0	250	0	250	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	220	0	240	10	250	0	213	0	0	0	60	0	0	0	
From rank 8 to: 	0	0	0	0	7	0	0	213	0	230	270	30	240	0	20	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	230	0	0	230	0	0	270	0	
From rank 10 to: 	0	0	0	0	240	0	0	0	270	0	0	240	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	30	230	240	0	0	0	0	0	
From rank 12 to: 	0	0	230	0	0	0	0	60	240	0	0	0	0	240	230	0	
From rank 13 to: 	280	0	10	0	0	0	0	0	0	0	0	0	240	0	60	210	
From rank 14 to: 	0	0	0	0	0	0	0	0	20	260	0	0	230	60	0	270	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	210	270	0	
loadb
After loadbalancing process 11 has 6790 cells.
After loadbalancing process 1 has 6802 cells.
After loadbalancing process 15 has 6790 cells.
After loadbalancing process 5 has 6740 cells.
After loadbalancing process 14 has 7140 cells.
After loadbalancing process 6 has 7000 cells.
After loadbalancing process 10 has 6990 cells.
After loadbalancing process 0 has 7058 cells.
After loadbalancing process 2 has 7290 cells.
After loadbalancing process 9 has 6930 cells.
After loadbalancing process 8 has 7280 cells.
After loadbalancing process 4 has 6902 cells.
After loadbalancing process 3 has 6880 cells.
After loadbalancing process 13 has 7060 cells.
After loadbalancing process 12 has 7330 cells.
After loadbalancing process 7 has 7165 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	260	234	0	0	0	0	0	0	0	0	0	0	280	0	0	
Rank 1's ghost cells:	260	0	46	240	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	234	46	0	250	0	0	30	220	0	0	0	0	230	10	0	0	
Rank 3's ghost cells:	0	250	250	0	0	0	220	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	240	0	247	7	0	240	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	240	0	250	10	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	30	220	0	250	0	250	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	220	0	240	10	250	0	213	0	0	0	60	0	0	0	
Rank 8's ghost cells:	0	0	0	0	7	0	0	213	0	230	270	30	240	0	20	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	230	0	0	230	0	0	270	0	
Rank 10's ghost cells:	0	0	0	0	240	0	0	0	270	0	0	240	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	30	230	240	0	0	0	0	0	
Rank 12's ghost cells:	0	0	230	0	0	0	0	60	240	0	0	0	0	240	230	0	
Rank 13's ghost cells:	280	0	10	0	0	0	0	0	0	0	0	0	240	0	60	210	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	20	260	0	0	230	60	0	270	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	210	270	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.4108         0.220972
    2           21.444         0.605579
    3          20.1618         0.940205
    4           14.523         0.720322
    5          7.00884         0.482604
    6          4.52811         0.646057
    7          2.83518         0.626129
    8          1.84037          0.64912
    9          1.16738         0.634317
   10         0.688139         0.589473
   11         0.521703         0.758136
   12          0.25572         0.490165
   13         0.169878         0.664311
   14         0.106867         0.629081
   15        0.0626047         0.585819
   16        0.0402797         0.643398
   17        0.0262737          0.65228
   18        0.0157644         0.600008
=== rate=0.598938, T=0.324405, TIT=0.0180225, IT=18

 Elapsed time: 0.324405
Rank 0: Matrix-vector product took 0.00564624 seconds
Rank 1: Matrix-vector product took 0.00546461 seconds
Rank 2: Matrix-vector product took 0.00588793 seconds
Rank 3: Matrix-vector product took 0.00554903 seconds
Rank 4: Matrix-vector product took 0.0056496 seconds
Rank 5: Matrix-vector product took 0.00544275 seconds
Rank 6: Matrix-vector product took 0.00553593 seconds
Rank 7: Matrix-vector product took 0.00577074 seconds
Rank 8: Matrix-vector product took 0.00583503 seconds
Rank 9: Matrix-vector product took 0.00558413 seconds
Rank 10: Matrix-vector product took 0.0054939 seconds
Rank 11: Matrix-vector product took 0.00535076 seconds
Rank 12: Matrix-vector product took 0.00579155 seconds
Rank 13: Matrix-vector product took 0.00561094 seconds
Rank 14: Matrix-vector product took 0.00563435 seconds
Rank 15: Matrix-vector product took 0.00545599 seconds
Average time for Matrix-vector product is 0.00560647 seconds

Rank 0: copyOwnerToAll took 0.00020326 seconds
Rank 1: copyOwnerToAll took 0.00020326 seconds
Rank 2: copyOwnerToAll took 0.00020326 seconds
Rank 3: copyOwnerToAll took 0.00020326 seconds
Rank 4: copyOwnerToAll took 0.00020326 seconds
Rank 5: copyOwnerToAll took 0.00020326 seconds
Rank 6: copyOwnerToAll took 0.00020326 seconds
Rank 7: copyOwnerToAll took 0.00020326 seconds
Rank 8: copyOwnerToAll took 0.00020326 seconds
Rank 9: copyOwnerToAll took 0.00020326 seconds
Rank 10: copyOwnerToAll took 0.00020326 seconds
Rank 11: copyOwnerToAll took 0.00020326 seconds
Rank 12: copyOwnerToAll took 0.00020326 seconds
Rank 13: copyOwnerToAll took 0.00020326 seconds
Rank 14: copyOwnerToAll took 0.00020326 seconds
Rank 15: copyOwnerToAll took 0.00020326 seconds
Average time for copyOwnertoAll is 0.000249958 seconds
