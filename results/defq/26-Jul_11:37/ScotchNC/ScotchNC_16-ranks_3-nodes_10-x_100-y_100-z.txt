Number of cores/ranks per node is: 5
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 33500
Cell on rank 1 before loadbalancing: 33180
Cell on rank 2 before loadbalancing: 33320
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	0	310	0	180	150	0	0	0	0	0	0	0	0	140	0	
From rank 1 to: 	0	0	330	0	0	0	0	0	0	0	0	0	0	0	0	190	
From rank 2 to: 	310	330	0	0	0	0	0	0	0	0	0	0	0	0	60	85	
From rank 3 to: 	0	0	0	0	200	130	0	0	0	0	160	0	0	0	0	0	
From rank 4 to: 	180	0	0	210	0	290	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	150	0	0	130	290	0	0	0	0	190	100	0	0	0	90	0	
From rank 6 to: 	0	0	0	0	0	0	0	280	0	0	0	220	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	0	280	0	320	58	0	122	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	320	0	120	0	0	0	340	0	0	
From rank 9 to: 	0	0	0	0	0	190	0	58	120	0	180	160	0	20	270	0	
From rank 10 to: 	0	0	0	160	0	100	0	0	0	180	0	280	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	220	112	0	170	280	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	350	0	240	
From rank 13 to: 	0	0	0	0	0	0	0	0	340	20	0	0	350	0	230	60	
From rank 14 to: 	140	0	70	0	0	90	0	0	0	270	0	0	0	230	0	310	
From rank 15 to: 	0	185	85	0	0	0	0	0	0	0	0	0	230	60	310	0	
loadb
After loadbalancing process 1 has 6048 cells.
After loadbalancing process 10 has 6270 cells.
After loadbalancing process 12 has 8900 cells.
After loadbalancing process 4 has 6260 cells.
After loadbalancing process 15 has 9280 cells.
After loadbalancing process 11 has 6268 cells.
After loadbalancing process 2 has 6407 cells.
After loadbalancing process 3 has 6100 cells.
After loadbalancing process 9 has 6582 cells.
After loadbalancing process 7 has 6290 cells.
After loadbalancing process 0 has 6310 cells.
After loadbalancing process 13 has 9250 cells.
After loadbalancing process 14 has 9460 cells.
After loadbalancing process 5 has 6580 cells.
After loadbalancing process 8 has 6300 cells.
After loadbalancing process 6 has 6030 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	0	310	0	180	150	0	0	0	0	0	0	0	0	140	0	
Rank 1's ghost cells:	0	0	330	0	0	0	0	0	0	0	0	0	0	0	0	190	
Rank 2's ghost cells:	310	330	0	0	0	0	0	0	0	0	0	0	0	0	60	85	
Rank 3's ghost cells:	0	0	0	0	200	130	0	0	0	0	160	0	0	0	0	0	
Rank 4's ghost cells:	180	0	0	210	0	290	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	150	0	0	130	290	0	0	0	0	190	100	0	0	0	90	0	
Rank 6's ghost cells:	0	0	0	0	0	0	0	280	0	0	0	220	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	280	0	320	58	0	122	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	320	0	120	0	0	0	340	0	0	
Rank 9's ghost cells:	0	0	0	0	0	190	0	58	120	0	180	160	0	20	270	0	
Rank 10's ghost cells:	0	0	0	160	0	100	0	0	0	180	0	280	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	220	112	0	170	280	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	350	0	240	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	340	20	0	0	350	0	230	60	
Rank 14's ghost cells:	140	0	70	0	0	90	0	0	0	270	0	0	0	230	0	310	
Rank 15's ghost cells:	0	185	85	0	0	0	0	0	0	0	0	0	230	60	310	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.7963         0.223378
    2          21.6666         0.605275
    3          20.3496         0.939215
    4          15.3419         0.753916
    5           7.4964         0.488624
    6          4.67417         0.623522
    7          3.01561         0.645164
    8          1.96894         0.652915
    9          1.13658         0.577258
   10          0.69142         0.608331
   11         0.539104         0.779706
   12         0.263918         0.489549
   13         0.192005         0.727519
   14         0.128353         0.668484
   15        0.0700941         0.546106
   16        0.0463974          0.66193
   17         0.029253         0.630488
   18        0.0171833         0.587401
   19         0.011736         0.682989
=== rate=0.605834, T=0.505925, TIT=0.0266276, IT=19

 Elapsed time: 0.505925
Rank 0: Matrix-vector product took 0.00500157 seconds
Rank 1: Matrix-vector product took 0.00476037 seconds
Rank 2: Matrix-vector product took 0.0050964 seconds
Rank 3: Matrix-vector product took 0.00478362 seconds
Rank 4: Matrix-vector product took 0.00493624 seconds
Rank 5: Matrix-vector product took 0.00529602 seconds
Rank 6: Matrix-vector product took 0.00477385 seconds
Rank 7: Matrix-vector product took 0.00503312 seconds
Rank 8: Matrix-vector product took 0.00494518 seconds
Rank 9: Matrix-vector product took 0.00521541 seconds
Rank 10: Matrix-vector product took 0.0050611 seconds
Rank 11: Matrix-vector product took 0.00489995 seconds
Rank 12: Matrix-vector product took 0.00704219 seconds
Rank 13: Matrix-vector product took 0.00734839 seconds
Rank 14: Matrix-vector product took 0.00747139 seconds
Rank 15: Matrix-vector product took 0.00737156 seconds
Average time for Matrix-vector product is 0.00556477 seconds

Rank 0: copyOwnerToAll took 0.00022263 seconds
Rank 1: copyOwnerToAll took 0.00022263 seconds
Rank 2: copyOwnerToAll took 0.00022263 seconds
Rank 3: copyOwnerToAll took 0.00022263 seconds
Rank 4: copyOwnerToAll took 0.00022263 seconds
Rank 5: copyOwnerToAll took 0.00022263 seconds
Rank 6: copyOwnerToAll took 0.00022263 seconds
Rank 7: copyOwnerToAll took 0.00022263 seconds
Rank 8: copyOwnerToAll took 0.00022263 seconds
Rank 9: copyOwnerToAll took 0.00022263 seconds
Rank 10: copyOwnerToAll took 0.00022263 seconds
Rank 11: copyOwnerToAll took 0.00022263 seconds
Rank 12: copyOwnerToAll took 0.00022263 seconds
Rank 13: copyOwnerToAll took 0.00022263 seconds
Rank 14: copyOwnerToAll took 0.00022263 seconds
Rank 15: copyOwnerToAll took 0.00022263 seconds
Average time for copyOwnertoAll is 0.000247314 seconds
