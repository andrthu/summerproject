Number of cores/ranks per node is: 2
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 1247336
Cell on rank 1 before loadbalancing: 1241119
Cell on rank 2 before loadbalancing: 1237500
Cell on rank 3 before loadbalancing: 1240806
Cell on rank 4 before loadbalancing: 1257230
Cell on rank 5 before loadbalancing: 1262500
Cell on rank 6 before loadbalancing: 1253445
Cell on rank 7 before loadbalancing: 1260064


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	10147	0	0	0	0	0	10025	
From rank 1 to: 	10146	0	10046	0	0	0	0	0	
From rank 2 to: 	0	10045	0	10010	0	0	0	0	
From rank 3 to: 	0	0	10009	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	10378	11890	0	
From rank 5 to: 	0	0	0	0	10381	0	0	0	
From rank 6 to: 	0	0	0	0	11899	0	0	10201	
From rank 7 to: 	10025	0	0	0	0	0	10198	0	

Edge-cut for node partition: 22049



After loadbalancing process 4 has 1272881 cells.
After loadbalancing process 5 has 1267508 cells.
After loadbalancing process 1 has 1257555 cells.
After loadbalancing process 2 has 1250815 cells.
After loadbalancing process 0 has 1261311 cells.
After loadbalancing process 3 has 1279498 cells.
After loadbalancing process 7 has 1280287 cells.
After loadbalancing process 6 has 1275545 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          137.517         0.209808
    2          78.7024         0.572312
    3          55.3241         0.702953
    4          44.1561         0.798136
    5          40.6379         0.920322
    6          38.4342         0.945773
    7          30.5726         0.795453
    8          27.0171         0.883702
    9          27.1286          1.00413
   10          23.3083         0.859178
   11          20.4232         0.876219
   12           20.997           1.0281
   13          18.7416         0.892588
   14          16.2213         0.865523
   15          16.9916          1.04748
   16          15.9754         0.940197
   17           13.584         0.850308
   18           14.222          1.04696
   19          14.0232         0.986022
   20          12.1244         0.864597
   21          12.5608          1.03599
   22          11.7915          0.93876
   23          10.3415         0.877026
   24          10.6614          1.03093
   25          10.4113         0.976541
   26          9.39308         0.902202
   27          9.61596          1.02373
   28          9.31556          0.96876
   29          8.63402         0.926839
   30          9.29901          1.07702
   31          9.87775          1.06224
   32          10.6779          1.08101
   33          12.9735          1.21499
   34          12.6398         0.974273
   35          9.46961         0.749192
   36          6.74989         0.712795
   37          5.00435         0.741398
   38          4.79065         0.957296
   39          4.75503         0.992565
   40           3.5966         0.756378
   41          3.17465          0.88268
   42          3.32592          1.04765
   43          2.52033         0.757783
   44          2.08045         0.825469
   45          2.21094          1.06272
   46          1.70003          0.76892
   47          1.42083         0.835768
   48          1.52134          1.07074
   49          1.22677         0.806376
   50          1.17041         0.954057
   51          1.17246          1.00175
   52         0.954188         0.813831
   53          1.01617          1.06496
   54         0.894289         0.880057
   55         0.835127         0.933845
   56         0.893833           1.0703
   57         0.758644         0.848754
   58          0.81291          1.07153
   59         0.749925          0.92252
   60           0.7339         0.978631
   61         0.715596         0.975059
   62         0.693175         0.968668
   63         0.671968         0.969406
   64         0.594214         0.884289
   65         0.571016          0.96096
   66         0.437392          0.76599
   67         0.398664         0.911458
   68         0.305655         0.766698
   69         0.272946         0.892985
   70         0.238126         0.872432
   71         0.228401         0.959159
   72         0.212754         0.931494
   73         0.229866          1.08043
   74         0.234325           1.0194
   75         0.243979           1.0412
   76         0.218647         0.896169
   77          0.18763         0.858142
   78         0.170767         0.910124
   79         0.169807         0.994381
   80         0.139038         0.818801
   81         0.120443         0.866257
   82         0.114438         0.950147
   83         0.118925          1.03921
   84         0.107335         0.902544
   85         0.105917         0.986789
   86         0.099281         0.937347
   87        0.0982428         0.989543
   88        0.0872703         0.888312
   89        0.0855884         0.980728
   90        0.0768799         0.898251
   91        0.0733567         0.954173
   92        0.0673236         0.917757
   93        0.0608684         0.904116
=== rate=0.90499, T=265.076, TIT=2.85027, IT=93

 Elapsed time: 265.076
Rank 0: Matrix-vector product took 1.03703 seconds
Rank 1: Matrix-vector product took 1.03476 seconds
Rank 2: Matrix-vector product took 1.03017 seconds
Rank 3: Matrix-vector product took 1.0692 seconds
Rank 4: Matrix-vector product took 1.04679 seconds
Rank 5: Matrix-vector product took 1.04325 seconds
Rank 6: Matrix-vector product took 1.04971 seconds
Rank 7: Matrix-vector product took 1.05229 seconds
Average time for Matrix-vector product is 1.0454 seconds

Rank 0: copyOwnerToAll took 0.00153198 seconds
Rank 1: copyOwnerToAll took 0.00153198 seconds
Rank 2: copyOwnerToAll took 0.00153198 seconds
Rank 3: copyOwnerToAll took 0.00153198 seconds
Rank 4: copyOwnerToAll took 0.00153198 seconds
Rank 5: copyOwnerToAll took 0.00153198 seconds
Rank 6: copyOwnerToAll took 0.00153198 seconds
Rank 7: copyOwnerToAll took 0.00153198 seconds
Average time for copyOwnertoAll is 0.00167042 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	10046	0	0	0	10146	0	0	
Rank 1's ghost cells:	10045	0	10010	0	0	0	0	0	
Rank 2's ghost cells:	0	10009	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	0	0	10378	0	11890	0	
Rank 4's ghost cells:	0	0	0	10381	0	0	0	0	
Rank 5's ghost cells:	10147	0	0	0	0	0	0	10025	
Rank 6's ghost cells:	0	0	0	11899	0	0	0	10201	
Rank 7's ghost cells:	0	0	0	0	0	10025	10198	0	
