Number of cores/ranks per node is: 2
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 124682
Cell on rank 1 before loadbalancing: 124976
Cell on rank 2 before loadbalancing: 124061
Cell on rank 3 before loadbalancing: 124507
Cell on rank 4 before loadbalancing: 125853
Cell on rank 5 before loadbalancing: 125805
Cell on rank 6 before loadbalancing: 124093
Cell on rank 7 before loadbalancing: 126023


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2310	237	2235	0	0	217	2334	
From rank 1 to: 	2307	0	2320	32	0	205	2334	202	
From rank 2 to: 	230	2319	0	2322	337	2316	0	40	
From rank 3 to: 	2240	37	2319	0	2337	0	0	364	
From rank 4 to: 	0	0	344	2339	0	2352	0	2455	
From rank 5 to: 	0	196	2314	0	2358	0	2309	447	
From rank 6 to: 	217	2334	0	0	0	2309	0	2186	
From rank 7 to: 	2342	208	36	386	2452	443	2179	0	

Edge-cut for node partition: 27207



After loadbalancing process 0 has 131625 cells.
After loadbalancing process 4 has 132376 cells.
After loadbalancing process 2 has 132015 cells.
After loadbalancing process 5 has 131804 cells.
After loadbalancing process 6 has 133343 cells.
After loadbalancing process 7 has 133429 cells.
After loadbalancing process 3 has 131139 cells.
After loadbalancing process 1 has 134069 cells.


=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.1983         0.212964
    2          30.6139         0.575467
    3          21.5364         0.703483
    4          17.7044         0.822069
    5           16.445         0.928868
    6          14.1019         0.857518
    7          11.3774         0.806801
    8          10.7623         0.945934
    9          10.1106         0.939446
   10          8.34859         0.825727
   11          7.87959         0.943823
   12          7.88452          1.00063
   13          6.60205         0.837344
   14          5.96271          0.90316
   15          6.17523          1.03564
   16          5.61277         0.908917
   17          4.85187         0.864434
   18          4.91674          1.01337
   19          4.74433         0.964934
   20          4.16773         0.878465
   21          4.15877         0.997851
   22          4.02541         0.967932
   23          3.60539         0.895658
   24          3.58171         0.993432
   25          3.43311         0.958513
   26          3.06582         0.893015
   27          3.04921         0.994582
   28          2.99818         0.983264
   29          2.70017         0.900602
   30          2.69971         0.999833
   31          2.85962          1.05923
   32          2.87298          1.00467
   33          3.02215          1.05192
   34           2.9843         0.987476
   35          2.39319         0.801926
   36          1.91582          0.80053
   37          1.70983         0.892478
   38          1.48484         0.868418
   39          1.25102         0.842524
   40          1.09961         0.878975
   41         0.987503         0.898046
   42         0.831597         0.842121
   43         0.680763         0.818622
   44         0.612124         0.899173
   45         0.534791         0.873665
   46         0.418424         0.782408
   47         0.361224         0.863296
   48         0.307923         0.852443
   49         0.244175         0.792975
   50         0.210452          0.86189
   51          0.17311         0.822562
   52         0.147001         0.849178
   53         0.121022         0.823276
   54        0.0981989          0.81141
   55        0.0838592         0.853973
   56        0.0640195         0.763416
   57         0.054615         0.853099
   58        0.0432139         0.791245
   59        0.0333405         0.771524
   60        0.0272367         0.816925
   61        0.0201465         0.739682
=== rate=0.856831, T=18.2768, TIT=0.29962, IT=61

 Elapsed time: 18.2768
Rank 0: Matrix-vector product took 0.107434 seconds
Rank 1: Matrix-vector product took 0.110961 seconds
Rank 2: Matrix-vector product took 0.107602 seconds
Rank 3: Matrix-vector product took 0.106917 seconds
Rank 4: Matrix-vector product took 0.110412 seconds
Rank 5: Matrix-vector product took 0.107442 seconds
Rank 6: Matrix-vector product took 0.109006 seconds
Rank 7: Matrix-vector product took 0.108977 seconds
Average time for Matrix-vector product is 0.108594 seconds

Rank 0: copyOwnerToAll took 0.000648989 seconds
Rank 1: copyOwnerToAll took 0.000648989 seconds
Rank 2: copyOwnerToAll took 0.000648989 seconds
Rank 3: copyOwnerToAll took 0.000648989 seconds
Rank 4: copyOwnerToAll took 0.000648989 seconds
Rank 5: copyOwnerToAll took 0.000648989 seconds
Rank 6: copyOwnerToAll took 0.000648989 seconds
Rank 7: copyOwnerToAll took 0.000648989 seconds
Average time for copyOwnertoAll is 0.000674613 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	40	230	0	2319	2322	337	2316	
Rank 1's ghost cells:	36	0	2342	2179	208	386	2452	443	
Rank 2's ghost cells:	237	2334	0	217	2310	2235	0	0	
Rank 3's ghost cells:	0	2186	217	0	2334	0	0	2309	
Rank 4's ghost cells:	2320	202	2307	2334	0	32	0	205	
Rank 5's ghost cells:	2319	364	2240	0	37	0	2337	0	
Rank 6's ghost cells:	344	2455	0	0	0	2339	0	2352	
Rank 7's ghost cells:	2314	447	0	2309	196	0	2358	0	
