Number of cores/ranks per node is: 5
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 333331
Cell on rank 1 before loadbalancing: 333344
Cell on rank 2 before loadbalancing: 333325
Edge-cut: 19019


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2144	1699	0	0	0	0	1837	72	0	0	0	0	0	0	0	
From rank 1 to: 	2145	0	2758	0	1341	683	1205	907	585	0	0	0	0	0	0	0	
From rank 2 to: 	1736	2758	0	0	634	892	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	0	0	1651	1344	0	0	0	0	0	0	0	1981	0	0	
From rank 4 to: 	0	1341	618	1652	0	1505	470	0	0	0	0	0	1400	501	0	0	
From rank 5 to: 	0	692	899	1360	1491	0	73	0	0	0	0	0	772	277	0	0	
From rank 6 to: 	0	1213	0	0	475	73	0	1520	1424	0	2022	0	2963	0	3	0	
From rank 7 to: 	1827	907	0	0	0	0	1520	0	2088	555	752	0	0	0	0	0	
From rank 8 to: 	72	591	0	0	0	0	1447	2085	0	1186	983	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	555	1177	0	1101	1808	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	2024	767	1005	1192	0	3037	395	0	1309	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	1808	3042	0	0	0	1947	0	
From rank 12 to: 	0	0	0	0	1428	771	2959	0	0	0	351	0	0	3240	2178	643	
From rank 13 to: 	0	0	0	1980	501	295	0	0	0	0	0	0	3238	0	0	2151	
From rank 14 to: 	0	0	0	0	0	0	3	0	0	0	1286	1947	2178	0	0	3276	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	651	2150	3275	0	


After loadbalancing process 11 has 62346 cells.
After loadbalancing process 9 has 60199 cells.
After loadbalancing process 3 has 60544 cells.
After loadbalancing process 2 has 61583 cells.
After loadbalancing process 0 has 61312 cells.
After loadbalancing process 10 has 65299 cells.
After loadbalancing process 15 has 89411 cells.
After loadbalancing process 13 has 91496 cells.
After loadbalancing process 7 has 63206 cells.
After loadbalancing process 1 has 65173 cells.
After loadbalancing process 4 has 63035 cells.
After loadbalancing process 12 has 94916 cells.
After loadbalancing process 5 has 61107 cells.
After loadbalancing process 8 has 61920 cells.
After loadbalancing process 6 has 65247 cells.


After loadbalancing process 14 has 92003 cells.
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.8195         0.219454
    2          31.8118           0.5803
    3          22.4212         0.704807
    4          18.8246         0.839593
    5          17.1891         0.913118
    6          14.1646         0.824044
    7           11.853         0.836807
    8          11.4319         0.964472
    9           10.102         0.883669
   10          8.53544         0.844924
   11          8.39762         0.983854
   12          7.96872         0.948925
   13          6.77754         0.850518
   14          6.63909         0.979573
   15          6.54712         0.986148
   16          5.67109         0.866195
   17          5.44352         0.959873
   18          5.35721         0.984145
   19          4.74562         0.885838
   20          4.55082         0.958952
   21          4.48232         0.984947
   22          4.07218         0.908498
   23          3.98824         0.979387
   24           3.8274         0.959671
   25          3.45561         0.902861
   26          3.41885         0.989361
   27          3.33878         0.976582
   28          3.01827         0.904002
   29          2.96061         0.980898
   30          2.90766         0.982113
   31          2.73952         0.942173
   32          2.86271          1.04497
   33          3.05103          1.06579
   34          3.08725          1.01187
   35          3.09776           1.0034
   36          2.66171         0.859236
   37            1.984         0.745386
   38           1.5251         0.768701
   39          1.24387         0.815598
   40           1.0905         0.876697
   41           1.0101         0.926271
   42         0.865944         0.857288
   43         0.718293         0.829491
   44          0.61894         0.861682
   45         0.532145         0.859768
   46         0.456058         0.857019
   47         0.388613         0.852113
   48         0.311211         0.800825
   49         0.257562         0.827611
   50         0.223816         0.868979
   51         0.185694         0.829673
   52         0.156181          0.84107
   53         0.136448         0.873651
   54         0.111369         0.816199
   55        0.0933863         0.838532
   56        0.0767378         0.821724
   57        0.0615594         0.802205
   58        0.0519121         0.843285
   59        0.0416857         0.803006
   60        0.0335092         0.803853
   61        0.0271486         0.810185
   62        0.0211601         0.779416
=== rate=0.859649, T=14.0098, TIT=0.225964, IT=62

 Elapsed time: 14.0098
Rank 0: Matrix-vector product took 0.0503109 seconds
Rank 1: Matrix-vector product took 0.0538074 seconds
Rank 2: Matrix-vector product took 0.0497756 seconds
Rank 3: Matrix-vector product took 0.0488715 seconds
Rank 4: Matrix-vector product took 0.0510896 seconds
Rank 5: Matrix-vector product took 0.0506157 seconds
Rank 6: Matrix-vector product took 0.0530693 seconds
Rank 7: Matrix-vector product took 0.0509047 seconds
Rank 8: Matrix-vector product took 0.0504701 seconds
Rank 9: Matrix-vector product took 0.0491742 seconds
Rank 10: Matrix-vector product took 0.0531964 seconds
Rank 11: Matrix-vector product took 0.0508055 seconds
Rank 12: Matrix-vector product took 0.0851993 seconds
Rank 13: Matrix-vector product took 0.0742007 seconds
Rank 14: Matrix-vector product took 0.0803738 seconds
Rank 15: Matrix-vector product took 0.0730154 seconds
Average time for Matrix-vector product is 0.057805 seconds

Rank 0: copyOwnerToAll took 0.00091233 seconds
Rank 1: copyOwnerToAll took 0.00091233 seconds
Rank 2: copyOwnerToAll took 0.00091233 seconds
Rank 3: copyOwnerToAll took 0.00091233 seconds
Rank 4: copyOwnerToAll took 0.00091233 seconds
Rank 5: copyOwnerToAll took 0.00091233 seconds
Rank 6: copyOwnerToAll took 0.00091233 seconds
Rank 7: copyOwnerToAll took 0.00091233 seconds
Rank 8: copyOwnerToAll took 0.00091233 seconds
Rank 9: copyOwnerToAll took 0.00091233 seconds
Rank 10: copyOwnerToAll took 0.00091233 seconds
Rank 11: copyOwnerToAll took 0.00091233 seconds
Rank 12: copyOwnerToAll took 0.00091233 seconds
Rank 13: copyOwnerToAll took 0.00091233 seconds
Rank 14: copyOwnerToAll took 0.00091233 seconds
Rank 15: copyOwnerToAll took 0.00091233 seconds
Average time for copyOwnertoAll is 0.000983783 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2144	1699	0	0	0	0	1837	72	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2145	0	2758	0	1341	683	1205	907	585	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1736	2758	0	0	634	892	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	0	0	1651	1344	0	0	0	0	0	0	0	1981	0	0	
Rank 4's ghost cells:	0	1341	618	1652	0	1505	470	0	0	0	0	0	1400	501	0	0	
Rank 5's ghost cells:	0	692	899	1360	1491	0	73	0	0	0	0	0	772	277	0	0	
Rank 6's ghost cells:	0	1213	0	0	475	73	0	1520	1424	0	2022	0	2963	0	3	0	
Rank 7's ghost cells:	1827	907	0	0	0	0	1520	0	2088	555	752	0	0	0	0	0	
Rank 8's ghost cells:	72	591	0	0	0	0	1447	2085	0	1186	983	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	555	1177	0	1101	1808	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	2024	767	1005	1192	0	3037	395	0	1309	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	1808	3042	0	0	0	1947	0	
Rank 12's ghost cells:	0	0	0	0	1428	771	2959	0	0	0	351	0	0	3240	2178	643	
Rank 13's ghost cells:	0	0	0	1980	501	295	0	0	0	0	0	0	3238	0	0	2151	
Rank 14's ghost cells:	0	0	0	0	0	0	3	0	0	0	1286	1947	2178	0	0	3276	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	651	2150	3275	0	
