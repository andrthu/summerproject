Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2555	291	2319	0	0	2297	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2556	0	2077	96	2597	0	149	0	2317	0	0	0	0	0	0	329	
From rank 2 to: 	258	2076	0	2556	0	0	0	0	321	2329	0	0	0	0	0	0	
From rank 3 to: 	2303	96	2557	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	2578	0	0	0	2531	2327	172	100	0	0	0	0	174	0	2160	
From rank 5 to: 	0	0	0	0	2531	0	144	2282	0	0	0	0	0	2390	0	96	
From rank 6 to: 	2297	136	0	0	2325	141	0	2526	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	179	2282	2524	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	2355	336	0	100	0	0	0	0	2562	2146	148	0	0	126	2124	
From rank 9 to: 	0	0	2329	0	0	0	0	0	2568	0	155	2328	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	2155	182	0	2572	0	0	2505	150	
From rank 11 to: 	0	0	0	0	0	0	0	0	151	2322	2572	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	2392	2130	393	
From rank 13 to: 	0	0	0	0	175	2391	0	0	0	0	0	0	2392	0	365	2161	
From rank 14 to: 	0	0	0	0	0	0	0	0	126	0	2505	0	2130	360	0	2635	
From rank 15 to: 	0	328	0	0	2145	83	0	0	2130	0	155	0	434	2168	2631	0	


After loadbalancing process 7 has 67477 cells.
After loadbalancing process 3 has 67453 cells.
After loadbalancing process 6 has 69922 cells.
After loadbalancing process 11 has 67544 cells.
After loadbalancing process 2 has 70048 cells.
After loadbalancing process 12 has 67412 cells.
After loadbalancing process 0 has 69960 cells.
After loadbalancing process 9 has 69880 cells.
After loadbalancing process 10 has 70058 cells.
After loadbalancing process 14 has 70247 cells.
After loadbalancing process 13 has 69986 cells.
After loadbalancing process 5 has 69952 cells.
After loadbalancing process 4 has 72540 cells.
After loadbalancing process 1 has 72617 cells.
After loadbalancing process 15 has 72592 cells.
After loadbalancing process 8 has 72401 cells.


=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1           54.739         0.219131
    2          31.8079         0.581083
    3          22.3508         0.702679
    4          18.6044         0.832384
    5          16.9186         0.909385
    6          14.0817         0.832325
    7            11.81         0.838678
    8          11.3655         0.962362
    9          10.0519         0.884422
   10          8.39649         0.835311
   11          8.17853         0.974041
   12          8.04259         0.983378
   13          6.74228         0.838323
   14          6.34202         0.940634
   15           6.4628          1.01904
   16          5.70649         0.882975
   17           5.2695         0.923422
   18          5.35393          1.01602
   19          4.90602          0.91634
   20          4.56274         0.930028
   21          4.52688         0.992141
   22          4.12053         0.910236
   23          3.87596         0.940648
   24          3.82808         0.987647
   25          3.50597         0.915855
   26          3.26335         0.930799
   27          3.24218         0.993513
   28          3.03183         0.935121
   29          2.79891         0.923175
   30          2.82228          1.00835
   31          2.78122         0.985452
   32          2.77303         0.997056
   33          3.01757          1.08818
   34          3.16191          1.04783
   35           2.9568         0.935129
   36          2.51396         0.850231
   37          1.86339         0.741218
   38          1.33422         0.716015
   39           1.1093         0.831422
   40           1.0314         0.929776
   41         0.970865         0.941309
   42         0.837386         0.862515
   43         0.677332         0.808865
   44         0.589025         0.869625
   45         0.507825         0.862146
   46         0.406366         0.800209
   47         0.345878         0.851148
   48         0.293231         0.847787
   49         0.230471          0.78597
   50         0.197515         0.857008
   51         0.174134         0.881622
   52         0.145839          0.83751
   53         0.120593         0.826893
   54         0.101233         0.839456
   55        0.0855619         0.845202
   56        0.0686735         0.802617
   57        0.0578825         0.842866
   58        0.0482879          0.83424
   59        0.0377535         0.781841
   60        0.0295131         0.781732
   61        0.0225514         0.764114
=== rate=0.858416, T=9.95795, TIT=0.163245, IT=61

 Elapsed time: 9.95795
Rank 0: Matrix-vector product took 0.0568791 seconds
Rank 1: Matrix-vector product took 0.0597084 seconds
Rank 2: Matrix-vector product took 0.0575799 seconds
Rank 3: Matrix-vector product took 0.0547293 seconds
Rank 4: Matrix-vector product took 0.0596949 seconds
Rank 5: Matrix-vector product took 0.056693 seconds
Rank 6: Matrix-vector product took 0.0576055 seconds
Rank 7: Matrix-vector product took 0.0547428 seconds
Rank 8: Matrix-vector product took 0.0594643 seconds
Rank 9: Matrix-vector product took 0.057511 seconds
Rank 10: Matrix-vector product took 0.0575785 seconds
Rank 11: Matrix-vector product took 0.055898 seconds
Rank 12: Matrix-vector product took 0.0554629 seconds
Rank 13: Matrix-vector product took 0.0567848 seconds
Rank 14: Matrix-vector product took 0.0564986 seconds
Rank 15: Matrix-vector product took 0.0587368 seconds
Average time for Matrix-vector product is 0.057223 seconds

Rank 0: copyOwnerToAll took 0.000758769 seconds
Rank 1: copyOwnerToAll took 0.000758769 seconds
Rank 2: copyOwnerToAll took 0.000758769 seconds
Rank 3: copyOwnerToAll took 0.000758769 seconds
Rank 4: copyOwnerToAll took 0.000758769 seconds
Rank 5: copyOwnerToAll took 0.000758769 seconds
Rank 6: copyOwnerToAll took 0.000758769 seconds
Rank 7: copyOwnerToAll took 0.000758769 seconds
Rank 8: copyOwnerToAll took 0.000758769 seconds
Rank 9: copyOwnerToAll took 0.000758769 seconds
Rank 10: copyOwnerToAll took 0.000758769 seconds
Rank 11: copyOwnerToAll took 0.000758769 seconds
Rank 12: copyOwnerToAll took 0.000758769 seconds
Rank 13: copyOwnerToAll took 0.000758769 seconds
Rank 14: copyOwnerToAll took 0.000758769 seconds
Rank 15: copyOwnerToAll took 0.000758769 seconds
Average time for copyOwnertoAll is 0.000786553 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2555	291	2319	0	0	2297	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2556	0	2077	96	2597	0	149	0	2317	0	0	0	0	0	0	329	
Rank 2's ghost cells:	258	2076	0	2556	0	0	0	0	321	2329	0	0	0	0	0	0	
Rank 3's ghost cells:	2303	96	2557	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	2578	0	0	0	2531	2327	172	100	0	0	0	0	174	0	2160	
Rank 5's ghost cells:	0	0	0	0	2531	0	144	2282	0	0	0	0	0	2390	0	96	
Rank 6's ghost cells:	2297	136	0	0	2325	141	0	2526	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	179	2282	2524	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	2355	336	0	100	0	0	0	0	2562	2146	148	0	0	126	2124	
Rank 9's ghost cells:	0	0	2329	0	0	0	0	0	2568	0	155	2328	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	2155	182	0	2572	0	0	2505	150	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	151	2322	2572	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	2392	2130	393	
Rank 13's ghost cells:	0	0	0	0	175	2391	0	0	0	0	0	0	2392	0	365	2161	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	126	0	2505	0	2130	360	0	2635	
Rank 15's ghost cells:	0	328	0	0	2145	83	0	0	2130	0	155	0	434	2168	2631	0	
