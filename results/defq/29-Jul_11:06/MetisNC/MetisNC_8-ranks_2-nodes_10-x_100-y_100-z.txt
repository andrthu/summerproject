Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	450	230	0	0	0	300	250	
From rank 1 to: 	450	0	303	0	0	0	0	0	
From rank 2 to: 	227	303	0	430	0	182	48	0	
From rank 3 to: 	0	0	440	0	0	270	0	0	
From rank 4 to: 	0	0	0	0	0	420	230	0	
From rank 5 to: 	0	0	178	270	420	0	297	0	
From rank 6 to: 	310	0	48	0	230	297	0	410	
From rank 7 to: 	250	0	0	0	0	0	410	0	


After loadbalancing process 4 has 13130 cells.
After loadbalancing process 1 has 13204 cells.
After loadbalancing process 3 has 13200 cells.
After loadbalancing process 7 has 13148 cells.
After loadbalancing process 5 has 13644 cells.
After loadbalancing process 0 has 13849 cells.
After loadbalancing process 6 has 13788 cells.
After loadbalancing process 2 has 13690 cells.


=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1           35.096         0.219008
    2          21.1303         0.602074
    3          20.2317         0.957473
    4          14.5342         0.718389
    5           7.1508         0.491996
    6          4.60833          0.64445
    7          2.71698         0.589579
    8          1.87401         0.689739
    9          1.11532         0.595154
   10         0.663333         0.594746
   11         0.531613         0.801427
   12          0.25824         0.485767
   13          0.18097         0.700781
   14         0.122043         0.674382
   15        0.0614493         0.503507
   16        0.0427871         0.696298
   17         0.028414          0.66408
   18        0.0159588         0.561654
=== rate=0.599347, T=0.582364, TIT=0.0323535, IT=18

 Elapsed time: 0.582364
Rank 0: Matrix-vector product took 0.0112479 seconds
Rank 1: Matrix-vector product took 0.0106534 seconds
Rank 2: Matrix-vector product took 0.0110364 seconds
Rank 3: Matrix-vector product took 0.0107225 seconds
Rank 4: Matrix-vector product took 0.0108052 seconds
Rank 5: Matrix-vector product took 0.0109973 seconds
Rank 6: Matrix-vector product took 0.0111502 seconds
Rank 7: Matrix-vector product took 0.0106309 seconds
Average time for Matrix-vector product is 0.0109055 seconds

Rank 0: copyOwnerToAll took 0.00018748 seconds
Rank 1: copyOwnerToAll took 0.00018748 seconds
Rank 2: copyOwnerToAll took 0.00018748 seconds
Rank 3: copyOwnerToAll took 0.00018748 seconds
Rank 4: copyOwnerToAll took 0.00018748 seconds
Rank 5: copyOwnerToAll took 0.00018748 seconds
Rank 6: copyOwnerToAll took 0.00018748 seconds
Rank 7: copyOwnerToAll took 0.00018748 seconds
Average time for copyOwnertoAll is 0.000219941 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	450	230	0	0	0	300	250	
Rank 1's ghost cells:	450	0	303	0	0	0	0	0	
Rank 2's ghost cells:	227	303	0	430	0	182	48	0	
Rank 3's ghost cells:	0	0	440	0	0	270	0	0	
Rank 4's ghost cells:	0	0	0	0	0	420	230	0	
Rank 5's ghost cells:	0	0	178	270	420	0	297	0	
Rank 6's ghost cells:	310	0	48	0	230	297	0	410	
Rank 7's ghost cells:	250	0	0	0	0	0	410	0	
