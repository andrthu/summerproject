Number of cores/ranks per node is: 16
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 2499572
Cell on rank 1 before loadbalancing: 2497852
Cell on rank 2 before loadbalancing: 2503005
Cell on rank 3 before loadbalancing: 2499571
Edge-cut: 62194


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	4137	1666	1685	1789	1188	2905	0	577	0	0	0	1173	0	0	961	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	4101	0	322	663	0	0	2136	0	0	0	0	0	1282	0	0	3836	0	0	0	0	0	1343	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	1636	315	0	4710	376	1917	0	0	1873	0	0	0	667	3548	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	1662	666	4710	0	2395	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	1791	0	378	2361	0	5374	615	835	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	1204	0	1913	0	5375	0	866	897	1194	4620	1	390	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	2899	2134	0	0	631	873	0	4387	524	99	1821	0	0	0	0	1783	0	0	0	0	0	1288	0	0	0	0	0	0	0	355	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	828	880	4387	0	0	12	1351	2016	0	0	0	0	0	0	0	0	0	6	0	0	0	49	0	0	1679	902	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	573	0	1864	0	0	1223	524	0	0	5159	1907	240	3207	2864	17	224	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	4634	103	12	5160	0	399	2214	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	1	1815	1373	1875	405	0	5596	570	0	3553	1262	0	0	0	0	0	99	278	336	623	462	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	387	0	2024	240	2223	5598	0	0	0	0	0	0	0	0	0	0	0	0	0	1091	908	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	1176	1281	667	0	0	0	0	0	3210	0	563	0	0	2605	1840	1238	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	3556	0	0	0	0	0	2869	0	0	0	2602	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	16	0	3561	0	1840	0	0	4154	0	0	0	0	0	0	1788	387	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	964	3856	0	0	0	0	1780	0	209	0	1272	0	1248	0	4152	0	0	0	0	0	0	794	0	1317	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4361	3683	2054	2244	0	0	565	0	48	0	1525	0	0	715	0	36	93	0	0	0	0	0	0	0	239	0	0	0	0	1737	487	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4381	0	0	0	2119	0	0	0	0	0	0	33	0	0	3590	0	2432	715	0	0	0	0	0	0	0	0	0	0	0	0	135	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3707	0	0	3132	0	0	0	0	0	0	2634	908	0	0	0	0	0	0	0	0	0	0	0	0	0	620	0	0	0	0	18	2118	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2085	0	3110	0	0	0	2691	1656	855	530	878	741	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2247	2117	0	0	0	3496	0	2803	0	115	0	0	0	1144	2546	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	1287	0	0	0	0	1272	7	0	0	99	0	0	0	0	792	0	0	0	0	3478	0	0	3660	0	57	0	0	0	4164	124	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	284	0	0	0	1811	0	0	0	0	2689	0	0	0	4882	2693	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	334	0	0	0	390	1317	563	0	0	1671	2782	3641	4887	0	362	2729	0	0	0	133	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	613	1103	0	0	0	0	0	0	0	870	0	0	2693	397	0	5619	1594	302	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	42	0	0	463	935	0	0	0	0	49	0	0	516	92	56	138	2729	5623	0	0	2435	2921	3552	347	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2641	878	0	0	0	0	1624	0	0	5349	0	0	0	0	0	0	0	0	0	0	0	0	935	743	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1533	33	919	739	0	0	0	0	304	2398	5330	0	52	25	2589	3246	0	216	0	0	0	0	553	0	344	499	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	1686	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	2928	0	50	0	5525	0	1390	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	354	907	0	0	75	0	0	0	0	0	0	0	0	0	1144	4136	0	115	0	3562	0	26	5528	0	1397	667	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	755	3588	0	0	2553	121	0	0	0	346	0	2595	0	1395	0	4730	25	1716	0	0	0	0	113	201	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	43	0	3235	1382	665	4741	0	0	5	0	0	0	0	672	1167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	2424	0	0	0	0	0	0	0	0	0	0	0	0	23	0	0	4657	1890	760	0	0	0	0	0	0	0	0	0	0	3243	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	704	0	0	0	0	0	0	0	0	0	213	0	0	1710	5	4648	0	0	1465	0	0	3162	3740	0	2058	0	0	0	0	1510	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1887	0	0	5560	0	0	0	0	0	0	0	0	0	3137	623	0	0	0	0	0	0	0	0	0	44	1709	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	767	1473	5563	0	0	5550	776	427	0	0	330	0	0	2883	744	0	0	0	0	0	0	0	0	0	1015	402	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6002	1133	1304	149	0	0	2681	0	0	0	0	0	0	900	910	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5557	6006	0	595	878	0	0	2221	540	0	211	0	0	0	0	486	874	0	0	0	0	1139	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	559	0	0	108	683	0	3127	0	786	1110	589	0	4729	4156	1465	540	80	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	196	1174	0	3733	0	424	1305	892	4737	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	973	355	0	0	0	0	0	0	0	0	159	0	4148	0	0	5586	211	2084	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	640	0	0	0	0	0	0	0	741	506	0	0	0	0	0	2049	0	0	0	0	1478	0	5581	0	1003	137	103	0	2490	3409	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	321	0	2246	539	0	219	999	0	5769	3221	3229	309	0	0	0	69	0	0	597	0	0	513	0	0	0	0	0	628	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2648	546	82	0	2098	137	5758	0	0	0	0	0	0	0	332	0	0	1321	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	101	3229	0	0	5074	358	1690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	524	1033	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3147	2832	0	209	0	0	0	0	3251	0	5052	0	1967	129	0	0	0	0	0	0	0	0	322	773	0	0	0	0	360	474	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1723	121	17	0	0	0	0	0	0	0	0	0	0	0	0	0	3243	1452	632	719	0	0	210	0	0	2527	316	0	360	1973	0	4653	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	476	0	2129	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3442	0	0	1683	130	4676	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4930	582	1116	711	0	3243	0	0	0	3388	2332	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4928	0	997	1370	849	0	2899	0	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	918	472	0	0	0	0	67	340	0	0	0	0	580	991	0	4578	1493	4126	0	0	2742	0	272	366	0	0	14	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	905	868	0	0	0	0	0	0	0	0	0	0	1139	1366	4586	0	0	0	0	0	2728	0	802	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	830	1494	0	0	2646	1295	1735	0	0	0	198	537	1222	1711	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	594	1341	0	0	0	0	0	0	4141	0	2669	0	0	0	930	0	0	0	0	0	3281	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3224	2891	0	0	1303	0	0	5384	0	0	0	350	1555	509	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1766	0	5388	0	0	0	0	0	0	3078	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	1017	0	1133	0	0	0	0	524	0	0	306	0	0	0	0	2750	2721	0	934	0	0	0	4775	1102	382	0	0	2889	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1674	396	0	0	0	0	0	0	0	0	0	769	0	0	0	0	0	0	0	0	0	0	4779	0	1258	1337	0	0	757	2633	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3451	0	290	799	0	0	0	0	1104	1276	0	6073	0	0	0	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2328	0	366	0	225	0	354	0	396	1364	6048	0	5251	0	953	618	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	537	0	1557	0	0	0	0	5255	0	6214	889	1453	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1235	0	522	3080	0	0	0	0	6250	0	491	0	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	643	0	532	354	0	0	0	0	12	0	1696	3282	0	0	2852	758	0	955	892	473	0	5508	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1036	476	0	0	0	0	0	0	0	0	0	0	0	2641	0	610	1455	0	5509	0	


After loadbalancing process 13 has 165234 cells.
After loadbalancing process 61 has 167799 cells.
After loadbalancing process 12 has 168807 cells.
After loadbalancing process 9 has 168739 cells.
After loadbalancing process 60 has 172130 cells.
After loadbalancing process 49 has 167268 cells.
After loadbalancing process 55 has 166458 cells.
After loadbalancing process 47 has 168971 cells.
After loadbalancing process 52 has 168604 cells.
After loadbalancing process 3 has 165657 cells.
After loadbalancing process 14 has 167983 cells.
After loadbalancing process 54 has 171443 cells.
After loadbalancing process 5 has 172683 cells.
After loadbalancing process 0 has 172303 cells.
After loadbalancing process 4 has 167584 cells.
After loadbalancing process 8 has 174020 cells.
After loadbalancing process 59 has 174129 cells.
After loadbalancing process 48 has 172527 cells.
After loadbalancing process 51 has 168619 cells.
After loadbalancing process 2 has 171266 cells.
After loadbalancing process 15 has 171815 cells.
After loadbalancing process 1 has 169907 cells.
After loadbalancing process 34 has 169387 cells.
After loadbalancing process 20 has 170591 cells.
After loadbalancing process 63 has 167954 cells.
After loadbalancing process 10 has 174555 cells.
After loadbalancing process 28 has 167808 cells.
After loadbalancing process 46 has 174376 cells.
After loadbalancing process 62 has 174182 cells.
After loadbalancing process 11 has 168810 cells.
After loadbalancing process 22 has 168631 cells.
After loadbalancing process 19 has 168658 cells.
After loadbalancing process 40 has 169953 cells.
After loadbalancing process 42 has 175102 cells.
After loadbalancing process 41 has 174791 cells.
After loadbalancing process 58 has 169198 cells.
After loadbalancing process 24 has 169301 cells.
After loadbalancing process 38 has 174583 cells.
After loadbalancing process 16 has 173901 cells.
After loadbalancing process 18 has 169244 cells.
After loadbalancing process 36 has 169507 cells.
After loadbalancing process 30 has 174257 cells.
After loadbalancing process 6 has 173015 cells.
After loadbalancing process 7 has 168334 cells.
After loadbalancing process 32 has 169473 cells.
After loadbalancing process 39 has 168866 cells.
After loadbalancing process 50 has 173187 cells.
After loadbalancing process 53 has 169173 cells.
After loadbalancing process 56 has 174784 cells.
After loadbalancing process 44 has 168451 cells.
After loadbalancing process 31 has 168013 cells.
After loadbalancing process 57 has 169827 cells.
After loadbalancing process 45 has 174955 cells.
After loadbalancing process 37 has 174944 cells.
After loadbalancing process 23 has 174930 cells.
After loadbalancing process 35 has 176363 cells.
After loadbalancing process 43 has 169356 cells.
After loadbalancing process 25 has 176068 cells.
After loadbalancing process 27 has 174891 cells.
After loadbalancing process 26 has 168281 cells.
After loadbalancing process 33 has 175803 cells.
After loadbalancing process 21 has 171062 cells.
After loadbalancing process 17 has 169534 cells.
After loadbalancing process 29 has 174030 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          143.077         0.218292
    2          82.9895         0.580035
    3          58.7534         0.707962
    4          49.3505          0.83996
    5          45.5804         0.923604
    6          38.2428          0.83902
    7          32.0707         0.838607
    8          31.1719         0.971976
    9          27.1292         0.870309
   10          23.2475         0.856917
   11           23.307          1.00256
   12          21.3676          0.91679
   13          18.3951         0.860886
   14          18.6721          1.01506
   15          17.5864         0.941853
   16          15.4013          0.87575
   17           15.507          1.00686
   18          14.8405         0.957023
   19          13.3634         0.900466
   20          13.4874          1.00928
   21          12.6644         0.938979
   22          11.6327         0.918539
   23          11.8555          1.01915
   24          11.0364         0.930909
   25           10.183         0.922673
   26          10.4066          1.02196
   27           9.8095         0.942618
   28          9.09193         0.926849
   29          9.35869          1.02934
   30          9.09212         0.971516
   31          8.92258         0.981353
   32          9.91567           1.1113
   33          10.7976          1.08894
   34          11.7606          1.08918
   35          12.3128          1.04695
   36          10.2471         0.832233
   37          7.92152         0.773051
   38          6.63945         0.838153
   39           5.6265         0.847435
   40          5.05694         0.898771
   41          4.58537         0.906748
   42          4.02845         0.878545
   43          3.64401         0.904569
   44          3.10011         0.850741
   45          2.69982         0.870879
   46          2.44453         0.905441
   47          2.05061         0.838859
   48          1.82074         0.887902
   49          1.67393         0.919367
   50          1.45376         0.868473
   51          1.35688         0.933356
   52          1.26042         0.928909
   53          1.15587          0.91705
   54          1.08767         0.941004
   55          1.01986         0.937654
   56         0.962159          0.94342
   57         0.905728         0.941349
   58         0.876296         0.967505
   59         0.816916         0.932238
   60         0.795232         0.973456
   61         0.783523         0.985276
   62         0.742129         0.947169
   63         0.746391          1.00574
   64         0.716272         0.959647
   65         0.690137         0.963513
   66         0.656557         0.951342
   67          0.60144         0.916052
   68          0.52911         0.879739
   69         0.463793         0.876553
   70         0.380967         0.821415
   71         0.318064         0.834886
   72         0.276234         0.868485
   73         0.248082         0.898089
   74         0.247121         0.996125
   75         0.254105          1.02826
   76         0.253319         0.996908
   77         0.247587         0.977371
   78         0.238633         0.963837
   79         0.222686          0.93317
   80         0.209492         0.940752
   81         0.188314         0.898906
   82         0.167429         0.889094
   83         0.154327          0.92175
   84          0.14169         0.918114
   85         0.131964         0.931352
   86         0.126316         0.957207
   87         0.119216         0.943789
   88         0.113617         0.953032
   89         0.110079         0.968865
   90         0.103035         0.936008
   91        0.0989534         0.960384
   92        0.0954985         0.965086
   93        0.0900683         0.943138
   94        0.0852229         0.946203
   95        0.0800594         0.939412
   96        0.0729471         0.911162
   97        0.0668148         0.915936
   98         0.059969          0.89754
=== rate=0.909473, T=41.8468, TIT=0.427009, IT=98

 Elapsed time: 41.8468
Rank 0: Matrix-vector product took 0.140195 seconds
Rank 1: Matrix-vector product took 0.138075 seconds
Rank 2: Matrix-vector product took 0.139858 seconds
Rank 3: Matrix-vector product took 0.134609 seconds
Rank 4: Matrix-vector product took 0.136049 seconds
Rank 5: Matrix-vector product took 0.140119 seconds
Rank 6: Matrix-vector product took 0.141459 seconds
Rank 7: Matrix-vector product took 0.137384 seconds
Rank 8: Matrix-vector product took 0.141555 seconds
Rank 9: Matrix-vector product took 0.137627 seconds
Rank 10: Matrix-vector product took 0.141539 seconds
Rank 11: Matrix-vector product took 0.140052 seconds
Rank 12: Matrix-vector product took 0.144961 seconds
Rank 13: Matrix-vector product took 0.13502 seconds
Rank 14: Matrix-vector product took 0.137222 seconds
Rank 15: Matrix-vector product took 0.140168 seconds
Rank 16: Matrix-vector product took 0.141963 seconds
Rank 17: Matrix-vector product took 0.141132 seconds
Rank 18: Matrix-vector product took 0.137973 seconds
Rank 19: Matrix-vector product took 0.137001 seconds
Rank 20: Matrix-vector product took 0.138638 seconds
Rank 21: Matrix-vector product took 0.139574 seconds
Rank 22: Matrix-vector product took 0.141892 seconds
Rank 23: Matrix-vector product took 0.142422 seconds
Rank 24: Matrix-vector product took 0.14099 seconds
Rank 25: Matrix-vector product took 0.145706 seconds
Rank 26: Matrix-vector product took 0.139845 seconds
Rank 27: Matrix-vector product took 0.145295 seconds
Rank 28: Matrix-vector product took 0.136195 seconds
Rank 29: Matrix-vector product took 0.142061 seconds
Rank 30: Matrix-vector product took 0.142061 seconds
Rank 31: Matrix-vector product took 0.13647 seconds
Rank 32: Matrix-vector product took 0.142715 seconds
Rank 33: Matrix-vector product took 0.142521 seconds
Rank 34: Matrix-vector product took 0.138237 seconds
Rank 35: Matrix-vector product took 0.143667 seconds
Rank 36: Matrix-vector product took 0.137453 seconds
Rank 37: Matrix-vector product took 0.14521 seconds
Rank 38: Matrix-vector product took 0.1431 seconds
Rank 39: Matrix-vector product took 0.137448 seconds
Rank 40: Matrix-vector product took 0.138731 seconds
Rank 41: Matrix-vector product took 0.142241 seconds
Rank 42: Matrix-vector product took 0.145018 seconds
Rank 43: Matrix-vector product took 0.137775 seconds
Rank 44: Matrix-vector product took 0.137085 seconds
Rank 45: Matrix-vector product took 0.142764 seconds
Rank 46: Matrix-vector product took 0.142197 seconds
Rank 47: Matrix-vector product took 0.138263 seconds
Rank 48: Matrix-vector product took 0.14372 seconds
Rank 49: Matrix-vector product took 0.136605 seconds
Rank 50: Matrix-vector product took 0.141201 seconds
Rank 51: Matrix-vector product took 0.136889 seconds
Rank 52: Matrix-vector product took 0.137881 seconds
Rank 53: Matrix-vector product took 0.138034 seconds
Rank 54: Matrix-vector product took 0.13985 seconds
Rank 55: Matrix-vector product took 0.13665 seconds
Rank 56: Matrix-vector product took 0.146781 seconds
Rank 57: Matrix-vector product took 0.142716 seconds
Rank 58: Matrix-vector product took 0.138505 seconds
Rank 59: Matrix-vector product took 0.145038 seconds
Rank 60: Matrix-vector product took 0.13993 seconds
Rank 61: Matrix-vector product took 0.138183 seconds
Rank 62: Matrix-vector product took 0.141191 seconds
Rank 63: Matrix-vector product took 0.136262 seconds
Average time for Matrix-vector product is 0.14014 seconds

Rank 0: copyOwnerToAll took 0.00152183 seconds
Rank 1: copyOwnerToAll took 0.00152183 seconds
Rank 2: copyOwnerToAll took 0.00152183 seconds
Rank 3: copyOwnerToAll took 0.00152183 seconds
Rank 4: copyOwnerToAll took 0.00152183 seconds
Rank 5: copyOwnerToAll took 0.00152183 seconds
Rank 6: copyOwnerToAll took 0.00152183 seconds
Rank 7: copyOwnerToAll took 0.00152183 seconds
Rank 8: copyOwnerToAll took 0.00152183 seconds
Rank 9: copyOwnerToAll took 0.00152183 seconds
Rank 10: copyOwnerToAll took 0.00152183 seconds
Rank 11: copyOwnerToAll took 0.00152183 seconds
Rank 12: copyOwnerToAll took 0.00152183 seconds
Rank 13: copyOwnerToAll took 0.00152183 seconds
Rank 14: copyOwnerToAll took 0.00152183 seconds
Rank 15: copyOwnerToAll took 0.00152183 seconds
Rank 16: copyOwnerToAll took 0.00152183 seconds
Rank 17: copyOwnerToAll took 0.00152183 seconds
Rank 18: copyOwnerToAll took 0.00152183 seconds
Rank 19: copyOwnerToAll took 0.00152183 seconds
Rank 20: copyOwnerToAll took 0.00152183 seconds
Rank 21: copyOwnerToAll took 0.00152183 seconds
Rank 22: copyOwnerToAll took 0.00152183 seconds
Rank 23: copyOwnerToAll took 0.00152183 seconds
Rank 24: copyOwnerToAll took 0.00152183 seconds
Rank 25: copyOwnerToAll took 0.00152183 seconds
Rank 26: copyOwnerToAll took 0.00152183 seconds
Rank 27: copyOwnerToAll took 0.00152183 seconds
Rank 28: copyOwnerToAll took 0.00152183 seconds
Rank 29: copyOwnerToAll took 0.00152183 seconds
Rank 30: copyOwnerToAll took 0.00152183 seconds
Rank 31: copyOwnerToAll took 0.00152183 seconds
Rank 32: copyOwnerToAll took 0.00152183 seconds
Rank 33: copyOwnerToAll took 0.00152183 seconds
Rank 34: copyOwnerToAll took 0.00152183 seconds
Rank 35: copyOwnerToAll took 0.00152183 seconds
Rank 36: copyOwnerToAll took 0.00152183 seconds
Rank 37: copyOwnerToAll took 0.00152183 seconds
Rank 38: copyOwnerToAll took 0.00152183 seconds
Rank 39: copyOwnerToAll took 0.00152183 seconds
Rank 40: copyOwnerToAll took 0.00152183 seconds
Rank 41: copyOwnerToAll took 0.00152183 seconds
Rank 42: copyOwnerToAll took 0.00152183 seconds
Rank 43: copyOwnerToAll took 0.00152183 seconds
Rank 44: copyOwnerToAll took 0.00152183 seconds
Rank 45: copyOwnerToAll took 0.00152183 seconds
Rank 46: copyOwnerToAll took 0.00152183 seconds
Rank 47: copyOwnerToAll took 0.00152183 seconds
Rank 48: copyOwnerToAll took 0.00152183 seconds
Rank 49: copyOwnerToAll took 0.00152183 seconds
Rank 50: copyOwnerToAll took 0.00152183 seconds
Rank 51: copyOwnerToAll took 0.00152183 seconds
Rank 52: copyOwnerToAll took 0.00152183 seconds
Rank 53: copyOwnerToAll took 0.00152183 seconds
Rank 54: copyOwnerToAll took 0.00152183 seconds
Rank 55: copyOwnerToAll took 0.00152183 seconds
Rank 56: copyOwnerToAll took 0.00152183 seconds
Rank 57: copyOwnerToAll took 0.00152183 seconds
Rank 58: copyOwnerToAll took 0.00152183 seconds
Rank 59: copyOwnerToAll took 0.00152183 seconds
Rank 60: copyOwnerToAll took 0.00152183 seconds
Rank 61: copyOwnerToAll took 0.00152183 seconds
Rank 62: copyOwnerToAll took 0.00152183 seconds
Rank 63: copyOwnerToAll took 0.00152183 seconds
Average time for copyOwnertoAll is 0.00158772 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	4137	1666	1685	1789	1188	2905	0	577	0	0	0	1173	0	0	961	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	4101	0	322	663	0	0	2136	0	0	0	0	0	1282	0	0	3836	0	0	0	0	0	1343	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1636	315	0	4710	376	1917	0	0	1873	0	0	0	667	3548	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	1662	666	4710	0	2395	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	1791	0	378	2361	0	5374	615	835	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	1204	0	1913	0	5375	0	866	897	1194	4620	1	390	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	2899	2134	0	0	631	873	0	4387	524	99	1821	0	0	0	0	1783	0	0	0	0	0	1288	0	0	0	0	0	0	0	355	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	828	880	4387	0	0	12	1351	2016	0	0	0	0	0	0	0	0	0	6	0	0	0	49	0	0	1679	902	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	573	0	1864	0	0	1223	524	0	0	5159	1907	240	3207	2864	17	224	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	4634	103	12	5160	0	399	2214	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	1	1815	1373	1875	405	0	5596	570	0	3553	1262	0	0	0	0	0	99	278	336	623	462	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	387	0	2024	240	2223	5598	0	0	0	0	0	0	0	0	0	0	0	0	0	1091	908	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	1176	1281	667	0	0	0	0	0	3210	0	563	0	0	2605	1840	1238	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	3556	0	0	0	0	0	2869	0	0	0	2602	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	16	0	3561	0	1840	0	0	4154	0	0	0	0	0	0	1788	387	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	964	3856	0	0	0	0	1780	0	209	0	1272	0	1248	0	4152	0	0	0	0	0	0	794	0	1317	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4361	3683	2054	2244	0	0	565	0	48	0	1525	0	0	715	0	36	93	0	0	0	0	0	0	0	239	0	0	0	0	1737	487	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4381	0	0	0	2119	0	0	0	0	0	0	33	0	0	3590	0	2432	715	0	0	0	0	0	0	0	0	0	0	0	0	135	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3707	0	0	3132	0	0	0	0	0	0	2634	908	0	0	0	0	0	0	0	0	0	0	0	0	0	620	0	0	0	0	18	2118	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2085	0	3110	0	0	0	2691	1656	855	530	878	741	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2247	2117	0	0	0	3496	0	2803	0	115	0	0	0	1144	2546	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	1287	0	0	0	0	1272	7	0	0	99	0	0	0	0	792	0	0	0	0	3478	0	0	3660	0	57	0	0	0	4164	124	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	284	0	0	0	1811	0	0	0	0	2689	0	0	0	4882	2693	155	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	334	0	0	0	390	1317	563	0	0	1671	2782	3641	4887	0	362	2729	0	0	0	133	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	613	1103	0	0	0	0	0	0	0	870	0	0	2693	397	0	5619	1594	302	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	42	0	0	463	935	0	0	0	0	49	0	0	516	92	56	138	2729	5623	0	0	2435	2921	3552	347	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2641	878	0	0	0	0	1624	0	0	5349	0	0	0	0	0	0	0	0	0	0	0	0	935	743	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1533	33	919	739	0	0	0	0	304	2398	5330	0	52	25	2589	3246	0	216	0	0	0	0	553	0	344	499	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	1686	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	2928	0	50	0	5525	0	1390	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	354	907	0	0	75	0	0	0	0	0	0	0	0	0	1144	4136	0	115	0	3562	0	26	5528	0	1397	667	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	755	3588	0	0	2553	121	0	0	0	346	0	2595	0	1395	0	4730	25	1716	0	0	0	0	113	201	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	43	0	3235	1382	665	4741	0	0	5	0	0	0	0	672	1167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	2424	0	0	0	0	0	0	0	0	0	0	0	0	23	0	0	4657	1890	760	0	0	0	0	0	0	0	0	0	0	3243	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	704	0	0	0	0	0	0	0	0	0	213	0	0	1710	5	4648	0	0	1465	0	0	3162	3740	0	2058	0	0	0	0	1510	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1887	0	0	5560	0	0	0	0	0	0	0	0	0	3137	623	0	0	0	0	0	0	0	0	0	44	1709	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	767	1473	5563	0	0	5550	776	427	0	0	330	0	0	2883	744	0	0	0	0	0	0	0	0	0	1015	402	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6002	1133	1304	149	0	0	2681	0	0	0	0	0	0	900	910	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5557	6006	0	595	878	0	0	2221	540	0	211	0	0	0	0	486	874	0	0	0	0	1139	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	559	0	0	108	683	0	3127	0	786	1110	589	0	4729	4156	1465	540	80	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	196	1174	0	3733	0	424	1305	892	4737	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	973	355	0	0	0	0	0	0	0	0	159	0	4148	0	0	5586	211	2084	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	640	0	0	0	0	0	0	0	741	506	0	0	0	0	0	2049	0	0	0	0	1478	0	5581	0	1003	137	103	0	2490	3409	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	321	0	2246	539	0	219	999	0	5769	3221	3229	309	0	0	0	69	0	0	597	0	0	513	0	0	0	0	0	628	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2648	546	82	0	2098	137	5758	0	0	0	0	0	0	0	332	0	0	1321	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	101	3229	0	0	5074	358	1690	0	0	0	0	0	0	0	0	0	0	0	0	0	0	524	1033	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3147	2832	0	209	0	0	0	0	3251	0	5052	0	1967	129	0	0	0	0	0	0	0	0	322	773	0	0	0	0	360	474	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1723	121	17	0	0	0	0	0	0	0	0	0	0	0	0	0	3243	1452	632	719	0	0	210	0	0	2527	316	0	360	1973	0	4653	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	476	0	2129	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3442	0	0	1683	130	4676	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4930	582	1116	711	0	3243	0	0	0	3388	2332	0	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4928	0	997	1370	849	0	2899	0	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	918	472	0	0	0	0	67	340	0	0	0	0	580	991	0	4578	1493	4126	0	0	2742	0	272	366	0	0	14	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	905	868	0	0	0	0	0	0	0	0	0	0	1139	1366	4586	0	0	0	0	0	2728	0	802	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	830	1494	0	0	2646	1295	1735	0	0	0	198	537	1222	1711	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	594	1341	0	0	0	0	0	0	4141	0	2669	0	0	0	930	0	0	0	0	0	3281	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3224	2891	0	0	1303	0	0	5384	0	0	0	350	1555	509	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1766	0	5388	0	0	0	0	0	0	3078	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	1017	0	1133	0	0	0	0	524	0	0	306	0	0	0	0	2750	2721	0	934	0	0	0	4775	1102	382	0	0	2889	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1674	396	0	0	0	0	0	0	0	0	0	769	0	0	0	0	0	0	0	0	0	0	4779	0	1258	1337	0	0	757	2633	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3451	0	290	799	0	0	0	0	1104	1276	0	6073	0	0	0	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2328	0	366	0	225	0	354	0	396	1364	6048	0	5251	0	953	618	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	537	0	1557	0	0	0	0	5255	0	6214	889	1453	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1235	0	522	3080	0	0	0	0	6250	0	491	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	643	0	532	354	0	0	0	0	12	0	1696	3282	0	0	2852	758	0	955	892	473	0	5508	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1036	476	0	0	0	0	0	0	0	0	0	0	0	2641	0	610	1455	0	5509	0	
