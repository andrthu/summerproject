Number of cores/ranks per node is: 4
Scotch partitioner
Cell on rank 0 before loadbalancing: 6310
Cell on rank 1 before loadbalancing: 6260
Cell on rank 2 before loadbalancing: 6196
Cell on rank 3 before loadbalancing: 6312
Cell on rank 4 before loadbalancing: 6240
Cell on rank 5 before loadbalancing: 6312
Cell on rank 6 before loadbalancing: 6300
Cell on rank 7 before loadbalancing: 6260
Cell on rank 8 before loadbalancing: 6200
Cell on rank 9 before loadbalancing: 6200
Cell on rank 10 before loadbalancing: 6190
Cell on rank 11 before loadbalancing: 6310
Cell on rank 12 before loadbalancing: 6190
Cell on rank 13 before loadbalancing: 6290
Cell on rank 14 before loadbalancing: 6190
Cell on rank 15 before loadbalancing: 6240


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	200	230	0	180	0	0	0	0	0	0	100	80	140	0	0	
From rank 1 to: 	200	0	22	258	0	0	0	0	0	0	0	0	240	0	0	0	
From rank 2 to: 	230	20	0	260	50	230	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	258	260	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	180	0	56	0	0	254	60	200	0	0	0	190	0	0	0	0	
From rank 5 to: 	0	0	230	0	254	0	210	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	60	210	0	200	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	200	0	200	0	0	190	0	90	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	270	200	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	190	270	0	90	170	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	200	100	0	180	0	130	200	0	
From rank 11 to: 	110	0	0	0	190	0	0	90	0	170	180	0	0	160	0	0	
From rank 12 to: 	80	240	0	0	0	0	0	0	0	0	0	0	0	230	0	230	
From rank 13 to: 	140	0	0	0	0	0	0	0	0	0	130	160	220	0	250	60	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	200	0	0	250	0	230	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	230	60	230	0	


After loadbalancing process 8 has 6670 cells.
After loadbalancing process 3 has 6830 cells.
After loadbalancing process 6 has 6770 cells.
After loadbalancing process 15 has 6760 cells.
After loadbalancing process 13 has 7250 cells.
After loadbalancing process 11 has 7210 cells.
After loadbalancing process 5 has 7006 cells.
After loadbalancing process 0 has 7240 cells.
After loadbalancing process 4 has 7180 cells.
After loadbalancing process 14 has 6870 cells.
After loadbalancing process 10 has 7000 cells.
After loadbalancing process 9 has 6920 cells.
After loadbalancing process 7 has 6940 cells.
After loadbalancing process 12 has 6970 cells.
After loadbalancing process 1 has 6980 cells.
After loadbalancing process 2 has 6986 cells.


=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1           35.991         0.224593
    2           21.737         0.603954
    3          20.5082         0.943473
    4          16.0115         0.780735
    5          7.86715         0.491344
    6          4.79269         0.609203
    7          3.08593         0.643883
    8          2.09096         0.677578
    9          1.19195         0.570048
   10         0.744493         0.624601
   11         0.561175         0.753768
   12         0.260814         0.464765
   13         0.177592         0.680915
   14         0.121889         0.686342
   15        0.0659207         0.540826
   16        0.0440949         0.668907
   17        0.0289425         0.656369
   18        0.0168123         0.580884
   19        0.0117314         0.697788
=== rate=0.605822, T=0.356858, TIT=0.018782, IT=19

 Elapsed time: 0.356858
Rank 0: Matrix-vector product took 0.00569399 seconds
Rank 1: Matrix-vector product took 0.00564766 seconds
Rank 2: Matrix-vector product took 0.0056479 seconds
Rank 3: Matrix-vector product took 0.00535175 seconds
Rank 4: Matrix-vector product took 0.00591996 seconds
Rank 5: Matrix-vector product took 0.00555668 seconds
Rank 6: Matrix-vector product took 0.00543668 seconds
Rank 7: Matrix-vector product took 0.0056247 seconds
Rank 8: Matrix-vector product took 0.00527606 seconds
Rank 9: Matrix-vector product took 0.00573722 seconds
Rank 10: Matrix-vector product took 0.00567194 seconds
Rank 11: Matrix-vector product took 0.00569942 seconds
Rank 12: Matrix-vector product took 0.00562899 seconds
Rank 13: Matrix-vector product took 0.00581938 seconds
Rank 14: Matrix-vector product took 0.00547244 seconds
Rank 15: Matrix-vector product took 0.00545127 seconds
Average time for Matrix-vector product is 0.00560225 seconds

Rank 0: copyOwnerToAll took 0.0002192 seconds
Rank 1: copyOwnerToAll took 0.0002192 seconds
Rank 2: copyOwnerToAll took 0.0002192 seconds
Rank 3: copyOwnerToAll took 0.0002192 seconds
Rank 4: copyOwnerToAll took 0.0002192 seconds
Rank 5: copyOwnerToAll took 0.0002192 seconds
Rank 6: copyOwnerToAll took 0.0002192 seconds
Rank 7: copyOwnerToAll took 0.0002192 seconds
Rank 8: copyOwnerToAll took 0.0002192 seconds
Rank 9: copyOwnerToAll took 0.0002192 seconds
Rank 10: copyOwnerToAll took 0.0002192 seconds
Rank 11: copyOwnerToAll took 0.0002192 seconds
Rank 12: copyOwnerToAll took 0.0002192 seconds
Rank 13: copyOwnerToAll took 0.0002192 seconds
Rank 14: copyOwnerToAll took 0.0002192 seconds
Rank 15: copyOwnerToAll took 0.0002192 seconds
Average time for copyOwnertoAll is 0.00023509 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	200	230	0	180	0	0	0	0	0	0	100	80	140	0	0	
Rank 1's ghost cells:	200	0	22	258	0	0	0	0	0	0	0	0	240	0	0	0	
Rank 2's ghost cells:	230	20	0	260	50	230	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	258	260	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	180	0	56	0	0	254	60	200	0	0	0	190	0	0	0	0	
Rank 5's ghost cells:	0	0	230	0	254	0	210	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	60	210	0	200	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	200	0	200	0	0	190	0	90	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	270	200	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	190	270	0	90	170	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	200	100	0	180	0	130	200	0	
Rank 11's ghost cells:	110	0	0	0	190	0	0	90	0	170	180	0	0	160	0	0	
Rank 12's ghost cells:	80	240	0	0	0	0	0	0	0	0	0	0	0	230	0	230	
Rank 13's ghost cells:	140	0	0	0	0	0	0	0	0	0	130	160	220	0	250	60	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	200	0	0	250	0	230	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	230	60	230	0	
