Number of cores/ranks per node is: 4
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 1250867
Cell on rank 1 before loadbalancing: 1247426
Cell on rank 2 before loadbalancing: 1250858
Cell on rank 3 before loadbalancing: 1250853
Cell on rank 4 before loadbalancing: 1250859
Cell on rank 5 before loadbalancing: 1250857
Cell on rank 6 before loadbalancing: 1250851
Cell on rank 7 before loadbalancing: 1247429
Edge-cut: 187184


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	24446	5100	2883	0	0	0	0	
From rank 1 to: 	24394	0	5257	6022	0	0	0	0	
From rank 2 to: 	5107	5208	0	23799	3073	5148	0	0	
From rank 3 to: 	2942	6004	23794	0	3772	4479	0	0	
From rank 4 to: 	0	0	3017	3780	0	22830	3356	6383	
From rank 5 to: 	0	0	5175	4532	22791	0	2492	3570	
From rank 6 to: 	0	0	0	0	3364	2536	0	25681	
From rank 7 to: 	0	0	0	0	6360	3576	25690	0	

Edge-cut for node partition: 16488



After loadbalancing process 4 has 1283296 cells.
After loadbalancing process 2 has 1282432 cells.
After loadbalancing process 5 has 1283099 cells.
After loadbalancing process 3 has 1283055 cells.
After loadbalancing process 0 has 1290225 cells.
After loadbalancing process 7 has 1291844 cells.
After loadbalancing process 6 has 1293193 cells.
After loadbalancing process 1 has 1289417 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          138.274         0.210964
    2          79.2562         0.573182
    3          55.7653         0.703608
    4          44.6591         0.800841
    5          40.7724         0.912969
    6          38.7397         0.950145
    7          31.5599         0.814665
    8          28.5646         0.905092
    9          27.5805         0.965549
   10          22.8713         0.829255
   11          21.1734         0.925762
   12          21.3793          1.00973
   13          18.2008         0.851327
   14          16.8851          0.92771
   15          17.4176          1.03154
   16          15.2489         0.875491
   17          13.8421         0.907743
   18          14.3512          1.03678
   19          13.2033         0.920016
   20          12.0483         0.912518
   21          12.3499          1.02504
   22          11.3433          0.91849
   23          10.5594         0.930894
   24          10.8532          1.02782
   25          9.94943         0.916728
   26          9.27913         0.932628
   27          9.67094          1.04223
   28          9.02201         0.932899
   29          8.55538         0.948279
   30          9.24783          1.08094
   31          9.44016           1.0208
   32          10.3525          1.09664
   33          12.4952          1.20697
   34            12.22         0.977982
   35          9.85019         0.806069
   36           7.2424         0.735255
   37          5.36486         0.740757
   38          5.24324         0.977331
   39          5.17526         0.987034
   40          3.87023         0.747833
   41          3.39289         0.876665
   42          3.50163          1.03205
   43          2.73871         0.782125
   44          2.29963         0.839678
   45          2.31047          1.00472
   46          1.80992         0.783355
   47           1.5626         0.863354
   48          1.55855         0.997404
   49          1.25172         0.803136
   50          1.20816         0.965195
   51          1.16888         0.967486
   52         0.980059         0.838463
   53          1.01445          1.03509
   54          0.90271         0.889851
   55         0.850658         0.942338
   56         0.853329          1.00314
   57         0.778178         0.911931
   58         0.783974          1.00745
   59           0.7424          0.94697
   60         0.755567          1.01774
   61         0.698127         0.923978
   62         0.716412          1.02619
   63          0.66711         0.931181
   64         0.616636          0.92434
   65         0.582646         0.944878
   66          0.48736          0.83646
   67         0.426969         0.876085
   68          0.34801         0.815071
   69         0.293419         0.843134
   70         0.244708         0.833987
   71         0.233653         0.954823
   72         0.218067         0.933297
   73         0.241852          1.10907
   74         0.246631          1.01976
   75          0.24951          1.01167
   76         0.229723           0.9207
   77         0.215045         0.936105
   78         0.191788          0.89185
   79         0.183159         0.955008
   80         0.154334         0.842623
   81         0.140991         0.913542
   82         0.132125         0.937121
   83         0.126722         0.959101
   84         0.117138         0.924375
   85         0.117122         0.999864
   86         0.107904         0.921293
   87         0.103696         0.961004
   88        0.0996436         0.960919
   89        0.0943207          0.94658
   90        0.0878978         0.931903
   91        0.0839931         0.955577
   92        0.0772354         0.919545
   93        0.0697563         0.903165
   94        0.0650155         0.932038
=== rate=0.906587, T=271.535, TIT=2.88867, IT=94

 Elapsed time: 271.535
Rank 0: Matrix-vector product took 1.0583 seconds
Rank 1: Matrix-vector product took 1.05781 seconds
Rank 2: Matrix-vector product took 1.051 seconds
Rank 3: Matrix-vector product took 1.0793 seconds
Rank 4: Matrix-vector product took 1.05371 seconds
Rank 5: Matrix-vector product took 1.05404 seconds
Rank 6: Matrix-vector product took 1.08371 seconds
Rank 7: Matrix-vector product took 1.06079 seconds
Average time for Matrix-vector product is 1.06233 seconds

Rank 0: copyOwnerToAll took 0.00313301 seconds
Rank 1: copyOwnerToAll took 0.00313301 seconds
Rank 2: copyOwnerToAll took 0.00313301 seconds
Rank 3: copyOwnerToAll took 0.00313301 seconds
Rank 4: copyOwnerToAll took 0.00313301 seconds
Rank 5: copyOwnerToAll took 0.00313301 seconds
Rank 6: copyOwnerToAll took 0.00313301 seconds
Rank 7: copyOwnerToAll took 0.00313301 seconds
Average time for copyOwnertoAll is 0.0031541 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	22830	3356	6383	0	0	3017	3780	
Rank 1's ghost cells:	22791	0	2492	3570	0	0	5175	4532	
Rank 2's ghost cells:	3364	2536	0	25681	0	0	0	0	
Rank 3's ghost cells:	6360	3576	25690	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	24446	5100	2883	
Rank 5's ghost cells:	0	0	0	0	24394	0	5257	6022	
Rank 6's ghost cells:	3073	5148	0	0	5107	5208	0	23799	
Rank 7's ghost cells:	3772	4479	0	0	2942	6004	23794	0	
