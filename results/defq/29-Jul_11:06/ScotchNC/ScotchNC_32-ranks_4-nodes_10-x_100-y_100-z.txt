Number of cores/ranks per node is: 8
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	230	0	118	0	0	92	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	130	0	0	0	0	0	0	
From rank 1 to: 	230	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	
From rank 2 to: 	0	0	0	200	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	118	160	200	0	0	90	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	210	150	0	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	150	90	210	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	82	0	0	60	150	90	0	220	0	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	142	0	0	0	0	0	220	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	194	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	250	0	117	13	0	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	210	0	40	0	250	0	120	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	100	120	0	120	0	250	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	60	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	117	10	250	0	240	0	0	0	0	0	9	0	0	0	60	58	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	10	0	0	240	0	260	110	10	0	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	260	0	0	120	0	0	13	107	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	250	0	0	0	110	0	0	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	10	120	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	100	9	0	200	0	60	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	5	135	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	9	123	16	0	0	100	8	0	246	0	0	0	250	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	9	135	248	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	90	0	0	0	110	0	0	0	150	40	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	230	0	24	110	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	90	60	0	0	0	0	0	0	0	0	100	24	0	270	0	0	100	130	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	58	0	0	0	0	56	0	250	0	0	114	270	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	30	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	171	0	0	0	
From rank 25 to: 	130	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	120	100	39	0	0	67	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	100	0	0	126	0	214	0	0	0	173	
From rank 27 to: 	0	0	0	0	0	0	0	194	0	0	60	0	0	0	0	0	0	0	0	0	0	0	140	0	0	100	214	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	171	49	0	0	0	200	0	140	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	120	30	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	120	0	240	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	67	173	0	140	30	240	0	


After loadbalancing process 2 has 3490 cells.
After loadbalancing process 14 has 3493 cells.
After loadbalancing process 21 has 3694 cells.
After loadbalancing process 16 has 3748 cells.
After loadbalancing process 17 has 3507 cells.
After loadbalancing process 8 has 3703 cells.
After loadbalancing process 4 has 3720 cells.
After loadbalancing process 15 has 3630 cells.
After loadbalancing process 1 has 3620 cells.
After loadbalancing process 11 has 3852 cells.
After loadbalancing process 23 has 3888 cells.
After loadbalancing process 5 has 3670 cells.
After loadbalancing process 9 has 3744 cells.
After loadbalancing process 3 has 3752 cells.
After loadbalancing process 10 has 3830 cells.
After loadbalancing process 24 has 3702 cells.
After loadbalancing process 7 has 3734 cells.
After loadbalancing process 29 has 3480 cells.
After loadbalancing process 18 has 3846 cells.
After loadbalancing process 28 has 3712 cells.
After loadbalancing process 30 has 3640 cells.
After loadbalancing process 0 has 3868 cells.
After loadbalancing process 27 has 3828 cells.
After loadbalancing process 13 has 3720 cells.
After loadbalancing process 19 has 3593 cells.
After loadbalancing process 12 has 3845 cells.
After loadbalancing process 25 has 3883 cells.
After loadbalancing process 20 has 3720 cells.
After loadbalancing process 22 has 3930 cells.
After loadbalancing process 6 has 3918 cells.
After loadbalancing process 26 has 3899 cells.
After loadbalancing process 31 has 3820 cells.


=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.1857         0.232048
    2          22.6744         0.609762
    3          20.8582         0.919902
    4          17.5496         0.841375
    5          8.62233         0.491312
    6          5.00573         0.580554
    7           3.6633         0.731822
    8          2.24635         0.613204
    9           1.2139         0.540388
   10         0.817235         0.673231
   11         0.609611         0.745943
   12         0.299172          0.49076
   13         0.212724          0.71104
   14         0.137511         0.646429
   15        0.0812602         0.590937
   16        0.0519885         0.639778
   17         0.034513         0.663859
   18        0.0214734         0.622182
   19        0.0134995         0.628661
=== rate=0.610314, T=0.207786, TIT=0.0109361, IT=19

 Elapsed time: 0.207786
Rank 0: Matrix-vector product took 0.00311202 seconds
Rank 1: Matrix-vector product took 0.00289048 seconds
Rank 2: Matrix-vector product took 0.00273508 seconds
Rank 3: Matrix-vector product took 0.00297388 seconds
Rank 4: Matrix-vector product took 0.00292657 seconds
Rank 5: Matrix-vector product took 0.00290616 seconds
Rank 6: Matrix-vector product took 0.00315301 seconds
Rank 7: Matrix-vector product took 0.00294077 seconds
Rank 8: Matrix-vector product took 0.00294122 seconds
Rank 9: Matrix-vector product took 0.00296752 seconds
Rank 10: Matrix-vector product took 0.00305172 seconds
Rank 11: Matrix-vector product took 0.00305717 seconds
Rank 12: Matrix-vector product took 0.00303732 seconds
Rank 13: Matrix-vector product took 0.00286117 seconds
Rank 14: Matrix-vector product took 0.00280202 seconds
Rank 15: Matrix-vector product took 0.0029043 seconds
Rank 16: Matrix-vector product took 0.00295709 seconds
Rank 17: Matrix-vector product took 0.00273017 seconds
Rank 18: Matrix-vector product took 0.00299099 seconds
Rank 19: Matrix-vector product took 0.00279549 seconds
Rank 20: Matrix-vector product took 0.00294348 seconds
Rank 21: Matrix-vector product took 0.00287988 seconds
Rank 22: Matrix-vector product took 0.00307326 seconds
Rank 23: Matrix-vector product took 0.00302311 seconds
Rank 24: Matrix-vector product took 0.00295698 seconds
Rank 25: Matrix-vector product took 0.00304909 seconds
Rank 26: Matrix-vector product took 0.0030789 seconds
Rank 27: Matrix-vector product took 0.0029989 seconds
Rank 28: Matrix-vector product took 0.00297017 seconds
Rank 29: Matrix-vector product took 0.00272616 seconds
Rank 30: Matrix-vector product took 0.00286115 seconds
Rank 31: Matrix-vector product took 0.00302528 seconds
Average time for Matrix-vector product is 0.00294752 seconds

Rank 0: copyOwnerToAll took 0.00021207 seconds
Rank 1: copyOwnerToAll took 0.00021207 seconds
Rank 2: copyOwnerToAll took 0.00021207 seconds
Rank 3: copyOwnerToAll took 0.00021207 seconds
Rank 4: copyOwnerToAll took 0.00021207 seconds
Rank 5: copyOwnerToAll took 0.00021207 seconds
Rank 6: copyOwnerToAll took 0.00021207 seconds
Rank 7: copyOwnerToAll took 0.00021207 seconds
Rank 8: copyOwnerToAll took 0.00021207 seconds
Rank 9: copyOwnerToAll took 0.00021207 seconds
Rank 10: copyOwnerToAll took 0.00021207 seconds
Rank 11: copyOwnerToAll took 0.00021207 seconds
Rank 12: copyOwnerToAll took 0.00021207 seconds
Rank 13: copyOwnerToAll took 0.00021207 seconds
Rank 14: copyOwnerToAll took 0.00021207 seconds
Rank 15: copyOwnerToAll took 0.00021207 seconds
Rank 16: copyOwnerToAll took 0.00021207 seconds
Rank 17: copyOwnerToAll took 0.00021207 seconds
Rank 18: copyOwnerToAll took 0.00021207 seconds
Rank 19: copyOwnerToAll took 0.00021207 seconds
Rank 20: copyOwnerToAll took 0.00021207 seconds
Rank 21: copyOwnerToAll took 0.00021207 seconds
Rank 22: copyOwnerToAll took 0.00021207 seconds
Rank 23: copyOwnerToAll took 0.00021207 seconds
Rank 24: copyOwnerToAll took 0.00021207 seconds
Rank 25: copyOwnerToAll took 0.00021207 seconds
Rank 26: copyOwnerToAll took 0.00021207 seconds
Rank 27: copyOwnerToAll took 0.00021207 seconds
Rank 28: copyOwnerToAll took 0.00021207 seconds
Rank 29: copyOwnerToAll took 0.00021207 seconds
Rank 30: copyOwnerToAll took 0.00021207 seconds
Rank 31: copyOwnerToAll took 0.00021207 seconds
Average time for copyOwnertoAll is 0.000274323 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	230	0	118	0	0	92	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	130	0	0	0	0	0	0	
Rank 1's ghost cells:	230	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	0	0	200	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	118	160	200	0	0	90	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	210	150	0	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	150	90	210	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	82	0	0	60	150	90	0	220	0	40	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	142	0	0	0	0	0	220	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	194	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	250	0	117	13	0	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	210	0	40	0	250	0	120	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	100	120	0	120	0	250	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	60	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	117	10	250	0	240	0	0	0	0	0	9	0	0	0	60	58	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	10	0	0	240	0	260	110	10	0	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	260	0	0	120	0	0	13	107	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	250	0	0	0	110	0	0	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	10	120	250	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	100	9	0	200	0	60	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	5	135	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	9	123	16	0	0	100	8	0	246	0	0	0	250	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	107	0	0	9	135	248	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	90	0	0	0	110	0	0	0	150	40	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	230	0	24	110	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	90	60	0	0	0	0	0	0	0	0	100	24	0	270	0	0	100	130	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	58	0	0	0	0	56	0	250	0	0	114	270	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	30	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	171	0	0	0	
Rank 25's ghost cells:	130	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	120	100	39	0	0	67	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	100	0	0	126	0	214	0	0	0	173	
Rank 27's ghost cells:	0	0	0	0	0	0	0	194	0	0	60	0	0	0	0	0	0	0	0	0	0	0	140	0	0	100	214	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	171	49	0	0	0	200	0	140	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	120	30	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	120	0	240	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	67	173	0	140	30	240	0	
