Number of cores/ranks per node is: 10
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 3331710
Cell on rank 1 before loadbalancing: 3333244
Cell on rank 2 before loadbalancing: 3335046


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	5009	0	2607	2623	4259	1561	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	5011	0	3184	1188	971	0	4473	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	3184	0	4353	4016	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	2622	1224	4359	0	4837	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	2617	992	4014	4839	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	4262	0	0	0	0	0	4509	2439	0	3093	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	1550	4473	0	0	0	4509	0	5683	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	2491	5682	0	3959	3683	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	3957	0	2927	4329	0	0	0	3935	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	3093	0	3665	2934	0	5255	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	4331	5256	0	0	0	3331	3066	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	0	0	4226	0	0	4610	3559	1288	966	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	4226	0	3893	0	1884	0	2511	2205	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	3334	0	3893	0	4117	3801	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	3938	0	3133	0	0	4111	0	5550	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	4611	1896	3805	5550	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	3562	0	0	0	0	0	3609	3389	0	2040	1890	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	1282	2514	0	0	0	3629	0	4869	1547	152	1431	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	949	2216	0	0	0	3388	4868	0	1517	1730	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1542	1517	0	3935	3936	4307	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2058	171	1734	3957	0	5101	734	1893	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1888	1413	0	3937	5100	0	624	2648	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4264	730	624	0	6175	0	5123	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1898	2654	6173	0	3484	1606	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3486	0	4447	4307	0	2933	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5121	1596	4459	0	5644	336	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4339	5661	0	5026	2346	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	336	5005	0	4290	0	5439	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2933	0	2307	4290	0	2523	2609	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2521	0	4740	4743	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5443	2639	4760	0	5728	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4749	5728	0	


After loadbalancing process 0 has 320529 cells.
After loadbalancing process 12 has 320010 cells.
After loadbalancing process 13 has 319513 cells.
After loadbalancing process 20 has 316859 cells.
After loadbalancing process 4 has 314309 cells.
After loadbalancing process 31 has 341936 cells.
After loadbalancing process 2 has 312686 cells.
After loadbalancing process 25 has 351999 cells.
After loadbalancing process 27 has 347851 cells.
After loadbalancing process 17 has 320936 cells.
After loadbalancing process 18 has 319417 cells.
After loadbalancing process 24 has 350799 cells.
After loadbalancing process 28 has 350379 cells.
After loadbalancing process 14 has 318533 cells.
After loadbalancing process 23 has 349113 cells.
After loadbalancing process 26 has 351387 cells.
After loadbalancing process 8 has 317084 cells.
After loadbalancing process 15 has 318561 cells.
After loadbalancing process 3 has 315886 cells.
After loadbalancing process 5 has 319825 cells.
After loadbalancing process 1 has 318483 cells.
After loadbalancing process 7 has 318940 cells.
After loadbalancing process 6 has 319083 cells.
After loadbalancing process 30 has 351681 cells.
After loadbalancing process 9 has 318397 cells.
After loadbalancing process 21 has 316289 cells.
After loadbalancing process 22 has 349114 cells.
After loadbalancing process 11 has 318448 cells.
After loadbalancing process 10 has 316843 cells.
After loadbalancing process 29 has 344002 cells.
After loadbalancing process 16 has 315780 cells.
After loadbalancing process 19 has 317082 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          141.009         0.215137
    2          81.3101         0.576631
    3          57.6173         0.708611
    4          48.7945         0.846873
    5          46.5717         0.954446
    6          38.1048         0.818196
    7          30.8819         0.810448
    8          30.3573         0.983013
    9          27.5326         0.906951
   10          22.8689         0.830612
   11          22.3378         0.976774
   12          21.4947         0.962261
   13          18.0882         0.841517
   14           17.779         0.982906
   15          17.7539         0.998589
   16          15.1668          0.85428
   17          14.8141         0.976744
   18          15.0653          1.01695
   19          13.0679         0.867415
   20          12.8094          0.98022
   21          12.8836          1.00579
   22          11.4643         0.889839
   23          11.3042         0.986035
   24          11.1767         0.988721
   25          10.1841         0.911187
   26          10.1661         0.998233
   27          9.84242         0.968163
   28           9.1381          0.92844
   29          9.36528          1.02486
   30           9.1422         0.976181
   31           9.1033         0.995744
   32          10.3839          1.14067
   33          11.3751          1.09546
   34          12.0619          1.06037
   35          11.7464         0.973845
   36          9.05655         0.771006
   37          7.00289         0.773241
   38          6.25392         0.893048
   39          5.36717         0.858209
   40          4.53028         0.844072
   41          4.11615         0.908586
   42          3.80055         0.923326
   43          3.29416         0.866759
   44          2.75366         0.835923
   45          2.50177         0.908524
   46          2.21453         0.885187
   47          1.82966         0.826205
   48          1.70159         0.930003
   49          1.50643         0.885306
   50           1.3005         0.863301
   51          1.26481         0.972561
   52          1.12338         0.888177
   53          1.04522         0.930425
   54         0.996619         0.953502
   55         0.910398         0.913486
   56         0.885843         0.973029
   57         0.813845         0.918724
   58         0.813923           1.0001
   59         0.766038         0.941167
   60         0.751008          0.98038
   61         0.750699         0.999588
   62          0.69875           0.9308
   63         0.721411          1.03243
   64           0.6614         0.916814
   65         0.648633         0.980697
   66         0.590882         0.910965
   67         0.530601         0.897982
   68         0.458875         0.864822
   69         0.389779         0.849423
   70         0.317464         0.814471
   71         0.264427         0.832937
   72         0.233846         0.884348
   73         0.215459         0.921373
   74         0.230654          1.07052
   75         0.233306           1.0115
   76         0.236947          1.01561
   77         0.225745         0.952725
   78         0.216999         0.961257
   79          0.20076         0.925166
   80         0.187447         0.933685
   81         0.161037         0.859106
   82         0.152751         0.948549
   83         0.140766         0.921535
   84          0.12732          0.90448
   85         0.117497         0.922848
   86          0.11626         0.989474
   87         0.108797         0.935805
   88         0.103894         0.954933
   89        0.0979802         0.943082
   90        0.0947287         0.966815
   91        0.0891856         0.941484
   92        0.0845182         0.947667
   93        0.0799394         0.945825
   94        0.0737067         0.922032
   95        0.0678965         0.921171
   96        0.0628485         0.925651
=== rate=0.90812, T=76.5948, TIT=0.797863, IT=96

 Elapsed time: 76.5948
Rank 0: Matrix-vector product took 0.265467 seconds
Rank 1: Matrix-vector product took 0.262532 seconds
Rank 2: Matrix-vector product took 0.255061 seconds
Rank 3: Matrix-vector product took 0.258359 seconds
Rank 4: Matrix-vector product took 0.259987 seconds
Rank 5: Matrix-vector product took 0.261002 seconds
Rank 6: Matrix-vector product took 0.260062 seconds
Rank 7: Matrix-vector product took 0.260983 seconds
Rank 8: Matrix-vector product took 0.260135 seconds
Rank 9: Matrix-vector product took 0.260325 seconds
Rank 10: Matrix-vector product took 0.259513 seconds
Rank 11: Matrix-vector product took 0.261085 seconds
Rank 12: Matrix-vector product took 0.261944 seconds
Rank 13: Matrix-vector product took 0.261281 seconds
Rank 14: Matrix-vector product took 0.26086 seconds
Rank 15: Matrix-vector product took 0.260981 seconds
Rank 16: Matrix-vector product took 0.25801 seconds
Rank 17: Matrix-vector product took 0.266743 seconds
Rank 18: Matrix-vector product took 0.260614 seconds
Rank 19: Matrix-vector product took 0.259827 seconds
Rank 20: Matrix-vector product took 0.259205 seconds
Rank 21: Matrix-vector product took 0.259079 seconds
Rank 22: Matrix-vector product took 0.286526 seconds
Rank 23: Matrix-vector product took 0.290318 seconds
Rank 24: Matrix-vector product took 0.292132 seconds
Rank 25: Matrix-vector product took 0.290279 seconds
Rank 26: Matrix-vector product took 0.287254 seconds
Rank 27: Matrix-vector product took 0.284111 seconds
Rank 28: Matrix-vector product took 0.287034 seconds
Rank 29: Matrix-vector product took 0.282169 seconds
Rank 30: Matrix-vector product took 0.287907 seconds
Rank 31: Matrix-vector product took 0.281889 seconds
Average time for Matrix-vector product is 0.268834 seconds

Rank 0: copyOwnerToAll took 0.0012418 seconds
Rank 1: copyOwnerToAll took 0.0012418 seconds
Rank 2: copyOwnerToAll took 0.0012418 seconds
Rank 3: copyOwnerToAll took 0.0012418 seconds
Rank 4: copyOwnerToAll took 0.0012418 seconds
Rank 5: copyOwnerToAll took 0.0012418 seconds
Rank 6: copyOwnerToAll took 0.0012418 seconds
Rank 7: copyOwnerToAll took 0.0012418 seconds
Rank 8: copyOwnerToAll took 0.0012418 seconds
Rank 9: copyOwnerToAll took 0.0012418 seconds
Rank 10: copyOwnerToAll took 0.0012418 seconds
Rank 11: copyOwnerToAll took 0.0012418 seconds
Rank 12: copyOwnerToAll took 0.0012418 seconds
Rank 13: copyOwnerToAll took 0.0012418 seconds
Rank 14: copyOwnerToAll took 0.0012418 seconds
Rank 15: copyOwnerToAll took 0.0012418 seconds
Rank 16: copyOwnerToAll took 0.0012418 seconds
Rank 17: copyOwnerToAll took 0.0012418 seconds
Rank 18: copyOwnerToAll took 0.0012418 seconds
Rank 19: copyOwnerToAll took 0.0012418 seconds
Rank 20: copyOwnerToAll took 0.0012418 seconds
Rank 21: copyOwnerToAll took 0.0012418 seconds
Rank 22: copyOwnerToAll took 0.0012418 seconds
Rank 23: copyOwnerToAll took 0.0012418 seconds
Rank 24: copyOwnerToAll took 0.0012418 seconds
Rank 25: copyOwnerToAll took 0.0012418 seconds
Rank 26: copyOwnerToAll took 0.0012418 seconds
Rank 27: copyOwnerToAll took 0.0012418 seconds
Rank 28: copyOwnerToAll took 0.0012418 seconds
Rank 29: copyOwnerToAll took 0.0012418 seconds
Rank 30: copyOwnerToAll took 0.0012418 seconds
Rank 31: copyOwnerToAll took 0.0012418 seconds
Average time for copyOwnertoAll is 0.00294433 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	5009	0	2607	2623	4259	1561	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	5011	0	3184	1188	971	0	4473	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	3184	0	4353	4016	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	2622	1224	4359	0	4837	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	2617	992	4014	4839	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	4262	0	0	0	0	0	4509	2439	0	3093	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	1550	4473	0	0	0	4509	0	5683	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	2491	5682	0	3959	3683	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	3957	0	2927	4329	0	0	0	3935	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	3093	0	3665	2934	0	5255	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	4331	5256	0	0	0	3331	3066	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	4226	0	0	4610	3559	1288	966	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	4226	0	3893	0	1884	0	2511	2205	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	3334	0	3893	0	4117	3801	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	3938	0	3133	0	0	4111	0	5550	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	4611	1896	3805	5550	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	3562	0	0	0	0	0	3609	3389	0	2040	1890	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	1282	2514	0	0	0	3629	0	4869	1547	152	1431	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	949	2216	0	0	0	3388	4868	0	1517	1730	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1542	1517	0	3935	3936	4307	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2058	171	1734	3957	0	5101	734	1893	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1888	1413	0	3937	5100	0	624	2648	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4264	730	624	0	6175	0	5123	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1898	2654	6173	0	3484	1606	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3486	0	4447	4307	0	2933	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5121	1596	4459	0	5644	336	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4339	5661	0	5026	2346	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	336	5005	0	4290	0	5439	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2933	0	2307	4290	0	2523	2609	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2521	0	4740	4743	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5443	2639	4760	0	5728	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4749	5728	0	
