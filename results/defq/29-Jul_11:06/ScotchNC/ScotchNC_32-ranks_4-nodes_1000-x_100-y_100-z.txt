Number of cores/ranks per node is: 8
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 2506528
Cell on rank 1 before loadbalancing: 2507530
Cell on rank 2 before loadbalancing: 2491681
Cell on rank 3 before loadbalancing: 2494261


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	5789	879	4273	0	0	4989	903	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	5784	0	4831	439	0	0	160	4541	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	867	4825	0	6294	0	0	0	0	0	0	0	0	0	0	518	4822	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	4276	435	6291	0	0	0	0	0	0	0	0	0	0	0	4775	158	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	6124	4657	568	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4697	279	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	6121	0	482	4632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	468	4590	0	0	0	0	0	0	
From rank 6 to: 	4992	154	0	0	4660	464	0	5349	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	880	4543	0	0	579	4633	5345	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	5887	536	4963	594	4906	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	5886	0	4664	33	4471	464	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	500	4662	0	5911	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	4972	28	5911	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	584	4469	0	0	0	5550	4988	185	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	4913	468	0	0	5540	0	537	4547	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	524	4775	0	0	0	0	0	0	0	0	4988	560	0	6154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	4829	157	0	0	0	0	0	0	0	0	196	4553	6157	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5576	4100	1151	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5576	0	843	4262	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4104	851	0	5071	4291	1098	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1147	4261	5070	0	580	4634	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4291	581	0	5176	879	5219	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1108	4629	5181	0	4291	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	874	4289	0	5773	0	0	0	0	4753	45	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5223	0	5779	0	0	0	0	0	142	5084	0	0	
From rank 24 to: 	0	0	0	0	4697	467	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6147	4719	652	0	0	0	0	
From rank 25 to: 	0	0	0	0	279	4589	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6145	0	300	4511	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4724	309	0	5463	0	0	4423	486	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	634	4508	5468	0	0	0	662	4680	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4752	141	0	0	0	0	0	6116	4746	327	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	5083	0	0	0	0	6115	0	469	4807	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4422	661	4746	472	0	5446	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	498	4679	313	4812	5447	0	


After loadbalancing process 17 has 321292 cells.
After loadbalancing process 10 has 323159 cells.
After loadbalancing process 16 has 321970 cells.
After loadbalancing process 21 has 326958 cells.
After loadbalancing process 1 has 328852 cells.
After loadbalancing process 26 has 329038 cells.
After loadbalancing process 20 has 327852 cells.
After loadbalancing process 18 has 329989 cells.
After loadbalancing process 19 has 326478 cells.
After loadbalancing process 0 has 332732 cells.
After loadbalancing process 7 has 329601 cells.
After loadbalancing process 11 has 322707 cells.
After loadbalancing process 8 has 330868 cells.
After loadbalancing process 3 has 331037 cells.
After loadbalancing process 9 has 330551 cells.
After loadbalancing process 2 has 332355 cells.
After loadbalancing process 22 has 327006 cells.
After loadbalancing process 6 has 327885 cells.
After loadbalancing process 12 has 330584 cells.
After loadbalancing process 28 has 325603 cells.
After loadbalancing process 13 has 330945 cells.
After loadbalancing process 31 has 327173 cells.
After loadbalancing process 30 has 327318 cells.
After loadbalancing process 27 has 329999 cells.
After loadbalancing process 14 has 330508 cells.
After loadbalancing process 23 has 326068 cells.
After loadbalancing process 15 has 327270 cells.
After loadbalancing process 4 has 327149 cells.
After loadbalancing process 5 has 326983 cells.
After loadbalancing process 25 has 326969 cells.
After loadbalancing process 24 has 329982 cells.
After loadbalancing process 29 has 326126 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          140.624          0.21455
    2          81.1454         0.577037
    3          57.3232         0.706426
    4          47.8595         0.834906
    5          45.4271         0.949176
    6          38.1929         0.840752
    7          30.9269         0.809755
    8          29.9456         0.968272
    9           27.271         0.910683
   10          22.6618         0.830985
   11          22.0018         0.970877
   12          21.4417         0.974544
   13          17.9438         0.836863
   14          17.1187          0.95402
   15          17.5871          1.02736
   16          15.2277         0.865842
   17          14.0393         0.921956
   18          14.6902          1.04637
   19          13.3658          0.90984
   20          12.2713         0.918114
   21          12.6551          1.03127
   22           11.676         0.922638
   23          10.9818         0.940543
   24          11.1319          1.01367
   25          10.2808         0.923543
   26           9.9807          0.97081
   27          9.92597         0.994516
   28          9.08371         0.915146
   29          9.12984          1.00508
   30          9.25019          1.01318
   31          9.11332         0.985204
   32          10.3853          1.13957
   33          11.7919          1.13544
   34          12.0967          1.02585
   35          11.4627         0.947588
   36          8.86964         0.773782
   37          6.74469         0.760423
   38          6.10354          0.90494
   39          5.29924         0.868224
   40          4.39651          0.82965
   41          4.11072         0.934995
   42          3.81018         0.926889
   43           3.1534         0.827626
   44          2.69939         0.856024
   45          2.52745         0.936306
   46          2.16375         0.856098
   47          1.80084         0.832279
   48          1.68282         0.934463
   49          1.47354         0.875637
   50          1.29927         0.881735
   51          1.23014         0.946795
   52          1.10036           0.8945
   53          1.06059         0.963852
   54         0.972279         0.916737
   55         0.907569         0.933446
   56         0.888162         0.978616
   57         0.798888         0.899485
   58          0.81158          1.01589
   59         0.754335         0.929465
   60         0.737437         0.977599
   61         0.737241         0.999734
   62         0.686926         0.931752
   63         0.707221          1.02954
   64         0.650045         0.919154
   65         0.635034         0.976908
   66         0.573872         0.903687
   67          0.52331         0.911892
   68         0.439544         0.839931
   69         0.376696         0.857016
   70         0.299899         0.796128
   71         0.245882         0.819883
   72         0.213788         0.869474
   73         0.198051         0.926391
   74         0.214076          1.08091
   75         0.227781          1.06402
   76          0.23096          1.01396
   77         0.224381         0.971512
   78         0.220222         0.981466
   79         0.204321         0.927796
   80         0.186093         0.910785
   81         0.155258         0.834307
   82         0.141755         0.913029
   83         0.134827         0.951126
   84         0.123014         0.912382
   85         0.112877         0.917598
   86         0.112485         0.996524
   87         0.107938         0.959578
   88         0.102327         0.948019
   89        0.0993897         0.971292
   90        0.0918939         0.924581
   91        0.0865184         0.941503
   92        0.0818904         0.946509
   93        0.0750835         0.916878
   94          0.06728         0.896069
   95        0.0633131         0.941039
=== rate=0.90727, T=72.4434, TIT=0.762563, IT=95

 Elapsed time: 72.4434
Rank 0: Matrix-vector product took 0.272675 seconds
Rank 1: Matrix-vector product took 0.268916 seconds
Rank 2: Matrix-vector product took 0.271189 seconds
Rank 3: Matrix-vector product took 0.271681 seconds
Rank 4: Matrix-vector product took 0.272282 seconds
Rank 5: Matrix-vector product took 0.268786 seconds
Rank 6: Matrix-vector product took 0.269074 seconds
Rank 7: Matrix-vector product took 0.270217 seconds
Rank 8: Matrix-vector product took 0.27138 seconds
Rank 9: Matrix-vector product took 0.270348 seconds
Rank 10: Matrix-vector product took 0.265253 seconds
Rank 11: Matrix-vector product took 0.271412 seconds
Rank 12: Matrix-vector product took 0.271105 seconds
Rank 13: Matrix-vector product took 0.27033 seconds
Rank 14: Matrix-vector product took 0.270893 seconds
Rank 15: Matrix-vector product took 0.268469 seconds
Rank 16: Matrix-vector product took 0.263601 seconds
Rank 17: Matrix-vector product took 0.263926 seconds
Rank 18: Matrix-vector product took 0.274803 seconds
Rank 19: Matrix-vector product took 0.268111 seconds
Rank 20: Matrix-vector product took 0.268301 seconds
Rank 21: Matrix-vector product took 0.267174 seconds
Rank 22: Matrix-vector product took 0.267331 seconds
Rank 23: Matrix-vector product took 0.271252 seconds
Rank 24: Matrix-vector product took 0.278694 seconds
Rank 25: Matrix-vector product took 0.267622 seconds
Rank 26: Matrix-vector product took 0.269103 seconds
Rank 27: Matrix-vector product took 0.269089 seconds
Rank 28: Matrix-vector product took 0.266936 seconds
Rank 29: Matrix-vector product took 0.267287 seconds
Rank 30: Matrix-vector product took 0.268598 seconds
Rank 31: Matrix-vector product took 0.26724 seconds
Average time for Matrix-vector product is 0.269471 seconds

Rank 0: copyOwnerToAll took 0.00116497 seconds
Rank 1: copyOwnerToAll took 0.00116497 seconds
Rank 2: copyOwnerToAll took 0.00116497 seconds
Rank 3: copyOwnerToAll took 0.00116497 seconds
Rank 4: copyOwnerToAll took 0.00116497 seconds
Rank 5: copyOwnerToAll took 0.00116497 seconds
Rank 6: copyOwnerToAll took 0.00116497 seconds
Rank 7: copyOwnerToAll took 0.00116497 seconds
Rank 8: copyOwnerToAll took 0.00116497 seconds
Rank 9: copyOwnerToAll took 0.00116497 seconds
Rank 10: copyOwnerToAll took 0.00116497 seconds
Rank 11: copyOwnerToAll took 0.00116497 seconds
Rank 12: copyOwnerToAll took 0.00116497 seconds
Rank 13: copyOwnerToAll took 0.00116497 seconds
Rank 14: copyOwnerToAll took 0.00116497 seconds
Rank 15: copyOwnerToAll took 0.00116497 seconds
Rank 16: copyOwnerToAll took 0.00116497 seconds
Rank 17: copyOwnerToAll took 0.00116497 seconds
Rank 18: copyOwnerToAll took 0.00116497 seconds
Rank 19: copyOwnerToAll took 0.00116497 seconds
Rank 20: copyOwnerToAll took 0.00116497 seconds
Rank 21: copyOwnerToAll took 0.00116497 seconds
Rank 22: copyOwnerToAll took 0.00116497 seconds
Rank 23: copyOwnerToAll took 0.00116497 seconds
Rank 24: copyOwnerToAll took 0.00116497 seconds
Rank 25: copyOwnerToAll took 0.00116497 seconds
Rank 26: copyOwnerToAll took 0.00116497 seconds
Rank 27: copyOwnerToAll took 0.00116497 seconds
Rank 28: copyOwnerToAll took 0.00116497 seconds
Rank 29: copyOwnerToAll took 0.00116497 seconds
Rank 30: copyOwnerToAll took 0.00116497 seconds
Rank 31: copyOwnerToAll took 0.00116497 seconds
Average time for copyOwnertoAll is 0.00119504 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	5789	879	4273	0	0	4989	903	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	5784	0	4831	439	0	0	160	4541	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	867	4825	0	6294	0	0	0	0	0	0	0	0	0	0	518	4822	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	4276	435	6291	0	0	0	0	0	0	0	0	0	0	0	4775	158	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	6124	4657	568	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4697	279	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	6121	0	482	4632	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	468	4590	0	0	0	0	0	0	
Rank 6's ghost cells:	4992	154	0	0	4660	464	0	5349	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	880	4543	0	0	579	4633	5345	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	5887	536	4963	594	4906	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	5886	0	4664	33	4471	464	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	500	4662	0	5911	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	4972	28	5911	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	584	4469	0	0	0	5550	4988	185	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	4913	468	0	0	5540	0	537	4547	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	524	4775	0	0	0	0	0	0	0	0	4988	560	0	6154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	4829	157	0	0	0	0	0	0	0	0	196	4553	6157	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5576	4100	1151	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5576	0	843	4262	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4104	851	0	5071	4291	1098	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1147	4261	5070	0	580	4634	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4291	581	0	5176	879	5219	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1108	4629	5181	0	4291	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	874	4289	0	5773	0	0	0	0	4753	45	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5223	0	5779	0	0	0	0	0	142	5084	0	0	
Rank 24's ghost cells:	0	0	0	0	4697	467	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6147	4719	652	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	279	4589	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6145	0	300	4511	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4724	309	0	5463	0	0	4423	486	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	634	4508	5468	0	0	0	662	4680	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4752	141	0	0	0	0	0	6116	4746	327	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	5083	0	0	0	0	6115	0	469	4807	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4422	661	4746	472	0	5446	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	498	4679	313	4812	5447	0	
