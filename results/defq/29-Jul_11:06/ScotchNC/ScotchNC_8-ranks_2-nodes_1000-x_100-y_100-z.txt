Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 4998932
Cell on rank 1 before loadbalancing: 5001068


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	10332	0	0	0	0	0	10055	
From rank 1 to: 	10334	0	0	10076	0	0	0	0	
From rank 2 to: 	0	0	0	10123	0	0	0	0	
From rank 3 to: 	0	10076	10126	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	10062	0	0	
From rank 5 to: 	0	0	0	0	10064	0	10012	0	
From rank 6 to: 	0	0	0	0	0	10011	0	10020	
From rank 7 to: 	10055	0	0	0	0	0	10018	0	


After loadbalancing process 4 has 1264778 cells.
After loadbalancing process 2 has 1255807 cells.
After loadbalancing process 0 has 1276247 cells.
After loadbalancing process 6 has 1266190 cells.
After loadbalancing process 7 has 1267940 cells.
After loadbalancing process 3 has 1264342 cells.
After loadbalancing process 1 has 1273658 cells.
After loadbalancing process 5 has 1272402 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          137.506         0.209792
    2          78.6834         0.572219
    3          55.2765         0.702518
    4          43.9198         0.794548
    5          40.1406         0.913952
    6          38.5085         0.959341
    7          30.9442         0.803568
    8          27.0882         0.875388
    9          27.0633          0.99908
   10          23.4199         0.865374
   11          20.3121         0.867302
   12          20.6666          1.01745
   13          18.7649         0.907982
   14          15.7822         0.841051
   15           15.792          1.00062
   16           16.163           1.0235
   17          13.6539          0.84476
   18          12.7843         0.936312
   19           13.537          1.05888
   20          12.2818         0.907275
   21          11.1602         0.908681
   22          11.6473          1.04365
   23          10.9705         0.941893
   24          10.0069         0.912162
   25          10.1999          1.01929
   26           9.5601         0.937275
   27           8.8027         0.920775
   28          9.10655          1.03452
   29          8.94543         0.982307
   30          8.70775          0.97343
   31          10.1101          1.16105
   32          11.6726          1.15455
   33          12.2719          1.05133
   34          11.7463         0.957173
   35          8.39375         0.714588
   36          5.40668         0.644132
   37           4.8296         0.893264
   38          4.91067          1.01679
   39          3.94287         0.802919
   40          3.27386         0.830325
   41          3.32835          1.01664
   42          2.86077         0.859516
   43          2.19623         0.767705
   44          2.14733         0.977738
   45          1.93772         0.902386
   46          1.48349         0.765584
   47          1.45754         0.982508
   48          1.31646         0.903203
   49          1.12775          0.85666
   50          1.16349          1.03169
   51         0.970209         0.833878
   52         0.984581          1.01481
   53          0.92835         0.942888
   54         0.802895         0.864863
   55         0.887151          1.10494
   56          0.75164         0.847252
   57         0.776381          1.03292
   58          0.73999         0.953127
   59         0.724349         0.978863
   60          0.69573          0.96049
   61         0.672672         0.966858
   62         0.667983          0.99303
   63         0.572586         0.857187
   64          0.55858         0.975538
   65         0.438094         0.784301
   66         0.386303         0.881781
   67         0.302594         0.783306
   68         0.273365         0.903404
   69          0.22644         0.828343
   70         0.228014          1.00695
   71         0.204985            0.899
   72          0.22272          1.08652
   73         0.232198          1.04256
   74         0.246015           1.0595
   75         0.204505          0.83127
   76          0.18974         0.927801
   77         0.166991         0.880105
   78          0.16425         0.983587
   79         0.135516         0.825058
   80         0.119063         0.878588
   81         0.111072          0.93289
   82         0.119477          1.07567
   83         0.105151         0.880088
   84         0.100576         0.956491
   85         0.100194         0.996201
   86         0.093413         0.932325
   87        0.0866424          0.92752
   88        0.0833406         0.961891
   89        0.0766551         0.919781
   90        0.0693756         0.905036
   91        0.0684753         0.987022
   92        0.0568651         0.830447
=== rate=0.90334, T=261.338, TIT=2.84063, IT=92

 Elapsed time: 261.338
Rank 0: Matrix-vector product took 1.05152 seconds
Rank 1: Matrix-vector product took 1.04857 seconds
Rank 2: Matrix-vector product took 1.05094 seconds
Rank 3: Matrix-vector product took 1.04218 seconds
Rank 4: Matrix-vector product took 1.04263 seconds
Rank 5: Matrix-vector product took 1.04643 seconds
Rank 6: Matrix-vector product took 1.03951 seconds
Rank 7: Matrix-vector product took 1.04205 seconds
Average time for Matrix-vector product is 1.04548 seconds

Rank 0: copyOwnerToAll took 0.00149517 seconds
Rank 1: copyOwnerToAll took 0.00149517 seconds
Rank 2: copyOwnerToAll took 0.00149517 seconds
Rank 3: copyOwnerToAll took 0.00149517 seconds
Rank 4: copyOwnerToAll took 0.00149517 seconds
Rank 5: copyOwnerToAll took 0.00149517 seconds
Rank 6: copyOwnerToAll took 0.00149517 seconds
Rank 7: copyOwnerToAll took 0.00149517 seconds
Average time for copyOwnertoAll is 0.00158418 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	10332	0	0	0	0	0	10055	
Rank 1's ghost cells:	10334	0	0	10076	0	0	0	0	
Rank 2's ghost cells:	0	0	0	10123	0	0	0	0	
Rank 3's ghost cells:	0	10076	10126	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	10062	0	0	
Rank 5's ghost cells:	0	0	0	0	10064	0	10012	0	
Rank 6's ghost cells:	0	0	0	0	0	10011	0	10020	
Rank 7's ghost cells:	10055	0	0	0	0	0	10018	0	
