Number of cores/ranks per node is: 10
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 33500
Cell on rank 1 before loadbalancing: 33180
Cell on rank 2 before loadbalancing: 33320


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	180	0	150	0	140	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	180	0	100	60	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	50	0	0	0	0	0	
From rank 2 to: 	0	100	0	140	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	120	0	20	0	0	0	0	0	0	
From rank 3 to: 	150	60	140	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	150	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	140	0	0	0	0	0	100	130	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	100	0	220	104	136	0	0	0	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	20	170	0	0	0	131	220	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	104	0	0	122	138	0	190	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	130	126	0	122	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	138	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	0	0	182	150	50	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	190	0	0	182	0	0	148	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	152	0	0	148	70	110	0	0	145	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	90	0	70	0	0	50	140	148	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	50	50	0	0	0	0	0	75	200	0	0	0	0	105	0	0	0	0	30	0	210	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	180	0	100	0	0	0	112	160	60	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	0	200	48	0	200	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	135	0	95	60	50	0	0	106	114	0	0	110	0	0	0	30	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	0	210	0	0	0	0	0	150	120	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	114	214	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	0	0	0	0	0	0	140	
From rank 23 to: 	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	64	230	0	0	0	0	0	130	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	100	0	0	0	64	0	140	130	0	230	0	0	46	
From rank 25 to: 	0	130	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	140	0	220	0	0	0	0	0	
From rank 26 to: 	0	50	0	0	0	0	0	110	0	0	0	0	0	0	0	210	0	0	0	0	0	0	0	0	130	220	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	200	140	46	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	110	0	0	0	230	0	0	200	0	0	94	60	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	230	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	94	230	0	240	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	130	46	0	0	0	60	0	240	0	


After loadbalancing process 4 has 3420 cells.
After loadbalancing process 3 has 3600 cells.
After loadbalancing process 18 has 3380 cells.
After loadbalancing process 29 has 3680 cells.After loadbalancing process 16 has 3644 cells.
After loadbalancing process 20 has 3582 cells.

After loadbalancing process 31 has 3955 cells.
After loadbalancing process 30 has 3940 cells.
After loadbalancing process 22 has 3822 cells.After loadbalancing process 27 has 3901 cells.
After loadbalancing process 17 has 3600 cells.

After loadbalancing process 28 has 4089 cells.
After loadbalancing process 25 has 4032 cells.
After loadbalancing process 21 has 3514 cells.After loadbalancing process 23 has 4083 cells.
After loadbalancing process 11 has 3592 cells.

After loadbalancing process 12 has 3550 cells.
After loadbalancing process 5 has 3515 cells.
After loadbalancing process 9 has 3638 cells.
After loadbalancing process 2 has 3690 cells.
After loadbalancing process 10 has 3423 cells.
After loadbalancing process 0 has 3530 cells.
After loadbalancing process 1 has 3730 cells.
After loadbalancing process 13 has 3635 cells.
After loadbalancing process 8 has 3699 cells.
After loadbalancing process 6 has 3750 cells.
After loadbalancing process 7 has 3716 cells.
After loadbalancing process 19 has 3746 cells.
After loadbalancing process 14 has 3698 cells.
After loadbalancing process 24 has 4100 cells.After loadbalancing process 26 has 4068 cells.

After loadbalancing process 15 has 3720 cells.


=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.3692         0.233194
    2          22.7581         0.609007
    3          20.9672         0.921306
    4          17.9591         0.856531
    5          8.78371         0.489096
    6          4.96015         0.564699
    7          3.60822         0.727442
    8          2.25683          0.62547
    9          1.20484         0.533865
   10         0.820169         0.680726
   11         0.606894         0.739962
   12         0.296914         0.489236
   13         0.215604          0.72615
   14         0.143867         0.667275
   15        0.0823315         0.572275
   16         0.055266         0.671261
   17        0.0357405         0.646701
   18        0.0219561          0.61432
   19        0.0143091         0.651713
=== rate=0.612188, T=0.283806, TIT=0.0149371, IT=19

 Elapsed time: 0.283806
Rank 0: Matrix-vector product took 0.00276574 seconds
Rank 1: Matrix-vector product took 0.00291423 seconds
Rank 2: Matrix-vector product took 0.00295396 seconds
Rank 3: Matrix-vector product took 0.00284099 seconds
Rank 4: Matrix-vector product took 0.00267108 seconds
Rank 5: Matrix-vector product took 0.00279076 seconds
Rank 6: Matrix-vector product took 0.00295601 seconds
Rank 7: Matrix-vector product took 0.00290415 seconds
Rank 8: Matrix-vector product took 0.00287892 seconds
Rank 9: Matrix-vector product took 0.00288052 seconds
Rank 10: Matrix-vector product took 0.00270414 seconds
Rank 11: Matrix-vector product took 0.00287013 seconds
Rank 12: Matrix-vector product took 0.00278295 seconds
Rank 13: Matrix-vector product took 0.00284382 seconds
Rank 14: Matrix-vector product took 0.00291596 seconds
Rank 15: Matrix-vector product took 0.0029932 seconds
Rank 16: Matrix-vector product took 0.00287002 seconds
Rank 17: Matrix-vector product took 0.00366739 seconds
Rank 18: Matrix-vector product took 0.00269746 seconds
Rank 19: Matrix-vector product took 0.00295083 seconds
Rank 20: Matrix-vector product took 0.00285556 seconds
Rank 21: Matrix-vector product took 0.00420881 seconds
Rank 22: Matrix-vector product took 0.00309071 seconds
Rank 23: Matrix-vector product took 0.00322196 seconds
Rank 24: Matrix-vector product took 0.00323308 seconds
Rank 25: Matrix-vector product took 0.00315542 seconds
Rank 26: Matrix-vector product took 0.00318695 seconds
Rank 27: Matrix-vector product took 0.00306079 seconds
Rank 28: Matrix-vector product took 0.00327044 seconds
Rank 29: Matrix-vector product took 0.00288789 seconds
Rank 30: Matrix-vector product took 0.0030684 seconds
Rank 31: Matrix-vector product took 0.00308927 seconds
Average time for Matrix-vector product is 0.00300567 seconds

Rank 0: copyOwnerToAll took 0.00022927 seconds
Rank 1: copyOwnerToAll took 0.00022927 seconds
Rank 2: copyOwnerToAll took 0.00022927 seconds
Rank 3: copyOwnerToAll took 0.00022927 seconds
Rank 4: copyOwnerToAll took 0.00022927 seconds
Rank 5: copyOwnerToAll took 0.00022927 seconds
Rank 6: copyOwnerToAll took 0.00022927 seconds
Rank 7: copyOwnerToAll took 0.00022927 seconds
Rank 8: copyOwnerToAll took 0.00022927 seconds
Rank 9: copyOwnerToAll took 0.00022927 seconds
Rank 10: copyOwnerToAll took 0.00022927 seconds
Rank 11: copyOwnerToAll took 0.00022927 seconds
Rank 12: copyOwnerToAll took 0.00022927 seconds
Rank 13: copyOwnerToAll took 0.00022927 seconds
Rank 14: copyOwnerToAll took 0.00022927 seconds
Rank 15: copyOwnerToAll took 0.00022927 seconds
Rank 16: copyOwnerToAll took 0.00022927 seconds
Rank 17: copyOwnerToAll took 0.00022927 seconds
Rank 18: copyOwnerToAll took 0.00022927 seconds
Rank 19: copyOwnerToAll took 0.00022927 seconds
Rank 20: copyOwnerToAll took 0.00022927 seconds
Rank 21: copyOwnerToAll took 0.00022927 seconds
Rank 22: copyOwnerToAll took 0.00022927 seconds
Rank 23: copyOwnerToAll took 0.00022927 seconds
Rank 24: copyOwnerToAll took 0.00022927 seconds
Rank 25: copyOwnerToAll took 0.00022927 seconds
Rank 26: copyOwnerToAll took 0.00022927 seconds
Rank 27: copyOwnerToAll took 0.00022927 seconds
Rank 28: copyOwnerToAll took 0.00022927 seconds
Rank 29: copyOwnerToAll took 0.00022927 seconds
Rank 30: copyOwnerToAll took 0.00022927 seconds
Rank 31: copyOwnerToAll took 0.00022927 seconds
Average time for copyOwnertoAll is 0.000251144 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	180	0	150	0	140	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	180	0	100	60	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	50	0	0	0	0	0	
Rank 2's ghost cells:	0	100	0	140	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	120	0	20	0	0	0	0	0	0	
Rank 3's ghost cells:	150	60	140	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	150	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	140	0	0	0	0	0	100	130	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	100	0	220	104	136	0	0	0	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	20	170	0	0	0	131	220	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	104	0	0	122	138	0	190	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	130	126	0	122	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	138	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	182	150	50	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	190	0	0	182	0	0	148	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	152	0	0	148	70	110	0	0	145	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	90	0	70	0	0	50	140	148	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	50	50	0	0	0	0	0	75	200	0	0	0	0	105	0	0	0	0	30	0	210	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	180	0	100	0	0	0	112	160	60	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	0	200	48	0	200	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	135	0	95	60	50	0	0	106	114	0	0	110	0	0	0	30	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	0	210	0	0	0	0	0	150	120	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	114	214	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	0	0	0	0	0	0	140	
Rank 23's ghost cells:	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	64	230	0	0	0	0	0	130	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	100	0	0	0	64	0	140	130	0	230	0	0	46	
Rank 25's ghost cells:	0	130	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	140	0	220	0	0	0	0	0	
Rank 26's ghost cells:	0	50	0	0	0	0	0	110	0	0	0	0	0	0	0	210	0	0	0	0	0	0	0	0	130	220	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	200	140	46	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	110	0	0	0	230	0	0	200	0	0	94	60	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	230	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	94	230	0	240	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	130	46	0	0	0	60	0	240	0	
