Number of cores/ranks per node is: 16
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 497728
Cell on rank 1 before loadbalancing: 502272


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	1063	0	1009	0	0	0	0	0	0	0	0	0	44	626	585	196	0	0	0	0	0	0	0	0	0	0	0	0	379	0	0	
From rank 1 to: 	1068	0	1065	207	458	0	395	164	0	298	0	0	0	900	240	24	68	0	0	0	0	0	0	0	0	0	0	0	0	380	0	0	
From rank 2 to: 	0	1066	0	1200	321	505	0	596	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	624	102	0	0	
From rank 3 to: 	1004	220	1198	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	455	225	0	0	
From rank 4 to: 	0	467	325	0	0	1147	826	199	420	951	100	0	0	0	0	0	0	0	0	0	0	0	0	0	26	83	539	696	124	58	0	0	
From rank 5 to: 	0	0	509	0	1148	0	301	986	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	587	387	2	3	295	0	0	0	
From rank 6 to: 	0	395	0	0	827	315	0	1165	79	37	1084	0	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	180	596	0	204	984	1159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	418	0	76	0	0	912	521	771	318	0	0	0	0	0	0	0	261	24	746	112	0	0	0	382	0	0	0	0	
From rank 9 to: 	0	316	0	0	954	0	37	0	930	0	605	271	715	830	0	0	327	0	0	0	179	379	0	0	0	0	247	15	0	32	0	0	
From rank 10 to: 	0	0	0	0	107	0	1092	0	521	614	0	984	0	466	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	781	270	984	0	612	381	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	319	709	0	599	0	917	637	468	174	476	0	0	305	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	44	913	0	0	0	0	132	0	0	820	464	385	921	0	440	637	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	617	245	0	0	0	0	0	0	0	0	0	0	635	453	0	1237	608	790	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	581	27	0	0	0	0	0	0	0	0	0	0	465	632	1236	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	196	69	0	0	0	0	0	0	0	323	0	0	162	60	608	0	0	1118	855	218	122	471	0	0	0	0	214	0	0	1039	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	478	0	790	0	1117	0	0	975	493	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	861	0	0	1080	0	501	0	0	0	0	14	0	0	405	1042	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	967	1081	0	491	323	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	283	179	0	0	305	0	0	0	129	487	0	501	0	1317	1002	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	24	380	0	0	0	0	0	0	452	0	499	314	1321	0	153	952	0	0	883	30	0	0	262	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	744	0	0	0	0	0	0	0	0	0	0	0	997	148	0	1339	0	0	0	55	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	112	0	0	0	0	0	0	0	0	0	0	0	0	961	1348	0	0	0	287	1362	0	0	0	0	
From rank 24 to: 	0	0	0	0	30	588	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1219	0	1069	0	0	0	0	
From rank 25 to: 	0	0	0	0	84	387	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1219	0	864	328	391	0	239	562	
From rank 26 to: 	0	0	0	0	540	2	0	0	0	250	0	0	0	0	0	0	217	0	14	0	0	887	0	287	0	865	0	1011	269	311	801	0	
From rank 27 to: 	0	0	0	0	698	3	0	0	382	15	0	0	0	0	0	0	0	0	0	0	0	36	55	1349	1061	338	1015	0	0	0	0	0	
From rank 28 to: 	0	0	624	455	121	306	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	389	275	0	0	1141	236	1020	
From rank 29 to: 	379	380	110	248	68	0	0	0	0	38	0	0	0	0	0	0	1038	0	423	0	0	0	0	0	0	0	301	0	1121	0	699	118	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1039	0	0	263	0	0	0	236	801	0	236	711	0	1234	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	567	0	0	1020	122	1231	0	


After loadbalancing process 15 has 33901 cells.
After loadbalancing process 11 has 34322 cells.
After loadbalancing process 3 has 34472 cells.
After loadbalancing process 10 has 34581 cells.
After loadbalancing process 6 has 35074 cells.
After loadbalancing process 7 has 33920 cells.
After loadbalancing process 30 has 35681 cells.
After loadbalancing process 19 has 34624 cells.
After loadbalancing process 22 has 34869 cells.
After loadbalancing process 28 has 35648 cells.
After loadbalancing process 24 has 34007 cells.
After loadbalancing process 2 has 35249 cells.
After loadbalancing process 23 has 35327 cells.
After loadbalancing process 8 has 35931 cells.
After loadbalancing process 1 has 36686 cells.
After loadbalancing process 14 has 35475 cells.
After loadbalancing process 31 has 34019 cells.
After loadbalancing process 4 has 37334 cells.
After loadbalancing process 18 has 35383 cells.
After loadbalancing process 13 has 36186 cells.
After loadbalancing process 25 has 35779 cells.
After loadbalancing process 21 has 36975 cells.
After loadbalancing process 20 has 35881 cells.
After loadbalancing process 0 has 35129 cells.
After loadbalancing process 5 has 35023 cells.
After loadbalancing process 17 has 34958 cells.
After loadbalancing process 12 has 35684 cells.
After loadbalancing process 9 has 36945 cells.
After loadbalancing process 27 has 36451 cells.
After loadbalancing process 26 has 36950 cells.
After loadbalancing process 16 has 37160 cells.


After loadbalancing process 29 has 36025 cells.
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.6186         0.222653
    2          32.4485         0.583411
    3          23.1755         0.714224
    4          20.3373         0.877535
    5           18.089         0.889451
    6          14.2199         0.786109
    7          12.4486         0.875433
    8          11.9966         0.963689
    9          10.0096         0.834373
   10          8.87715         0.886861
   11          8.84526         0.996408
   12          7.80193         0.882047
   13          6.84881         0.877835
   14          6.96417          1.01684
   15           6.3884         0.917324
   16          5.64929         0.884304
   17          5.68532          1.00638
   18          5.40704         0.951052
   19           4.8337         0.893964
   20          4.81418         0.995962
   21          4.57358         0.950023
   22          4.19446         0.917106
   23          4.16176         0.992204
   24          3.88108         0.932557
   25           3.5585         0.916883
   26          3.53573         0.993603
   27          3.33354         0.942813
   28          3.07628         0.922826
   29          3.08346          1.00234
   30          2.91845         0.946484
   31          2.78624           0.9547
   32          2.89996          1.04082
   33          2.94252          1.01468
   34          3.05143          1.03701
   35          3.17731          1.04125
   36          2.86931         0.903063
   37          2.39698         0.835385
   38          1.92066         0.801282
   39          1.47373         0.767307
   40          1.23159         0.835694
   41          1.12242         0.911359
   42         0.998058         0.889201
   43         0.860401         0.862075
   44         0.726525         0.844403
   45         0.614132           0.8453
   46         0.533486         0.868684
   47         0.454583         0.852099
   48         0.373353         0.821309
   49         0.315339         0.844615
   50         0.268057         0.850058
   51         0.223225         0.832752
   52         0.188999         0.846675
   53         0.161477         0.854382
   54         0.136266         0.843874
   55         0.117816         0.864604
   56        0.0986676         0.837469
   57        0.0814274          0.82527
   58        0.0684166         0.840216
   59        0.0546356         0.798572
   60        0.0452321         0.827888
   61        0.0365859         0.808849
   62         0.028632         0.782596
   63        0.0229723         0.802331
=== rate=0.86284, T=5.42411, TIT=0.0860969, IT=63

 Elapsed time: 5.42411
Rank 0: Matrix-vector product took 0.0285777 seconds
Rank 1: Matrix-vector product took 0.0300255 seconds
Rank 2: Matrix-vector product took 0.0292966 seconds
Rank 3: Matrix-vector product took 0.027833 seconds
Rank 4: Matrix-vector product took 0.0302435 seconds
Rank 5: Matrix-vector product took 0.0284445 seconds
Rank 6: Matrix-vector product took 0.0286794 seconds
Rank 7: Matrix-vector product took 0.0279433 seconds
Rank 8: Matrix-vector product took 0.0294617 seconds
Rank 9: Matrix-vector product took 0.0299109 seconds
Rank 10: Matrix-vector product took 0.0278748 seconds
Rank 11: Matrix-vector product took 0.0283965 seconds
Rank 12: Matrix-vector product took 0.029022 seconds
Rank 13: Matrix-vector product took 0.0292062 seconds
Rank 14: Matrix-vector product took 0.0290541 seconds
Rank 15: Matrix-vector product took 0.0274181 seconds
Rank 16: Matrix-vector product took 0.0304862 seconds
Rank 17: Matrix-vector product took 0.0280863 seconds
Rank 18: Matrix-vector product took 0.0288799 seconds
Rank 19: Matrix-vector product took 0.0280325 seconds
Rank 20: Matrix-vector product took 0.0290249 seconds
Rank 21: Matrix-vector product took 0.0298912 seconds
Rank 22: Matrix-vector product took 0.0286513 seconds
Rank 23: Matrix-vector product took 0.0293494 seconds
Rank 24: Matrix-vector product took 0.0280307 seconds
Rank 25: Matrix-vector product took 0.029608 seconds
Rank 26: Matrix-vector product took 0.0297954 seconds
Rank 27: Matrix-vector product took 0.0296561 seconds
Rank 28: Matrix-vector product took 0.0285882 seconds
Rank 29: Matrix-vector product took 0.0290186 seconds
Rank 30: Matrix-vector product took 0.0292615 seconds
Rank 31: Matrix-vector product took 0.0274962 seconds
Average time for Matrix-vector product is 0.0289139 seconds

Rank 0: copyOwnerToAll took 0.000567599 seconds
Rank 1: copyOwnerToAll took 0.000567599 seconds
Rank 2: copyOwnerToAll took 0.000567599 seconds
Rank 3: copyOwnerToAll took 0.000567599 seconds
Rank 4: copyOwnerToAll took 0.000567599 seconds
Rank 5: copyOwnerToAll took 0.000567599 seconds
Rank 6: copyOwnerToAll took 0.000567599 seconds
Rank 7: copyOwnerToAll took 0.000567599 seconds
Rank 8: copyOwnerToAll took 0.000567599 seconds
Rank 9: copyOwnerToAll took 0.000567599 seconds
Rank 10: copyOwnerToAll took 0.000567599 seconds
Rank 11: copyOwnerToAll took 0.000567599 seconds
Rank 12: copyOwnerToAll took 0.000567599 seconds
Rank 13: copyOwnerToAll took 0.000567599 seconds
Rank 14: copyOwnerToAll took 0.000567599 seconds
Rank 15: copyOwnerToAll took 0.000567599 seconds
Rank 16: copyOwnerToAll took 0.000567599 seconds
Rank 17: copyOwnerToAll took 0.000567599 seconds
Rank 18: copyOwnerToAll took 0.000567599 seconds
Rank 19: copyOwnerToAll took 0.000567599 seconds
Rank 20: copyOwnerToAll took 0.000567599 seconds
Rank 21: copyOwnerToAll took 0.000567599 seconds
Rank 22: copyOwnerToAll took 0.000567599 seconds
Rank 23: copyOwnerToAll took 0.000567599 seconds
Rank 24: copyOwnerToAll took 0.000567599 seconds
Rank 25: copyOwnerToAll took 0.000567599 seconds
Rank 26: copyOwnerToAll took 0.000567599 seconds
Rank 27: copyOwnerToAll took 0.000567599 seconds
Rank 28: copyOwnerToAll took 0.000567599 seconds
Rank 29: copyOwnerToAll took 0.000567599 seconds
Rank 30: copyOwnerToAll took 0.000567599 seconds
Rank 31: copyOwnerToAll took 0.000567599 seconds
Average time for copyOwnertoAll is 0.000593218 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	1063	0	1009	0	0	0	0	0	0	0	0	0	44	626	585	196	0	0	0	0	0	0	0	0	0	0	0	0	379	0	0	
Rank 1's ghost cells:	1068	0	1065	207	458	0	395	164	0	298	0	0	0	900	240	24	68	0	0	0	0	0	0	0	0	0	0	0	0	380	0	0	
Rank 2's ghost cells:	0	1066	0	1200	321	505	0	596	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	624	102	0	0	
Rank 3's ghost cells:	1004	220	1198	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	455	225	0	0	
Rank 4's ghost cells:	0	467	325	0	0	1147	826	199	420	951	100	0	0	0	0	0	0	0	0	0	0	0	0	0	26	83	539	696	124	58	0	0	
Rank 5's ghost cells:	0	0	509	0	1148	0	301	986	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	587	387	2	3	295	0	0	0	
Rank 6's ghost cells:	0	395	0	0	827	315	0	1165	79	37	1084	0	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	180	596	0	204	984	1159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	418	0	76	0	0	912	521	771	318	0	0	0	0	0	0	0	261	24	746	112	0	0	0	382	0	0	0	0	
Rank 9's ghost cells:	0	316	0	0	954	0	37	0	930	0	605	271	715	830	0	0	327	0	0	0	179	379	0	0	0	0	247	15	0	32	0	0	
Rank 10's ghost cells:	0	0	0	0	107	0	1092	0	521	614	0	984	0	466	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	781	270	984	0	612	381	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	319	709	0	599	0	917	637	468	174	476	0	0	305	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	44	913	0	0	0	0	132	0	0	820	464	385	921	0	440	637	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	617	245	0	0	0	0	0	0	0	0	0	0	635	453	0	1237	608	790	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	581	27	0	0	0	0	0	0	0	0	0	0	465	632	1236	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	196	69	0	0	0	0	0	0	0	323	0	0	162	60	608	0	0	1118	855	218	122	471	0	0	0	0	214	0	0	1039	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	478	0	790	0	1117	0	0	975	493	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	861	0	0	1080	0	501	0	0	0	0	14	0	0	405	1042	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	967	1081	0	491	323	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	283	179	0	0	305	0	0	0	129	487	0	501	0	1317	1002	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	24	380	0	0	0	0	0	0	452	0	499	314	1321	0	153	952	0	0	883	30	0	0	262	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	744	0	0	0	0	0	0	0	0	0	0	0	997	148	0	1339	0	0	0	55	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	112	0	0	0	0	0	0	0	0	0	0	0	0	961	1348	0	0	0	287	1362	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	30	588	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1219	0	1069	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	84	387	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1219	0	864	328	391	0	239	562	
Rank 26's ghost cells:	0	0	0	0	540	2	0	0	0	250	0	0	0	0	0	0	217	0	14	0	0	887	0	287	0	865	0	1011	269	311	801	0	
Rank 27's ghost cells:	0	0	0	0	698	3	0	0	382	15	0	0	0	0	0	0	0	0	0	0	0	36	55	1349	1061	338	1015	0	0	0	0	0	
Rank 28's ghost cells:	0	0	624	455	121	306	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	389	275	0	0	1141	236	1020	
Rank 29's ghost cells:	379	380	110	248	68	0	0	0	0	38	0	0	0	0	0	0	1038	0	423	0	0	0	0	0	0	0	301	0	1121	0	699	118	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1039	0	0	263	0	0	0	236	801	0	236	711	0	1234	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	567	0	0	1020	122	1231	0	
