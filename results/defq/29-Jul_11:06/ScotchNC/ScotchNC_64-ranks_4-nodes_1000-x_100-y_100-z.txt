Number of cores/ranks per node is: 16
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 2506528
Cell on rank 1 before loadbalancing: 2507530
Cell on rank 2 before loadbalancing: 2491681
Cell on rank 3 before loadbalancing: 2494261


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	2717	59	2281	66	1120	2063	0	1925	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2722	0	2221	314	546	0	640	1788	515	1995	80	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	59	2219	0	2752	2220	0	0	0	0	440	191	2271	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	2271	305	2760	0	293	2048	0	0	489	0	2616	159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	66	564	2220	280	0	2724	290	2868	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1833	218	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	1131	0	0	2055	2721	0	2537	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2219	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	2053	659	0	0	286	2531	0	3197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	7	466	2167	466	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	1791	0	0	2867	20	3182	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	0	0	2286	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	1925	500	0	486	0	0	0	0	0	2376	2436	130	1963	0	0	213	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	1995	442	0	0	0	0	0	2378	0	175	2918	357	1973	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	81	181	2616	0	0	0	0	2434	182	0	2823	170	0	358	2181	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	81	2275	155	0	0	0	0	134	2922	2818	0	258	645	2266	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	1963	369	176	271	0	2963	443	3101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2082	105	111	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	1973	0	648	2965	0	2434	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	2413	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	366	2258	439	2434	0	2701	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	459	150	1985	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	219	0	2183	59	3102	0	2702	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	2599	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2713	2704	342	175	1916	246	0	0	0	0	0	0	83	76	2570	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2714	0	0	2587	2265	205	0	0	0	0	0	0	122	0	2486	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2705	0	0	2578	78	110	2350	504	0	0	0	0	0	2028	0	198	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	376	2585	2578	0	622	0	0	2200	0	0	0	0	2227	470	161	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	2260	78	635	0	2806	0	2855	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1913	213	112	0	2808	0	2729	511	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	267	0	2349	0	0	2729	0	2904	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	484	2200	2852	540	2903	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	1835	0	6	678	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2740	201	2736	2274	204	496	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	226	2216	459	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2739	0	2654	261	41	1895	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	2205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	194	2651	0	2961	63	154	0	2482	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	467	2249	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2719	282	2963	0	40	0	2295	436	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	0	2235	0	0	0	0	2281	41	61	40	0	2728	2713	544	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	2031	478	0	0	0	0	199	1895	150	0	2734	0	0	2699	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	73	2488	0	154	0	0	0	0	510	0	0	2293	2724	0	0	2771	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2566	115	184	34	0	0	0	0	0	33	2480	433	523	2701	2773	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2668	3083	226	1957	23	0	582	2306	220	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2680	0	300	2392	322	2359	0	86	335	1878	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3058	305	0	2655	0	0	246	2534	94	27	2379	176	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	226	2401	2653	0	0	296	2029	160	0	731	100	2084	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1957	357	0	0	0	2985	168	2682	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	2359	0	296	2983	0	2738	482	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	243	2029	165	2737	0	2817	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	591	91	2537	170	2682	490	2821	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2314	328	84	0	0	0	0	0	0	2527	2406	373	0	23	2426	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	221	1879	28	712	0	0	0	0	2526	0	0	2604	321	0	123	2051	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	0	2382	110	0	0	0	0	2402	0	0	2539	0	2238	479	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	159	2094	0	0	0	0	388	2600	2540	0	2125	591	57	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	326	0	2125	0	2673	0	2801	0	0	0	0	0	2494	218	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	2239	614	2673	0	2495	506	0	0	0	0	2359	188	7	42	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2433	135	466	59	0	2495	0	3020	0	0	0	0	54	0	279	1942	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	2049	0	182	2795	539	3022	0	0	0	0	0	0	0	2440	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2233	36	2647	0	522	2142	723	0	0	0	0	0	2124	391	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2231	0	2885	215	293	0	0	2293	0	0	0	0	2346	681	127	17	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	2883	0	2689	2035	26	0	154	0	0	0	0	424	0	294	2013	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2644	199	2687	0	226	2251	0	88	0	0	0	0	0	0	2132	92	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2367	54	0	0	302	2035	230	0	2813	299	2826	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2492	188	0	0	532	0	26	2252	2817	0	2693	254	0	0	0	0	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	225	7	303	2438	2142	0	0	0	295	2693	0	2671	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	1941	0	725	2292	162	98	2818	258	2691	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	2082	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2575	2593	160	0	0	2151	684	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	103	2412	460	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2574	0	395	2897	54	0	0	2409	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	110	0	152	2596	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2592	389	0	2806	0	1685	328	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1996	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	2886	2807	0	2270	368	112	308	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2346	430	0	0	0	0	0	0	56	0	2269	0	2196	442	2688	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2144	687	0	0	0	0	0	0	0	0	1683	373	2193	0	2505	0	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	384	132	303	2125	0	0	0	0	2149	0	324	103	432	2504	0	2706	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	2014	96	0	0	0	0	703	2410	0	335	2683	0	2720	0	


After loadbalancing process 38 has 163074 cells.
After loadbalancing process 53 has 166321 cells.
After loadbalancing process 8 has 167279 cells.
After loadbalancing process 33 has 167595 cells.
After loadbalancing process 39 has 165058 cells.
After loadbalancing process 46 has 166026 cells.
After loadbalancing process 23 has 164431 cells.
After loadbalancing process 9 has 166896 cells.
After loadbalancing process 48 has 167193 cells.
After loadbalancing process 20 has 164679 cells.
After loadbalancing process 36 has 163121 cells.
After loadbalancing process 45 has 165847 cells.
After loadbalancing process 0 has 166727 cells.
After loadbalancing process 35 has 167756 cells.
After loadbalancing process 32 has 167822 cells.
After loadbalancing process 28 has 167932 cells.
After loadbalancing process 4 has 168206 cells.
After loadbalancing process 40 has 166322 cells.
After loadbalancing process 16 has 168578 cells.
After loadbalancing process 2 has 167639 cells.
After loadbalancing process 10 has 168897 cells.
After loadbalancing process 21 has 164202 cells.
After loadbalancing process 3 has 168663 cells.
After loadbalancing process 31 has 169867 cells.
After loadbalancing process 12 has 167952 cells.
After loadbalancing process 11 has 169027 cells.
After loadbalancing process 22 has 163548 cells.
After loadbalancing process 5 has 166740 cells.
After loadbalancing process 18 has 166556 cells.
After loadbalancing process 62 has 168289 cells.
After loadbalancing process 17 has 166856 cells.
After loadbalancing process 30 has 169039 cells.
After loadbalancing process 41 has 167615 cells.
After loadbalancing process 34 has 168007 cells.
After loadbalancing process 50 has 167200 cells.
After loadbalancing process 44 has 165067 cells.
After loadbalancing process 14 has 166311 cells.
After loadbalancing process 19 has 169048 cells.
After loadbalancing process 61 has 166360 cells.
After loadbalancing process 60 has 165992 cells.
After loadbalancing process 29 has 167910 cells.
After loadbalancing process 6 has 167583 cells.
After loadbalancing process 51 has 166693 cells.
After loadbalancing process 1 has 168912 cells.
After loadbalancing process 25 has 167013 cells.
After loadbalancing process 15 has 166635 cells.
After loadbalancing process 49 has 167902 cells.
After loadbalancing process 37 has 163951 cells.
After loadbalancing process 47 has 165368 cells.
After loadbalancing process 43 has 166860 cells.
After loadbalancing process 13 has 165999 cells.
After loadbalancing process 42 has 166175 cells.
After loadbalancing process 7 has 166421 cells.
After loadbalancing process 26 has 167232 cells.
After loadbalancing process 54 has 165554 cells.
After loadbalancing process 24 has 168018 cells.
After loadbalancing process 59 has 166354 cells.
After loadbalancing process 27 has 167816 cells.
After loadbalancing process 55 has 166548 cells.
After loadbalancing process 63 has 167243 cells.
After loadbalancing process 56 has 165699 cells.
After loadbalancing process 57 has 166074 cells.
After loadbalancing process 52 has 166780 cells.
After loadbalancing process 58 has 166226 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          142.094         0.216792
    2          82.2081         0.578547
    3          58.2874         0.709023
    4          49.6446         0.851721
    5          46.7772         0.942241
    6          37.8758         0.809706
    7          31.3853         0.828638
    8          30.9374         0.985727
    9          27.3144         0.882893
   10          22.9857         0.841524
   11           22.783         0.991181
   12          21.5088         0.944071
   13          18.1574         0.844188
   14          18.0599         0.994627
   15          17.7371          0.98213
   16          15.1064         0.851682
   17          14.7503         0.976425
   18          15.0278          1.01882
   19          13.1017         0.871831
   20          12.7431         0.972627
   21          12.8894          1.01148
   22          11.5509         0.896153
   23          11.3126          0.97937
   24          11.2242          0.99219
   25          10.2837         0.916211
   26          10.2226         0.994053
   27          9.88746         0.967217
   28          9.23877         0.934393
   29          9.41143          1.01869
   30          9.09166         0.966023
   31          9.09069         0.999893
   32          10.1375          1.11515
   33          10.9196          1.07715
   34          11.8397          1.08426
   35          11.9287          1.00752
   36          9.77408         0.819377
   37          7.96205         0.814609
   38          6.99584         0.878649
   39          5.93238         0.847986
   40          5.06583          0.85393
   41          4.51528          0.89132
   42            4.138         0.916444
   43          3.59929         0.869814
   44          3.00356         0.834486
   45          2.78503         0.927243
   46          2.48319         0.891621
   47           2.0266          0.81613
   48          1.88286         0.929072
   49          1.67061         0.887271
   50           1.4176         0.848553
   51          1.36375         0.962014
   52          1.20614         0.884428
   53          1.10506         0.916195
   54          1.05291         0.952811
   55         0.947184         0.899585
   56         0.915565         0.966618
   57         0.852996          0.93166
   58         0.831116         0.974349
   59          0.78649         0.946307
   60         0.767774         0.976202
   61         0.756557         0.985391
   62         0.715614         0.945882
   63         0.733502            1.025
   64         0.681714         0.929396
   65         0.676707         0.992656
   66         0.635576         0.939218
   67         0.589265         0.927137
   68         0.535436         0.908651
   69         0.466072         0.870453
   70         0.383218         0.822228
   71         0.308862         0.805971
   72         0.251451         0.814121
   73         0.213253         0.848089
   74          0.21269          0.99736
   75         0.214609          1.00902
   76         0.227538          1.06025
   77          0.23199          1.01956
   78         0.237692          1.02458
   79         0.232489         0.978111
   80         0.211748         0.910786
   81         0.175618         0.829374
   82         0.164178          0.93486
   83         0.153794          0.93675
   84         0.138016         0.897407
   85         0.122351         0.886498
   86         0.119833         0.979422
   87         0.117099         0.977185
   88         0.112201         0.958173
   89         0.106269         0.947128
   90         0.104736         0.985575
   91        0.0971933         0.927985
   92        0.0925776          0.95251
   93         0.089176         0.963257
   94        0.0808645         0.906797
   95        0.0739123         0.914027
   96        0.0695839         0.941438
   97        0.0612332         0.879992
=== rate=0.908779, T=38.6195, TIT=0.398139, IT=97

 Elapsed time: 38.6195
Rank 0: Matrix-vector product took 0.138351 seconds
Rank 1: Matrix-vector product took 0.138045 seconds
Rank 2: Matrix-vector product took 0.137552 seconds
Rank 3: Matrix-vector product took 0.138245 seconds
Rank 4: Matrix-vector product took 0.13723 seconds
Rank 5: Matrix-vector product took 0.138216 seconds
Rank 6: Matrix-vector product took 0.136867 seconds
Rank 7: Matrix-vector product took 0.136373 seconds
Rank 8: Matrix-vector product took 0.136785 seconds
Rank 9: Matrix-vector product took 0.136124 seconds
Rank 10: Matrix-vector product took 0.138333 seconds
Rank 11: Matrix-vector product took 0.137592 seconds
Rank 12: Matrix-vector product took 0.137486 seconds
Rank 13: Matrix-vector product took 0.137396 seconds
Rank 14: Matrix-vector product took 0.136238 seconds
Rank 15: Matrix-vector product took 0.136239 seconds
Rank 16: Matrix-vector product took 0.139816 seconds
Rank 17: Matrix-vector product took 0.136223 seconds
Rank 18: Matrix-vector product took 0.135482 seconds
Rank 19: Matrix-vector product took 0.138751 seconds
Rank 20: Matrix-vector product took 0.134399 seconds
Rank 21: Matrix-vector product took 0.133825 seconds
Rank 22: Matrix-vector product took 0.133785 seconds
Rank 23: Matrix-vector product took 0.134542 seconds
Rank 24: Matrix-vector product took 0.137049 seconds
Rank 25: Matrix-vector product took 0.136363 seconds
Rank 26: Matrix-vector product took 0.136301 seconds
Rank 27: Matrix-vector product took 0.137311 seconds
Rank 28: Matrix-vector product took 0.13707 seconds
Rank 29: Matrix-vector product took 0.136815 seconds
Rank 30: Matrix-vector product took 0.14742 seconds
Rank 31: Matrix-vector product took 0.138587 seconds
Rank 32: Matrix-vector product took 0.136781 seconds
Rank 33: Matrix-vector product took 0.136819 seconds
Rank 34: Matrix-vector product took 0.137066 seconds
Rank 35: Matrix-vector product took 0.136673 seconds
Rank 36: Matrix-vector product took 0.133623 seconds
Rank 37: Matrix-vector product took 0.134386 seconds
Rank 38: Matrix-vector product took 0.13452 seconds
Rank 39: Matrix-vector product took 0.134729 seconds
Rank 40: Matrix-vector product took 0.138173 seconds
Rank 41: Matrix-vector product took 0.137302 seconds
Rank 42: Matrix-vector product took 0.13524 seconds
Rank 43: Matrix-vector product took 0.135746 seconds
Rank 44: Matrix-vector product took 0.134974 seconds
Rank 45: Matrix-vector product took 0.135347 seconds
Rank 46: Matrix-vector product took 0.137918 seconds
Rank 47: Matrix-vector product took 0.135072 seconds
Rank 48: Matrix-vector product took 0.138695 seconds
Rank 49: Matrix-vector product took 0.137212 seconds
Rank 50: Matrix-vector product took 0.136239 seconds
Rank 51: Matrix-vector product took 0.135845 seconds
Rank 52: Matrix-vector product took 0.139264 seconds
Rank 53: Matrix-vector product took 0.135473 seconds
Rank 54: Matrix-vector product took 0.13742 seconds
Rank 55: Matrix-vector product took 0.137914 seconds
Rank 56: Matrix-vector product took 0.135948 seconds
Rank 57: Matrix-vector product took 0.138034 seconds
Rank 58: Matrix-vector product took 0.136191 seconds
Rank 59: Matrix-vector product took 0.136412 seconds
Rank 60: Matrix-vector product took 0.135607 seconds
Rank 61: Matrix-vector product took 0.135506 seconds
Rank 62: Matrix-vector product took 0.137004 seconds
Rank 63: Matrix-vector product took 0.140425 seconds
Average time for Matrix-vector product is 0.136849 seconds

Rank 0: copyOwnerToAll took 0.000894709 seconds
Rank 1: copyOwnerToAll took 0.000894709 seconds
Rank 2: copyOwnerToAll took 0.000894709 seconds
Rank 3: copyOwnerToAll took 0.000894709 seconds
Rank 4: copyOwnerToAll took 0.000894709 seconds
Rank 5: copyOwnerToAll took 0.000894709 seconds
Rank 6: copyOwnerToAll took 0.000894709 seconds
Rank 7: copyOwnerToAll took 0.000894709 seconds
Rank 8: copyOwnerToAll took 0.000894709 seconds
Rank 9: copyOwnerToAll took 0.000894709 seconds
Rank 10: copyOwnerToAll took 0.000894709 seconds
Rank 11: copyOwnerToAll took 0.000894709 seconds
Rank 12: copyOwnerToAll took 0.000894709 seconds
Rank 13: copyOwnerToAll took 0.000894709 seconds
Rank 14: copyOwnerToAll took 0.000894709 seconds
Rank 15: copyOwnerToAll took 0.000894709 seconds
Rank 16: copyOwnerToAll took 0.000894709 seconds
Rank 17: copyOwnerToAll took 0.000894709 seconds
Rank 18: copyOwnerToAll took 0.000894709 seconds
Rank 19: copyOwnerToAll took 0.000894709 seconds
Rank 20: copyOwnerToAll took 0.000894709 seconds
Rank 21: copyOwnerToAll took 0.000894709 seconds
Rank 22: copyOwnerToAll took 0.000894709 seconds
Rank 23: copyOwnerToAll took 0.000894709 seconds
Rank 24: copyOwnerToAll took 0.000894709 seconds
Rank 25: copyOwnerToAll took 0.000894709 seconds
Rank 26: copyOwnerToAll took 0.000894709 seconds
Rank 27: copyOwnerToAll took 0.000894709 seconds
Rank 28: copyOwnerToAll took 0.000894709 seconds
Rank 29: copyOwnerToAll took 0.000894709 seconds
Rank 30: copyOwnerToAll took 0.000894709 seconds
Rank 31: copyOwnerToAll took 0.000894709 seconds
Rank 32: copyOwnerToAll took 0.000894709 seconds
Rank 33: copyOwnerToAll took 0.000894709 seconds
Rank 34: copyOwnerToAll took 0.000894709 seconds
Rank 35: copyOwnerToAll took 0.000894709 seconds
Rank 36: copyOwnerToAll took 0.000894709 seconds
Rank 37: copyOwnerToAll took 0.000894709 seconds
Rank 38: copyOwnerToAll took 0.000894709 seconds
Rank 39: copyOwnerToAll took 0.000894709 seconds
Rank 40: copyOwnerToAll took 0.000894709 seconds
Rank 41: copyOwnerToAll took 0.000894709 seconds
Rank 42: copyOwnerToAll took 0.000894709 seconds
Rank 43: copyOwnerToAll took 0.000894709 seconds
Rank 44: copyOwnerToAll took 0.000894709 seconds
Rank 45: copyOwnerToAll took 0.000894709 seconds
Rank 46: copyOwnerToAll took 0.000894709 seconds
Rank 47: copyOwnerToAll took 0.000894709 seconds
Rank 48: copyOwnerToAll took 0.000894709 seconds
Rank 49: copyOwnerToAll took 0.000894709 seconds
Rank 50: copyOwnerToAll took 0.000894709 seconds
Rank 51: copyOwnerToAll took 0.000894709 seconds
Rank 52: copyOwnerToAll took 0.000894709 seconds
Rank 53: copyOwnerToAll took 0.000894709 seconds
Rank 54: copyOwnerToAll took 0.000894709 seconds
Rank 55: copyOwnerToAll took 0.000894709 seconds
Rank 56: copyOwnerToAll took 0.000894709 seconds
Rank 57: copyOwnerToAll took 0.000894709 seconds
Rank 58: copyOwnerToAll took 0.000894709 seconds
Rank 59: copyOwnerToAll took 0.000894709 seconds
Rank 60: copyOwnerToAll took 0.000894709 seconds
Rank 61: copyOwnerToAll took 0.000894709 seconds
Rank 62: copyOwnerToAll took 0.000894709 seconds
Rank 63: copyOwnerToAll took 0.000894709 seconds
Average time for copyOwnertoAll is 0.000927412 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	2717	59	2281	66	1120	2063	0	1925	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2722	0	2221	314	546	0	640	1788	515	1995	80	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	59	2219	0	2752	2220	0	0	0	0	440	191	2271	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	2271	305	2760	0	293	2048	0	0	489	0	2616	159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	66	564	2220	280	0	2724	290	2868	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1833	218	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	1131	0	0	2055	2721	0	2537	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2219	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	2053	659	0	0	286	2531	0	3197	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	7	466	2167	466	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	1791	0	0	2867	20	3182	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	0	0	2286	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	1925	500	0	486	0	0	0	0	0	2376	2436	130	1963	0	0	213	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	1995	442	0	0	0	0	0	2378	0	175	2918	357	1973	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	81	181	2616	0	0	0	0	2434	182	0	2823	170	0	358	2181	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	81	2275	155	0	0	0	0	134	2922	2818	0	258	645	2266	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	1963	369	176	271	0	2963	443	3101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2082	105	111	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	1973	0	648	2965	0	2434	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	2413	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	366	2258	439	2434	0	2701	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	459	150	1985	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	219	0	2183	59	3102	0	2702	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	2599	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2713	2704	342	175	1916	246	0	0	0	0	0	0	83	76	2570	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2714	0	0	2587	2265	205	0	0	0	0	0	0	122	0	2486	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2705	0	0	2578	78	110	2350	504	0	0	0	0	0	2028	0	198	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	376	2585	2578	0	622	0	0	2200	0	0	0	0	2227	470	161	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	2260	78	635	0	2806	0	2855	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1913	213	112	0	2808	0	2729	511	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	267	0	2349	0	0	2729	0	2904	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	484	2200	2852	540	2903	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	1835	0	6	678	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2740	201	2736	2274	204	496	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	226	2216	459	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2739	0	2654	261	41	1895	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	2205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	194	2651	0	2961	63	154	0	2482	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	467	2249	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2719	282	2963	0	40	0	2295	436	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	0	2235	0	0	0	0	2281	41	61	40	0	2728	2713	544	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	2031	478	0	0	0	0	199	1895	150	0	2734	0	0	2699	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	73	2488	0	154	0	0	0	0	510	0	0	2293	2724	0	0	2771	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2566	115	184	34	0	0	0	0	0	33	2480	433	523	2701	2773	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2668	3083	226	1957	23	0	582	2306	220	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2680	0	300	2392	322	2359	0	86	335	1878	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3058	305	0	2655	0	0	246	2534	94	27	2379	176	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	226	2401	2653	0	0	296	2029	160	0	731	100	2084	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1957	357	0	0	0	2985	168	2682	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	2359	0	296	2983	0	2738	482	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	243	2029	165	2737	0	2817	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	591	91	2537	170	2682	490	2821	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2314	328	84	0	0	0	0	0	0	2527	2406	373	0	23	2426	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	221	1879	28	712	0	0	0	0	2526	0	0	2604	321	0	123	2051	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	0	2382	110	0	0	0	0	2402	0	0	2539	0	2238	479	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	159	2094	0	0	0	0	388	2600	2540	0	2125	591	57	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	326	0	2125	0	2673	0	2801	0	0	0	0	0	2494	218	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	2239	614	2673	0	2495	506	0	0	0	0	2359	188	7	42	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2433	135	466	59	0	2495	0	3020	0	0	0	0	54	0	279	1942	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	2049	0	182	2795	539	3022	0	0	0	0	0	0	0	2440	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2233	36	2647	0	522	2142	723	0	0	0	0	0	2124	391	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2231	0	2885	215	293	0	0	2293	0	0	0	0	2346	681	127	17	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	2883	0	2689	2035	26	0	154	0	0	0	0	424	0	294	2013	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2644	199	2687	0	226	2251	0	88	0	0	0	0	0	0	2132	92	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2367	54	0	0	302	2035	230	0	2813	299	2826	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2492	188	0	0	532	0	26	2252	2817	0	2693	254	0	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	225	7	303	2438	2142	0	0	0	295	2693	0	2671	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	1941	0	725	2292	162	98	2818	258	2691	0	0	0	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	2082	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2575	2593	160	0	0	2151	684	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	103	2412	460	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2574	0	395	2897	54	0	0	2409	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	110	0	152	2596	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2592	389	0	2806	0	1685	328	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1996	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	2886	2807	0	2270	368	112	308	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2346	430	0	0	0	0	0	0	56	0	2269	0	2196	442	2688	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2144	687	0	0	0	0	0	0	0	0	1683	373	2193	0	2505	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	384	132	303	2125	0	0	0	0	2149	0	324	103	432	2504	0	2706	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	2014	96	0	0	0	0	703	2410	0	335	2683	0	2720	0	
