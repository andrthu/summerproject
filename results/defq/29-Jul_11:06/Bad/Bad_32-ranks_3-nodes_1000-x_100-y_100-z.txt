Number of cores/ranks per node is: 10
Bad partitioner
 Cell on rank 0 before loadbalancing: 312500
Cell on rank 1 before loadbalancing: 312493
Cell on rank 2 before loadbalancing: 312513
Cell on rank 3 before loadbalancing: 312494
Cell on rank 4 before loadbalancing: 312502
Cell on rank 5 before loadbalancing: 312517
Cell on rank 6 before loadbalancing: 312489
Cell on rank 7 before loadbalancing: 312499
Cell on rank 8 before loadbalancing: 312499
Cell on rank 9 before loadbalancing: 312503
Cell on rank 10 before loadbalancing: 312501
Cell on rank 11 before loadbalancing: 312498
Cell on rank 12 before loadbalancing: 312504
Cell on rank 13 before loadbalancing: 312496
Cell on rank 14 before loadbalancing: 312498
Cell on rank 15 before loadbalancing: 312487
Cell on rank 16 before loadbalancing: 312498
Cell on rank 17 before loadbalancing: 312508
Cell on rank 18 before loadbalancing: 312501
Cell on rank 19 before loadbalancing: 312502
Cell on rank 20 before loadbalancing: 312498
Cell on rank 21 before loadbalancing: 312503
Cell on rank 22 before loadbalancing: 312489
Cell on rank 23 before loadbalancing: 312503
Cell on rank 24 before loadbalancing: 312510
Cell on rank 25 before loadbalancing: 312504
Cell on rank 26 before loadbalancing: 312497
Cell on rank 27 before loadbalancing: 312501
Cell on rank 28 before loadbalancing: 312495
Cell on rank 29 before loadbalancing: 312495
Cell on rank 30 before loadbalancing: 312506
Cell on rank 31 before loadbalancing: 312497
Edge-cut: 398439


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	2561	6017	358	0	0	5598	452	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2557	0	1621	5406	0	3710	1732	0	0	0	0	0	613	0	0	3039	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	5990	1623	0	5607	2378	1025	31	5112	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	351	5423	5586	0	3626	1960	0	0	0	851	0	3703	406	0	0	1783	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	2358	3631	0	5551	0	3140	0	3554	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	3689	1017	1951	5535	0	2439	843	0	1281	0	0	4194	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	5594	1721	36	0	0	2462	0	6193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	434	0	5111	0	3118	840	6202	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	3227	6023	630	1071	5174	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1451	0	0	3069	
From rank 9 to: 	0	0	0	867	3520	1276	0	0	3236	0	12	5889	4712	0	0	517	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	6044	12	0	2545	0	2062	5348	87	0	0	0	0	0	0	0	0	0	0	0	3333	302	0	0	1329	
From rank 11 to: 	0	0	0	3770	0	0	0	0	638	5895	2545	0	702	27	305	5762	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	606	0	406	0	4172	0	0	1072	4697	0	702	0	3571	1028	5323	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	5113	0	2063	36	3563	0	5792	0	0	0	0	0	0	0	0	0	0	0	835	273	2213	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	5411	307	1021	5789	0	3143	0	0	0	0	0	0	0	0	0	0	1742	1817	0	0	0	0	
From rank 15 to: 	0	3014	0	1790	0	0	0	0	0	517	92	5779	5317	0	3140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3447	0	5868	416	0	978	5606	602	4864	0	0	0	201	12	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3448	0	4882	1737	0	0	5183	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4881	0	3719	0	5282	1052	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5865	1744	3724	0	6200	1242	633	93	3113	0	0	0	0	0	266	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	428	0	0	6235	0	2500	270	6836	218	0	0	0	0	0	3798	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5318	1220	2501	0	5804	594	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	976	5182	1010	632	256	5803	0	3250	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5582	0	0	93	6863	588	3247	0	30	505	0	0	0	4442	664	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	592	0	0	3070	212	0	0	30	0	4582	1018	4634	259	1038	5276	932	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4903	0	0	0	0	0	0	501	4590	0	3260	6	0	5946	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	824	1758	0	0	0	0	0	0	0	0	0	1024	3261	0	6152	4694	548	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	3351	0	0	280	1838	0	0	0	0	0	0	0	0	0	4632	6	6185	0	2967	0	25	3428	
From rank 28 to: 	0	0	0	0	0	0	0	0	1425	0	310	0	0	2195	0	0	0	0	0	0	0	0	0	0	257	0	4630	2968	0	2800	661	6011	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	196	0	0	0	0	0	0	4477	1028	5947	550	0	2802	0	4759	228	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	12	0	0	262	3748	0	0	664	5322	0	0	25	664	4760	0	4864	
From rank 31 to: 	0	0	0	0	0	0	0	0	3094	0	1314	0	0	0	0	0	0	0	0	0	0	0	0	0	931	0	0	3479	6012	225	4886	0	

Edge-cut for node partition: 218282



After loadbalancing process 22 has 333466 cells.
After loadbalancing process 0 has 331171 cells.
After loadbalancing process 15 has 332532 cells.
After loadbalancing process 19 has 331710 cells.
After loadbalancing process 29 has 334081 cells.
After loadbalancing process 26 has 327758 cells.
After loadbalancing process 24 has 332783 cells.
After loadbalancing process 23 has 328204 cells.
After loadbalancing process 30 has 332482 cells.
After loadbalancing process 1 has 336183 cells.
After loadbalancing process 9 has 335213 cells.
After loadbalancing process 10 has 332827 cells.
After loadbalancing process 7 has 327940 cells.
After loadbalancing process 6 has 327435 cells.
After loadbalancing process 13 has 334153 cells.
After loadbalancing process 16 has 332384 cells.
After loadbalancing process 11 has 327486 cells.
After loadbalancing process 14 has 330736 cells.
After loadbalancing process 20 has 331728 cells.
After loadbalancing process 27 has 329598 cells.
After loadbalancing process 21 has 334279 cells.
After loadbalancing process 28 has 334517 cells.
After loadbalancing process 25 has 332142 cells.
After loadbalancing process 3 has 333752 cells.
After loadbalancing process 18 has 333563 cells.
After loadbalancing process 12 has 334492 cells.
After loadbalancing process 17 has 335382 cells.
After loadbalancing process 2 has 328495 cells.
After loadbalancing process 4 has 332136 cells.
After loadbalancing process 31 has 332438 cells.
After loadbalancing process 5 has 333144 cells.
After loadbalancing process 8 has 330758 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          141.036         0.215178
    2           81.349         0.576797
    3          57.4348         0.706029
    4          47.3143         0.823793
    5          43.8122         0.925981
    6          38.4548         0.877718
    7           31.231         0.812149
    8          29.8221         0.954887
    9          27.4675         0.921046
   10          22.8822         0.833066
   11          22.2489         0.972323
   12          21.5602         0.969043
   13          18.1639         0.842474
   14          17.6446         0.971412
   15          17.5752         0.996064
   16          15.1889         0.864223
   17          14.6192         0.962496
   18          14.6799          1.00415
   19          13.0845         0.891323
   20          12.7288         0.972815
   21          12.5796         0.988273
   22          11.2649         0.895489
   23          11.2087         0.995015
   24           10.996         0.981023
   25          9.88119         0.898617
   26          9.92073            1.004
   27          9.84577         0.992444
   28          8.91389         0.905352
   29          9.03654          1.01376
   30          9.21657          1.01992
   31          9.00876         0.977452
   32          10.1503          1.12671
   33          11.5379           1.1367
   34          11.9698          1.03743
   35          11.4656         0.957877
   36          8.85833         0.772603
   37          6.56121         0.740683
   38          6.03706         0.920114
   39          5.54046         0.917742
   40          4.58122         0.826865
   41           4.1481         0.905458
   42           3.8336         0.924183
   43          3.22715         0.841807
   44          2.78177          0.86199
   45          2.51592         0.904431
   46          2.16527         0.860626
   47          1.88882         0.872328
   48          1.70019         0.900134
   49           1.4878         0.875078
   50          1.38401         0.930239
   51          1.26641         0.915029
   52          1.13318         0.894792
   53          1.10406         0.974302
   54         0.988417         0.895261
   55         0.941219         0.952248
   56         0.918659         0.976031
   57         0.832071         0.905746
   58         0.844751          1.01524
   59         0.796361         0.942717
   60         0.773254         0.970984
   61          0.76818         0.993438
   62         0.742739         0.966882
   63         0.718168         0.966918
   64         0.698306         0.972345
   65         0.661557         0.947374
   66         0.593204         0.896678
   67         0.549069           0.9256
   68         0.453299         0.825576
   69         0.383994          0.84711
   70         0.312754         0.814478
   71         0.259807         0.830707
   72         0.232951         0.896629
   73         0.238722          1.02478
   74         0.246388          1.03211
   75           0.2622          1.06417
   76         0.251957         0.960934
   77         0.232309         0.922019
   78         0.220001         0.947016
   79         0.207586          0.94357
   80         0.181661         0.875112
   81         0.164336          0.90463
   82         0.147402         0.896957
   83         0.136017         0.922765
   84         0.127116         0.934555
   85         0.120603         0.948768
   86         0.114434          0.94885
   87         0.110495         0.965577
   88         0.103573         0.937354
   89          0.10075         0.972746
   90        0.0955107         0.947994
   91        0.0900612         0.942944
   92        0.0862564         0.957752
   93        0.0802836         0.930756
   94        0.0730237         0.909572
   95        0.0681529         0.933298
   96        0.0616691         0.904864
=== rate=0.907941, T=73.7333, TIT=0.768055, IT=96

 Elapsed time: 73.7333
Rank 0: Matrix-vector product took 0.270633 seconds
Rank 1: Matrix-vector product took 0.275016 seconds
Rank 2: Matrix-vector product took 0.269156 seconds
Rank 3: Matrix-vector product took 0.284345 seconds
Rank 4: Matrix-vector product took 0.276646 seconds
Rank 5: Matrix-vector product took 0.272082 seconds
Rank 6: Matrix-vector product took 0.267308 seconds
Rank 7: Matrix-vector product took 0.268381 seconds
Rank 8: Matrix-vector product took 0.271066 seconds
Rank 9: Matrix-vector product took 0.274636 seconds
Rank 10: Matrix-vector product took 0.271147 seconds
Rank 11: Matrix-vector product took 0.273925 seconds
Rank 12: Matrix-vector product took 0.281379 seconds
Rank 13: Matrix-vector product took 0.272479 seconds
Rank 14: Matrix-vector product took 0.275712 seconds
Rank 15: Matrix-vector product took 0.272383 seconds
Rank 16: Matrix-vector product took 0.272213 seconds
Rank 17: Matrix-vector product took 0.279162 seconds
Rank 18: Matrix-vector product took 0.278688 seconds
Rank 19: Matrix-vector product took 0.271047 seconds
Rank 20: Matrix-vector product took 0.278659 seconds
Rank 21: Matrix-vector product took 0.277675 seconds
Rank 22: Matrix-vector product took 0.277886 seconds
Rank 23: Matrix-vector product took 0.268729 seconds
Rank 24: Matrix-vector product took 0.271885 seconds
Rank 25: Matrix-vector product took 0.271896 seconds
Rank 26: Matrix-vector product took 0.268551 seconds
Rank 27: Matrix-vector product took 0.270378 seconds
Rank 28: Matrix-vector product took 0.276797 seconds
Rank 29: Matrix-vector product took 0.275004 seconds
Rank 30: Matrix-vector product took 0.274441 seconds
Rank 31: Matrix-vector product took 0.27219 seconds
Average time for Matrix-vector product is 0.273797 seconds

Rank 0: copyOwnerToAll took 0.00169074 seconds
Rank 1: copyOwnerToAll took 0.00169074 seconds
Rank 2: copyOwnerToAll took 0.00169074 seconds
Rank 3: copyOwnerToAll took 0.00169074 seconds
Rank 4: copyOwnerToAll took 0.00169074 seconds
Rank 5: copyOwnerToAll took 0.00169074 seconds
Rank 6: copyOwnerToAll took 0.00169074 seconds
Rank 7: copyOwnerToAll took 0.00169074 seconds
Rank 8: copyOwnerToAll took 0.00169074 seconds
Rank 9: copyOwnerToAll took 0.00169074 seconds
Rank 10: copyOwnerToAll took 0.00169074 seconds
Rank 11: copyOwnerToAll took 0.00169074 seconds
Rank 12: copyOwnerToAll took 0.00169074 seconds
Rank 13: copyOwnerToAll took 0.00169074 seconds
Rank 14: copyOwnerToAll took 0.00169074 seconds
Rank 15: copyOwnerToAll took 0.00169074 seconds
Rank 16: copyOwnerToAll took 0.00169074 seconds
Rank 17: copyOwnerToAll took 0.00169074 seconds
Rank 18: copyOwnerToAll took 0.00169074 seconds
Rank 19: copyOwnerToAll took 0.00169074 seconds
Rank 20: copyOwnerToAll took 0.00169074 seconds
Rank 21: copyOwnerToAll took 0.00169074 seconds
Rank 22: copyOwnerToAll took 0.00169074 seconds
Rank 23: copyOwnerToAll took 0.00169074 seconds
Rank 24: copyOwnerToAll took 0.00169074 seconds
Rank 25: copyOwnerToAll took 0.00169074 seconds
Rank 26: copyOwnerToAll took 0.00169074 seconds
Rank 27: copyOwnerToAll took 0.00169074 seconds
Rank 28: copyOwnerToAll took 0.00169074 seconds
Rank 29: copyOwnerToAll took 0.00169074 seconds
Rank 30: copyOwnerToAll took 0.00169074 seconds
Rank 31: copyOwnerToAll took 0.00169074 seconds
Average time for copyOwnertoAll is 0.0017472 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	5406	1732	0	3039	0	0	0	0	0	0	2557	0	0	0	0	0	0	0	0	0	1621	3710	0	0	0	0	0	0	613	0	0	
Rank 1's ghost cells:	5423	0	0	0	1783	0	0	0	0	0	0	351	0	0	3626	851	0	0	0	0	0	5586	1960	0	0	3703	0	0	0	406	0	0	
Rank 2's ghost cells:	1721	0	0	0	0	0	0	0	0	0	0	5594	0	0	0	0	0	0	0	0	0	36	2462	6193	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	0	0	0	1425	0	0	4630	2968	661	0	0	257	0	0	2195	0	310	0	0	0	0	0	0	0	0	0	0	0	2800	6011	
Rank 4's ghost cells:	3014	1790	0	0	0	0	0	0	0	0	0	0	0	0	0	517	0	0	92	0	3140	0	0	0	0	5779	0	0	0	5317	0	0	
Rank 5's ghost cells:	0	0	0	1451	0	0	0	0	0	0	0	0	0	0	0	3227	5174	0	6023	0	0	0	0	0	0	630	0	0	0	1071	0	3069	
Rank 6's ghost cells:	0	0	0	0	0	0	0	5282	0	0	0	0	0	0	0	0	0	3719	0	0	0	0	0	0	0	0	4881	1052	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	5318	0	0	0	0	0	0	0	0	0	0	1220	0	0	0	0	0	0	2501	0	0	5804	594	0	0	0	
Rank 8's ghost cells:	0	0	0	4694	0	0	0	0	0	6152	0	0	0	1024	0	0	824	0	0	3261	1758	0	0	0	0	0	0	0	0	0	548	0	
Rank 9's ghost cells:	0	0	0	2967	0	0	0	0	6185	0	25	0	0	4632	0	0	280	0	3351	6	1838	0	0	0	0	0	0	0	0	0	0	3428	
Rank 10's ghost cells:	0	0	0	664	0	0	0	0	0	25	0	0	12	5322	0	0	0	262	0	0	0	0	0	0	3748	0	0	0	664	0	4760	4864	
Rank 11's ghost cells:	2561	358	5598	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6017	0	452	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	12	0	0	602	0	0	0	5868	0	4864	0	0	0	0	416	0	3447	978	5606	0	201	0	
Rank 13's ghost cells:	0	0	0	259	0	0	0	0	1018	4634	5276	0	592	0	0	0	0	3070	0	4582	0	0	0	0	212	0	0	0	30	0	1038	932	
Rank 14's ghost cells:	0	3631	0	0	0	0	0	0	0	0	0	0	0	0	0	3554	0	0	0	0	0	2358	5551	3140	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	867	0	0	517	3236	0	0	0	0	0	0	0	0	3520	0	0	0	12	0	0	0	1276	0	0	5889	0	0	0	4712	0	0	
Rank 16's ghost cells:	0	0	0	2213	0	5113	0	0	835	273	0	0	0	0	0	0	0	0	2063	0	5792	0	0	0	0	36	0	0	0	3563	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	3724	1242	0	0	266	0	5865	3113	0	0	0	0	0	0	0	0	0	0	6200	0	1744	633	93	0	0	0	
Rank 18's ghost cells:	0	0	0	302	87	6044	0	0	0	3333	0	0	0	0	0	12	2062	0	0	0	5348	0	0	0	0	2545	0	0	0	0	0	1329	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	3260	6	0	0	4903	4590	0	0	0	0	0	0	0	0	0	0	0	0	0	0	501	0	5946	0	
Rank 20's ghost cells:	0	0	0	0	3143	0	0	0	1742	1817	0	0	0	0	0	0	5789	0	5411	0	0	0	0	0	0	307	0	0	0	1021	0	0	
Rank 21's ghost cells:	1623	5607	31	0	0	0	0	0	0	0	0	5990	0	0	2378	0	0	0	0	0	0	0	1025	5112	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	3689	1951	2439	0	0	0	0	0	0	0	0	0	0	0	5535	1281	0	0	0	0	0	1017	0	843	0	0	0	0	0	4194	0	0	
Rank 23's ghost cells:	0	0	6202	0	0	0	0	0	0	0	0	434	0	0	3118	0	0	0	0	0	0	5111	840	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	2500	0	0	3798	0	428	218	0	0	0	6235	0	0	0	0	0	0	0	0	0	270	6836	0	0	0	
Rank 25's ghost cells:	0	3770	0	0	5762	638	0	0	0	0	0	0	0	0	0	5895	27	0	2545	0	305	0	0	0	0	0	0	0	0	702	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	4882	0	0	0	0	0	3448	0	0	0	0	1737	0	0	0	0	0	0	0	0	0	5183	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	1010	5803	0	0	0	0	976	0	0	0	0	632	0	0	0	0	0	0	256	0	5182	0	3250	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	588	0	0	664	0	5582	30	0	0	0	93	0	505	0	0	0	0	6863	0	0	3247	0	0	4442	0	
Rank 29's ghost cells:	606	406	0	0	5323	1072	0	0	0	0	0	0	0	0	0	4697	3571	0	0	0	1028	0	4172	0	0	702	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	2802	0	0	0	0	550	0	4759	0	196	1028	0	0	0	0	0	5947	0	0	0	0	0	0	0	0	4477	0	0	228	
Rank 31's ghost cells:	0	0	0	6012	0	3094	0	0	0	3479	4886	0	0	931	0	0	0	0	1314	0	0	0	0	0	0	0	0	0	0	0	225	0	
