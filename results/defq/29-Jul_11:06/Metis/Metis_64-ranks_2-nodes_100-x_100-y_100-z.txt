Number of cores/ranks per node is: 32
METIS partitioner
Cell on rank 0 before loadbalancing: 15636
Cell on rank 1 before loadbalancing: 15627
Cell on rank 2 before loadbalancing: 15628
Cell on rank 3 before loadbalancing: 15625
Cell on rank 4 before loadbalancing: 15652
Cell on rank 5 before loadbalancing: 15618
Cell on rank 6 before loadbalancing: 15622
Cell on rank 7 before loadbalancing: 15617
Cell on rank 8 before loadbalancing: 15622
Cell on rank 9 before loadbalancing: 15629
Cell on rank 10 before loadbalancing: 15621
Cell on rank 11 before loadbalancing: 15621
Cell on rank 12 before loadbalancing: 15631
Cell on rank 13 before loadbalancing: 15626
Cell on rank 14 before loadbalancing: 15634
Cell on rank 15 before loadbalancing: 15616
Cell on rank 16 before loadbalancing: 15628
Cell on rank 17 before loadbalancing: 15621
Cell on rank 18 before loadbalancing: 15624
Cell on rank 19 before loadbalancing: 15621
Cell on rank 20 before loadbalancing: 15620
Cell on rank 21 before loadbalancing: 15616
Cell on rank 22 before loadbalancing: 15623
Cell on rank 23 before loadbalancing: 15616
Cell on rank 24 before loadbalancing: 15626
Cell on rank 25 before loadbalancing: 15639
Cell on rank 26 before loadbalancing: 15608
Cell on rank 27 before loadbalancing: 15622
Cell on rank 28 before loadbalancing: 15625
Cell on rank 29 before loadbalancing: 15620
Cell on rank 30 before loadbalancing: 15623
Cell on rank 31 before loadbalancing: 15625
Cell on rank 32 before loadbalancing: 15617
Cell on rank 33 before loadbalancing: 15634
Cell on rank 34 before loadbalancing: 15625
Cell on rank 35 before loadbalancing: 15623
Cell on rank 36 before loadbalancing: 15623
Cell on rank 37 before loadbalancing: 15642
Cell on rank 38 before loadbalancing: 15621
Cell on rank 39 before loadbalancing: 15617
Cell on rank 40 before loadbalancing: 15620
Cell on rank 41 before loadbalancing: 15623
Cell on rank 42 before loadbalancing: 15605
Cell on rank 43 before loadbalancing: 15646
Cell on rank 44 before loadbalancing: 15638
Cell on rank 45 before loadbalancing: 15617
Cell on rank 46 before loadbalancing: 15617
Cell on rank 47 before loadbalancing: 15632
Cell on rank 48 before loadbalancing: 15625
Cell on rank 49 before loadbalancing: 15630
Cell on rank 50 before loadbalancing: 15623
Cell on rank 51 before loadbalancing: 15626
Cell on rank 52 before loadbalancing: 15626
Cell on rank 53 before loadbalancing: 15616
Cell on rank 54 before loadbalancing: 15635
Cell on rank 55 before loadbalancing: 15621
Cell on rank 56 before loadbalancing: 15625
Cell on rank 57 before loadbalancing: 15624
Cell on rank 58 before loadbalancing: 15630
Cell on rank 59 before loadbalancing: 15629
Cell on rank 60 before loadbalancing: 15636
Cell on rank 61 before loadbalancing: 15621
Cell on rank 62 before loadbalancing: 15627
Cell on rank 63 before loadbalancing: 15624
Edge-cut: 128842


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	1133	291	273	0	135	0	1036	0	310	0	0	0	86	0	654	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	1128	0	276	0	0	0	0	49	0	0	0	0	0	688	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	287	274	0	1050	0	0	0	0	0	0	0	0	646	97	0	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	272	0	1034	0	445	442	0	87	0	0	0	0	0	0	0	298	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	445	0	795	318	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	649	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	136	0	0	462	799	0	296	308	0	25	926	0	0	0	0	273	0	0	0	0	0	0	0	0	0	0	373	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	319	308	0	1016	0	666	217	0	0	0	0	0	110	0	23	0	0	0	0	0	0	915	221	267	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	1024	49	0	88	297	300	1019	0	0	546	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	1098	216	154	0	0	545	60	43	0	650	0	0	0	0	0	0	0	0	0	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1074	0	0	0	0	166	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	288	0	0	0	0	25	626	552	1098	0	171	0	0	0	14	451	36	0	432	0	0	0	0	0	0	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	920	213	36	225	172	0	935	0	0	190	644	475	0	0	0	0	0	0	0	0	0	89	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	156	0	937	0	0	0	471	0	281	462	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	287	0	0	0	0	0	0	0	0	0	0	0	0	0	0	318	399	201	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	646	0	0	0	0	0	0	0	0	0	0	323	444	498	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	491	248	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	75	688	112	0	0	0	0	0	0	0	0	0	326	0	425	649	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	584	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	545	14	182	471	459	429	0	1051	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	218	0	293	721	0	382	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	652	53	175	298	0	258	0	0	60	453	644	0	498	647	1050	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	93	0	42	35	477	266	0	0	0	0	0	892	224	144	0	626	0	0	93	125	872	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	462	0	0	0	0	891	0	126	180	476	473	0	30	0	0	0	0	0	0	0	0	218	0	653	0	0	0	0	0	179	0	71	528	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	20	0	641	430	0	29	0	0	0	0	218	131	0	1056	0	0	0	0	0	574	0	0	0	0	0	0	233	524	15	0	0	0	0	0	0	0	0	0	0	0	0	0	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	156	177	1056	0	0	163	607	439	532	148	0	0	0	0	0	0	387	0	0	0	0	0	0	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	483	0	0	0	1073	93	321	0	0	0	0	217	0	121	0	0	0	0	0	0	0	0	0	453	0	519	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	626	475	0	164	1118	0	203	29	122	0	127	0	140	123	791	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	610	92	199	0	1054	53	0	0	0	454	641	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	439	319	34	1053	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	485	699	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	99	0	0	532	0	131	53	0	0	997	181	362	0	1054	186	19	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	919	0	0	162	0	0	0	0	0	0	125	0	574	166	0	0	0	0	1001	0	154	286	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	135	372	222	0	0	0	84	0	0	0	0	0	863	0	0	0	0	124	0	0	171	154	0	823	0	0	574	95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	643	0	267	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	361	298	823	0	0	0	135	577	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	133	455	0	0	0	0	0	0	1034	449	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	663	0	1054	0	0	0	1036	0	207	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	786	0	0	175	0	574	135	446	214	0	980	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	15	0	99	577	280	122	978	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	219	235	392	0	0	0	0	0	0	0	0	0	0	0	0	0	1031	218	216	0	454	0	85	898	8	0	77	0	222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	503	0	0	0	0	0	0	0	0	0	0	0	0	0	1031	0	178	79	0	50	0	550	0	0	0	0	0	0	0	0	598	557	57	0	0	0	0	0	13	0	189	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	302	0	0	0	0	0	651	19	0	0	0	0	0	0	0	0	0	0	0	0	0	222	177	0	823	0	0	0	0	0	0	0	625	0	0	0	0	0	147	715	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	223	80	819	0	0	227	966	364	0	0	0	630	151	214	0	0	0	0	419	0	0	0	0	0	95	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	777	0	468	0	0	0	0	0	69	436	154	0	0	0	0	0	0	0	0	116	0	0	419	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	457	34	0	224	781	0	223	1033	0	0	0	0	214	670	266	8	0	0	0	0	0	0	0	0	0	0	78	17	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	965	0	226	0	316	0	0	0	20	585	0	0	0	0	0	71	0	0	0	0	0	43	436	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	551	0	362	468	1041	324	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	708	0	405	27	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	0	175	449	0	0	489	0	0	0	0	0	0	0	0	910	0	0	0	0	0	0	0	0	910	305	278	0	539	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	0	0	0	0	0	0	0	0	8	0	0	0	0	0	0	0	921	0	85	0	0	504	0	688	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	0	0	519	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	295	85	0	998	420	270	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	538	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	0	625	636	0	0	20	0	281	0	1004	0	288	174	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	199	575	0	0	0	403	297	0	440	273	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	0	210	69	660	0	0	539	477	282	172	443	0	259	923	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	446	258	0	0	0	0	0	0	279	259	0	1075	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	9	0	0	0	690	0	0	86	922	1100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	599	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	1034	190	316	0	0	231	540	193	56	983	0	47	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	1078	0	0	318	0	0	208	0	0	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	566	147	0	0	0	0	0	0	0	0	0	0	0	0	0	1037	0	191	234	154	0	375	5	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	399	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	707	425	0	0	87	0	0	0	0	0	0	0	0	0	198	179	0	803	0	0	0	0	224	579	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	207	0	0	293	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	309	239	803	0	535	422	0	185	0	410	70	0	9	0	321	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	495	250	721	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	0	555	0	1005	365	285	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	248	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	422	985	0	0	250	0	0	0	0	0	0	400	534	
From rank 54 to: 	0	0	0	0	0	0	0	0	163	0	0	0	0	586	366	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	216	375	0	0	363	0	0	923	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	540	5	0	185	276	248	923	0	0	0	45	0	1009	0	85	103	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	12	0	94	114	0	45	706	0	0	0	0	0	0	0	0	189	0	235	0	0	0	0	0	0	615	408	443	18	158	225	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	442	0	0	0	0	0	0	0	0	0	62	0	584	404	0	0	0	0	621	0	53	0	0	0	774	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	199	0	0	0	78	0	407	0	0	0	0	0	0	0	0	1002	0	0	64	0	0	0	57	419	53	0	839	656	0	42	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	419	17	0	26	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	441	0	840	0	226	603	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	0	0	10	0	0	0	1019	18	0	647	198	0	941	131	188	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	158	0	0	600	942	0	195	254	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	332	0	400	0	85	235	769	38	0	131	192	0	1069	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	535	0	103	0	0	0	0	191	261	1069	0	


After loadbalancing process 1 has 17825 cells.
After loadbalancing process 30 has 19054 cells.
After loadbalancing process 61 has 17770 cells.
After loadbalancing process 27 has 18726 cells.
After loadbalancing process 31 has 17696 cells.
After loadbalancing process 63 has 17783 cells.
After loadbalancing process 28 has 18201 cells.
After loadbalancing process 58 has 19446 cells.
After loadbalancing process 22 has 18726 cells.
After loadbalancing process 26 has 19225 cells.
After loadbalancing process 36 has 18062 cells.
After loadbalancing process 60 has 18836 cells.
After loadbalancing process 3 has 18203 cells.
After loadbalancing process 5 has 19216 cells.
After loadbalancing process 57 has 18564 cells.
After loadbalancing process 4 has 18277 cells.
After loadbalancing process 21 has 19534 cells.
After loadbalancing process 12 has 18281 cells.
After loadbalancing process 44 has 18056 cells.
After loadbalancing process 0 has 19554 cells.
After loadbalancing process 25 has 19026 cells.
After loadbalancing process 55 has 19040 cells.
After loadbalancing process 7 has 18976 cells.
After loadbalancing process 47 has 18593 cells.
After loadbalancing process 41 has 18529 cells.
After loadbalancing process 15 has 20404 cells.
After loadbalancing process 24 has 19240 cells.
After loadbalancing process 16 has 19517 cells.
After loadbalancing process 32 has 19672 cells.
After loadbalancing process 43 has 19286 cells.
After loadbalancing process 48 has 19815 cells.
After loadbalancing process 38 has 18283 cells.
After loadbalancing process 23 has 18675 cells.
After loadbalancing process 29 has 18820 cells.
After loadbalancing process 50 has 19285 cells.
After loadbalancing process 8 has 19699 cells.
After loadbalancing process 51 has 19429 cells.
After loadbalancing process 20 has 18900 cells.
After loadbalancing process 62 has 18878 cells.
After loadbalancing process 14 has 20399 cells.
After loadbalancing process 46 has 17934 cells.
After loadbalancing process 6 has 19684 cells.
After loadbalancing process 18 has 19587 cells.
After loadbalancing process 13 has 18735 cells.
After loadbalancing process 39 has 19593 cells.
After loadbalancing process 35 has 19811 cells.
After loadbalancing process 42 has 18273 cells.
After loadbalancing process 52 has 19456 cells.
After loadbalancing process 53 has 18455 cells.
After loadbalancing process 37 has 19647 cells.
After loadbalancing process 54 has 18627 cells.
After loadbalancing process 45 has 19881 cells.
After loadbalancing process 34 has 19306 cells.
After loadbalancing process 56 has 18887 cells.
After loadbalancing process 49 has 20035 cells.
After loadbalancing process 9 has 19483 cells.
After loadbalancing process 33 has 19510 cells.
After loadbalancing process 17 has 19908 cells.
After loadbalancing process 10 has 19520 cells.
After loadbalancing process 59 has 18201 cells.
After loadbalancing process 40 has 19862 cells.
After loadbalancing process 19 has 19461 cells.
After loadbalancing process 11 has 19162 cells.


After loadbalancing process 2 has 18143 cells.
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          57.4678         0.230055
    2          33.8481         0.588993
    3          24.0533         0.710624
    4           20.864         0.867407
    5          18.0801          0.86657
    6          14.5942         0.807199
    7          13.1108         0.898354
    8          12.3341         0.940755
    9             10.2         0.826982
   10          9.34712         0.916381
   11          9.29555         0.994483
   12          8.13929         0.875611
   13           7.2549         0.891344
   14          7.27045          1.00214
   15          6.59951         0.907717
   16          5.97233         0.904965
   17          5.90669         0.989009
   18          5.55919         0.941168
   19          5.06618         0.911316
   20          4.98269         0.983521
   21           4.6328         0.929778
   22          4.27497         0.922762
   23          4.26259         0.997104
   24          3.98175         0.934114
   25          3.71535         0.933094
   26          3.68011         0.990516
   27          3.44198         0.935293
   28          3.23369         0.939485
   29          3.18821         0.985935
   30          3.02435         0.948605
   31          2.91155         0.962704
   32          2.92113          1.00329
   33          2.89205         0.990044
   34          2.99291          1.03488
   35          3.19808          1.06855
   36          3.12346         0.976667
   37          2.79384          0.89447
   38          2.26133         0.809396
   39          1.70186         0.752592
   40          1.40185         0.823719
   41          1.25903         0.898119
   42          1.13886         0.904559
   43          1.02724         0.901989
   44         0.884529         0.861071
   45         0.759198         0.858307
   46         0.678476         0.893675
   47         0.580797         0.856031
   48         0.485717         0.836295
   49         0.415695         0.855837
   50         0.347103         0.834996
   51          0.29285         0.843696
   52         0.245976          0.83994
   53         0.205776         0.836569
   54          0.17686         0.859476
   55          0.15098          0.85367
   56         0.126308         0.836592
   57         0.105864          0.83814
   58        0.0868647          0.82053
   59        0.0717826         0.826372
   60        0.0593292         0.826512
   61        0.0478796         0.807016
   62        0.0389776         0.814075
   63        0.0316854         0.812914
   64        0.0247658         0.781616
=== rate=0.865848, T=3.0007, TIT=0.046886, IT=64

 Elapsed time: 3.0007
Rank 0: Matrix-vector product took 0.0158768 seconds
Rank 1: Matrix-vector product took 0.014238 seconds
Rank 2: Matrix-vector product took 0.0144714 seconds
Rank 3: Matrix-vector product took 0.0145436 seconds
Rank 4: Matrix-vector product took 0.014718 seconds
Rank 5: Matrix-vector product took 0.0154556 seconds
Rank 6: Matrix-vector product took 0.0156914 seconds
Rank 7: Matrix-vector product took 0.0151196 seconds
Rank 8: Matrix-vector product took 0.0158847 seconds
Rank 9: Matrix-vector product took 0.0155089 seconds
Rank 10: Matrix-vector product took 0.0156449 seconds
Rank 11: Matrix-vector product took 0.0156281 seconds
Rank 12: Matrix-vector product took 0.0145728 seconds
Rank 13: Matrix-vector product took 0.014949 seconds
Rank 14: Matrix-vector product took 0.0162089 seconds
Rank 15: Matrix-vector product took 0.0161294 seconds
Rank 16: Matrix-vector product took 0.0158586 seconds
Rank 17: Matrix-vector product took 0.0161877 seconds
Rank 18: Matrix-vector product took 0.0155698 seconds
Rank 19: Matrix-vector product took 0.0155567 seconds
Rank 20: Matrix-vector product took 0.0150529 seconds
Rank 21: Matrix-vector product took 0.0156876 seconds
Rank 22: Matrix-vector product took 0.0151899 seconds
Rank 23: Matrix-vector product took 0.0149905 seconds
Rank 24: Matrix-vector product took 0.0153847 seconds
Rank 25: Matrix-vector product took 0.015261 seconds
Rank 26: Matrix-vector product took 0.0153514 seconds
Rank 27: Matrix-vector product took 0.0149706 seconds
Rank 28: Matrix-vector product took 0.0144875 seconds
Rank 29: Matrix-vector product took 0.0152159 seconds
Rank 30: Matrix-vector product took 0.0152399 seconds
Rank 31: Matrix-vector product took 0.0140892 seconds
Rank 32: Matrix-vector product took 0.0157273 seconds
Rank 33: Matrix-vector product took 0.0157417 seconds
Rank 34: Matrix-vector product took 0.0155448 seconds
Rank 35: Matrix-vector product took 0.0159468 seconds
Rank 36: Matrix-vector product took 0.0146722 seconds
Rank 37: Matrix-vector product took 0.0155677 seconds
Rank 38: Matrix-vector product took 0.014791 seconds
Rank 39: Matrix-vector product took 0.0157004 seconds
Rank 40: Matrix-vector product took 0.0159488 seconds
Rank 41: Matrix-vector product took 0.0147452 seconds
Rank 42: Matrix-vector product took 0.0147625 seconds
Rank 43: Matrix-vector product took 0.0154573 seconds
Rank 44: Matrix-vector product took 0.0145298 seconds
Rank 45: Matrix-vector product took 0.0159226 seconds
Rank 46: Matrix-vector product took 0.0144051 seconds
Rank 47: Matrix-vector product took 0.0148623 seconds
Rank 48: Matrix-vector product took 0.0160485 seconds
Rank 49: Matrix-vector product took 0.0161157 seconds
Rank 50: Matrix-vector product took 0.0154246 seconds
Rank 51: Matrix-vector product took 0.0155923 seconds
Rank 52: Matrix-vector product took 0.0154568 seconds
Rank 53: Matrix-vector product took 0.0147904 seconds
Rank 54: Matrix-vector product took 0.0150628 seconds
Rank 55: Matrix-vector product took 0.0153183 seconds
Rank 56: Matrix-vector product took 0.0153505 seconds
Rank 57: Matrix-vector product took 0.0151431 seconds
Rank 58: Matrix-vector product took 0.0155143 seconds
Rank 59: Matrix-vector product took 0.0148709 seconds
Rank 60: Matrix-vector product took 0.0154461 seconds
Rank 61: Matrix-vector product took 0.0145004 seconds
Rank 62: Matrix-vector product took 0.0155421 seconds
Rank 63: Matrix-vector product took 0.0145011 seconds
Average time for Matrix-vector product is 0.0152772 seconds

Rank 0: copyOwnerToAll took 0.00048542 seconds
Rank 1: copyOwnerToAll took 0.00048542 seconds
Rank 2: copyOwnerToAll took 0.00048542 seconds
Rank 3: copyOwnerToAll took 0.00048542 seconds
Rank 4: copyOwnerToAll took 0.00048542 seconds
Rank 5: copyOwnerToAll took 0.00048542 seconds
Rank 6: copyOwnerToAll took 0.00048542 seconds
Rank 7: copyOwnerToAll took 0.00048542 seconds
Rank 8: copyOwnerToAll took 0.00048542 seconds
Rank 9: copyOwnerToAll took 0.00048542 seconds
Rank 10: copyOwnerToAll took 0.00048542 seconds
Rank 11: copyOwnerToAll took 0.00048542 seconds
Rank 12: copyOwnerToAll took 0.00048542 seconds
Rank 13: copyOwnerToAll took 0.00048542 seconds
Rank 14: copyOwnerToAll took 0.00048542 seconds
Rank 15: copyOwnerToAll took 0.00048542 seconds
Rank 16: copyOwnerToAll took 0.00048542 seconds
Rank 17: copyOwnerToAll took 0.00048542 seconds
Rank 18: copyOwnerToAll took 0.00048542 seconds
Rank 19: copyOwnerToAll took 0.00048542 seconds
Rank 20: copyOwnerToAll took 0.00048542 seconds
Rank 21: copyOwnerToAll took 0.00048542 seconds
Rank 22: copyOwnerToAll took 0.00048542 seconds
Rank 23: copyOwnerToAll took 0.00048542 seconds
Rank 24: copyOwnerToAll took 0.00048542 seconds
Rank 25: copyOwnerToAll took 0.00048542 seconds
Rank 26: copyOwnerToAll took 0.00048542 seconds
Rank 27: copyOwnerToAll took 0.00048542 seconds
Rank 28: copyOwnerToAll took 0.00048542 seconds
Rank 29: copyOwnerToAll took 0.00048542 seconds
Rank 30: copyOwnerToAll took 0.00048542 seconds
Rank 31: copyOwnerToAll took 0.00048542 seconds
Rank 32: copyOwnerToAll took 0.00048542 seconds
Rank 33: copyOwnerToAll took 0.00048542 seconds
Rank 34: copyOwnerToAll took 0.00048542 seconds
Rank 35: copyOwnerToAll took 0.00048542 seconds
Rank 36: copyOwnerToAll took 0.00048542 seconds
Rank 37: copyOwnerToAll took 0.00048542 seconds
Rank 38: copyOwnerToAll took 0.00048542 seconds
Rank 39: copyOwnerToAll took 0.00048542 seconds
Rank 40: copyOwnerToAll took 0.00048542 seconds
Rank 41: copyOwnerToAll took 0.00048542 seconds
Rank 42: copyOwnerToAll took 0.00048542 seconds
Rank 43: copyOwnerToAll took 0.00048542 seconds
Rank 44: copyOwnerToAll took 0.00048542 seconds
Rank 45: copyOwnerToAll took 0.00048542 seconds
Rank 46: copyOwnerToAll took 0.00048542 seconds
Rank 47: copyOwnerToAll took 0.00048542 seconds
Rank 48: copyOwnerToAll took 0.00048542 seconds
Rank 49: copyOwnerToAll took 0.00048542 seconds
Rank 50: copyOwnerToAll took 0.00048542 seconds
Rank 51: copyOwnerToAll took 0.00048542 seconds
Rank 52: copyOwnerToAll took 0.00048542 seconds
Rank 53: copyOwnerToAll took 0.00048542 seconds
Rank 54: copyOwnerToAll took 0.00048542 seconds
Rank 55: copyOwnerToAll took 0.00048542 seconds
Rank 56: copyOwnerToAll took 0.00048542 seconds
Rank 57: copyOwnerToAll took 0.00048542 seconds
Rank 58: copyOwnerToAll took 0.00048542 seconds
Rank 59: copyOwnerToAll took 0.00048542 seconds
Rank 60: copyOwnerToAll took 0.00048542 seconds
Rank 61: copyOwnerToAll took 0.00048542 seconds
Rank 62: copyOwnerToAll took 0.00048542 seconds
Rank 63: copyOwnerToAll took 0.00048542 seconds
Average time for copyOwnertoAll is 0.000524901 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	1133	291	273	0	135	0	1036	0	310	0	0	0	86	0	654	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	1128	0	276	0	0	0	0	49	0	0	0	0	0	688	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	287	274	0	1050	0	0	0	0	0	0	0	0	646	97	0	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	272	0	1034	0	445	442	0	87	0	0	0	0	0	0	0	298	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	445	0	795	318	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	649	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	136	0	0	462	799	0	296	308	0	25	926	0	0	0	0	273	0	0	0	0	0	0	0	0	0	0	373	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	319	308	0	1016	0	666	217	0	0	0	0	0	110	0	23	0	0	0	0	0	0	915	221	267	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	1024	49	0	88	297	300	1019	0	0	546	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1098	216	154	0	0	545	60	43	0	650	0	0	0	0	0	0	0	0	0	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1074	0	0	0	0	166	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	288	0	0	0	0	25	626	552	1098	0	171	0	0	0	14	451	36	0	432	0	0	0	0	0	0	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	920	213	36	225	172	0	935	0	0	190	644	475	0	0	0	0	0	0	0	0	0	89	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	156	0	937	0	0	0	471	0	281	462	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	287	0	0	0	0	0	0	0	0	0	0	0	0	0	0	318	399	201	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	646	0	0	0	0	0	0	0	0	0	0	323	444	498	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	491	248	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	75	688	112	0	0	0	0	0	0	0	0	0	326	0	425	649	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	584	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	545	14	182	471	459	429	0	1051	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	218	0	293	721	0	382	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	652	53	175	298	0	258	0	0	60	453	644	0	498	647	1050	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	93	0	42	35	477	266	0	0	0	0	0	892	224	144	0	626	0	0	93	125	872	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	462	0	0	0	0	891	0	126	180	476	473	0	30	0	0	0	0	0	0	0	0	218	0	653	0	0	0	0	0	179	0	71	528	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	20	0	641	430	0	29	0	0	0	0	218	131	0	1056	0	0	0	0	0	574	0	0	0	0	0	0	233	524	15	0	0	0	0	0	0	0	0	0	0	0	0	0	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	156	177	1056	0	0	163	607	439	532	148	0	0	0	0	0	0	387	0	0	0	0	0	0	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	483	0	0	0	1073	93	321	0	0	0	0	217	0	121	0	0	0	0	0	0	0	0	0	453	0	519	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	626	475	0	164	1118	0	203	29	122	0	127	0	140	123	791	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	610	92	199	0	1054	53	0	0	0	454	641	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	439	319	34	1053	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	485	699	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	99	0	0	532	0	131	53	0	0	997	181	362	0	1054	186	19	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	919	0	0	162	0	0	0	0	0	0	125	0	574	166	0	0	0	0	1001	0	154	286	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	135	372	222	0	0	0	84	0	0	0	0	0	863	0	0	0	0	124	0	0	171	154	0	823	0	0	574	95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	643	0	267	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	361	298	823	0	0	0	135	577	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	133	455	0	0	0	0	0	0	1034	449	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	663	0	1054	0	0	0	1036	0	207	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	786	0	0	175	0	574	135	446	214	0	980	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	15	0	99	577	280	122	978	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	219	235	392	0	0	0	0	0	0	0	0	0	0	0	0	0	1031	218	216	0	454	0	85	898	8	0	77	0	222	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	503	0	0	0	0	0	0	0	0	0	0	0	0	0	1031	0	178	79	0	50	0	550	0	0	0	0	0	0	0	0	598	557	57	0	0	0	0	0	13	0	189	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	302	0	0	0	0	0	651	19	0	0	0	0	0	0	0	0	0	0	0	0	0	222	177	0	823	0	0	0	0	0	0	0	625	0	0	0	0	0	147	715	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	223	80	819	0	0	227	966	364	0	0	0	630	151	214	0	0	0	0	419	0	0	0	0	0	95	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	777	0	468	0	0	0	0	0	69	436	154	0	0	0	0	0	0	0	0	116	0	0	419	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	457	34	0	224	781	0	223	1033	0	0	0	0	214	670	266	8	0	0	0	0	0	0	0	0	0	0	78	17	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	965	0	226	0	316	0	0	0	20	585	0	0	0	0	0	71	0	0	0	0	0	43	436	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	551	0	362	468	1041	324	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	708	0	405	27	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	0	175	449	0	0	489	0	0	0	0	0	0	0	0	910	0	0	0	0	0	0	0	0	910	305	278	0	539	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	0	0	0	0	0	0	0	0	8	0	0	0	0	0	0	0	921	0	85	0	0	504	0	688	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	0	0	519	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	295	85	0	998	420	270	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	538	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	0	625	636	0	0	20	0	281	0	1004	0	288	174	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	199	575	0	0	0	403	297	0	440	273	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	0	210	69	660	0	0	539	477	282	172	443	0	259	923	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	446	258	0	0	0	0	0	0	279	259	0	1075	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	9	0	0	0	690	0	0	86	922	1100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	599	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	1034	190	316	0	0	231	540	193	56	983	0	47	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	1078	0	0	318	0	0	208	0	0	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	566	147	0	0	0	0	0	0	0	0	0	0	0	0	0	1037	0	191	234	154	0	375	5	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	399	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	707	425	0	0	87	0	0	0	0	0	0	0	0	0	198	179	0	803	0	0	0	0	224	579	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	207	0	0	293	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	309	239	803	0	535	422	0	185	0	410	70	0	9	0	321	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	495	250	721	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	154	0	555	0	1005	365	285	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	248	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	422	985	0	0	250	0	0	0	0	0	0	400	534	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	163	0	0	0	0	586	366	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	216	375	0	0	363	0	0	923	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	540	5	0	185	276	248	923	0	0	0	45	0	1009	0	85	103	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	12	0	94	114	0	45	706	0	0	0	0	0	0	0	0	189	0	235	0	0	0	0	0	0	615	408	443	18	158	225	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	442	0	0	0	0	0	0	0	0	0	62	0	584	404	0	0	0	0	621	0	53	0	0	0	774	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	199	0	0	0	78	0	407	0	0	0	0	0	0	0	0	1002	0	0	64	0	0	0	57	419	53	0	839	656	0	42	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	419	17	0	26	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	441	0	840	0	226	603	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	0	0	10	0	0	0	1019	18	0	647	198	0	941	131	188	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	158	0	0	600	942	0	195	254	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	332	0	400	0	85	235	769	38	0	131	192	0	1069	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	535	0	103	0	0	0	0	191	261	1069	0	
