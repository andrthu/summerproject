Number of cores/ranks per node is: 8
METIS partitioner
Cell on rank 0 before loadbalancing: 62498
Cell on rank 1 before loadbalancing: 62499
Cell on rank 2 before loadbalancing: 62494
Cell on rank 3 before loadbalancing: 62527
Cell on rank 4 before loadbalancing: 62496
Cell on rank 5 before loadbalancing: 62500
Cell on rank 6 before loadbalancing: 62470
Cell on rank 7 before loadbalancing: 62500
Cell on rank 8 before loadbalancing: 62498
Cell on rank 9 before loadbalancing: 62503
Cell on rank 10 before loadbalancing: 62502
Cell on rank 11 before loadbalancing: 62491
Cell on rank 12 before loadbalancing: 62508
Cell on rank 13 before loadbalancing: 62511
Cell on rank 14 before loadbalancing: 62499
Cell on rank 15 before loadbalancing: 62504
Edge-cut: 71321


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2511	162	2210	0	470	2571	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2509	0	2101	431	0	2044	8	0	187	0	0	0	0	0	241	2345	
From rank 2 to: 	168	2098	0	2610	0	0	0	0	0	0	0	0	0	0	2548	0	
From rank 3 to: 	2212	403	2597	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	2499	139	2419	0	0	2289	0	0	0	0	0	
From rank 5 to: 	404	2036	0	0	2497	0	1977	206	1693	0	960	0	0	0	0	267	
From rank 6 to: 	2595	8	0	0	136	1968	0	2498	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	2419	227	2493	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	190	0	0	0	1677	0	0	0	2493	2173	711	0	600	0	2489	
From rank 9 to: 	0	0	0	0	0	0	0	0	2492	0	0	2215	0	1940	0	0	
From rank 10 to: 	0	0	0	0	2279	974	0	0	2207	0	0	2751	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	711	2178	2752	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	2803	2062	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	599	1922	0	0	2802	0	508	2307	
From rank 14 to: 	0	275	2549	0	0	0	0	0	0	0	0	0	2032	507	0	2387	
From rank 15 to: 	0	2345	0	0	0	267	0	0	2509	0	0	0	0	2354	2390	0	


After loadbalancing process 11 has 68132 cells.
After loadbalancing process 12 has 67373 cells.
After loadbalancing process 9 has 69150 cells.
After loadbalancing process 7 has 67639 cells.
After loadbalancing process 13 has 70649 cells.
After loadbalancing process 3 has 67739 cells.
After loadbalancing process 10 has 70713 cells.
After loadbalancing process 6 has 69675 cells.
After loadbalancing process 14 has 70249 cells.
After loadbalancing process 8 has 72831 cells.
After loadbalancing process 15 has 72369 cells.
After loadbalancing process 0 has 70422 cells.
After loadbalancing process 4 has 69842 cells.
After loadbalancing process 5 has 72540 cells.
After loadbalancing process 2 has 69918 cells.
After loadbalancing process 1 has 72365 cells.


=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.9896         0.220135
    2          31.9607         0.581214
    3          22.4857          0.70354
    4          18.8167         0.836831
    5          17.1091         0.909252
    6           14.198          0.82985
    7           11.938         0.840823
    8          11.4967         0.963031
    9          10.0593         0.874973
   10          8.53459         0.848429
   11          8.41532         0.986024
   12          7.99809         0.950421
   13          6.77377         0.846922
   14          6.61804          0.97701
   15          6.50687         0.983202
   16          5.63867         0.866571
   17          5.39622         0.957003
   18          5.35399         0.992174
   19          4.82933         0.902006
   20          4.59436         0.951345
   21          4.51689         0.983137
   22          4.08689         0.904802
   23          3.96055         0.969087
   24          3.90259         0.985367
   25          3.52882         0.904226
   26          3.35558         0.950905
   27          3.33242         0.993098
   28          3.02361         0.907332
   29          2.89799         0.958452
   30          2.90519          1.00249
   31          2.76315         0.951108
   32          2.82187          1.02125
   33          3.02703          1.07271
   34          3.09639          1.02291
   35          3.07939          0.99451
   36          2.68099         0.870622
   37          1.99557          0.74434
   38          1.49918         0.751253
   39          1.20796         0.805747
   40          1.05613         0.874316
   41          1.01075         0.957031
   42         0.891047         0.881567
   43         0.735957         0.825946
   44         0.653159         0.887496
   45         0.563362         0.862519
   46         0.450467         0.799605
   47         0.385121         0.854938
   48          0.32771         0.850928
   49         0.260143         0.793822
   50         0.216764          0.83325
   51         0.184841         0.852728
   52         0.156752         0.848036
   53         0.136715         0.872172
   54         0.112023         0.819391
   55        0.0930448         0.830589
   56         0.079163         0.850805
   57        0.0616344         0.778576
   58        0.0499448         0.810339
   59        0.0405088         0.811071
   60        0.0316242         0.780676
   61        0.0247854         0.783749
=== rate=0.859747, T=9.92647, TIT=0.162729, IT=61

 Elapsed time: 9.92647
Rank 0: Matrix-vector product took 0.0572857 seconds
Rank 1: Matrix-vector product took 0.0580958 seconds
Rank 2: Matrix-vector product took 0.0564232 seconds
Rank 3: Matrix-vector product took 0.0548431 seconds
Rank 4: Matrix-vector product took 0.0581328 seconds
Rank 5: Matrix-vector product took 0.058578 seconds
Rank 6: Matrix-vector product took 0.0562487 seconds
Rank 7: Matrix-vector product took 0.0545109 seconds
Rank 8: Matrix-vector product took 0.0596616 seconds
Rank 9: Matrix-vector product took 0.0572363 seconds
Rank 10: Matrix-vector product took 0.0581714 seconds
Rank 11: Matrix-vector product took 0.0552381 seconds
Rank 12: Matrix-vector product took 0.0551249 seconds
Rank 13: Matrix-vector product took 0.0571083 seconds
Rank 14: Matrix-vector product took 0.0578012 seconds
Rank 15: Matrix-vector product took 0.059521 seconds
Average time for Matrix-vector product is 0.0571238 seconds

Rank 0: copyOwnerToAll took 0.000735351 seconds
Rank 1: copyOwnerToAll took 0.000735351 seconds
Rank 2: copyOwnerToAll took 0.000735351 seconds
Rank 3: copyOwnerToAll took 0.000735351 seconds
Rank 4: copyOwnerToAll took 0.000735351 seconds
Rank 5: copyOwnerToAll took 0.000735351 seconds
Rank 6: copyOwnerToAll took 0.000735351 seconds
Rank 7: copyOwnerToAll took 0.000735351 seconds
Rank 8: copyOwnerToAll took 0.000735351 seconds
Rank 9: copyOwnerToAll took 0.000735351 seconds
Rank 10: copyOwnerToAll took 0.000735351 seconds
Rank 11: copyOwnerToAll took 0.000735351 seconds
Rank 12: copyOwnerToAll took 0.000735351 seconds
Rank 13: copyOwnerToAll took 0.000735351 seconds
Rank 14: copyOwnerToAll took 0.000735351 seconds
Rank 15: copyOwnerToAll took 0.000735351 seconds
Average time for copyOwnertoAll is 0.000753417 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2511	162	2210	0	470	2571	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2509	0	2101	431	0	2044	8	0	187	0	0	0	0	0	241	2345	
Rank 2's ghost cells:	168	2098	0	2610	0	0	0	0	0	0	0	0	0	0	2548	0	
Rank 3's ghost cells:	2212	403	2597	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	2499	139	2419	0	0	2289	0	0	0	0	0	
Rank 5's ghost cells:	404	2036	0	0	2497	0	1977	206	1693	0	960	0	0	0	0	267	
Rank 6's ghost cells:	2595	8	0	0	136	1968	0	2498	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	2419	227	2493	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	190	0	0	0	1677	0	0	0	2493	2173	711	0	600	0	2489	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	2492	0	0	2215	0	1940	0	0	
Rank 10's ghost cells:	0	0	0	0	2279	974	0	0	2207	0	0	2751	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	711	2178	2752	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	2803	2062	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	599	1922	0	0	2802	0	508	2307	
Rank 14's ghost cells:	0	275	2549	0	0	0	0	0	0	0	0	0	2032	507	0	2387	
Rank 15's ghost cells:	0	2345	0	0	0	267	0	0	2509	0	0	0	0	2354	2390	0	
