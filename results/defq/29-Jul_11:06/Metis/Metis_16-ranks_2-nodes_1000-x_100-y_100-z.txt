Number of cores/ranks per node is: 8
METIS partitioner
Cell on rank 0 before loadbalancing: 625501
Cell on rank 1 before loadbalancing: 625479
Cell on rank 2 before loadbalancing: 625473
Cell on rank 3 before loadbalancing: 625475
Cell on rank 4 before loadbalancing: 625469
Cell on rank 5 before loadbalancing: 625461
Cell on rank 6 before loadbalancing: 631280
Cell on rank 7 before loadbalancing: 625469
Cell on rank 8 before loadbalancing: 625476
Cell on rank 9 before loadbalancing: 625481
Cell on rank 10 before loadbalancing: 625467
Cell on rank 11 before loadbalancing: 618782
Cell on rank 12 before loadbalancing: 625465
Cell on rank 13 before loadbalancing: 618781
Cell on rank 14 before loadbalancing: 625470
Cell on rank 15 before loadbalancing: 625471
Edge-cut: 313926


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	12788	1518	12408	73	0	3021	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	12787	0	11385	380	0	0	1536	3652	0	0	0	0	0	0	0	0	
From rank 2 to: 	1488	11386	0	12897	1239	4422	332	480	0	0	0	0	0	0	0	0	
From rank 3 to: 	12414	440	12878	0	3152	0	263	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	73	0	1238	3144	0	11775	10766	144	0	0	0	0	0	0	3636	0	
From rank 5 to: 	0	0	4479	0	11792	0	2200	11169	0	0	0	0	505	0	1304	2233	
From rank 6 to: 	2962	1517	340	264	10735	2183	0	12342	0	0	0	0	92	3233	1479	0	
From rank 7 to: 	0	3650	481	0	144	11226	12333	0	0	0	0	0	4965	187	208	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	12666	11366	146	0	567	3101	1762	
From rank 9 to: 	0	0	0	0	0	0	0	0	12700	0	3012	11440	0	0	0	1974	
From rank 10 to: 	0	0	0	0	0	0	0	0	11224	3012	0	13601	1014	4203	370	447	
From rank 11 to: 	0	0	0	0	0	0	0	0	146	11578	13590	0	3393	0	0	1348	
From rank 12 to: 	0	0	0	0	0	514	91	4934	0	0	1022	3345	0	11368	1445	10516	
From rank 13 to: 	0	0	0	0	0	0	3255	182	571	0	4230	0	11374	0	10769	1980	
From rank 14 to: 	0	0	0	0	3608	1296	1447	208	3153	0	384	0	1442	10824	0	12930	
From rank 15 to: 	0	0	0	0	0	2266	0	0	1745	1945	447	1334	10500	2008	12963	0	


After loadbalancing process 9 has 654607 cells.
After loadbalancing process 8 has 655084 cells.
After loadbalancing process 3 has 654622 cells.
After loadbalancing process 1 has 655219 cells.
After loadbalancing process 10 has 659338 cells.
After loadbalancing process 0 has 655309 cells.
After loadbalancing process 7 has 658663 cells.
After loadbalancing process 4 has 656245 cells.
After loadbalancing process 11 has 648837 cells.
After loadbalancing process 2 has 657717 cells.
After loadbalancing process 14 has 660762 cells.
After loadbalancing process 5 has 659143 cells.
After loadbalancing process 15 has 658679 cells.
After loadbalancing process 12 has 658700 cells.
After loadbalancing process 13 has 651142 cells.
After loadbalancing process 6 has 666427 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          139.575         0.212949
    2           80.245         0.574925
    3          56.4698         0.703716
    4          45.6618         0.808606
    5          41.9788         0.919343
    6           38.318         0.912792
    7          31.0063         0.809184
    8          28.7229         0.926358
    9          27.3282         0.951442
   10          22.6758         0.829758
   11           21.301         0.939373
   12          21.4939          1.00905
   13          18.1973         0.846626
   14          16.8513         0.926036
   15          17.4529           1.0357
   16            15.41         0.882949
   17          14.1293         0.916895
   18           14.559          1.03041
   19          13.2553         0.910456
   20          12.3661         0.932919
   21          12.5516            1.015
   22          11.3895         0.907409
   23          10.9366         0.960233
   24          11.0581          1.01111
   25          9.95451         0.900202
   26          9.72132         0.976574
   27          9.91088           1.0195
   28          8.92112         0.900135
   29          8.82325         0.989029
   30          9.21124          1.04397
   31          9.02599         0.979889
   32          10.1143          1.12057
   33          11.9143          1.17797
   34          12.0388          1.01045
   35          10.8925         0.904788
   36           8.3913         0.770372
   37          6.21956         0.741192
   38          5.88891         0.946837
   39           5.6111         0.952825
   40          4.46978         0.796596
   41          4.01908         0.899168
   42          3.83173         0.953385
   43          3.11894         0.813976
   44          2.68952         0.862319
   45           2.4959         0.928008
   46          2.06737         0.828307
   47          1.79917         0.870269
   48           1.6651         0.925482
   49          1.41116         0.847494
   50          1.33301         0.944619
   51          1.23793         0.928674
   52          1.07058         0.864816
   53          1.07584          1.00491
   54         0.953161         0.885969
   55         0.899246         0.943435
   56         0.900985          1.00193
   57         0.804259         0.892644
   58         0.823034          1.02334
   59         0.778387         0.945754
   60         0.757639         0.973344
   61         0.745524          0.98401
   62         0.734607         0.985357
   63         0.705018         0.959721
   64         0.680441         0.965141
   65         0.654264         0.961529
   66         0.573008         0.875806
   67         0.532142         0.928681
   68         0.432435         0.812632
   69         0.363976         0.841689
   70         0.292368          0.80326
   71         0.244498         0.836269
   72         0.219313         0.896992
   73         0.232813          1.06156
   74         0.244526          1.05031
   75         0.258903           1.0588
   76         0.243718         0.941348
   77          0.22358         0.917374
   78         0.216298         0.967427
   79         0.209545         0.968779
   80         0.178057         0.849734
   81         0.157338          0.88364
   82         0.143218         0.910253
   83         0.135737         0.947765
   84         0.126084         0.928888
   85         0.121653         0.964855
   86         0.115721         0.951234
   87         0.112061         0.968376
   88         0.104587         0.933302
   89         0.102796         0.982879
   90        0.0965825         0.939554
   91        0.0905168         0.937197
   92        0.0863009         0.953424
   93        0.0794572         0.920699
   94        0.0714003         0.898601
   95        0.0668505         0.936277
   96        0.0607093         0.908136
=== rate=0.907793, T=144.526, TIT=1.50548, IT=96

 Elapsed time: 144.526
Rank 0: Matrix-vector product took 0.549993 seconds
Rank 1: Matrix-vector product took 0.53631 seconds
Rank 2: Matrix-vector product took 0.540509 seconds
Rank 3: Matrix-vector product took 0.535488 seconds
Rank 4: Matrix-vector product took 0.537276 seconds
Rank 5: Matrix-vector product took 0.540988 seconds
Rank 6: Matrix-vector product took 0.558076 seconds
Rank 7: Matrix-vector product took 0.539823 seconds
Rank 8: Matrix-vector product took 0.549041 seconds
Rank 9: Matrix-vector product took 0.53738 seconds
Rank 10: Matrix-vector product took 0.541934 seconds
Rank 11: Matrix-vector product took 0.534736 seconds
Rank 12: Matrix-vector product took 0.547041 seconds
Rank 13: Matrix-vector product took 0.533036 seconds
Rank 14: Matrix-vector product took 0.540604 seconds
Rank 15: Matrix-vector product took 0.538294 seconds
Average time for Matrix-vector product is 0.541283 seconds

Rank 0: copyOwnerToAll took 0.00261498 seconds
Rank 1: copyOwnerToAll took 0.00261498 seconds
Rank 2: copyOwnerToAll took 0.00261498 seconds
Rank 3: copyOwnerToAll took 0.00261498 seconds
Rank 4: copyOwnerToAll took 0.00261498 seconds
Rank 5: copyOwnerToAll took 0.00261498 seconds
Rank 6: copyOwnerToAll took 0.00261498 seconds
Rank 7: copyOwnerToAll took 0.00261498 seconds
Rank 8: copyOwnerToAll took 0.00261498 seconds
Rank 9: copyOwnerToAll took 0.00261498 seconds
Rank 10: copyOwnerToAll took 0.00261498 seconds
Rank 11: copyOwnerToAll took 0.00261498 seconds
Rank 12: copyOwnerToAll took 0.00261498 seconds
Rank 13: copyOwnerToAll took 0.00261498 seconds
Rank 14: copyOwnerToAll took 0.00261498 seconds
Rank 15: copyOwnerToAll took 0.00261498 seconds
Average time for copyOwnertoAll is 0.00270403 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	12788	1518	12408	73	0	3021	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	12787	0	11385	380	0	0	1536	3652	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1488	11386	0	12897	1239	4422	332	480	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	12414	440	12878	0	3152	0	263	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	73	0	1238	3144	0	11775	10766	144	0	0	0	0	0	0	3636	0	
Rank 5's ghost cells:	0	0	4479	0	11792	0	2200	11169	0	0	0	0	505	0	1304	2233	
Rank 6's ghost cells:	2962	1517	340	264	10735	2183	0	12342	0	0	0	0	92	3233	1479	0	
Rank 7's ghost cells:	0	3650	481	0	144	11226	12333	0	0	0	0	0	4965	187	208	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	12666	11366	146	0	567	3101	1762	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	12700	0	3012	11440	0	0	0	1974	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	11224	3012	0	13601	1014	4203	370	447	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	146	11578	13590	0	3393	0	0	1348	
Rank 12's ghost cells:	0	0	0	0	0	514	91	4934	0	0	1022	3345	0	11368	1445	10516	
Rank 13's ghost cells:	0	0	0	0	0	0	3255	182	571	0	4230	0	11374	0	10769	1980	
Rank 14's ghost cells:	0	0	0	0	3608	1296	1447	208	3153	0	384	0	1442	10824	0	12930	
Rank 15's ghost cells:	0	0	0	0	0	2266	0	0	1745	1945	447	1334	10500	2008	12963	0	
