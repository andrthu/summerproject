Number of cores/ranks per node is: 32
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 155954
Cell on rank 1 before loadbalancing: 155953
Cell on rank 2 before loadbalancing: 156449
Cell on rank 3 before loadbalancing: 156449
Cell on rank 4 before loadbalancing: 155090
Cell on rank 5 before loadbalancing: 155090
Cell on rank 6 before loadbalancing: 157313
Cell on rank 7 before loadbalancing: 157312
Cell on rank 8 before loadbalancing: 156278
Cell on rank 9 before loadbalancing: 156277
Cell on rank 10 before loadbalancing: 156045
Cell on rank 11 before loadbalancing: 156510
Cell on rank 12 before loadbalancing: 156141
Cell on rank 13 before loadbalancing: 156142
Cell on rank 14 before loadbalancing: 156141
Cell on rank 15 before loadbalancing: 156141
Cell on rank 16 before loadbalancing: 157373
Cell on rank 17 before loadbalancing: 157373
Cell on rank 18 before loadbalancing: 155334
Cell on rank 19 before loadbalancing: 155335
Cell on rank 20 before loadbalancing: 155880
Cell on rank 21 before loadbalancing: 155878
Cell on rank 22 before loadbalancing: 156828
Cell on rank 23 before loadbalancing: 156829
Cell on rank 24 before loadbalancing: 156416
Cell on rank 25 before loadbalancing: 156168
Cell on rank 26 before loadbalancing: 156292
Cell on rank 27 before loadbalancing: 156292
Cell on rank 28 before loadbalancing: 156292
Cell on rank 29 before loadbalancing: 156292
Cell on rank 30 before loadbalancing: 156292
Cell on rank 31 before loadbalancing: 156292
Cell on rank 32 before loadbalancing: 157486
Cell on rank 33 before loadbalancing: 157202
Cell on rank 34 before loadbalancing: 155179
Cell on rank 35 before loadbalancing: 155179
Cell on rank 36 before loadbalancing: 156230
Cell on rank 37 before loadbalancing: 156231
Cell on rank 38 before loadbalancing: 156293
Cell on rank 39 before loadbalancing: 156293
Cell on rank 40 before loadbalancing: 155340
Cell on rank 41 before loadbalancing: 155340
Cell on rank 42 before loadbalancing: 157169
Cell on rank 43 before loadbalancing: 157170
Cell on rank 44 before loadbalancing: 157161
Cell on rank 45 before loadbalancing: 157161
Cell on rank 46 before loadbalancing: 155102
Cell on rank 47 before loadbalancing: 155298
Cell on rank 48 before loadbalancing: 156250
Cell on rank 49 before loadbalancing: 156248
Cell on rank 50 before loadbalancing: 156248
Cell on rank 51 before loadbalancing: 156249
Cell on rank 52 before loadbalancing: 156242
Cell on rank 53 before loadbalancing: 156243
Cell on rank 54 before loadbalancing: 156242
Cell on rank 55 before loadbalancing: 156243
Cell on rank 56 before loadbalancing: 156275
Cell on rank 57 before loadbalancing: 156274
Cell on rank 58 before loadbalancing: 156220
Cell on rank 59 before loadbalancing: 156107
Cell on rank 60 before loadbalancing: 156420
Cell on rank 61 before loadbalancing: 156420
Cell on rank 62 before loadbalancing: 156017
Cell on rank 63 before loadbalancing: 156017


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	2628	392	3271	1147	1676	0	0	0	0	0	2113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2643	0	3130	13	0	1653	0	0	885	2324	0	554	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	390	3164	0	3179	0	867	2447	0	3226	0	0	23	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	3290	13	3181	0	153	252	647	1897	869	0	1411	753	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	1145	0	0	159	0	4736	933	2651	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	1675	1652	878	248	4792	0	2325	535	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	2443	646	933	2320	0	3310	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	1906	2632	537	3312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	904	3246	865	0	0	0	0	0	3691	2263	608	248	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	2314	0	0	0	0	0	0	3716	0	1310	2911	577	364	1977	940	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	1438	0	0	0	0	2232	1313	0	3520	2998	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	2076	556	23	760	0	0	0	0	605	2913	3508	0	0	2566	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	260	572	2968	0	0	4000	241	3039	1862	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	368	322	2568	4022	0	3456	437	817	0	2087	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	1986	0	273	249	3459	0	3532	0	0	2061	312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	204	937	0	0	3006	430	3585	0	847	1636	1397	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	1862	797	0	859	0	4507	1290	1631	4	458	49	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1635	4538	0	2627	1005	2570	1400	326	135	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	2087	2022	1362	1294	2652	0	5316	0	0	1642	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	319	0	1633	994	5330	0	0	0	2557	1438	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	2574	0	0	0	3384	2879	741	0	0	0	0	0	0	472	2613	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	461	1374	0	0	3378	0	452	3279	0	0	1340	0	0	51	1422	121	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	312	1638	2524	2916	452	0	3696	0	0	0	0	187	0	0	2218	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	146	0	1467	743	3275	3683	0	0	0	0	0	2877	443	506	322	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3480	503	2029	1274	124	652	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	1065	927	1539	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3514	0	1400	2882	67	3769	696	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2503	0	1086	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1335	0	0	509	1391	0	4047	0	598	5565	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2021	2872	4073	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2345	849	0	412	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	2911	1262	67	0	0	0	3126	2202	3867	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	0	435	107	3768	595	0	3155	0	2379	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	522	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	471	1453	0	515	655	710	5542	0	2208	2400	0	4388	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2590	116	2218	330	0	0	0	0	3894	0	4435	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3305	3069	0	58	2638	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	211	2215	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3302	0	365	3073	2235	169	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2918	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3066	351	0	3210	61	173	0	2318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2763	71	10	83	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3092	3213	0	248	0	2432	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	265	2286	109	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	2232	61	246	0	3416	2981	478	0	0	0	0	1044	1852	68	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2623	186	171	0	3419	0	0	2719	0	0	0	0	2097	0	327	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2433	2984	0	0	2714	0	0	0	0	0	0	28	2972	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2317	127	468	2706	2721	0	0	0	0	0	0	0	2955	164	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3158	679	2654	0	2352	192	475	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3161	0	2464	806	1687	523	283	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	711	2447	0	4340	0	0	2828	992	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2657	804	4347	0	0	0	0	1257	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1041	2106	0	0	0	1701	0	0	0	3204	3484	465	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1869	0	0	0	2349	536	0	0	3198	0	792	3024	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	338	28	2963	192	277	2823	0	3489	816	0	3364	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	343	0	2963	160	484	0	974	1252	462	3027	3372	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	178	2494	0	2380	0	522	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3893	3449	0	0	1066	113	0	0	0	0	0	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1086	0	0	849	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3893	0	1654	4514	2224	1334	263	167	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	919	1086	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3450	1656	0	2390	0	0	1758	4	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1540	0	0	412	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4496	2389	0	940	0	487	2917	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2224	0	944	0	3000	152	3136	179	22	0	2338	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1077	1310	0	0	2982	0	2989	132	2102	0	0	36	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	113	256	1754	455	145	2999	0	3463	411	1996	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	4	2886	3137	132	3471	0	62	860	2225	762	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	2089	414	62	0	3897	7	2274	467	0	0	2833	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	0	1963	866	3903	0	3447	458	2419	630	0	338	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2256	7	3467	0	3405	0	1713	74	61	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2351	36	0	775	2251	468	3399	0	0	4	1696	530	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2745	263	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	457	2420	0	0	0	3313	268	2926	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	2309	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	615	1713	4	3298	0	3331	161	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	225	2901	12	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	1689	268	3331	0	3571	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2213	0	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2818	326	66	552	2919	161	3582	0	

Edge-cut for node partition: 11446



After loadbalancing process 36 has 164714 cells.
After loadbalancing process 35 has 168915 cells.
After loadbalancing process 38 has 166965 cells.
After loadbalancing process 52 has 168547 cells.
After loadbalancing process 39 has 165699 cells.
After loadbalancing process 51 has 167606 cells.
After loadbalancing process 53 has 167756 cells.
After loadbalancing process 37 has 167195 cells.
After loadbalancing process 26 has 167203 cells.
After loadbalancing process 10 has 168487 cells.
After loadbalancing process 0 has 168982 cells.
After loadbalancing process 43 has 169797 cells.
After loadbalancing process 42 has 167874 cells.
After loadbalancing process 15 has 168335 cells.
After loadbalancing process 29 has 167930 cells.
After loadbalancing process 7 has 167751 cells.
After loadbalancing process 30 has 168208 cells.
After loadbalancing process 24 has 168497 cells.
After loadbalancing process 32 has 167181 cells.
After loadbalancing process 41 has 170386 cells.
After loadbalancing process 45 has 170219 cells.
After loadbalancing process 25 has 170320 cells.
After loadbalancing process 9 has 164264 cells.
After loadbalancing process 8 has 164850 cells.
After loadbalancing process 11 has 166235 cells.
After loadbalancing process 33 has 167155 cells.
After loadbalancing process 34 has 169745 cells.
After loadbalancing process 58 has 169737 cells.
After loadbalancing process 27 has 167617 cells.
After loadbalancing process 28 has 168812 cells.
After loadbalancing process 2 has 167285 cells.
After loadbalancing process 22 has 167834 cells.
After loadbalancing process 50 has 171709 cells.
After loadbalancing process 3 has 166965 cells.
After loadbalancing process 44 has 169083 cells.
After loadbalancing process 49 has 171609 cells.
After loadbalancing process 1 has 169264 cells.
After loadbalancing process 6 has 167424 cells.
After loadbalancing process 14 has 169468 cells.
After loadbalancing process 12 has 169162 cells.
After loadbalancing process 48 has 168830 cells.
After loadbalancing process 56 has 168188 cells.
After loadbalancing process 46 has 168013 cells.
After loadbalancing process 21 has 166871 cells.
After loadbalancing process 57 has 172085 cells.
After loadbalancing process 17 has 172232 cells.
After loadbalancing process 59 has 168864 cells.
After loadbalancing process 54 has 170821 cells.
After loadbalancing process 23 has 169948 cells.
After loadbalancing process 40 has 168308 cells.
After loadbalancing process 61 has 167298 cells.
After loadbalancing process 60 has 169914 cells.
After loadbalancing process 18 has 167511 cells.
After loadbalancing process 47 has 168183 cells.
After loadbalancing process 55 has 170291 cells.
After loadbalancing process 31 has 168741 cells.
After loadbalancing process 16 has 170345 cells.
After loadbalancing process 13 has 168929 cells.
After loadbalancing process 63 has 169875 cells.
After loadbalancing process 19 has 169430 cells.
After loadbalancing process 20 has 168237 cells.
After loadbalancing process 62 has 174634 cells.
After loadbalancing process 4 has 168989 cells.
After loadbalancing process 5 has 167773 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          142.946         0.218092
    2          82.8709         0.579736
    3          59.0135         0.712113
    4          50.6107         0.857613
    5          47.0095         0.928845
    6          38.0123         0.808609
    7          32.2217         0.847665
    8           31.772         0.986043
    9          27.3424         0.860581
   10          23.9323         0.875283
   11          24.1692           1.0099
   12          21.1841         0.876491
   13           18.785          0.88675
   14          19.2555          1.02505
   15          17.2283         0.894719
   16          15.4189         0.894975
   17          15.8121           1.0255
   18          14.6968         0.929467
   19          13.2805          0.90363
   20          13.5518          1.02043
   21          12.5878         0.928865
   22          11.7039         0.929786
   23          11.9091          1.01753
   24          10.9693         0.921085
   25          10.5162         0.958696
   26          10.6372          1.01151
   27          9.65834         0.907978
   28          9.47801         0.981329
   29          9.60639          1.01354
   30           8.9018         0.926654
   31          9.19089          1.03248
   32           10.047          1.09315
   33          10.6177          1.05681
   34          12.0136          1.13147
   35           12.238          1.01867
   36          10.2406         0.836787
   37          8.46053         0.826177
   38          6.99819         0.827158
   39           5.8867         0.841175
   40          5.18124         0.880159
   41          4.47751         0.864178
   42          4.13842         0.924268
   43          3.75855         0.908209
   44          3.06992         0.816782
   45          2.80641         0.914164
   46          2.54539         0.906991
   47          2.06596          0.81165
   48          1.89543         0.917457
   49          1.69401         0.893733
   50          1.44003          0.85007
   51           1.3945         0.968386
   52          1.24265          0.89111
   53          1.12413         0.904623
   54          1.09095         0.970482
   55         0.973076         0.891952
   56         0.931556         0.957331
   57         0.885139         0.950173
   58         0.836284         0.944806
   59         0.810571         0.969252
   60         0.791065         0.975936
   61         0.768974         0.972074
   62         0.741418         0.964165
   63         0.746684           1.0071
   64         0.700248          0.93781
   65         0.694559         0.991875
   66         0.652171         0.938972
   67         0.599302         0.918933
   68          0.54912         0.916266
   69          0.47037         0.856588
   70         0.393453         0.836475
   71         0.323209         0.821469
   72         0.264632         0.818762
   73         0.231362          0.87428
   74         0.228914         0.989418
   75         0.229523          1.00266
   76         0.244132          1.06365
   77         0.245218          1.00445
   78         0.242387         0.988457
   79         0.238791         0.985161
   80          0.22207         0.929977
   81         0.194152         0.874282
   82         0.177372         0.913572
   83         0.159436         0.898879
   84          0.14615         0.916673
   85         0.133663         0.914557
   86         0.126013         0.942767
   87         0.120964         0.959931
   88         0.115515         0.954957
   89         0.109381         0.946898
   90         0.106279         0.971641
   91        0.0990166         0.931665
   92        0.0934754         0.944038
   93        0.0903745         0.966826
   94         0.083451         0.923391
   95        0.0772575         0.925784
   96        0.0722575         0.935281
   97         0.064914          0.89837
=== rate=0.909326, T=39.9675, TIT=0.412036, IT=97

 Elapsed time: 39.9675
Rank 0: Matrix-vector product took 0.138031 seconds
Rank 1: Matrix-vector product took 0.137943 seconds
Rank 2: Matrix-vector product took 0.136033 seconds
Rank 3: Matrix-vector product took 0.136598 seconds
Rank 4: Matrix-vector product took 0.138328 seconds
Rank 5: Matrix-vector product took 0.138522 seconds
Rank 6: Matrix-vector product took 0.136234 seconds
Rank 7: Matrix-vector product took 0.136607 seconds
Rank 8: Matrix-vector product took 0.135353 seconds
Rank 9: Matrix-vector product took 0.134339 seconds
Rank 10: Matrix-vector product took 0.137629 seconds
Rank 11: Matrix-vector product took 0.13571 seconds
Rank 12: Matrix-vector product took 0.138235 seconds
Rank 13: Matrix-vector product took 0.137769 seconds
Rank 14: Matrix-vector product took 0.138365 seconds
Rank 15: Matrix-vector product took 0.137094 seconds
Rank 16: Matrix-vector product took 0.139257 seconds
Rank 17: Matrix-vector product took 0.140584 seconds
Rank 18: Matrix-vector product took 0.137261 seconds
Rank 19: Matrix-vector product took 0.137783 seconds
Rank 20: Matrix-vector product took 0.13727 seconds
Rank 21: Matrix-vector product took 0.138519 seconds
Rank 22: Matrix-vector product took 0.137019 seconds
Rank 23: Matrix-vector product took 0.138192 seconds
Rank 24: Matrix-vector product took 0.137185 seconds
Rank 25: Matrix-vector product took 0.141399 seconds
Rank 26: Matrix-vector product took 0.137191 seconds
Rank 27: Matrix-vector product took 0.139273 seconds
Rank 28: Matrix-vector product took 0.137631 seconds
Rank 29: Matrix-vector product took 0.137373 seconds
Rank 30: Matrix-vector product took 0.136867 seconds
Rank 31: Matrix-vector product took 0.13789 seconds
Rank 32: Matrix-vector product took 0.139114 seconds
Rank 33: Matrix-vector product took 0.139021 seconds
Rank 34: Matrix-vector product took 0.13832 seconds
Rank 35: Matrix-vector product took 0.139062 seconds
Rank 36: Matrix-vector product took 0.134681 seconds
Rank 37: Matrix-vector product took 0.135925 seconds
Rank 38: Matrix-vector product took 0.136823 seconds
Rank 39: Matrix-vector product took 0.135434 seconds
Rank 40: Matrix-vector product took 0.13682 seconds
Rank 41: Matrix-vector product took 0.138634 seconds
Rank 42: Matrix-vector product took 0.136665 seconds
Rank 43: Matrix-vector product took 0.138542 seconds
Rank 44: Matrix-vector product took 0.13781 seconds
Rank 45: Matrix-vector product took 0.139398 seconds
Rank 46: Matrix-vector product took 0.136769 seconds
Rank 47: Matrix-vector product took 0.136907 seconds
Rank 48: Matrix-vector product took 0.138284 seconds
Rank 49: Matrix-vector product took 0.139657 seconds
Rank 50: Matrix-vector product took 0.139781 seconds
Rank 51: Matrix-vector product took 0.136841 seconds
Rank 52: Matrix-vector product took 0.13778 seconds
Rank 53: Matrix-vector product took 0.136632 seconds
Rank 54: Matrix-vector product took 0.138252 seconds
Rank 55: Matrix-vector product took 0.138405 seconds
Rank 56: Matrix-vector product took 0.137077 seconds
Rank 57: Matrix-vector product took 0.140282 seconds
Rank 58: Matrix-vector product took 0.138452 seconds
Rank 59: Matrix-vector product took 0.141349 seconds
Rank 60: Matrix-vector product took 0.137914 seconds
Rank 61: Matrix-vector product took 0.137866 seconds
Rank 62: Matrix-vector product took 0.141525 seconds
Rank 63: Matrix-vector product took 0.138242 seconds
Average time for Matrix-vector product is 0.13784 seconds

Rank 0: copyOwnerToAll took 0.0011812 seconds
Rank 1: copyOwnerToAll took 0.0011812 seconds
Rank 2: copyOwnerToAll took 0.0011812 seconds
Rank 3: copyOwnerToAll took 0.0011812 seconds
Rank 4: copyOwnerToAll took 0.0011812 seconds
Rank 5: copyOwnerToAll took 0.0011812 seconds
Rank 6: copyOwnerToAll took 0.0011812 seconds
Rank 7: copyOwnerToAll took 0.0011812 seconds
Rank 8: copyOwnerToAll took 0.0011812 seconds
Rank 9: copyOwnerToAll took 0.0011812 seconds
Rank 10: copyOwnerToAll took 0.0011812 seconds
Rank 11: copyOwnerToAll took 0.0011812 seconds
Rank 12: copyOwnerToAll took 0.0011812 seconds
Rank 13: copyOwnerToAll took 0.0011812 seconds
Rank 14: copyOwnerToAll took 0.0011812 seconds
Rank 15: copyOwnerToAll took 0.0011812 seconds
Rank 16: copyOwnerToAll took 0.0011812 seconds
Rank 17: copyOwnerToAll took 0.0011812 seconds
Rank 18: copyOwnerToAll took 0.0011812 seconds
Rank 19: copyOwnerToAll took 0.0011812 seconds
Rank 20: copyOwnerToAll took 0.0011812 seconds
Rank 21: copyOwnerToAll took 0.0011812 seconds
Rank 22: copyOwnerToAll took 0.0011812 seconds
Rank 23: copyOwnerToAll took 0.0011812 seconds
Rank 24: copyOwnerToAll took 0.0011812 seconds
Rank 25: copyOwnerToAll took 0.0011812 seconds
Rank 26: copyOwnerToAll took 0.0011812 seconds
Rank 27: copyOwnerToAll took 0.0011812 seconds
Rank 28: copyOwnerToAll took 0.0011812 seconds
Rank 29: copyOwnerToAll took 0.0011812 seconds
Rank 30: copyOwnerToAll took 0.0011812 seconds
Rank 31: copyOwnerToAll took 0.0011812 seconds
Rank 32: copyOwnerToAll took 0.0011812 seconds
Rank 33: copyOwnerToAll took 0.0011812 seconds
Rank 34: copyOwnerToAll took 0.0011812 seconds
Rank 35: copyOwnerToAll took 0.0011812 seconds
Rank 36: copyOwnerToAll took 0.0011812 seconds
Rank 37: copyOwnerToAll took 0.0011812 seconds
Rank 38: copyOwnerToAll took 0.0011812 seconds
Rank 39: copyOwnerToAll took 0.0011812 seconds
Rank 40: copyOwnerToAll took 0.0011812 seconds
Rank 41: copyOwnerToAll took 0.0011812 seconds
Rank 42: copyOwnerToAll took 0.0011812 seconds
Rank 43: copyOwnerToAll took 0.0011812 seconds
Rank 44: copyOwnerToAll took 0.0011812 seconds
Rank 45: copyOwnerToAll took 0.0011812 seconds
Rank 46: copyOwnerToAll took 0.0011812 seconds
Rank 47: copyOwnerToAll took 0.0011812 seconds
Rank 48: copyOwnerToAll took 0.0011812 seconds
Rank 49: copyOwnerToAll took 0.0011812 seconds
Rank 50: copyOwnerToAll took 0.0011812 seconds
Rank 51: copyOwnerToAll took 0.0011812 seconds
Rank 52: copyOwnerToAll took 0.0011812 seconds
Rank 53: copyOwnerToAll took 0.0011812 seconds
Rank 54: copyOwnerToAll took 0.0011812 seconds
Rank 55: copyOwnerToAll took 0.0011812 seconds
Rank 56: copyOwnerToAll took 0.0011812 seconds
Rank 57: copyOwnerToAll took 0.0011812 seconds
Rank 58: copyOwnerToAll took 0.0011812 seconds
Rank 59: copyOwnerToAll took 0.0011812 seconds
Rank 60: copyOwnerToAll took 0.0011812 seconds
Rank 61: copyOwnerToAll took 0.0011812 seconds
Rank 62: copyOwnerToAll took 0.0011812 seconds
Rank 63: copyOwnerToAll took 0.0011812 seconds
Average time for copyOwnertoAll is 0.00119657 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	3305	3069	0	58	2638	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	211	2215	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	3302	0	365	3073	2235	169	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2918	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	3066	351	0	3210	61	173	0	2318	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2763	71	10	83	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	3092	3213	0	248	0	2432	141	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	265	2286	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	53	2232	61	246	0	3416	2981	478	0	0	0	0	1044	1852	68	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	2623	186	171	0	3419	0	0	2719	0	0	0	0	2097	0	327	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	2433	2984	0	0	2714	0	0	0	0	0	0	28	2972	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	2317	127	468	2706	2721	0	0	0	0	0	0	0	2955	164	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	3158	679	2654	0	2352	192	475	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	3161	0	2464	806	1687	523	283	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	711	2447	0	4340	0	0	2828	992	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	2657	804	4347	0	0	0	0	1257	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	1041	2106	0	0	0	1701	0	0	0	3204	3484	465	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	1869	0	0	0	2349	536	0	0	3198	0	792	3024	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	76	338	28	2963	192	277	2823	0	3489	816	0	3364	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	343	0	2963	160	484	0	974	1252	462	3027	3372	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3893	3449	0	0	1066	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	178	2494	0	2380	0	522	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3893	0	1654	4514	2224	1334	263	167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1086	0	0	849	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3450	1656	0	2390	0	0	1758	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	919	1086	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4496	2389	0	940	0	487	2917	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1540	0	0	412	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2224	0	944	0	3000	152	3136	179	22	0	2338	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1077	1310	0	0	2982	0	2989	132	2102	0	0	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	113	256	1754	455	145	2999	0	3463	411	1996	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	4	2886	3137	132	3471	0	62	860	2225	762	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	2089	414	62	0	3897	7	2274	467	0	0	2833	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	0	1963	866	3903	0	3447	458	2419	630	0	338	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2256	7	3467	0	3405	0	1713	74	61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2351	36	0	775	2251	468	3399	0	0	4	1696	530	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	2745	263	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	457	2420	0	0	0	3313	268	2926	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	79	2309	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	615	1713	4	3298	0	3331	161	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	225	2901	12	109	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	1689	268	3331	0	3571	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	2213	0	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2818	326	66	552	2919	161	3582	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2628	392	3271	1147	1676	0	0	0	0	0	2113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2643	0	3130	13	0	1653	0	0	885	2324	0	554	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	390	3164	0	3179	0	867	2447	0	3226	0	0	23	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3290	13	3181	0	153	252	647	1897	869	0	1411	753	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1145	0	0	159	0	4736	933	2651	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1675	1652	878	248	4792	0	2325	535	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2443	646	933	2320	0	3310	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1906	2632	537	3312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	904	3246	865	0	0	0	0	0	3691	2263	608	248	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2314	0	0	0	0	0	0	3716	0	1310	2911	577	364	1977	940	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1438	0	0	0	0	2232	1313	0	3520	2998	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2076	556	23	760	0	0	0	0	605	2913	3508	0	0	2566	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	260	572	2968	0	0	4000	241	3039	1862	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	368	322	2568	4022	0	3456	437	817	0	2087	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1986	0	273	249	3459	0	3532	0	0	2061	312	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	204	937	0	0	3006	430	3585	0	847	1636	1397	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1862	797	0	859	0	4507	1290	1631	4	458	49	0	0	0	0	0	0	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1635	4538	0	2627	1005	2570	1400	326	135	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2087	2022	1362	1294	2652	0	5316	0	0	1642	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	319	0	1633	994	5330	0	0	0	2557	1438	0	0	0	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	2574	0	0	0	3384	2879	741	0	0	0	0	0	0	472	2613	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	461	1374	0	0	3378	0	452	3279	0	0	1340	0	0	51	1422	121	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	312	1638	2524	2916	452	0	3696	0	0	0	0	187	0	0	2218	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	146	0	1467	743	3275	3683	0	0	0	0	0	2877	443	506	322	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	179	1065	927	1539	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3480	503	2029	1274	124	652	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2503	0	1086	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3514	0	1400	2882	67	3769	696	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1335	0	0	509	1391	0	4047	0	598	5565	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2345	849	0	412	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2021	2872	4073	0	0	0	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	187	2911	1262	67	0	0	0	3126	2202	3867	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	522	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	0	435	107	3768	595	0	3155	0	2379	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	471	1453	0	515	655	710	5542	0	2208	2400	0	4388	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2590	116	2218	330	0	0	0	0	3894	0	4435	0	
