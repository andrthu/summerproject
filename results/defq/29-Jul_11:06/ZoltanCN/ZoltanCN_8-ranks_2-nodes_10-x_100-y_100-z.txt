Number of cores/ranks per node is: 4
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 12455
Cell on rank 1 before loadbalancing: 12545
Cell on rank 2 before loadbalancing: 12465
Cell on rank 3 before loadbalancing: 12535
Cell on rank 4 before loadbalancing: 12511
Cell on rank 5 before loadbalancing: 12489
Cell on rank 6 before loadbalancing: 12500
Cell on rank 7 before loadbalancing: 12500


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	520	0	0	0	206	0	0	
From rank 1 to: 	520	0	506	0	120	144	0	0	
From rank 2 to: 	0	506	0	508	0	0	0	223	
From rank 3 to: 	0	0	508	0	0	0	230	77	
From rank 4 to: 	0	120	0	0	0	553	0	507	
From rank 5 to: 	206	144	0	0	553	0	0	0	
From rank 6 to: 	0	0	0	230	0	0	0	500	
From rank 7 to: 	0	0	223	77	507	0	500	0	

Edge-cut for node partition: 1000



After loadbalancing process 0 has 13181 cells.
After loadbalancing process 1 has 13835 cells.
After loadbalancing process 5 has 13392 cells.
After loadbalancing process 4 has 13691 cells.
After loadbalancing process 6 has 13230 cells.
After loadbalancing process 3 has 13350 cells.
After loadbalancing process 2 has 13702 cells.
After loadbalancing process 7 has 13807 cells.


=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          34.8319          0.21736
    2          20.9897           0.6026
    3           19.964         0.951136
    4          13.5449         0.678465
    5          6.83143         0.504355
    6          4.67269         0.683998
    7          2.80503         0.600303
    8          1.89515         0.675627
    9          1.10981         0.585603
   10         0.694257         0.625566
   11         0.548619         0.790226
   12         0.243698         0.444202
   13         0.181389         0.744321
   14         0.119923         0.661134
   15        0.0632631         0.527533
   16        0.0470039         0.742992
   17         0.030592         0.650839
   18        0.0178349         0.582991
   19        0.0130453         0.731449
=== rate=0.609216, T=0.603375, TIT=0.0317566, IT=19

 Elapsed time: 0.603375
Rank 0: Matrix-vector product took 0.010467 seconds
Rank 1: Matrix-vector product took 0.0111964 seconds
Rank 2: Matrix-vector product took 0.0109142 seconds
Rank 3: Matrix-vector product took 0.0106807 seconds
Rank 4: Matrix-vector product took 0.0111162 seconds
Rank 5: Matrix-vector product took 0.0106195 seconds
Rank 6: Matrix-vector product took 0.0105066 seconds
Rank 7: Matrix-vector product took 0.0109516 seconds
Average time for Matrix-vector product is 0.0108066 seconds

Rank 0: copyOwnerToAll took 0.00014548 seconds
Rank 1: copyOwnerToAll took 0.00014548 seconds
Rank 2: copyOwnerToAll took 0.00014548 seconds
Rank 3: copyOwnerToAll took 0.00014548 seconds
Rank 4: copyOwnerToAll took 0.00014548 seconds
Rank 5: copyOwnerToAll took 0.00014548 seconds
Rank 6: copyOwnerToAll took 0.00014548 seconds
Rank 7: copyOwnerToAll took 0.00014548 seconds
Average time for copyOwnertoAll is 0.000157457 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	520	0	0	0	206	0	0	
Rank 1's ghost cells:	520	0	506	0	120	144	0	0	
Rank 2's ghost cells:	0	506	0	508	0	0	0	223	
Rank 3's ghost cells:	0	0	508	0	0	0	230	77	
Rank 4's ghost cells:	0	120	0	0	0	553	0	507	
Rank 5's ghost cells:	206	144	0	0	553	0	0	0	
Rank 6's ghost cells:	0	0	0	230	0	0	0	500	
Rank 7's ghost cells:	0	0	223	77	507	0	500	0	
