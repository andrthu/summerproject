Number of cores/ranks per node is: 16
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 311727
Cell on rank 1 before loadbalancing: 311728
Cell on rank 2 before loadbalancing: 311511
Cell on rank 3 before loadbalancing: 311511
Cell on rank 4 before loadbalancing: 311620
Cell on rank 5 before loadbalancing: 311619
Cell on rank 6 before loadbalancing: 311619
Cell on rank 7 before loadbalancing: 311619
Cell on rank 8 before loadbalancing: 313409
Cell on rank 9 before loadbalancing: 313409
Cell on rank 10 before loadbalancing: 313409
Cell on rank 11 before loadbalancing: 313409
Cell on rank 12 before loadbalancing: 313407
Cell on rank 13 before loadbalancing: 313407
Cell on rank 14 before loadbalancing: 313591
Cell on rank 15 before loadbalancing: 313223
Cell on rank 16 before loadbalancing: 312224
Cell on rank 17 before loadbalancing: 312224
Cell on rank 18 before loadbalancing: 312224
Cell on rank 19 before loadbalancing: 312223
Cell on rank 20 before loadbalancing: 312748
Cell on rank 21 before loadbalancing: 312749
Cell on rank 22 before loadbalancing: 312749
Cell on rank 23 before loadbalancing: 312749
Cell on rank 24 before loadbalancing: 312487
Cell on rank 25 before loadbalancing: 312487
Cell on rank 26 before loadbalancing: 312486
Cell on rank 27 before loadbalancing: 312487
Cell on rank 28 before loadbalancing: 312487
Cell on rank 29 before loadbalancing: 312487
Cell on rank 30 before loadbalancing: 312485
Cell on rank 31 before loadbalancing: 312486


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	7812	3851	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	7771	0	2080	4883	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	3849	2031	0	6011	0	2711	2202	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	4883	5973	0	1049	1651	3085	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	1053	0	6022	2679	4895	0	0	0	0	2353	1627	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	2735	1651	6011	0	4965	2711	0	0	0	0	0	1168	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	2203	3041	2679	4985	0	5913	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	4897	2703	5888	0	0	0	0	0	2709	2683	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	4658	4311	677	0	0	0	0	458	1563	0	0	0	0	0	0	0	0	0	0	0	1799	2570	2508	
From rank 9 to: 	0	0	0	0	0	0	0	0	4659	0	2185	7438	0	0	0	3950	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	4315	2163	0	5952	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	754	0	3866	
From rank 11 to: 	0	0	0	0	0	0	0	0	710	7487	5883	0	0	0	3458	3014	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1809	
From rank 12 to: 	0	0	0	0	2346	0	0	2714	0	0	0	0	0	6251	3923	3471	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	1647	1112	0	2699	0	0	0	0	6225	0	3100	2082	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	3485	3969	3055	0	8462	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	3927	0	3004	3452	2110	8475	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	458	0	0	0	0	0	0	0	0	6280	4567	159	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	1529	0	0	0	0	0	0	0	6242	0	1571	5387	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4578	1560	0	6812	1241	0	3656	68	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	151	5385	6853	0	262	0	973	4282	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1241	296	0	6838	4067	5175	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6777	0	2409	3067	4584	0	5436	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3675	976	4073	2410	0	6957	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	68	4285	5161	3061	7024	0	0	0	491	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4592	0	0	0	4570	5159	2028	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4557	0	1556	6693	6235	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5463	0	485	5150	1553	0	4989	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2044	6732	5032	0	4136	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6240	0	4088	0	7515	4329	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	1798	0	752	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	7564	0	4746	2061	
From rank 30 to: 	0	0	0	0	0	0	0	0	2623	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4338	4747	0	7601	
From rank 31 to: 	0	0	0	0	0	0	0	0	2533	0	3876	1805	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2091	7637	0	

Edge-cut for node partition: 15350



After loadbalancing process 11 has 330431 cells.
After loadbalancing process 20 has 330249 cells.
After loadbalancing process 6 has 330840 cells.
After loadbalancing process 9 has 331528 cells.
After loadbalancing process 10 has 330126 cells.
After loadbalancing process 8 has 328836 cells.
After loadbalancing process 21 has 330860 cells.
After loadbalancing process 7 has 332839 cells.
After loadbalancing process 23 has 330499 cells.
After loadbalancing process 22 has 330440 cells.
After loadbalancing process 4 has 330365 cells.
After loadbalancing process 5 has 335022 cells.
After loadbalancing process 17 has 326462 cells.
After loadbalancing process 16 has 323390 cells.
After loadbalancing process 3 has 330129 cells.
After loadbalancing process 19 has 328152 cells.
After loadbalancing process 18 has 328315 cells.
After loadbalancing process 1 has 326953 cells.
After loadbalancing process 2 has 330139 cells.
After loadbalancing process 25 has 331641 cells.
After loadbalancing process 0 has 323688 cells.
After loadbalancing process 27 has 335770 cells.
After loadbalancing process 29 has 330272 cells.
After loadbalancing process 15 has 330428 cells.
After loadbalancing process 12 has 334659 cells.
After loadbalancing process 28 has 332112 cells.
After loadbalancing process 24 has 331953 cells.
After loadbalancing process 13 has 329408 cells.
After loadbalancing process 30 has 332562 cells.
After loadbalancing process 26 has 330459 cells.
After loadbalancing process 31 has 334191 cells.
After loadbalancing process 14 has 331794 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          141.676         0.216154
    2          81.8737         0.577894
    3          58.2884         0.711931
    4           49.837         0.855007
    5          46.8687          0.94044
    6          37.8602         0.807793
    7           31.423         0.829975
    8          31.0536         0.988242
    9          27.4552         0.884122
   10          23.4483         0.854059
   11          23.6102           1.0069
   12          21.4113         0.906866
   13          18.4623         0.862272
   14          18.8672          1.02193
   15          17.5056         0.927834
   16          15.2322         0.870133
   17          15.5043          1.01786
   18          14.8957         0.960746
   19          13.1233         0.881011
   20          13.3095          1.01419
   21          12.8326         0.964166
   22          11.6154         0.905144
   23          11.8143          1.01713
   24          11.2171         0.949451
   25          10.4022          0.92735
   26          10.6619          1.02497
   27          9.90622         0.929121
   28          9.40387         0.949289
   29           9.6917          1.03061
   30           9.0833         0.937224
   31          9.29323          1.02311
   32          10.4569          1.12521
   33          11.1717          1.06836
   34          12.5178          1.12049
   35          12.3777         0.988808
   36          9.68968         0.782835
   37          7.62746         0.787174
   38          6.36822         0.834908
   39          5.60925         0.880819
   40          4.99964         0.891319
   41           4.1846         0.836981
   42          3.82953         0.915149
   43          3.55713         0.928868
   44          2.88372         0.810686
   45           2.5605         0.887917
   46          2.34385         0.915389
   47          1.90775         0.813938
   48          1.75618         0.920547
   49          1.59504         0.908246
   50          1.36198         0.853883
   51          1.33237         0.978265
   52          1.20203         0.902172
   53          1.09147         0.908018
   54          1.06538         0.976104
   55         0.951865         0.893447
   56          0.91443         0.960672
   57         0.869047         0.950371
   58         0.823305         0.947366
   59         0.798858         0.970306
   60         0.776021         0.971412
   61          0.76054         0.980051
   62         0.725679         0.954163
   63         0.735349          1.01333
   64          0.67996         0.924677
   65         0.667509         0.981688
   66         0.618566         0.926678
   67         0.551339         0.891317
   68         0.490555         0.889752
   69         0.411086         0.838003
   70         0.340969         0.829435
   71         0.284674         0.834895
   72         0.245533         0.862507
   73         0.221841         0.903507
   74         0.228557          1.03028
   75         0.229875          1.00577
   76         0.242863           1.0565
   77         0.236648         0.974409
   78         0.225402         0.952478
   79         0.213418         0.946833
   80         0.200731         0.940553
   81         0.177682         0.885174
   82          0.16413          0.92373
   83         0.145534         0.886702
   84         0.135165         0.928754
   85         0.126103         0.932951
   86         0.120383         0.954644
   87          0.11563         0.960513
   88          0.11169         0.965928
   89         0.104898         0.939193
   90         0.103178         0.983603
   91        0.0957665         0.928164
   92        0.0908888         0.949067
   93        0.0857203         0.943134
   94        0.0790739         0.922464
   95        0.0721807         0.912826
   96         0.067022          0.92853
   97        0.0594819         0.887499
=== rate=0.908507, T=74.2977, TIT=0.765956, IT=97

 Elapsed time: 74.2977
Rank 0: Matrix-vector product took 0.264681 seconds
Rank 1: Matrix-vector product took 0.266701 seconds
Rank 2: Matrix-vector product took 0.270457 seconds
Rank 3: Matrix-vector product took 0.270789 seconds
Rank 4: Matrix-vector product took 0.270016 seconds
Rank 5: Matrix-vector product took 0.273458 seconds
Rank 6: Matrix-vector product took 0.273146 seconds
Rank 7: Matrix-vector product took 0.273338 seconds
Rank 8: Matrix-vector product took 0.269004 seconds
Rank 9: Matrix-vector product took 0.270964 seconds
Rank 10: Matrix-vector product took 0.269512 seconds
Rank 11: Matrix-vector product took 0.269732 seconds
Rank 12: Matrix-vector product took 0.271871 seconds
Rank 13: Matrix-vector product took 0.269551 seconds
Rank 14: Matrix-vector product took 0.270914 seconds
Rank 15: Matrix-vector product took 0.270294 seconds
Rank 16: Matrix-vector product took 0.265661 seconds
Rank 17: Matrix-vector product took 0.267208 seconds
Rank 18: Matrix-vector product took 0.274149 seconds
Rank 19: Matrix-vector product took 0.267999 seconds
Rank 20: Matrix-vector product took 0.271719 seconds
Rank 21: Matrix-vector product took 0.27034 seconds
Rank 22: Matrix-vector product took 0.271812 seconds
Rank 23: Matrix-vector product took 0.275222 seconds
Rank 24: Matrix-vector product took 0.270987 seconds
Rank 25: Matrix-vector product took 0.270918 seconds
Rank 26: Matrix-vector product took 0.27008 seconds
Rank 27: Matrix-vector product took 0.279679 seconds
Rank 28: Matrix-vector product took 0.271539 seconds
Rank 29: Matrix-vector product took 0.270297 seconds
Rank 30: Matrix-vector product took 0.27269 seconds
Rank 31: Matrix-vector product took 0.278175 seconds
Average time for Matrix-vector product is 0.271028 seconds

Rank 0: copyOwnerToAll took 0.00167446 seconds
Rank 1: copyOwnerToAll took 0.00167446 seconds
Rank 2: copyOwnerToAll took 0.00167446 seconds
Rank 3: copyOwnerToAll took 0.00167446 seconds
Rank 4: copyOwnerToAll took 0.00167446 seconds
Rank 5: copyOwnerToAll took 0.00167446 seconds
Rank 6: copyOwnerToAll took 0.00167446 seconds
Rank 7: copyOwnerToAll took 0.00167446 seconds
Rank 8: copyOwnerToAll took 0.00167446 seconds
Rank 9: copyOwnerToAll took 0.00167446 seconds
Rank 10: copyOwnerToAll took 0.00167446 seconds
Rank 11: copyOwnerToAll took 0.00167446 seconds
Rank 12: copyOwnerToAll took 0.00167446 seconds
Rank 13: copyOwnerToAll took 0.00167446 seconds
Rank 14: copyOwnerToAll took 0.00167446 seconds
Rank 15: copyOwnerToAll took 0.00167446 seconds
Rank 16: copyOwnerToAll took 0.00167446 seconds
Rank 17: copyOwnerToAll took 0.00167446 seconds
Rank 18: copyOwnerToAll took 0.00167446 seconds
Rank 19: copyOwnerToAll took 0.00167446 seconds
Rank 20: copyOwnerToAll took 0.00167446 seconds
Rank 21: copyOwnerToAll took 0.00167446 seconds
Rank 22: copyOwnerToAll took 0.00167446 seconds
Rank 23: copyOwnerToAll took 0.00167446 seconds
Rank 24: copyOwnerToAll took 0.00167446 seconds
Rank 25: copyOwnerToAll took 0.00167446 seconds
Rank 26: copyOwnerToAll took 0.00167446 seconds
Rank 27: copyOwnerToAll took 0.00167446 seconds
Rank 28: copyOwnerToAll took 0.00167446 seconds
Rank 29: copyOwnerToAll took 0.00167446 seconds
Rank 30: copyOwnerToAll took 0.00167446 seconds
Rank 31: copyOwnerToAll took 0.00167446 seconds
Average time for copyOwnertoAll is 0.0017011 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	6280	4567	159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	458	0	0	0	0	0	0	0	
Rank 1's ghost cells:	6242	0	1571	5387	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1529	0	0	0	0	0	0	0	
Rank 2's ghost cells:	4578	1560	0	6812	1241	0	3656	68	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	151	5385	6853	0	262	0	973	4282	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	1241	296	0	6838	4067	5175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	6777	0	2409	3067	4584	0	5436	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	3675	976	4073	2410	0	6957	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	68	4285	5161	3061	7024	0	0	0	491	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	4592	0	0	0	4570	5159	2028	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	4557	0	1556	6693	6235	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	5463	0	485	5150	1553	0	4989	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	2044	6732	5032	0	4136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	6240	0	4088	0	7515	4329	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	7564	0	4746	2061	0	0	0	0	0	0	0	0	1798	0	752	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	4338	4747	0	7601	0	0	0	0	0	0	0	0	2623	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	2091	7637	0	0	0	0	0	0	0	0	0	2533	0	3876	1805	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	7812	3851	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	7771	0	2080	4883	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3849	2031	0	6011	0	2711	2202	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4883	5973	0	1049	1651	3085	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1053	0	6022	2679	4895	0	0	0	0	2353	1627	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2735	1651	6011	0	4965	2711	0	0	0	0	0	1168	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2203	3041	2679	4985	0	5913	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4897	2703	5888	0	0	0	0	0	2709	2683	0	0	
Rank 24's ghost cells:	458	1563	0	0	0	0	0	0	0	0	0	0	0	1799	2570	2508	0	0	0	0	0	0	0	0	0	4658	4311	677	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4659	0	2185	7438	0	0	0	3950	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	754	0	3866	0	0	0	0	0	0	0	0	4315	2163	0	5952	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1809	0	0	0	0	0	0	0	0	710	7487	5883	0	0	0	3458	3014	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2346	0	0	2714	0	0	0	0	0	6251	3923	3471	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1647	1112	0	2699	0	0	0	0	6225	0	3100	2082	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3485	3969	3055	0	8462	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3927	0	3004	3452	2110	8475	0	
