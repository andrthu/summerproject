Number of cores/ranks per node is: 8
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 62745
Cell on rank 1 before loadbalancing: 62744
Cell on rank 2 before loadbalancing: 62376
Cell on rank 3 before loadbalancing: 62376
Cell on rank 4 before loadbalancing: 62617
Cell on rank 5 before loadbalancing: 62617
Cell on rank 6 before loadbalancing: 61564
Cell on rank 7 before loadbalancing: 62961
Cell on rank 8 before loadbalancing: 62494
Cell on rank 9 before loadbalancing: 62553
Cell on rank 10 before loadbalancing: 62665
Cell on rank 11 before loadbalancing: 62665
Cell on rank 12 before loadbalancing: 62406
Cell on rank 13 before loadbalancing: 62406
Cell on rank 14 before loadbalancing: 62406
Cell on rank 15 before loadbalancing: 62405


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2187	44	0	0	1474	0	0	0	0	1012	109	0	0	0	0	
From rank 1 to: 	2192	0	1891	1234	135	836	215	0	0	0	411	148	632	84	0	1	
From rank 2 to: 	44	1886	0	2164	0	0	0	0	0	0	0	0	197	865	0	0	
From rank 3 to: 	0	1252	2142	0	0	87	1136	1567	0	0	21	0	0	1105	0	177	
From rank 4 to: 	0	136	0	0	0	2623	2208	0	573	349	0	0	0	0	0	0	
From rank 5 to: 	1461	838	0	87	2642	0	388	0	230	996	393	0	0	0	0	0	
From rank 6 to: 	0	214	0	1111	2208	392	0	2105	446	0	4	0	0	0	402	385	
From rank 7 to: 	0	0	0	1570	0	0	2104	0	0	0	0	0	0	867	564	29	
From rank 8 to: 	0	0	0	0	573	230	446	0	0	2627	535	816	0	0	1468	954	
From rank 9 to: 	0	0	0	0	349	996	0	0	2626	0	941	398	0	0	0	0	
From rank 10 to: 	1012	411	0	21	0	393	4	0	529	932	0	3062	69	240	0	243	
From rank 11 to: 	109	148	0	0	0	0	0	0	799	417	3080	0	1720	153	0	688	
From rank 12 to: 	0	632	197	0	0	0	0	0	0	0	75	1724	0	2166	0	1010	
From rank 13 to: 	0	84	865	1105	0	0	0	867	0	0	239	159	2174	0	991	2269	
From rank 14 to: 	0	0	0	0	0	0	402	564	1481	0	0	0	0	952	0	1979	
From rank 15 to: 	0	1	0	177	0	0	385	29	974	0	243	653	1010	2253	1976	0	

Edge-cut for node partition: 10000



After loadbalancing process 4 has 68506 cells.
After loadbalancing process 8 has 70143 cells.
After loadbalancing process 7 has 68095 cells.
After loadbalancing process 10 has 69581 cells.
After loadbalancing process 12 has 68210 cells.
After loadbalancing process 6 has 68831 cells.
After loadbalancing process 0 has 67571 cells.
After loadbalancing process 11 has 69779 cells.
After loadbalancing process 14 has 67784 cells.
After loadbalancing process 3 has 69863 cells.
After loadbalancing process 13 has 71159 cells.
After loadbalancing process 2 has 67532 cells.
After loadbalancing process 9 has 67863 cells.
After loadbalancing process 5 has 69652 cells.
After loadbalancing process 1 has 70523 cells.
After loadbalancing process 15 has 70106 cells.


=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.5534         0.218388
    2          31.6682           0.5805
    3          22.5083         0.710754
    4          19.3268         0.858652
    5          17.7034         0.916001
    6          14.1972          0.80195
    7           12.158         0.856364
    8          11.8165         0.971907
    9          10.0789         0.852954
   10          8.80176         0.873287
   11          8.78625         0.998237
   12          7.78595         0.886152
   13          6.76919         0.869411
   14          6.86368          1.01396
   15          6.39387         0.931552
   16          5.57916         0.872579
   17          5.62853          1.00885
   18          5.32688         0.946407
   19          4.71692         0.885495
   20          4.70287         0.997021
   21          4.44285          0.94471
   22          4.05317         0.912291
   23          4.07389          1.00511
   24          3.81085         0.935433
   25          3.50302         0.919224
   26          3.51297          1.00284
   27          3.27347         0.931825
   28          3.04435         0.930006
   29          3.09048          1.01515
   30          2.89481         0.936686
   31          2.78585         0.962362
   32          2.91599          1.04671
   33          2.91055         0.998136
   34          3.04532           1.0463
   35          3.10158          1.01847
   36          2.63698         0.850207
   37          2.16053          0.81932
   38          1.79077         0.828854
   39           1.4609         0.815797
   40          1.29854         0.888865
   41          1.17775         0.906979
   42          1.02226         0.867972
   43         0.875221         0.856166
   44         0.724127         0.827365
   45          0.62776          0.86692
   46          0.55219         0.879619
   47          0.45201         0.818576
   48         0.381677           0.8444
   49         0.320974         0.840958
   50         0.260657         0.812081
   51         0.223226         0.856397
   52          0.18994         0.850888
   53         0.160472         0.844856
   54          0.13788         0.859212
   55         0.114629         0.831373
   56        0.0945294         0.824652
   57        0.0781393         0.826613
   58        0.0641051         0.820396
   59        0.0520893         0.812561
   60        0.0428569         0.822758
   61        0.0343069           0.8005
   62        0.0266015         0.775396
   63        0.0213383         0.802148
=== rate=0.86183, T=10.1014, TIT=0.16034, IT=63

 Elapsed time: 10.1014
Rank 0: Matrix-vector product took 0.0549179 seconds
Rank 1: Matrix-vector product took 0.0570899 seconds
Rank 2: Matrix-vector product took 0.0546881 seconds
Rank 3: Matrix-vector product took 0.0570707 seconds
Rank 4: Matrix-vector product took 0.0554732 seconds
Rank 5: Matrix-vector product took 0.0574125 seconds
Rank 6: Matrix-vector product took 0.0568847 seconds
Rank 7: Matrix-vector product took 0.0558277 seconds
Rank 8: Matrix-vector product took 0.0577922 seconds
Rank 9: Matrix-vector product took 0.0550456 seconds
Rank 10: Matrix-vector product took 0.0564858 seconds
Rank 11: Matrix-vector product took 0.0563066 seconds
Rank 12: Matrix-vector product took 0.055225 seconds
Rank 13: Matrix-vector product took 0.0573893 seconds
Rank 14: Matrix-vector product took 0.0547734 seconds
Rank 15: Matrix-vector product took 0.0564528 seconds
Average time for Matrix-vector product is 0.0561772 seconds

Rank 0: copyOwnerToAll took 0.000653931 seconds
Rank 1: copyOwnerToAll took 0.000653931 seconds
Rank 2: copyOwnerToAll took 0.000653931 seconds
Rank 3: copyOwnerToAll took 0.000653931 seconds
Rank 4: copyOwnerToAll took 0.000653931 seconds
Rank 5: copyOwnerToAll took 0.000653931 seconds
Rank 6: copyOwnerToAll took 0.000653931 seconds
Rank 7: copyOwnerToAll took 0.000653931 seconds
Rank 8: copyOwnerToAll took 0.000653931 seconds
Rank 9: copyOwnerToAll took 0.000653931 seconds
Rank 10: copyOwnerToAll took 0.000653931 seconds
Rank 11: copyOwnerToAll took 0.000653931 seconds
Rank 12: copyOwnerToAll took 0.000653931 seconds
Rank 13: copyOwnerToAll took 0.000653931 seconds
Rank 14: copyOwnerToAll took 0.000653931 seconds
Rank 15: copyOwnerToAll took 0.000653931 seconds
Average time for copyOwnertoAll is 0.000671754 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2187	44	0	0	1474	0	0	0	0	1012	109	0	0	0	0	
Rank 1's ghost cells:	2192	0	1891	1234	135	836	215	0	0	0	411	148	632	84	0	1	
Rank 2's ghost cells:	44	1886	0	2164	0	0	0	0	0	0	0	0	197	865	0	0	
Rank 3's ghost cells:	0	1252	2142	0	0	87	1136	1567	0	0	21	0	0	1105	0	177	
Rank 4's ghost cells:	0	136	0	0	0	2623	2208	0	573	349	0	0	0	0	0	0	
Rank 5's ghost cells:	1461	838	0	87	2642	0	388	0	230	996	393	0	0	0	0	0	
Rank 6's ghost cells:	0	214	0	1111	2208	392	0	2105	446	0	4	0	0	0	402	385	
Rank 7's ghost cells:	0	0	0	1570	0	0	2104	0	0	0	0	0	0	867	564	29	
Rank 8's ghost cells:	0	0	0	0	573	230	446	0	0	2627	535	816	0	0	1468	954	
Rank 9's ghost cells:	0	0	0	0	349	996	0	0	2626	0	941	398	0	0	0	0	
Rank 10's ghost cells:	1012	411	0	21	0	393	4	0	529	932	0	3062	69	240	0	243	
Rank 11's ghost cells:	109	148	0	0	0	0	0	0	799	417	3080	0	1720	153	0	688	
Rank 12's ghost cells:	0	632	197	0	0	0	0	0	0	0	75	1724	0	2166	0	1010	
Rank 13's ghost cells:	0	84	865	1105	0	0	0	867	0	0	239	159	2174	0	991	2269	
Rank 14's ghost cells:	0	0	0	0	0	0	402	564	1481	0	0	0	0	952	0	1979	
Rank 15's ghost cells:	0	1	0	177	0	0	385	29	974	0	243	653	1010	2253	1976	0	
