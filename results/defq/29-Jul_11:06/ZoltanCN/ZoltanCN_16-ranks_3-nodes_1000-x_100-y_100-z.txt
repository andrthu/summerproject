Number of cores/ranks per node is: 5
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 625002
Cell on rank 1 before loadbalancing: 624999
Cell on rank 2 before loadbalancing: 625001
Cell on rank 3 before loadbalancing: 625000
Cell on rank 4 before loadbalancing: 625000
Cell on rank 5 before loadbalancing: 625001
Cell on rank 6 before loadbalancing: 625001
Cell on rank 7 before loadbalancing: 625000
Cell on rank 8 before loadbalancing: 625015
Cell on rank 9 before loadbalancing: 625012
Cell on rank 10 before loadbalancing: 624986
Cell on rank 11 before loadbalancing: 624986
Cell on rank 12 before loadbalancing: 624999
Cell on rank 13 before loadbalancing: 624999
Cell on rank 14 before loadbalancing: 625000
Cell on rank 15 before loadbalancing: 624999


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	14264	3955	0	7505	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	14296	0	7199	0	2811	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	3956	7192	0	10734	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	10734	0	0	0	0	0	0	0	0	0	0	11165	0	0	
From rank 4 to: 	7498	2791	0	0	0	10687	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	10689	0	12280	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	12280	0	10511	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	0	10499	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	10384	10610	0	0	0	0	336	
From rank 9 to: 	0	0	0	0	0	0	0	0	10411	0	0	0	0	0	0	13201	
From rank 10 to: 	0	0	0	0	0	0	0	0	10611	0	0	10744	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	10742	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	10634	12744	0	
From rank 13 to: 	0	0	0	11172	0	0	0	0	0	0	0	0	10633	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	12757	0	0	10261	
From rank 15 to: 	0	0	0	0	0	0	0	0	336	13193	0	0	0	0	10264	0	

Edge-cut for node partition: 21413



After loadbalancing process 3 has 635728 cells.
After loadbalancing process 15 has 635499 cells.
After loadbalancing process 2 has 646341 cells.
After loadbalancing process 12 has 645976 cells.
After loadbalancing process 9 has 648018 cells.
After loadbalancing process 7 has 648377 cells.
After loadbalancing process 0 has 646345 cells.
After loadbalancing process 14 has 647792 cells.
After loadbalancing process 6 has 646899 cells.
After loadbalancing process 11 has 649305 cells.
After loadbalancing process 13 has 647970 cells.
After loadbalancing process 5 has 646883 cells.
After loadbalancing process 4 has 648792 cells.
After loadbalancing process 8 has 646804 cells.
After loadbalancing process 1 has 648624 cells.
After loadbalancing process 10 has 650726 cells.


=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          139.773         0.213251
    2          80.4542         0.575606
    3          56.8922         0.707137
    4          47.2139         0.829885
    5          44.7386         0.947572
    6          38.2243         0.854391
    7          30.6781         0.802583
    8          29.7547         0.969899
    9          27.7193         0.931592
   10          22.9654         0.828499
   11          22.4359         0.976943
   12          21.6113         0.963247
   13          18.1771         0.841091
   14          18.0345         0.992159
   15           17.728         0.983003
   16          15.0876         0.851059
   17          14.9832         0.993079
   18          15.0213          1.00255
   19          13.0406         0.868138
   20          13.0248         0.998795
   21          12.8615         0.987459
   22          11.4468         0.890007
   23          11.5411          1.00824
   24          11.1669         0.967578
   25          10.1581         0.909662
   26          10.3823          1.02207
   27           9.8631          0.94999
   28          9.22634          0.93544
   29          9.61775          1.04242
   30          9.16513         0.952939
   31          9.29654          1.01434
   32          10.7938          1.16106
   33          11.7221            1.086
   34           12.655          1.07959
   35          11.8752         0.938375
   36          8.51604          0.71713
   37           6.4482         0.757183
   38          5.68324         0.881368
   39          5.04568         0.887818
   40          4.35571         0.863256
   41          3.68026         0.844928
   42          3.46152         0.940564
   43          3.14074          0.90733
   44          2.49379         0.794013
   45          2.31084         0.926638
   46          2.12748         0.920651
   47          1.67949         0.789425
   48          1.57747         0.939255
   49          1.43344         0.908701
   50          1.22073         0.851604
   51          1.21143         0.992381
   52           1.0746         0.887058
   53          1.00397         0.934266
   54          0.96511         0.961296
   55         0.875958         0.907625
   56         0.861272         0.983235
   57         0.789628         0.916816
   58         0.803311          1.01733
   59         0.747492         0.930513
   60         0.740797         0.991044
   61         0.740155         0.999134
   62         0.675716         0.912939
   63         0.703414          1.04099
   64         0.626605         0.890806
   65         0.603569         0.963237
   66         0.535494         0.887212
   67         0.471554         0.880597
   68         0.389432         0.825849
   69         0.329498         0.846099
   70         0.265487          0.80573
   71         0.228181         0.859483
   72         0.213518          0.93574
   73         0.209573         0.981524
   74         0.232688          1.11029
   75         0.240094          1.03183
   76         0.234472         0.976582
   77         0.217688         0.928419
   78         0.207685         0.954052
   79         0.189398         0.911946
   80         0.176014         0.929337
   81         0.149372         0.848636
   82         0.137454         0.920209
   83         0.128797         0.937018
   84         0.120489         0.935501
   85         0.115766         0.960802
   86         0.114847         0.992061
   87         0.105142         0.915496
   88         0.100247         0.953446
   89        0.0982783         0.980357
   90         0.088916         0.904737
   91        0.0830502          0.93403
   92        0.0785709         0.946066
   93        0.0719528         0.915769
   94        0.0644311         0.895463
=== rate=0.9065, T=137.624, TIT=1.46408, IT=94

 Elapsed time: 137.624
Rank 0: Matrix-vector product took 0.530524 seconds
Rank 1: Matrix-vector product took 0.541879 seconds
Rank 2: Matrix-vector product took 0.530405 seconds
Rank 3: Matrix-vector product took 0.522962 seconds
Rank 4: Matrix-vector product took 0.532362 seconds
Rank 5: Matrix-vector product took 0.529346 seconds
Rank 6: Matrix-vector product took 0.530351 seconds
Rank 7: Matrix-vector product took 0.541156 seconds
Rank 8: Matrix-vector product took 0.530862 seconds
Rank 9: Matrix-vector product took 0.534175 seconds
Rank 10: Matrix-vector product took 0.535425 seconds
Rank 11: Matrix-vector product took 0.532798 seconds
Rank 12: Matrix-vector product took 0.530185 seconds
Rank 13: Matrix-vector product took 0.535754 seconds
Rank 14: Matrix-vector product took 0.529335 seconds
Rank 15: Matrix-vector product took 0.52285 seconds
Average time for Matrix-vector product is 0.531898 seconds

Rank 0: copyOwnerToAll took 0.00221383 seconds
Rank 1: copyOwnerToAll took 0.00221383 seconds
Rank 2: copyOwnerToAll took 0.00221383 seconds
Rank 3: copyOwnerToAll took 0.00221383 seconds
Rank 4: copyOwnerToAll took 0.00221383 seconds
Rank 5: copyOwnerToAll took 0.00221383 seconds
Rank 6: copyOwnerToAll took 0.00221383 seconds
Rank 7: copyOwnerToAll took 0.00221383 seconds
Rank 8: copyOwnerToAll took 0.00221383 seconds
Rank 9: copyOwnerToAll took 0.00221383 seconds
Rank 10: copyOwnerToAll took 0.00221383 seconds
Rank 11: copyOwnerToAll took 0.00221383 seconds
Rank 12: copyOwnerToAll took 0.00221383 seconds
Rank 13: copyOwnerToAll took 0.00221383 seconds
Rank 14: copyOwnerToAll took 0.00221383 seconds
Rank 15: copyOwnerToAll took 0.00221383 seconds
Average time for copyOwnertoAll is 0.00224737 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	10384	10610	0	336	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	10411	0	0	0	13201	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	10611	0	0	10744	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	10742	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	336	13193	0	0	0	0	0	0	0	10264	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	0	0	10734	0	0	0	3956	7192	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	10734	0	0	11165	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	0	0	10634	12744	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	11172	10633	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	10261	0	0	12757	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	3955	0	0	0	0	0	14264	7505	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	7199	0	0	0	0	14296	0	2811	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	7498	2791	0	10687	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	10689	0	12280	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	12280	0	10511	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10499	0	
