Number of cores/ranks per node is: 8
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 25009
Cell on rank 1 before loadbalancing: 25005
Cell on rank 2 before loadbalancing: 24980
Cell on rank 3 before loadbalancing: 25006
Edge-cut: 2729
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	270	110	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	40	0	0	0	0	120	0	
From rank 1 to: 	270	0	140	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	
From rank 2 to: 	110	150	0	220	30	80	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	20	220	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	40	0	0	230	190	110	10	145	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	130	0	80	0	240	0	0	0	126	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	
From rank 6 to: 	0	0	0	0	190	0	0	150	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	100	140	120	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	10	126	0	0	0	180	0	0	0	0	0	100	0	0	190	0	0	0	0	0	0	0	0	0	0	0	84	0	
From rank 9 to: 	0	0	0	0	145	0	0	0	180	0	156	114	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	50	0	150	0	0	156	0	197	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	114	207	0	140	103	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	140	0	180	170	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	70	93	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	140	90	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	100	130	0	100	90	0	140	0	0	65	100	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	280	0	0	222	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	65	280	0	110	170	32	0	46	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	190	0	0	0	0	0	0	110	0	110	0	180	0	0	0	0	0	0	0	0	0	0	30	100	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	180	0	0	0	50	100	0	0	0	0	90	0	0	80	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	222	32	0	0	0	220	230	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	80	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	50	220	80	0	230	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	230	0	0	0	0	0	100	150	0	0	
From rank 24 to: 	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	195	75	74	0	76	110	114	
From rank 25 to: 	40	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	205	0	175	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	166	0	180	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	0	180	0	0	246	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	110	0	0	0	0	0	266	0	224	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	76	0	0	236	256	0	0	60	
From rank 30 to: 	120	0	0	0	0	110	0	0	80	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	120	0	0	0	0	0	0	220	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	80	0	0	0	0	120	0	0	0	214	60	220	0	
loadb
After loadbalancing process 16 has 3726 cells.
After loadbalancing process 21 has 3420 cells.
After loadbalancing process 20 has 3838 cells.
After loadbalancing process 22 has 3728 cells.
After loadbalancing process 12 has 3681 cells.
After loadbalancing process 3 has 3480 cells.
After loadbalancing process 14 has 3645 cells.
After loadbalancing process 13 has 3438 cells.
After loadbalancing process 26 has 3566 cells.
After loadbalancing process 17 has 3964 cells.
After loadbalancing process 11 has 3758 cells.
After loadbalancing process 1 has 3662 cells.
After loadbalancing process 19 has 3780 cells.
After loadbalancing process 0 has 3906 cells.
After loadbalancing process 6 has 3600 cells.
After loadbalancing process 27 has 3620 cells.
After loadbalancing process 15 has 3881 cells.
After loadbalancing process 25 has 3620 cells.
After loadbalancing process 24 has 3891 cells.
After loadbalancing process 28 has 3800 cells.
After loadbalancing process 29 has 3892 cells.
After loadbalancing process 5 has 3830 cells.
After loadbalancing process 7 has 3630 cells.
After loadbalancing process 30 has 3816 cells.
After loadbalancing process 31 has 3898 cells.
After loadbalancing process 9 has 3878 cells.
After loadbalancing process 8 has 3840 cells.
After loadbalancing process 2 has 3822 cells.
After loadbalancing process 23 has 3684 cells.
After loadbalancing process 18 has 3870 cells.
After loadbalancing process 10 has 3769 cells.
After loadbalancing process 4 has 3890 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	270	110	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	40	0	0	0	0	120	0	
Rank 1's ghost cells:	270	0	140	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	
Rank 2's ghost cells:	110	150	0	220	30	80	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	20	220	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	40	0	0	230	190	110	10	145	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	130	0	80	0	240	0	0	0	126	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	
Rank 6's ghost cells:	0	0	0	0	190	0	0	150	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	100	140	120	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	10	126	0	0	0	180	0	0	0	0	0	100	0	0	190	0	0	0	0	0	0	0	0	0	0	0	84	0	
Rank 9's ghost cells:	0	0	0	0	145	0	0	0	180	0	156	114	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	50	0	150	0	0	156	0	197	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	114	207	0	140	103	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	140	0	180	170	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	70	93	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	140	90	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	100	130	0	100	90	0	140	0	0	65	100	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	280	0	0	222	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	65	280	0	110	170	32	0	46	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	190	0	0	0	0	0	0	110	0	110	0	180	0	0	0	0	0	0	0	0	0	0	30	100	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	180	0	0	0	50	100	0	0	0	0	90	0	0	80	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	222	32	0	0	0	220	230	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	80	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	50	220	80	0	230	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	230	0	0	0	0	0	100	150	0	0	
Rank 24's ghost cells:	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	195	75	74	0	76	110	114	
Rank 25's ghost cells:	40	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	205	0	175	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	166	0	180	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	74	0	180	0	0	246	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	110	0	0	0	0	0	266	0	224	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	76	0	0	236	256	0	0	60	
Rank 30's ghost cells:	120	0	0	0	0	110	0	0	80	0	0	0	0	0	0	0	0	0	30	0	0	0	0	0	120	0	0	0	0	0	0	220	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	80	0	0	0	0	120	0	0	0	214	60	220	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.4018         0.239637
    2          23.3273         0.607452
    3          21.5711         0.924716
    4            19.56         0.906767
    5          9.73735         0.497821
    6          5.24054         0.538189
    7           3.8709         0.738645
    8          2.41632         0.624227
    9          1.21422         0.502508
   10          0.85371         0.703093
   11         0.609044         0.713408
   12          0.31059         0.509964
   13         0.234536         0.755131
   14         0.154116          0.65711
   15        0.0869142         0.563953
   16        0.0593132         0.682434
   17        0.0384099         0.647578
   18        0.0235758         0.613796
   19        0.0162678         0.690022
   20       0.00929677         0.571481
=== rate=0.614012, T=0.203247, TIT=0.0101624, IT=20

 Elapsed time: 0.203247
Rank 0: Matrix-vector product took 0.00306761 seconds
Rank 1: Matrix-vector product took 0.00291148 seconds
Rank 2: Matrix-vector product took 0.0030473 seconds
Rank 3: Matrix-vector product took 0.00276961 seconds
Rank 4: Matrix-vector product took 0.00301987 seconds
Rank 5: Matrix-vector product took 0.00305159 seconds
Rank 6: Matrix-vector product took 0.00284816 seconds
Rank 7: Matrix-vector product took 0.00289273 seconds
Rank 8: Matrix-vector product took 0.00306828 seconds
Rank 9: Matrix-vector product took 0.00306343 seconds
Rank 10: Matrix-vector product took 0.00300163 seconds
Rank 11: Matrix-vector product took 0.00293955 seconds
Rank 12: Matrix-vector product took 0.0029368 seconds
Rank 13: Matrix-vector product took 0.00270334 seconds
Rank 14: Matrix-vector product took 0.00290938 seconds
Rank 15: Matrix-vector product took 0.00309551 seconds
Rank 16: Matrix-vector product took 0.00295334 seconds
Rank 17: Matrix-vector product took 0.00314373 seconds
Rank 18: Matrix-vector product took 0.00308606 seconds
Rank 19: Matrix-vector product took 0.00300643 seconds
Rank 20: Matrix-vector product took 0.00305054 seconds
Rank 21: Matrix-vector product took 0.00270585 seconds
Rank 22: Matrix-vector product took 0.00296755 seconds
Rank 23: Matrix-vector product took 0.00293659 seconds
Rank 24: Matrix-vector product took 0.00309815 seconds
Rank 25: Matrix-vector product took 0.00287733 seconds
Rank 26: Matrix-vector product took 0.00285504 seconds
Rank 27: Matrix-vector product took 0.00290292 seconds
Rank 28: Matrix-vector product took 0.00303042 seconds
Rank 29: Matrix-vector product took 0.00301665 seconds
Rank 30: Matrix-vector product took 0.0030623 seconds
Rank 31: Matrix-vector product took 0.00312035 seconds
Average time for Matrix-vector product is 0.00297311 seconds

Rank 0: copyOwnerToAll took 0.00018122 seconds
Rank 1: copyOwnerToAll took 0.00015286 seconds
Rank 2: copyOwnerToAll took 8.967e-05 seconds
Rank 3: copyOwnerToAll took 7.482e-05 seconds
Rank 4: copyOwnerToAll took 0.00016374 seconds
Rank 5: copyOwnerToAll took 0.00018679 seconds
Rank 6: copyOwnerToAll took 0.00012737 seconds
Rank 7: copyOwnerToAll took 8.371e-05 seconds
Rank 8: copyOwnerToAll took 0.0001813 seconds
Rank 9: copyOwnerToAll took 0.00012868 seconds
Rank 10: copyOwnerToAll took 0.00013383 seconds
Rank 11: copyOwnerToAll took 0.00012823 seconds
Rank 12: copyOwnerToAll took 8.788e-05 seconds
Rank 13: copyOwnerToAll took 6.92e-05 seconds
Rank 14: copyOwnerToAll took 0.00012331 seconds
Rank 15: copyOwnerToAll took 0.00015398 seconds
Rank 16: copyOwnerToAll took 0.0001489 seconds
Rank 17: copyOwnerToAll took 0.00022344 seconds
Rank 18: copyOwnerToAll took 0.00019844 seconds
Rank 19: copyOwnerToAll took 0.00016634 seconds
Rank 20: copyOwnerToAll took 0.00012748 seconds
Rank 21: copyOwnerToAll took 5.402e-05 seconds
Rank 22: copyOwnerToAll took 0.00010114 seconds
Rank 23: copyOwnerToAll took 0.00015429 seconds
Rank 24: copyOwnerToAll took 0.00011338 seconds
Rank 25: copyOwnerToAll took 0.00014889 seconds
Rank 26: copyOwnerToAll took 8.119e-05 seconds
Rank 27: copyOwnerToAll took 6.874e-05 seconds
Rank 28: copyOwnerToAll took 0.00013252 seconds
Rank 29: copyOwnerToAll took 0.00012696 seconds
Rank 30: copyOwnerToAll took 0.000179631 seconds
Rank 31: copyOwnerToAll took 0.00013887 seconds
Average time for copyOwnertoAll is 0.000132213 seconds
