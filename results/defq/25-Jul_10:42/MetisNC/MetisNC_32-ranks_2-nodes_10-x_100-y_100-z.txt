Number of cores/ranks per node is: 16
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	245	241	11	0	0	0	0	0	0	0	0	0	210	0	0	0	0	0	60	0	0	0	0	0	0	60	0	0	0	0	0	
From rank 1 to: 	238	0	18	134	0	230	0	110	0	0	0	0	0	50	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	244	18	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	92	0	0	0	128	0	0	0	0	0	0	0	0	
From rank 3 to: 	11	130	196	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	200	60	0	0	0	0	0	0	0	140	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	230	0	0	200	0	130	140	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	60	140	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	100	0	90	0	140	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	190	30	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	190	0	130	0	0	0	170	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	30	130	0	190	120	0	0	56	0	0	0	0	0	0	0	0	120	50	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	200	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	110	0	0	220	0	120	0	0	0	0	0	0	0	0	80	0	150	0	0	0	0	0	
From rank 13 to: 	210	50	0	0	0	0	0	0	0	0	0	0	220	0	0	160	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	140	0	0	0	0	170	0	0	0	0	0	153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	60	0	0	107	30	0	0	0	134	66	0	120	150	153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	110	0	0	0	0	0	0	0	0	0	190	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	190	0	0	150	0	0	0	0	0	0	70	50	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	190	0	200	70	20	0	0	0	0	0	120	50	0	0	0	
From rank 19 to: 	60	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	122	0	0	40	0	0	130	68	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	132	0	240	160	158	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	20	0	240	0	100	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	100	0	110	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	128	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	160	0	110	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	120	0	80	0	0	0	0	0	0	0	0	0	0	0	0	205	110	80	0	0	0	125	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	50	110	0	0	0	0	0	0	0	0	0	0	0	0	195	0	0	0	0	0	120	40	
From rank 26 to: 	60	0	0	0	0	0	0	0	0	0	0	0	150	100	0	0	0	0	0	130	0	0	0	0	110	0	0	190	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	68	0	0	0	0	90	0	192	0	130	0	0	80	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	70	50	0	0	0	0	0	0	0	0	130	0	100	0	170	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	110	0	110	80	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	120	0	265	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	40	0	80	170	80	265	0	
loadb
After loadbalancing process 16 has 3640 cells.
After loadbalancing process 17 has 3800 cells.
After loadbalancing process 29 has 3490 cells.
After loadbalancing process 21 has 3620 cells.
After loadbalancing process 28 has 3810 cells.
After loadbalancing process 14 has 3575 cells.
After loadbalancing process 6 has 3510 cells.
After loadbalancing process 7 has 3650 cells.
After loadbalancing process 4 has 3623 cells.
After loadbalancing process 18 has 3870 cells.
After loadbalancing process 27 has 3780 cells.
After loadbalancing process 5 has 3845 cells.
After loadbalancing process 30 has 3630 cells.
After loadbalancing process 22 has 3480 cells.
After loadbalancing process 20 has 3880 cells.
After loadbalancing process 8 has 3520 cells.
After loadbalancing process 9 has 3722 cells.
After loadbalancing process 31 has 3865 cells.
After loadbalancing process 24 has 3860 cells.
After loadbalancing process 15 has 3970 cells.
After loadbalancing process 11 has 3610 cells.
After loadbalancing process 25 has 3665 cells.
After loadbalancing process 1 has 3988 cells.
After loadbalancing process 12 has 3830 cells.
After loadbalancing process 13 has 3870 cells.
After loadbalancing process 23 has 3680 cells.
After loadbalancing process 26 has 3836 cells.
After loadbalancing process 19 has 3872 cells.
After loadbalancing process 3 has 3686 cells.
After loadbalancing process 10 has 3844 cells.
After loadbalancing process 2 has 3793 cells.
After loadbalancing process 0 has 3959 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	245	241	11	0	0	0	0	0	0	0	0	0	210	0	0	0	0	0	60	0	0	0	0	0	0	60	0	0	0	0	0	
Rank 1's ghost cells:	238	0	18	134	0	230	0	110	0	0	0	0	0	50	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	244	18	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	92	0	0	0	128	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	11	130	196	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	200	60	0	0	0	0	0	0	0	140	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	230	0	0	200	0	130	140	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	60	140	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	100	0	90	0	140	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	190	30	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	190	0	130	0	0	0	170	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	30	130	0	190	120	0	0	56	0	0	0	0	0	0	0	0	120	50	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	200	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	110	0	0	220	0	120	0	0	0	0	0	0	0	0	80	0	150	0	0	0	0	0	
Rank 13's ghost cells:	210	50	0	0	0	0	0	0	0	0	0	0	220	0	0	160	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	140	0	0	0	0	170	0	0	0	0	0	153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	60	0	0	107	30	0	0	0	134	66	0	120	150	153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	110	0	0	0	0	0	0	0	0	0	190	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	190	0	0	150	0	0	0	0	0	0	70	50	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	190	0	200	70	20	0	0	0	0	0	120	50	0	0	0	
Rank 19's ghost cells:	60	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	122	0	0	40	0	0	130	68	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	132	0	240	160	158	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	20	0	240	0	100	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	100	0	110	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	128	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	160	0	110	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	120	0	80	0	0	0	0	0	0	0	0	0	0	0	0	205	110	80	0	0	0	125	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	50	110	0	0	0	0	0	0	0	0	0	0	0	0	195	0	0	0	0	0	120	40	
Rank 26's ghost cells:	60	0	0	0	0	0	0	0	0	0	0	0	150	100	0	0	0	0	0	130	0	0	0	0	110	0	0	190	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	68	0	0	0	0	90	0	192	0	130	0	0	80	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	70	50	0	0	0	0	0	0	0	0	130	0	100	0	170	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	110	0	110	80	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	125	0	0	0	120	0	265	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	40	0	80	170	80	265	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1           38.195         0.238347
    2           23.209         0.607644
    3          21.4624         0.924747
    4            19.26         0.897384
    5          9.55875         0.496299
    6          5.23952         0.548138
    7          3.90636         0.745557
    8           2.4333         0.622907
    9          1.20783         0.496374
   10         0.837461         0.693362
   11         0.612868         0.731817
   12          0.31294         0.510615
   13         0.231388           0.7394
   14         0.153933         0.665259
   15        0.0860656         0.559111
   16        0.0593121          0.68915
   17        0.0390445         0.658289
   18        0.0242732          0.62168
   19         0.016331           0.6728
   20       0.00939828         0.575487
=== rate=0.614345, T=0.189149, TIT=0.00945745, IT=20

 Elapsed time: 0.189149
Rank 0: Matrix-vector product took 0.00314153 seconds
Rank 1: Matrix-vector product took 0.00316395 seconds
Rank 2: Matrix-vector product took 0.00302576 seconds
Rank 3: Matrix-vector product took 0.00291933 seconds
Rank 4: Matrix-vector product took 0.00288114 seconds
Rank 5: Matrix-vector product took 0.00305371 seconds
Rank 6: Matrix-vector product took 0.00281414 seconds
Rank 7: Matrix-vector product took 0.00291515 seconds
Rank 8: Matrix-vector product took 0.00281725 seconds
Rank 9: Matrix-vector product took 0.00296126 seconds
Rank 10: Matrix-vector product took 0.00307135 seconds
Rank 11: Matrix-vector product took 0.00288636 seconds
Rank 12: Matrix-vector product took 0.00306371 seconds
Rank 13: Matrix-vector product took 0.003088 seconds
Rank 14: Matrix-vector product took 0.00285197 seconds
Rank 15: Matrix-vector product took 0.00314591 seconds
Rank 16: Matrix-vector product took 0.0028991 seconds
Rank 17: Matrix-vector product took 0.00303533 seconds
Rank 18: Matrix-vector product took 0.00308484 seconds
Rank 19: Matrix-vector product took 0.00307191 seconds
Rank 20: Matrix-vector product took 0.00309165 seconds
Rank 21: Matrix-vector product took 0.00286278 seconds
Rank 22: Matrix-vector product took 0.00280487 seconds
Rank 23: Matrix-vector product took 0.00293702 seconds
Rank 24: Matrix-vector product took 0.00308458 seconds
Rank 25: Matrix-vector product took 0.00289772 seconds
Rank 26: Matrix-vector product took 0.00305869 seconds
Rank 27: Matrix-vector product took 0.00301698 seconds
Rank 28: Matrix-vector product took 0.00303654 seconds
Rank 29: Matrix-vector product took 0.00273091 seconds
Rank 30: Matrix-vector product took 0.00289578 seconds
Rank 31: Matrix-vector product took 0.00301154 seconds
Average time for Matrix-vector product is 0.00297877 seconds

Rank 0: copyOwnerToAll took 0.00016154 seconds
Rank 1: copyOwnerToAll took 0.00010412 seconds
Rank 2: copyOwnerToAll took 0.00015795 seconds
Rank 3: copyOwnerToAll took 0.00010581 seconds
Rank 4: copyOwnerToAll took 5.931e-05 seconds
Rank 5: copyOwnerToAll took 8.477e-05 seconds
Rank 6: copyOwnerToAll took 5.272e-05 seconds
Rank 7: copyOwnerToAll took 7.065e-05 seconds
Rank 8: copyOwnerToAll took 7.452e-05 seconds
Rank 9: copyOwnerToAll took 7.634e-05 seconds
Rank 10: copyOwnerToAll took 0.00015481 seconds
Rank 11: copyOwnerToAll took 0.00015816 seconds
Rank 12: copyOwnerToAll took 0.00021746 seconds
Rank 13: copyOwnerToAll took 0.00014268 seconds
Rank 14: copyOwnerToAll took 8.859e-05 seconds
Rank 15: copyOwnerToAll took 0.00010996 seconds
Rank 16: copyOwnerToAll took 6.471e-05 seconds
Rank 17: copyOwnerToAll took 8.235e-05 seconds
Rank 18: copyOwnerToAll took 0.00010897 seconds
Rank 19: copyOwnerToAll took 0.00019603 seconds
Rank 20: copyOwnerToAll took 8.844e-05 seconds
Rank 21: copyOwnerToAll took 7.143e-05 seconds
Rank 22: copyOwnerToAll took 5.879e-05 seconds
Rank 23: copyOwnerToAll took 0.00019719 seconds
Rank 24: copyOwnerToAll took 0.00014967 seconds
Rank 25: copyOwnerToAll took 0.00020316 seconds
Rank 26: copyOwnerToAll took 0.00016361 seconds
Rank 27: copyOwnerToAll took 9.927e-05 seconds
Rank 28: copyOwnerToAll took 9.321e-05 seconds
Rank 29: copyOwnerToAll took 8.977e-05 seconds
Rank 30: copyOwnerToAll took 0.00010048 seconds
Rank 31: copyOwnerToAll took 0.0001049 seconds
Average time for copyOwnertoAll is 0.000115355 seconds
