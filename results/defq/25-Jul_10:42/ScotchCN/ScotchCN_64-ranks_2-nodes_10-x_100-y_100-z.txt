Number of cores/ranks per node is: 32
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 1601
Cell on rank 1 before loadbalancing: 1576
Cell on rank 2 before loadbalancing: 1570
Cell on rank 3 before loadbalancing: 1562
Cell on rank 4 before loadbalancing: 1573
Cell on rank 5 before loadbalancing: 1577
Cell on rank 6 before loadbalancing: 1570
Cell on rank 7 before loadbalancing: 1577
Cell on rank 8 before loadbalancing: 1570
Cell on rank 9 before loadbalancing: 1577
Cell on rank 10 before loadbalancing: 1572
Cell on rank 11 before loadbalancing: 1560
Cell on rank 12 before loadbalancing: 1554
Cell on rank 13 before loadbalancing: 1548
Cell on rank 14 before loadbalancing: 1548
Cell on rank 15 before loadbalancing: 1570
Cell on rank 16 before loadbalancing: 1577
Cell on rank 17 before loadbalancing: 1547
Cell on rank 18 before loadbalancing: 1547
Cell on rank 19 before loadbalancing: 1547
Cell on rank 20 before loadbalancing: 1572
Cell on rank 21 before loadbalancing: 1547
Cell on rank 22 before loadbalancing: 1577
Cell on rank 23 before loadbalancing: 1548
Cell on rank 24 before loadbalancing: 1539
Cell on rank 25 before loadbalancing: 1547
Cell on rank 26 before loadbalancing: 1557
Cell on rank 27 before loadbalancing: 1547
Cell on rank 28 before loadbalancing: 1547
Cell on rank 29 before loadbalancing: 1547
Cell on rank 30 before loadbalancing: 1547
Cell on rank 31 before loadbalancing: 1504
Cell on rank 32 before loadbalancing: 1577
Cell on rank 33 before loadbalancing: 1548
Cell on rank 34 before loadbalancing: 1550
Cell on rank 35 before loadbalancing: 1575
Cell on rank 36 before loadbalancing: 1555
Cell on rank 37 before loadbalancing: 1577
Cell on rank 38 before loadbalancing: 1550
Cell on rank 39 before loadbalancing: 1576
Cell on rank 40 before loadbalancing: 1577
Cell on rank 41 before loadbalancing: 1577
Cell on rank 42 before loadbalancing: 1576
Cell on rank 43 before loadbalancing: 1550
Cell on rank 44 before loadbalancing: 1592
Cell on rank 45 before loadbalancing: 1577
Cell on rank 46 before loadbalancing: 1562
Cell on rank 47 before loadbalancing: 1550
Cell on rank 48 before loadbalancing: 1577
Cell on rank 49 before loadbalancing: 1584
Cell on rank 50 before loadbalancing: 1577
Cell on rank 51 before loadbalancing: 1561
Cell on rank 52 before loadbalancing: 1547
Cell on rank 53 before loadbalancing: 1550
Cell on rank 54 before loadbalancing: 1561
Cell on rank 55 before loadbalancing: 1556
Cell on rank 56 before loadbalancing: 1576
Cell on rank 57 before loadbalancing: 1547
Cell on rank 58 before loadbalancing: 1558
Cell on rank 59 before loadbalancing: 1577
Cell on rank 60 before loadbalancing: 1550
Cell on rank 61 before loadbalancing: 1577
Cell on rank 62 before loadbalancing: 1577
Cell on rank 63 before loadbalancing: 1554
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	70	0	54	66	40	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	0	0	60	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	70	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	0	0	140	100	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	54	150	140	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	66	0	100	51	0	170	0	0	0	0	0	60	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	40	0	0	0	170	0	97	116	0	0	50	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	97	0	110	0	0	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	40	0	0	0	0	126	110	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	44	133	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	170	30	80	0	5	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	170	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	50	90	0	30	80	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	60	50	0	0	80	0	110	0	70	86	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	140	0	30	0	0	0	0	0	0	70	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	5	0	0	87	150	0	90	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	175	0	0	10	0	80	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	50	0	24	114	0	0	70	77	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	0	118	0	132	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	110	120	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	16	
From rank 20 to: 	0	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	94	0	0	0	0	0	0	0	0	0	0	110	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	132	0	0	110	0	38	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	0	72	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	86	0	0	0	0	0	0	44	0	0	0	0	0	0	0	0	114	0	0	0	0	38	0	121	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	94	66	131	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	0	114	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	110	85	0	0	0	0	124	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	77	0	0	0	0	0	77	0	0	0	0	130	139	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	0	0	0	0	0	0	0	114	71	130	0	10	36	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	50	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	139	19	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	131	0	60	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	71	0	60	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	30	90	112	0	0	0	0	0	0	89	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	50	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	92	0	38	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	40	90	0	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	27	0	0	0	0	0	0	0	0	0	0	0	90	0	113	0	0	0	0	0	0	0	88	33	0	0	0	0	0	0	77	48	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	38	0	0	0	172	80	13	0	0	0	0	74	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	172	0	0	90	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	76	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	80	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	90	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	33	90	40	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	69	0	0	0	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	88	0	0	0	0	33	69	0	127	0	0	0	0	0	0	0	90	0	0	71	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	0	0	33	0	0	0	0	90	0	130	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	74	0	0	0	30	0	0	120	0	133	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	90	0	0	0	0	0	0	133	0	26	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	76	24	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	38	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	104	80	0	0	0	0	0	0	0	0	0	110	36	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	104	0	50	100	0	120	0	0	33	0	0	0	0	79	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	72	0	0	0	0	0	0	0	0	0	0	0	0	0	77	0	0	0	0	0	0	0	0	0	0	0	0	80	50	0	110	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	0	0	0	0	0	0	90	0	0	0	0	0	0	100	110	0	0	30	90	0	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	100	19	91	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	154	0	80	30	86	0	0	0	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	80	0	0	0	0	0	0	0	0	90	0	80	0	150	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	100	30	150	0	0	0	0	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	0	14	86	0	0	0	144	0	94	0	110	0	0	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	144	0	90	10	0	0	0	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	150	0	0	0	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	94	16	150	0	0	44	124	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	96	24	120	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	79	0	0	0	0	0	0	110	0	0	48	96	0	100	0	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	24	100	0	120	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	

Edge-cut for node partition: 1137

loadb
After loadbalancing process 58 has 1798 cells.
After loadbalancing process 57 has 1887 cells.
After loadbalancing process 45 has 1932 cells.
After loadbalancing process 47 has 1790 cells.
After loadbalancing process 46 has 1972 cells.
After loadbalancing process 14 has 1798 cells.
After loadbalancing process 56 has 2056 cells.
After loadbalancing process 15 has 1985 cells.
After loadbalancing process 31 has 1774 cells.
After loadbalancing process 24 has 1862 cells.
After loadbalancing process 28 has 1995 cells.
After loadbalancing process 30 has 1912 cells.
After loadbalancing process 1 has 1952 cells.
After loadbalancing process 3 has 1954 cells.
After loadbalancing process 13 has 1966 cells.
After loadbalancing process 5 has 2100 cells.
After loadbalancing process 12 has 1944 cells.
After loadbalancing process 23 has 1982 cells.
After loadbalancing process 29 has 1951 cells.
After loadbalancing process 40 has 2050 cells.
After loadbalancing process 53 has 2050 cells.
After loadbalancing process 44 has 2065 cells.
After loadbalancing process 59 has 2005 cells.
After loadbalancing process 43 has 2012 cells.
After loadbalancing process 37 has 1929 cells.
After loadbalancing process 2 has 1950 cells.
After loadbalancing process 41 has 1946 cells.
After loadbalancing process 6 has 2030 cells.
After loadbalancing process 42 has 2054 cells.
After loadbalancing process 8 has 2030 cells.
After loadbalancing process 52 has 1907 cells.
After loadbalancing process 11 has 2026 cells.
After loadbalancing process 54 has 2021 cells.
After loadbalancing process 26 has 2040 cells.
After loadbalancing process 55 has 1926 cells.
After loadbalancing process 7 has 2096 cells.
After loadbalancing process 32 has 2048 cells.
After loadbalancing process 49 has 2070 cells.
After loadbalancing process 35 has 2051 cells.
After loadbalancing process 39 has 1926 cells.
After loadbalancing process 48 has 2045 cells.
After loadbalancing process 34 has 2023 cells.
After loadbalancing process 51 has 2034 cells.
After loadbalancing process 22 has 2060 cells.
After loadbalancing process 62 has 1945 cells.
After loadbalancing process 36 has 2060 cells.
After loadbalancing process 61 has 2048 cells.
After loadbalancing process 33 has 2008 cells.
After loadbalancing process 25 has 2007 cells.
After loadbalancing process 50 has 2066 cells.
After loadbalancing process 0 has 2055 cells.
After loadbalancing process 38 has 2040 cells.
After loadbalancing process 63 has 1918 cells.
After loadbalancing process 27 has 2015 cells.
After loadbalancing process 60 has 2020 cells.
After loadbalancing process 17 has 2029 cells.
After loadbalancing process 9 has 1993 cells.
After loadbalancing process 10 has 2042 cells.
After loadbalancing process 4 has 2060 cells.
After loadbalancing process 21 has 2023 cells.
After loadbalancing process 18 has 1891 cells.
After loadbalancing process 19 has 2023 cells.
After loadbalancing process 20 has 2049 cells.
After loadbalancing process 16 has 2039 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	70	0	54	66	40	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	84	0	0	0	60	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	70	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	0	0	140	100	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	54	150	140	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	66	0	100	51	0	170	0	0	0	0	0	60	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	40	0	0	0	170	0	97	116	0	0	50	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	97	0	110	0	0	93	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	40	0	0	0	0	126	110	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	44	133	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	170	30	80	0	5	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	170	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	50	90	0	30	80	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	60	50	0	0	80	0	110	0	70	86	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	140	0	30	0	0	0	0	0	0	70	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	5	0	0	87	150	0	90	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	175	0	0	10	0	80	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	50	0	24	114	0	0	70	77	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	0	118	0	132	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	30	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	110	120	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	16	
Rank 20's ghost cells:	0	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	94	0	0	0	0	0	0	0	0	0	0	110	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	132	0	0	110	0	38	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	0	72	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	86	0	0	0	0	0	0	44	0	0	0	0	0	0	0	0	114	0	0	0	0	38	0	121	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	94	66	131	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	0	114	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	110	85	0	0	0	0	124	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	77	0	0	0	0	0	77	0	0	0	0	130	139	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	0	0	0	0	0	0	0	114	71	130	0	10	36	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	50	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	139	19	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	131	0	60	111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	71	0	60	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	30	90	112	0	0	0	0	0	0	89	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	50	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	92	0	38	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	40	90	0	113	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	27	0	0	0	0	0	0	0	0	0	0	0	90	0	113	0	0	0	0	0	0	0	88	33	0	0	0	0	0	0	77	48	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	112	38	0	0	0	172	80	13	0	0	0	0	74	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	172	0	0	90	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	76	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	80	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	90	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	33	90	40	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	69	0	0	0	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	88	0	0	0	0	33	69	0	127	0	0	0	0	0	0	0	90	0	0	71	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	0	0	33	0	0	0	0	90	0	130	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	74	0	0	0	30	0	0	120	0	133	76	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	90	0	0	0	0	0	0	133	0	26	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	76	24	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	38	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	104	80	0	0	0	0	0	0	0	0	0	110	36	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	104	0	50	100	0	120	0	0	33	0	0	0	0	79	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	72	0	0	0	0	0	0	0	0	0	0	0	0	0	77	0	0	0	0	0	0	0	0	0	0	0	0	80	50	0	110	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	0	0	0	0	0	0	90	0	0	0	0	0	0	100	110	0	0	30	90	0	0	0	0	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	100	19	91	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	30	154	0	80	30	86	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	80	0	0	0	0	0	0	0	0	90	0	80	0	150	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	100	30	150	0	0	0	0	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	0	14	86	0	0	0	144	0	94	0	110	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	144	0	90	10	0	0	0	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	150	0	0	0	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	94	16	150	0	0	44	124	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	96	24	120	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	38	79	0	0	0	0	0	0	110	0	0	48	96	0	100	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	124	24	100	0	120	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	114	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          39.9052         0.249019
    2          24.3354          0.60983
    3          21.9072         0.900221
    4           21.278         0.971278
    5          11.1218          0.52269
    6          5.58659          0.50231
    7          4.28356         0.766757
    8          2.66746         0.622721
    9           1.3548         0.507898
   10         0.981679         0.724594
   11         0.665249         0.677664
   12         0.369034         0.554731
   13         0.258029         0.699201
   14         0.169541         0.657061
   15         0.109918         0.648325
   16        0.0651254         0.592493
   17        0.0470954         0.723149
   18        0.0314962         0.668775
   19        0.0192722         0.611888
   20        0.0127939         0.663855
=== rate=0.623893, T=0.126101, TIT=0.00630506, IT=20

 Elapsed time: 0.126101
Rank 0: Matrix-vector product took 0.00161676 seconds
Rank 1: Matrix-vector product took 0.00152809 seconds
Rank 2: Matrix-vector product took 0.00153396 seconds
Rank 3: Matrix-vector product took 0.00152737 seconds
Rank 4: Matrix-vector product took 0.00162312 seconds
Rank 5: Matrix-vector product took 0.00164593 seconds
Rank 6: Matrix-vector product took 0.00159983 seconds
Rank 7: Matrix-vector product took 0.00161805 seconds
Rank 8: Matrix-vector product took 0.00160168 seconds
Rank 9: Matrix-vector product took 0.00156039 seconds
Rank 10: Matrix-vector product took 0.00160633 seconds
Rank 11: Matrix-vector product took 0.00156934 seconds
Rank 12: Matrix-vector product took 0.00153505 seconds
Rank 13: Matrix-vector product took 0.00154554 seconds
Rank 14: Matrix-vector product took 0.00140008 seconds
Rank 15: Matrix-vector product took 0.00155535 seconds
Rank 16: Matrix-vector product took 0.00159473 seconds
Rank 17: Matrix-vector product took 0.00157451 seconds
Rank 18: Matrix-vector product took 0.00148888 seconds
Rank 19: Matrix-vector product took 0.00160354 seconds
Rank 20: Matrix-vector product took 0.00160786 seconds
Rank 21: Matrix-vector product took 0.00157873 seconds
Rank 22: Matrix-vector product took 0.00163837 seconds
Rank 23: Matrix-vector product took 0.00151609 seconds
Rank 24: Matrix-vector product took 0.00145868 seconds
Rank 25: Matrix-vector product took 0.00155408 seconds
Rank 26: Matrix-vector product took 0.00160421 seconds
Rank 27: Matrix-vector product took 0.00161509 seconds
Rank 28: Matrix-vector product took 0.00156502 seconds
Rank 29: Matrix-vector product took 0.0015226 seconds
Rank 30: Matrix-vector product took 0.00149111 seconds
Rank 31: Matrix-vector product took 0.00135987 seconds
Rank 32: Matrix-vector product took 0.00162856 seconds
Rank 33: Matrix-vector product took 0.00159724 seconds
Rank 34: Matrix-vector product took 0.00161395 seconds
Rank 35: Matrix-vector product took 0.00164359 seconds
Rank 36: Matrix-vector product took 0.00163317 seconds
Rank 37: Matrix-vector product took 0.00153856 seconds
Rank 38: Matrix-vector product took 0.00159126 seconds
Rank 39: Matrix-vector product took 0.00154464 seconds
Rank 40: Matrix-vector product took 0.00163423 seconds
Rank 41: Matrix-vector product took 0.00155228 seconds
Rank 42: Matrix-vector product took 0.00162192 seconds
Rank 43: Matrix-vector product took 0.00160077 seconds
Rank 44: Matrix-vector product took 0.00162946 seconds
Rank 45: Matrix-vector product took 0.00151077 seconds
Rank 46: Matrix-vector product took 0.00156857 seconds
Rank 47: Matrix-vector product took 0.00143347 seconds
Rank 48: Matrix-vector product took 0.0016226 seconds
Rank 49: Matrix-vector product took 0.00164553 seconds
Rank 50: Matrix-vector product took 0.00164611 seconds
Rank 51: Matrix-vector product took 0.00162009 seconds
Rank 52: Matrix-vector product took 0.00149319 seconds
Rank 53: Matrix-vector product took 0.0016488 seconds
Rank 54: Matrix-vector product took 0.0016042 seconds
Rank 55: Matrix-vector product took 0.00155142 seconds
Rank 56: Matrix-vector product took 0.00162297 seconds
Rank 57: Matrix-vector product took 0.00151187 seconds
Rank 58: Matrix-vector product took 0.00143846 seconds
Rank 59: Matrix-vector product took 0.00155597 seconds
Rank 60: Matrix-vector product took 0.00161477 seconds
Rank 61: Matrix-vector product took 0.00161584 seconds
Rank 62: Matrix-vector product took 0.0015461 seconds
Rank 63: Matrix-vector product took 0.00153865 seconds
Average time for Matrix-vector product is 0.00156967 seconds

Rank 0: copyOwnerToAll took 9.192e-05 seconds
Rank 1: copyOwnerToAll took 8.65e-05 seconds
Rank 2: copyOwnerToAll took 4.976e-05 seconds
Rank 3: copyOwnerToAll took 5.279e-05 seconds
Rank 4: copyOwnerToAll took 7.794e-05 seconds
Rank 5: copyOwnerToAll took 8.033e-05 seconds
Rank 6: copyOwnerToAll took 0.000169 seconds
Rank 7: copyOwnerToAll took 0.00016821 seconds
Rank 8: copyOwnerToAll took 7.462e-05 seconds
Rank 9: copyOwnerToAll took 0.00016806 seconds
Rank 10: copyOwnerToAll took 0.00018115 seconds
Rank 11: copyOwnerToAll took 7.902e-05 seconds
Rank 12: copyOwnerToAll took 5.909e-05 seconds
Rank 13: copyOwnerToAll took 9.135e-05 seconds
Rank 14: copyOwnerToAll took 5.817e-05 seconds
Rank 15: copyOwnerToAll took 6.31e-05 seconds
Rank 16: copyOwnerToAll took 0.00010074 seconds
Rank 17: copyOwnerToAll took 0.00012527 seconds
Rank 18: copyOwnerToAll took 0.00014984 seconds
Rank 19: copyOwnerToAll took 0.00013167 seconds
Rank 20: copyOwnerToAll took 0.00026415 seconds
Rank 21: copyOwnerToAll took 0.00024596 seconds
Rank 22: copyOwnerToAll took 0.00012943 seconds
Rank 23: copyOwnerToAll took 0.00013236 seconds
Rank 24: copyOwnerToAll took 6.98e-05 seconds
Rank 25: copyOwnerToAll took 7.704e-05 seconds
Rank 26: copyOwnerToAll took 8.21e-05 seconds
Rank 27: copyOwnerToAll took 0.00011316 seconds
Rank 28: copyOwnerToAll took 8.731e-05 seconds
Rank 29: copyOwnerToAll took 8.877e-05 seconds
Rank 30: copyOwnerToAll took 9.283e-05 seconds
Rank 31: copyOwnerToAll took 5.132e-05 seconds
Rank 32: copyOwnerToAll took 0.00011507 seconds
Rank 33: copyOwnerToAll took 0.00016784 seconds
Rank 34: copyOwnerToAll took 0.00015086 seconds
Rank 35: copyOwnerToAll took 0.00016963 seconds
Rank 36: copyOwnerToAll took 0.00014462 seconds
Rank 37: copyOwnerToAll took 6.471e-05 seconds
Rank 38: copyOwnerToAll took 0.00016727 seconds
Rank 39: copyOwnerToAll took 0.00012749 seconds
Rank 40: copyOwnerToAll took 0.00012898 seconds
Rank 41: copyOwnerToAll took 6.661e-05 seconds
Rank 42: copyOwnerToAll took 7.523e-05 seconds
Rank 43: copyOwnerToAll took 7.574e-05 seconds
Rank 44: copyOwnerToAll took 0.00013852 seconds
Rank 45: copyOwnerToAll took 0.00011888 seconds
Rank 46: copyOwnerToAll took 0.00013868 seconds
Rank 47: copyOwnerToAll took 4.229e-05 seconds
Rank 48: copyOwnerToAll took 0.00015844 seconds
Rank 49: copyOwnerToAll took 0.00012568 seconds
Rank 50: copyOwnerToAll took 0.00026131 seconds
Rank 51: copyOwnerToAll took 0.00012726 seconds
Rank 52: copyOwnerToAll took 7.106e-05 seconds
Rank 53: copyOwnerToAll took 0.00011218 seconds
Rank 54: copyOwnerToAll took 8.879e-05 seconds
Rank 55: copyOwnerToAll took 7.64e-05 seconds
Rank 56: copyOwnerToAll took 8.354e-05 seconds
Rank 57: copyOwnerToAll took 6.048e-05 seconds
Rank 58: copyOwnerToAll took 3.887e-05 seconds
Rank 59: copyOwnerToAll took 7.008e-05 seconds
Rank 60: copyOwnerToAll took 0.00012927 seconds
Rank 61: copyOwnerToAll took 8.522e-05 seconds
Rank 62: copyOwnerToAll took 7.202e-05 seconds
Rank 63: copyOwnerToAll took 0.0001103 seconds
Average time for copyOwnertoAll is 0.000110251 seconds
