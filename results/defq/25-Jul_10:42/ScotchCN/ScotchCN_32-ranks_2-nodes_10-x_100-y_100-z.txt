Number of cores/ranks per node is: 16
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 3100
Cell on rank 1 before loadbalancing: 3100
Cell on rank 2 before loadbalancing: 3130
Cell on rank 3 before loadbalancing: 3130
Cell on rank 4 before loadbalancing: 3150
Cell on rank 5 before loadbalancing: 3140
Cell on rank 6 before loadbalancing: 3140
Cell on rank 7 before loadbalancing: 3130
Cell on rank 8 before loadbalancing: 3124
Cell on rank 9 before loadbalancing: 3156
Cell on rank 10 before loadbalancing: 3156
Cell on rank 11 before loadbalancing: 3110
Cell on rank 12 before loadbalancing: 3130
Cell on rank 13 before loadbalancing: 3110
Cell on rank 14 before loadbalancing: 3124
Cell on rank 15 before loadbalancing: 3156
Cell on rank 16 before loadbalancing: 3156
Cell on rank 17 before loadbalancing: 3120
Cell on rank 18 before loadbalancing: 3094
Cell on rank 19 before loadbalancing: 3110
Cell on rank 20 before loadbalancing: 3102
Cell on rank 21 before loadbalancing: 3156
Cell on rank 22 before loadbalancing: 3140
Cell on rank 23 before loadbalancing: 3156
Cell on rank 24 before loadbalancing: 3100
Cell on rank 25 before loadbalancing: 3110
Cell on rank 26 before loadbalancing: 3130
Cell on rank 27 before loadbalancing: 3100
Cell on rank 28 before loadbalancing: 3110
Cell on rank 29 before loadbalancing: 3120
Cell on rank 30 before loadbalancing: 3100
Cell on rank 31 before loadbalancing: 3110
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	200	0	150	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	200	0	150	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	150	0	200	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	150	20	200	0	0	210	10	0	100	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	200	40	130	0	0	70	0	0	0	0	0	100	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	210	200	0	110	0	30	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	190	0	0	20	30	110	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	130	0	190	0	0	0	0	0	0	0	0	0	60	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	100	0	20	0	0	0	190	140	90	0	0	100	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	170	10	0	0	0	0	190	0	0	0	0	0	0	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	70	160	0	0	140	0	0	188	0	0	0	0	0	0	0	0	0	0	60	90	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	90	0	188	0	0	0	105	0	0	0	0	0	0	0	0	42	0	0	0	0	0	80	135	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	170	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	170	0	52	138	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	100	0	0	105	150	42	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	85	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	42	138	0	0	0	138	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	100	0	0	70	0	0	0	0	0	0	0	0	0	188	25	135	10	0	140	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	188	0	162	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	25	160	0	180	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	135	0	180	0	230	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	230	0	250	136	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	44	110	0	0	90	170	0	0	0	0	
From rank 22 to: 	0	0	0	0	100	0	0	0	0	0	60	0	0	0	0	0	140	0	0	0	136	48	0	166	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	90	40	0	0	0	0	0	0	0	0	0	110	166	0	0	0	40	0	0	0	130	70	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	60	100	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	120	0	110	0	0	100	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	40	50	120	0	190	0	0	0	170	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	100	0	190	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	175	40	135	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	80	190	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	175	0	95	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	145	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	40	95	0	185	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	100	170	0	145	0	185	0	

Edge-cut for node partition: 1225

loadb
After loadbalancing process 10 has 3459 cells.
After loadbalancing process 11 has 3655 cells.
After loadbalancing process 20 has 3640 cells.
After loadbalancing process 21 has 3470 cells.
After loadbalancing process 7 has 3660 cells.
After loadbalancing process 1 has 3460 cells.
After loadbalancing process 12 has 3728 cells.
After loadbalancing process 9 has 3620 cells.
After loadbalancing process 0 has 3640 cells.
After loadbalancing process 22 has 3776 cells.
After loadbalancing process 8 has 3824 cells.
After loadbalancing process 25 has 3620 cells.
After loadbalancing process 28 has 3570 cells.
After loadbalancing process 14 has 3790 cells.
After loadbalancing process 4 has 3800 cells.
After loadbalancing process 6 has 3680 cells.
After loadbalancing process 13 has 3820 cells.
After loadbalancing process 24 has 3440 cells.
After loadbalancing process 29 has 3745 cells.
After loadbalancing process 23 has 3644 cells.
After loadbalancing process 17 has 3664 cells.
After loadbalancing process 5 has 3850 cells.
After loadbalancing process 27 has 3560 cells.
After loadbalancing process 2 has 3650 cells.
After loadbalancing process 16 has 3806 cells.
After loadbalancing process 31 has 3780 cells.
After loadbalancing process 19 has 3750 cells.
After loadbalancing process 3 has 3830 cells.
After loadbalancing process 18 has 3864 cells.
After loadbalancing process 30 has 3695 cells.
After loadbalancing process 15 has 3802 cells.
After loadbalancing process 26 has 3790 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	200	0	150	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	200	0	150	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	150	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	150	20	200	0	0	210	10	0	0	0	0	0	0	0	0	0	100	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	200	40	130	100	0	0	0	0	0	110	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	210	200	0	110	0	0	0	0	0	0	0	0	0	30	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	190	0	0	20	30	110	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	130	0	190	0	60	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	100	0	0	70	0	188	25	135	10	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	150	188	0	162	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	25	160	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	135	0	180	0	230	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	10	0	0	230	0	250	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	250	0	44	110	0	0	0	0	0	0	0	0	0	0	90	170	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	100	0	0	0	140	0	0	0	136	48	0	166	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	110	166	0	0	0	90	40	0	0	0	0	0	0	40	0	0	0	130	70	
Rank 16's ghost cells:	0	0	0	100	0	20	0	0	0	0	0	0	0	0	0	0	0	190	140	90	0	0	100	42	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	170	10	0	0	0	0	0	0	0	0	0	0	0	0	190	0	0	0	0	0	0	138	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	70	160	0	0	0	0	0	0	0	0	60	90	140	0	0	188	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	90	0	188	0	0	0	105	0	0	0	0	0	0	80	135	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	150	0	0	0	0	0	0	190	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	52	138	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	105	150	42	0	170	0	0	0	0	0	85	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	138	0	0	0	138	170	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	60	100	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	120	0	110	0	0	100	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	40	0	0	0	0	0	0	0	0	50	120	0	190	0	0	0	170	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	100	0	190	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	175	40	135	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	190	0	85	0	0	0	0	0	175	0	95	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	145	0	0	0	0	0	0	0	0	40	95	0	185	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	100	170	0	145	0	185	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.7113         0.235328
    2          22.8793         0.606697
    3          21.2363         0.928187
    4          18.8526         0.887756
    5          9.33787         0.495308
    6          5.18155         0.554896
    7          3.77698         0.728929
    8           2.4464         0.647715
    9          1.24621         0.509403
   10         0.810029         0.649997
   11         0.597681         0.737852
   12         0.293215         0.490588
   13         0.207648         0.708174
   14         0.140923         0.678664
   15        0.0816142         0.579141
   16        0.0550921         0.675031
   17        0.0377169         0.684616
   18        0.0243994          0.64691
   19        0.0155704         0.638148
=== rate=0.614916, T=0.236926, TIT=0.0124698, IT=19

 Elapsed time: 0.236926
Rank 0: Matrix-vector product took 0.00291501 seconds
Rank 1: Matrix-vector product took 0.00274421 seconds
Rank 2: Matrix-vector product took 0.00290164 seconds
Rank 3: Matrix-vector product took 0.00311981 seconds
Rank 4: Matrix-vector product took 0.00304542 seconds
Rank 5: Matrix-vector product took 0.00313174 seconds
Rank 6: Matrix-vector product took 0.00298574 seconds
Rank 7: Matrix-vector product took 0.00293466 seconds
Rank 8: Matrix-vector product took 0.00303921 seconds
Rank 9: Matrix-vector product took 0.0029013 seconds
Rank 10: Matrix-vector product took 0.00277078 seconds
Rank 11: Matrix-vector product took 0.00284172 seconds
Rank 12: Matrix-vector product took 0.00295099 seconds
Rank 13: Matrix-vector product took 0.00307468 seconds
Rank 14: Matrix-vector product took 0.00303839 seconds
Rank 15: Matrix-vector product took 0.00304452 seconds
Rank 16: Matrix-vector product took 0.00304337 seconds
Rank 17: Matrix-vector product took 0.00294686 seconds
Rank 18: Matrix-vector product took 0.00306916 seconds
Rank 19: Matrix-vector product took 0.00299127 seconds
Rank 20: Matrix-vector product took 0.00284983 seconds
Rank 21: Matrix-vector product took 0.00280965 seconds
Rank 22: Matrix-vector product took 0.00305175 seconds
Rank 23: Matrix-vector product took 0.00294581 seconds
Rank 24: Matrix-vector product took 0.00349446 seconds
Rank 25: Matrix-vector product took 0.00294263 seconds
Rank 26: Matrix-vector product took 0.00307289 seconds
Rank 27: Matrix-vector product took 0.00284505 seconds
Rank 28: Matrix-vector product took 0.00282194 seconds
Rank 29: Matrix-vector product took 0.00297965 seconds
Rank 30: Matrix-vector product took 0.00287955 seconds
Rank 31: Matrix-vector product took 0.00297381 seconds
Average time for Matrix-vector product is 0.00297367 seconds

Rank 0: copyOwnerToAll took 6.224e-05 seconds
Rank 1: copyOwnerToAll took 6.261e-05 seconds
Rank 2: copyOwnerToAll took 0.00011601 seconds
Rank 3: copyOwnerToAll took 0.000232769 seconds
Rank 4: copyOwnerToAll took 0.000247289 seconds
Rank 5: copyOwnerToAll took 0.000158899 seconds
Rank 6: copyOwnerToAll took 8.008e-05 seconds
Rank 7: copyOwnerToAll took 6.993e-05 seconds
Rank 8: copyOwnerToAll took 0.00010386 seconds
Rank 9: copyOwnerToAll took 5.6e-05 seconds
Rank 10: copyOwnerToAll took 6.214e-05 seconds
Rank 11: copyOwnerToAll took 6.383e-05 seconds
Rank 12: copyOwnerToAll took 9.231e-05 seconds
Rank 13: copyOwnerToAll took 0.000238529 seconds
Rank 14: copyOwnerToAll took 0.000170669 seconds
Rank 15: copyOwnerToAll took 0.000186769 seconds
Rank 16: copyOwnerToAll took 0.00013212 seconds
Rank 17: copyOwnerToAll took 0.00014379 seconds
Rank 18: copyOwnerToAll took 0.00019533 seconds
Rank 19: copyOwnerToAll took 0.00013654 seconds
Rank 20: copyOwnerToAll took 6.677e-05 seconds
Rank 21: copyOwnerToAll took 5.121e-05 seconds
Rank 22: copyOwnerToAll took 9.604e-05 seconds
Rank 23: copyOwnerToAll took 7.385e-05 seconds
Rank 24: copyOwnerToAll took 8.863e-05 seconds
Rank 25: copyOwnerToAll took 7.984e-05 seconds
Rank 26: copyOwnerToAll took 0.00015792 seconds
Rank 27: copyOwnerToAll took 0.00010179 seconds
Rank 28: copyOwnerToAll took 6.725e-05 seconds
Rank 29: copyOwnerToAll took 9.118e-05 seconds
Rank 30: copyOwnerToAll took 0.00012818 seconds
Rank 31: copyOwnerToAll took 0.00011922 seconds
Average time for copyOwnertoAll is 0.000116675 seconds
