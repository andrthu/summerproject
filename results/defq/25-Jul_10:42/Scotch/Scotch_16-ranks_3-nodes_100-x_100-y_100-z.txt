Number of cores/ranks per node is: 5
Scotch partitioner
Cell on rank 0 before loadbalancing: 61892
Cell on rank 1 before loadbalancing: 63000
Cell on rank 2 before loadbalancing: 62262
Cell on rank 3 before loadbalancing: 62567
Cell on rank 4 before loadbalancing: 61998
Cell on rank 5 before loadbalancing: 62826
Cell on rank 6 before loadbalancing: 61892
Cell on rank 7 before loadbalancing: 61875
Cell on rank 8 before loadbalancing: 62668
Cell on rank 9 before loadbalancing: 63048
Cell on rank 10 before loadbalancing: 62520
Cell on rank 11 before loadbalancing: 62615
Cell on rank 12 before loadbalancing: 62020
Cell on rank 13 before loadbalancing: 63040
Cell on rank 14 before loadbalancing: 62775
Cell on rank 15 before loadbalancing: 63002
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2125	1216	191	0	294	1198	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2123	0	0	1017	0	123	1405	0	21	0	0	99	0	2513	86	68	
From rank 2 to: 	1220	0	0	2007	834	521	4	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	198	1005	2007	0	344	269	20	0	0	0	0	594	0	39	875	1306	
From rank 4 to: 	0	0	838	347	0	1753	0	0	0	0	0	1507	0	0	0	0	
From rank 5 to: 	294	124	518	269	1754	0	856	1296	553	0	23	970	0	0	0	0	
From rank 6 to: 	1213	1419	4	20	0	872	0	1756	778	0	0	0	0	459	0	0	
From rank 7 to: 	0	0	0	0	0	1296	1760	0	1483	90	0	0	0	0	0	0	
From rank 8 to: 	0	19	0	0	0	553	773	1496	0	1944	600	840	0	796	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	90	1932	0	1026	26	1191	369	199	0	
From rank 10 to: 	0	0	0	0	0	23	0	0	608	1032	0	1648	0	0	1199	0	
From rank 11 to: 	0	100	0	595	1504	945	0	0	839	30	1657	0	0	126	1061	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	1191	0	0	0	2572	473	686	
From rank 13 to: 	0	2500	0	39	0	0	458	0	791	381	0	117	2571	0	461	792	
From rank 14 to: 	0	89	0	882	0	0	0	0	0	199	1195	1074	469	462	0	1933	
From rank 15 to: 	0	68	0	1306	0	0	0	0	0	0	0	0	678	793	1933	0	
loadb
After loadbalancing process 10 has 67030 cells.
After loadbalancing process 7 has 66504 cells.
After loadbalancing process 12 has 66942 cells.
After loadbalancing process 9 has 67881 cells.
After loadbalancing process 0 has 66916 cells.
After loadbalancing process 11 has 69472 cells.
After loadbalancing process 4 has 66443 cells.
After loadbalancing process 14 has 69078 cells.
After loadbalancing process 6 has 68413 cells.
After loadbalancing process 8 has 69689 cells.
After loadbalancing process 13 has 71150 cells.
After loadbalancing process 2 has 66848 cells.
After loadbalancing process 5 has 69483 cells.
After loadbalancing process 15 has 67780 cells.
After loadbalancing process 1 has 70455 cells.
After loadbalancing process 3 has 69224 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2125	1216	191	0	294	1198	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2123	0	0	1017	0	123	1405	0	21	0	0	99	0	2513	86	68	
Rank 2's ghost cells:	1220	0	0	2007	834	521	4	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	198	1005	2007	0	344	269	20	0	0	0	0	594	0	39	875	1306	
Rank 4's ghost cells:	0	0	838	347	0	1753	0	0	0	0	0	1507	0	0	0	0	
Rank 5's ghost cells:	294	124	518	269	1754	0	856	1296	553	0	23	970	0	0	0	0	
Rank 6's ghost cells:	1213	1419	4	20	0	872	0	1756	778	0	0	0	0	459	0	0	
Rank 7's ghost cells:	0	0	0	0	0	1296	1760	0	1483	90	0	0	0	0	0	0	
Rank 8's ghost cells:	0	19	0	0	0	553	773	1496	0	1944	600	840	0	796	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	90	1932	0	1026	26	1191	369	199	0	
Rank 10's ghost cells:	0	0	0	0	0	23	0	0	608	1032	0	1648	0	0	1199	0	
Rank 11's ghost cells:	0	100	0	595	1504	945	0	0	839	30	1657	0	0	126	1061	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	1191	0	0	0	2572	473	686	
Rank 13's ghost cells:	0	2500	0	39	0	0	458	0	791	381	0	117	2571	0	461	792	
Rank 14's ghost cells:	0	89	0	882	0	0	0	0	0	199	1195	1074	469	462	0	1933	
Rank 15's ghost cells:	0	68	0	1306	0	0	0	0	0	0	0	0	678	793	1933	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.7116         0.219022
    2          31.6692          0.57884
    3          22.5178          0.71103
    4            19.51         0.866424
    5          17.9763          0.92139
    6          14.1984         0.789841
    7          12.0562         0.849128
    8           11.818         0.980239
    9          10.0898         0.853767
   10           8.6806         0.860332
   11          8.64016         0.995342
   12          7.83189         0.906452
   13          6.72124         0.858189
   14          6.79808          1.01143
   15          6.38759         0.939617
   16           5.5018         0.861327
   17          5.49105         0.998046
   18           5.3698         0.977917
   19          4.68187          0.87189
   20          4.62661         0.988197
   21          4.54785         0.982977
   22          4.10061         0.901658
   23          4.06015         0.990134
   24          3.84932         0.948074
   25          3.50687         0.911035
   26          3.51199          1.00146
   27          3.29093         0.937055
   28           3.0534         0.927825
   29          3.08101          1.00904
   30          2.87244         0.932302
   31          2.78505         0.969576
   32          2.98118          1.07042
   33          3.00534           1.0081
   34          3.10354          1.03268
   35          3.07791         0.991743
   36          2.59625         0.843509
   37          2.14919         0.827807
   38          1.81308         0.843612
   39           1.5168         0.836584
   40          1.34167         0.884541
   41          1.17976         0.879321
   42          1.02916         0.872354
   43         0.919608         0.893548
   44         0.769092         0.836326
   45         0.647625         0.842064
   46         0.550545         0.850099
   47         0.449771         0.816957
   48          0.39403         0.876068
   49          0.33174         0.841916
   50         0.269973         0.813807
   51         0.235695         0.873034
   52         0.196451         0.833495
   53         0.163163         0.830555
   54         0.139952         0.857741
   55         0.115304         0.823886
   56        0.0975597         0.846106
   57        0.0803445         0.823542
   58        0.0663824         0.826222
   59         0.054664         0.823472
   60        0.0440972         0.806695
   61        0.0353142         0.800826
   62        0.0275515         0.780182
   63         0.021678         0.786817
=== rate=0.862046, T=10.2174, TIT=0.162181, IT=63

 Elapsed time: 10.2174
Rank 0: Matrix-vector product took 0.0544627 seconds
Rank 1: Matrix-vector product took 0.057495 seconds
Rank 2: Matrix-vector product took 0.054665 seconds
Rank 3: Matrix-vector product took 0.0567962 seconds
Rank 4: Matrix-vector product took 0.0544038 seconds
Rank 5: Matrix-vector product took 0.0561196 seconds
Rank 6: Matrix-vector product took 0.0558065 seconds
Rank 7: Matrix-vector product took 0.0542746 seconds
Rank 8: Matrix-vector product took 0.0562735 seconds
Rank 9: Matrix-vector product took 0.0566188 seconds
Rank 10: Matrix-vector product took 0.062766 seconds
Rank 11: Matrix-vector product took 0.0566732 seconds
Rank 12: Matrix-vector product took 0.0557904 seconds
Rank 13: Matrix-vector product took 0.0578405 seconds
Rank 14: Matrix-vector product took 0.0561727 seconds
Rank 15: Matrix-vector product took 0.0554728 seconds
Average time for Matrix-vector product is 0.056352 seconds

Rank 0: copyOwnerToAll took 0.000563759 seconds
Rank 1: copyOwnerToAll took 0.000758819 seconds
Rank 2: copyOwnerToAll took 0.000585159 seconds
Rank 3: copyOwnerToAll took 0.000759929 seconds
Rank 4: copyOwnerToAll took 0.000667099 seconds
Rank 5: copyOwnerToAll took 0.000798841 seconds
Rank 6: copyOwnerToAll took 0.000689091 seconds
Rank 7: copyOwnerToAll took 0.000446661 seconds
Rank 8: copyOwnerToAll took 0.000644881 seconds
Rank 9: copyOwnerToAll took 0.000657941 seconds
Rank 10: copyOwnerToAll took 0.00058841 seconds
Rank 11: copyOwnerToAll took 0.000780109 seconds
Rank 12: copyOwnerToAll took 0.00056109 seconds
Rank 13: copyOwnerToAll took 0.000801809 seconds
Rank 14: copyOwnerToAll took 0.000683349 seconds
Rank 15: copyOwnerToAll took 0.000617769 seconds
Average time for copyOwnertoAll is 0.000662795 seconds
Number of cores/ranks per node is: 5
Scotch partitioner
Cell on rank 0 before loadbalancing: 61892
Cell on rank 1 before loadbalancing: 63000
Cell on rank 2 before loadbalancing: 62262
Cell on rank 3 before loadbalancing: 62567
Cell on rank 4 before loadbalancing: 61998
Cell on rank 5 before loadbalancing: 62826
Cell on rank 6 before loadbalancing: 61892
Cell on rank 7 before loadbalancing: 61875
Cell on rank 8 before loadbalancing: 62668
Cell on rank 9 before loadbalancing: 63048
Cell on rank 10 before loadbalancing: 62520
Cell on rank 11 before loadbalancing: 62615
Cell on rank 12 before loadbalancing: 62020
Cell on rank 13 before loadbalancing: 63040
Cell on rank 14 before loadbalancing: 62775
Cell on rank 15 before loadbalancing: 63002
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2125	1216	191	0	294	1198	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2123	0	0	1017	0	123	1405	0	21	0	0	99	0	2513	86	68	
From rank 2 to: 	1220	0	0	2007	834	521	4	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	198	1005	2007	0	344	269	20	0	0	0	0	594	0	39	875	1306	
From rank 4 to: 	0	0	838	347	0	1753	0	0	0	0	0	1507	0	0	0	0	
From rank 5 to: 	294	124	518	269	1754	0	856	1296	553	0	23	970	0	0	0	0	
From rank 6 to: 	1213	1419	4	20	0	872	0	1756	778	0	0	0	0	459	0	0	
From rank 7 to: 	0	0	0	0	0	1296	1760	0	1483	90	0	0	0	0	0	0	
From rank 8 to: 	0	19	0	0	0	553	773	1496	0	1944	600	840	0	796	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	90	1932	0	1026	26	1191	369	199	0	
From rank 10 to: 	0	0	0	0	0	23	0	0	608	1032	0	1648	0	0	1199	0	
From rank 11 to: 	0	100	0	595	1504	945	0	0	839	30	1657	0	0	126	1061	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	1191	0	0	0	2572	473	686	
From rank 13 to: 	0	2500	0	39	0	0	458	0	791	381	0	117	2571	0	461	792	
From rank 14 to: 	0	89	0	882	0	0	0	0	0	199	1195	1074	469	462	0	1933	
From rank 15 to: 	0	68	0	1306	0	0	0	0	0	0	0	0	678	793	1933	0	
loadb
After loadbalancing process 12 has 66942 cells.
After loadbalancing process 10 has 67030 cells.
After loadbalancing process 5 has 69483 cells.
After loadbalancing process 7 has 66504 cells.
After loadbalancing process 11 has 69472 cells.
After loadbalancing process 9 has 67881 cells.
After loadbalancing process 4 has 66443 cells.
After loadbalancing process 1 has 70455 cells.
After loadbalancing process 14 has 69078 cells.
After loadbalancing process 13 has 71150 cells.
After loadbalancing process 0 has 66916 cells.
After loadbalancing process 15 has 67780 cells.
After loadbalancing process 8 has 69689 cells.
After loadbalancing process 6 has 68413 cells.
After loadbalancing process 2 has 66848 cells.
After loadbalancing process 3 has 69224 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2125	1216	191	0	294	1198	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2123	0	0	1017	0	123	1405	0	21	0	0	99	0	2513	86	68	
Rank 2's ghost cells:	1220	0	0	2007	834	521	4	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	198	1005	2007	0	344	269	20	0	0	0	0	594	0	39	875	1306	
Rank 4's ghost cells:	0	0	838	347	0	1753	0	0	0	0	0	1507	0	0	0	0	
Rank 5's ghost cells:	294	124	518	269	1754	0	856	1296	553	0	23	970	0	0	0	0	
Rank 6's ghost cells:	1213	1419	4	20	0	872	0	1756	778	0	0	0	0	459	0	0	
Rank 7's ghost cells:	0	0	0	0	0	1296	1760	0	1483	90	0	0	0	0	0	0	
Rank 8's ghost cells:	0	19	0	0	0	553	773	1496	0	1944	600	840	0	796	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	90	1932	0	1026	26	1191	369	199	0	
Rank 10's ghost cells:	0	0	0	0	0	23	0	0	608	1032	0	1648	0	0	1199	0	
Rank 11's ghost cells:	0	100	0	595	1504	945	0	0	839	30	1657	0	0	126	1061	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	1191	0	0	0	2572	473	686	
Rank 13's ghost cells:	0	2500	0	39	0	0	458	0	791	381	0	117	2571	0	461	792	
Rank 14's ghost cells:	0	89	0	882	0	0	0	0	0	199	1195	1074	469	462	0	1933	
Rank 15's ghost cells:	0	68	0	1306	0	0	0	0	0	0	0	0	678	793	1933	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.7116         0.219022
    2          31.6692          0.57884
    3          22.5178          0.71103
    4            19.51         0.866424
    5          17.9763          0.92139
    6          14.1984         0.789841
    7          12.0562         0.849128
    8           11.818         0.980239
    9          10.0898         0.853767
   10           8.6806         0.860332
   11          8.64016         0.995342
   12          7.83189         0.906452
   13          6.72124         0.858189
   14          6.79808          1.01143
   15          6.38759         0.939617
   16           5.5018         0.861327
   17          5.49105         0.998046
   18           5.3698         0.977917
   19          4.68187          0.87189
   20          4.62661         0.988197
   21          4.54785         0.982977
   22          4.10061         0.901658
   23          4.06015         0.990134
   24          3.84932         0.948074
   25          3.50687         0.911035
   26          3.51199          1.00146
   27          3.29093         0.937055
   28           3.0534         0.927825
   29          3.08101          1.00904
   30          2.87244         0.932302
   31          2.78505         0.969576
   32          2.98118          1.07042
   33          3.00534           1.0081
   34          3.10354          1.03268
   35          3.07791         0.991743
   36          2.59625         0.843509
   37          2.14919         0.827807
   38          1.81308         0.843612
   39           1.5168         0.836584
   40          1.34167         0.884541
   41          1.17976         0.879321
   42          1.02916         0.872354
   43         0.919608         0.893548
   44         0.769092         0.836326
   45         0.647625         0.842064
   46         0.550545         0.850099
   47         0.449771         0.816957
   48          0.39403         0.876068
   49          0.33174         0.841916
   50         0.269973         0.813807
   51         0.235695         0.873034
   52         0.196451         0.833495
   53         0.163163         0.830555
   54         0.139952         0.857741
   55         0.115304         0.823886
   56        0.0975597         0.846106
   57        0.0803445         0.823542
   58        0.0663824         0.826222
   59         0.054664         0.823472
   60        0.0440972         0.806695
   61        0.0353142         0.800826
   62        0.0275515         0.780182
   63         0.021678         0.786817
=== rate=0.862046, T=10.1764, TIT=0.16153, IT=63

 Elapsed time: 10.1764
Rank 0: Matrix-vector product took 0.0547354 seconds
Rank 1: Matrix-vector product took 0.056636 seconds
Rank 2: Matrix-vector product took 0.0546735 seconds
Rank 3: Matrix-vector product took 0.0565389 seconds
Rank 4: Matrix-vector product took 0.0543336 seconds
Rank 5: Matrix-vector product took 0.0566937 seconds
Rank 6: Matrix-vector product took 0.0559502 seconds
Rank 7: Matrix-vector product took 0.0536514 seconds
Rank 8: Matrix-vector product took 0.0568358 seconds
Rank 9: Matrix-vector product took 0.0551717 seconds
Rank 10: Matrix-vector product took 0.0547287 seconds
Rank 11: Matrix-vector product took 0.0566964 seconds
Rank 12: Matrix-vector product took 0.05447 seconds
Rank 13: Matrix-vector product took 0.0577325 seconds
Rank 14: Matrix-vector product took 0.0567612 seconds
Rank 15: Matrix-vector product took 0.0553365 seconds
Average time for Matrix-vector product is 0.0556841 seconds

Rank 0: copyOwnerToAll took 0.00052555 seconds
Rank 1: copyOwnerToAll took 0.00077394 seconds
Rank 2: copyOwnerToAll took 0.00055597 seconds
Rank 3: copyOwnerToAll took 0.00072122 seconds
Rank 4: copyOwnerToAll took 0.00060878 seconds
Rank 5: copyOwnerToAll took 0.00078455 seconds
Rank 6: copyOwnerToAll took 0.00068844 seconds
Rank 7: copyOwnerToAll took 0.00040836 seconds
Rank 8: copyOwnerToAll took 0.00065448 seconds
Rank 9: copyOwnerToAll took 0.00067024 seconds
Rank 10: copyOwnerToAll took 0.00057544 seconds
Rank 11: copyOwnerToAll took 0.00082608 seconds
Rank 12: copyOwnerToAll took 0.00055331 seconds
Rank 13: copyOwnerToAll took 0.00079138 seconds
Rank 14: copyOwnerToAll took 0.00068031 seconds
Rank 15: copyOwnerToAll took 0.00059068 seconds
Average time for copyOwnertoAll is 0.000650546 seconds
