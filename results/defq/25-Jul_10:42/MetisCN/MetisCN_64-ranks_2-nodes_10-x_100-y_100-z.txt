Number of cores/ranks per node is: 32
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 1567
Cell on rank 1 before loadbalancing: 1578
Cell on rank 2 before loadbalancing: 1574
Cell on rank 3 before loadbalancing: 1575
Cell on rank 4 before loadbalancing: 1560
Cell on rank 5 before loadbalancing: 1556
Cell on rank 6 before loadbalancing: 1569
Cell on rank 7 before loadbalancing: 1555
Cell on rank 8 before loadbalancing: 1578
Cell on rank 9 before loadbalancing: 1546
Cell on rank 10 before loadbalancing: 1574
Cell on rank 11 before loadbalancing: 1570
Cell on rank 12 before loadbalancing: 1546
Cell on rank 13 before loadbalancing: 1546
Cell on rank 14 before loadbalancing: 1578
Cell on rank 15 before loadbalancing: 1578
Cell on rank 16 before loadbalancing: 1546
Cell on rank 17 before loadbalancing: 1546
Cell on rank 18 before loadbalancing: 1560
Cell on rank 19 before loadbalancing: 1554
Cell on rank 20 before loadbalancing: 1578
Cell on rank 21 before loadbalancing: 1546
Cell on rank 22 before loadbalancing: 1546
Cell on rank 23 before loadbalancing: 1578
Cell on rank 24 before loadbalancing: 1555
Cell on rank 25 before loadbalancing: 1546
Cell on rank 26 before loadbalancing: 1568
Cell on rank 27 before loadbalancing: 1560
Cell on rank 28 before loadbalancing: 1550
Cell on rank 29 before loadbalancing: 1546
Cell on rank 30 before loadbalancing: 1559
Cell on rank 31 before loadbalancing: 1570
Cell on rank 32 before loadbalancing: 1578
Cell on rank 33 before loadbalancing: 1560
Cell on rank 34 before loadbalancing: 1570
Cell on rank 35 before loadbalancing: 1560
Cell on rank 36 before loadbalancing: 1565
Cell on rank 37 before loadbalancing: 1549
Cell on rank 38 before loadbalancing: 1550
Cell on rank 39 before loadbalancing: 1570
Cell on rank 40 before loadbalancing: 1570
Cell on rank 41 before loadbalancing: 1576
Cell on rank 42 before loadbalancing: 1551
Cell on rank 43 before loadbalancing: 1578
Cell on rank 44 before loadbalancing: 1576
Cell on rank 45 before loadbalancing: 1549
Cell on rank 46 before loadbalancing: 1564
Cell on rank 47 before loadbalancing: 1551
Cell on rank 48 before loadbalancing: 1547
Cell on rank 49 before loadbalancing: 1577
Cell on rank 50 before loadbalancing: 1578
Cell on rank 51 before loadbalancing: 1578
Cell on rank 52 before loadbalancing: 1572
Cell on rank 53 before loadbalancing: 1578
Cell on rank 54 before loadbalancing: 1570
Cell on rank 55 before loadbalancing: 1570
Cell on rank 56 before loadbalancing: 1552
Cell on rank 57 before loadbalancing: 1546
Cell on rank 58 before loadbalancing: 1578
Cell on rank 59 before loadbalancing: 1560
Cell on rank 60 before loadbalancing: 1550
Cell on rank 61 before loadbalancing: 1547
Cell on rank 62 before loadbalancing: 1576
Cell on rank 63 before loadbalancing: 1546
Edge-cut: 17190
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	90	66	80	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	90	0	86	0	0	168	0	0	98	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	76	86	0	140	0	0	0	0	46	106	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	80	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	110	0	0	0	0	140	0	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	170	0	0	140	0	110	40	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	110	0	80	0	0	0	0	0	79	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	60	0	0	
From rank 7 to: 	0	0	0	0	125	40	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	30	0	
From rank 8 to: 	0	98	46	0	0	0	0	0	0	158	102	40	0	0	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	116	0	0	0	0	0	158	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	100	0	0	120	0	0	70	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	40	80	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	150	47	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	79	0	0	0	0	0	150	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	0	90	0	0	0	0	
From rank 14 to: 	0	52	0	0	0	70	10	0	58	0	60	0	47	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	80	0	0	0	100	0	173	60	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	43	110	0	40	60	0	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	37	0	120	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	43	37	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	50	0	0	25	0	0	0	0	0	0	0	0	0	0	0	0	110	0	160	0	0	0	0	0	0	0	0	0	0	170	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	118	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	42	0	0	118	0	116	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	123	0	100	0	0	0	0	0	0	110	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	72	90	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	90	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	95	0	115	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	100	0	150	90	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	20	100	0	0	0	0	0	0	0	0	0	0	0	0	60	70	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	90	0	0	130	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	95	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	70	0	0	0	120	0	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	10	0	0	110	0	0	0	30	20	150	50	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	90	0	0	0	90	0	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	100	0	0	0	0	0	0	0	30	0	9	93	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	130	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	83	0	100	0	0	0	120	100	0	0	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	90	0	140	0	0	0	40	0	0	0	0	0	0	0	0	65	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	70	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	155	0	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	155	0	103	60	0	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	98	0	110	0	102	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	40	70	0	60	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	94	56	102	0	120	0	70	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	97	0	0	0	0	40	0	0	60	0	114	0	161	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	70	110	0	0	8	116	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	76	60	0	0	0	0	9	0	0	0	0	0	0	0	0	0	0	0	0	191	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	103	0	0	0	0	0	0	0	0	0	161	8	181	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	70	0	0	0	0	0	0	0	0	0	0	12	111	0	87	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	0	0	70	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	65	110	0	0	0	0	0	0	0	0	0	0	0	0	0	90	88	82	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	45	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	76	0	70	0	0	0	0	0	0	89	0	0	95	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	148	0	0	96	0	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	82	86	148	0	60	112	20	0	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	102	130	120	0	0	100	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	112	112	0	0	0	110	0	40	0	36	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	14	130	0	0	120	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	0	0	70	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	130	100	20	116	17	0	0	
From rank 57 to: 	0	0	0	0	0	0	60	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	117	0	90	0	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	40	0	60	90	0	0	157	0	0	0	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	107	157	0	0	0	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	0	0	0	46	0	0	116	0	0	0	0	114	50	90	
From rank 61 to: 	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	90	0	0	114	0	60	0	
From rank 62 to: 	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	60	0	186	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	95	0	0	0	0	0	0	0	0	0	0	90	0	176	0	

Edge-cut for node partition: 1145

loadb
After loadbalancing process 8 has 1910 cells.
After loadbalancing process 3 has 1890 cells.
After loadbalancing process 4 has 1814 cells.
After loadbalancing process 9 has 2078 cells.
After loadbalancing process 61 has 2071 cells.
After loadbalancing process 41 has 1900 cells.
After loadbalancing process 2 has 2060 cells.
After loadbalancing process 42 has 1964 cells.
After loadbalancing process 50 has 1900 cells.
After loadbalancing process 60 has 2035 cells.
After loadbalancing process 6 has 2020 cells.
After loadbalancing process 16 has 1982 cells.
After loadbalancing process 18 has 1912 cells.
After loadbalancing process 5 has 1923 cells.
After loadbalancing process 43 has 1810 cells.
After loadbalancing process 23 has 1880 cells.
After loadbalancing process 51 has 2079 cells.
After loadbalancing process 26 has 2025 cells.
After loadbalancing process 46 has 2065 cells.
After loadbalancing process 17 has 2102 cells.
After loadbalancing process 20 has 2084 cells.
After loadbalancing process 36 has 2050 cells.
After loadbalancing process 22 has 1930 cells.
After loadbalancing process 10 has 2065 cells.
After loadbalancing process 44 has 1916 cells.
After loadbalancing process 1 has 2060 cells.
After loadbalancing process 7 has 1940 cells.
After loadbalancing process 19 has 2086 cells.
After loadbalancing process 11 has 2152 cells.
After loadbalancing process 40 has 2080 cells.
After loadbalancing process 53 has 1934 cells.
After loadbalancing process 21 has 2058 cells.
After loadbalancing process 24 has 2045 cells.
After loadbalancing process 63 has 2070 cells.
After loadbalancing process 15 has 1911 cells.
After loadbalancing process 27 has 1934 cells.
After loadbalancing process 28 has 2055 cells.
After loadbalancing process 0 has 2090 cells.
After loadbalancing process 30 has 2082 cells.
After loadbalancing process 57 has 2020 cells.
After loadbalancing process 31 has 2097 cells.
After loadbalancing process 13 has 2164 cells.
After loadbalancing process 14 has 2044 cells.
After loadbalancing process 29 has 2014 cells.
After loadbalancing process 33 has 2082 cells.
After loadbalancing process 54 has 2020 cells.
After loadbalancing process 58 has 2109 cells.
After loadbalancing process 39 has 2085 cells.
After loadbalancing process 45 has 2046 cells.
After loadbalancing process 56 has 2050 cells.
After loadbalancing process 12 has 2056 cells.
After loadbalancing process 35 has 1920 cells.
After loadbalancing process 48 has 2049 cells.
After loadbalancing process 32 has 2058 cells.
After loadbalancing process 62 has 2119 cells.
After loadbalancing process 34 has 2028 cells.
After loadbalancing process 47 has 2181 cells.
After loadbalancing process 38 has 2058 cells.
After loadbalancing process 52 has 1816 cells.
After loadbalancing process 55 has 1950 cells.
After loadbalancing process 37 has 2086 cells.
After loadbalancing process 49 has 1915 cells.
After loadbalancing process 59 has 2030 cells.
After loadbalancing process 25 has 2063 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	90	100	0	0	0	0	0	0	0	30	0	9	93	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	83	0	100	0	0	0	120	100	0	0	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	100	90	0	140	0	0	0	40	0	0	0	0	0	0	0	0	65	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	140	0	0	0	0	70	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	155	0	0	0	94	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	155	0	103	60	0	56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	120	0	0	0	98	0	110	0	102	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	100	40	70	0	60	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	130	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	94	56	102	0	120	0	70	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	30	97	0	0	0	0	40	0	0	60	0	114	0	161	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	200	70	110	0	0	8	116	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	9	0	0	0	0	0	0	0	0	0	0	0	0	191	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	76	60	0	0	0	0	
Rank 13's ghost cells:	103	0	0	0	0	0	0	0	0	0	161	8	181	0	92	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	12	111	0	87	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	70	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	70	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	50	
Rank 16's ghost cells:	0	0	65	110	0	0	0	0	0	0	0	0	0	0	0	0	0	90	88	82	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	60	0	45	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	76	0	70	0	0	0	0	0	0	89	0	0	95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	148	0	0	96	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	82	86	148	0	60	112	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	102	130	120	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	112	112	0	0	0	110	0	40	0	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	14	130	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	120	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	130	100	20	116	17	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	117	0	90	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	40	0	60	90	0	0	157	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	107	157	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	89	0	0	0	46	0	0	116	0	0	0	0	114	50	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	90	0	0	114	0	60	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	60	0	186	0	0	0	0	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	0	0	
Rank 31's ghost cells:	130	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	95	0	0	0	0	0	0	0	0	0	0	90	0	176	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	66	80	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0	0	0	0	0	95	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	86	0	0	168	0	0	98	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	86	0	140	0	0	0	0	46	106	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	25	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	0	0	140	0	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	0	0	0	20	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	140	0	110	40	0	0	0	0	0	0	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	60	0	0	0	0	0	0	0	110	0	80	0	0	0	0	0	79	20	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	30	0	0	0	0	0	125	40	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	115	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	98	46	0	0	0	0	0	0	158	102	40	0	0	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	0	0	0	0	0	158	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	120	0	0	70	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	80	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	47	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	0	90	0	0	0	0	0	0	0	0	0	0	79	0	0	0	0	0	150	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	52	0	0	0	70	10	0	58	0	60	0	47	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	100	0	173	60	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	43	110	0	40	60	0	0	0	0	0	0	0	80	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	37	0	120	42	0	0	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	43	37	0	160	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	25	0	0	0	0	0	0	0	0	0	0	0	0	110	0	160	0	0	0	0	0	0	0	0	0	0	170	10	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	118	0	0	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	42	0	0	118	0	116	72	0	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	123	0	100	0	0	0	0	0	0	110	81	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	72	90	0	0	0	0	0	0	0	0	90	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	95	0	115	70	0	0	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	34	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	100	0	0	0	0	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	86	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	85	100	0	150	90	0	30	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	60	70	70	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	20	100	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	90	0	0	130	150	0	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	70	0	0	0	120	0	50	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	10	0	0	110	0	0	0	30	20	150	50	0	110	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	90	0	0	0	90	0	0	119	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          40.1909         0.250802
    2          24.5836          0.61167
    3          22.0589         0.897302
    4          21.5441         0.976663
    5          11.1341         0.516806
    6          5.47569         0.491794
    7          4.25038         0.776226
    8          2.57094         0.604873
    9          1.29105         0.502171
   10         0.979936         0.759023
   11         0.649373         0.662669
   12         0.369795         0.569464
   13         0.263183         0.711701
   14         0.172067         0.653792
   15         0.110956          0.64484
   16        0.0684906         0.617278
   17        0.0482451         0.704405
   18        0.0308965         0.640408
   19        0.0191525          0.61989
   20        0.0122457         0.639379
=== rate=0.622529, T=0.113551, TIT=0.00567757, IT=20

 Elapsed time: 0.113551
Rank 0: Matrix-vector product took 0.00165923 seconds
Rank 1: Matrix-vector product took 0.00162751 seconds
Rank 2: Matrix-vector product took 0.00163275 seconds
Rank 3: Matrix-vector product took 0.00149867 seconds
Rank 4: Matrix-vector product took 0.00144191 seconds
Rank 5: Matrix-vector product took 0.00151838 seconds
Rank 6: Matrix-vector product took 0.00159491 seconds
Rank 7: Matrix-vector product took 0.00154203 seconds
Rank 8: Matrix-vector product took 0.00151437 seconds
Rank 9: Matrix-vector product took 0.00163799 seconds
Rank 10: Matrix-vector product took 0.00162873 seconds
Rank 11: Matrix-vector product took 0.00169006 seconds
Rank 12: Matrix-vector product took 0.00163561 seconds
Rank 13: Matrix-vector product took 0.00169372 seconds
Rank 14: Matrix-vector product took 0.00161652 seconds
Rank 15: Matrix-vector product took 0.00150589 seconds
Rank 16: Matrix-vector product took 0.00157018 seconds
Rank 17: Matrix-vector product took 0.00165396 seconds
Rank 18: Matrix-vector product took 0.00151179 seconds
Rank 19: Matrix-vector product took 0.00165072 seconds
Rank 20: Matrix-vector product took 0.00164818 seconds
Rank 21: Matrix-vector product took 0.00163417 seconds
Rank 22: Matrix-vector product took 0.00152801 seconds
Rank 23: Matrix-vector product took 0.00146495 seconds
Rank 24: Matrix-vector product took 0.00162343 seconds
Rank 25: Matrix-vector product took 0.00159858 seconds
Rank 26: Matrix-vector product took 0.00160857 seconds
Rank 27: Matrix-vector product took 0.00152767 seconds
Rank 28: Matrix-vector product took 0.00160889 seconds
Rank 29: Matrix-vector product took 0.00159749 seconds
Rank 30: Matrix-vector product took 0.00164773 seconds
Rank 31: Matrix-vector product took 0.00161192 seconds
Rank 32: Matrix-vector product took 0.0016343 seconds
Rank 33: Matrix-vector product took 0.00169168 seconds
Rank 34: Matrix-vector product took 0.00161388 seconds
Rank 35: Matrix-vector product took 0.00152279 seconds
Rank 36: Matrix-vector product took 0.00162597 seconds
Rank 37: Matrix-vector product took 0.00166089 seconds
Rank 38: Matrix-vector product took 0.00164347 seconds
Rank 39: Matrix-vector product took 0.00165072 seconds
Rank 40: Matrix-vector product took 0.00164467 seconds
Rank 41: Matrix-vector product took 0.00148074 seconds
Rank 42: Matrix-vector product took 0.00156446 seconds
Rank 43: Matrix-vector product took 0.00140921 seconds
Rank 44: Matrix-vector product took 0.00152565 seconds
Rank 45: Matrix-vector product took 0.00161295 seconds
Rank 46: Matrix-vector product took 0.00163929 seconds
Rank 47: Matrix-vector product took 0.00170659 seconds
Rank 48: Matrix-vector product took 0.0016217 seconds
Rank 49: Matrix-vector product took 0.00151165 seconds
Rank 50: Matrix-vector product took 0.00151062 seconds
Rank 51: Matrix-vector product took 0.00165199 seconds
Rank 52: Matrix-vector product took 0.00144273 seconds
Rank 53: Matrix-vector product took 0.00154868 seconds
Rank 54: Matrix-vector product took 0.00158082 seconds
Rank 55: Matrix-vector product took 0.00155538 seconds
Rank 56: Matrix-vector product took 0.00162817 seconds
Rank 57: Matrix-vector product took 0.00159305 seconds
Rank 58: Matrix-vector product took 0.00167451 seconds
Rank 59: Matrix-vector product took 0.00161305 seconds
Rank 60: Matrix-vector product took 0.00159294 seconds
Rank 61: Matrix-vector product took 0.0016396 seconds
Rank 62: Matrix-vector product took 0.00167397 seconds
Rank 63: Matrix-vector product took 0.00164055 seconds
Average time for Matrix-vector product is 0.00159424 seconds

Rank 0: copyOwnerToAll took 8.963e-05 seconds
Rank 1: copyOwnerToAll took 6.978e-05 seconds
Rank 2: copyOwnerToAll took 7.979e-05 seconds
Rank 3: copyOwnerToAll took 4.783e-05 seconds
Rank 4: copyOwnerToAll took 4.035e-05 seconds
Rank 5: copyOwnerToAll took 5.682e-05 seconds
Rank 6: copyOwnerToAll took 7.225e-05 seconds
Rank 7: copyOwnerToAll took 5.699e-05 seconds
Rank 8: copyOwnerToAll took 5.85e-05 seconds
Rank 9: copyOwnerToAll took 8.778e-05 seconds
Rank 10: copyOwnerToAll took 0.00013117 seconds
Rank 11: copyOwnerToAll took 7.748e-05 seconds
Rank 12: copyOwnerToAll took 0.00016032 seconds
Rank 13: copyOwnerToAll took 0.00013107 seconds
Rank 14: copyOwnerToAll took 0.0001268 seconds
Rank 15: copyOwnerToAll took 0.00013388 seconds
Rank 16: copyOwnerToAll took 7.114e-05 seconds
Rank 17: copyOwnerToAll took 8.6e-05 seconds
Rank 18: copyOwnerToAll took 4.429e-05 seconds
Rank 19: copyOwnerToAll took 7.28e-05 seconds
Rank 20: copyOwnerToAll took 6.878e-05 seconds
Rank 21: copyOwnerToAll took 8.243e-05 seconds
Rank 22: copyOwnerToAll took 8.35e-05 seconds
Rank 23: copyOwnerToAll took 5.099e-05 seconds
Rank 24: copyOwnerToAll took 6.907e-05 seconds
Rank 25: copyOwnerToAll took 0.00012783 seconds
Rank 26: copyOwnerToAll took 7.265e-05 seconds
Rank 27: copyOwnerToAll took 0.00019123 seconds
Rank 28: copyOwnerToAll took 7.921e-05 seconds
Rank 29: copyOwnerToAll took 0.00012109 seconds
Rank 30: copyOwnerToAll took 0.00015533 seconds
Rank 31: copyOwnerToAll took 7.987e-05 seconds
Rank 32: copyOwnerToAll took 9.023e-05 seconds
Rank 33: copyOwnerToAll took 0.00010177 seconds
Rank 34: copyOwnerToAll took 7.9e-05 seconds
Rank 35: copyOwnerToAll took 6.552e-05 seconds
Rank 36: copyOwnerToAll took 7.407e-05 seconds
Rank 37: copyOwnerToAll took 8.009e-05 seconds
Rank 38: copyOwnerToAll took 0.00012103 seconds
Rank 39: copyOwnerToAll took 0.00016684 seconds
Rank 40: copyOwnerToAll took 8.013e-05 seconds
Rank 41: copyOwnerToAll took 5.601e-05 seconds
Rank 42: copyOwnerToAll took 6.716e-05 seconds
Rank 43: copyOwnerToAll took 4.747e-05 seconds
Rank 44: copyOwnerToAll took 6.529e-05 seconds
Rank 45: copyOwnerToAll took 0.00013359 seconds
Rank 46: copyOwnerToAll took 9.037e-05 seconds
Rank 47: copyOwnerToAll took 7.443e-05 seconds
Rank 48: copyOwnerToAll took 0.00011766 seconds
Rank 49: copyOwnerToAll took 6.212e-05 seconds
Rank 50: copyOwnerToAll took 5.502e-05 seconds
Rank 51: copyOwnerToAll took 7.742e-05 seconds
Rank 52: copyOwnerToAll took 4.599e-05 seconds
Rank 53: copyOwnerToAll took 7.868e-05 seconds
Rank 54: copyOwnerToAll took 8.131e-05 seconds
Rank 55: copyOwnerToAll took 0.00013181 seconds
Rank 56: copyOwnerToAll took 7.79e-05 seconds
Rank 57: copyOwnerToAll took 0.00012152 seconds
Rank 58: copyOwnerToAll took 0.00014944 seconds
Rank 59: copyOwnerToAll took 0.00017487 seconds
Rank 60: copyOwnerToAll took 8.62e-05 seconds
Rank 61: copyOwnerToAll took 8.992e-05 seconds
Rank 62: copyOwnerToAll took 0.00014535 seconds
Rank 63: copyOwnerToAll took 0.00011644 seconds
Average time for copyOwnertoAll is 9.18953e-05 seconds
