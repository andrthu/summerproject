Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 247808
Cell on rank 1 before loadbalancing: 250352
Cell on rank 2 before loadbalancing: 252288
Cell on rank 3 before loadbalancing: 249552
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2308	2271	169	262	2061	0	0	
From rank 1 to: 	2304	0	169	2294	2221	0	386	0	
From rank 2 to: 	2274	159	0	2608	106	658	109	2371	
From rank 3 to: 	169	2295	2631	0	0	0	2054	282	
From rank 4 to: 	284	2221	110	0	0	2494	2326	279	
From rank 5 to: 	2061	0	658	0	2497	0	0	2239	
From rank 6 to: 	0	400	96	2064	2347	0	0	2666	
From rank 7 to: 	0	0	2348	282	279	2233	2679	0	
loadb
After loadbalancing process 0 has 130975 cells.
After loadbalancing process 3 has 132607 cells.
After loadbalancing process 2 has 133461 cells.
After loadbalancing process 6 has 132349 cells.
After loadbalancing process 5 has 133201 cells.
After loadbalancing process 7 has 132597 cells.
After loadbalancing process 1 has 131278 cells.
After loadbalancing process 4 has 134256 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2308	2271	169	262	2061	0	0	
Rank 1's ghost cells:	2304	0	169	2294	2221	0	386	0	
Rank 2's ghost cells:	2274	159	0	2608	106	658	109	2371	
Rank 3's ghost cells:	169	2295	2631	0	0	0	2054	282	
Rank 4's ghost cells:	284	2221	110	0	0	2494	2326	279	
Rank 5's ghost cells:	2061	0	658	0	2497	0	0	2239	
Rank 6's ghost cells:	0	400	96	2064	2347	0	0	2666	
Rank 7's ghost cells:	0	0	2348	282	279	2233	2679	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.1672         0.212839
    2          30.5752         0.575076
    3          21.4624         0.701954
    4          17.4175         0.811535
    5          16.0108         0.919239
    6          14.0657         0.878514
    7           11.438         0.813179
    8          10.7498         0.939838
    9          10.1682         0.945891
   10          8.33443         0.819659
   11           7.7798         0.933453
   12          7.87635          1.01241
   13          6.61266         0.839559
   14          5.85514         0.885444
   15          6.04073           1.0317
   16          5.66985         0.938603
   17          4.82584         0.851142
   18            4.777         0.989879
   19          4.75104         0.994566
   20          4.16853         0.877393
   21           4.0277         0.966215
   22          4.01238         0.996196
   23          3.59903         0.896982
   24          3.44699         0.957756
   25           3.4322         0.995709
   26           3.0811         0.897704
   27          2.93885         0.953831
   28          2.97732          1.01309
   29          2.75025         0.923732
   30          2.67445         0.972438
   31          2.85902          1.06901
   32          2.90958          1.01768
   33          2.98927          1.02739
   34          2.93976         0.983437
   35          2.36492          0.80446
   36          1.86088          0.78687
   37          1.69734         0.912116
   38          1.51223         0.890938
   39          1.25205         0.827949
   40          1.09906         0.877813
   41         0.997133         0.907258
   42         0.833007         0.835402
   43         0.681425          0.81803
   44         0.599538          0.87983
   45         0.511546         0.853233
   46          0.41876         0.818617
   47         0.357482         0.853668
   48         0.295682         0.827125
   49         0.249339         0.843266
   50         0.213416         0.855926
   51         0.169721         0.795263
   52         0.149048          0.87819
   53         0.120857         0.810863
   54        0.0978008         0.809226
   55        0.0855849         0.875094
   56        0.0648984         0.758293
   57        0.0549487         0.846688
   58        0.0436633         0.794618
   59         0.033595          0.76941
   60         0.027269         0.811698
   61        0.0205663         0.754203
=== rate=0.857121, T=18.5504, TIT=0.304104, IT=61

 Elapsed time: 18.5504
Rank 0: Matrix-vector product took 0.10756 seconds
Rank 1: Matrix-vector product took 0.106814 seconds
Rank 2: Matrix-vector product took 0.108742 seconds
Rank 3: Matrix-vector product took 0.108551 seconds
Rank 4: Matrix-vector product took 0.110226 seconds
Rank 5: Matrix-vector product took 0.109371 seconds
Rank 6: Matrix-vector product took 0.108354 seconds
Rank 7: Matrix-vector product took 0.108328 seconds
Average time for Matrix-vector product is 0.108493 seconds

Rank 0: copyOwnerToAll took 0.000626849 seconds
Rank 1: copyOwnerToAll took 0.000674869 seconds
Rank 2: copyOwnerToAll took 0.00072602 seconds
Rank 3: copyOwnerToAll took 0.00072106 seconds
Rank 4: copyOwnerToAll took 0.000720439 seconds
Rank 5: copyOwnerToAll took 0.000668199 seconds
Rank 6: copyOwnerToAll took 0.000751083 seconds
Rank 7: copyOwnerToAll took 0.000710742 seconds
Average time for copyOwnertoAll is 0.000699908 seconds
Number of cores/ranks per node is: 2
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 247808
Cell on rank 1 before loadbalancing: 250352
Cell on rank 2 before loadbalancing: 252288
Cell on rank 3 before loadbalancing: 249552
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2308	2271	169	262	2061	0	0	
From rank 1 to: 	2304	0	169	2294	2221	0	386	0	
From rank 2 to: 	2274	159	0	2608	106	658	109	2371	
From rank 3 to: 	169	2295	2631	0	0	0	2054	282	
From rank 4 to: 	284	2221	110	0	0	2494	2326	279	
From rank 5 to: 	2061	0	658	0	2497	0	0	2239	
From rank 6 to: 	0	400	96	2064	2347	0	0	2666	
From rank 7 to: 	0	0	2348	282	279	2233	2679	0	
loadb
After loadbalancing process 6 has 132349 cells.
After loadbalancing process 0 has 130975 cells.
After loadbalancing process 1 has 131278 cells.
After loadbalancing process 3 has 132607 cells.
After loadbalancing process 7 has 132597 cells.
After loadbalancing process 4 has 134256 cells.
After loadbalancing process 5 has 133201 cells.
After loadbalancing process 2 has 133461 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2308	2271	169	262	2061	0	0	
Rank 1's ghost cells:	2304	0	169	2294	2221	0	386	0	
Rank 2's ghost cells:	2274	159	0	2608	106	658	109	2371	
Rank 3's ghost cells:	169	2295	2631	0	0	0	2054	282	
Rank 4's ghost cells:	284	2221	110	0	0	2494	2326	279	
Rank 5's ghost cells:	2061	0	658	0	2497	0	0	2239	
Rank 6's ghost cells:	0	400	96	2064	2347	0	0	2666	
Rank 7's ghost cells:	0	0	2348	282	279	2233	2679	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.1672         0.212839
    2          30.5752         0.575076
    3          21.4624         0.701954
    4          17.4175         0.811535
    5          16.0108         0.919239
    6          14.0657         0.878514
    7           11.438         0.813179
    8          10.7498         0.939838
    9          10.1682         0.945891
   10          8.33443         0.819659
   11           7.7798         0.933453
   12          7.87635          1.01241
   13          6.61266         0.839559
   14          5.85514         0.885444
   15          6.04073           1.0317
   16          5.66985         0.938603
   17          4.82584         0.851142
   18            4.777         0.989879
   19          4.75104         0.994566
   20          4.16853         0.877393
   21           4.0277         0.966215
   22          4.01238         0.996196
   23          3.59903         0.896982
   24          3.44699         0.957756
   25           3.4322         0.995709
   26           3.0811         0.897704
   27          2.93885         0.953831
   28          2.97732          1.01309
   29          2.75025         0.923732
   30          2.67445         0.972438
   31          2.85902          1.06901
   32          2.90958          1.01768
   33          2.98927          1.02739
   34          2.93976         0.983437
   35          2.36492          0.80446
   36          1.86088          0.78687
   37          1.69734         0.912116
   38          1.51223         0.890938
   39          1.25205         0.827949
   40          1.09906         0.877813
   41         0.997133         0.907258
   42         0.833007         0.835402
   43         0.681425          0.81803
   44         0.599538          0.87983
   45         0.511546         0.853233
   46          0.41876         0.818617
   47         0.357482         0.853668
   48         0.295682         0.827125
   49         0.249339         0.843266
   50         0.213416         0.855926
   51         0.169721         0.795263
   52         0.149048          0.87819
   53         0.120857         0.810863
   54        0.0978008         0.809226
   55        0.0855849         0.875094
   56        0.0648984         0.758293
   57        0.0549487         0.846688
   58        0.0436633         0.794618
   59         0.033595          0.76941
   60         0.027269         0.811698
   61        0.0205663         0.754203
=== rate=0.857121, T=18.4843, TIT=0.303022, IT=61

 Elapsed time: 18.4843
Rank 0: Matrix-vector product took 0.106491 seconds
Rank 1: Matrix-vector product took 0.107853 seconds
Rank 2: Matrix-vector product took 0.10983 seconds
Rank 3: Matrix-vector product took 0.108471 seconds
Rank 4: Matrix-vector product took 0.109708 seconds
Rank 5: Matrix-vector product took 0.109493 seconds
Rank 6: Matrix-vector product took 0.108802 seconds
Rank 7: Matrix-vector product took 0.108433 seconds
Average time for Matrix-vector product is 0.108635 seconds

Rank 0: copyOwnerToAll took 0.000600709 seconds
Rank 1: copyOwnerToAll took 0.000676819 seconds
Rank 2: copyOwnerToAll took 0.000732671 seconds
Rank 3: copyOwnerToAll took 0.000694881 seconds
Rank 4: copyOwnerToAll took 0.00067408 seconds
Rank 5: copyOwnerToAll took 0.00068194 seconds
Rank 6: copyOwnerToAll took 0.000676712 seconds
Rank 7: copyOwnerToAll took 0.000692752 seconds
Average time for copyOwnertoAll is 0.000678821 seconds
