Number of cores/ranks per node is: 8
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 497728
Cell on rank 1 before loadbalancing: 502272
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2063	0	0	1244	217	0	0	0	0	0	0	0	1303	0	0	
From rank 1 to: 	2063	0	1365	1031	993	0	0	0	0	0	410	0	0	698	0	0	
From rank 2 to: 	0	1365	0	1872	0	0	0	0	0	0	1306	0	0	0	0	0	
From rank 3 to: 	0	1017	1874	0	492	0	1168	1398	0	91	831	0	0	8	0	0	
From rank 4 to: 	1250	992	0	486	0	2131	806	331	0	9	0	0	0	635	548	541	
From rank 5 to: 	217	0	0	0	2117	0	598	651	0	0	0	0	0	0	138	743	
From rank 6 to: 	0	0	0	1184	781	598	0	2006	1104	1378	40	0	0	6	504	0	
From rank 7 to: 	0	0	0	1398	324	651	2010	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	1104	0	0	1875	0	0	0	0	1408	0	
From rank 9 to: 	0	0	0	90	9	0	1396	0	1860	0	1109	1303	0	120	1151	0	
From rank 10 to: 	0	412	1306	851	0	0	40	0	0	1098	0	2280	31	1360	70	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	1304	2283	0	1067	40	200	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	31	1064	0	2188	755	607	
From rank 13 to: 	1302	698	0	8	654	0	6	0	0	119	1357	34	2191	0	670	514	
From rank 14 to: 	0	0	0	0	543	138	504	0	1383	1170	62	203	765	669	0	2214	
From rank 15 to: 	0	0	0	0	539	754	0	0	0	0	0	0	607	509	2213	0	
loadb
After loadbalancing process 2 has 66145 cells.
After loadbalancing process 11 has 68069 cells.
After loadbalancing process 7 has 66463 cells.
After loadbalancing process 5 has 66994 cells.
After loadbalancing process 10 has 70122 cells.
After loadbalancing process 15 has 67752 cells.
After loadbalancing process 8 has 66643 cells.
After loadbalancing process 3 has 69506 cells.
After loadbalancing process 0 has 66595 cells.
After loadbalancing process 4 has 70423 cells.
After loadbalancing process 13 has 70063 cells.
After loadbalancing process 6 has 70071 cells.
After loadbalancing process 1 has 68517 cells.
After loadbalancing process 14 has 70785 cells.
After loadbalancing process 9 has 70103 cells.
After loadbalancing process 12 has 66973 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2063	0	0	1244	217	0	0	0	0	0	0	0	1303	0	0	
Rank 1's ghost cells:	2063	0	1365	1031	993	0	0	0	0	0	410	0	0	698	0	0	
Rank 2's ghost cells:	0	1365	0	1872	0	0	0	0	0	0	1306	0	0	0	0	0	
Rank 3's ghost cells:	0	1017	1874	0	492	0	1168	1398	0	91	831	0	0	8	0	0	
Rank 4's ghost cells:	1250	992	0	486	0	2131	806	331	0	9	0	0	0	635	548	541	
Rank 5's ghost cells:	217	0	0	0	2117	0	598	651	0	0	0	0	0	0	138	743	
Rank 6's ghost cells:	0	0	0	1184	781	598	0	2006	1104	1378	40	0	0	6	504	0	
Rank 7's ghost cells:	0	0	0	1398	324	651	2010	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	1104	0	0	1875	0	0	0	0	1408	0	
Rank 9's ghost cells:	0	0	0	90	9	0	1396	0	1860	0	1109	1303	0	120	1151	0	
Rank 10's ghost cells:	0	412	1306	851	0	0	40	0	0	1098	0	2280	31	1360	70	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	1304	2283	0	1067	40	200	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	31	1064	0	2188	755	607	
Rank 13's ghost cells:	1302	698	0	8	654	0	6	0	0	119	1357	34	2191	0	670	514	
Rank 14's ghost cells:	0	0	0	0	543	138	504	0	1383	1170	62	203	765	669	0	2214	
Rank 15's ghost cells:	0	0	0	0	539	754	0	0	0	0	0	0	607	509	2213	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.5641         0.218431
    2          31.6312         0.579708
    3          22.4664          0.71026
    4          19.3558         0.861546
    5          17.7985         0.919544
    6          14.1487         0.794938
    7          12.0635         0.852616
    8          11.8021         0.978334
    9          10.0537         0.851855
   10          8.75803         0.871129
   11          8.78236          1.00278
   12          7.75368          0.88287
   13          6.76899         0.873003
   14          6.88768          1.01753
   15          6.21372         0.902151
   16          5.48537         0.882783
   17          5.62149          1.02482
   18          5.28622         0.940358
   19          4.71198         0.891371
   20          4.78574          1.01565
   21          4.46226         0.932407
   22          4.06841         0.911738
   23          4.09107          1.00557
   24          3.80538         0.930167
   25          3.54149         0.930653
   26          3.52535         0.995444
   27          3.20978         0.910484
   28          3.03349         0.945078
   29           3.0503          1.00554
   30          2.84441         0.932502
   31          2.83598         0.997036
   32          2.98018          1.05085
   33          2.99483          1.00492
   34          3.16904          1.05817
   35          3.11333         0.982418
   36          2.64146         0.848438
   37           2.2008         0.833173
   38          1.76734         0.803045
   39          1.46202         0.827245
   40           1.3002         0.889316
   41          1.14873         0.883505
   42          1.03345         0.899645
   43         0.900492         0.871344
   44         0.737735         0.819257
   45         0.646193         0.875915
   46          0.55323         0.856137
   47         0.446314         0.806743
   48         0.391505         0.877195
   49         0.326044         0.832796
   50         0.265214         0.813431
   51         0.233815          0.88161
   52         0.190859         0.816282
   53         0.156713         0.821092
   54         0.136616          0.87176
   55         0.111842         0.818661
   56        0.0946485         0.846268
   57        0.0785997         0.830438
   58        0.0642048         0.816858
   59        0.0524932          0.81759
   60        0.0414442         0.789516
   61        0.0328156         0.791801
   62          0.02572         0.783775
   63         0.020152         0.783516
=== rate=0.861048, T=10.2504, TIT=0.162705, IT=63

 Elapsed time: 10.2504
Rank 0: Matrix-vector product took 0.0542782 seconds
Rank 1: Matrix-vector product took 0.0561691 seconds
Rank 2: Matrix-vector product took 0.0536073 seconds
Rank 3: Matrix-vector product took 0.057168 seconds
Rank 4: Matrix-vector product took 0.0574184 seconds
Rank 5: Matrix-vector product took 0.0546121 seconds
Rank 6: Matrix-vector product took 0.0568166 seconds
Rank 7: Matrix-vector product took 0.0542735 seconds
Rank 8: Matrix-vector product took 0.054298 seconds
Rank 9: Matrix-vector product took 0.0570446 seconds
Rank 10: Matrix-vector product took 0.0572376 seconds
Rank 11: Matrix-vector product took 0.0558295 seconds
Rank 12: Matrix-vector product took 0.0543642 seconds
Rank 13: Matrix-vector product took 0.0572526 seconds
Rank 14: Matrix-vector product took 0.0587361 seconds
Rank 15: Matrix-vector product took 0.0554142 seconds
Average time for Matrix-vector product is 0.0559075 seconds

Rank 0: copyOwnerToAll took 0.000725929 seconds
Rank 1: copyOwnerToAll took 0.000733369 seconds
Rank 2: copyOwnerToAll took 0.000626659 seconds
Rank 3: copyOwnerToAll took 0.000740999 seconds
Rank 4: copyOwnerToAll took 0.000757999 seconds
Rank 5: copyOwnerToAll took 0.000662469 seconds
Rank 6: copyOwnerToAll took 0.000807319 seconds
Rank 7: copyOwnerToAll took 0.000581909 seconds
Rank 8: copyOwnerToAll took 0.00062633 seconds
Rank 9: copyOwnerToAll took 0.00064518 seconds
Rank 10: copyOwnerToAll took 0.00072368 seconds
Rank 11: copyOwnerToAll took 0.00055329 seconds
Rank 12: copyOwnerToAll took 0.00056872 seconds
Rank 13: copyOwnerToAll took 0.00082029 seconds
Rank 14: copyOwnerToAll took 0.00069019 seconds
Rank 15: copyOwnerToAll took 0.00063663 seconds
Average time for copyOwnertoAll is 0.00068131 seconds
Number of cores/ranks per node is: 8
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 497728
Cell on rank 1 before loadbalancing: 502272
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	2063	0	0	1244	217	0	0	0	0	0	0	0	1303	0	0	
From rank 1 to: 	2063	0	1365	1031	993	0	0	0	0	0	410	0	0	698	0	0	
From rank 2 to: 	0	1365	0	1872	0	0	0	0	0	0	1306	0	0	0	0	0	
From rank 3 to: 	0	1017	1874	0	492	0	1168	1398	0	91	831	0	0	8	0	0	
From rank 4 to: 	1250	992	0	486	0	2131	806	331	0	9	0	0	0	635	548	541	
From rank 5 to: 	217	0	0	0	2117	0	598	651	0	0	0	0	0	0	138	743	
From rank 6 to: 	0	0	0	1184	781	598	0	2006	1104	1378	40	0	0	6	504	0	
From rank 7 to: 	0	0	0	1398	324	651	2010	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	1104	0	0	1875	0	0	0	0	1408	0	
From rank 9 to: 	0	0	0	90	9	0	1396	0	1860	0	1109	1303	0	120	1151	0	
From rank 10 to: 	0	412	1306	851	0	0	40	0	0	1098	0	2280	31	1360	70	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	1304	2283	0	1067	40	200	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	31	1064	0	2188	755	607	
From rank 13 to: 	1302	698	0	8	654	0	6	0	0	119	1357	34	2191	0	670	514	
From rank 14 to: 	0	0	0	0	543	138	504	0	1383	1170	62	203	765	669	0	2214	
From rank 15 to: 	0	0	0	0	539	754	0	0	0	0	0	0	607	509	2213	0	
loadb
After loadbalancing process 7 has 66463 cells.
After loadbalancing process 12 has 66973 cells.
After loadbalancing process 1 has 68517 cells.
After loadbalancing process 11 has 68069 cells.
After loadbalancing process 6 has 70071 cells.
After loadbalancing process 2 has 66145 cells.
After loadbalancing process 3 has 69506 cells.
After loadbalancing process 0 has 66595 cells.
After loadbalancing process 15 has 67752 cells.
After loadbalancing process 8 has 66643 cells.
After loadbalancing process 9 has 70103 cells.
After loadbalancing process 5 has 66994 cells.
After loadbalancing process 4 has 70423 cells.
After loadbalancing process 13 has 70063 cells.
After loadbalancing process 10 has 70122 cells.
After loadbalancing process 14 has 70785 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	2063	0	0	1244	217	0	0	0	0	0	0	0	1303	0	0	
Rank 1's ghost cells:	2063	0	1365	1031	993	0	0	0	0	0	410	0	0	698	0	0	
Rank 2's ghost cells:	0	1365	0	1872	0	0	0	0	0	0	1306	0	0	0	0	0	
Rank 3's ghost cells:	0	1017	1874	0	492	0	1168	1398	0	91	831	0	0	8	0	0	
Rank 4's ghost cells:	1250	992	0	486	0	2131	806	331	0	9	0	0	0	635	548	541	
Rank 5's ghost cells:	217	0	0	0	2117	0	598	651	0	0	0	0	0	0	138	743	
Rank 6's ghost cells:	0	0	0	1184	781	598	0	2006	1104	1378	40	0	0	6	504	0	
Rank 7's ghost cells:	0	0	0	1398	324	651	2010	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	1104	0	0	1875	0	0	0	0	1408	0	
Rank 9's ghost cells:	0	0	0	90	9	0	1396	0	1860	0	1109	1303	0	120	1151	0	
Rank 10's ghost cells:	0	412	1306	851	0	0	40	0	0	1098	0	2280	31	1360	70	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	1304	2283	0	1067	40	200	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	31	1064	0	2188	755	607	
Rank 13's ghost cells:	1302	698	0	8	654	0	6	0	0	119	1357	34	2191	0	670	514	
Rank 14's ghost cells:	0	0	0	0	543	138	504	0	1383	1170	62	203	765	669	0	2214	
Rank 15's ghost cells:	0	0	0	0	539	754	0	0	0	0	0	0	607	509	2213	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.5641         0.218431
    2          31.6312         0.579708
    3          22.4664          0.71026
    4          19.3558         0.861546
    5          17.7985         0.919544
    6          14.1487         0.794938
    7          12.0635         0.852616
    8          11.8021         0.978334
    9          10.0537         0.851855
   10          8.75803         0.871129
   11          8.78236          1.00278
   12          7.75368          0.88287
   13          6.76899         0.873003
   14          6.88768          1.01753
   15          6.21372         0.902151
   16          5.48537         0.882783
   17          5.62149          1.02482
   18          5.28622         0.940358
   19          4.71198         0.891371
   20          4.78574          1.01565
   21          4.46226         0.932407
   22          4.06841         0.911738
   23          4.09107          1.00557
   24          3.80538         0.930167
   25          3.54149         0.930653
   26          3.52535         0.995444
   27          3.20978         0.910484
   28          3.03349         0.945078
   29           3.0503          1.00554
   30          2.84441         0.932502
   31          2.83598         0.997036
   32          2.98018          1.05085
   33          2.99483          1.00492
   34          3.16904          1.05817
   35          3.11333         0.982418
   36          2.64146         0.848438
   37           2.2008         0.833173
   38          1.76734         0.803045
   39          1.46202         0.827245
   40           1.3002         0.889316
   41          1.14873         0.883505
   42          1.03345         0.899645
   43         0.900492         0.871344
   44         0.737735         0.819257
   45         0.646193         0.875915
   46          0.55323         0.856137
   47         0.446314         0.806743
   48         0.391505         0.877195
   49         0.326044         0.832796
   50         0.265214         0.813431
   51         0.233815          0.88161
   52         0.190859         0.816282
   53         0.156713         0.821092
   54         0.136616          0.87176
   55         0.111842         0.818661
   56        0.0946485         0.846268
   57        0.0785997         0.830438
   58        0.0642048         0.816858
   59        0.0524932          0.81759
   60        0.0414442         0.789516
   61        0.0328156         0.791801
   62          0.02572         0.783775
   63         0.020152         0.783516
=== rate=0.861048, T=10.2162, TIT=0.162162, IT=63

 Elapsed time: 10.2162
Rank 0: Matrix-vector product took 0.0544937 seconds
Rank 1: Matrix-vector product took 0.0561385 seconds
Rank 2: Matrix-vector product took 0.0534121 seconds
Rank 3: Matrix-vector product took 0.0567586 seconds
Rank 4: Matrix-vector product took 0.0584689 seconds
Rank 5: Matrix-vector product took 0.0555943 seconds
Rank 6: Matrix-vector product took 0.0569152 seconds
Rank 7: Matrix-vector product took 0.0540822 seconds
Rank 8: Matrix-vector product took 0.0537453 seconds
Rank 9: Matrix-vector product took 0.0567365 seconds
Rank 10: Matrix-vector product took 0.0577456 seconds
Rank 11: Matrix-vector product took 0.0556962 seconds
Rank 12: Matrix-vector product took 0.0549359 seconds
Rank 13: Matrix-vector product took 0.0571794 seconds
Rank 14: Matrix-vector product took 0.0577247 seconds
Rank 15: Matrix-vector product took 0.054854 seconds
Average time for Matrix-vector product is 0.0559051 seconds

Rank 0: copyOwnerToAll took 0.0006061 seconds
Rank 1: copyOwnerToAll took 0.000657369 seconds
Rank 2: copyOwnerToAll took 0.000685729 seconds
Rank 3: copyOwnerToAll took 0.000669059 seconds
Rank 4: copyOwnerToAll took 0.000699509 seconds
Rank 5: copyOwnerToAll took 0.000661359 seconds
Rank 6: copyOwnerToAll took 0.000772779 seconds
Rank 7: copyOwnerToAll took 0.00054244 seconds
Rank 8: copyOwnerToAll took 0.00058208 seconds
Rank 9: copyOwnerToAll took 0.00071585 seconds
Rank 10: copyOwnerToAll took 0.00079645 seconds
Rank 11: copyOwnerToAll took 0.00054749 seconds
Rank 12: copyOwnerToAll took 0.00056138 seconds
Rank 13: copyOwnerToAll took 0.00073807 seconds
Rank 14: copyOwnerToAll took 0.00071836 seconds
Rank 15: copyOwnerToAll took 0.00058005 seconds
Average time for copyOwnertoAll is 0.00065838 seconds
