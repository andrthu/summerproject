Number of cores/ranks per node is: 8
Bad partitioner
 Cell on rank 0 before loadbalancing: 41653
Cell on rank 1 before loadbalancing: 41670
Cell on rank 2 before loadbalancing: 41654
Cell on rank 3 before loadbalancing: 41663
Cell on rank 4 before loadbalancing: 41661
Cell on rank 5 before loadbalancing: 41661
Cell on rank 6 before loadbalancing: 41677
Cell on rank 7 before loadbalancing: 41667
Cell on rank 8 before loadbalancing: 41665
Cell on rank 9 before loadbalancing: 41654
Cell on rank 10 before loadbalancing: 41668
Cell on rank 11 before loadbalancing: 41657
Cell on rank 12 before loadbalancing: 41668
Cell on rank 13 before loadbalancing: 41667
Cell on rank 14 before loadbalancing: 41670
Cell on rank 15 before loadbalancing: 41669
Cell on rank 16 before loadbalancing: 41670
Cell on rank 17 before loadbalancing: 41660
Cell on rank 18 before loadbalancing: 41695
Cell on rank 19 before loadbalancing: 41669
Cell on rank 20 before loadbalancing: 41668
Cell on rank 21 before loadbalancing: 41673
Cell on rank 22 before loadbalancing: 41674
Cell on rank 23 before loadbalancing: 41667
Edge-cut: 84670


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	1057	1324	0	690	677	0	0	0	1657	873	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	1057	0	929	878	134	1034	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	1324	931	0	783	486	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	881	781	0	1222	1138	0	0	0	0	0	0	0	1132	583	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	690	134	481	1222	0	631	0	0	0	0	677	698	0	646	152	0	0	974	0	0	0	228	0	0	
From rank 5 to: 	683	1016	0	1152	631	0	0	0	0	0	675	593	0	0	843	0	0	753	0	0	0	56	0	0	
From rank 6 to: 	0	0	0	0	0	0	0	1841	635	0	0	1278	0	0	0	0	0	0	0	0	0	0	1078	1247	
From rank 7 to: 	0	0	0	0	0	0	1855	0	1878	795	1351	583	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	635	1858	0	1266	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	1657	0	0	0	0	0	0	774	1265	0	2152	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	890	0	0	0	701	708	0	1352	0	2155	0	2348	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	698	594	1278	590	0	0	2329	0	0	0	0	0	0	59	0	0	0	1667	1177	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	1138	1124	1020	822	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	1130	625	0	0	0	0	0	0	0	1141	0	866	0	878	636	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	550	151	845	0	0	0	0	0	0	1126	855	0	240	269	1376	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	1020	0	238	0	1011	824	0	1254	311	212	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	831	921	277	1011	0	1255	0	0	1156	271	0	0	
From rank 17 to: 	0	0	0	0	965	753	0	0	0	0	0	65	0	636	1366	864	1252	0	0	0	20	2043	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1634	946	0	0	1638	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1253	0	0	1640	0	911	551	444	343	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	294	1156	19	946	903	0	920	838	194	
From rank 21 to: 	0	0	0	0	247	62	0	0	0	0	0	1668	0	0	0	211	275	2031	0	574	929	0	2156	0	
From rank 22 to: 	0	0	0	0	0	0	1136	0	0	0	0	1181	0	0	0	0	0	0	0	444	839	2158	0	2335	
From rank 23 to: 	0	0	0	0	0	0	1247	0	0	0	0	0	0	0	0	0	0	0	1636	342	238	0	2332	0	

Edge-cut for node partition: 22864





=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.5012         0.222183
    2           32.315          0.58224
    3          22.8212         0.706212
    4          19.3484         0.847824
    5          17.4621         0.902509
    6          14.2434         0.815678
    7          12.1182         0.850789
    8          11.6898         0.964648
    9          10.1345         0.866954
   10           8.7146         0.859895
   11           8.6403         0.991474
   12          8.04326         0.930901
   13          6.84694         0.851264
   14          6.72524         0.982226
   15           6.5685         0.976693
   16          5.72501         0.871586
   17          5.51009          0.96246
   18          5.40111         0.980222
   19          4.84001         0.896113
   20          4.69635         0.970318
   21          4.54777         0.968362
   22          4.11983         0.905901
   23          4.07127         0.988214
   24          3.93398         0.966277
   25          3.55719         0.904222
   26          3.48771         0.980469
   27          3.36969         0.966159
   28          3.04566         0.903841
   29          2.97653         0.977302
   30          2.94172         0.988307
   31          2.77496         0.943311
   32          2.84655           1.0258
   33          3.00001          1.05391
   34           3.0824          1.02746
   35           3.1649          1.02676
   36          2.83796         0.896699
   37          2.19463         0.773312
   38          1.70769         0.778122
   39          1.35606         0.794089
   40          1.16092           0.8561
   41          1.09591            0.944
   42          0.98651         0.900176
   43         0.835973         0.847405
   44          0.70665         0.845302
   45         0.592232         0.838085
   46         0.510869         0.862617
   47         0.431926         0.845473
   48         0.348235         0.806236
   49         0.292859         0.840981
   50         0.249392          0.85158
   51         0.207032         0.830146
   52         0.177791         0.858762
   53           0.1538         0.865058
   54         0.126766         0.824227
   55         0.107464         0.847732
   56        0.0898049         0.835677
   57        0.0721928         0.803884
   58        0.0605231         0.838354
   59        0.0489853         0.809365
   60         0.039114         0.798485
   61        0.0314788         0.804796
   62        0.0243129         0.772357
=== rate=0.861577, T=7.3781, TIT=0.119002, IT=62

 Elapsed time: 7.3781
Rank 0: Matrix-vector product took 0.0389307 seconds
Rank 1: Matrix-vector product took 0.03693 seconds
Rank 2: Matrix-vector product took 0.0370462 seconds
Rank 3: Matrix-vector product took 0.03766 seconds
Rank 4: Matrix-vector product took 0.0402462 seconds
Rank 5: Matrix-vector product took 0.0404515 seconds
Rank 6: Matrix-vector product took 0.0400283 seconds
Rank 7: Matrix-vector product took 0.0384285 seconds
Rank 8: Matrix-vector product took 0.0387601 seconds
Rank 9: Matrix-vector product took 0.0369418 seconds
Rank 10: Matrix-vector product took 0.0383287 seconds
Rank 11: Matrix-vector product took 0.0390623 seconds
Rank 12: Matrix-vector product took 0.0389582 seconds
Rank 13: Matrix-vector product took 0.0383231 seconds
Rank 14: Matrix-vector product took 0.0379598 seconds
Rank 15: Matrix-vector product took 0.0402564 seconds
Rank 16: Matrix-vector product took 0.0380977 seconds
Rank 17: Matrix-vector product took 0.0373457 seconds
Rank 18: Matrix-vector product took 0.0384451 seconds
Rank 19: Matrix-vector product took 0.0379502 seconds
Rank 20: Matrix-vector product took 0.0380239 seconds
Rank 21: Matrix-vector product took 0.0380549 seconds
Rank 22: Matrix-vector product took 0.0366294 seconds
Rank 23: Matrix-vector product took 0.0405365 seconds
Average time for Matrix-vector product is 0.0384748 seconds

Rank 0: copyOwnerToAll took 0.00068489 seconds
Rank 1: copyOwnerToAll took 0.00068489 seconds
Rank 2: copyOwnerToAll took 0.00068489 seconds
Rank 3: copyOwnerToAll took 0.00068489 seconds
Rank 4: copyOwnerToAll took 0.00068489 seconds
Rank 5: copyOwnerToAll took 0.00068489 seconds
Rank 6: copyOwnerToAll took 0.00068489 seconds
Rank 7: copyOwnerToAll took 0.00068489 seconds
Rank 8: copyOwnerToAll took 0.00068489 seconds
Rank 9: copyOwnerToAll took 0.00068489 seconds
Rank 10: copyOwnerToAll took 0.00068489 seconds
Rank 11: copyOwnerToAll took 0.00068489 seconds
Rank 12: copyOwnerToAll took 0.00068489 seconds
Rank 13: copyOwnerToAll took 0.00068489 seconds
Rank 14: copyOwnerToAll took 0.00068489 seconds
Rank 15: copyOwnerToAll took 0.00068489 seconds
Rank 16: copyOwnerToAll took 0.00068489 seconds
Rank 17: copyOwnerToAll took 0.00068489 seconds
Rank 18: copyOwnerToAll took 0.00068489 seconds
Rank 19: copyOwnerToAll took 0.00068489 seconds
Rank 20: copyOwnerToAll took 0.00068489 seconds
Rank 21: copyOwnerToAll took 0.00068489 seconds
Rank 22: copyOwnerToAll took 0.00068489 seconds
Rank 23: copyOwnerToAll took 0.00068489 seconds
Average time for copyOwnertoAll is 0.000715056 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	0	0	0	675	56	0	0	683	1016	0	0	631	0	0	593	1152	0	0	0	843	0	0	753	
Rank 1's ghost cells:	0	0	0	0	0	0	0	0	0	0	822	0	0	0	0	0	0	0	0	1138	1124	1020	0	0	
Rank 2's ghost cells:	0	0	0	1634	0	0	0	1638	0	0	0	0	0	0	946	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	1640	0	0	551	444	343	0	0	0	0	0	0	911	0	0	0	0	0	0	1253	0	0	
Rank 4's ghost cells:	708	0	0	0	0	0	0	0	890	0	0	1352	701	2155	0	2348	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	62	0	0	574	0	0	2156	0	0	0	275	0	247	0	929	1668	0	0	0	0	0	211	0	2031	
Rank 6's ghost cells:	0	0	0	444	0	2158	0	2335	0	0	0	0	0	0	839	1181	0	0	1136	0	0	0	0	0	
Rank 7's ghost cells:	0	0	1636	342	0	0	2332	0	0	0	0	0	0	0	238	0	0	0	1247	0	0	0	0	0	
Rank 8's ghost cells:	677	0	0	0	873	0	0	0	0	1057	0	0	690	1657	0	0	0	1324	0	0	0	0	0	0	
Rank 9's ghost cells:	1034	0	0	0	0	0	0	0	1057	0	0	0	134	0	0	0	878	929	0	0	0	0	0	0	
Rank 10's ghost cells:	0	831	0	0	0	271	0	0	0	0	0	0	0	0	1156	0	0	0	0	921	277	1011	0	1255	
Rank 11's ghost cells:	0	0	0	0	1351	0	0	0	0	0	0	0	0	795	0	583	0	0	1855	0	0	0	1878	0	
Rank 12's ghost cells:	631	0	0	0	677	228	0	0	690	134	0	0	0	0	0	698	1222	481	0	646	152	0	0	974	
Rank 13's ghost cells:	0	0	0	0	2152	0	0	0	1657	0	0	774	0	0	0	0	0	0	0	0	0	0	1265	0	
Rank 14's ghost cells:	0	0	946	903	0	920	838	194	0	0	1156	0	0	0	0	0	0	0	0	0	0	294	0	19	
Rank 15's ghost cells:	594	0	0	0	2329	1667	1177	0	0	0	0	590	698	0	0	0	0	0	1278	0	0	0	0	59	
Rank 16's ghost cells:	1138	0	0	0	0	0	0	0	0	881	0	0	1222	0	0	0	0	781	0	1132	583	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	1324	931	0	0	486	0	0	0	783	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	1078	1247	0	0	0	1841	0	0	0	1278	0	0	0	0	0	0	635	0	
Rank 19's ghost cells:	0	1141	0	0	0	0	0	0	0	0	878	0	625	0	0	0	1130	0	0	0	866	0	0	636	
Rank 20's ghost cells:	845	1126	0	0	0	0	0	0	0	0	269	0	151	0	0	0	550	0	0	855	0	240	0	1376	
Rank 21's ghost cells:	0	1020	0	1254	0	212	0	0	0	0	1011	0	0	0	311	0	0	0	0	0	238	0	0	824	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	1858	0	1266	0	0	0	0	635	0	0	0	0	0	
Rank 23's ghost cells:	753	0	0	0	0	2043	0	0	0	0	1252	0	965	0	20	65	0	0	0	636	1366	864	0	0	
