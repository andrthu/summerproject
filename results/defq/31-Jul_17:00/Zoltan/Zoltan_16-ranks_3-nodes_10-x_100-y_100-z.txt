Number of cores/ranks per node is: 16
Zoltan partitioner
Cell on rank 0 before loadbalancing: 2073
Cell on rank 1 before loadbalancing: 2102
Cell on rank 2 before loadbalancing: 2087
Cell on rank 3 before loadbalancing: 2104
Cell on rank 4 before loadbalancing: 2055
Cell on rank 5 before loadbalancing: 2080
Cell on rank 6 before loadbalancing: 2085
Cell on rank 7 before loadbalancing: 2084
Cell on rank 8 before loadbalancing: 2084
Cell on rank 9 before loadbalancing: 2083
Cell on rank 10 before loadbalancing: 2082
Cell on rank 11 before loadbalancing: 2083
Cell on rank 12 before loadbalancing: 2090
Cell on rank 13 before loadbalancing: 2089
Cell on rank 14 before loadbalancing: 2090
Cell on rank 15 before loadbalancing: 2091
Cell on rank 16 before loadbalancing: 2091
Cell on rank 17 before loadbalancing: 2088
Cell on rank 18 before loadbalancing: 2104
Cell on rank 19 before loadbalancing: 2068
Cell on rank 20 before loadbalancing: 2086
Cell on rank 21 before loadbalancing: 2073
Cell on rank 22 before loadbalancing: 2062
Cell on rank 23 before loadbalancing: 2067
Cell on rank 24 before loadbalancing: 2090
Cell on rank 25 before loadbalancing: 2089
Cell on rank 26 before loadbalancing: 2090
Cell on rank 27 before loadbalancing: 2099
Cell on rank 28 before loadbalancing: 2099
Cell on rank 29 before loadbalancing: 2069
Cell on rank 30 before loadbalancing: 2069
Cell on rank 31 before loadbalancing: 2070
Cell on rank 32 before loadbalancing: 2069
Cell on rank 33 before loadbalancing: 2102
Cell on rank 34 before loadbalancing: 2076
Cell on rank 35 before loadbalancing: 2076
Cell on rank 36 before loadbalancing: 2096
Cell on rank 37 before loadbalancing: 2097
Cell on rank 38 before loadbalancing: 2053
Cell on rank 39 before loadbalancing: 2082
Cell on rank 40 before loadbalancing: 2082
Cell on rank 41 before loadbalancing: 2082
Cell on rank 42 before loadbalancing: 2092
Cell on rank 43 before loadbalancing: 2092
Cell on rank 44 before loadbalancing: 2087
Cell on rank 45 before loadbalancing: 2092
Cell on rank 46 before loadbalancing: 2092
Cell on rank 47 before loadbalancing: 2054


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
From rank 0 to: 	0	173	0	0	0	0	10	0	121	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	
From rank 1 to: 	173	0	166	120	0	0	122	0	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	176	0	100	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	118	105	0	165	125	52	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	144	164	0	101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	124	101	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	227	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	10	122	0	53	0	0	0	214	191	54	69	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	55	0	110	226	0	0	94	0	0	0	0	0	0	0	0	0	0	97	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	117	6	0	0	0	0	194	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	0	0	111	
From rank 9 to: 	0	0	0	0	0	0	54	91	0	0	181	123	0	0	0	0	0	0	0	0	0	39	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	68	0	90	172	0	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	130	0	80	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	121	87	0	0	0	0	0	0	0	0	0	0	130	0	0	40	0	0	0	0	68	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	188	156	8	115	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	191	0	70	0	84	0	0	0	0	105	23	0	0	0	0	0	54	46	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	155	73	0	0	0	0	0	0	0	0	0	0	0	0	0	66	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	8	0	0	0	191	129	0	0	0	0	0	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	107	89	0	193	0	50	0	0	0	0	196	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	128	0	0	124	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	184	53	0	176	168	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	191	0	231	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	225	0	103	0	0	0	0	0	0	0	0	0	0	52	220	0	0	0	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	34	0	130	0	105	0	0	0	0	0	0	0	0	182	69	51	0	0	0	0	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	23	0	0	193	0	166	0	0	185	0	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	24	0	143	0	0	0	0	0	97	22	0	170	0	103	59	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	0	0	0	0	0	54	0	0	0	150	0	0	0	129	0	0	0	0	116	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	235	8	0	0	0	0	0	0	10	128	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	237	0	207	0	5	0	106	0	0	0	26	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	66	0	0	0	0	0	0	0	0	0	0	8	203	0	354	144	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	54	114	0	0	0	0	0	0	0	0	0	0	0	0	370	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	68	0	44	0	0	0	0	0	0	0	29	0	0	110	0	5	140	115	0	0	105	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	97	106	136	0	74	0	0	0	10	0	0	0	0	0	70	82	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	5	0	106	107	0	76	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	20	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	106	82	0	0	0	0	0	0	0	0	0	0	0	0	0	119	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	136	0	0	0	171	50	0	0	0	92	0	166	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	14	0	0	0	0	0	0	0	175	0	178	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	26	0	0	0	69	146	0	57	178	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	62	94	158	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	140	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	140	0	0	72	0	132	0	52	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	92	0	0	97	0	0	0	204	221	0	0	0	0	108	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	75	211	0	14	0	0	170	0	52	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	177	0	0	0	0	0	222	11	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	132	0	0	0	0	238	161	0	0	13	
From rank 43 to: 	140	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	236	0	27	0	0	84	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	51	0	169	0	164	24	0	0	134	96	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	75	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	149	106	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	83	0	0	0	0	0	0	0	0	108	52	0	0	0	134	154	0	90	
From rank 47 to: 	0	0	0	0	0	0	0	0	108	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	9	84	101	106	86	0	




=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1           39.692         0.247688
    2          24.5215         0.617795
    3          22.6008         0.921672
    4          22.2132         0.982848
    5          11.5627         0.520534
    6          6.33453         0.547841
    7          4.68861         0.740168
    8          2.92503         0.623858
    9          1.60935         0.550199
   10          1.12752         0.700605
   11         0.749581         0.664807
   12         0.458428         0.611579
   13         0.311437         0.679359
   14         0.193797         0.622268
   15         0.138456         0.714434
   16        0.0857452         0.619297
   17        0.0546273          0.63709
   18        0.0371362          0.67981
   19        0.0237268         0.638914
   20        0.0146084         0.615691
=== rate=0.628044, T=0.146962, TIT=0.00734812, IT=20

 Elapsed time: 0.146962
Rank 0: Matrix-vector product took 0.00196981 seconds
Rank 1: Matrix-vector product took 0.00209047 seconds
Rank 2: Matrix-vector product took 0.00194112 seconds
Rank 3: Matrix-vector product took 0.00210818 seconds
Rank 4: Matrix-vector product took 0.00190926 seconds
Rank 5: Matrix-vector product took 0.00203909 seconds
Rank 6: Matrix-vector product took 0.00215423 seconds
Rank 7: Matrix-vector product took 0.00207194 seconds
Rank 8: Matrix-vector product took 0.00208642 seconds
Rank 9: Matrix-vector product took 0.00211065 seconds
Rank 10: Matrix-vector product took 0.00210669 seconds
Rank 11: Matrix-vector product took 0.00205505 seconds
Rank 12: Matrix-vector product took 0.00205533 seconds
Rank 13: Matrix-vector product took 0.00206194 seconds
Rank 14: Matrix-vector product took 0.00196391 seconds
Rank 15: Matrix-vector product took 0.00194759 seconds
Rank 16: Matrix-vector product took 0.00210568 seconds
Rank 17: Matrix-vector product took 0.00185157 seconds
Rank 18: Matrix-vector product took 0.00208972 seconds
Rank 19: Matrix-vector product took 0.00192464 seconds
Rank 20: Matrix-vector product took 0.00216514 seconds
Rank 21: Matrix-vector product took 0.00207366 seconds
Rank 22: Matrix-vector product took 0.00212533 seconds
Rank 23: Matrix-vector product took 0.00215165 seconds
Rank 24: Matrix-vector product took 0.00197309 seconds
Rank 25: Matrix-vector product took 0.00202725 seconds
Rank 26: Matrix-vector product took 0.00205607 seconds
Rank 27: Matrix-vector product took 0.00217779 seconds
Rank 28: Matrix-vector product took 0.00212029 seconds
Rank 29: Matrix-vector product took 0.00214196 seconds
Rank 30: Matrix-vector product took 0.00205186 seconds
Rank 31: Matrix-vector product took 0.00202866 seconds
Rank 32: Matrix-vector product took 0.00204751 seconds
Rank 33: Matrix-vector product took 0.00210088 seconds
Rank 34: Matrix-vector product took 0.00198797 seconds
Rank 35: Matrix-vector product took 0.00209378 seconds
Rank 36: Matrix-vector product took 0.00197097 seconds
Rank 37: Matrix-vector product took 0.0018602 seconds
Rank 38: Matrix-vector product took 0.00194667 seconds
Rank 39: Matrix-vector product took 0.00215216 seconds
Rank 40: Matrix-vector product took 0.00215782 seconds
Rank 41: Matrix-vector product took 0.00191997 seconds
Rank 42: Matrix-vector product took 0.00210338 seconds
Rank 43: Matrix-vector product took 0.00206102 seconds
Rank 44: Matrix-vector product took 0.00214108 seconds
Rank 45: Matrix-vector product took 0.00207359 seconds
Rank 46: Matrix-vector product took 0.00210739 seconds
Rank 47: Matrix-vector product took 0.00203177 seconds
Average time for Matrix-vector product is 0.00205192 seconds

Rank 0: copyOwnerToAll took 0.00020703 seconds
Rank 1: copyOwnerToAll took 0.00020703 seconds
Rank 2: copyOwnerToAll took 0.00020703 seconds
Rank 3: copyOwnerToAll took 0.00020703 seconds
Rank 4: copyOwnerToAll took 0.00020703 seconds
Rank 5: copyOwnerToAll took 0.00020703 seconds
Rank 6: copyOwnerToAll took 0.00020703 seconds
Rank 7: copyOwnerToAll took 0.00020703 seconds
Rank 8: copyOwnerToAll took 0.00020703 seconds
Rank 9: copyOwnerToAll took 0.00020703 seconds
Rank 10: copyOwnerToAll took 0.00020703 seconds
Rank 11: copyOwnerToAll took 0.00020703 seconds
Rank 12: copyOwnerToAll took 0.00020703 seconds
Rank 13: copyOwnerToAll took 0.00020703 seconds
Rank 14: copyOwnerToAll took 0.00020703 seconds
Rank 15: copyOwnerToAll took 0.00020703 seconds
Rank 16: copyOwnerToAll took 0.00020703 seconds
Rank 17: copyOwnerToAll took 0.00020703 seconds
Rank 18: copyOwnerToAll took 0.00020703 seconds
Rank 19: copyOwnerToAll took 0.00020703 seconds
Rank 20: copyOwnerToAll took 0.00020703 seconds
Rank 21: copyOwnerToAll took 0.00020703 seconds
Rank 22: copyOwnerToAll took 0.00020703 seconds
Rank 23: copyOwnerToAll took 0.00020703 seconds
Rank 24: copyOwnerToAll took 0.00020703 seconds
Rank 25: copyOwnerToAll took 0.00020703 seconds
Rank 26: copyOwnerToAll took 0.00020703 seconds
Rank 27: copyOwnerToAll took 0.00020703 seconds
Rank 28: copyOwnerToAll took 0.00020703 seconds
Rank 29: copyOwnerToAll took 0.00020703 seconds
Rank 30: copyOwnerToAll took 0.00020703 seconds
Rank 31: copyOwnerToAll took 0.00020703 seconds
Rank 32: copyOwnerToAll took 0.00020703 seconds
Rank 33: copyOwnerToAll took 0.00020703 seconds
Rank 34: copyOwnerToAll took 0.00020703 seconds
Rank 35: copyOwnerToAll took 0.00020703 seconds
Rank 36: copyOwnerToAll took 0.00020703 seconds
Rank 37: copyOwnerToAll took 0.00020703 seconds
Rank 38: copyOwnerToAll took 0.00020703 seconds
Rank 39: copyOwnerToAll took 0.00020703 seconds
Rank 40: copyOwnerToAll took 0.00020703 seconds
Rank 41: copyOwnerToAll took 0.00020703 seconds
Rank 42: copyOwnerToAll took 0.00020703 seconds
Rank 43: copyOwnerToAll took 0.00020703 seconds
Rank 44: copyOwnerToAll took 0.00020703 seconds
Rank 45: copyOwnerToAll took 0.00020703 seconds
Rank 46: copyOwnerToAll took 0.00020703 seconds
Rank 47: copyOwnerToAll took 0.00020703 seconds
Average time for copyOwnertoAll is 0.000316293 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
Rank 0's ghost cells:	0	173	0	0	0	0	10	0	121	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	
Rank 1's ghost cells:	173	0	166	120	0	0	122	0	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	176	0	100	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	118	105	0	165	125	52	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	144	164	0	101	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	124	101	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	227	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	10	122	0	53	0	0	0	214	191	54	69	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	55	0	110	226	0	0	94	0	0	0	0	0	0	0	0	0	0	97	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	117	6	0	0	0	0	194	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	0	0	111	
Rank 9's ghost cells:	0	0	0	0	0	0	54	91	0	0	181	123	0	0	0	0	0	0	0	0	0	39	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	68	0	90	172	0	87	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	130	0	80	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	121	87	0	0	0	0	0	0	0	0	0	0	130	0	0	40	0	0	0	0	68	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	188	156	8	115	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	191	0	70	0	84	0	0	0	0	105	23	0	0	0	0	0	54	46	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	155	73	0	0	0	0	0	0	0	0	0	0	0	0	0	66	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	8	0	0	0	191	129	0	0	0	0	0	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	107	89	0	193	0	50	0	0	0	0	196	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	128	0	0	124	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	184	53	0	176	168	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	191	0	231	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	225	0	103	0	0	0	0	0	0	0	0	0	0	52	220	0	0	0	102	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	34	0	130	0	105	0	0	0	0	0	0	0	0	182	69	51	0	0	0	0	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	23	0	0	193	0	166	0	0	185	0	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	24	0	143	0	0	0	0	0	97	22	0	170	0	103	59	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	0	0	0	0	0	54	0	0	0	150	0	0	0	129	0	0	0	0	116	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	235	8	0	0	0	0	0	0	10	128	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	237	0	207	0	5	0	106	0	0	0	26	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	66	0	0	0	0	0	0	0	0	0	0	8	203	0	354	144	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	54	114	0	0	0	0	0	0	0	0	0	0	0	0	370	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	68	0	44	0	0	0	0	0	0	0	29	0	0	110	0	5	140	115	0	0	105	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	97	106	136	0	74	0	0	0	10	0	0	0	0	0	70	82	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	5	0	106	107	0	76	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	20	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	106	82	0	0	0	0	0	0	0	0	0	0	0	0	0	119	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	136	0	0	0	171	50	0	0	0	92	0	166	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	14	0	0	0	0	0	0	0	175	0	178	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	26	0	0	0	69	146	0	57	178	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	62	94	158	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	147	0	140	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	140	0	0	72	0	132	0	52	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	92	0	0	97	0	0	0	204	221	0	0	0	0	108	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	0	75	211	0	14	0	0	170	0	52	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	177	0	0	0	0	0	222	11	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	132	0	0	0	0	238	161	0	0	13	
Rank 43's ghost cells:	140	0	0	0	0	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	236	0	27	0	0	84	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	51	0	169	0	164	24	0	0	134	96	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	75	0	119	0	0	0	0	0	0	0	0	0	0	0	0	0	149	106	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	83	0	0	0	0	0	0	0	0	108	52	0	0	0	134	154	0	90	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	108	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	9	84	101	106	86	0	
