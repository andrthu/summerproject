Number of cores/ranks per node is: 16
Zoltan partitioner
Cell on rank 0 before loadbalancing: 15588
Cell on rank 1 before loadbalancing: 15646
Cell on rank 2 before loadbalancing: 15630
Cell on rank 3 before loadbalancing: 15629
Cell on rank 4 before loadbalancing: 15623
Cell on rank 5 before loadbalancing: 15623
Cell on rank 6 before loadbalancing: 15624
Cell on rank 7 before loadbalancing: 15623
Cell on rank 8 before loadbalancing: 15719
Cell on rank 9 before loadbalancing: 15718
Cell on rank 10 before loadbalancing: 15658
Cell on rank 11 before loadbalancing: 15658
Cell on rank 12 before loadbalancing: 15562
Cell on rank 13 before loadbalancing: 15563
Cell on rank 14 before loadbalancing: 15617
Cell on rank 15 before loadbalancing: 15618
Cell on rank 16 before loadbalancing: 15663
Cell on rank 17 before loadbalancing: 15664
Cell on rank 18 before loadbalancing: 15687
Cell on rank 19 before loadbalancing: 15697
Cell on rank 20 before loadbalancing: 15697
Cell on rank 21 before loadbalancing: 15697
Cell on rank 22 before loadbalancing: 15709
Cell on rank 23 before loadbalancing: 15608
Cell on rank 24 before loadbalancing: 15566
Cell on rank 25 before loadbalancing: 15563
Cell on rank 26 before loadbalancing: 15565
Cell on rank 27 before loadbalancing: 15565
Cell on rank 28 before loadbalancing: 15668
Cell on rank 29 before loadbalancing: 15669
Cell on rank 30 before loadbalancing: 15138
Cell on rank 31 before loadbalancing: 15693
Cell on rank 32 before loadbalancing: 15661
Cell on rank 33 before loadbalancing: 15661
Cell on rank 34 before loadbalancing: 15661
Cell on rank 35 before loadbalancing: 15660
Cell on rank 36 before loadbalancing: 15648
Cell on rank 37 before loadbalancing: 15647
Cell on rank 38 before loadbalancing: 15580
Cell on rank 39 before loadbalancing: 15580
Cell on rank 40 before loadbalancing: 15684
Cell on rank 41 before loadbalancing: 15684
Cell on rank 42 before loadbalancing: 15584
Cell on rank 43 before loadbalancing: 15584
Cell on rank 44 before loadbalancing: 15538
Cell on rank 45 before loadbalancing: 15538
Cell on rank 46 before loadbalancing: 15727
Cell on rank 47 before loadbalancing: 15727
Cell on rank 48 before loadbalancing: 15631
Cell on rank 49 before loadbalancing: 15632
Cell on rank 50 before loadbalancing: 15632
Cell on rank 51 before loadbalancing: 15632
Cell on rank 52 before loadbalancing: 15774
Cell on rank 53 before loadbalancing: 15633
Cell on rank 54 before loadbalancing: 15562
Cell on rank 55 before loadbalancing: 15558
Cell on rank 56 before loadbalancing: 15652
Cell on rank 57 before loadbalancing: 15651
Cell on rank 58 before loadbalancing: 15652
Cell on rank 59 before loadbalancing: 15651
Cell on rank 60 before loadbalancing: 15557
Cell on rank 61 before loadbalancing: 15557
Cell on rank 62 before loadbalancing: 15557
Cell on rank 63 before loadbalancing: 15557


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	421	314	365	196	91	0	574	0	0	0	0	0	0	0	0	0	0	54	490	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	429	0	642	0	0	0	183	253	0	0	0	681	0	0	0	36	0	0	5	327	0	0	0	0	0	415	0	0	0	35	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	320	648	0	616	208	7	554	70	311	0	0	142	0	0	0	0	0	0	368	53	0	12	0	0	236	144	0	0	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	365	0	618	0	99	578	0	0	0	0	0	0	0	0	0	0	0	0	479	0	0	440	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	653	131	0	0	0	0	0	0	0	0	0	0	0	85	0	9	0	0	0	0	
From rank 4 to: 	187	0	208	99	0	846	621	634	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	48	101	241	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	91	0	7	581	854	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	277	0	0	639	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	191	579	0	601	0	0	783	121	0	0	98	0	0	574	88	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	338	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	561	252	70	0	640	0	762	0	0	0	0	0	0	0	0	271	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	310	0	0	0	121	0	0	796	39	733	0	197	439	0	0	0	0	0	0	0	0	0	437	4	0	0	119	0	0	0	0	0	556	260	0	0	0	0	0	0	0	0	0	18	7	0	0	0	0	0	0	0	0	57	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	794	0	562	13	254	349	18	0	0	0	0	0	0	0	0	0	0	0	0	0	447	0	0	0	54	0	0	166	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	275	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	40	560	0	617	484	0	65	190	0	0	0	0	0	0	0	0	0	0	0	0	109	393	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	660	156	0	0	0	98	0	728	14	616	0	0	0	245	406	0	0	0	0	0	0	0	0	30	154	0	0	101	321	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	253	485	0	0	590	214	568	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	199	344	0	0	567	0	464	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	509	250	32	106	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	582	0	436	19	68	246	214	437	0	784	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	304	112	0	0	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	45	0	0	0	0	88	271	0	0	190	403	592	0	777	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	132	195	163	2	0	414	2	294	3	740	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	702	0	353	458	0	52	315	201	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	54	7	384	484	0	0	0	0	0	0	0	0	0	0	0	0	130	349	0	905	239	779	0	0	130	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	491	310	52	0	0	0	0	0	0	0	0	0	0	0	0	0	190	447	907	0	0	0	0	0	0	410	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	163	0	240	0	0	876	420	736	263	18	448	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	0	0	0	0	0	0	54	0	0	0	119	0	396	0	519	28	0	
From rank 21 to: 	0	0	9	414	0	0	0	0	0	0	0	0	0	0	0	0	2	52	796	0	869	0	626	49	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	127	0	0	0	0	0	0	0	0	0	0	0	163	0	80	0	259	113	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	328	0	0	441	628	0	878	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	82	455	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	407	210	0	0	728	52	883	0	0	0	419	98	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	152	0	0	0	0	0	0	0	393	0	0	
From rank 24 to: 	0	0	229	0	0	0	0	0	429	0	0	27	0	0	0	0	2	0	133	0	260	54	0	0	0	629	683	96	437	0	1	0	0	0	111	0	0	0	0	0	0	0	0	0	0	135	0	0	0	0	0	0	0	8	81	296	0	0	0	482	0	0	0	0	
From rank 25 to: 	0	405	143	0	0	0	0	0	4	0	0	149	0	0	0	0	299	0	293	407	19	0	0	0	628	0	0	590	114	335	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	462	0	0	419	689	0	0	601	19	0	327	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	708	0	53	0	0	0	106	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	734	0	0	0	3	0	0	98	97	602	609	0	9	11	350	437	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	112	448	109	100	0	0	0	0	0	0	0	0	0	0	0	0	432	113	16	9	0	832	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	12	547	7	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	38	0	0	0	0	0	0	0	0	393	319	0	0	0	0	0	0	0	0	0	0	0	0	0	342	0	12	826	0	73	631	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	320	352	681	73	0	858	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	464	88	63	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	443	0	634	852	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	54	0	0	0	503	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	527	0	505	508	419	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	233	304	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	538	0	341	763	235	0	87	0	0	13	0	109	0	0	579	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	45	0	0	0	0	0	578	0	0	0	0	32	110	0	0	0	0	0	0	0	0	0	113	0	0	0	0	0	0	0	0	347	0	902	0	0	369	0	0	0	0	0	0	694	353	0	0	0	0	0	0	0	0	115	0	0	21	346	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	257	163	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	508	754	912	0	109	368	676	0	0	13	0	0	0	0	138	0	0	0	0	0	0	0	0	335	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	500	235	0	109	0	576	273	370	0	157	0	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	414	0	0	365	594	0	299	302	0	0	0	0	0	0	0	0	56	394	0	0	0	0	57	341	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	91	369	652	273	309	0	1044	243	270	0	92	0	187	16	0	110	71	0	0	0	0	0	102	0	0	457	15	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	370	301	1044	0	505	224	0	0	0	0	0	0	265	44	0	0	0	0	0	0	317	0	331	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	490	0	733	659	107	79	359	49	40	0	0	0	0	0	0	0	0	242	117	74	70	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	13	162	0	271	225	729	0	24	707	0	5	356	124	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	654	24	0	858	511	16	0	0	0	0	0	0	0	0	0	0	157	482	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	113	0	0	85	0	91	0	121	716	849	0	130	0	64	675	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	672	4	260	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	0	490	130	0	755	0	642	0	0	0	0	0	0	0	0	0	509	0	114	0	0	0	0	
From rank 45 to: 	0	0	332	131	40	0	116	0	21	0	0	0	0	0	0	0	0	0	0	0	16	122	0	0	136	0	0	0	0	0	0	0	0	0	692	0	0	0	197	0	351	2	17	0	750	0	372	223	0	0	0	0	0	0	0	0	0	3	11	522	0	0	0	0	
From rank 46 to: 	0	0	0	0	111	0	339	0	7	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	584	340	126	0	0	17	0	47	355	0	64	0	372	0	839	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 47 to: 	0	0	0	0	241	621	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	43	140	0	659	625	223	835	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	56	110	264	0	0	0	0	0	0	0	0	0	887	146	691	0	155	0	303	160	0	685	0	114	17	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	394	74	38	0	0	0	0	0	0	0	0	887	0	512	0	55	9	429	186	0	0	0	0	0	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	512	0	736	417	208	0	0	0	0	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	694	0	735	0	0	520	0	0	0	0	0	0	636	82	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	17	0	471	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	420	0	0	775	777	57	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	0	0	148	11	0	688	0	12	0	88	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	155	9	218	516	776	0	33	509	0	0	77	45	0	252	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	0	0	0	557	0	58	0	0	0	0	0	0	66	0	0	0	0	0	0	0	0	0	0	0	435	0	0	783	35	0	1143	0	0	0	0	0	0	0	0	
From rank 55 to: 	0	0	0	0	0	0	0	0	55	290	0	0	0	0	0	0	0	0	0	0	0	0	0	0	292	0	48	0	7	0	0	0	23	0	115	331	0	335	102	0	0	0	0	0	0	0	0	0	304	188	0	0	57	507	1138	0	0	0	195	319	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	309	238	0	158	0	0	0	0	0	160	0	0	0	0	0	0	0	0	652	408	225	296	50	258	281	
From rank 57 to: 	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	164	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	493	0	510	3	0	0	0	0	0	0	0	0	0	0	653	0	0	525	0	581	130	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	0	0	457	341	74	0	0	0	0	11	0	0	711	0	0	0	0	85	0	197	408	0	0	1015	240	156	0	0	
From rank 59 to: 	0	0	0	9	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	396	84	0	0	484	0	99	0	0	0	0	0	0	0	358	0	0	0	16	0	70	0	0	0	115	520	0	0	0	0	0	0	0	46	0	324	253	526	996	0	12	343	0	0	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	0	658	0	0	0	0	298	0	243	9	0	727	0	547	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	502	259	78	386	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	0	0	82	0	231	0	0	55	597	156	350	727	0	898	158	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	107	455	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	257	132	0	0	0	894	0	911	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	281	0	0	0	546	172	910	0	




=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          57.3111         0.229428
    2          33.7167         0.588311
    3          24.2707         0.719842
    4          21.4548         0.883981
    5          18.3345         0.854564
    6          14.6315          0.79803
    7          13.3089         0.909605
    8          12.4499         0.935455
    9          10.2874         0.826308
   10          9.74827         0.947592
   11          9.31448         0.955501
   12          7.91739         0.850008
   13          7.65374           0.9667
   14          7.49708         0.979533
   15          6.49152         0.865873
   16           6.3523         0.978553
   17           6.1501         0.968168
   18          5.51568         0.896845
   19          5.39943         0.978923
   20          5.15056         0.953908
   21          4.64329         0.901513
   22          4.60535         0.991829
   23          4.32705         0.939569
   24          3.98497         0.920944
   25          3.96087         0.993952
   26           3.7422         0.944793
   27          3.49387         0.933642
   28          3.48384         0.997128
   29          3.28203         0.942073
   30            3.091         0.941795
   31          3.07218         0.993909
   32          2.90374         0.945173
   33          2.90815          1.00152
   34            3.041          1.04568
   35          3.11445          1.02415
   36          3.23507          1.03873
   37          3.11709          0.96353
   38          2.61194         0.837943
   39          2.06775         0.791654
   40          1.57545         0.761915
   41          1.26709         0.804269
   42          1.14855         0.906453
   43           1.0376         0.903401
   44         0.918843         0.885542
   45         0.808185         0.879568
   46         0.693075          0.85757
   47          0.60992          0.88002
   48         0.521096         0.854368
   49         0.428811         0.822902
   50         0.359777          0.83901
   51           0.2973         0.826347
   52         0.252037         0.847753
   53          0.21755         0.863165
   54         0.183953         0.845566
   55         0.158509         0.861686
   56          0.13693          0.86386
   57          0.11428         0.834584
   58         0.096862         0.847588
   59        0.0808611         0.834807
   60        0.0657398         0.812996
   61        0.0547943         0.833503
   62        0.0443873         0.810071
   63        0.0353276         0.795895
   64         0.028591         0.809311
   65        0.0227072         0.794209
=== rate=0.866611, T=3.16105, TIT=0.0486316, IT=65

 Elapsed time: 3.16105
Rank 0: Matrix-vector product took 0.0145644 seconds
Rank 1: Matrix-vector product took 0.0148681 seconds
Rank 2: Matrix-vector product took 0.0157038 seconds
Rank 3: Matrix-vector product took 0.0151893 seconds
Rank 4: Matrix-vector product took 0.0150129 seconds
Rank 5: Matrix-vector product took 0.014462 seconds
Rank 6: Matrix-vector product took 0.0152926 seconds
Rank 7: Matrix-vector product took 0.014473 seconds
Rank 8: Matrix-vector product took 0.0158596 seconds
Rank 9: Matrix-vector product took 0.0151572 seconds
Rank 10: Matrix-vector product took 0.014571 seconds
Rank 11: Matrix-vector product took 0.01527 seconds
Rank 12: Matrix-vector product took 0.0141505 seconds
Rank 13: Matrix-vector product took 0.0144807 seconds
Rank 14: Matrix-vector product took 0.015088 seconds
Rank 15: Matrix-vector product took 0.0143199 seconds
Rank 16: Matrix-vector product took 0.0146805 seconds
Rank 17: Matrix-vector product took 0.0142265 seconds
Rank 18: Matrix-vector product took 0.0155786 seconds
Rank 19: Matrix-vector product took 0.0147756 seconds
Rank 20: Matrix-vector product took 0.0159404 seconds
Rank 21: Matrix-vector product took 0.0153722 seconds
Rank 22: Matrix-vector product took 0.0148521 seconds
Rank 23: Matrix-vector product took 0.0150774 seconds
Rank 24: Matrix-vector product took 0.0155705 seconds
Rank 25: Matrix-vector product took 0.0151667 seconds
Rank 26: Matrix-vector product took 0.0152634 seconds
Rank 27: Matrix-vector product took 0.0147959 seconds
Rank 28: Matrix-vector product took 0.015395 seconds
Rank 29: Matrix-vector product took 0.0147862 seconds
Rank 30: Matrix-vector product took 0.0144089 seconds
Rank 31: Matrix-vector product took 0.014313 seconds
Rank 32: Matrix-vector product took 0.0145832 seconds
Rank 33: Matrix-vector product took 0.0150044 seconds
Rank 34: Matrix-vector product took 0.0156599 seconds
Rank 35: Matrix-vector product took 0.0158686 seconds
Rank 36: Matrix-vector product took 0.01431 seconds
Rank 37: Matrix-vector product took 0.0146461 seconds
Rank 38: Matrix-vector product took 0.0158482 seconds
Rank 39: Matrix-vector product took 0.0150795 seconds
Rank 40: Matrix-vector product took 0.0152613 seconds
Rank 41: Matrix-vector product took 0.0146354 seconds
Rank 42: Matrix-vector product took 0.0146664 seconds
Rank 43: Matrix-vector product took 0.0148446 seconds
Rank 44: Matrix-vector product took 0.0152968 seconds
Rank 45: Matrix-vector product took 0.0155323 seconds
Rank 46: Matrix-vector product took 0.0150791 seconds
Rank 47: Matrix-vector product took 0.0152367 seconds
Rank 48: Matrix-vector product took 0.015273 seconds
Rank 49: Matrix-vector product took 0.0144932 seconds
Rank 50: Matrix-vector product took 0.0141004 seconds
Rank 51: Matrix-vector product took 0.0146982 seconds
Rank 52: Matrix-vector product took 0.0148428 seconds
Rank 53: Matrix-vector product took 0.0153497 seconds
Rank 54: Matrix-vector product took 0.015109 seconds
Rank 55: Matrix-vector product took 0.0157864 seconds
Rank 56: Matrix-vector product took 0.0149317 seconds
Rank 57: Matrix-vector product took 0.0152769 seconds
Rank 58: Matrix-vector product took 0.0157268 seconds
Rank 59: Matrix-vector product took 0.0161223 seconds
Rank 60: Matrix-vector product took 0.0144766 seconds
Rank 61: Matrix-vector product took 0.015883 seconds
Rank 62: Matrix-vector product took 0.0145388 seconds
Rank 63: Matrix-vector product took 0.0138542 seconds
Average time for Matrix-vector product is 0.0150106 seconds

Rank 0: copyOwnerToAll took 0.000566769 seconds
Rank 1: copyOwnerToAll took 0.000566769 seconds
Rank 2: copyOwnerToAll took 0.000566769 seconds
Rank 3: copyOwnerToAll took 0.000566769 seconds
Rank 4: copyOwnerToAll took 0.000566769 seconds
Rank 5: copyOwnerToAll took 0.000566769 seconds
Rank 6: copyOwnerToAll took 0.000566769 seconds
Rank 7: copyOwnerToAll took 0.000566769 seconds
Rank 8: copyOwnerToAll took 0.000566769 seconds
Rank 9: copyOwnerToAll took 0.000566769 seconds
Rank 10: copyOwnerToAll took 0.000566769 seconds
Rank 11: copyOwnerToAll took 0.000566769 seconds
Rank 12: copyOwnerToAll took 0.000566769 seconds
Rank 13: copyOwnerToAll took 0.000566769 seconds
Rank 14: copyOwnerToAll took 0.000566769 seconds
Rank 15: copyOwnerToAll took 0.000566769 seconds
Rank 16: copyOwnerToAll took 0.000566769 seconds
Rank 17: copyOwnerToAll took 0.000566769 seconds
Rank 18: copyOwnerToAll took 0.000566769 seconds
Rank 19: copyOwnerToAll took 0.000566769 seconds
Rank 20: copyOwnerToAll took 0.000566769 seconds
Rank 21: copyOwnerToAll took 0.000566769 seconds
Rank 22: copyOwnerToAll took 0.000566769 seconds
Rank 23: copyOwnerToAll took 0.000566769 seconds
Rank 24: copyOwnerToAll took 0.000566769 seconds
Rank 25: copyOwnerToAll took 0.000566769 seconds
Rank 26: copyOwnerToAll took 0.000566769 seconds
Rank 27: copyOwnerToAll took 0.000566769 seconds
Rank 28: copyOwnerToAll took 0.000566769 seconds
Rank 29: copyOwnerToAll took 0.000566769 seconds
Rank 30: copyOwnerToAll took 0.000566769 seconds
Rank 31: copyOwnerToAll took 0.000566769 seconds
Rank 32: copyOwnerToAll took 0.000566769 seconds
Rank 33: copyOwnerToAll took 0.000566769 seconds
Rank 34: copyOwnerToAll took 0.000566769 seconds
Rank 35: copyOwnerToAll took 0.000566769 seconds
Rank 36: copyOwnerToAll took 0.000566769 seconds
Rank 37: copyOwnerToAll took 0.000566769 seconds
Rank 38: copyOwnerToAll took 0.000566769 seconds
Rank 39: copyOwnerToAll took 0.000566769 seconds
Rank 40: copyOwnerToAll took 0.000566769 seconds
Rank 41: copyOwnerToAll took 0.000566769 seconds
Rank 42: copyOwnerToAll took 0.000566769 seconds
Rank 43: copyOwnerToAll took 0.000566769 seconds
Rank 44: copyOwnerToAll took 0.000566769 seconds
Rank 45: copyOwnerToAll took 0.000566769 seconds
Rank 46: copyOwnerToAll took 0.000566769 seconds
Rank 47: copyOwnerToAll took 0.000566769 seconds
Rank 48: copyOwnerToAll took 0.000566769 seconds
Rank 49: copyOwnerToAll took 0.000566769 seconds
Rank 50: copyOwnerToAll took 0.000566769 seconds
Rank 51: copyOwnerToAll took 0.000566769 seconds
Rank 52: copyOwnerToAll took 0.000566769 seconds
Rank 53: copyOwnerToAll took 0.000566769 seconds
Rank 54: copyOwnerToAll took 0.000566769 seconds
Rank 55: copyOwnerToAll took 0.000566769 seconds
Rank 56: copyOwnerToAll took 0.000566769 seconds
Rank 57: copyOwnerToAll took 0.000566769 seconds
Rank 58: copyOwnerToAll took 0.000566769 seconds
Rank 59: copyOwnerToAll took 0.000566769 seconds
Rank 60: copyOwnerToAll took 0.000566769 seconds
Rank 61: copyOwnerToAll took 0.000566769 seconds
Rank 62: copyOwnerToAll took 0.000566769 seconds
Rank 63: copyOwnerToAll took 0.000566769 seconds
Average time for copyOwnertoAll is 0.000600794 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	421	314	365	196	91	0	574	0	0	0	0	0	0	0	0	0	0	54	490	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	429	0	642	0	0	0	183	253	0	0	0	681	0	0	0	36	0	0	5	327	0	0	0	0	0	415	0	0	0	35	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	320	648	0	616	208	7	554	70	311	0	0	142	0	0	0	0	0	0	368	53	0	12	0	0	236	144	0	0	0	0	0	0	0	0	46	0	0	0	0	0	0	0	0	0	0	328	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	365	0	618	0	99	578	0	0	0	0	0	0	0	0	0	0	0	0	479	0	0	440	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	653	131	0	0	0	0	0	0	0	0	0	0	0	85	0	9	0	0	0	0	
Rank 4's ghost cells:	187	0	208	99	0	846	621	634	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	48	101	241	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	91	0	7	581	854	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	277	0	0	639	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	191	579	0	601	0	0	783	121	0	0	98	0	0	574	88	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	338	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	561	252	70	0	640	0	762	0	0	0	0	0	0	0	0	271	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	310	0	0	0	121	0	0	796	39	733	0	197	439	0	0	0	0	0	0	0	0	0	437	4	0	0	119	0	0	0	0	0	556	260	0	0	0	0	0	0	0	0	0	18	7	0	0	0	0	0	0	0	0	57	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	794	0	562	13	254	349	18	0	0	0	0	0	0	0	0	0	0	0	0	0	447	0	0	0	54	0	0	166	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	275	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	40	560	0	617	484	0	65	190	0	0	0	0	0	0	0	0	0	0	0	0	109	393	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	660	156	0	0	0	98	0	728	14	616	0	0	0	245	406	0	0	0	0	0	0	0	0	30	154	0	0	101	321	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	253	485	0	0	590	214	568	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	199	344	0	0	567	0	464	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	509	250	32	106	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	582	0	436	19	68	246	214	437	0	784	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	304	112	0	0	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	45	0	0	0	0	88	271	0	0	190	403	592	0	777	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	700	132	195	163	2	0	414	2	294	3	740	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	702	0	353	458	0	52	315	201	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	54	7	384	484	0	0	0	0	0	0	0	0	0	0	0	0	130	349	0	905	239	779	0	0	130	288	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	491	310	52	0	0	0	0	0	0	0	0	0	0	0	0	0	190	447	907	0	0	0	0	0	0	410	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	163	0	240	0	0	876	420	736	263	18	448	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	0	0	0	0	0	0	54	0	0	0	119	0	396	0	519	28	0	
Rank 21's ghost cells:	0	0	9	414	0	0	0	0	0	0	0	0	0	0	0	0	2	52	796	0	869	0	626	49	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	127	0	0	0	0	0	0	0	0	0	0	0	163	0	80	0	259	113	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	328	0	0	441	628	0	878	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	82	455	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	407	210	0	0	728	52	883	0	0	0	419	98	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	152	0	0	0	0	0	0	0	393	0	0	
Rank 24's ghost cells:	0	0	229	0	0	0	0	0	429	0	0	27	0	0	0	0	2	0	133	0	260	54	0	0	0	629	683	96	437	0	1	0	0	0	111	0	0	0	0	0	0	0	0	0	0	135	0	0	0	0	0	0	0	8	81	296	0	0	0	482	0	0	0	0	
Rank 25's ghost cells:	0	405	143	0	0	0	0	0	4	0	0	149	0	0	0	0	299	0	293	407	19	0	0	0	628	0	0	590	114	335	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	462	0	0	419	689	0	0	601	19	0	327	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	708	0	53	0	0	0	106	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	734	0	0	0	3	0	0	98	97	602	609	0	9	11	350	437	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	112	448	109	100	0	0	0	0	0	0	0	0	0	0	0	0	432	113	16	9	0	832	685	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	12	547	7	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	38	0	0	0	0	0	0	0	0	393	319	0	0	0	0	0	0	0	0	0	0	0	0	0	342	0	12	826	0	73	631	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	320	352	681	73	0	858	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	464	88	63	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	443	0	634	852	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	54	0	0	0	503	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	527	0	505	508	419	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	18	0	0	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	233	304	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	538	0	341	763	235	0	87	0	0	13	0	109	0	0	579	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	45	0	0	0	0	0	578	0	0	0	0	32	110	0	0	0	0	0	0	0	0	0	113	0	0	0	0	0	0	0	0	347	0	902	0	0	369	0	0	0	0	0	0	694	353	0	0	0	0	0	0	0	0	115	0	0	21	346	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	257	163	0	0	0	110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	508	754	912	0	109	368	676	0	0	13	0	0	0	0	138	0	0	0	0	0	0	0	0	335	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	500	235	0	109	0	576	273	370	0	157	0	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	414	0	0	365	594	0	299	302	0	0	0	0	0	0	0	0	56	394	0	0	0	0	57	341	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	91	369	652	273	309	0	1044	243	270	0	92	0	187	16	0	110	71	0	0	0	0	0	102	0	0	457	15	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	370	301	1044	0	505	224	0	0	0	0	0	0	265	44	0	0	0	0	0	0	317	0	331	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	490	0	733	659	107	79	359	49	40	0	0	0	0	0	0	0	0	242	117	74	70	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	13	0	13	162	0	271	225	729	0	24	707	0	5	356	124	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	654	24	0	858	511	16	0	0	0	0	0	0	0	0	0	0	157	482	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	113	0	0	85	0	91	0	121	716	849	0	130	0	64	675	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	672	4	260	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	0	490	130	0	755	0	642	0	0	0	0	0	0	0	0	0	509	0	114	0	0	0	0	
Rank 45's ghost cells:	0	0	332	131	40	0	116	0	21	0	0	0	0	0	0	0	0	0	0	0	16	122	0	0	136	0	0	0	0	0	0	0	0	0	692	0	0	0	197	0	351	2	17	0	750	0	372	223	0	0	0	0	0	0	0	0	0	3	11	522	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	111	0	339	0	7	0	0	0	0	0	71	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	584	340	126	0	0	17	0	47	355	0	64	0	372	0	839	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 47's ghost cells:	0	0	0	0	241	621	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	43	140	0	659	625	223	835	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	56	110	264	0	0	0	0	0	0	0	0	0	887	146	691	0	155	0	303	160	0	685	0	114	17	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	394	74	38	0	0	0	0	0	0	0	0	887	0	512	0	55	9	429	186	0	0	0	0	0	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	512	0	736	417	208	0	0	0	0	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	694	0	735	0	0	520	0	0	0	0	0	0	636	82	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	17	0	471	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	420	0	0	775	777	57	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	0	0	148	11	0	688	0	12	0	88	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	155	9	218	516	776	0	33	509	0	0	77	45	0	252	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	205	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	0	0	0	557	0	58	0	0	0	0	0	0	66	0	0	0	0	0	0	0	0	0	0	0	435	0	0	783	35	0	1143	0	0	0	0	0	0	0	0	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	55	290	0	0	0	0	0	0	0	0	0	0	0	0	0	0	292	0	48	0	7	0	0	0	23	0	115	331	0	335	102	0	0	0	0	0	0	0	0	0	304	188	0	0	57	507	1138	0	0	0	195	319	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	309	238	0	158	0	0	0	0	0	160	0	0	0	0	0	0	0	0	652	408	225	296	50	258	281	
Rank 57's ghost cells:	0	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	105	164	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	493	0	510	3	0	0	0	0	0	0	0	0	0	0	653	0	0	525	0	581	130	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	0	0	457	341	74	0	0	0	0	11	0	0	711	0	0	0	0	85	0	197	408	0	0	1015	240	156	0	0	
Rank 59's ghost cells:	0	0	0	9	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	396	84	0	0	484	0	99	0	0	0	0	0	0	0	358	0	0	0	16	0	70	0	0	0	115	520	0	0	0	0	0	0	0	46	0	324	253	526	996	0	12	343	0	0	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	0	658	0	0	0	0	298	0	243	9	0	727	0	547	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	502	259	78	386	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	17	0	0	82	0	231	0	0	55	597	156	350	727	0	898	158	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	24	107	455	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	257	132	0	0	0	894	0	911	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	281	0	0	0	546	172	910	0	
