Number of cores/ranks per node is: 8
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 3148
Cell on rank 1 before loadbalancing: 3147
Cell on rank 2 before loadbalancing: 3148
Cell on rank 3 before loadbalancing: 3147
Cell on rank 4 before loadbalancing: 3101
Cell on rank 5 before loadbalancing: 3114
Cell on rank 6 before loadbalancing: 3110
Cell on rank 7 before loadbalancing: 3119
Cell on rank 8 before loadbalancing: 3140
Cell on rank 9 before loadbalancing: 3121
Cell on rank 10 before loadbalancing: 3131
Cell on rank 11 before loadbalancing: 3131
Cell on rank 12 before loadbalancing: 3096
Cell on rank 13 before loadbalancing: 3125
Cell on rank 14 before loadbalancing: 3141
Cell on rank 15 before loadbalancing: 3081
Cell on rank 16 before loadbalancing: 3105
Cell on rank 17 before loadbalancing: 3104
Cell on rank 18 before loadbalancing: 3119
Cell on rank 19 before loadbalancing: 3119
Cell on rank 20 before loadbalancing: 3117
Cell on rank 21 before loadbalancing: 3151
Cell on rank 22 before loadbalancing: 3137
Cell on rank 23 before loadbalancing: 3137
Cell on rank 24 before loadbalancing: 3127
Cell on rank 25 before loadbalancing: 3126
Cell on rank 26 before loadbalancing: 3127
Cell on rank 27 before loadbalancing: 3126
Cell on rank 28 before loadbalancing: 3129
Cell on rank 29 before loadbalancing: 3141
Cell on rank 30 before loadbalancing: 3118
Cell on rank 31 before loadbalancing: 3117


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	283	175	278	80	0	74	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	29	0	0	0	0	0	0	0	
From rank 1 to: 	283	0	0	0	0	50	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	141	0	0	253	89	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	159	0	56	39	0	0	0	0	
From rank 3 to: 	277	0	253	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	80	0	95	122	0	240	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	
From rank 5 to: 	0	62	0	0	240	0	30	120	0	0	0	0	0	60	0	159	0	0	0	0	61	0	0	0	0	0	0	62	0	0	0	0	
From rank 6 to: 	70	118	0	0	120	34	0	193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	110	193	0	0	0	0	0	179	73	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	228	272	0	5	129	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	229	0	0	0	127	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	272	0	0	240	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	240	0	0	0	134	0	0	0	130	113	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	179	3	127	0	0	0	231	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	60	0	65	129	2	0	0	230	0	125	145	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	54	0	85	134	0	135	0	246	0	2	0	134	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	165	0	0	0	0	0	0	0	145	254	0	0	130	0	0	65	0	0	0	0	0	0	3	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	238	0	183	0	140	140	0	0	0	0	0	0	0	53	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	130	238	0	0	73	3	0	0	0	0	0	78	157	0	0	104	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	0	0	0	0	209	4	116	75	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	110	0	0	134	0	173	73	209	0	0	0	60	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	61	0	0	0	0	0	0	0	0	0	65	0	3	4	0	0	131	101	120	0	0	0	131	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	116	0	131	0	83	156	0	0	0	0	0	0	97	90	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	0	74	60	101	83	0	244	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	156	244	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	29	0	159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	93	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	260	0	264	0	42	203	0	0	
From rank 26 to: 	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	78	0	0	0	0	0	0	93	255	0	243	98	37	116	0	
From rank 27 to: 	0	0	42	0	134	65	0	0	0	0	0	0	0	0	0	3	0	167	0	0	119	0	0	0	0	0	243	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	98	0	0	240	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	213	38	0	240	0	213	170	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	94	0	0	0	97	0	0	0	0	119	0	0	213	0	234	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	180	234	0	

Edge-cut for node partition: 2297





=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.3158         0.239101
    2          23.5018          0.61337
    3          21.7929         0.927288
    4          20.0434         0.919721
    5          9.97661          0.49775
    6          5.64831         0.566155
    7          4.24325         0.751243
    8          2.65736         0.626255
    9          1.37863         0.518797
   10         0.969345         0.703121
   11         0.670872         0.692088
   12         0.372506         0.555256
   13         0.272185         0.730688
   14         0.170404         0.626059
   15         0.105025         0.616332
   16        0.0712158         0.678081
   17         0.043617         0.612462
   18        0.0286403         0.656632
   19        0.0195524         0.682687
   20        0.0115565         0.591053
=== rate=0.620728, T=0.211715, TIT=0.0105858, IT=20

 Elapsed time: 0.211715
Rank 0: Matrix-vector product took 0.00296864 seconds
Rank 1: Matrix-vector product took 0.0027081 seconds
Rank 2: Matrix-vector product took 0.00292557 seconds
Rank 3: Matrix-vector product took 0.00292391 seconds
Rank 4: Matrix-vector product took 0.00285117 seconds
Rank 5: Matrix-vector product took 0.00304484 seconds
Rank 6: Matrix-vector product took 0.00312551 seconds
Rank 7: Matrix-vector product took 0.00300799 seconds
Rank 8: Matrix-vector product took 0.00311197 seconds
Rank 9: Matrix-vector product took 0.00279419 seconds
Rank 10: Matrix-vector product took 0.00303196 seconds
Rank 11: Matrix-vector product took 0.00296621 seconds
Rank 12: Matrix-vector product took 0.00305611 seconds
Rank 13: Matrix-vector product took 0.00305464 seconds
Rank 14: Matrix-vector product took 0.00286952 seconds
Rank 15: Matrix-vector product took 0.00285959 seconds
Rank 16: Matrix-vector product took 0.00302309 seconds
Rank 17: Matrix-vector product took 0.00302773 seconds
Rank 18: Matrix-vector product took 0.0028847 seconds
Rank 19: Matrix-vector product took 0.00301763 seconds
Rank 20: Matrix-vector product took 0.00288267 seconds
Rank 21: Matrix-vector product took 0.00308129 seconds
Rank 22: Matrix-vector product took 0.00301263 seconds
Rank 23: Matrix-vector product took 0.0028689 seconds
Rank 24: Matrix-vector product took 0.00286144 seconds
Rank 25: Matrix-vector product took 0.00302088 seconds
Rank 26: Matrix-vector product took 0.00314595 seconds
Rank 27: Matrix-vector product took 0.00303067 seconds
Rank 28: Matrix-vector product took 0.00276242 seconds
Rank 29: Matrix-vector product took 0.00314991 seconds
Rank 30: Matrix-vector product took 0.0030684 seconds
Rank 31: Matrix-vector product took 0.00285031 seconds
Average time for Matrix-vector product is 0.00296839 seconds

Rank 0: copyOwnerToAll took 0.00021928 seconds
Rank 1: copyOwnerToAll took 0.00021928 seconds
Rank 2: copyOwnerToAll took 0.00021928 seconds
Rank 3: copyOwnerToAll took 0.00021928 seconds
Rank 4: copyOwnerToAll took 0.00021928 seconds
Rank 5: copyOwnerToAll took 0.00021928 seconds
Rank 6: copyOwnerToAll took 0.00021928 seconds
Rank 7: copyOwnerToAll took 0.00021928 seconds
Rank 8: copyOwnerToAll took 0.00021928 seconds
Rank 9: copyOwnerToAll took 0.00021928 seconds
Rank 10: copyOwnerToAll took 0.00021928 seconds
Rank 11: copyOwnerToAll took 0.00021928 seconds
Rank 12: copyOwnerToAll took 0.00021928 seconds
Rank 13: copyOwnerToAll took 0.00021928 seconds
Rank 14: copyOwnerToAll took 0.00021928 seconds
Rank 15: copyOwnerToAll took 0.00021928 seconds
Rank 16: copyOwnerToAll took 0.00021928 seconds
Rank 17: copyOwnerToAll took 0.00021928 seconds
Rank 18: copyOwnerToAll took 0.00021928 seconds
Rank 19: copyOwnerToAll took 0.00021928 seconds
Rank 20: copyOwnerToAll took 0.00021928 seconds
Rank 21: copyOwnerToAll took 0.00021928 seconds
Rank 22: copyOwnerToAll took 0.00021928 seconds
Rank 23: copyOwnerToAll took 0.00021928 seconds
Rank 24: copyOwnerToAll took 0.00021928 seconds
Rank 25: copyOwnerToAll took 0.00021928 seconds
Rank 26: copyOwnerToAll took 0.00021928 seconds
Rank 27: copyOwnerToAll took 0.00021928 seconds
Rank 28: copyOwnerToAll took 0.00021928 seconds
Rank 29: copyOwnerToAll took 0.00021928 seconds
Rank 30: copyOwnerToAll took 0.00021928 seconds
Rank 31: copyOwnerToAll took 0.00021928 seconds
Average time for copyOwnertoAll is 0.000236742 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	228	272	0	5	129	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	229	0	0	0	127	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	272	0	0	240	0	0	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	240	0	0	0	134	0	0	0	0	0	0	0	0	0	0	0	130	113	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	3	127	0	0	0	231	0	0	0	0	0	0	0	0	0	179	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	129	2	0	0	230	0	125	145	0	0	0	0	0	60	0	65	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	54	0	85	134	0	135	0	246	0	0	0	0	0	0	0	0	0	2	0	134	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	145	254	0	0	0	0	0	0	165	0	0	0	130	0	0	65	0	0	0	0	0	0	3	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	283	175	278	80	0	74	0	0	0	0	0	0	0	0	0	29	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	283	0	0	0	0	50	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	141	0	0	253	89	0	0	0	0	0	0	0	0	0	0	0	159	0	56	39	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	277	0	253	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	80	0	95	122	0	240	130	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	60	0	159	0	62	0	0	240	0	30	120	0	0	0	0	61	0	0	0	0	0	0	62	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	70	118	0	0	120	34	0	193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	179	73	0	0	0	0	0	0	0	110	193	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	238	0	183	0	140	140	0	0	0	0	0	0	0	53	0	
Rank 17's ghost cells:	0	0	0	0	0	0	2	130	0	0	0	0	0	0	0	0	238	0	0	73	3	0	0	0	0	0	78	157	0	0	104	0	
Rank 18's ghost cells:	0	0	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	209	4	116	75	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	110	0	0	134	0	0	0	0	0	0	0	0	0	173	73	209	0	0	0	60	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	65	0	0	0	0	0	61	0	0	0	3	4	0	0	131	101	120	0	0	0	131	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	116	0	131	0	83	156	0	0	0	0	0	0	97	90	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	145	0	74	60	101	83	0	244	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	156	244	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	29	0	159	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	93	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	260	0	264	0	42	203	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	78	0	0	0	0	0	0	93	255	0	243	98	37	116	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	3	0	0	42	0	134	65	0	0	0	167	0	0	119	0	0	0	0	0	243	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	98	0	0	240	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	213	38	0	240	0	213	170	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	94	0	0	0	97	0	0	0	0	119	0	0	213	0	234	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	0	0	0	180	234	0	
