Number of cores/ranks per node is: 16
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 20820
Cell on rank 1 before loadbalancing: 20768
Cell on rank 2 before loadbalancing: 20794
Cell on rank 3 before loadbalancing: 20795
Cell on rank 4 before loadbalancing: 20794
Cell on rank 5 before loadbalancing: 20795
Cell on rank 6 before loadbalancing: 20849
Cell on rank 7 before loadbalancing: 20850
Cell on rank 8 before loadbalancing: 20850
Cell on rank 9 before loadbalancing: 20919
Cell on rank 10 before loadbalancing: 20782
Cell on rank 11 before loadbalancing: 20849
Cell on rank 12 before loadbalancing: 20850
Cell on rank 13 before loadbalancing: 20849
Cell on rank 14 before loadbalancing: 20849
Cell on rank 15 before loadbalancing: 20850
Cell on rank 16 before loadbalancing: 20850
Cell on rank 17 before loadbalancing: 20848
Cell on rank 18 before loadbalancing: 20796
Cell on rank 19 before loadbalancing: 20795
Cell on rank 20 before loadbalancing: 20796
Cell on rank 21 before loadbalancing: 20796
Cell on rank 22 before loadbalancing: 20796
Cell on rank 23 before loadbalancing: 20791
Cell on rank 24 before loadbalancing: 20902
Cell on rank 25 before loadbalancing: 20902
Cell on rank 26 before loadbalancing: 20902
Cell on rank 27 before loadbalancing: 20761
Cell on rank 28 before loadbalancing: 20753
Cell on rank 29 before loadbalancing: 20756
Cell on rank 30 before loadbalancing: 20844
Cell on rank 31 before loadbalancing: 20843
Cell on rank 32 before loadbalancing: 20887
Cell on rank 33 before loadbalancing: 20862
Cell on rank 34 before loadbalancing: 20862
Cell on rank 35 before loadbalancing: 20861
Cell on rank 36 before loadbalancing: 20925
Cell on rank 37 before loadbalancing: 20924
Cell on rank 38 before loadbalancing: 20925
Cell on rank 39 before loadbalancing: 20939
Cell on rank 40 before loadbalancing: 20708
Cell on rank 41 before loadbalancing: 20823
Cell on rank 42 before loadbalancing: 20821
Cell on rank 43 before loadbalancing: 20790
Cell on rank 44 before loadbalancing: 20675
Cell on rank 45 before loadbalancing: 20954
Cell on rank 46 before loadbalancing: 20954
Cell on rank 47 before loadbalancing: 20696


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
From rank 0 to: 	0	987	624	443	0	250	256	0	530	0	128	0	0	0	0	0	0	0	322	0	0	0	0	161	0	0	88	0	0	103	0	0	0	0	0	0	304	0	466	392	0	0	0	55	48	0	0	0	
From rank 1 to: 	989	0	198	229	0	767	0	0	0	0	0	0	0	0	0	0	0	0	145	0	0	98	0	95	20	16	317	0	0	31	0	0	0	0	0	0	304	161	116	615	774	0	0	0	0	0	0	0	
From rank 2 to: 	649	208	0	1139	295	0	297	205	127	104	595	0	0	0	0	0	0	0	0	0	0	26	0	731	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	438	222	1143	0	1017	584	0	0	0	0	384	0	0	0	0	0	0	0	0	0	0	274	0	81	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	295	1008	0	514	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	492	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	249	785	0	548	509	0	0	0	0	0	0	0	0	0	0	0	0	0	12	59	0	584	0	0	0	0	13	0	0	0	0	0	0	0	0	0	0	0	0	417	141	0	0	0	0	0	0	0	
From rank 6 to: 	247	0	296	0	0	0	0	1337	1017	0	0	0	0	0	329	0	386	433	119	0	0	0	0	230	0	0	0	0	0	4	0	0	0	305	0	58	0	0	0	0	0	0	0	0	92	0	0	0	
From rank 7 to: 	0	0	207	0	0	0	1305	0	511	1506	145	0	0	0	200	0	451	0	0	0	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	11	0	0	0	0	0	
From rank 8 to: 	530	0	130	0	0	0	1016	504	0	166	469	464	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	284	0	0	0	0	64	0	0	0	451	656	580	0	0	0	
From rank 9 to: 	0	0	109	0	0	0	0	1529	166	0	854	769	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	
From rank 10 to: 	128	0	592	381	0	0	0	143	474	851	0	776	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	477	772	771	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	681	97	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	652	593	540	0	293	275	400	42	0	14	34	0	0	0	0	0	108	0	0	0	0	120	303	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	652	0	137	0	0	0	0	683	237	0	677	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	333	198	0	0	0	0	598	141	0	605	217	309	213	0	0	0	186	940	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	551	0	615	0	966	862	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	167	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	374	448	0	0	0	0	0	0	217	944	0	760	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	445	0	0	0	0	0	309	0	320	880	768	0	541	0	0	0	0	0	0	0	0	0	0	0	0	0	0	907	766	202	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	327	151	0	0	0	15	137	0	0	0	0	0	291	0	199	0	0	526	0	1067	0	179	299	755	0	0	500	0	0	388	0	0	0	0	0	233	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	59	0	0	0	0	0	0	409	703	0	0	0	0	1049	0	1113	408	621	0	0	0	432	0	0	105	0	0	0	0	0	0	0	0	0	0	3	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	42	237	0	0	0	0	0	1113	0	0	0	0	0	0	501	0	417	431	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	97	31	262	492	581	0	0	0	0	0	0	0	0	0	0	0	0	181	412	0	0	1004	599	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	15	658	175	0	0	0	298	641	0	1026	0	607	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	166	96	721	81	0	0	216	85	0	0	0	0	30	0	961	0	0	0	774	0	0	607	601	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	947	284	1065	167	615	0	369	0	0	0	156	722	47	0	0	34	0	0	0	0	0	0	0	
From rank 25 to: 	0	18	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	947	0	867	12	589	1	0	0	0	0	0	0	9	229	0	0	713	160	0	0	0	0	0	0	
From rank 26 to: 	73	319	0	0	0	13	0	0	0	0	0	0	0	0	0	0	0	0	500	432	500	40	0	0	287	862	0	0	201	588	0	0	0	0	0	0	0	0	0	0	386	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1063	10	0	0	735	658	0	364	336	0	0	24	102	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	417	0	0	0	148	586	204	723	0	179	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	98	34	0	0	0	0	4	0	0	0	0	0	108	0	0	0	0	0	388	105	435	0	0	0	634	1	575	662	170	0	0	0	54	0	0	1118	241	0	0	0	0	0	0	0	61	0	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	959	593	606	191	0	0	0	0	0	0	0	0	0	349	508	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	379	0	0	364	0	0	964	0	917	118	84	603	51	0	0	0	0	0	0	0	28	423	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	335	0	54	593	914	0	0	526	364	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	291	0	261	0	0	0	0	0	0	0	173	931	0	0	0	0	0	0	0	0	0	0	0	0	598	116	0	0	954	691	0	0	0	0	0	0	0	0	525	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	167	0	765	0	0	0	0	0	0	0	0	0	0	0	0	198	84	512	957	0	529	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	58	0	0	0	0	0	300	0	0	0	0	204	221	0	0	0	0	0	145	0	0	24	0	1128	0	604	362	692	530	0	2	0	0	0	0	0	0	0	314	2	0	0	
From rank 36 to: 	309	294	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	727	9	0	95	0	240	0	51	0	0	0	2	0	1278	582	0	79	269	0	145	335	300	436	0	
From rank 37 to: 	0	164	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	47	218	0	0	0	0	0	0	0	0	0	0	1260	0	745	127	359	732	0	0	0	0	448	0	
From rank 38 to: 	464	126	0	0	0	0	0	0	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	593	726	0	642	0	128	0	974	0	0	257	145	
From rank 39 to: 	394	632	0	0	0	417	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	122	655	0	535	587	0	0	0	0	0	0	
From rank 40 to: 	0	754	0	0	0	141	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	21	0	0	36	704	383	0	0	0	0	0	0	0	0	0	82	357	0	570	0	1020	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	163	0	0	0	0	0	0	0	0	0	0	255	741	128	586	1024	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	13	455	20	0	689	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	935	338	147	4	424	
From rank 43 to: 	55	0	0	0	0	0	0	0	656	0	0	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	149	0	970	0	0	0	930	0	854	0	378	416	
From rank 44 to: 	48	0	0	0	0	0	92	0	580	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	344	23	0	523	0	314	335	0	0	0	0	0	345	854	0	786	271	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	508	433	0	0	0	2	296	0	0	0	0	0	161	0	792	0	997	297	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	438	444	267	0	0	0	4	365	280	1015	0	996	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	142	0	0	0	424	414	0	309	995	0	

Edge-cut for node partition: 23867





=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          57.2046         0.229002
    2          33.6082         0.587508
    3          24.3941         0.725839
    4          21.8489         0.895664
    5           18.471         0.845398
    6          14.7081         0.796277
    7           13.617          0.92582
    8          12.5202         0.919449
    9          10.2498         0.818661
   10          9.83829         0.959855
   11           9.2936         0.944636
   12          7.91779         0.851961
   13          7.70113         0.972637
   14          7.50748         0.974854
   15          6.51976         0.868436
   16          6.39314         0.980578
   17          6.13139         0.959058
   18          5.48579         0.894705
   19          5.36758         0.978452
   20          5.14975         0.959417
   21          4.70094         0.912849
   22          4.67496         0.994473
   23          4.36648         0.934014
   24          4.08013         0.934422
   25            4.054         0.993595
   26          3.72964         0.919989
   27          3.50658         0.940193
   28          3.45531         0.985379
   29          3.17466         0.918779
   30          3.03848         0.957104
   31           3.0277         0.996451
   32          2.85517         0.943017
   33          2.86363          1.00296
   34          3.00961          1.05098
   35          3.08532          1.02516
   36          3.18893          1.03358
   37          3.04854         0.955975
   38          2.52328         0.827701
   39          2.01623          0.79905
   40          1.60743         0.797247
   41          1.32891         0.826731
   42          1.21095         0.911235
   43          1.08714         0.897757
   44          0.93518          0.86022
   45         0.818768          0.87552
   46         0.699668         0.854538
   47         0.603294         0.862257
   48         0.529012         0.876873
   49         0.443286         0.837951
   50          0.37277         0.840924
   51         0.312971         0.839582
   52         0.259788         0.830069
   53         0.223309         0.859584
   54         0.189765         0.849783
   55         0.160038         0.843352
   56         0.137351         0.858238
   57         0.114723         0.835255
   58        0.0949324         0.827492
   59        0.0801717         0.844513
   60        0.0655549         0.817681
   61        0.0539842         0.823496
   62        0.0448721         0.831207
   63        0.0359983         0.802243
   64        0.0291387         0.809446
   65        0.0230717          0.79179
=== rate=0.866823, T=4.03406, TIT=0.0620625, IT=65

 Elapsed time: 4.03406
Rank 0: Matrix-vector product took 0.0204022 seconds
Rank 1: Matrix-vector product took 0.0201293 seconds
Rank 2: Matrix-vector product took 0.0210463 seconds
Rank 3: Matrix-vector product took 0.0199052 seconds
Rank 4: Matrix-vector product took 0.0195387 seconds
Rank 5: Matrix-vector product took 0.0189362 seconds
Rank 6: Matrix-vector product took 0.0195683 seconds
Rank 7: Matrix-vector product took 0.0206315 seconds
Rank 8: Matrix-vector product took 0.019925 seconds
Rank 9: Matrix-vector product took 0.0200423 seconds
Rank 10: Matrix-vector product took 0.0191073 seconds
Rank 11: Matrix-vector product took 0.0202571 seconds
Rank 12: Matrix-vector product took 0.0202255 seconds
Rank 13: Matrix-vector product took 0.0195416 seconds
Rank 14: Matrix-vector product took 0.0196889 seconds
Rank 15: Matrix-vector product took 0.0185894 seconds
Rank 16: Matrix-vector product took 0.0193547 seconds
Rank 17: Matrix-vector product took 0.0196059 seconds
Rank 18: Matrix-vector product took 0.0193475 seconds
Rank 19: Matrix-vector product took 0.0190876 seconds
Rank 20: Matrix-vector product took 0.0207094 seconds
Rank 21: Matrix-vector product took 0.0202535 seconds
Rank 22: Matrix-vector product took 0.0192755 seconds
Rank 23: Matrix-vector product took 0.0184149 seconds
Rank 24: Matrix-vector product took 0.0202499 seconds
Rank 25: Matrix-vector product took 0.0192654 seconds
Rank 26: Matrix-vector product took 0.0199313 seconds
Rank 27: Matrix-vector product took 0.019018 seconds
Rank 28: Matrix-vector product took 0.0203207 seconds
Rank 29: Matrix-vector product took 0.0194492 seconds
Rank 30: Matrix-vector product took 0.0202867 seconds
Rank 31: Matrix-vector product took 0.0188877 seconds
Rank 32: Matrix-vector product took 0.0206198 seconds
Rank 33: Matrix-vector product took 0.0203212 seconds
Rank 34: Matrix-vector product took 0.0200015 seconds
Rank 35: Matrix-vector product took 0.0198949 seconds
Rank 36: Matrix-vector product took 0.018525 seconds
Rank 37: Matrix-vector product took 0.0192771 seconds
Rank 38: Matrix-vector product took 0.018623 seconds
Rank 39: Matrix-vector product took 0.0207951 seconds
Rank 40: Matrix-vector product took 0.0205969 seconds
Rank 41: Matrix-vector product took 0.0189825 seconds
Rank 42: Matrix-vector product took 0.0196403 seconds
Rank 43: Matrix-vector product took 0.0194732 seconds
Rank 44: Matrix-vector product took 0.0198721 seconds
Rank 45: Matrix-vector product took 0.0202211 seconds
Rank 46: Matrix-vector product took 0.0194908 seconds
Rank 47: Matrix-vector product took 0.0196713 seconds
Average time for Matrix-vector product is 0.0197291 seconds

Rank 0: copyOwnerToAll took 0.00060571 seconds
Rank 1: copyOwnerToAll took 0.00060571 seconds
Rank 2: copyOwnerToAll took 0.00060571 seconds
Rank 3: copyOwnerToAll took 0.00060571 seconds
Rank 4: copyOwnerToAll took 0.00060571 seconds
Rank 5: copyOwnerToAll took 0.00060571 seconds
Rank 6: copyOwnerToAll took 0.00060571 seconds
Rank 7: copyOwnerToAll took 0.00060571 seconds
Rank 8: copyOwnerToAll took 0.00060571 seconds
Rank 9: copyOwnerToAll took 0.00060571 seconds
Rank 10: copyOwnerToAll took 0.00060571 seconds
Rank 11: copyOwnerToAll took 0.00060571 seconds
Rank 12: copyOwnerToAll took 0.00060571 seconds
Rank 13: copyOwnerToAll took 0.00060571 seconds
Rank 14: copyOwnerToAll took 0.00060571 seconds
Rank 15: copyOwnerToAll took 0.00060571 seconds
Rank 16: copyOwnerToAll took 0.00060571 seconds
Rank 17: copyOwnerToAll took 0.00060571 seconds
Rank 18: copyOwnerToAll took 0.00060571 seconds
Rank 19: copyOwnerToAll took 0.00060571 seconds
Rank 20: copyOwnerToAll took 0.00060571 seconds
Rank 21: copyOwnerToAll took 0.00060571 seconds
Rank 22: copyOwnerToAll took 0.00060571 seconds
Rank 23: copyOwnerToAll took 0.00060571 seconds
Rank 24: copyOwnerToAll took 0.00060571 seconds
Rank 25: copyOwnerToAll took 0.00060571 seconds
Rank 26: copyOwnerToAll took 0.00060571 seconds
Rank 27: copyOwnerToAll took 0.00060571 seconds
Rank 28: copyOwnerToAll took 0.00060571 seconds
Rank 29: copyOwnerToAll took 0.00060571 seconds
Rank 30: copyOwnerToAll took 0.00060571 seconds
Rank 31: copyOwnerToAll took 0.00060571 seconds
Rank 32: copyOwnerToAll took 0.00060571 seconds
Rank 33: copyOwnerToAll took 0.00060571 seconds
Rank 34: copyOwnerToAll took 0.00060571 seconds
Rank 35: copyOwnerToAll took 0.00060571 seconds
Rank 36: copyOwnerToAll took 0.00060571 seconds
Rank 37: copyOwnerToAll took 0.00060571 seconds
Rank 38: copyOwnerToAll took 0.00060571 seconds
Rank 39: copyOwnerToAll took 0.00060571 seconds
Rank 40: copyOwnerToAll took 0.00060571 seconds
Rank 41: copyOwnerToAll took 0.00060571 seconds
Rank 42: copyOwnerToAll took 0.00060571 seconds
Rank 43: copyOwnerToAll took 0.00060571 seconds
Rank 44: copyOwnerToAll took 0.00060571 seconds
Rank 45: copyOwnerToAll took 0.00060571 seconds
Rank 46: copyOwnerToAll took 0.00060571 seconds
Rank 47: copyOwnerToAll took 0.00060571 seconds
Average time for copyOwnertoAll is 0.000641455 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
Rank 0's ghost cells:	0	1337	1017	0	0	0	0	0	0	0	0	0	92	0	0	0	0	329	0	386	433	0	0	0	4	0	0	0	305	0	58	0	247	0	296	0	0	0	0	119	0	0	0	0	230	0	0	0	
Rank 1's ghost cells:	1305	0	511	1506	145	0	0	0	0	0	11	0	0	0	0	0	0	200	0	451	0	0	0	0	0	0	0	0	0	0	0	0	0	0	207	0	0	0	0	0	0	0	0	0	80	0	0	0	
Rank 2's ghost cells:	1016	504	0	166	469	464	0	0	0	64	451	656	580	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	284	0	0	0	530	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	1529	166	0	854	769	0	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	109	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	143	474	851	0	776	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	0	592	381	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	477	772	771	0	0	0	0	0	681	97	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	0	0	9	229	0	0	0	0	0	0	0	0	0	0	0	0	947	12	589	1	0	0	0	0	0	0	160	0	18	0	0	0	0	0	0	0	0	0	0	0	867	0	713	
Rank 7's ghost cells:	0	0	0	0	0	0	9	0	1278	582	0	145	335	300	436	0	0	0	0	0	0	727	95	0	240	0	51	0	0	0	2	269	309	294	0	0	0	0	0	0	0	0	0	0	0	0	0	79	
Rank 8's ghost cells:	0	0	0	0	0	0	218	1260	0	745	0	0	0	0	448	0	0	0	0	0	0	47	0	0	0	0	0	0	0	0	0	732	0	164	0	0	0	0	0	0	0	0	0	0	0	0	127	359	
Rank 9's ghost cells:	0	0	64	0	0	0	0	593	726	0	0	974	0	0	257	145	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	128	464	126	0	0	0	0	0	0	0	0	0	0	0	0	642	0	
Rank 10's ghost cells:	0	13	455	20	0	689	0	0	0	0	0	935	338	147	4	424	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	656	0	0	97	0	149	0	970	930	0	854	0	378	416	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	92	0	580	0	0	0	0	335	0	0	345	854	0	786	271	0	0	0	0	0	0	0	0	0	61	344	23	0	523	0	314	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	296	0	0	161	0	792	0	997	297	0	0	0	0	0	0	0	0	0	508	433	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	438	444	267	4	365	280	1015	0	996	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	142	424	414	0	309	995	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	593	540	0	293	0	0	0	108	0	0	0	0	120	303	0	0	0	0	0	0	0	652	275	400	42	0	14	34	0	0	0	
Rank 17's ghost cells:	333	198	0	0	0	0	0	0	0	0	0	0	0	0	0	0	598	0	605	217	309	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	141	213	0	0	0	186	940	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	551	615	0	966	862	0	0	0	0	0	0	0	0	167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	374	448	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	217	944	0	760	0	0	0	0	0	0	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	445	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	309	320	880	768	0	0	0	0	0	0	0	0	907	766	202	0	0	0	0	0	0	0	0	541	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	947	722	47	0	0	0	0	0	0	0	0	0	0	0	0	0	1065	167	615	0	369	0	0	0	156	0	0	24	0	0	0	0	0	0	0	0	0	0	0	284	0	34	
Rank 22's ghost cells:	0	0	0	0	0	0	10	102	0	0	0	0	0	0	0	0	0	0	0	0	0	1063	0	735	658	0	364	336	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	586	0	0	0	0	0	0	0	0	0	0	0	0	0	0	148	723	0	179	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	417	0	0	0	204	0	0	
Rank 24's ghost cells:	4	0	0	0	0	0	1	241	0	0	0	0	61	0	0	0	108	0	0	0	0	634	662	170	0	0	0	54	0	0	1118	0	98	34	0	0	0	0	0	388	105	435	0	0	0	575	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	349	508	0	0	0	0	0	0	0	0	0	0	0	0	959	593	606	191	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	51	0	0	0	0	28	423	0	0	0	0	0	0	0	379	364	0	0	964	0	917	118	84	603	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	335	0	54	593	914	0	0	526	364	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	291	0	261	0	0	0	0	0	0	0	0	0	525	0	0	0	0	0	0	173	931	0	0	0	0	598	116	0	0	954	691	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	167	0	765	0	0	0	0	198	84	512	957	0	529	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 30's ghost cells:	58	0	0	0	0	0	0	2	0	0	0	0	314	2	0	0	300	0	0	0	204	145	24	0	1128	0	604	362	692	530	0	0	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	163	255	741	128	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	586	1024	
Rank 32's ghost cells:	256	0	530	0	128	0	0	304	0	466	0	55	48	0	0	0	0	0	0	0	0	0	0	0	103	0	0	0	0	0	0	0	0	987	624	443	0	250	0	322	0	0	0	0	161	88	392	0	
Rank 33's ghost cells:	0	0	0	0	0	0	16	304	161	116	0	0	0	0	0	0	0	0	0	0	0	20	0	0	31	0	0	0	0	0	0	0	989	0	198	229	0	767	0	145	0	0	98	0	95	317	615	774	
Rank 34's ghost cells:	297	205	127	104	595	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	649	208	0	1139	295	0	0	0	0	0	26	0	731	0	0	0	
Rank 35's ghost cells:	0	0	0	0	384	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	438	222	1143	0	1017	584	0	0	0	0	274	0	81	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	295	1008	0	514	0	0	0	0	492	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	249	785	0	548	509	0	0	12	59	0	584	0	0	13	417	141	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	652	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	683	237	0	677	0	0	0	0	
Rank 39's ghost cells:	137	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	291	199	0	0	526	0	0	0	388	0	0	0	0	0	233	0	327	151	0	0	0	15	0	0	1067	0	179	299	755	500	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	409	0	0	0	0	0	0	0	105	0	0	0	0	0	0	0	0	0	0	0	0	59	703	1049	0	1113	408	621	0	432	0	3	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	0	0	0	0	0	0	417	431	0	0	0	0	0	0	0	0	0	0	0	0	0	237	0	1113	0	0	0	0	501	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	97	31	262	492	581	0	181	412	0	0	1004	599	40	0	21	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	15	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	658	298	641	0	1026	0	607	0	0	0	
Rank 44's ghost cells:	216	85	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	961	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	96	721	81	0	0	0	774	0	0	607	601	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	862	0	0	0	0	0	0	0	0	0	0	0	0	0	0	287	0	201	588	0	0	0	0	0	0	0	73	319	0	0	0	13	0	500	432	500	40	0	0	0	0	386	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	122	655	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	587	394	632	0	0	0	417	0	0	0	0	0	0	0	0	0	535	
Rank 47's ghost cells:	0	0	0	0	0	0	704	82	357	0	0	0	0	0	0	0	0	0	0	0	0	36	0	0	0	0	0	0	0	0	0	1020	0	754	0	0	0	141	0	0	3	0	21	0	0	383	570	0	
