Number of cores/ranks per node is: 8
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 333331
Cell on rank 1 before loadbalancing: 333344
Cell on rank 2 before loadbalancing: 333325
Edge-cut: 19019


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	771	1116	806	0	0	294	1609	0	0	0	0	0	0	1026	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	775	0	811	911	0	0	1520	162	0	0	0	0	0	76	137	996	0	0	0	0	0	0	0	0	
From rank 2 to: 	1118	811	0	2153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	816	917	2153	0	0	0	0	0	0	0	0	0	0	2265	118	16	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	1516	88	1370	0	0	0	0	0	0	0	0	0	0	0	0	639	0	0	0	
From rank 5 to: 	0	0	0	0	1518	0	1488	125	0	0	0	0	0	0	0	0	0	0	0	0	541	1301	0	233	
From rank 6 to: 	293	1523	0	0	94	1487	0	1086	0	0	0	0	0	0	138	341	0	0	0	0	0	19	304	949	
From rank 7 to: 	1615	156	0	0	1364	125	1081	0	0	0	0	0	0	0	66	0	0	0	0	0	107	0	861	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	2252	411	818	435	0	1396	1251	1355	0	0	0	0	0	261	64	
From rank 9 to: 	0	0	0	0	0	0	0	0	2259	0	1051	799	0	0	0	0	495	1437	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	400	1059	0	1001	1065	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	830	806	1004	0	1672	0	0	24	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	435	0	1058	1679	0	2309	450	674	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	76	0	2262	0	0	0	0	0	0	0	0	2309	0	1017	827	0	0	0	0	0	0	0	0	
From rank 14 to: 	1018	137	0	108	0	0	138	66	1396	0	0	0	434	1016	0	733	0	0	0	0	0	0	1185	394	
From rank 15 to: 	0	997	0	16	0	0	343	0	1240	0	0	24	688	825	732	0	148	0	0	0	0	0	0	1318	
From rank 16 to: 	0	0	0	0	0	0	0	0	1320	494	0	0	0	0	0	115	0	2390	238	1601	0	0	907	1123	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	1437	0	0	0	0	0	0	2381	0	1588	260	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	238	1574	0	2246	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1626	260	2214	0	974	1311	275	313	
From rank 20 to: 	0	0	0	0	639	538	0	103	0	0	0	0	0	0	0	0	0	0	0	998	0	868	1393	0	
From rank 21 to: 	0	0	0	0	0	1301	19	0	0	0	0	0	0	0	0	0	0	0	0	1312	864	0	405	1550	
From rank 22 to: 	0	0	0	0	0	0	317	864	225	0	0	0	0	0	1193	0	914	0	0	248	1391	402	0	824	
From rank 23 to: 	0	0	0	0	0	241	969	0	57	0	0	0	0	0	394	1316	1123	0	0	320	0	1553	826	0	




=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.4305         0.221899
    2          32.2876         0.582489
    3          22.7739         0.705344
    4           19.308         0.847814
    5          17.4858         0.905623
    6          14.2354         0.814113
    7          12.1622         0.854364
    8          11.7161          0.96332
    9          10.0099         0.854368
   10          8.60677          0.85983
   11          8.57399         0.996191
   12           7.9673         0.929241
   13          6.75338         0.847637
   14          6.66217         0.986495
   15          6.55832         0.984411
   16             5.67         0.864551
   17          5.43539         0.958623
   18          5.39248         0.992105
   19          4.80105         0.890324
   20          4.62566         0.963469
   21          4.55785         0.985339
   22            4.131         0.906348
   23          4.00213         0.968804
   24          3.88503         0.970741
   25          3.52779         0.908046
   26          3.41877         0.969098
   27          3.37835         0.988176
   28          3.08287         0.912538
   29          2.98407         0.967953
   30          2.90768           0.9744
   31          2.74741          0.94488
   32          2.83824          1.03306
   33          2.99907          1.05667
   34          3.06078          1.02058
   35          3.12014          1.01939
   36           2.7888         0.893805
   37          2.14728         0.769964
   38          1.64496         0.766067
   39          1.31726         0.800789
   40          1.13697         0.863131
   41          1.08691         0.955966
   42         0.958553          0.88191
   43         0.786502          0.82051
   44         0.684922         0.870845
   45         0.593894         0.867097
   46         0.494992         0.833469
   47         0.423021         0.854602
   48         0.344759         0.814992
   49         0.283316         0.821781
   50         0.245809         0.867612
   51         0.206613         0.840544
   52          0.17318         0.838185
   53         0.149206         0.861564
   54         0.122793          0.82298
   55         0.105545         0.859535
   56          0.08754         0.829409
   57        0.0692562         0.791138
   58        0.0586268          0.84652
   59        0.0472006         0.805103
   60        0.0373967         0.792293
   61        0.0305138         0.815948
   62        0.0238916         0.782977
=== rate=0.861334, T=6.91208, TIT=0.111485, IT=62

 Elapsed time: 6.91208
Rank 0: Matrix-vector product took 0.0382968 seconds
Rank 1: Matrix-vector product took 0.0382121 seconds
Rank 2: Matrix-vector product took 0.036911 seconds
Rank 3: Matrix-vector product took 0.0386463 seconds
Rank 4: Matrix-vector product took 0.036456 seconds
Rank 5: Matrix-vector product took 0.0378029 seconds
Rank 6: Matrix-vector product took 0.046338 seconds
Rank 7: Matrix-vector product took 0.0380949 seconds
Rank 8: Matrix-vector product took 0.0403072 seconds
Rank 9: Matrix-vector product took 0.0384088 seconds
Rank 10: Matrix-vector product took 0.036463 seconds
Rank 11: Matrix-vector product took 0.0371382 seconds
Rank 12: Matrix-vector product took 0.0389749 seconds
Rank 13: Matrix-vector product took 0.0388146 seconds
Rank 14: Matrix-vector product took 0.0389765 seconds
Rank 15: Matrix-vector product took 0.0388695 seconds
Rank 16: Matrix-vector product took 0.0401149 seconds
Rank 17: Matrix-vector product took 0.0380122 seconds
Rank 18: Matrix-vector product took 0.0368777 seconds
Rank 19: Matrix-vector product took 0.0392006 seconds
Rank 20: Matrix-vector product took 0.0374923 seconds
Rank 21: Matrix-vector product took 0.0380889 seconds
Rank 22: Matrix-vector product took 0.0391502 seconds
Rank 23: Matrix-vector product took 0.0391323 seconds
Average time for Matrix-vector product is 0.0386158 seconds

Rank 0: copyOwnerToAll took 0.00061091 seconds
Rank 1: copyOwnerToAll took 0.00061091 seconds
Rank 2: copyOwnerToAll took 0.00061091 seconds
Rank 3: copyOwnerToAll took 0.00061091 seconds
Rank 4: copyOwnerToAll took 0.00061091 seconds
Rank 5: copyOwnerToAll took 0.00061091 seconds
Rank 6: copyOwnerToAll took 0.00061091 seconds
Rank 7: copyOwnerToAll took 0.00061091 seconds
Rank 8: copyOwnerToAll took 0.00061091 seconds
Rank 9: copyOwnerToAll took 0.00061091 seconds
Rank 10: copyOwnerToAll took 0.00061091 seconds
Rank 11: copyOwnerToAll took 0.00061091 seconds
Rank 12: copyOwnerToAll took 0.00061091 seconds
Rank 13: copyOwnerToAll took 0.00061091 seconds
Rank 14: copyOwnerToAll took 0.00061091 seconds
Rank 15: copyOwnerToAll took 0.00061091 seconds
Rank 16: copyOwnerToAll took 0.00061091 seconds
Rank 17: copyOwnerToAll took 0.00061091 seconds
Rank 18: copyOwnerToAll took 0.00061091 seconds
Rank 19: copyOwnerToAll took 0.00061091 seconds
Rank 20: copyOwnerToAll took 0.00061091 seconds
Rank 21: copyOwnerToAll took 0.00061091 seconds
Rank 22: copyOwnerToAll took 0.00061091 seconds
Rank 23: copyOwnerToAll took 0.00061091 seconds
Average time for copyOwnertoAll is 0.000642669 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	771	1116	806	0	0	294	1609	0	0	0	0	0	0	1026	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	775	0	811	911	0	0	1520	162	0	0	0	0	0	76	137	996	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	1118	811	0	2153	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	816	917	2153	0	0	0	0	0	0	0	0	0	0	2265	118	16	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	1516	88	1370	0	0	0	0	0	0	0	0	0	0	0	0	639	0	0	0	
Rank 5's ghost cells:	0	0	0	0	1518	0	1488	125	0	0	0	0	0	0	0	0	0	0	0	0	541	1301	0	233	
Rank 6's ghost cells:	293	1523	0	0	94	1487	0	1086	0	0	0	0	0	0	138	341	0	0	0	0	0	19	304	949	
Rank 7's ghost cells:	1615	156	0	0	1364	125	1081	0	0	0	0	0	0	0	66	0	0	0	0	0	107	0	861	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	2252	411	818	435	0	1396	1251	1355	0	0	0	0	0	261	64	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	2259	0	1051	799	0	0	0	0	495	1437	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	400	1059	0	1001	1065	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	830	806	1004	0	1672	0	0	24	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	435	0	1058	1679	0	2309	450	674	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	76	0	2262	0	0	0	0	0	0	0	0	2309	0	1017	827	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	1018	137	0	108	0	0	138	66	1396	0	0	0	434	1016	0	733	0	0	0	0	0	0	1185	394	
Rank 15's ghost cells:	0	997	0	16	0	0	343	0	1240	0	0	24	688	825	732	0	148	0	0	0	0	0	0	1318	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	1320	494	0	0	0	0	0	115	0	2390	238	1601	0	0	907	1123	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	1437	0	0	0	0	0	0	2381	0	1588	260	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	238	1574	0	2246	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1626	260	2214	0	974	1311	275	313	
Rank 20's ghost cells:	0	0	0	0	639	538	0	103	0	0	0	0	0	0	0	0	0	0	0	998	0	868	1393	0	
Rank 21's ghost cells:	0	0	0	0	0	1301	19	0	0	0	0	0	0	0	0	0	0	0	0	1312	864	0	405	1550	
Rank 22's ghost cells:	0	0	0	0	0	0	317	864	225	0	0	0	0	0	1193	0	914	0	0	248	1391	402	0	824	
Rank 23's ghost cells:	0	0	0	0	0	241	969	0	57	0	0	0	0	0	394	1316	1123	0	0	320	0	1553	826	0	
