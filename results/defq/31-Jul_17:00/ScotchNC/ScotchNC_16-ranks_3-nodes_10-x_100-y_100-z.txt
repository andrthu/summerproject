Number of cores/ranks per node is: 16
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 33500
Cell on rank 1 before loadbalancing: 33180
Cell on rank 2 before loadbalancing: 33320


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
From rank 0 to: 	0	160	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	160	0	10	128	0	153	27	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	49	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	130	10	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	128	160	0	0	0	62	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	160	107	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	60	0	0	
From rank 5 to: 	0	153	0	0	160	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	27	0	60	107	123	0	200	17	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	100	0	0	200	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	156	0	17	0	0	160	129	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	160	0	1	120	0	120	0	51	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	35	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	57	90	129	1	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	120	150	0	0	0	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	180	112	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	120	0	0	180	0	24	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	64	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	109	24	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	51	0	149	0	120	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	120	23	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	10	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	160	140	0	20	0	0	0	0	0	0	0	95	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	23	127	160	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	140	130	10	0	120	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	150	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	20	0	130	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	140	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	170	100	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	170	0	0	75	55	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	50	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	75	200	0	188	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 28 to: 	0	49	0	0	0	61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	188	0	160	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	60	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	30	0	0	0	0	150	0	0	60	43	0	187	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	34	96	0	0	0	0	0	0	0	0	0	90	187	0	0	0	0	0	0	0	20	0	0	0	0	0	0	20	0	150	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	70	140	80	0	0	0	0	0	0	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	106	104	20	0	0	80	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	180	196	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	180	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	190	0	0	200	0	100	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	110	30	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	150	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	110	0	140	0	0	0	0	0	0	34	112	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	80	0	0	100	30	140	0	0	30	0	0	0	0	124	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	140	0	140	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	30	140	0	110	50	40	0	150	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	95	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	110	160	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	116	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	60	110	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	20	0	0	0	110	35	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	160	0	0	210	90	20	
From rank 45 to: 	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	10	0	0	0	0	0	0	0	0	0	0	0	0	220	0	0	130	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	34	124	0	160	0	0	90	0	0	150	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	0	0	0	112	0	0	0	0	0	20	130	150	0	




=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.2562         0.238729
    2          23.3931         0.611486
    3          21.1832         0.905528
    4           19.018         0.897789
    5          9.57919         0.503691
    6          5.25523         0.548609
    7          4.02701         0.766286
    8          2.45039         0.608488
    9          1.28591         0.524777
   10         0.912713         0.709782
   11         0.614669         0.673453
   12         0.329407         0.535909
   13         0.237919         0.722264
   14         0.154332         0.648674
   15         0.100086         0.648515
   16         0.062535          0.62481
   17        0.0401469          0.64199
   18        0.0269718         0.671828
   19        0.0165497         0.613593
   20        0.0104944         0.634113
=== rate=0.617743, T=0.162444, TIT=0.00812219, IT=20

 Elapsed time: 0.162444
Rank 0: Matrix-vector product took 0.00196895 seconds
Rank 1: Matrix-vector product took 0.00208539 seconds
Rank 2: Matrix-vector product took 0.00186505 seconds
Rank 3: Matrix-vector product took 0.0019524 seconds
Rank 4: Matrix-vector product took 0.00206494 seconds
Rank 5: Matrix-vector product took 0.00205756 seconds
Rank 6: Matrix-vector product took 0.00207116 seconds
Rank 7: Matrix-vector product took 0.00190062 seconds
Rank 8: Matrix-vector product took 0.00206817 seconds
Rank 9: Matrix-vector product took 0.00206456 seconds
Rank 10: Matrix-vector product took 0.0019767 seconds
Rank 11: Matrix-vector product took 0.00196198 seconds
Rank 12: Matrix-vector product took 0.00199354 seconds
Rank 13: Matrix-vector product took 0.00208543 seconds
Rank 14: Matrix-vector product took 0.00187576 seconds
Rank 15: Matrix-vector product took 0.00200652 seconds
Rank 16: Matrix-vector product took 0.00198079 seconds
Rank 17: Matrix-vector product took 0.00194325 seconds
Rank 18: Matrix-vector product took 0.00205624 seconds
Rank 19: Matrix-vector product took 0.00209355 seconds
Rank 20: Matrix-vector product took 0.00208542 seconds
Rank 21: Matrix-vector product took 0.00194788 seconds
Rank 22: Matrix-vector product took 0.00204722 seconds
Rank 23: Matrix-vector product took 0.00186798 seconds
Rank 24: Matrix-vector product took 0.00194541 seconds
Rank 25: Matrix-vector product took 0.00210211 seconds
Rank 26: Matrix-vector product took 0.00197569 seconds
Rank 27: Matrix-vector product took 0.00214467 seconds
Rank 28: Matrix-vector product took 0.0020772 seconds
Rank 29: Matrix-vector product took 0.00208435 seconds
Rank 30: Matrix-vector product took 0.00206597 seconds
Rank 31: Matrix-vector product took 0.00207792 seconds
Rank 32: Matrix-vector product took 0.00201594 seconds
Rank 33: Matrix-vector product took 0.00201318 seconds
Rank 34: Matrix-vector product took 0.00198415 seconds
Rank 35: Matrix-vector product took 0.00178703 seconds
Rank 36: Matrix-vector product took 0.00202822 seconds
Rank 37: Matrix-vector product took 0.00202281 seconds
Rank 38: Matrix-vector product took 0.00210921 seconds
Rank 39: Matrix-vector product took 0.00209256 seconds
Rank 40: Matrix-vector product took 0.00194302 seconds
Rank 41: Matrix-vector product took 0.00209514 seconds
Rank 42: Matrix-vector product took 0.00208107 seconds
Rank 43: Matrix-vector product took 0.00201205 seconds
Rank 44: Matrix-vector product took 0.00217286 seconds
Rank 45: Matrix-vector product took 0.00204896 seconds
Rank 46: Matrix-vector product took 0.0020763 seconds
Rank 47: Matrix-vector product took 0.00422388 seconds
Average time for Matrix-vector product is 0.00206668 seconds

Rank 0: copyOwnerToAll took 0.00018595 seconds
Rank 1: copyOwnerToAll took 0.00018595 seconds
Rank 2: copyOwnerToAll took 0.00018595 seconds
Rank 3: copyOwnerToAll took 0.00018595 seconds
Rank 4: copyOwnerToAll took 0.00018595 seconds
Rank 5: copyOwnerToAll took 0.00018595 seconds
Rank 6: copyOwnerToAll took 0.00018595 seconds
Rank 7: copyOwnerToAll took 0.00018595 seconds
Rank 8: copyOwnerToAll took 0.00018595 seconds
Rank 9: copyOwnerToAll took 0.00018595 seconds
Rank 10: copyOwnerToAll took 0.00018595 seconds
Rank 11: copyOwnerToAll took 0.00018595 seconds
Rank 12: copyOwnerToAll took 0.00018595 seconds
Rank 13: copyOwnerToAll took 0.00018595 seconds
Rank 14: copyOwnerToAll took 0.00018595 seconds
Rank 15: copyOwnerToAll took 0.00018595 seconds
Rank 16: copyOwnerToAll took 0.00018595 seconds
Rank 17: copyOwnerToAll took 0.00018595 seconds
Rank 18: copyOwnerToAll took 0.00018595 seconds
Rank 19: copyOwnerToAll took 0.00018595 seconds
Rank 20: copyOwnerToAll took 0.00018595 seconds
Rank 21: copyOwnerToAll took 0.00018595 seconds
Rank 22: copyOwnerToAll took 0.00018595 seconds
Rank 23: copyOwnerToAll took 0.00018595 seconds
Rank 24: copyOwnerToAll took 0.00018595 seconds
Rank 25: copyOwnerToAll took 0.00018595 seconds
Rank 26: copyOwnerToAll took 0.00018595 seconds
Rank 27: copyOwnerToAll took 0.00018595 seconds
Rank 28: copyOwnerToAll took 0.00018595 seconds
Rank 29: copyOwnerToAll took 0.00018595 seconds
Rank 30: copyOwnerToAll took 0.00018595 seconds
Rank 31: copyOwnerToAll took 0.00018595 seconds
Rank 32: copyOwnerToAll took 0.00018595 seconds
Rank 33: copyOwnerToAll took 0.00018595 seconds
Rank 34: copyOwnerToAll took 0.00018595 seconds
Rank 35: copyOwnerToAll took 0.00018595 seconds
Rank 36: copyOwnerToAll took 0.00018595 seconds
Rank 37: copyOwnerToAll took 0.00018595 seconds
Rank 38: copyOwnerToAll took 0.00018595 seconds
Rank 39: copyOwnerToAll took 0.00018595 seconds
Rank 40: copyOwnerToAll took 0.00018595 seconds
Rank 41: copyOwnerToAll took 0.00018595 seconds
Rank 42: copyOwnerToAll took 0.00018595 seconds
Rank 43: copyOwnerToAll took 0.00018595 seconds
Rank 44: copyOwnerToAll took 0.00018595 seconds
Rank 45: copyOwnerToAll took 0.00018595 seconds
Rank 46: copyOwnerToAll took 0.00018595 seconds
Rank 47: copyOwnerToAll took 0.00018595 seconds
Average time for copyOwnertoAll is 0.000223354 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	
Rank 0's ghost cells:	0	160	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	90	50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	160	0	10	128	0	153	27	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	49	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	130	10	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	128	160	0	0	0	62	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	160	107	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	60	0	0	
Rank 5's ghost cells:	0	153	0	0	160	0	123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	61	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	27	0	60	107	123	0	200	17	0	57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	100	0	0	200	0	0	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	156	0	17	0	0	160	129	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	160	0	1	120	0	120	0	51	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	35	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	57	90	129	1	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	120	150	0	0	0	0	149	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	180	112	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	116	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	120	0	0	180	0	24	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	64	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	109	24	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	51	0	149	0	120	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	120	23	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	127	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	10	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	160	140	0	20	0	0	0	0	0	0	0	95	29	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	23	127	160	0	0	0	0	0	0	0	0	0	0	0	0	96	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	140	130	10	0	120	0	0	0	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	150	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	20	0	130	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	140	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	0	170	100	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	170	0	0	75	55	0	150	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	50	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	30	75	200	0	188	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 28's ghost cells:	0	49	0	0	0	61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	188	0	160	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	60	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	95	0	30	0	0	0	0	150	0	0	60	43	0	187	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	34	96	0	0	0	0	0	0	0	0	0	90	187	0	0	0	0	0	0	0	20	0	0	0	0	0	0	20	0	150	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	0	70	140	80	0	0	0	0	0	0	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	106	104	20	0	0	80	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	106	0	180	196	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	180	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	190	0	0	200	0	100	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	110	30	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	150	0	0	0	0	0	0	0	0	0	0	0	20	0	0	0	0	0	110	0	140	0	0	0	0	0	0	34	112	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	80	0	0	100	30	140	0	0	30	0	0	0	0	124	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	0	0	140	0	140	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	0	0	0	0	0	0	30	140	0	110	50	40	0	150	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	95	0	0	0	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	110	160	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	116	64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	60	110	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	20	0	0	0	110	35	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	40	160	0	0	210	90	20	
Rank 45's ghost cells:	0	0	0	0	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	10	0	0	0	0	0	0	0	0	0	0	0	0	220	0	0	130	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	34	124	0	160	0	0	90	0	0	150	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	0	0	0	0	0	112	0	0	0	0	0	20	130	150	0	
