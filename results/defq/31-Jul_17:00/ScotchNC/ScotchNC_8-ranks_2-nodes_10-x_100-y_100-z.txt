Number of cores/ranks per node is: 8
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 50000
Cell on rank 1 before loadbalancing: 50000


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	256	30	220	30	190	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	256	0	244	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	30	240	0	234	0	0	0	0	0	0	0	0	0	0	190	80	
From rank 3 to: 	226	0	234	0	310	0	0	0	0	0	0	0	0	0	0	240	
From rank 4 to: 	30	0	0	310	0	210	240	0	0	0	0	190	0	0	0	30	
From rank 5 to: 	190	0	0	0	210	0	70	240	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	240	70	0	230	0	0	240	60	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	240	230	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	240	0	250	0	280	0	10	
From rank 9 to: 	0	0	0	0	0	0	0	0	240	0	200	50	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	240	0	0	200	0	260	0	0	0	0	
From rank 11 to: 	0	0	0	0	180	0	60	0	260	60	260	0	0	0	0	190	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	220	240	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	280	0	0	0	220	0	60	240	
From rank 14 to: 	0	0	190	0	0	0	0	0	0	0	0	0	240	60	0	220	
From rank 15 to: 	0	0	80	240	30	0	0	0	10	0	0	190	0	240	220	0	




=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.7458         0.223063
    2          21.6181         0.604773
    3          20.3808         0.942764
    4          15.6437         0.767573
    5          7.62249         0.487256
    6          4.71599         0.618694
    7          3.01359         0.639015
    8          2.01592         0.668944
    9          1.17608         0.583393
   10         0.720706         0.612805
   11         0.543284         0.753823
   12         0.258845         0.476445
   13         0.172775         0.667484
   14         0.114861         0.664805
   15        0.0643062         0.559859
   16         0.041353         0.643063
   17        0.0273018         0.660214
   18        0.0160009         0.586074
=== rate=0.599434, T=0.333021, TIT=0.0185011, IT=18

 Elapsed time: 0.333021
Rank 0: Matrix-vector product took 0.00545998 seconds
Rank 1: Matrix-vector product took 0.00529577 seconds
Rank 2: Matrix-vector product took 0.0055755 seconds
Rank 3: Matrix-vector product took 0.00569283 seconds
Rank 4: Matrix-vector product took 0.00573547 seconds
Rank 5: Matrix-vector product took 0.00555414 seconds
Rank 6: Matrix-vector product took 0.00562108 seconds
Rank 7: Matrix-vector product took 0.00533028 seconds
Rank 8: Matrix-vector product took 0.00553317 seconds
Rank 9: Matrix-vector product took 0.00540344 seconds
Rank 10: Matrix-vector product took 0.00549379 seconds
Rank 11: Matrix-vector product took 0.00573789 seconds
Rank 12: Matrix-vector product took 0.00525365 seconds
Rank 13: Matrix-vector product took 0.00560104 seconds
Rank 14: Matrix-vector product took 0.00551653 seconds
Rank 15: Matrix-vector product took 0.00572403 seconds
Average time for Matrix-vector product is 0.00553304 seconds

Rank 0: copyOwnerToAll took 0.00016929 seconds
Rank 1: copyOwnerToAll took 0.00016929 seconds
Rank 2: copyOwnerToAll took 0.00016929 seconds
Rank 3: copyOwnerToAll took 0.00016929 seconds
Rank 4: copyOwnerToAll took 0.00016929 seconds
Rank 5: copyOwnerToAll took 0.00016929 seconds
Rank 6: copyOwnerToAll took 0.00016929 seconds
Rank 7: copyOwnerToAll took 0.00016929 seconds
Rank 8: copyOwnerToAll took 0.00016929 seconds
Rank 9: copyOwnerToAll took 0.00016929 seconds
Rank 10: copyOwnerToAll took 0.00016929 seconds
Rank 11: copyOwnerToAll took 0.00016929 seconds
Rank 12: copyOwnerToAll took 0.00016929 seconds
Rank 13: copyOwnerToAll took 0.00016929 seconds
Rank 14: copyOwnerToAll took 0.00016929 seconds
Rank 15: copyOwnerToAll took 0.00016929 seconds
Average time for copyOwnertoAll is 0.00023651 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	256	30	220	30	190	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	256	0	244	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	30	240	0	234	0	0	0	0	0	0	0	0	0	0	190	80	
Rank 3's ghost cells:	226	0	234	0	310	0	0	0	0	0	0	0	0	0	0	240	
Rank 4's ghost cells:	30	0	0	310	0	210	240	0	0	0	0	190	0	0	0	30	
Rank 5's ghost cells:	190	0	0	0	210	0	70	240	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	240	70	0	230	0	0	240	60	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	240	230	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	240	0	250	0	280	0	10	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	240	0	200	50	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	240	0	0	200	0	260	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	180	0	60	0	260	60	260	0	0	0	0	190	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	220	240	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	280	0	0	0	220	0	60	240	
Rank 14's ghost cells:	0	0	190	0	0	0	0	0	0	0	0	0	240	60	0	220	
Rank 15's ghost cells:	0	0	80	240	30	0	0	0	10	0	0	190	0	240	220	0	
