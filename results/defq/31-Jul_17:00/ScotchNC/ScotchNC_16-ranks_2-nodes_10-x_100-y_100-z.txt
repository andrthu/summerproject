Number of cores/ranks per node is: 16
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 50000
Cell on rank 1 before loadbalancing: 50000


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	210	0	0	130	0	0	0	0	0	0	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	210	0	110	130	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	110	0	210	40	0	104	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	130	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	130	120	40	0	0	180	80	0	0	0	0	64	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	180	0	140	0	0	0	0	190	0	0	0	0	130	50	0	0	0	0	0	0	0	20	0	0	0	0	0	0	
From rank 6 to: 	0	0	104	0	70	140	0	200	0	0	0	0	0	0	0	0	0	87	0	73	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	150	0	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	230	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	225	0	95	0	0	90	70	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	100	85	0	256	20	117	0	0	0	0	0	0	0	0	0	0	140	0	0	40	0	0	0	0	
From rank 11 to: 	0	0	0	0	64	190	0	0	0	0	256	0	94	0	0	0	0	0	0	0	0	0	0	0	20	110	0	0	0	0	0	0	
From rank 12 to: 	210	0	0	0	66	0	0	0	0	0	20	90	0	264	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	90	123	0	264	0	220	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	70	0	0	0	230	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	170	0	0	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	237	0	0	86	50	0	0	0	220	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	60	87	0	0	0	0	0	0	0	0	0	237	0	100	150	67	0	33	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	210	0	0	74	166	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	73	150	0	0	0	0	0	0	0	0	0	150	210	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	86	64	0	0	0	240	250	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	240	0	0	0	0	30	0	0	0	0	140	120	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	71	0	250	0	0	206	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	167	0	0	0	206	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	140	30	0	0	0	0	0	0	0	0	0	0	0	0	0	170	110	140	0	30	0	100	
From rank 25 to: 	0	0	0	0	0	20	0	0	0	0	0	110	0	0	0	0	220	0	0	0	0	30	0	0	170	0	0	0	0	0	0	160	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	220	0	230	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	200	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	220	0	0	0	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	130	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	230	0	230	0	40	90	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	130	30	0	190	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	100	160	0	0	0	90	190	0	




=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.6456         0.234918
    2          22.9158         0.608725
    3          21.1609         0.923419
    4          18.4508         0.871928
    5          9.04421         0.490181
    6          5.04091         0.557363
    7          3.66363         0.726779
    8          2.30629         0.629509
    9          1.23149         0.533972
   10         0.848474         0.688981
   11         0.629166         0.741526
   12         0.311638         0.495319
   13         0.216437         0.694515
   14         0.143212          0.66168
   15        0.0829978         0.579544
   16        0.0543121         0.654381
   17        0.0347304         0.639459
   18        0.0211671          0.60947
   19        0.0134477          0.63531
=== rate=0.610191, T=0.183801, TIT=0.00967372, IT=19

 Elapsed time: 0.183801
Rank 0: Matrix-vector product took 0.00292535 seconds
Rank 1: Matrix-vector product took 0.0028575 seconds
Rank 2: Matrix-vector product took 0.00301189 seconds
Rank 3: Matrix-vector product took 0.00271475 seconds
Rank 4: Matrix-vector product took 0.00295476 seconds
Rank 5: Matrix-vector product took 0.00303822 seconds
Rank 6: Matrix-vector product took 0.00301205 seconds
Rank 7: Matrix-vector product took 0.00285215 seconds
Rank 8: Matrix-vector product took 0.0028331 seconds
Rank 9: Matrix-vector product took 0.00294558 seconds
Rank 10: Matrix-vector product took 0.00310208 seconds
Rank 11: Matrix-vector product took 0.00305256 seconds
Rank 12: Matrix-vector product took 0.00301436 seconds
Rank 13: Matrix-vector product took 0.00299517 seconds
Rank 14: Matrix-vector product took 0.00284193 seconds
Rank 15: Matrix-vector product took 0.0027731 seconds
Rank 16: Matrix-vector product took 0.00302819 seconds
Rank 17: Matrix-vector product took 0.00303669 seconds
Rank 18: Matrix-vector product took 0.00291405 seconds
Rank 19: Matrix-vector product took 0.00294122 seconds
Rank 20: Matrix-vector product took 0.00297347 seconds
Rank 21: Matrix-vector product took 0.00293549 seconds
Rank 22: Matrix-vector product took 0.00290442 seconds
Rank 23: Matrix-vector product took 0.00270582 seconds
Rank 24: Matrix-vector product took 0.00297266 seconds
Rank 25: Matrix-vector product took 0.00296725 seconds
Rank 26: Matrix-vector product took 0.0028699 seconds
Rank 27: Matrix-vector product took 0.00289783 seconds
Rank 28: Matrix-vector product took 0.00271063 seconds
Rank 29: Matrix-vector product took 0.00289383 seconds
Rank 30: Matrix-vector product took 0.00280299 seconds
Rank 31: Matrix-vector product took 0.00297647 seconds
Average time for Matrix-vector product is 0.00292048 seconds

Rank 0: copyOwnerToAll took 0.00014513 seconds
Rank 1: copyOwnerToAll took 0.00014513 seconds
Rank 2: copyOwnerToAll took 0.00014513 seconds
Rank 3: copyOwnerToAll took 0.00014513 seconds
Rank 4: copyOwnerToAll took 0.00014513 seconds
Rank 5: copyOwnerToAll took 0.00014513 seconds
Rank 6: copyOwnerToAll took 0.00014513 seconds
Rank 7: copyOwnerToAll took 0.00014513 seconds
Rank 8: copyOwnerToAll took 0.00014513 seconds
Rank 9: copyOwnerToAll took 0.00014513 seconds
Rank 10: copyOwnerToAll took 0.00014513 seconds
Rank 11: copyOwnerToAll took 0.00014513 seconds
Rank 12: copyOwnerToAll took 0.00014513 seconds
Rank 13: copyOwnerToAll took 0.00014513 seconds
Rank 14: copyOwnerToAll took 0.00014513 seconds
Rank 15: copyOwnerToAll took 0.00014513 seconds
Rank 16: copyOwnerToAll took 0.00014513 seconds
Rank 17: copyOwnerToAll took 0.00014513 seconds
Rank 18: copyOwnerToAll took 0.00014513 seconds
Rank 19: copyOwnerToAll took 0.00014513 seconds
Rank 20: copyOwnerToAll took 0.00014513 seconds
Rank 21: copyOwnerToAll took 0.00014513 seconds
Rank 22: copyOwnerToAll took 0.00014513 seconds
Rank 23: copyOwnerToAll took 0.00014513 seconds
Rank 24: copyOwnerToAll took 0.00014513 seconds
Rank 25: copyOwnerToAll took 0.00014513 seconds
Rank 26: copyOwnerToAll took 0.00014513 seconds
Rank 27: copyOwnerToAll took 0.00014513 seconds
Rank 28: copyOwnerToAll took 0.00014513 seconds
Rank 29: copyOwnerToAll took 0.00014513 seconds
Rank 30: copyOwnerToAll took 0.00014513 seconds
Rank 31: copyOwnerToAll took 0.00014513 seconds
Average time for copyOwnertoAll is 0.000188659 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	210	0	0	130	0	0	0	0	0	0	0	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	210	0	110	130	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	110	0	210	40	0	104	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	130	210	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	130	120	40	0	0	180	80	0	0	0	0	64	66	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	180	0	140	0	0	0	0	190	0	0	0	0	130	50	0	0	0	0	0	0	0	20	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	104	0	70	140	0	200	0	0	0	0	0	0	0	0	0	87	0	73	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	150	0	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	230	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	200	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	225	0	95	0	0	90	70	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	100	85	0	256	20	117	0	0	0	0	0	0	0	0	0	0	140	0	0	40	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	64	190	0	0	0	0	256	0	94	0	0	0	0	0	0	0	0	0	0	0	20	110	0	0	0	0	0	0	
Rank 12's ghost cells:	210	0	0	0	66	0	0	0	0	0	20	90	0	264	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	90	123	0	264	0	220	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	70	0	0	0	230	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	170	0	0	0	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	140	0	0	0	0	0	0	0	0	0	0	0	237	0	0	86	50	0	0	0	220	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	60	87	0	0	0	0	0	0	0	0	0	237	0	100	150	67	0	33	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	210	0	0	74	166	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	73	150	0	0	0	0	0	0	0	0	0	150	210	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	86	64	0	0	0	240	250	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	240	0	0	0	0	30	0	0	0	0	140	120	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	33	71	0	250	0	0	206	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	167	0	0	0	206	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	140	30	0	0	0	0	0	0	0	0	0	0	0	0	0	170	110	140	0	30	0	100	
Rank 25's ghost cells:	0	0	0	0	0	20	0	0	0	0	0	110	0	0	0	0	220	0	0	0	0	30	0	0	170	0	0	0	0	0	0	160	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	110	0	0	220	0	230	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	200	0	40	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	220	0	0	0	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	130	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	0	230	0	230	0	40	90	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	0	130	30	0	190	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	130	0	0	100	160	0	0	0	90	190	0	
