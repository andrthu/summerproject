Number of cores/ranks per node is: 8
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 41880
Cell on rank 1 before loadbalancing: 41393
Cell on rank 2 before loadbalancing: 41666
Cell on rank 3 before loadbalancing: 41296
Cell on rank 4 before loadbalancing: 42082
Cell on rank 5 before loadbalancing: 41627
Cell on rank 6 before loadbalancing: 41282
Cell on rank 7 before loadbalancing: 41462
Cell on rank 8 before loadbalancing: 42035
Cell on rank 9 before loadbalancing: 41335
Cell on rank 10 before loadbalancing: 41574
Cell on rank 11 before loadbalancing: 41599
Cell on rank 12 before loadbalancing: 42082
Cell on rank 13 before loadbalancing: 42039
Cell on rank 14 before loadbalancing: 41961
Cell on rank 15 before loadbalancing: 42062
Cell on rank 16 before loadbalancing: 41783
Cell on rank 17 before loadbalancing: 41429
Cell on rank 18 before loadbalancing: 42082
Cell on rank 19 before loadbalancing: 41298
Cell on rank 20 before loadbalancing: 41250
Cell on rank 21 before loadbalancing: 42064
Cell on rank 22 before loadbalancing: 41330
Cell on rank 23 before loadbalancing: 41389


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	1354	1047	0	366	377	0	0	876	0	0	0	484	0	0	422	0	1661	0	0	0	36	0	0	
From rank 1 to: 	1366	0	1159	0	0	0	0	622	313	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	1049	1159	0	1015	298	348	0	623	165	0	90	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	1015	0	702	1050	0	37	0	0	834	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	363	0	285	702	0	1301	0	0	48	0	1131	355	770	0	402	0	0	0	0	0	0	513	0	0	
From rank 5 to: 	377	0	324	1050	1301	0	0	0	0	0	0	0	584	764	131	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	0	0	960	748	881	0	0	0	0	0	0	0	0	887	0	0	0	0	0	
From rank 7 to: 	0	625	629	37	0	0	961	0	1492	446	422	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	851	310	169	0	46	0	759	1491	0	306	336	0	0	0	0	643	0	0	958	0	0	114	0	0	
From rank 9 to: 	0	0	0	0	0	0	880	464	309	0	905	1025	0	0	0	0	0	0	409	0	0	629	0	0	
From rank 10 to: 	0	0	84	834	1122	0	0	405	336	905	0	1159	0	0	0	0	0	0	0	0	0	404	0	0	
From rank 11 to: 	0	0	0	0	355	0	0	0	0	1026	1150	0	0	0	0	0	0	0	0	0	0	1060	0	0	
From rank 12 to: 	474	0	0	0	787	574	0	0	0	0	0	0	0	917	1023	516	115	893	55	206	0	522	57	376	
From rank 13 to: 	0	0	0	0	0	764	0	0	0	0	0	0	900	0	1457	0	522	92	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	401	115	0	0	0	0	0	0	999	1458	0	58	344	0	0	107	0	118	788	522	
From rank 15 to: 	416	0	0	0	0	0	0	0	644	0	0	0	514	0	59	0	1068	800	388	705	953	23	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	113	532	345	1069	0	1611	0	72	0	0	0	0	
From rank 17 to: 	1664	0	0	0	0	0	0	0	0	0	0	0	885	92	0	805	1611	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	887	0	947	394	0	0	55	0	0	383	0	0	0	1021	1050	733	0	257	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	213	0	113	730	72	0	1020	0	1275	93	0	1420	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	953	0	0	1072	1274	0	0	0	0	
From rank 21 to: 	38	0	0	0	506	0	0	0	123	612	404	1055	525	0	118	29	0	0	731	95	0	0	1153	933	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	58	0	788	0	0	0	0	0	0	1174	0	1553	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	370	0	521	0	0	0	261	1419	0	931	1553	0	

Edge-cut for node partition: 20532





=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.3756          0.22168
    2          32.1779         0.581084
    3          22.9267           0.7125
    4          20.0791         0.875793
    5          18.1717         0.905008
    6          14.2289         0.783025
    7          12.3048         0.864773
    8          11.9756         0.973249
    9          10.0754          0.84133
   10          8.81201         0.874603
   11          8.75813         0.993885
   12          7.81278          0.89206
   13           6.7284         0.861205
   14          6.79076          1.00927
   15          6.44228         0.948683
   16          5.58013         0.866173
   17          5.52916         0.990867
   18           5.4124         0.978882
   19          4.75688         0.878885
   20          4.74132         0.996729
   21          4.62613         0.975706
   22          4.17062         0.901535
   23          4.11471         0.986594
   24          3.88421         0.943982
   25          3.54916          0.91374
   26          3.53761         0.996746
   27          3.31662         0.937533
   28          3.08304         0.929573
   29          3.09524          1.00396
   30          2.90435         0.938329
   31          2.79318         0.961721
   32          2.92693          1.04788
   33          2.98092          1.01845
   34          3.06425          1.02795
   35          3.11821          1.01761
   36          2.74822         0.881345
   37          2.30224         0.837722
   38          1.87718         0.815372
   39          1.49656         0.797235
   40          1.31161         0.876417
   41          1.19603         0.911881
   42          1.04605         0.874602
   43         0.908937         0.868922
   44         0.764259         0.840828
   45         0.652479         0.853741
   46         0.561724         0.860906
   47         0.464927          0.82768
   48         0.395267         0.850168
   49         0.333321         0.843282
   50         0.275291         0.825902
   51         0.237363         0.862227
   52          0.20236         0.852533
   53         0.171044          0.84525
   54         0.144233         0.843249
   55         0.120875         0.838053
   56         0.100844         0.834287
   57        0.0840184         0.833148
   58        0.0702027         0.835563
   59        0.0572742          0.81584
   60        0.0467001         0.815379
   61        0.0377162         0.807626
   62        0.0302281          0.80146
   63        0.0248513         0.822125
=== rate=0.863918, T=6.92282, TIT=0.109886, IT=63

 Elapsed time: 6.92282
Rank 0: Matrix-vector product took 0.0362288 seconds
Rank 1: Matrix-vector product took 0.0372057 seconds
Rank 2: Matrix-vector product took 0.0388006 seconds
Rank 3: Matrix-vector product took 0.0371433 seconds
Rank 4: Matrix-vector product took 0.0377424 seconds
Rank 5: Matrix-vector product took 0.0365387 seconds
Rank 6: Matrix-vector product took 0.0385994 seconds
Rank 7: Matrix-vector product took 0.0374351 seconds
Rank 8: Matrix-vector product took 0.0390538 seconds
Rank 9: Matrix-vector product took 0.0362894 seconds
Rank 10: Matrix-vector product took 0.0375063 seconds
Rank 11: Matrix-vector product took 0.0363632 seconds
Rank 12: Matrix-vector product took 0.0385955 seconds
Rank 13: Matrix-vector product took 0.0368171 seconds
Rank 14: Matrix-vector product took 0.0375188 seconds
Rank 15: Matrix-vector product took 0.0360733 seconds
Rank 16: Matrix-vector product took 0.0387082 seconds
Rank 17: Matrix-vector product took 0.0373181 seconds
Rank 18: Matrix-vector product took 0.0391858 seconds
Rank 19: Matrix-vector product took 0.0369897 seconds
Rank 20: Matrix-vector product took 0.0378338 seconds
Rank 21: Matrix-vector product took 0.0388448 seconds
Rank 22: Matrix-vector product took 0.0362695 seconds
Rank 23: Matrix-vector product took 0.0374659 seconds
Average time for Matrix-vector product is 0.037522 seconds

Rank 0: copyOwnerToAll took 0.00062255 seconds
Rank 1: copyOwnerToAll took 0.00062255 seconds
Rank 2: copyOwnerToAll took 0.00062255 seconds
Rank 3: copyOwnerToAll took 0.00062255 seconds
Rank 4: copyOwnerToAll took 0.00062255 seconds
Rank 5: copyOwnerToAll took 0.00062255 seconds
Rank 6: copyOwnerToAll took 0.00062255 seconds
Rank 7: copyOwnerToAll took 0.00062255 seconds
Rank 8: copyOwnerToAll took 0.00062255 seconds
Rank 9: copyOwnerToAll took 0.00062255 seconds
Rank 10: copyOwnerToAll took 0.00062255 seconds
Rank 11: copyOwnerToAll took 0.00062255 seconds
Rank 12: copyOwnerToAll took 0.00062255 seconds
Rank 13: copyOwnerToAll took 0.00062255 seconds
Rank 14: copyOwnerToAll took 0.00062255 seconds
Rank 15: copyOwnerToAll took 0.00062255 seconds
Rank 16: copyOwnerToAll took 0.00062255 seconds
Rank 17: copyOwnerToAll took 0.00062255 seconds
Rank 18: copyOwnerToAll took 0.00062255 seconds
Rank 19: copyOwnerToAll took 0.00062255 seconds
Rank 20: copyOwnerToAll took 0.00062255 seconds
Rank 21: copyOwnerToAll took 0.00062255 seconds
Rank 22: copyOwnerToAll took 0.00062255 seconds
Rank 23: copyOwnerToAll took 0.00062255 seconds
Average time for copyOwnertoAll is 0.000700446 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	960	748	881	0	0	887	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	961	0	1492	446	422	0	0	0	0	625	629	37	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	759	1491	0	306	336	0	958	0	851	310	169	0	643	0	0	0	46	0	0	0	0	114	0	0	
Rank 3's ghost cells:	880	464	309	0	905	1025	409	0	0	0	0	0	0	0	0	0	0	0	0	0	0	629	0	0	
Rank 4's ghost cells:	0	405	336	905	0	1159	0	0	0	0	84	834	0	0	0	0	1122	0	0	0	0	404	0	0	
Rank 5's ghost cells:	0	0	0	1026	1150	0	0	0	0	0	0	0	0	0	0	0	355	0	0	0	0	1060	0	0	
Rank 6's ghost cells:	887	0	947	394	0	0	0	1021	0	0	0	0	383	0	0	1050	0	0	55	0	0	733	0	257	
Rank 7's ghost cells:	0	0	0	0	0	0	1020	0	0	0	0	0	730	72	0	1275	0	0	213	0	113	93	0	1420	
Rank 8's ghost cells:	0	0	876	0	0	0	0	0	0	1354	1047	0	422	0	1661	0	366	377	484	0	0	36	0	0	
Rank 9's ghost cells:	0	622	313	0	0	0	0	0	1366	0	1159	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	623	165	0	90	0	0	0	1049	1159	0	1015	0	0	0	0	298	348	0	0	0	0	0	0	
Rank 11's ghost cells:	0	37	0	0	834	0	0	0	0	0	1015	0	0	0	0	0	702	1050	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	644	0	0	0	388	705	416	0	0	0	0	1068	800	953	0	0	514	0	59	23	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	72	0	0	0	0	1069	0	1611	0	0	0	113	532	345	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	1664	0	0	0	805	1611	0	0	0	0	885	92	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	1072	1274	0	0	0	0	953	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	48	0	1131	355	0	0	363	0	285	702	0	0	0	0	0	1301	770	0	402	513	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	377	0	324	1050	0	0	0	0	1301	0	584	764	131	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	55	206	474	0	0	0	516	115	893	0	787	574	0	917	1023	522	57	376	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	522	92	0	0	764	900	0	1457	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	107	0	0	0	0	58	344	0	0	401	115	999	1458	0	118	788	522	
Rank 21's ghost cells:	0	0	123	612	404	1055	731	95	38	0	0	0	29	0	0	0	506	0	525	0	118	0	1153	933	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	0	788	1174	0	1553	
Rank 23's ghost cells:	0	0	0	0	0	0	261	1419	0	0	0	0	0	0	0	0	0	0	370	0	521	931	1553	0	
