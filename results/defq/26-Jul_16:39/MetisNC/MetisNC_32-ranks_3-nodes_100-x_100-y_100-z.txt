Number of cores/ranks per node is: 10
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 333331
Cell on rank 1 before loadbalancing: 333344
Cell on rank 2 before loadbalancing: 333325
Edge-cut: 19019
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	937	27	901	740	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	980	422	0	0	17	
From rank 1 to: 	934	0	827	537	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	619	0	0	0	
From rank 2 to: 	25	827	0	1440	658	0	0	0	1088	249	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	905	544	1443	0	1251	0	0	0	0	817	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	97	1433	0	599	
From rank 4 to: 	742	9	662	1236	0	0	0	0	0	268	1031	0	0	0	0	75	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	344	
From rank 5 to: 	0	0	0	0	0	0	1245	416	142	935	142	764	354	536	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	1247	0	717	1227	84	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	418	706	0	0	0	1074	0	900	0	89	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	1088	0	0	145	1244	0	0	1230	660	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	244	816	274	940	83	0	1232	0	775	0	0	713	106	610	0	0	0	0	0	0	0	0	0	0	0	0	0	419	0	27	
From rank 10 to: 	0	0	120	0	1031	146	125	1075	647	750	0	0	0	0	527	367	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	764	0	0	0	0	0	0	706	1090	401	0	135	0	0	0	459	923	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	348	0	900	0	0	0	707	0	0	1357	0	0	0	0	0	0	705	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	500	0	0	0	713	0	1098	0	0	454	534	1042	0	0	0	0	81	0	0	0	0	0	0	0	675	115	0	
From rank 14 to: 	0	0	0	0	0	176	0	78	0	106	526	401	1350	453	0	1742	146	0	648	0	0	435	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	82	0	0	0	0	598	369	0	0	554	1740	0	568	0	521	0	0	0	15	0	0	0	0	0	0	325	546	1058	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	135	0	1043	155	567	0	1053	677	0	450	284	766	34	0	0	0	0	0	0	519	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1053	0	1213	661	740	0	145	1196	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	627	521	675	1230	0	499	68	731	1059	113	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	668	479	0	560	1198	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	459	0	0	0	0	432	740	61	560	0	883	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	929	705	81	424	0	289	0	731	1198	902	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	767	145	1056	0	0	0	0	1831	0	719	972	0	0	0	1115	558	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	26	1196	113	0	0	0	1831	0	1284	386	203	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1284	0	1290	1285	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	733	372	1292	0	588	262	748	0	734	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	971	170	1293	588	0	897	0	0	226	347	
From rank 27 to: 	980	0	0	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	261	907	0	720	116	382	1280	
From rank 28 to: 	422	619	0	131	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	752	0	712	0	998	575	33	
From rank 29 to: 	0	0	0	1432	33	0	0	0	0	394	0	0	0	675	0	332	0	0	0	0	0	0	0	0	0	0	0	116	1000	0	1496	702	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	554	511	0	0	0	0	0	1122	0	0	718	216	396	582	1500	0	729	
From rank 31 to: 	17	0	0	598	344	0	0	0	0	20	0	0	0	0	0	1059	0	0	0	0	0	0	558	0	0	0	334	1300	34	695	724	0	
loadb
After loadbalancing process 25 has 38069 cells.
After loadbalancing process 8 has 34667 cells.
After loadbalancing process 24 has 37179 cells.
After loadbalancing process 19 has 33208 cells.
After loadbalancing process 7 has 33478 cells.
After loadbalancing process 0 has 34319 cells.
After loadbalancing process 17 has 35318 cells.
After loadbalancing process 1 has 33230 cells.
After loadbalancing process 2 has 34704 cells.
After loadbalancing process 6 has 33727 cells.
After loadbalancing process 13 has 35511 cells.
After loadbalancing process 18 has 35828 cells.
After loadbalancing process 11 has 34780 cells.
After loadbalancing process 20 has 33435 cells.
After loadbalancing process 10 has 35102 cells.
After loadbalancing process 9 has 36555 cells.
After loadbalancing process 30 has 39792 cells.
After loadbalancing process 22 has 40510 cells.
After loadbalancing process 28 has 37574 cells.
After loadbalancing process 26 has 37819 cells.
After loadbalancing process 21 has 35562 cells.
After loadbalancing process 23 has 38374 cells.
After loadbalancing process 12 has 34324 cells.
After loadbalancing process 27 has 37998 cells.
After loadbalancing process 5 has 35001 cells.
After loadbalancing process 16 has 35983 cells.
After loadbalancing process 4 has 34705 cells.
After loadbalancing process 31 has 39004 cells.
After loadbalancing process 3 has 37406 cells.
After loadbalancing process 29 has 39514 cells.
After loadbalancing process 14 has 36367 cells.
After loadbalancing process 15 has 36685 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	937	27	901	740	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	980	422	0	0	17	
Rank 1's ghost cells:	934	0	827	537	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	619	0	0	0	
Rank 2's ghost cells:	25	827	0	1440	658	0	0	0	1088	249	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	905	544	1443	0	1251	0	0	0	0	817	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	97	1433	0	599	
Rank 4's ghost cells:	742	9	662	1236	0	0	0	0	0	268	1031	0	0	0	0	75	0	0	0	0	0	0	0	0	0	0	0	0	0	33	0	344	
Rank 5's ghost cells:	0	0	0	0	0	0	1245	416	142	935	142	764	354	536	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	1247	0	717	1227	84	146	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	418	706	0	0	0	1074	0	900	0	89	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	1088	0	0	145	1244	0	0	1230	660	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	244	816	274	940	83	0	1232	0	775	0	0	713	106	610	0	0	0	0	0	0	0	0	0	0	0	0	0	419	0	27	
Rank 10's ghost cells:	0	0	120	0	1031	146	125	1075	647	750	0	0	0	0	527	367	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	764	0	0	0	0	0	0	706	1090	401	0	135	0	0	0	459	923	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	348	0	900	0	0	0	707	0	0	1357	0	0	0	0	0	0	705	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	500	0	0	0	713	0	1098	0	0	454	534	1042	0	0	0	0	81	0	0	0	0	0	0	0	675	115	0	
Rank 14's ghost cells:	0	0	0	0	0	176	0	78	0	106	526	401	1350	453	0	1742	146	0	648	0	0	435	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	82	0	0	0	0	598	369	0	0	554	1740	0	568	0	521	0	0	0	15	0	0	0	0	0	0	325	546	1058	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	135	0	1043	155	567	0	1053	677	0	450	284	766	34	0	0	0	0	0	0	519	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1053	0	1213	661	740	0	145	1196	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	627	521	675	1230	0	499	68	731	1059	113	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	668	479	0	560	1198	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	459	0	0	0	0	432	740	61	560	0	883	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	929	705	81	424	0	289	0	731	1198	902	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	767	145	1056	0	0	0	0	1831	0	719	972	0	0	0	1115	558	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	26	1196	113	0	0	0	1831	0	1284	386	203	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1284	0	1290	1285	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	733	372	1292	0	588	262	748	0	734	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	971	170	1293	588	0	897	0	0	226	347	
Rank 27's ghost cells:	980	0	0	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	261	907	0	720	116	382	1280	
Rank 28's ghost cells:	422	619	0	131	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	752	0	712	0	998	575	33	
Rank 29's ghost cells:	0	0	0	1432	33	0	0	0	0	394	0	0	0	675	0	332	0	0	0	0	0	0	0	0	0	0	0	116	1000	0	1496	702	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	115	0	554	511	0	0	0	0	0	1122	0	0	718	216	396	582	1500	0	729	
Rank 31's ghost cells:	17	0	0	598	344	0	0	0	0	20	0	0	0	0	0	1059	0	0	0	0	0	0	558	0	0	0	334	1300	34	695	724	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.8531         0.223592
    2          32.5721         0.583174
    3          23.0874         0.708809
    4          19.8854         0.861309
    5          17.7898         0.894616
    6          14.2525         0.801161
    7          12.2923         0.862468
    8          11.8468         0.963756
    9          10.0798         0.850851
   10          8.82914          0.87592
   11          8.80386         0.997137
   12          7.97166         0.905473
   13          7.00753         0.879056
   14          7.11456          1.01527
   15          6.52232         0.916756
   16          5.83758         0.895017
   17          5.82851         0.998445
   18          5.42759         0.931214
   19           4.8792         0.898962
   20          4.85929          0.99592
   21          4.49321         0.924663
   22          4.16876         0.927792
   23          4.15622         0.996992
   24          3.80924         0.916516
   25          3.54239         0.929946
   26          3.56573          1.00659
   27          3.36046         0.942432
   28          3.13511         0.932941
   29          3.14396          1.00282
   30          2.93733         0.934278
   31          2.80598         0.955282
   32           2.8984          1.03294
   33          2.90357          1.00178
   34          3.05032          1.05054
   35          3.22111          1.05599
   36          2.95561         0.917575
   37          2.45736         0.831422
   38          1.90419         0.774892
   39          1.43622         0.754244
   40          1.23373         0.859014
   41          1.14055         0.924471
   42          1.01033         0.885821
   43         0.869411         0.860526
   44         0.719701         0.827803
   45         0.607267         0.843777
   46         0.540971         0.890828
   47         0.458335         0.847246
   48         0.379401          0.82778
   49         0.318979         0.840744
   50         0.264739         0.829958
   51         0.226777         0.856607
   52         0.191483         0.844368
   53         0.159125         0.831012
   54          0.13808         0.867743
   55         0.118051         0.854945
   56        0.0966111         0.818387
   57        0.0804915         0.833149
   58        0.0675344         0.839026
   59        0.0549589          0.81379
   60        0.0454811         0.827548
   61        0.0373432         0.821071
   62        0.0299997         0.803351
   63        0.0240155         0.800526
=== rate=0.863449, T=5.91131, TIT=0.0938303, IT=63

 Elapsed time: 5.91131
Rank 0: Matrix-vector product took 0.0274166 seconds
Rank 1: Matrix-vector product took 0.0268866 seconds
Rank 2: Matrix-vector product took 0.0282026 seconds
Rank 3: Matrix-vector product took 0.0299128 seconds
Rank 4: Matrix-vector product took 0.028081 seconds
Rank 5: Matrix-vector product took 0.0284867 seconds
Rank 6: Matrix-vector product took 0.0272259 seconds
Rank 7: Matrix-vector product took 0.0272012 seconds
Rank 8: Matrix-vector product took 0.0277615 seconds
Rank 9: Matrix-vector product took 0.0294996 seconds
Rank 10: Matrix-vector product took 0.0283277 seconds
Rank 11: Matrix-vector product took 0.0281078 seconds
Rank 12: Matrix-vector product took 0.0276196 seconds
Rank 13: Matrix-vector product took 0.0286689 seconds
Rank 14: Matrix-vector product took 0.0294653 seconds
Rank 15: Matrix-vector product took 0.0315628 seconds
Rank 16: Matrix-vector product took 0.0293192 seconds
Rank 17: Matrix-vector product took 0.0285883 seconds
Rank 18: Matrix-vector product took 0.0288062 seconds
Rank 19: Matrix-vector product took 0.0269465 seconds
Rank 20: Matrix-vector product took 0.0270042 seconds
Rank 21: Matrix-vector product took 0.0289843 seconds
Rank 22: Matrix-vector product took 0.0326235 seconds
Rank 23: Matrix-vector product took 0.0308949 seconds
Rank 24: Matrix-vector product took 0.0296951 seconds
Rank 25: Matrix-vector product took 0.0306606 seconds
Rank 26: Matrix-vector product took 0.0306553 seconds
Rank 27: Matrix-vector product took 0.0308236 seconds
Rank 28: Matrix-vector product took 0.0302658 seconds
Rank 29: Matrix-vector product took 0.0318394 seconds
Rank 30: Matrix-vector product took 0.0322677 seconds
Rank 31: Matrix-vector product took 0.0317256 seconds
Average time for Matrix-vector product is 0.0292352 seconds

Rank 0: copyOwnerToAll took 0.00067671 seconds
Rank 1: copyOwnerToAll took 0.00067671 seconds
Rank 2: copyOwnerToAll took 0.00067671 seconds
Rank 3: copyOwnerToAll took 0.00067671 seconds
Rank 4: copyOwnerToAll took 0.00067671 seconds
Rank 5: copyOwnerToAll took 0.00067671 seconds
Rank 6: copyOwnerToAll took 0.00067671 seconds
Rank 7: copyOwnerToAll took 0.00067671 seconds
Rank 8: copyOwnerToAll took 0.00067671 seconds
Rank 9: copyOwnerToAll took 0.00067671 seconds
Rank 10: copyOwnerToAll took 0.00067671 seconds
Rank 11: copyOwnerToAll took 0.00067671 seconds
Rank 12: copyOwnerToAll took 0.00067671 seconds
Rank 13: copyOwnerToAll took 0.00067671 seconds
Rank 14: copyOwnerToAll took 0.00067671 seconds
Rank 15: copyOwnerToAll took 0.00067671 seconds
Rank 16: copyOwnerToAll took 0.00067671 seconds
Rank 17: copyOwnerToAll took 0.00067671 seconds
Rank 18: copyOwnerToAll took 0.00067671 seconds
Rank 19: copyOwnerToAll took 0.00067671 seconds
Rank 20: copyOwnerToAll took 0.00067671 seconds
Rank 21: copyOwnerToAll took 0.00067671 seconds
Rank 22: copyOwnerToAll took 0.00067671 seconds
Rank 23: copyOwnerToAll took 0.00067671 seconds
Rank 24: copyOwnerToAll took 0.00067671 seconds
Rank 25: copyOwnerToAll took 0.00067671 seconds
Rank 26: copyOwnerToAll took 0.00067671 seconds
Rank 27: copyOwnerToAll took 0.00067671 seconds
Rank 28: copyOwnerToAll took 0.00067671 seconds
Rank 29: copyOwnerToAll took 0.00067671 seconds
Rank 30: copyOwnerToAll took 0.00067671 seconds
Rank 31: copyOwnerToAll took 0.00067671 seconds
Average time for copyOwnertoAll is 0.000724408 seconds
