Number of cores/ranks per node is: 8
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 5000431
Cell on rank 1 before loadbalancing: 4999569
Edge-cut: 22016
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	13537	5587	8690	8	1057	0	4549	0	0	0	0	0	0	0	0	
From rank 1 to: 	13527	0	6954	4562	1040	2746	0	81	0	0	0	0	0	0	0	0	
From rank 2 to: 	5602	6931	0	6096	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	8646	4503	6110	0	1985	0	0	5397	0	0	0	0	0	0	0	0	
From rank 4 to: 	8	1036	0	1941	0	12329	6671	6487	0	0	0	0	3031	0	2	0	
From rank 5 to: 	1061	2755	0	0	12299	0	6798	4807	0	0	0	0	977	3253	178	0	
From rank 6 to: 	0	0	0	0	6663	6760	0	7223	0	0	0	0	209	22	7796	0	
From rank 7 to: 	4591	81	0	5358	6529	4763	7240	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	12582	7419	5226	0	3703	0	896	
From rank 9 to: 	0	0	0	0	0	0	0	0	12584	0	5285	7669	3452	560	0	657	
From rank 10 to: 	0	0	0	0	0	0	0	0	7421	5255	0	6397	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	5165	7678	6409	0	215	0	0	7019	
From rank 12 to: 	0	0	0	0	2995	986	209	0	0	3473	0	214	0	12382	7853	6105	
From rank 13 to: 	0	0	0	0	0	3310	22	0	3664	557	0	0	12356	0	4903	6657	
From rank 14 to: 	0	0	0	0	2	176	7807	0	0	0	0	0	7897	4838	0	7052	
From rank 15 to: 	0	0	0	0	0	0	0	0	884	662	0	6984	6108	6637	7048	0	
loadb
After loadbalancing process 10 has 644872 cells.
After loadbalancing process 2 has 644546 cells.
After loadbalancing process 11 has 652297 cells.
After loadbalancing process 3 has 652552 cells.
After loadbalancing process 8 has 655632 cells.
After loadbalancing process 14 has 653572 cells.
After loadbalancing process 0 has 659344 cells.
After loadbalancing process 1 has 647944 cells.
After loadbalancing process 15 has 654136 cells.
After loadbalancing process 7 has 654466 cells.
After loadbalancing process 9 has 656014 cells.
After loadbalancing process 13 has 657269 cells.
After loadbalancing process 4 has 657428 cells.
After loadbalancing process 6 has 654590 cells.
After loadbalancing process 5 has 658037 cells.
After loadbalancing process 12 has 653150 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	13537	5587	8690	8	1057	0	4549	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	13527	0	6954	4562	1040	2746	0	81	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	5602	6931	0	6096	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	8646	4503	6110	0	1985	0	0	5397	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	8	1036	0	1941	0	12329	6671	6487	0	0	0	0	3031	0	2	0	
Rank 5's ghost cells:	1061	2755	0	0	12299	0	6798	4807	0	0	0	0	977	3253	178	0	
Rank 6's ghost cells:	0	0	0	0	6663	6760	0	7223	0	0	0	0	209	22	7796	0	
Rank 7's ghost cells:	4591	81	0	5358	6529	4763	7240	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	12582	7419	5226	0	3703	0	896	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	12584	0	5285	7669	3452	560	0	657	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	7421	5255	0	6397	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	5165	7678	6409	0	215	0	0	7019	
Rank 12's ghost cells:	0	0	0	0	2995	986	209	0	0	3473	0	214	0	12382	7853	6105	
Rank 13's ghost cells:	0	0	0	0	0	3310	22	0	3664	557	0	0	12356	0	4903	6657	
Rank 14's ghost cells:	0	0	0	0	2	176	7807	0	0	0	0	0	7897	4838	0	7052	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	884	662	0	6984	6108	6637	7048	0	
=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          139.611         0.213003
    2          80.2587         0.574876
    3          56.5219         0.704247
    4          45.7641          0.80967
    5          41.9741         0.917184
    6          38.5833         0.919217
    7            31.31          0.81149
    8          29.0075         0.926461
    9          27.6463         0.953074
   10          22.8756         0.827437
   11            21.43         0.936806
   12          21.5574          1.00595
   13          18.3403         0.850765
   14          17.1239         0.933679
   15           17.571          1.02611
   16          15.3621         0.874286
   17          14.3354         0.933168
   18          14.6035           1.0187
   19          13.0677          0.89483
   20          12.3619         0.945987
   21          12.4616          1.00807
   22          11.2018          0.89891
   23          10.8067         0.964726
   24          11.0165          1.01941
   25          9.94401         0.902648
   26          9.74842         0.980331
   27          9.96349          1.02206
   28          9.00785         0.904085
   29           9.1631          1.01724
   30           9.2605          1.01063
   31          9.00338         0.972235
   32          10.3486          1.14941
   33           11.641          1.12489
   34          12.1334           1.0423
   35          11.4728         0.945553
   36          8.37001         0.729554
   37          6.18157         0.738538
   38          5.81552         0.940784
   39          5.35188         0.920275
   40          4.50539         0.841834
   41          3.95553         0.877954
   42          3.59315         0.908387
   43          3.14366         0.874904
   44          2.66665         0.848263
   45          2.39432         0.897874
   46          2.11304         0.882522
   47          1.78441         0.844478
   48          1.62233         0.909168
   49          1.44647         0.891601
   50          1.31152         0.906702
   51          1.23201         0.939375
   52          1.09687         0.890312
   53          1.06525          0.97117
   54         0.968701         0.909367
   55          0.90727         0.936584
   56         0.903971         0.996363
   57         0.810669         0.896787
   58          0.83725          1.03279
   59         0.781168         0.933017
   60         0.758668         0.971198
   61         0.765022          1.00837
   62         0.720947         0.942387
   63         0.713129         0.989156
   64         0.676427         0.948534
   65         0.640541         0.946947
   66         0.564053         0.880589
   67           0.5163          0.91534
   68          0.41468         0.803175
   69         0.352621         0.850345
   70         0.286671         0.812974
   71         0.244377         0.852463
   72           0.2298         0.940353
   73         0.238256          1.03679
   74         0.248533          1.04313
   75         0.261711          1.05303
   76          0.24429         0.933432
   77         0.221768         0.907807
   78         0.212676         0.959005
   79         0.197171         0.927092
   80          0.17074         0.865951
   81         0.153649         0.899899
   82         0.139673         0.909038
   83         0.132322         0.947372
   84         0.123872         0.936138
   85         0.118521         0.956805
   86         0.115089         0.971041
   87         0.111023         0.964672
   88         0.101941         0.918199
   89         0.101138         0.992118
   90        0.0950232         0.939544
   91        0.0883541         0.929816
   92        0.0848316         0.960133
   93        0.0791021          0.93246
   94        0.0701594         0.886947
   95        0.0662141         0.943767
   96        0.0591647         0.893536
=== rate=0.907549, T=141.426, TIT=1.47319, IT=96

 Elapsed time: 141.426
Rank 0: Matrix-vector product took 0.539543 seconds
Rank 1: Matrix-vector product took 0.531278 seconds
Rank 2: Matrix-vector product took 0.528461 seconds
Rank 3: Matrix-vector product took 0.534167 seconds
Rank 4: Matrix-vector product took 0.5391 seconds
Rank 5: Matrix-vector product took 0.537825 seconds
Rank 6: Matrix-vector product took 0.536519 seconds
Rank 7: Matrix-vector product took 0.53734 seconds
Rank 8: Matrix-vector product took 0.537974 seconds
Rank 9: Matrix-vector product took 0.539075 seconds
Rank 10: Matrix-vector product took 0.529464 seconds
Rank 11: Matrix-vector product took 0.534415 seconds
Rank 12: Matrix-vector product took 0.535957 seconds
Rank 13: Matrix-vector product took 0.538166 seconds
Rank 14: Matrix-vector product took 0.53632 seconds
Rank 15: Matrix-vector product took 0.537019 seconds
Average time for Matrix-vector product is 0.535789 seconds

Rank 0: copyOwnerToAll took 0.00258658 seconds
Rank 1: copyOwnerToAll took 0.00258658 seconds
Rank 2: copyOwnerToAll took 0.00258658 seconds
Rank 3: copyOwnerToAll took 0.00258658 seconds
Rank 4: copyOwnerToAll took 0.00258658 seconds
Rank 5: copyOwnerToAll took 0.00258658 seconds
Rank 6: copyOwnerToAll took 0.00258658 seconds
Rank 7: copyOwnerToAll took 0.00258658 seconds
Rank 8: copyOwnerToAll took 0.00258658 seconds
Rank 9: copyOwnerToAll took 0.00258658 seconds
Rank 10: copyOwnerToAll took 0.00258658 seconds
Rank 11: copyOwnerToAll took 0.00258658 seconds
Rank 12: copyOwnerToAll took 0.00258658 seconds
Rank 13: copyOwnerToAll took 0.00258658 seconds
Rank 14: copyOwnerToAll took 0.00258658 seconds
Rank 15: copyOwnerToAll took 0.00258658 seconds
Average time for copyOwnertoAll is 0.00261665 seconds
