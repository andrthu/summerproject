Number of cores/ranks per node is: 21
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 333331
Cell on rank 1 before loadbalancing: 333344
Cell on rank 2 before loadbalancing: 333325
Edge-cut: 19019
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
From rank 0 to: 	0	276	0	421	574	0	0	0	485	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	658	204	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	276	0	462	253	272	664	0	23	231	0	0	0	0	0	0	0	0	0	0	0	0	0	492	37	0	235	312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	465	0	149	257	54	0	908	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	431	252	150	0	1195	20	0	396	79	874	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	567	274	260	1195	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	675	71	20	0	0	374	647	286	0	55	0	0	0	0	0	0	0	4	538	0	0	114	196	0	0	64	718	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	365	0	1123	0	184	207	0	0	0	0	0	0	0	0	360	0	828	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	25	908	389	0	655	1115	0	54	125	45	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	488	227	0	72	0	285	0	50	0	674	527	0	0	0	0	0	0	0	511	16	0	0	0	0	0	0	323	0	815	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	117	0	0	863	0	0	181	129	673	0	1035	0	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	59	211	45	571	1042	0	0	0	0	0	0	664	0	689	102	0	158	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	0	0	977	120	0	518	145	0	0	0	947	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	184	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	976	0	195	0	593	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	316	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	120	195	0	868	235	526	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	870	0	354	0	670	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	118	0	0	695	0	0	95	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	529	564	243	354	0	14	420	0	0	281	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	535	451	0	0	0	0	0	5	32	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	664	132	0	526	0	16	0	723	576	18	92	325	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	131	670	414	759	0	862	136	222	39	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	521	370	0	
From rank 18 to: 	0	0	0	0	0	4	0	0	512	0	662	0	0	0	0	0	575	866	0	246	0	0	0	0	0	0	0	0	18	538	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	237	314	0	
From rank 19 to: 	0	0	0	0	0	538	335	0	11	0	98	0	0	0	0	0	9	134	246	0	533	646	0	0	0	0	0	278	1	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	218	423	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	929	0	0	0	280	92	213	0	529	0	787	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	142	0	0	0	0	0	0	0	87	234	
From rank 21 to: 	0	0	0	0	0	0	828	0	0	55	155	0	0	0	0	0	331	32	0	646	800	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	492	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1106	101	185	99	97	0	0	0	0	0	0	0	0	0	0	0	43	489	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	37	0	0	0	196	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1123	0	164	0	335	558	51	0	0	781	0	0	0	0	0	0	0	567	51	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	101	163	0	723	679	0	13	0	299	147	0	0	0	0	0	0	1036	15	352	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 25 to: 	658	244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	189	0	724	0	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 26 to: 	208	305	0	0	0	62	0	0	323	0	0	0	0	0	0	0	0	0	0	0	0	0	112	345	700	1088	0	15	1018	0	28	41	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	731	0	0	0	0	0	0	0	0	0	0	0	0	0	278	0	0	97	551	0	0	12	0	203	175	0	326	478	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	88	555	
From rank 28 to: 	0	0	0	0	0	40	0	0	816	0	0	0	0	0	0	0	0	0	18	1	0	0	0	48	20	0	1018	201	0	1097	470	330	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	105	0	0	0	0	0	0	0	0	0	543	6	0	0	0	0	0	0	0	182	1097	0	589	161	254	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	127	0	542	550	1	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	298	0	28	0	459	579	0	250	93	0	0	892	0	0	216	0	0	0	0	0	0	0	0	0	156	0	0	0	0	0	0	0	0	0	0	501	0	27	0	0	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	789	146	0	32	330	330	145	263	0	977	479	0	133	0	0	105	431	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 32 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	494	0	250	93	978	0	843	0	34	0	0	0	0	0	0	0	0	0	0	121	0	57	0	0	0	0	0	0	0	0	0	0	248	511	0	3	166	
From rank 33 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	478	835	0	1044	204	60	190	0	675	0	0	0	0	0	0	372	0	231	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 34 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1044	0	0	279	275	0	164	0	397	0	0	0	596	84	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 35 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	899	141	42	204	0	0	664	445	437	12	0	0	0	129	0	0	0	0	675	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 36 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	281	669	0	971	0	0	0	0	0	0	766	158	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 37 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	202	286	465	966	0	111	149	0	349	0	947	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 38 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1038	0	0	0	0	0	231	108	0	0	0	443	0	112	0	217	349	56	289	674	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 39 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	68	566	15	0	0	0	0	0	0	422	0	675	149	16	0	152	217	0	911	670	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 40 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	489	45	356	0	0	0	0	0	0	0	0	0	0	0	0	0	356	912	0	590	329	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 41 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	383	0	0	357	56	667	581	0	338	291	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 42 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	289	0	325	347	0	1110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 43 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	129	0	947	706	0	48	291	1111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 44 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	770	0	0	0	0	0	0	0	0	333	0	398	577	0	0	0	158	529	0	0	0	0	0	0	0	0	0	0	
From rank 45 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	601	0	154	0	0	0	0	0	0	0	335	0	890	47	359	0	0	310	0	520	0	0	0	0	0	0	0	0	0	0	
From rank 46 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	372	84	0	0	0	0	0	0	0	0	0	0	896	0	161	429	577	0	194	0	148	0	0	0	0	0	46	946	0	0	0	
From rank 47 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	415	48	161	0	880	69	940	0	270	295	0	0	24	318	0	534	31	0	0	0	
From rank 48 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	151	0	55	232	0	673	61	0	0	0	0	0	0	0	577	347	431	880	0	0	0	0	0	0	0	0	0	0	0	834	1	0	0	0	
From rank 49 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	561	69	0	0	245	915	26	307	0	0	1086	0	0	0	119	0	0	0	
From rank 50 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	941	0	252	0	0	723	186	0	0	172	893	0	0	0	0	0	0	
From rank 51 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	310	170	0	0	915	0	0	163	446	0	0	0	0	0	0	0	0	0	0	
From rank 52 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	158	0	0	270	0	28	732	162	0	967	0	0	0	0	0	0	0	0	0	0	
From rank 53 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	553	526	155	299	0	325	186	459	998	0	0	0	0	0	0	0	0	0	0	0	
From rank 54 to: 	0	0	0	0	0	0	0	0	0	0	0	184	316	0	2	531	0	1	0	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1154	192	0	319	0	45	27	397	562	
From rank 55 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	118	451	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1173	0	727	206	592	0	0	0	0	0	
From rank 56 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	1089	175	0	0	0	192	718	0	316	89	0	505	0	0	165	
From rank 57 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	318	0	0	889	0	0	0	0	215	335	0	1210	502	182	24	0	0	
From rank 58 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	695	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	579	96	1233	0	23	119	818	168	0	
From rank 59 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	501	0	248	0	0	0	0	0	0	0	0	0	0	0	0	0	51	500	829	0	0	0	0	0	0	0	0	502	22	0	451	858	325	12	
From rank 60 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	511	0	0	0	0	0	0	0	0	0	0	0	0	0	947	28	1	108	0	0	0	0	44	0	505	180	137	451	0	34	439	769	
From rank 61 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	5	0	521	239	0	0	0	0	0	0	0	0	0	0	542	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	12	817	860	40	0	532	0	
From rank 62 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	370	328	220	87	0	0	0	0	0	0	88	0	568	0	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	413	0	0	0	164	325	424	541	0	440	
From rank 63 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	408	234	0	0	0	0	0	0	555	0	1	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	546	0	169	0	0	12	790	0	432	0	
loadb
After loadbalancing process 53 has 20172 cells.
After loadbalancing process 52 has 18979 cells.
After loadbalancing process 2 has 16975 cells.
After loadbalancing process 45 has 19885 cells.
After loadbalancing process 3 has 18544 cells.
After loadbalancing process 7 has 18479 cells.
After loadbalancing process 50 has 19830 cells.
After loadbalancing process 47 has 20642 cells.
After loadbalancing process 44 has 19417 cells.
After loadbalancing process 6 has 18196 cells.
After loadbalancing process 4 has 17450 cells.
After loadbalancing process 13 has 17220 cells.
After loadbalancing process 10 has 18691 cells.
After loadbalancing process 23 has 19013 cells.
After loadbalancing process 46 has 20637 cells.
After loadbalancing process 12 has 17230 cells.
After loadbalancing process 39 has 19017 cells.
After loadbalancing process 51 has 18679 cells.
After loadbalancing process 41 has 17829 cells.
After loadbalancing process 11 has 18041 cells.
After loadbalancing process 49 has 20000 cells.
After loadbalancing process 24 has 18680 cells.
After loadbalancing process 21 has 18008 cells.
After loadbalancing process 37 has 18627 cells.
After loadbalancing process 42 has 17223 cells.
After loadbalancing process 16 has 18232 cells.
After loadbalancing process 31 has 19298 cells.
After loadbalancing process 8 has 19240 cells.
After loadbalancing process 34 has 17988 cells.
After loadbalancing process 28 has 19207 cells.
After loadbalancing process 0 has 17882 cells.
After loadbalancing process 57 has 20337 cells.
After loadbalancing process 26 has 19397 cells.
After loadbalancing process 63 has 19956 cells.
After loadbalancing process 54 has 20540 cells.
After loadbalancing process 56 has 19938 cells.
After loadbalancing process 30 has 18648 cells.
After loadbalancing process 60 has 20824 cells.
After loadbalancing process 29 has 19317 cells.
After loadbalancing process 19 has 18645 cells.
After loadbalancing process 59 has 21094 cells.
After loadbalancing process 9 has 18211 cells.
After loadbalancing process 5 has 18955 cells.
After loadbalancing process 27 has 18653 cells.
After loadbalancing process 43 has 18381 cells.
After loadbalancing process 48 has 20912 cells.
After loadbalancing process 33 has 19237 cells.
After loadbalancing process 55 has 19928 cells.
After loadbalancing process 1 has 18405 cells.
After loadbalancing process 14 has 17955 cells.
After loadbalancing process 22 has 17883 cells.After loadbalancing process 58 has 20720 cells.
After loadbalancing process 61 has 20395 cells.

After loadbalancing process 25 has 18051 cells.
After loadbalancing process 40 has 18277 cells.
After loadbalancing process 15 has 18583 cells.
After loadbalancing process 38 has 18673 cells.
After loadbalancing process 32 has 18956 cells.
After loadbalancing process 18 has 19116 cells.
After loadbalancing process 20 has 18450 cells.
After loadbalancing process 17 has 19278 cells.
After loadbalancing process 35 has 18804 cells.
After loadbalancing process 36 has 18117 cells.
After loadbalancing process 62 has 20669 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	Rank 32	Rank 33	Rank 34	Rank 35	Rank 36	Rank 37	Rank 38	Rank 39	Rank 40	Rank 41	Rank 42	Rank 43	Rank 44	Rank 45	Rank 46	Rank 47	Rank 48	Rank 49	Rank 50	Rank 51	Rank 52	Rank 53	Rank 54	Rank 55	Rank 56	Rank 57	Rank 58	Rank 59	Rank 60	Rank 61	Rank 62	Rank 63	
Rank 0's ghost cells:	0	276	0	421	574	0	0	0	485	116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	658	204	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	276	0	462	253	272	664	0	23	231	0	0	0	0	0	0	0	0	0	0	0	0	0	492	37	0	235	312	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	465	0	149	257	54	0	908	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	431	252	150	0	1195	20	0	396	79	874	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	567	274	260	1195	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	675	71	20	0	0	374	647	286	0	55	0	0	0	0	0	0	0	4	538	0	0	114	196	0	0	64	718	40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	365	0	1123	0	184	207	0	0	0	0	0	0	0	0	360	0	828	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	25	908	389	0	655	1115	0	54	125	45	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	488	227	0	72	0	285	0	50	0	674	527	0	0	0	0	0	0	0	511	16	0	0	0	0	0	0	323	0	815	107	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	117	0	0	863	0	0	181	129	673	0	1035	0	0	0	0	0	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	59	211	45	571	1042	0	0	0	0	0	0	664	0	689	102	0	158	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	977	120	0	518	145	0	0	0	947	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	184	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	976	0	195	0	593	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	316	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	120	195	0	868	235	526	125	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	870	0	354	0	670	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	118	0	0	695	0	0	95	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	529	564	243	354	0	14	420	0	0	281	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	535	451	0	0	0	0	0	5	32	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	664	132	0	526	0	16	0	723	576	18	92	325	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	131	670	414	759	0	862	136	222	39	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	521	370	0	
Rank 18's ghost cells:	0	0	0	0	0	4	0	0	512	0	662	0	0	0	0	0	575	866	0	246	0	0	0	0	0	0	0	0	18	538	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	237	314	0	
Rank 19's ghost cells:	0	0	0	0	0	538	335	0	11	0	98	0	0	0	0	0	9	134	246	0	533	646	0	0	0	0	0	278	1	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	218	423	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	929	0	0	0	280	92	213	0	529	0	787	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	142	0	0	0	0	0	0	0	87	234	
Rank 21's ghost cells:	0	0	0	0	0	0	828	0	0	55	155	0	0	0	0	0	331	32	0	646	800	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	492	0	0	0	115	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1106	101	185	99	97	0	0	0	0	0	0	0	0	0	0	0	43	489	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	37	0	0	0	196	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1123	0	164	0	335	558	51	0	0	781	0	0	0	0	0	0	0	567	51	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	101	163	0	723	679	0	13	0	299	147	0	0	0	0	0	0	1036	15	352	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 25's ghost cells:	658	244	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	189	0	724	0	1088	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 26's ghost cells:	208	305	0	0	0	62	0	0	323	0	0	0	0	0	0	0	0	0	0	0	0	0	112	345	700	1088	0	15	1018	0	28	41	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	731	0	0	0	0	0	0	0	0	0	0	0	0	0	278	0	0	97	551	0	0	12	0	203	175	0	326	478	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	88	555	
Rank 28's ghost cells:	0	0	0	0	0	40	0	0	816	0	0	0	0	0	0	0	0	0	18	1	0	0	0	48	20	0	1018	201	0	1097	470	330	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	105	0	0	0	0	0	0	0	0	0	543	6	0	0	0	0	0	0	0	182	1097	0	589	161	254	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	127	0	542	550	1	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	298	0	28	0	459	579	0	250	93	0	0	892	0	0	216	0	0	0	0	0	0	0	0	0	156	0	0	0	0	0	0	0	0	0	0	501	0	27	0	0	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	789	146	0	32	330	330	145	263	0	977	479	0	133	0	0	105	431	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 32's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	494	0	250	93	978	0	843	0	34	0	0	0	0	0	0	0	0	0	0	121	0	57	0	0	0	0	0	0	0	0	0	0	248	511	0	3	166	
Rank 33's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	478	835	0	1044	204	60	190	0	675	0	0	0	0	0	0	372	0	231	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 34's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1044	0	0	279	275	0	164	0	397	0	0	0	596	84	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 35's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	899	141	42	204	0	0	664	445	437	12	0	0	0	129	0	0	0	0	675	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 36's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	62	281	669	0	971	0	0	0	0	0	0	766	158	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 37's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	202	286	465	966	0	111	149	0	349	0	947	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 38's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1038	0	0	0	0	0	231	108	0	0	0	443	0	112	0	217	349	56	289	674	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 39's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	68	566	15	0	0	0	0	0	0	422	0	675	149	16	0	152	217	0	911	670	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 40's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	489	45	356	0	0	0	0	0	0	0	0	0	0	0	0	0	356	912	0	590	329	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 41's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	383	0	0	357	56	667	581	0	338	291	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 42's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	289	0	325	347	0	1110	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 43's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	129	0	947	706	0	48	291	1111	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 44's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	770	0	0	0	0	0	0	0	0	333	0	398	577	0	0	0	158	529	0	0	0	0	0	0	0	0	0	0	
Rank 45's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	601	0	154	0	0	0	0	0	0	0	335	0	890	47	359	0	0	310	0	520	0	0	0	0	0	0	0	0	0	0	
Rank 46's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	121	372	84	0	0	0	0	0	0	0	0	0	0	896	0	161	429	577	0	194	0	148	0	0	0	0	0	46	946	0	0	0	
Rank 47's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	415	48	161	0	880	69	940	0	270	295	0	0	24	318	0	534	31	0	0	0	
Rank 48's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	151	0	55	232	0	673	61	0	0	0	0	0	0	0	577	347	431	880	0	0	0	0	0	0	0	0	0	0	0	834	1	0	0	0	
Rank 49's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	561	69	0	0	245	915	26	307	0	0	1086	0	0	0	119	0	0	0	
Rank 50's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	941	0	252	0	0	723	186	0	0	172	893	0	0	0	0	0	0	
Rank 51's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	310	170	0	0	915	0	0	163	446	0	0	0	0	0	0	0	0	0	0	
Rank 52's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	158	0	0	270	0	28	732	162	0	967	0	0	0	0	0	0	0	0	0	0	
Rank 53's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	553	526	155	299	0	325	186	459	998	0	0	0	0	0	0	0	0	0	0	0	
Rank 54's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	184	316	0	2	531	0	1	0	0	136	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1154	192	0	319	0	45	27	397	562	
Rank 55's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	118	451	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1173	0	727	206	592	0	0	0	0	0	
Rank 56's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	21	0	1089	175	0	0	0	192	718	0	316	89	0	505	0	0	165	
Rank 57's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	318	0	0	889	0	0	0	0	215	335	0	1210	502	182	24	0	0	
Rank 58's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	695	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	322	579	96	1233	0	23	119	818	168	0	
Rank 59's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	126	501	0	248	0	0	0	0	0	0	0	0	0	0	0	0	0	51	500	829	0	0	0	0	0	0	0	0	502	22	0	451	858	325	12	
Rank 60's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	511	0	0	0	0	0	0	0	0	0	0	0	0	0	947	28	1	108	0	0	0	0	44	0	505	180	137	451	0	34	439	769	
Rank 61's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	108	5	0	521	239	0	0	0	0	0	0	0	0	0	0	542	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	12	817	860	40	0	532	0	
Rank 62's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	32	0	370	328	220	87	0	0	0	0	0	0	88	0	568	0	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	413	0	0	0	164	325	424	541	0	440	
Rank 63's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	408	234	0	0	0	0	0	0	555	0	1	0	0	143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	546	0	169	0	0	12	790	0	432	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          57.5241         0.230281
    2          33.8766          0.58891
    3          23.9954         0.708317
    4          20.6843         0.862014
    5          17.9988         0.870165
    6          14.5193         0.806684
    7          12.9141         0.889441
    8          12.2952         0.952072
    9          10.3147         0.838926
   10           9.3577         0.907218
   11          9.25288         0.988799
   12          8.08182         0.873438
   13           7.2635         0.898747
   14          7.30339          1.00549
   15           6.5995         0.903621
   16          5.97034         0.904666
   17          5.91382         0.990533
   18           5.5168         0.932865
   19          5.05124          0.91561
   20          5.00762         0.991364
   21          4.61113         0.920824
   22          4.33605         0.940344
   23            4.272         0.985228
   24          3.93517         0.921155
   25          3.73345         0.948739
   26          3.70997          0.99371
   27          3.45048         0.930058
   28          3.25983         0.944745
   29          3.23537         0.992498
   30          3.02491         0.934951
   31          2.92707         0.967654
   32          2.92832          1.00043
   33          2.87882         0.983095
   34          3.03102          1.05287
   35          3.21926           1.0621
   36          3.16443         0.982968
   37           2.8601         0.903828
   38           2.2961         0.802806
   39          1.70501         0.742567
   40          1.38169          0.81037
   41          1.23494         0.893793
   42          1.13549         0.919467
   43          1.00602         0.885975
   44         0.841138         0.836107
   45         0.720489         0.856565
   46         0.643115         0.892609
   47         0.545675         0.848488
   48         0.459192         0.841512
   49          0.39037         0.850123
   50         0.322835         0.826999
   51         0.274714         0.850942
   52         0.236073         0.859342
   53         0.199619         0.845582
   54         0.171546         0.859364
   55         0.145514         0.848253
   56         0.120035         0.824903
   57          0.10111         0.842335
   58        0.0838708         0.829502
   59        0.0684054         0.815604
   60        0.0569014         0.831825
   61        0.0462391         0.812618
   62        0.0372769         0.806177
   63        0.0299698         0.803978
   64        0.0235114         0.784503
=== rate=0.865145, T=3.24743, TIT=0.0507411, IT=64

 Elapsed time: 3.24743
Rank 0: Matrix-vector product took 0.0145454 seconds
Rank 1: Matrix-vector product took 0.014874 seconds
Rank 2: Matrix-vector product took 0.0135317 seconds
Rank 3: Matrix-vector product took 0.0147095 seconds
Rank 4: Matrix-vector product took 0.0138241 seconds
Rank 5: Matrix-vector product took 0.0151392 seconds
Rank 6: Matrix-vector product took 0.0146576 seconds
Rank 7: Matrix-vector product took 0.0147066 seconds
Rank 8: Matrix-vector product took 0.0154034 seconds
Rank 9: Matrix-vector product took 0.0147011 seconds
Rank 10: Matrix-vector product took 0.0150259 seconds
Rank 11: Matrix-vector product took 0.0144255 seconds
Rank 12: Matrix-vector product took 0.0139284 seconds
Rank 13: Matrix-vector product took 0.0138586 seconds
Rank 14: Matrix-vector product took 0.0144727 seconds
Rank 15: Matrix-vector product took 0.0148372 seconds
Rank 16: Matrix-vector product took 0.0147719 seconds
Rank 17: Matrix-vector product took 0.0155421 seconds
Rank 18: Matrix-vector product took 0.0152689 seconds
Rank 19: Matrix-vector product took 0.0148885 seconds
Rank 20: Matrix-vector product took 0.0147609 seconds
Rank 21: Matrix-vector product took 0.014465 seconds
Rank 22: Matrix-vector product took 0.0183307 seconds
Rank 23: Matrix-vector product took 0.0151373 seconds
Rank 24: Matrix-vector product took 0.015053 seconds
Rank 25: Matrix-vector product took 0.0144827 seconds
Rank 26: Matrix-vector product took 0.0156162 seconds
Rank 27: Matrix-vector product took 0.0148806 seconds
Rank 28: Matrix-vector product took 0.0153766 seconds
Rank 29: Matrix-vector product took 0.0154043 seconds
Rank 30: Matrix-vector product took 0.0150282 seconds
Rank 31: Matrix-vector product took 0.0155819 seconds
Rank 32: Matrix-vector product took 0.0152242 seconds
Rank 33: Matrix-vector product took 0.0156346 seconds
Rank 34: Matrix-vector product took 0.0145408 seconds
Rank 35: Matrix-vector product took 0.0154424 seconds
Rank 36: Matrix-vector product took 0.014568 seconds
Rank 37: Matrix-vector product took 0.0147996 seconds
Rank 38: Matrix-vector product took 0.0151681 seconds
Rank 39: Matrix-vector product took 0.0152594 seconds
Rank 40: Matrix-vector product took 0.0145635 seconds
Rank 41: Matrix-vector product took 0.0203285 seconds
Rank 42: Matrix-vector product took 0.0138889 seconds
Rank 43: Matrix-vector product took 0.0148324 seconds
Rank 44: Matrix-vector product took 0.0155219 seconds
Rank 45: Matrix-vector product took 0.0159671 seconds
Rank 46: Matrix-vector product took 0.0165466 seconds
Rank 47: Matrix-vector product took 0.0165293 seconds
Rank 48: Matrix-vector product took 0.0169868 seconds
Rank 49: Matrix-vector product took 0.0161675 seconds
Rank 50: Matrix-vector product took 0.0160435 seconds
Rank 51: Matrix-vector product took 0.0149727 seconds
Rank 52: Matrix-vector product took 0.0152949 seconds
Rank 53: Matrix-vector product took 0.0160381 seconds
Rank 54: Matrix-vector product took 0.016354 seconds
Rank 55: Matrix-vector product took 0.0159481 seconds
Rank 56: Matrix-vector product took 0.0201832 seconds
Rank 57: Matrix-vector product took 0.0164712 seconds
Rank 58: Matrix-vector product took 0.016625 seconds
Rank 59: Matrix-vector product took 0.0167091 seconds
Rank 60: Matrix-vector product took 0.0165952 seconds
Rank 61: Matrix-vector product took 0.0163436 seconds
Rank 62: Matrix-vector product took 0.0166961 seconds
Rank 63: Matrix-vector product took 0.0160263 seconds
Average time for Matrix-vector product is 0.0154609 seconds

Rank 0: copyOwnerToAll took 0.00062186 seconds
Rank 1: copyOwnerToAll took 0.00062186 seconds
Rank 2: copyOwnerToAll took 0.00062186 seconds
Rank 3: copyOwnerToAll took 0.00062186 seconds
Rank 4: copyOwnerToAll took 0.00062186 seconds
Rank 5: copyOwnerToAll took 0.00062186 seconds
Rank 6: copyOwnerToAll took 0.00062186 seconds
Rank 7: copyOwnerToAll took 0.00062186 seconds
Rank 8: copyOwnerToAll took 0.00062186 seconds
Rank 9: copyOwnerToAll took 0.00062186 seconds
Rank 10: copyOwnerToAll took 0.00062186 seconds
Rank 11: copyOwnerToAll took 0.00062186 seconds
Rank 12: copyOwnerToAll took 0.00062186 seconds
Rank 13: copyOwnerToAll took 0.00062186 seconds
Rank 14: copyOwnerToAll took 0.00062186 seconds
Rank 15: copyOwnerToAll took 0.00062186 seconds
Rank 16: copyOwnerToAll took 0.00062186 seconds
Rank 17: copyOwnerToAll took 0.00062186 seconds
Rank 18: copyOwnerToAll took 0.00062186 seconds
Rank 19: copyOwnerToAll took 0.00062186 seconds
Rank 20: copyOwnerToAll took 0.00062186 seconds
Rank 21: copyOwnerToAll took 0.00062186 seconds
Rank 22: copyOwnerToAll took 0.00062186 seconds
Rank 23: copyOwnerToAll took 0.00062186 seconds
Rank 24: copyOwnerToAll took 0.00062186 seconds
Rank 25: copyOwnerToAll took 0.00062186 seconds
Rank 26: copyOwnerToAll took 0.00062186 seconds
Rank 27: copyOwnerToAll took 0.00062186 seconds
Rank 28: copyOwnerToAll took 0.00062186 seconds
Rank 29: copyOwnerToAll took 0.00062186 seconds
Rank 30: copyOwnerToAll took 0.00062186 seconds
Rank 31: copyOwnerToAll took 0.00062186 seconds
Rank 32: copyOwnerToAll took 0.00062186 seconds
Rank 33: copyOwnerToAll took 0.00062186 seconds
Rank 34: copyOwnerToAll took 0.00062186 seconds
Rank 35: copyOwnerToAll took 0.00062186 seconds
Rank 36: copyOwnerToAll took 0.00062186 seconds
Rank 37: copyOwnerToAll took 0.00062186 seconds
Rank 38: copyOwnerToAll took 0.00062186 seconds
Rank 39: copyOwnerToAll took 0.00062186 seconds
Rank 40: copyOwnerToAll took 0.00062186 seconds
Rank 41: copyOwnerToAll took 0.00062186 seconds
Rank 42: copyOwnerToAll took 0.00062186 seconds
Rank 43: copyOwnerToAll took 0.00062186 seconds
Rank 44: copyOwnerToAll took 0.00062186 seconds
Rank 45: copyOwnerToAll took 0.00062186 seconds
Rank 46: copyOwnerToAll took 0.00062186 seconds
Rank 47: copyOwnerToAll took 0.00062186 seconds
Rank 48: copyOwnerToAll took 0.00062186 seconds
Rank 49: copyOwnerToAll took 0.00062186 seconds
Rank 50: copyOwnerToAll took 0.00062186 seconds
Rank 51: copyOwnerToAll took 0.00062186 seconds
Rank 52: copyOwnerToAll took 0.00062186 seconds
Rank 53: copyOwnerToAll took 0.00062186 seconds
Rank 54: copyOwnerToAll took 0.00062186 seconds
Rank 55: copyOwnerToAll took 0.00062186 seconds
Rank 56: copyOwnerToAll took 0.00062186 seconds
Rank 57: copyOwnerToAll took 0.00062186 seconds
Rank 58: copyOwnerToAll took 0.00062186 seconds
Rank 59: copyOwnerToAll took 0.00062186 seconds
Rank 60: copyOwnerToAll took 0.00062186 seconds
Rank 61: copyOwnerToAll took 0.00062186 seconds
Rank 62: copyOwnerToAll took 0.00062186 seconds
Rank 63: copyOwnerToAll took 0.00062186 seconds
Average time for copyOwnertoAll is 0.000659657 seconds
