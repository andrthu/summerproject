Number of cores/ranks per node is: 10
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 33380
Cell on rank 1 before loadbalancing: 33313
Cell on rank 2 before loadbalancing: 33307
Edge-cut: 2027
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	170	180	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	170	0	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	180	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	180	0	0	80	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	50	90	0	240	80	30	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	30	100	200	250	0	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	80	0	0	160	110	0	90	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	180	0	0	0	0	0	
From rank 6 to: 	0	0	0	30	180	160	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	110	200	0	110	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	110	0	170	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	90	0	170	180	0	230	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	40	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	80	232	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	130	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	0	0	125	80	100	0	140	0	0	0	80	0	0	0	50	0	0	0	100	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	125	0	0	248	30	0	0	0	0	0	0	0	0	0	0	0	170	20	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	80	0	0	120	150	0	0	0	120	150	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	100	258	130	0	172	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	30	150	162	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	170	90	0	100	0	0	0	200	0	50	0	0	0	0	0	
From rank 17 to: 	0	80	0	70	0	0	0	0	0	0	0	0	0	0	0	0	180	0	230	0	80	150	0	0	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	120	0	130	0	0	0	0	0	0	0	0	0	0	90	240	0	0	0	0	0	0	0	0	110	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	190	170	0	0	0	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	90	0	150	0	0	100	80	0	190	0	100	0	0	0	0	0	0	0	0	0	0	
From rank 21 to: 	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	170	100	0	0	0	0	0	0	0	0	0	0	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	60	102	0	0	0	262	0	60	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	220	0	0	210	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	200	0	0	0	0	0	60	0	0	58	140	0	170	78	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	20	120	0	0	0	0	0	0	0	0	0	0	0	102	210	68	0	210	0	0	0	0	0	
From rank 26 to: 	0	0	0	0	0	180	0	0	0	30	0	0	0	0	0	0	40	0	110	0	0	0	0	0	140	210	0	0	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	235	0	120	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	170	0	0	235	0	170	80	0	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	272	0	78	0	0	0	170	0	130	140	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	90	130	0	170	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	140	170	0	
loadb
After loadbalancing process 15 has 3348 cells.
After loadbalancing process 0 has 3430 cells.
After loadbalancing process 23 has 3890 cells.
After loadbalancing process 14 has 3680 cells.
After loadbalancing process 12 has 3622 cells.
After loadbalancing process 8 has 3425 cells.
After loadbalancing process 19 has 3530 cells.
After loadbalancing process 13 has 3660 cells.
After loadbalancing process 2 has 3470 cells.
After loadbalancing process 6 has 3610 cells.
After loadbalancing process 7 has 3615 cells.
After loadbalancing process 4 has 3780 cells.
After loadbalancing process 10 has 3602 cells.
After loadbalancing process 18 has 3700 cells.
After loadbalancing process 28 has 4097 cells.
After loadbalancing process 25 has 4072 cells.
After loadbalancing process 9 has 3750 cells.
After loadbalancing process 27 has 3840 cells.
After loadbalancing process 16 has 3800 cells.
After loadbalancing process 11 has 3723 cells.
After loadbalancing process 1 has 3610 cells.
After loadbalancing process 22 has 4016 cells.
After loadbalancing process 3 has 3700 cells.
After loadbalancing process 26 has 4048 cells.
After loadbalancing process 5 has 3770 cells.
After loadbalancing process 20 has 3750 cells.
After loadbalancing process 24 has 4094 cells.
After loadbalancing process 30 has 3850 cells.
After loadbalancing process 21 has 3610 cells.
After loadbalancing process 17 has 3810 cells.
After loadbalancing process 31 has 3670 cells.
After loadbalancing process 29 has 4140 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	170	180	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	170	0	0	40	90	0	0	0	0	0	0	0	0	0	0	0	0	80	0	0	0	180	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	180	0	0	80	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	50	90	0	240	80	30	0	0	0	0	0	0	0	0	0	0	70	120	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	30	100	200	250	0	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	80	0	0	160	110	0	90	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	180	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	30	180	160	0	200	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	110	200	0	110	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	110	0	170	80	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	90	0	170	180	0	230	0	0	0	0	0	0	0	0	0	0	0	0	0	0	20	40	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	80	232	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	130	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	125	80	100	0	140	0	0	0	80	0	0	0	50	0	0	0	100	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	125	0	0	248	30	0	0	0	0	0	0	0	0	0	0	0	170	20	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	80	0	0	120	150	0	0	0	120	150	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	100	258	130	0	172	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	30	150	162	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	140	0	0	0	0	0	170	90	0	100	0	0	0	200	0	50	0	0	0	0	0	
Rank 17's ghost cells:	0	80	0	70	0	0	0	0	0	0	0	0	0	0	0	0	180	0	230	0	80	150	0	0	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	120	0	130	0	0	0	0	0	0	0	0	0	0	90	240	0	0	0	0	0	0	0	0	110	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	190	170	0	0	0	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	90	0	150	0	0	100	80	0	190	0	100	0	0	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	0	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	170	100	0	0	0	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	60	102	0	0	0	262	0	60	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	120	0	0	0	0	0	0	0	0	0	0	0	220	0	0	210	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	200	0	0	0	0	0	60	0	0	58	140	0	170	78	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	20	120	0	0	0	0	0	0	0	0	0	0	0	102	210	68	0	210	0	0	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	180	0	0	0	30	0	0	0	0	0	0	40	0	110	0	0	0	0	0	140	210	0	0	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	235	0	120	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	100	20	0	0	0	0	0	0	0	0	0	0	0	170	0	0	235	0	170	80	0	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	272	0	78	0	0	0	170	0	130	140	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	90	130	0	170	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0	0	140	170	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          38.2667         0.238794
    2          23.2807         0.608382
    3          21.5429         0.925353
    4           19.409         0.900947
    5          9.54767         0.491919
    6          5.17802         0.542333
    7           3.8782         0.748974
    8          2.46362         0.635249
    9          1.23842         0.502682
   10         0.865798         0.699115
   11         0.618316         0.714157
   12         0.315192         0.509759
   13          0.23444         0.743799
   14         0.156209         0.666306
   15        0.0869058         0.556345
   16        0.0597266         0.687257
   17        0.0382982         0.641224
   18        0.0237047         0.618952
   19        0.0165783         0.699368
   20       0.00935125         0.564065
=== rate=0.614191, T=0.243691, TIT=0.0121845, IT=20

 Elapsed time: 0.243691
Rank 0: Matrix-vector product took 0.00271413 seconds
Rank 1: Matrix-vector product took 0.00281315 seconds
Rank 2: Matrix-vector product took 0.00271715 seconds
Rank 3: Matrix-vector product took 0.0028957 seconds
Rank 4: Matrix-vector product took 0.00295093 seconds
Rank 5: Matrix-vector product took 0.00292694 seconds
Rank 6: Matrix-vector product took 0.00284396 seconds
Rank 7: Matrix-vector product took 0.00284365 seconds
Rank 8: Matrix-vector product took 0.00270917 seconds
Rank 9: Matrix-vector product took 0.00292003 seconds
Rank 10: Matrix-vector product took 0.00286835 seconds
Rank 11: Matrix-vector product took 0.00297283 seconds
Rank 12: Matrix-vector product took 0.00285625 seconds
Rank 13: Matrix-vector product took 0.00287445 seconds
Rank 14: Matrix-vector product took 0.00291547 seconds
Rank 15: Matrix-vector product took 0.0026589 seconds
Rank 16: Matrix-vector product took 0.00302212 seconds
Rank 17: Matrix-vector product took 0.00295569 seconds
Rank 18: Matrix-vector product took 0.00290928 seconds
Rank 19: Matrix-vector product took 0.00275266 seconds
Rank 20: Matrix-vector product took 0.00299751 seconds
Rank 21: Matrix-vector product took 0.0028558 seconds
Rank 22: Matrix-vector product took 0.00316223 seconds
Rank 23: Matrix-vector product took 0.00305259 seconds
Rank 24: Matrix-vector product took 0.0032114 seconds
Rank 25: Matrix-vector product took 0.00321623 seconds
Rank 26: Matrix-vector product took 0.00313906 seconds
Rank 27: Matrix-vector product took 0.00302331 seconds
Rank 28: Matrix-vector product took 0.00317699 seconds
Rank 29: Matrix-vector product took 0.00323044 seconds
Rank 30: Matrix-vector product took 0.00300822 seconds
Rank 31: Matrix-vector product took 0.00284577 seconds
Average time for Matrix-vector product is 0.00293876 seconds

Rank 0: copyOwnerToAll took 0.00022942 seconds
Rank 1: copyOwnerToAll took 0.00022942 seconds
Rank 2: copyOwnerToAll took 0.00022942 seconds
Rank 3: copyOwnerToAll took 0.00022942 seconds
Rank 4: copyOwnerToAll took 0.00022942 seconds
Rank 5: copyOwnerToAll took 0.00022942 seconds
Rank 6: copyOwnerToAll took 0.00022942 seconds
Rank 7: copyOwnerToAll took 0.00022942 seconds
Rank 8: copyOwnerToAll took 0.00022942 seconds
Rank 9: copyOwnerToAll took 0.00022942 seconds
Rank 10: copyOwnerToAll took 0.00022942 seconds
Rank 11: copyOwnerToAll took 0.00022942 seconds
Rank 12: copyOwnerToAll took 0.00022942 seconds
Rank 13: copyOwnerToAll took 0.00022942 seconds
Rank 14: copyOwnerToAll took 0.00022942 seconds
Rank 15: copyOwnerToAll took 0.00022942 seconds
Rank 16: copyOwnerToAll took 0.00022942 seconds
Rank 17: copyOwnerToAll took 0.00022942 seconds
Rank 18: copyOwnerToAll took 0.00022942 seconds
Rank 19: copyOwnerToAll took 0.00022942 seconds
Rank 20: copyOwnerToAll took 0.00022942 seconds
Rank 21: copyOwnerToAll took 0.00022942 seconds
Rank 22: copyOwnerToAll took 0.00022942 seconds
Rank 23: copyOwnerToAll took 0.00022942 seconds
Rank 24: copyOwnerToAll took 0.00022942 seconds
Rank 25: copyOwnerToAll took 0.00022942 seconds
Rank 26: copyOwnerToAll took 0.00022942 seconds
Rank 27: copyOwnerToAll took 0.00022942 seconds
Rank 28: copyOwnerToAll took 0.00022942 seconds
Rank 29: copyOwnerToAll took 0.00022942 seconds
Rank 30: copyOwnerToAll took 0.00022942 seconds
Rank 31: copyOwnerToAll took 0.00022942 seconds
Average time for copyOwnertoAll is 0.000247104 seconds
