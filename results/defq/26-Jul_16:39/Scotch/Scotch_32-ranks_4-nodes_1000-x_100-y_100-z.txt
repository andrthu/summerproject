Number of cores/ranks per node is: 8
Scotch partitioner
Cell on rank 0 before loadbalancing: 311966
Cell on rank 1 before loadbalancing: 313700
Cell on rank 2 before loadbalancing: 310822
Cell on rank 3 before loadbalancing: 311037
Cell on rank 4 before loadbalancing: 311134
Cell on rank 5 before loadbalancing: 314671
Cell on rank 6 before loadbalancing: 311454
Cell on rank 7 before loadbalancing: 314295
Cell on rank 8 before loadbalancing: 314514
Cell on rank 9 before loadbalancing: 314834
Cell on rank 10 before loadbalancing: 313599
Cell on rank 11 before loadbalancing: 313719
Cell on rank 12 before loadbalancing: 312168
Cell on rank 13 before loadbalancing: 309872
Cell on rank 14 before loadbalancing: 309787
Cell on rank 15 before loadbalancing: 310094
Cell on rank 16 before loadbalancing: 313085
Cell on rank 17 before loadbalancing: 312087
Cell on rank 18 before loadbalancing: 309375
Cell on rank 19 before loadbalancing: 310397
Cell on rank 20 before loadbalancing: 309853
Cell on rank 21 before loadbalancing: 309981
Cell on rank 22 before loadbalancing: 310079
Cell on rank 23 before loadbalancing: 312072
Cell on rank 24 before loadbalancing: 311801
Cell on rank 25 before loadbalancing: 314622
Cell on rank 26 before loadbalancing: 314787
Cell on rank 27 before loadbalancing: 314953
Cell on rank 28 before loadbalancing: 315269
Cell on rank 29 before loadbalancing: 315625
Cell on rank 30 before loadbalancing: 314680
Cell on rank 31 before loadbalancing: 313668
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
From rank 0 to: 	0	4448	4523	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4365	1852	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	4448	0	1521	4985	3885	0	904	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	4521	1533	0	4922	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4429	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	4992	4918	0	0	0	5451	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	3885	0	0	0	4189	5838	886	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	4191	0	427	4478	0	2256	0	4379	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	916	0	5452	5838	438	0	5172	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	891	4471	5183	0	0	3964	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	4186	5217	1152	4158	0	274	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	2259	0	3963	4177	0	448	4496	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	5229	437	0	4957	1088	0	4907	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	4379	0	0	1166	4479	4962	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	4157	0	1090	0	0	4819	5212	594	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	4821	0	146	5961	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	264	0	4910	0	5204	153	0	4752	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	600	5961	4747	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4169	445	5617	0	0	0	0	625	0	4414	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4169	0	4934	683	1274	0	0	4443	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	437	4936	0	4982	4717	0	0	0	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5618	700	4982	0	0	0	0	0	5157	0	189	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1250	4716	0	0	4806	0	4655	0	0	0	0	0	0	0	0	
From rank 21 to: 	4365	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4806	0	4533	1835	0	0	0	0	0	0	0	0	
From rank 22 to: 	1851	0	4429	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4525	0	4460	0	0	0	0	0	0	0	0	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4443	0	0	4654	1833	4460	0	0	0	0	0	0	0	0	0	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	615	0	0	5157	0	0	0	0	0	4560	5303	0	0	0	0	0	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4560	0	1272	5296	4365	180	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4412	0	0	199	0	0	0	0	5303	1271	0	4705	0	0	0	0	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5314	4705	0	1033	4838	0	0	
From rank 28 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4366	0	1012	0	5572	707	4658	
From rank 29 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	191	0	4838	5570	0	4905	0	
From rank 30 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	697	4905	0	6011	
From rank 31 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4661	0	6016	0	
loadb
After loadbalancing process 30 has 326293 cells.
After loadbalancing process 27 has 330843 cells.
After loadbalancing process 12 has 328040 cells.
After loadbalancing process 21 has 325520 cells.
After loadbalancing process 17 has 327590 cells.
After loadbalancing process 16 has 328355 cells.
After loadbalancing process 18 has 324447 cells.
After loadbalancing process 14 has 325070 cells.
After loadbalancing process 11 has 328705 cells.
After loadbalancing process 23 has 327462 cells.
After loadbalancing process 2 has 326227 cells.
After loadbalancing process 1 has 329443 cells.
After loadbalancing process 19 has 327043 cells.
After loadbalancing process 6 has 329270 cells.
After loadbalancing process 7 has 328804 cells.
After loadbalancing process 13 has 320800 cells.
After loadbalancing process 10 has 330217 cells.
After loadbalancing process 0 has 327154 cells.
After loadbalancing process 3 has 326398 cells.
After loadbalancing process 15 has 321402 cells.
After loadbalancing process 31 has 324345 cells.
After loadbalancing process 5 has 330402 cells.
After loadbalancing process 4 has 325932 cells.
After loadbalancing process 29 has 331129 cells.
After loadbalancing process 25 has 330295 cells.
After loadbalancing process 28 has 331584 cells.
After loadbalancing process 9 has 330177 cells.
After loadbalancing process 20 has 325280 cells.
After loadbalancing process 8 has 329501 cells.
After loadbalancing process 26 has 330677 cells.
After loadbalancing process 22 has 325344 cells.
After loadbalancing process 24 has 327436 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	Rank 28	Rank 29	Rank 30	Rank 31	
Rank 0's ghost cells:	0	4448	4523	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4365	1852	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	4448	0	1521	4985	3885	0	904	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	4521	1533	0	4922	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4429	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	4992	4918	0	0	0	5451	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	3885	0	0	0	4189	5838	886	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	4191	0	427	4478	0	2256	0	4379	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	916	0	5452	5838	438	0	5172	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	891	4471	5183	0	0	3964	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	4186	5217	1152	4158	0	274	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	2259	0	3963	4177	0	448	4496	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	5229	437	0	4957	1088	0	4907	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	4379	0	0	1166	4479	4962	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	4157	0	1090	0	0	4819	5212	594	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	4821	0	146	5961	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	264	0	4910	0	5204	153	0	4752	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	600	5961	4747	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4169	445	5617	0	0	0	0	625	0	4414	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4169	0	4934	683	1274	0	0	4443	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	437	4936	0	4982	4717	0	0	0	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5618	700	4982	0	0	0	0	0	5157	0	189	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1250	4716	0	0	4806	0	4655	0	0	0	0	0	0	0	0	
Rank 21's ghost cells:	4365	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4806	0	4533	1835	0	0	0	0	0	0	0	0	
Rank 22's ghost cells:	1851	0	4429	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4525	0	4460	0	0	0	0	0	0	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4443	0	0	4654	1833	4460	0	0	0	0	0	0	0	0	0	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	615	0	0	5157	0	0	0	0	0	4560	5303	0	0	0	0	0	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4560	0	1272	5296	4365	180	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4412	0	0	199	0	0	0	0	5303	1271	0	4705	0	0	0	0	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5314	4705	0	1033	4838	0	0	
Rank 28's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4366	0	1012	0	5572	707	4658	
Rank 29's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	191	0	4838	5570	0	4905	0	
Rank 30's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	697	4905	0	6011	
Rank 31's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4661	0	6016	0	
=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          140.765         0.214765
    2          81.2135         0.576942
    3          57.4085         0.706883
    4          48.1738         0.839142
    5            45.81         0.950932
    6          38.1241         0.832222
    7          30.8273         0.808603
    8          29.9563         0.971745
    9          27.3413         0.912708
   10          22.6179         0.827241
   11          21.8071         0.964153
   12          21.4899         0.985457
   13           18.042         0.839554
   14          17.0783         0.946588
   15          17.6328          1.03247
   16          15.3048         0.867969
   17          14.0436         0.917595
   18          14.7242          1.04847
   19          13.3594         0.907306
   20          12.2543         0.917282
   21          12.7213          1.03811
   22          11.7279         0.921913
   23          11.0199         0.939625
   24          11.2171           1.0179
   25          10.3395         0.921759
   26          9.99886         0.967054
   27          9.90962         0.991076
   28          9.10833          0.91914
   29          9.14995          1.00457
   30          9.18997          1.00437
   31          9.08925         0.989041
   32          10.3032          1.13356
   33          11.5957          1.12545
   34          12.0155          1.03621
   35          11.4509         0.953005
   36          8.92454         0.779377
   37          6.83506         0.765873
   38          6.16969         0.902652
   39          5.37179         0.870674
   40          4.45809         0.829907
   41          4.10015         0.919712
   42           3.7997         0.926722
   43          3.20863         0.844444
   44          2.74479          0.85544
   45          2.54702         0.927946
   46          2.19024         0.859922
   47          1.83532         0.837955
   48          1.71569         0.934815
   49          1.49392         0.870741
   50          1.31591         0.880844
   51          1.25766         0.955731
   52          1.11314         0.885093
   53            1.062         0.954055
   54         0.969702         0.913092
   55         0.908147         0.936521
   56         0.888046         0.977866
   57         0.790884         0.890589
   58         0.816352           1.0322
   59         0.753338          0.92281
   60         0.741642         0.984475
   61         0.738134         0.995269
   62         0.693365         0.939348
   63         0.703409          1.01449
   64         0.651801         0.926631
   65         0.630399         0.967164
   66          0.56458         0.895593
   67         0.523416         0.927088
   68         0.427483         0.816718
   69         0.372019         0.870255
   70         0.292746         0.786912
   71          0.24329         0.831062
   72         0.213039         0.875656
   73         0.205721         0.965653
   74         0.212825          1.03453
   75         0.229622          1.07892
   76         0.227159         0.989276
   77         0.223428         0.983575
   78         0.220926         0.988802
   79         0.203532         0.921267
   80         0.176992         0.869603
   81         0.157427         0.889455
   82         0.144917          0.92054
   83         0.137349         0.947775
   84         0.122673         0.893146
   85         0.115217         0.939222
   86         0.114761         0.996038
   87         0.111008         0.967302
   88          0.10208          0.91957
   89         0.100579         0.985295
   90        0.0922736         0.917428
   91         0.087666         0.950065
   92        0.0839074         0.957126
   93         0.077093         0.918786
   94        0.0687393         0.891642
   95        0.0650211         0.945908
=== rate=0.907524, T=71.5818, TIT=0.753493, IT=95

 Elapsed time: 71.5818
Rank 0: Matrix-vector product took 0.270957 seconds
Rank 1: Matrix-vector product took 0.270318 seconds
Rank 2: Matrix-vector product took 0.266693 seconds
Rank 3: Matrix-vector product took 0.26747 seconds
Rank 4: Matrix-vector product took 0.271435 seconds
Rank 5: Matrix-vector product took 0.269812 seconds
Rank 6: Matrix-vector product took 0.26992 seconds
Rank 7: Matrix-vector product took 0.26894 seconds
Rank 8: Matrix-vector product took 0.272969 seconds
Rank 9: Matrix-vector product took 0.270897 seconds
Rank 10: Matrix-vector product took 0.271133 seconds
Rank 11: Matrix-vector product took 0.269492 seconds
Rank 12: Matrix-vector product took 0.26908 seconds
Rank 13: Matrix-vector product took 0.26378 seconds
Rank 14: Matrix-vector product took 0.267177 seconds
Rank 15: Matrix-vector product took 0.263577 seconds
Rank 16: Matrix-vector product took 0.2691 seconds
Rank 17: Matrix-vector product took 0.26881 seconds
Rank 18: Matrix-vector product took 0.265446 seconds
Rank 19: Matrix-vector product took 0.267932 seconds
Rank 20: Matrix-vector product took 0.267935 seconds
Rank 21: Matrix-vector product took 0.266957 seconds
Rank 22: Matrix-vector product took 0.267315 seconds
Rank 23: Matrix-vector product took 0.266984 seconds
Rank 24: Matrix-vector product took 0.268022 seconds
Rank 25: Matrix-vector product took 0.272114 seconds
Rank 26: Matrix-vector product took 0.274445 seconds
Rank 27: Matrix-vector product took 0.271153 seconds
Rank 28: Matrix-vector product took 0.271855 seconds
Rank 29: Matrix-vector product took 0.271526 seconds
Rank 30: Matrix-vector product took 0.26821 seconds
Rank 31: Matrix-vector product took 0.271879 seconds
Average time for Matrix-vector product is 0.269167 seconds

Rank 0: copyOwnerToAll took 0.00117526 seconds
Rank 1: copyOwnerToAll took 0.00117526 seconds
Rank 2: copyOwnerToAll took 0.00117526 seconds
Rank 3: copyOwnerToAll took 0.00117526 seconds
Rank 4: copyOwnerToAll took 0.00117526 seconds
Rank 5: copyOwnerToAll took 0.00117526 seconds
Rank 6: copyOwnerToAll took 0.00117526 seconds
Rank 7: copyOwnerToAll took 0.00117526 seconds
Rank 8: copyOwnerToAll took 0.00117526 seconds
Rank 9: copyOwnerToAll took 0.00117526 seconds
Rank 10: copyOwnerToAll took 0.00117526 seconds
Rank 11: copyOwnerToAll took 0.00117526 seconds
Rank 12: copyOwnerToAll took 0.00117526 seconds
Rank 13: copyOwnerToAll took 0.00117526 seconds
Rank 14: copyOwnerToAll took 0.00117526 seconds
Rank 15: copyOwnerToAll took 0.00117526 seconds
Rank 16: copyOwnerToAll took 0.00117526 seconds
Rank 17: copyOwnerToAll took 0.00117526 seconds
Rank 18: copyOwnerToAll took 0.00117526 seconds
Rank 19: copyOwnerToAll took 0.00117526 seconds
Rank 20: copyOwnerToAll took 0.00117526 seconds
Rank 21: copyOwnerToAll took 0.00117526 seconds
Rank 22: copyOwnerToAll took 0.00117526 seconds
Rank 23: copyOwnerToAll took 0.00117526 seconds
Rank 24: copyOwnerToAll took 0.00117526 seconds
Rank 25: copyOwnerToAll took 0.00117526 seconds
Rank 26: copyOwnerToAll took 0.00117526 seconds
Rank 27: copyOwnerToAll took 0.00117526 seconds
Rank 28: copyOwnerToAll took 0.00117526 seconds
Rank 29: copyOwnerToAll took 0.00117526 seconds
Rank 30: copyOwnerToAll took 0.00117526 seconds
Rank 31: copyOwnerToAll took 0.00117526 seconds
Average time for copyOwnertoAll is 0.00119231 seconds
