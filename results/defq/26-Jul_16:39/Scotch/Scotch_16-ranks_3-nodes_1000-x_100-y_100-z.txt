Number of cores/ranks per node is: 5
Scotch partitioner
Cell on rank 0 before loadbalancing: 623138
Cell on rank 1 before loadbalancing: 618750
Cell on rank 2 before loadbalancing: 619262
Cell on rank 3 before loadbalancing: 619006
Cell on rank 4 before loadbalancing: 619589
Cell on rank 5 before loadbalancing: 623887
Cell on rank 6 before loadbalancing: 624041
Cell on rank 7 before loadbalancing: 619639
Cell on rank 8 before loadbalancing: 631243
Cell on rank 9 before loadbalancing: 629393
Cell on rank 10 before loadbalancing: 628326
Cell on rank 11 before loadbalancing: 631250
Cell on rank 12 before loadbalancing: 629869
Cell on rank 13 before loadbalancing: 631250
Cell on rank 14 before loadbalancing: 625000
Cell on rank 15 before loadbalancing: 626357
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	11660	23	5220	0	0	5050	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	11658	0	4183	1250	0	0	1129	4404	0	0	0	0	0	0	0	0	
From rank 2 to: 	30	4186	0	12052	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	5219	1221	12047	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	10461	5031	2467	0	0	0	0	0	0	0	4088	
From rank 5 to: 	0	0	0	0	10459	0	0	3947	0	0	0	0	0	0	4614	2739	
From rank 6 to: 	5052	1112	0	0	5029	0	0	11009	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	4405	0	0	2415	3946	11019	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	11618	4547	1369	4896	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	11608	0	0	4792	2071	4295	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	4544	0	0	12216	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	1396	4787	12199	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	4895	2052	0	0	0	11443	4380	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	4290	0	0	11445	0	2400	4346	
From rank 14 to: 	0	0	0	0	0	4614	0	0	0	0	0	0	4379	2332	0	10606	
From rank 15 to: 	0	0	0	0	4088	2727	0	0	0	0	0	0	0	4348	10602	0	
loadb
After loadbalancing process 10 has 645086 cells.
After loadbalancing process 4 has 641636 cells.
After loadbalancing process 2 has 635530 cells.
After loadbalancing process 12 has 652639 cells.
After loadbalancing process 8 has 653673 cells.
After loadbalancing process 3 has 637493 cells.
After loadbalancing process 0 has 645091 cells.
After loadbalancing process 6 has 646243 cells.
After loadbalancing process 14 has 646931 cells.
After loadbalancing process 1 has 641374 cells.
After loadbalancing process 11 has 649632 cells.
After loadbalancing process 15 has 648122 cells.
After loadbalancing process 5 has 645646 cells.
After loadbalancing process 13 has 653731 cells.
After loadbalancing process 7 has 641424 cells.
After loadbalancing process 9 has 652159 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	11660	23	5220	0	0	5050	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	11658	0	4183	1250	0	0	1129	4404	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	30	4186	0	12052	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	5219	1221	12047	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	10461	5031	2467	0	0	0	0	0	0	0	4088	
Rank 5's ghost cells:	0	0	0	0	10459	0	0	3947	0	0	0	0	0	0	4614	2739	
Rank 6's ghost cells:	5052	1112	0	0	5029	0	0	11009	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	4405	0	0	2415	3946	11019	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	11618	4547	1369	4896	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	11608	0	0	4792	2071	4295	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	4544	0	0	12216	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	1396	4787	12199	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	4895	2052	0	0	0	11443	4380	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	4290	0	0	11445	0	2400	4346	
Rank 14's ghost cells:	0	0	0	0	0	4614	0	0	0	0	0	0	4379	2332	0	10606	
Rank 15's ghost cells:	0	0	0	0	4088	2727	0	0	0	0	0	0	0	4348	10602	0	
=== CGSolver
 Iter          Defect            Rate
    0          655.439
    1          138.692         0.211602
    2          79.6205          0.57408
    3          55.9975         0.703305
    4          45.2015         0.807205
    5          41.8832         0.926589
    6          38.1882         0.911778
    7          30.3708         0.795294
    8          27.6148         0.909252
    9          27.0207          0.97849
   10           22.676         0.839208
   11          20.2123         0.891352
   12           20.742          1.02621
   13           18.632         0.898276
   14          16.1026          0.86424
   15          16.6197          1.03212
   16          16.0119         0.963427
   17          13.6893         0.854944
   18          13.8286          1.01018
   19          13.6047         0.983808
   20          11.8784         0.873108
   21          11.8312         0.996032
   22          11.7463         0.992817
   23          10.5398         0.897289
   24          10.4409         0.990614
   25          10.3309         0.989469
   26          9.29246         0.899481
   27          9.22993         0.993271
   28           9.3154          1.00926
   29          8.55828         0.918724
   30          8.78936            1.027
   31          9.87595          1.12362
   32          10.6071          1.07403
   33          12.0265          1.13382
   34           12.284          1.02141
   35          9.15874         0.745584
   36           6.5462         0.714749
   37          5.71424         0.872909
   38          5.28329         0.924583
   39          4.53639         0.858629
   40          3.84662         0.847948
   41          3.55877         0.925168
   42            3.178         0.893007
   43          2.55671         0.804501
   44          2.34644          0.91776
   45          2.14126         0.912555
   46          1.70414          0.79586
   47          1.59114         0.933688
   48          1.44391         0.907472
   49          1.24018         0.858905
   50           1.2177         0.981871
   51          1.06101         0.871326
   52          1.03372         0.974276
   53         0.966996         0.935452
   54         0.868445         0.898085
   55         0.911891          1.05003
   56            0.786         0.861945
   57         0.811593          1.03256
   58         0.777013         0.957391
   59          0.72782          0.93669
   60         0.751352          1.03233
   61         0.698522         0.929686
   62         0.698579          1.00008
   63           0.6428         0.920154
   64         0.634998         0.987862
   65         0.526849         0.829686
   66         0.489558         0.929218
   67         0.389864          0.79636
   68         0.330953         0.848894
   69         0.269353         0.813871
   70         0.234848         0.871897
   71         0.204132          0.86921
   72         0.216781          1.06196
   73         0.227052          1.04738
   74         0.240806          1.06058
   75         0.227913         0.946459
   76          0.21139         0.927504
   77         0.191068         0.903865
   78         0.187057         0.979006
   79         0.154883         0.827996
   80         0.134966         0.871408
   81         0.126228         0.935262
   82         0.123636          0.97946
   83         0.107838          0.87222
   84         0.107735         0.999051
   85         0.104961         0.974251
   86        0.0992723         0.945801
   87        0.0925629         0.932414
   88        0.0906817         0.979676
   89        0.0838545         0.924712
   90        0.0799879         0.953889
   91        0.0760681         0.950995
   92        0.0672118         0.883574
   93        0.0632809         0.941515
=== rate=0.905368, T=137.259, TIT=1.47591, IT=93

 Elapsed time: 137.259
Rank 0: Matrix-vector product took 0.529982 seconds
Rank 1: Matrix-vector product took 0.524956 seconds
Rank 2: Matrix-vector product took 0.521072 seconds
Rank 3: Matrix-vector product took 0.523574 seconds
Rank 4: Matrix-vector product took 0.527802 seconds
Rank 5: Matrix-vector product took 0.540681 seconds
Rank 6: Matrix-vector product took 0.543573 seconds
Rank 7: Matrix-vector product took 0.531588 seconds
Rank 8: Matrix-vector product took 0.546142 seconds
Rank 9: Matrix-vector product took 0.542351 seconds
Rank 10: Matrix-vector product took 0.529093 seconds
Rank 11: Matrix-vector product took 0.532912 seconds
Rank 12: Matrix-vector product took 0.537977 seconds
Rank 13: Matrix-vector product took 0.539003 seconds
Rank 14: Matrix-vector product took 0.532028 seconds
Rank 15: Matrix-vector product took 0.53689 seconds
Average time for Matrix-vector product is 0.533726 seconds

Rank 0: copyOwnerToAll took 0.00174351 seconds
Rank 1: copyOwnerToAll took 0.00174351 seconds
Rank 2: copyOwnerToAll took 0.00174351 seconds
Rank 3: copyOwnerToAll took 0.00174351 seconds
Rank 4: copyOwnerToAll took 0.00174351 seconds
Rank 5: copyOwnerToAll took 0.00174351 seconds
Rank 6: copyOwnerToAll took 0.00174351 seconds
Rank 7: copyOwnerToAll took 0.00174351 seconds
Rank 8: copyOwnerToAll took 0.00174351 seconds
Rank 9: copyOwnerToAll took 0.00174351 seconds
Rank 10: copyOwnerToAll took 0.00174351 seconds
Rank 11: copyOwnerToAll took 0.00174351 seconds
Rank 12: copyOwnerToAll took 0.00174351 seconds
Rank 13: copyOwnerToAll took 0.00174351 seconds
Rank 14: copyOwnerToAll took 0.00174351 seconds
Rank 15: copyOwnerToAll took 0.00174351 seconds
Average time for copyOwnertoAll is 0.00184478 seconds
