Scotch partitioner
Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	470	0	520	
From rank 1 to: 	470	0	460	60	
From rank 2 to: 	0	460	0	520	
From rank 3 to: 	520	60	520	0	
After loadbalancing process 0 has 25960 cells.
After loadbalancing process 1 has 25820 cells.
After loadbalancing process 2 has 25980 cells.
After loadbalancing process 3 has 26300 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	470	0	520	
Rank 1's ghost cells:	470	0	460	60	
Rank 2's ghost cells:	0	460	0	520	
Rank 3's ghost cells:	520	60	520	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.7455         0.210581
    2          20.2184         0.599143
    3          19.5083         0.964878
    4           11.465         0.587697
    5          5.71572         0.498538
    6          4.20886         0.736365
    7          2.07944         0.494062
    8          1.31634         0.633028
    9         0.972371         0.738692
   10         0.493824         0.507856
   11          0.35279         0.714405
   12         0.223215         0.632713
   13         0.147014         0.658622
   14        0.0868114         0.590497
   15        0.0598824         0.689799
   16        0.0363819         0.607555
   17        0.0227188         0.624455
   18        0.0143025         0.629543
=== rate=0.595709, T=1.04375, TIT=0.0579859, IT=18

 Elapsed time: 1.04375
Rank 0: Matrix-vector product took 20 milliseconds
Rank 1: Matrix-vector product took 20 milliseconds
Rank 2: Matrix-vector product took 20 milliseconds
Rank 3: Matrix-vector product took 20 milliseconds
Average time for Matrix-vector product is 20 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 0 milliseconds
Rank 2: copyOwnerToAll took 0 milliseconds
Rank 3: copyOwnerToAll took 0 milliseconds
Average time for copyOwnertoAll is 0 milliseconds
Scotch partitioner
Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	470	0	520	
From rank 1 to: 	470	0	460	60	
From rank 2 to: 	0	460	0	520	
From rank 3 to: 	520	60	520	0	
After loadbalancing process 0 has 25960 cells.
After loadbalancing process 1 has 25820 cells.
After loadbalancing process 2 has 25980 cells.
After loadbalancing process 3 has 26300 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	470	0	520	
Rank 1's ghost cells:	470	0	460	60	
Rank 2's ghost cells:	0	460	0	520	
Rank 3's ghost cells:	520	60	520	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.7455         0.210581
    2          20.2184         0.599143
    3          19.5083         0.964878
    4           11.465         0.587697
    5          5.71572         0.498538
    6          4.20886         0.736365
    7          2.07944         0.494062
    8          1.31634         0.633028
    9         0.972371         0.738692
   10         0.493824         0.507856
   11          0.35279         0.714405
   12         0.223215         0.632713
   13         0.147014         0.658622
   14        0.0868114         0.590497
   15        0.0598824         0.689799
   16        0.0363819         0.607555
   17        0.0227188         0.624455
   18        0.0143025         0.629543
=== rate=0.595709, T=1.07447, TIT=0.059693, IT=18

 Elapsed time: 1.07447
Rank 0: Matrix-vector product took 0.0210177 seconds
Rank 1: Matrix-vector product took 0.0207626 seconds
Rank 2: Matrix-vector product took 0.0210448 seconds
Rank 3: Matrix-vector product took 0.0212708 seconds
Average time for Matrix-vector product is 0.021024 seconds

Rank 0: copyOwnerToAll took 9.899e-05 seconds
Rank 1: copyOwnerToAll took 9.975e-05 seconds
Rank 2: copyOwnerToAll took 0.00011209 seconds
Rank 3: copyOwnerToAll took 0.00011756 seconds
Average time for copyOwnertoAll is 0.000107098 seconds
Scotch partitioner
Cell on rank 0 before loadbalancing: 24970
Cell on rank 1 before loadbalancing: 24830
Cell on rank 2 before loadbalancing: 25000
Cell on rank 3 before loadbalancing: 25200
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	470	0	520	
From rank 1 to: 	470	0	460	60	
From rank 2 to: 	0	460	0	520	
From rank 3 to: 	520	60	520	0	
After loadbalancing process 2 has 25980 cells.
After loadbalancing process 0 has 25960 cells.
After loadbalancing process 3 has 26300 cells.
After loadbalancing process 1 has 25820 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	470	0	520	
Rank 1's ghost cells:	470	0	460	60	
Rank 2's ghost cells:	0	460	0	520	
Rank 3's ghost cells:	520	60	520	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.7455         0.210581
    2          20.2184         0.599143
    3          19.5083         0.964878
    4           11.465         0.587697
    5          5.71572         0.498538
    6          4.20886         0.736365
    7          2.07944         0.494062
    8          1.31634         0.633028
    9         0.972371         0.738692
   10         0.493824         0.507856
   11          0.35279         0.714405
   12         0.223215         0.632713
   13         0.147014         0.658622
   14        0.0868114         0.590497
   15        0.0598824         0.689799
   16        0.0363819         0.607555
   17        0.0227188         0.624455
   18        0.0143025         0.629543
=== rate=0.595709, T=1.04482, TIT=0.0580458, IT=18

 Elapsed time: 1.04482
Rank 0: Matrix-vector product took 0.0210029 seconds
Rank 1: Matrix-vector product took 0.0209001 seconds
Rank 2: Matrix-vector product took 0.0207159 seconds
Rank 3: Matrix-vector product took 0.0209147 seconds
Average time for Matrix-vector product is 0.0208834 seconds

Rank 0: copyOwnerToAll took 8.361e-05 seconds
Rank 1: copyOwnerToAll took 9.613e-05 seconds
Rank 2: copyOwnerToAll took 9.686e-05 seconds
Rank 3: copyOwnerToAll took 0.00010275 seconds
Average time for copyOwnertoAll is 9.48375e-05 seconds
