
Usage: ./mpiexec [global opts] [local opts for exec1] [exec1] [exec1 args] : [local opts for exec2] [exec2] [exec2 args] : ...

Global options (passed to all executables):

  Global environment options:
    -genv {name} {value}             environment variable name and value
    -genvlist {env1,env2,...}        environment variable list to pass
    -genvnone                        do not pass any environment variables
    -genvall                         pass all environment variables not managed
                                          by the launcher (default)

  Other global options:
    -f {name}                        file containing the host names
    -hosts {host list}               comma separated host list
    -wdir {dirname}                  working directory to use
    -configfile {name}               config file containing MPMD launch options


Local options (passed to individual executables):

  Local environment options:
    -env {name} {value}              environment variable name and value
    -envlist {env1,env2,...}         environment variable list to pass
    -envnone                         do not pass any environment variables
    -envall                          pass all environment variables (default)

  Other local options:
    -n/-np {value}                   number of processes
    {exec_name} {args}               executable name and arguments


Hydra specific options (treated as global):

  Launch options:
    -launcher                        launcher to use (ssh rsh fork slurm ll lsf sge manual persist)
    -launcher-exec                   executable to use to launch processes
    -enable-x/-disable-x             enable or disable X forwarding

  Resource management kernel options:
    -rmk                             resource management kernel to use (user slurm ll lsf sge pbs cobalt)

  Processor topology options:
    -topolib                         processor topology library (hwloc)
    -bind-to                         process binding
    -map-by                          process mapping
    -membind                         memory binding policy

  Checkpoint/Restart options:
    -ckpoint-interval                checkpoint interval
    -ckpoint-prefix                  checkpoint file prefix
    -ckpoint-num                     checkpoint number to restart
    -ckpointlib                      checkpointing library (none)

  Demux engine options:
    -demux                           demux engine (poll select)

  Other Hydra options:
    -verbose                         verbose mode
    -info                            build information
    -print-all-exitcodes             print exit codes of all processes
    -iface                           network interface to use
    -ppn                             processes per node
    -profile                         turn on internal profiling
    -prepend-rank                    prepend rank to output
    -prepend-pattern                 prepend pattern to output
    -outfile-pattern                 direct stdout to file
    -errfile-pattern                 direct stderr to file
    -nameserver                      name server information (host:port format)
    -disable-auto-cleanup            don't cleanup processes on error
    -disable-hostname-propagation    let MPICH auto-detect the hostname
    -order-nodes                     order nodes as ascending/descending cores
    -localhost                       local hostname for the launching node
    -usize                           universe size (SYSTEM, INFINITE, <value>)

Please see the intructions provided at
http://wiki.mpich.org/mpich/index.php/Using_the_Hydra_Process_Manager
for further details


Usage: ./mpiexec [global opts] [local opts for exec1] [exec1] [exec1 args] : [local opts for exec2] [exec2] [exec2 args] : ...

Global options (passed to all executables):

  Global environment options:
    -genv {name} {value}             environment variable name and value
    -genvlist {env1,env2,...}        environment variable list to pass
    -genvnone                        do not pass any environment variables
    -genvall                         pass all environment variables not managed
                                          by the launcher (default)

  Other global options:
    -f {name}                        file containing the host names
    -hosts {host list}               comma separated host list
    -wdir {dirname}                  working directory to use
    -configfile {name}               config file containing MPMD launch options


Local options (passed to individual executables):

  Local environment options:
    -env {name} {value}              environment variable name and value
    -envlist {env1,env2,...}         environment variable list to pass
    -envnone                         do not pass any environment variables
    -envall                          pass all environment variables (default)

  Other local options:
    -n/-np {value}                   number of processes
    {exec_name} {args}               executable name and arguments


Hydra specific options (treated as global):

  Launch options:
    -launcher                        launcher to use (ssh rsh fork slurm ll lsf sge manual persist)
    -launcher-exec                   executable to use to launch processes
    -enable-x/-disable-x             enable or disable X forwarding

  Resource management kernel options:
    -rmk                             resource management kernel to use (user slurm ll lsf sge pbs cobalt)

  Processor topology options:
    -topolib                         processor topology library (hwloc)
    -bind-to                         process binding
    -map-by                          process mapping
    -membind                         memory binding policy

  Checkpoint/Restart options:
    -ckpoint-interval                checkpoint interval
    -ckpoint-prefix                  checkpoint file prefix
    -ckpoint-num                     checkpoint number to restart
    -ckpointlib                      checkpointing library (none)

  Demux engine options:
    -demux                           demux engine (poll select)

  Other Hydra options:
    -verbose                         verbose mode
    -info                            build information
    -print-all-exitcodes             print exit codes of all processes
    -iface                           network interface to use
    -ppn                             processes per node
    -profile                         turn on internal profiling
    -prepend-rank                    prepend rank to output
    -prepend-pattern                 prepend pattern to output
    -outfile-pattern                 direct stdout to file
    -errfile-pattern                 direct stderr to file
    -nameserver                      name server information (host:port format)
    -disable-auto-cleanup            don't cleanup processes on error
    -disable-hostname-propagation    let MPICH auto-detect the hostname
    -order-nodes                     order nodes as ascending/descending cores
    -localhost                       local hostname for the launching node
    -usize                           universe size (SYSTEM, INFINITE, <value>)

Please see the intructions provided at
http://wiki.mpich.org/mpich/index.php/Using_the_Hydra_Process_Manager
for further details

