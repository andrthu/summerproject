METIS partitioner
Cell on rank 0 before loadbalancing: 249990
Cell on rank 1 before loadbalancing: 250002
Cell on rank 2 before loadbalancing: 250007
Cell on rank 3 before loadbalancing: 250001
Edge-cut: 23408
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	5064	98	4880	
From rank 1 to: 	5077	0	5024	252	
From rank 2 to: 	98	5037	0	4891	
From rank 3 to: 	4861	252	4917	0	
After loadbalancing process 3 has 260031 cells.
After loadbalancing process 2 has 260033 cells.
After loadbalancing process 0 has 260032 cells.
After loadbalancing process 1 has 260355 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	5064	98	4880	
Rank 1's ghost cells:	5077	0	5024	252	
Rank 2's ghost cells:	98	5037	0	4891	
Rank 3's ghost cells:	4861	252	4917	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          52.7265         0.211075
    2          30.2057         0.572874
    3          21.1627         0.700619
    4           16.938         0.800372
    5          15.4573         0.912583
    6          14.2354         0.920946
    7           11.434         0.803212
    8          10.2114         0.893072
    9          9.92858         0.972304
   10          8.38858         0.844892
   11          7.37096         0.878689
   12          7.46393          1.01261
   13          6.84605         0.917219
   14          5.77501         0.843554
   15           5.7144         0.989505
   16          5.73432          1.00349
   17          4.90137         0.854743
   18          4.63218         0.945079
   19          4.70609          1.01595
   20          4.19914         0.892278
   21          3.94046         0.938398
   22          3.94668          1.00158
   23          3.59996         0.912148
   24          3.39046         0.941807
   25          3.40395          1.00398
   26          3.09106         0.908081
   27          2.89462          0.93645
   28          2.93735          1.01476
   29          2.74887         0.935833
   30          2.64818         0.963369
   31          2.87339          1.08505
   32          3.01407          1.04896
   33          3.04126          1.00902
   34           2.8809         0.947271
   35          2.19034         0.760296
   36          1.63426         0.746123
   37          1.50389         0.920226
   38          1.38466         0.920722
   39          1.12598         0.813181
   40         0.976391         0.867145
   41         0.909848         0.931849
   42         0.749299         0.823542
   43         0.594486          0.79339
   44         0.540568         0.909302
   45         0.451269         0.834806
   46         0.344676         0.763793
   47         0.302893         0.878776
   48         0.249425         0.823473
   49         0.206022         0.825989
   50         0.178847         0.868095
   51         0.140315         0.784555
   52          0.12571         0.895911
   53        0.0995953         0.792264
   54        0.0779235         0.782401
   55        0.0692873         0.889171
   56        0.0512403         0.739534
   57        0.0420911         0.821446
   58        0.0330226         0.784551
   59        0.0254747         0.771432
   60        0.0190223         0.746715
=== rate=0.85381, T=34.4169, TIT=0.573614, IT=60

 Elapsed time: 34.4169
Rank 0: Matrix-vector product took 209 milliseconds
Rank 1: Matrix-vector product took 209 milliseconds
Rank 2: Matrix-vector product took 209 milliseconds
Rank 3: Matrix-vector product took 209 milliseconds
Average time for Matrix-vector product is 209 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 0 milliseconds
Rank 2: copyOwnerToAll took 0 milliseconds
Rank 3: copyOwnerToAll took 0 milliseconds
Average time for copyOwnertoAll is 0 milliseconds
METIS partitioner
Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	5030	4959	329	
From rank 1 to: 	5010	0	100	4806	
From rank 2 to: 	4997	100	0	4903	
From rank 3 to: 	328	4791	4909	0	
After loadbalancing process 1 has 259912 cells.
After loadbalancing process 3 has 260036 cells.
After loadbalancing process 0 has 260317 cells.
After loadbalancing process 2 has 259997 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	5030	4959	329	
Rank 1's ghost cells:	5010	0	100	4806	
Rank 2's ghost cells:	4997	100	0	4903	
Rank 3's ghost cells:	328	4791	4909	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          52.6495         0.210767
    2          30.1548         0.572747
    3          21.1031         0.699826
    4          16.7671         0.794531
    5          15.0651         0.898495
    6          13.9523         0.926131
    7           11.394         0.816639
    8          10.0376          0.88096
    9          9.88759         0.985052
   10          8.49871         0.859533
   11          7.34828         0.864634
   12          7.56096          1.02894
   13          7.07437         0.935645
   14          5.87511         0.830478
   15          5.86339         0.998004
   16          5.77955         0.985702
   17          4.83018         0.835735
   18           4.6144         0.955327
   19          4.73173          1.02543
   20          4.19278         0.886099
   21          3.87727         0.924749
   22          3.91331           1.0093
   23          3.61191          0.92298
   24          3.33721         0.923947
   25          3.34323           1.0018
   26          3.10657         0.929211
   27          2.88232         0.927815
   28          2.90033          1.00625
   29          2.74506         0.946466
METIS partitioner
Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	5030	4959	329	
From rank 1 to: 	5010	0	100	4806	
From rank 2 to: 	4997	100	0	4903	
From rank 3 to: 	328	4791	4909	0	
After loadbalancing process 1 has 259912 cells.
After loadbalancing process 2 has 259997 cells.
After loadbalancing process 0 has 260317 cells.
After loadbalancing process 3 has 260036 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	5030	4959	329	
Rank 1's ghost cells:	5010	0	100	4806	
Rank 2's ghost cells:	4997	100	0	4903	
Rank 3's ghost cells:	328	4791	4909	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          52.6495         0.210767
    2          30.1548         0.572747
    3          21.1031         0.699826
    4          16.7671         0.794531
    5          15.0651         0.898495
    6          13.9523         0.926131
    7           11.394         0.816639
    8          10.0376          0.88096
    9          9.88759         0.985052
   10          8.49871         0.859533
   11          7.34828         0.864634
   12          7.56096          1.02894
   13          7.07437         0.935645
   14          5.87511         0.830478
   15          5.86339         0.998004
   16          5.77955         0.985702
   17          4.83018         0.835735
   18           4.6144         0.955327
   19          4.73173          1.02543
   20          4.19278         0.886099
   21          3.87727         0.924749
   22          3.91331           1.0093
   23          3.61191          0.92298
   24          3.33721         0.923947
   25          3.34323           1.0018
   26          3.10657         0.929211
   27          2.88232         0.927815
   28          2.90033          1.00625
   29          2.74506         0.946466
   30          2.61962         0.954303
   31          2.80476          1.07067
   32          2.97244          1.05978
   33          2.98194           1.0032
   34          2.77771         0.931511
   35          2.14785         0.773244
   36           1.5959         0.743023
   37          1.47173         0.922197
   38          1.38829         0.943301
   39          1.09704         0.790211
   40         0.945376         0.861752
   41         0.909315         0.961855
   42         0.718948         0.790648
   43         0.572051         0.795678
   44         0.538427         0.941222
   45          0.42215         0.784042
   46         0.325475         0.770994
   47         0.304818         0.936534
   48         0.237357         0.778683
   49         0.195324         0.822913
   50         0.176509         0.903673
   51         0.135735         0.768998
   52         0.119251         0.878556
   53        0.0956255         0.801885
   54        0.0764722         0.799704
   55        0.0654446         0.855796
   56         0.049597         0.757847
   57        0.0410052         0.826768
   58        0.0308664         0.752743
   59        0.0246202         0.797638
=== rate=0.855257, T=34.0441, TIT=0.577019, IT=59

 Elapsed time: 34.0441
Rank 0: Matrix-vector product took 0.211543 seconds
Rank 1: Matrix-vector product took 0.212646 seconds
Rank 2: Matrix-vector product took 0.212303 seconds
Rank 3: Matrix-vector product took 0.21115 seconds
Average time for Matrix-vector product is 0.21191 seconds

Rank 0: copyOwnerToAll took 0.000665162 seconds
Rank 1: copyOwnerToAll took 0.000639152 seconds
Rank 2: copyOwnerToAll took 0.000626492 seconds
Rank 3: copyOwnerToAll took 0.000650162 seconds
Average time for copyOwnertoAll is 0.000645242 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 249999
Cell on rank 1 before loadbalancing: 249996
Cell on rank 2 before loadbalancing: 249997
Cell on rank 3 before loadbalancing: 250008
Edge-cut: 22646
			Rank 0	Rank 1	Rank 2	Rank 3	
From rank 0 to: 	0	5030	4959	329	
From rank 1 to: 	5010	0	100	4806	
From rank 2 to: 	4997	100	0	4903	
From rank 3 to: 	328	4791	4909	0	
After loadbalancing process 2 has 259997 cells.
After loadbalancing process 1 has 259912 cells.
After loadbalancing process 0 has 260317 cells.
After loadbalancing process 3 has 260036 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	
Rank 0's ghost cells:	0	5030	4959	329	
Rank 1's ghost cells:	5010	0	100	4806	
Rank 2's ghost cells:	4997	100	0	4903	
Rank 3's ghost cells:	328	4791	4909	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          52.6495         0.210767
    2          30.1548         0.572747
    3          21.1031         0.699826
    4          16.7671         0.794531
    5          15.0651         0.898495
    6          13.9523         0.926131
    7           11.394         0.816639
    8          10.0376          0.88096
    9          9.88759         0.985052
   10          8.49871         0.859533
   11          7.34828         0.864634
   12          7.56096          1.02894
   13          7.07437         0.935645
   14          5.87511         0.830478
   15          5.86339         0.998004
   16          5.77955         0.985702
   17          4.83018         0.835735
   18           4.6144         0.955327
   19          4.73173          1.02543
   20          4.19278         0.886099
   21          3.87727         0.924749
   22          3.91331           1.0093
   23          3.61191          0.92298
   24          3.33721         0.923947
   25          3.34323           1.0018
   26          3.10657         0.929211
   27          2.88232         0.927815
   28          2.90033          1.00625
   29          2.74506         0.946466
   30          2.61962         0.954303
   31          2.80476          1.07067
   32          2.97244          1.05978
   33          2.98194           1.0032
   34          2.77771         0.931511
   35          2.14785         0.773244
   36           1.5959         0.743023
   37          1.47173         0.922197
   38          1.38829         0.943301
   39          1.09704         0.790211
   40         0.945376         0.861752
   41         0.909315         0.961855
   42         0.718948         0.790648
   43         0.572051         0.795678
   44         0.538427         0.941222
   45          0.42215         0.784042
   46         0.325475         0.770994
   47         0.304818         0.936534
   48         0.237357         0.778683
   49         0.195324         0.822913
   50         0.176509         0.903673
   51         0.135735         0.768998
   52         0.119251         0.878556
   53        0.0956255         0.801885
   54        0.0764722         0.799704
   55        0.0654446         0.855796
   56         0.049597         0.757847
   57        0.0410052         0.826768
   58        0.0308664         0.752743
   59        0.0246202         0.797638
=== rate=0.855257, T=33.7569, TIT=0.57215, IT=59

 Elapsed time: 33.7569
Rank 0: Matrix-vector product took 0.211713 seconds
Rank 1: Matrix-vector product took 0.212174 seconds
Rank 2: Matrix-vector product took 0.21194 seconds
Rank 3: Matrix-vector product took 0.211818 seconds
Average time for Matrix-vector product is 0.211911 seconds

Rank 0: copyOwnerToAll took 0.000724261 seconds
Rank 1: copyOwnerToAll took 0.000630161 seconds
Rank 2: copyOwnerToAll took 0.000661961 seconds
Rank 3: copyOwnerToAll took 0.000700781 seconds
Average time for copyOwnertoAll is 0.000679291 seconds
