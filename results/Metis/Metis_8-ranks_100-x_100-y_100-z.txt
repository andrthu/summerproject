METIS partitioner
Cell on rank 0 before loadbalancing: 125006
Cell on rank 1 before loadbalancing: 124991
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 124974
Cell on rank 4 before loadbalancing: 125022
Cell on rank 5 before loadbalancing: 124999
Cell on rank 6 before loadbalancing: 125003
Cell on rank 7 before loadbalancing: 125001
Edge-cut: 46138
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	5161	2237	2669	360	0	2176	16	
From rank 1 to: 	5161	0	0	0	0	0	103	2567	
From rank 2 to: 	2264	0	0	4978	0	0	0	0	
From rank 3 to: 	2672	0	4980	0	5033	0	0	0	
From rank 4 to: 	401	0	0	5032	0	4625	2565	0	
From rank 5 to: 	0	0	0	0	4625	0	2604	0	
From rank 6 to: 	2179	102	0	0	2529	2593	0	4782	
From rank 7 to: 	14	2567	0	0	0	0	4784	0	
After loadbalancing process 7 has 132366 cells.
After loadbalancing process 2 has 132246 cells.
After loadbalancing process 5 has 132228 cells.
After loadbalancing process 1 has 132822 cells.
After loadbalancing process 4 has 137645 cells.
After loadbalancing process 6 has 137188 cells.
After loadbalancing process 0 has 137625 cells.
After loadbalancing process 3 has 137659 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	5161	2237	2669	360	0	2176	16	
Rank 1's ghost cells:	5161	0	0	0	0	0	103	2567	
Rank 2's ghost cells:	2264	0	0	4978	0	0	0	0	
Rank 3's ghost cells:	2672	0	4980	0	5033	0	0	0	
Rank 4's ghost cells:	401	0	0	5032	0	4625	2565	0	
Rank 5's ghost cells:	0	0	0	0	4625	0	2604	0	
Rank 6's ghost cells:	2179	102	0	0	2529	2593	0	4782	
Rank 7's ghost cells:	14	2567	0	0	0	0	4784	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.8165         0.215439
    2          31.0714         0.577358
    3          21.8238         0.702374
    4          17.9456         0.822295
    5          16.5214         0.920638
    6          14.1156         0.854384
    7          11.5441         0.817828
    8          10.9583         0.949256
    9          10.0217         0.914528
   10          8.28873         0.827079
   11          7.85104         0.947195
   12          7.83156         0.997519
   13          6.64125         0.848011
   14          6.05457          0.91166
   15          6.23163          1.02924
   16          5.67618         0.910867
   17          5.05741         0.890988
   18          5.09836           1.0081
   19          4.77984         0.937527
   20          4.30713         0.901102
   21          4.29081         0.996212
   22          4.03444         0.940252
   23          3.70814         0.919121
   24          3.69644         0.996843
   25          3.43911         0.930387
   26          3.15593         0.917659
   27          3.16842          1.00396
   28          2.95527         0.932726
   29          2.73788          0.92644
   30          2.80506          1.02454
   31          2.79664         0.996996
   32          2.81766          1.00752
   33          3.10407          1.10165
   34          3.10891          1.00156
   35           2.6939         0.866509
   36          2.16491         0.803634
   37           1.6146         0.745806
   38           1.2824         0.794252
   39          1.17147         0.913499
   40          1.06661         0.910487
   41         0.947314         0.888154
   42         0.803698         0.848397
   43         0.642614         0.799572
   44         0.576645         0.897342
   45         0.515422         0.893829
   46         0.391523         0.759618
   47         0.332613         0.849535
   48         0.295578         0.888656
   49         0.228423         0.772799
   50         0.196503         0.860262
   51         0.171048         0.870457
   52         0.138842         0.811713
   53         0.121302         0.873672
   54        0.0980253          0.80811
   55        0.0805786         0.822018
   56        0.0669277         0.830589
   57        0.0528858         0.790193
   58        0.0433743          0.82015
   59        0.0342715         0.790134
   60        0.0272142         0.794078
   61        0.0208389         0.765734
=== rate=0.857306, T=18.7421, TIT=0.307248, IT=61

 Elapsed time: 18.7421
Rank 0: Matrix-vector product took 110 milliseconds
Rank 1: Matrix-vector product took 106 milliseconds
Rank 2: Matrix-vector product took 105 milliseconds
Rank 3: Matrix-vector product took 110 milliseconds
Rank 4: Matrix-vector product took 111 milliseconds
Rank 5: Matrix-vector product took 105 milliseconds
Rank 6: Matrix-vector product took 109 milliseconds
Rank 7: Matrix-vector product took 106 milliseconds
Average time for Matrix-vector product is 107 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 6 milliseconds
Rank 2: copyOwnerToAll took 2 milliseconds
Rank 3: copyOwnerToAll took 2 milliseconds
Rank 4: copyOwnerToAll took 0 milliseconds
Rank 5: copyOwnerToAll took 6 milliseconds
Rank 6: copyOwnerToAll took 3 milliseconds
Rank 7: copyOwnerToAll took 6 milliseconds
Average time for copyOwnertoAll is 3 milliseconds
METIS partitioner
Cell on rank 0 before loadbalancing: 125002
Cell on rank 1 before loadbalancing: 124995
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 125000
Cell on rank 4 before loadbalancing: 125002
Cell on rank 5 before loadbalancing: 125004
Cell on rank 6 before loadbalancing: 124995
Cell on rank 7 before loadbalancing: 124998
Edge-cut: 44997
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4686	0	0	0	0	2634	0	
From rank 1 to: 	4686	0	1927	2846	0	659	2063	0	
From rank 2 to: 	0	1931	0	4643	2358	2471	0	0	
From rank 3 to: 	0	2855	4641	0	0	0	0	0	
From rank 4 to: 	0	0	2361	0	0	4601	0	0	
From rank 5 to: 	0	692	2462	0	4598	0	2547	2656	
From rank 6 to: 	2625	2075	0	0	0	2550	0	4739	
From rank 7 to: 	0	0	0	0	0	2690	4738	0	
After loadbalancing process 3 has 132496 cells.
After loadbalancing process 7 has 132426 cells.
After loadbalancing process 0 has 132322 cells.
After loadbalancing process 1 has 137176 cells.
After loadbalancing process 6 has 136984 cells.
After loadbalancing process 2 has 136407 cells.
After loadbalancing process 4 has 131964 cells.
After loadbalancing process 5 has 137959 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4686	0	0	0	0	2634	0	
Rank 1's ghost cells:	4686	0	1927	2846	0	659	2063	0	
Rank 2's ghost cells:	0	1931	0	4643	2358	2471	0	0	
Rank 3's ghost cells:	0	2855	4641	0	0	0	0	0	
Rank 4's ghost cells:	0	0	2361	0	0	4601	0	0	
Rank 5's ghost cells:	0	692	2462	0	4598	0	2547	2656	
Rank 6's ghost cells:	2625	2075	0	0	0	2550	0	4739	
Rank 7's ghost cells:	0	0	0	0	0	2690	4738	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7694          0.21525
    2          31.0392         0.577265
    3          21.8254         0.703156
    4          18.0322         0.826203
    5          16.6646         0.924157
    6          14.0991         0.846049
    7          11.4744         0.813842
    8          10.8675         0.947112
    9          10.0682         0.926446
   10          8.33076         0.827433
   11          7.82907         0.939778
   12          7.83876          1.00124
   13          6.66097         0.849748
   14          5.99701         0.900321
   15          6.15707          1.02669
   16           5.6756         0.921801
   17          5.02639         0.885615
   18          5.09697          1.01404
   19          4.81509         0.944698
   20          4.30564         0.894197
   21          4.30625          1.00014
   22          4.06637         0.944295
   23          3.71177         0.912798
   24          3.71072         0.999717
   25          3.47652         0.936885
   26          3.14708         0.905239
   27          3.16041          1.00424
   28          2.96968         0.939651
   29          2.73196         0.919949
   30          2.80115          1.02533
   31          2.80685          1.00204
   32           2.8251           1.0065
   33          3.11008          1.10087
   34          3.11887          1.00283
   35          2.65059         0.849856
   36          2.12673          0.80236
   37          1.61891         0.761222
   38            1.302         0.804245
   39          1.18462         0.909842
   40           1.0602         0.894971
   41         0.934567         0.881503
   42         0.812852         0.869763
   43         0.658791         0.810469
   44         0.570956         0.866671
   45         0.500396         0.876419
   46         0.386851         0.773089
   47            0.326         0.842702
   48         0.282079         0.865272
   49          0.22281         0.789886
   50         0.196633         0.882514
   51         0.166235         0.845409
METIS partitioner
Cell on rank 0 before loadbalancing: 125002
Cell on rank 1 before loadbalancing: 124995
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 125000
Cell on rank 4 before loadbalancing: 125002
Cell on rank 5 before loadbalancing: 125004
Cell on rank 6 before loadbalancing: 124995
Cell on rank 7 before loadbalancing: 124998
Edge-cut: 44997
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4686	0	0	0	0	2634	0	
From rank 1 to: 	4686	0	1927	2846	0	659	2063	0	
From rank 2 to: 	0	1931	0	4643	2358	2471	0	0	
From rank 3 to: 	0	2855	4641	0	0	0	0	0	
From rank 4 to: 	0	0	2361	0	0	4601	0	0	
From rank 5 to: 	0	692	2462	0	4598	0	2547	2656	
From rank 6 to: 	2625	2075	0	0	0	2550	0	4739	
From rank 7 to: 	0	0	0	0	0	2690	4738	0	
After loadbalancing process 4 has 131964 cells.
After loadbalancing process 3 has 132496 cells.
After loadbalancing process 0 has 132322 cells.
After loadbalancing process 7 has 132426 cells.
After loadbalancing process 2 has 136407 cells.
After loadbalancing process 6 has 136984 cells.
After loadbalancing process 1 has 137176 cells.
After loadbalancing process 5 has 137959 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4686	0	0	0	0	2634	0	
Rank 1's ghost cells:	4686	0	1927	2846	0	659	2063	0	
Rank 2's ghost cells:	0	1931	0	4643	2358	2471	0	0	
Rank 3's ghost cells:	0	2855	4641	0	0	0	0	0	
Rank 4's ghost cells:	0	0	2361	0	0	4601	0	0	
Rank 5's ghost cells:	0	692	2462	0	4598	0	2547	2656	
Rank 6's ghost cells:	2625	2075	0	0	0	2550	0	4739	
Rank 7's ghost cells:	0	0	0	0	0	2690	4738	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7694          0.21525
    2          31.0392         0.577265
    3          21.8254         0.703156
    4          18.0322         0.826203
    5          16.6646         0.924157
    6          14.0991         0.846049
    7          11.4744         0.813842
    8          10.8675         0.947112
    9          10.0682         0.926446
   10          8.33076         0.827433
   11          7.82907         0.939778
   12          7.83876          1.00124
   13          6.66097         0.849748
   14          5.99701         0.900321
   15          6.15707          1.02669
   16           5.6756         0.921801
   17          5.02639         0.885615
   18          5.09697          1.01404
   19          4.81509         0.944698
   20          4.30564         0.894197
   21          4.30625          1.00014
   22          4.06637         0.944295
   23          3.71177         0.912798
   24          3.71072         0.999717
   25          3.47652         0.936885
   26          3.14708         0.905239
   27          3.16041          1.00424
   28          2.96968         0.939651
   29          2.73196         0.919949
   30          2.80115          1.02533
   31          2.80685          1.00204
   32           2.8251           1.0065
   33          3.11008          1.10087
   34          3.11887          1.00283
   35          2.65059         0.849856
   36          2.12673          0.80236
   37          1.61891         0.761222
   38            1.302         0.804245
   39          1.18462         0.909842
   40           1.0602         0.894971
   41         0.934567         0.881503
   42         0.812852         0.869763
   43         0.658791         0.810469
   44         0.570956         0.866671
   45         0.500396         0.876419
   46         0.386851         0.773089
   47            0.326         0.842702
   48         0.282079         0.865272
   49          0.22281         0.789886
   50         0.196633         0.882514
   51         0.166235         0.845409
   52         0.135335          0.81412
   53         0.118351         0.874502
   54        0.0962141         0.812955
   55        0.0790824         0.821942
   56        0.0646433         0.817417
   57        0.0522044         0.807577
   58        0.0416891         0.798575
   59        0.0322266         0.773022
   60        0.0254921         0.791026
   61        0.0188471         0.739333
=== rate=0.855895, T=18.8525, TIT=0.309058, IT=61

 Elapsed time: 18.8525
Rank 0: Matrix-vector product took 0.107714 seconds
Rank 1: Matrix-vector product took 0.111869 seconds
Rank 2: Matrix-vector product took 0.110416 seconds
Rank 3: Matrix-vector product took 0.107541 seconds
Rank 4: Matrix-vector product took 0.107021 seconds
Rank 5: Matrix-vector product took 0.112023 seconds
Rank 6: Matrix-vector product took 0.111277 seconds
Rank 7: Matrix-vector product took 0.107119 seconds
Average time for Matrix-vector product is 0.109373 seconds

Rank 0: copyOwnerToAll took 0.000624892 seconds
Rank 1: copyOwnerToAll took 0.000808662 seconds
Rank 2: copyOwnerToAll took 0.000760102 seconds
Rank 3: copyOwnerToAll took 0.000655552 seconds
Rank 4: copyOwnerToAll took 0.000655242 seconds
Rank 5: copyOwnerToAll took 0.000820662 seconds
Rank 6: copyOwnerToAll took 0.000780682 seconds
Rank 7: copyOwnerToAll took 0.000752402 seconds
Average time for copyOwnertoAll is 0.000732275 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 125002
Cell on rank 1 before loadbalancing: 124995
Cell on rank 2 before loadbalancing: 125004
Cell on rank 3 before loadbalancing: 125000
Cell on rank 4 before loadbalancing: 125002
Cell on rank 5 before loadbalancing: 125004
Cell on rank 6 before loadbalancing: 124995
Cell on rank 7 before loadbalancing: 124998
Edge-cut: 44997
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	4686	0	0	0	0	2634	0	
From rank 1 to: 	4686	0	1927	2846	0	659	2063	0	
From rank 2 to: 	0	1931	0	4643	2358	2471	0	0	
From rank 3 to: 	0	2855	4641	0	0	0	0	0	
From rank 4 to: 	0	0	2361	0	0	4601	0	0	
From rank 5 to: 	0	692	2462	0	4598	0	2547	2656	
From rank 6 to: 	2625	2075	0	0	0	2550	0	4739	
From rank 7 to: 	0	0	0	0	0	2690	4738	0	
After loadbalancing process 4 has 131964 cells.
After loadbalancing process 3 has 132496 cells.
After loadbalancing process 6 has 136984 cells.
After loadbalancing process 2 has 136407 cells.
After loadbalancing process 0 has 132322 cells.
After loadbalancing process 7 has 132426 cells.
After loadbalancing process 5 has 137959 cells.
After loadbalancing process 1 has 137176 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	4686	0	0	0	0	2634	0	
Rank 1's ghost cells:	4686	0	1927	2846	0	659	2063	0	
Rank 2's ghost cells:	0	1931	0	4643	2358	2471	0	0	
Rank 3's ghost cells:	0	2855	4641	0	0	0	0	0	
Rank 4's ghost cells:	0	0	2361	0	0	4601	0	0	
Rank 5's ghost cells:	0	692	2462	0	4598	0	2547	2656	
Rank 6's ghost cells:	2625	2075	0	0	0	2550	0	4739	
Rank 7's ghost cells:	0	0	0	0	0	2690	4738	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.7694          0.21525
    2          31.0392         0.577265
    3          21.8254         0.703156
    4          18.0322         0.826203
    5          16.6646         0.924157
    6          14.0991         0.846049
    7          11.4744         0.813842
    8          10.8675         0.947112
    9          10.0682         0.926446
   10          8.33076         0.827433
   11          7.82907         0.939778
   12          7.83876          1.00124
   13          6.66097         0.849748
   14          5.99701         0.900321
   15          6.15707          1.02669
   16           5.6756         0.921801
   17          5.02639         0.885615
   18          5.09697          1.01404
   19          4.81509         0.944698
   20          4.30564         0.894197
   21          4.30625          1.00014
   22          4.06637         0.944295
   23          3.71177         0.912798
   24          3.71072         0.999717
   25          3.47652         0.936885
   26          3.14708         0.905239
   27          3.16041          1.00424
   28          2.96968         0.939651
   29          2.73196         0.919949
   30          2.80115          1.02533
   31          2.80685          1.00204
   32           2.8251           1.0065
   33          3.11008          1.10087
   34          3.11887          1.00283
   35          2.65059         0.849856
   36          2.12673          0.80236
   37          1.61891         0.761222
   38            1.302         0.804245
   39          1.18462         0.909842
   40           1.0602         0.894971
   41         0.934567         0.881503
   42         0.812852         0.869763
   43         0.658791         0.810469
   44         0.570956         0.866671
   45         0.500396         0.876419
   46         0.386851         0.773089
   47            0.326         0.842702
   48         0.282079         0.865272
   49          0.22281         0.789886
   50         0.196633         0.882514
   51         0.166235         0.845409
   52         0.135335          0.81412
   53         0.118351         0.874502
   54        0.0962141         0.812955
   55        0.0790824         0.821942
   56        0.0646433         0.817417
   57        0.0522044         0.807577
   58        0.0416891         0.798575
   59        0.0322266         0.773022
   60        0.0254921         0.791026
   61        0.0188471         0.739333
=== rate=0.855895, T=18.7156, TIT=0.306813, IT=61

 Elapsed time: 18.7156
Rank 0: Matrix-vector product took 0.108227 seconds
Rank 1: Matrix-vector product took 0.111169 seconds
Rank 2: Matrix-vector product took 0.111307 seconds
Rank 3: Matrix-vector product took 0.107547 seconds
Rank 4: Matrix-vector product took 0.107962 seconds
Rank 5: Matrix-vector product took 0.11308 seconds
Rank 6: Matrix-vector product took 0.110923 seconds
Rank 7: Matrix-vector product took 0.107629 seconds
Average time for Matrix-vector product is 0.109731 seconds

Rank 0: copyOwnerToAll took 0.000643431 seconds
Rank 1: copyOwnerToAll took 0.000830541 seconds
Rank 2: copyOwnerToAll took 0.000764511 seconds
Rank 3: copyOwnerToAll took 0.000646321 seconds
Rank 4: copyOwnerToAll took 0.000646921 seconds
Rank 5: copyOwnerToAll took 0.000848161 seconds
Rank 6: copyOwnerToAll took 0.000826971 seconds
Rank 7: copyOwnerToAll took 0.000767831 seconds
Average time for copyOwnertoAll is 0.000746836 seconds
