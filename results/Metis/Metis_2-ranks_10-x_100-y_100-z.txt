METIS partitioner
Cell on rank 0 before loadbalancing: 50210
Cell on rank 1 before loadbalancing: 49790
Edge-cut: 1220
			Rank 0	Rank 1	
From rank 0 to: 	0	1030	
From rank 1 to: 	1030	0	
After loadbalancing process 0 has 51240 cells.
After loadbalancing process 1 has 50820 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1030	
Rank 1's ghost cells:	1030	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.3944          0.20839
    2          19.9298         0.596802
    3          19.3254         0.969669
    4          10.5924         0.548111
    5          5.47491          0.51687
    6          4.26657         0.779295
    7          1.94083         0.454891
    8           1.2379         0.637819
    9         0.924289          0.74666
   10         0.451139         0.488094
   11         0.335492         0.743656
   12         0.203319         0.606033
   13         0.130432         0.641514
   14        0.0804908         0.617108
   15        0.0533947         0.663364
   16        0.0326504         0.611493
   17        0.0199978         0.612482
   18        0.0119085         0.595489
=== rate=0.589677, T=2.02401, TIT=0.112445, IT=18

 Elapsed time: 2.02401
Rank 0: Matrix-vector product took 40 milliseconds
Rank 1: Matrix-vector product took 40 milliseconds
Average time for Matrix-vector product is 40 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 0 milliseconds
Average time for copyOwnertoAll is 0 milliseconds
METIS partitioner
Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	
From rank 0 to: 	0	1040	
From rank 1 to: 	1040	0	
After loadbalancing process 0 has 51100 cells.
After loadbalancing process 1 has 50980 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1040	
Rank 1's ghost cells:	1040	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4733         0.208882
    2          19.9713         0.596635
    3          19.3347         0.968121
    4          10.4336         0.539631
    5          5.33967         0.511777
    6           4.0912         0.766191
    7          2.01747         0.493125
    8          1.23518         0.612243
    9          0.94187         0.762534
   10         0.466833         0.495645
   11         0.357261         0.765286
   12         0.208018         0.582259
   13         0.132457         0.636756
   14        0.0856209         0.646406
   15        0.0528238          0.61695
   16        0.0329169         0.623145
   17         0.020933         0.635934
   18        0.0118633         0.566728
=== rate=0.589553, T=2.32326, TIT=0.12907, IT=18

 Elapsed time: 2.32326
Rank 0: Matrix-vector product took 0.047674 seconds
Rank 1: Matrix-vector product took 0.042916 seconds
Average time for Matrix-vector product is 0.045295 seconds

Rank 0: copyOwnerToAll took 0.00017005 seconds
Rank 1: copyOwnerToAll took 0.00019268 seconds
Average time for copyOwnertoAll is 0.000181365 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	
From rank 0 to: 	0	1040	
From rank 1 to: 	1040	0	
After loadbalancing process 0 has 51100 cells.
After loadbalancing process 1 has 50980 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1040	
Rank 1's ghost cells:	1040	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4733         0.208882
    2          19.9713         0.596635
    3          19.3347         0.968121
    4          10.4336         0.539631
    5          5.33967         0.511777
    6           4.0912         0.766191
    7          2.01747         0.493125
    8          1.23518         0.612243
    9          0.94187         0.762534
   10         0.466833         0.495645
   11         0.357261         0.765286
   12         0.208018         0.582259
   13         0.132457         0.636756
   14        0.0856209         0.646406
   15        0.0528238          0.61695
   16        0.0329169         0.623145
   17         0.020933         0.635934
   18        0.0118633         0.566728
=== rate=0.589553, T=2.02901, TIT=0.112723, IT=18

 Elapsed time: 2.02901
Rank 0: Matrix-vector product took 0.0409785 seconds
Rank 1: Matrix-vector product took 0.0407909 seconds
Average time for Matrix-vector product is 0.0408847 seconds

Rank 0: copyOwnerToAll took 9.696e-05 seconds
Rank 1: copyOwnerToAll took 0.00010033 seconds
Average time for copyOwnertoAll is 9.8645e-05 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	
From rank 0 to: 	0	1040	
From rank 1 to: 	1040	0	
After loadbalancing process 0 has 51100 cells.
After loadbalancing process 1 has 50980 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1040	
Rank 1's ghost cells:	1040	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4733         0.208882
    2          19.9713         0.596635
    3          19.3347         0.968121
    4          10.4336         0.539631
    5          5.33967         0.511777
    6           4.0912         0.766191
    7          2.01747         0.493125
    8          1.23518         0.612243
    9          0.94187         0.762534
   10         0.466833         0.495645
   11         0.357261         0.765286
   12         0.208018         0.582259
   13         0.132457         0.636756
   14        0.0856209         0.646406
   15        0.0528238          0.61695
   16        0.0329169         0.623145
   17         0.020933         0.635934
   18        0.0118633         0.566728
=== rate=0.589553, T=2.03711, TIT=0.113173, IT=18

 Elapsed time: 2.03711
Rank 0: Matrix-vector product took 0.0406997 seconds
Rank 1: Matrix-vector product took 0.0408455 seconds
Average time for Matrix-vector product is 0.0407726 seconds

Rank 0: copyOwnerToAll took 9.058e-05 seconds
Rank 1: copyOwnerToAll took 9.729e-05 seconds
Average time for copyOwnertoAll is 9.3935e-05 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	
From rank 0 to: 	0	1040	
From rank 1 to: 	1040	0	
After loadbalancing process 0 has 51100 cells.
After loadbalancing process 1 has 50980 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1040	
Rank 1's ghost cells:	1040	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4733         0.208882
    2          19.9713         0.596635
    3          19.3347         0.968121
    4          10.4336         0.539631
    5          5.33967         0.511777
    6           4.0912         0.766191
    7          2.01747         0.493125
    8          1.23518         0.612243
    9          0.94187         0.762534
   10         0.466833         0.495645
   11         0.357261         0.765286
   12         0.208018         0.582259
   13         0.132457         0.636756
   14        0.0856209         0.646406
   15        0.0528238          0.61695
   16        0.0329169         0.623145
   17         0.020933         0.635934
   18        0.0118633         0.566728
=== rate=0.589553, T=2.03575, TIT=0.113097, IT=18

 Elapsed time: 2.03575
Rank 0: Matrix-vector product took 0.0412247 seconds
Rank 1: Matrix-vector product took 0.0412687 seconds
Average time for Matrix-vector product is 0.0412467 seconds

Rank 0: copyOwnerToAll took 9.573e-05 seconds
Rank 1: copyOwnerToAll took 8.353e-05 seconds
Average time for copyOwnertoAll is 8.963e-05 seconds
METIS partitioner
Cell on rank 0 before loadbalancing: 50060
Cell on rank 1 before loadbalancing: 49940
Edge-cut: 1300
			Rank 0	Rank 1	
From rank 0 to: 	0	1040	
From rank 1 to: 	1040	0	
After loadbalancing process 0 has 51100 cells.
After loadbalancing process 1 has 50980 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1040	
Rank 1's ghost cells:	1040	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4733         0.208882
    2          19.9713         0.596635
    3          19.3347         0.968121
    4          10.4336         0.539631
    5          5.33967         0.511777
    6           4.0912         0.766191
    7          2.01747         0.493125
    8          1.23518         0.612243
    9          0.94187         0.762534
   10         0.466833         0.495645
   11         0.357261         0.765286
   12         0.208018         0.582259
   13         0.132457         0.636756
   14        0.0856209         0.646406
   15        0.0528238          0.61695
   16        0.0329169         0.623145
   17         0.020933         0.635934
   18        0.0118633         0.566728
=== rate=0.589553, T=2.0535, TIT=0.114083, IT=18

 Elapsed time: 2.0535
Rank 0: Matrix-vector product took 0.0412498 seconds
Rank 1: Matrix-vector product took 0.0412416 seconds
Average time for Matrix-vector product is 0.0412457 seconds

Rank 0: copyOwnerToAll took 0.00010084 seconds
Rank 1: copyOwnerToAll took 0.00011083 seconds
Average time for copyOwnertoAll is 0.000105835 seconds
