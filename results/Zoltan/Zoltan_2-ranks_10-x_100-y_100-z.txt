Zoltan partitioner
Cell on rank 0 before loadbalancing: 50000
Cell on rank 1 before loadbalancing: 50000
			Rank 0	Rank 1	
From rank 0 to: 	0	1037	
From rank 1 to: 	1037	0	
After loadbalancing process 0 has 51037 cells.
After loadbalancing process 1 has 51037 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1037	
Rank 1's ghost cells:	1037	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          33.4383         0.208664
    2          19.9611         0.596952
    3          19.2988         0.966819
    4          10.2997         0.533695
    5          5.27209         0.511871
    6          4.07412         0.772771
    7          2.09456         0.514113
    8          1.31838         0.629432
    9         0.977262         0.741257
   10         0.486118         0.497429
   11         0.392823         0.808083
   12         0.221048         0.562717
   13          0.14031         0.634746
   14        0.0965127         0.687856
   15        0.0571583         0.592237
   16        0.0360085         0.629978
   17        0.0244514         0.679046
   18        0.0132076         0.540157
=== rate=0.593079, T=2.01327, TIT=0.111848, IT=18

 Elapsed time: 2.01327
Rank 0: Matrix-vector product took 40 milliseconds
Rank 1: Matrix-vector product took 40 milliseconds
Average time for Matrix-vector product is 40 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 0 milliseconds
Average time for copyOwnertoAll is 0 milliseconds
Zoltan partitioner
Cell on rank 0 before loadbalancing: 45276
Cell on rank 1 before loadbalancing: 54724
			Rank 0	Rank 1	
From rank 0 to: 	0	1011	
From rank 1 to: 	1011	0	
After loadbalancing process 0 has 46287 cells.
After loadbalancing process 1 has 55735 cells.
			Rank 0	Rank 1	
Rank 0's ghost cells:	0	1011	
Rank 1's ghost cells:	1011	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1           33.307         0.207844
    2          19.8871         0.597085
    3           19.283         0.969623
    4           10.353         0.536896
    5          5.32731         0.514568
    6          4.22245         0.792604
    7           1.9435         0.460277
    8          1.19775         0.616289
    9         0.895566         0.747704
   10         0.432241         0.482645
   11          0.30696         0.710159
   12         0.193309         0.629755
   13         0.125411         0.648756
   14        0.0693154         0.552707
   15        0.0484808         0.699424
   16        0.0303916         0.626879
   17        0.0177786         0.584984
   18         0.010852         0.610399
=== rate=0.586642, T=2.47613, TIT=0.137563, IT=18

 Elapsed time: 2.47613
Rank 0: Matrix-vector product took 0.0372472 seconds
Rank 1: Matrix-vector product took 0.0446345 seconds
Average time for Matrix-vector product is 0.0409408 seconds

Rank 0: copyOwnerToAll took 9.416e-05 seconds
Rank 1: copyOwnerToAll took 8.864e-05 seconds
Average time for copyOwnertoAll is 9.14e-05 seconds
