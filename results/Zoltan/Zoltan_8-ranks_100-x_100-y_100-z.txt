Zoltan partitioner
Cell on rank 0 before loadbalancing: 124512
Cell on rank 1 before loadbalancing: 125494
Cell on rank 2 before loadbalancing: 122273
Cell on rank 3 before loadbalancing: 127722
Cell on rank 4 before loadbalancing: 129454
Cell on rank 5 before loadbalancing: 129027
Cell on rank 6 before loadbalancing: 119955
Cell on rank 7 before loadbalancing: 121563
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2598	2641	114	2214	427	5	0	
From rank 1 to: 	2614	0	349	2245	408	0	1957	0	
From rank 2 to: 	2614	349	0	3017	127	1869	35	556	
From rank 3 to: 	114	2283	2994	0	0	0	346	2267	
From rank 4 to: 	2209	416	123	0	0	2679	2736	325	
From rank 5 to: 	408	0	1871	0	2677	0	78	2150	
From rank 6 to: 	8	1936	35	346	2744	78	0	2675	
From rank 7 to: 	0	0	558	2264	325	2150	2674	0	
After loadbalancing process 0 has 132511 cells.
After loadbalancing process 3 has 135726 cells.
After loadbalancing process 1 has 133067 cells.
After loadbalancing process 7 has 129534 cells.
After loadbalancing process 4 has 137942 cells.
After loadbalancing process 6 has 127777 cells.
After loadbalancing process 5 has 136211 cells.
After loadbalancing process 2 has 130840 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2598	2641	114	2214	427	5	0	
Rank 1's ghost cells:	2614	0	349	2245	408	0	1957	0	
Rank 2's ghost cells:	2614	349	0	3017	127	1869	35	556	
Rank 3's ghost cells:	114	2283	2994	0	0	0	346	2267	
Rank 4's ghost cells:	2209	416	123	0	0	2679	2736	325	
Rank 5's ghost cells:	408	0	1871	0	2677	0	78	2150	
Rank 6's ghost cells:	8	1936	35	346	2744	78	0	2675	
Rank 7's ghost cells:	0	0	558	2264	325	2150	2674	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.3892         0.213728
    2          30.7375         0.575725
    3          21.7266         0.706845
    4          18.1883         0.837143
    5          17.2006         0.945694
    6          14.4154         0.838077
    7          11.7405         0.814445
    8          11.3196         0.964146
    9          10.1589          0.89746
   10          8.53593         0.840244
   11          8.46525          0.99172
   12          7.83612          0.92568
   13          6.54341         0.835032
   14          6.50666         0.994385
   15          6.35801         0.977153
   16          5.35753         0.842643
   17          5.18802          0.96836
   18          5.28378          1.01846
   19          4.60965         0.872414
   20          4.44709         0.964736
   21          4.40408         0.990329
   22          3.90626         0.886963
   23          3.81697         0.977142
   24          3.74876         0.982131
   25          3.37174         0.899426
   26          3.35078         0.993786
   27          3.27162         0.976376
   28          2.92315         0.893487
   29          2.92914          1.00205
   30          2.90813         0.992827
   31          2.75927         0.948815
   32          2.96635          1.07505
   33          3.11655          1.05063
   34          2.98339         0.957275
   35          2.75398         0.923103
   36          2.23068         0.809983
   37          1.80457          0.80898
   38          1.67658         0.929072
   39          1.45603          0.86845
   40          1.19172         0.818476
   41          1.07407         0.901271
   42          0.96684         0.900169
   43         0.799998         0.827436
   44         0.669112         0.836392
   45         0.581538         0.869119
   46         0.495377          0.85184
   47         0.407187         0.821973
   48         0.343454         0.843479
   49         0.286921           0.8354
   50         0.238208         0.830222
   51         0.205678          0.86344
   52         0.168449         0.818991
   53         0.143863         0.854048
   54         0.118397         0.822981
   55        0.0982357         0.829716
   56        0.0813617          0.82823
   57        0.0640883         0.787696
   58        0.0543914         0.848694
   59        0.0417596         0.767761
   60        0.0337143         0.807342
   61        0.0274433         0.813998
   62        0.0201627         0.734703
=== rate=0.85898, T=18.9113, TIT=0.305021, IT=62

 Elapsed time: 18.9113
Rank 0: Matrix-vector product took 106 milliseconds
Rank 1: Matrix-vector product took 106 milliseconds
Rank 2: Matrix-vector product took 104 milliseconds
Rank 3: Matrix-vector product took 109 milliseconds
Rank 4: Matrix-vector product took 110 milliseconds
Rank 5: Matrix-vector product took 109 milliseconds
Rank 6: Matrix-vector product took 103 milliseconds
Rank 7: Matrix-vector product took 103 milliseconds
Average time for Matrix-vector product is 106 milliseconds

Rank 0: copyOwnerToAll took 0 milliseconds
Rank 1: copyOwnerToAll took 4 milliseconds
Rank 2: copyOwnerToAll took 1 milliseconds
Rank 3: copyOwnerToAll took 1 milliseconds
Rank 4: copyOwnerToAll took 0 milliseconds
Rank 5: copyOwnerToAll took 1 milliseconds
Rank 6: copyOwnerToAll took 7 milliseconds
Rank 7: copyOwnerToAll took 7 milliseconds
Average time for copyOwnertoAll is 2 milliseconds
Zoltan partitioner
Cell on rank 0 before loadbalancing: 123589
Cell on rank 1 before loadbalancing: 126006
Cell on rank 2 before loadbalancing: 130531
Cell on rank 3 before loadbalancing: 126192
Cell on rank 4 before loadbalancing: 102536
Cell on rank 5 before loadbalancing: 136035
Cell on rank 6 before loadbalancing: 119691
Cell on rank 7 before loadbalancing: 135420
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2565	1917	1	0	0	2257	467	
From rank 1 to: 	2567	0	464	2471	0	0	127	2212	
From rank 2 to: 	1898	467	0	2670	0	2228	460	0	
From rank 3 to: 	1	2476	2673	0	1887	637	141	277	
From rank 4 to: 	0	0	0	1941	0	2709	9	2119	
From rank 5 to: 	0	0	2228	639	2703	0	3316	96	
From rank 6 to: 	2223	117	483	141	9	3315	0	2857	
From rank 7 to: 	459	2181	0	277	2119	96	2835	0	
After loadbalancing process 1 has 133847 cells.
After loadbalancing process 4 has 109314 cells.
After loadbalancing process 0 has 130796 cells.
After loadbalancing process 2 has 138254 cells.
After loadbalancing process 3 has 134284 cells.
After loadbalancing process 7 has 143387 cells.
After loadbalancing process 5 has 145017 cells.
After loadbalancing process 6 has 128836 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2565	1917	1	0	0	2257	467	
Rank 1's ghost cells:	2567	0	464	2471	0	0	127	2212	
Rank 2's ghost cells:	1898	467	0	2670	0	2228	460	0	
Rank 3's ghost cells:	1	2476	2673	0	1887	637	141	277	
Rank 4's ghost cells:	0	0	0	1941	0	2709	9	2119	
Rank 5's ghost cells:	0	0	2228	639	2703	0	3316	96	
Rank 6's ghost cells:	2223	117	483	141	9	3315	0	2857	
Rank 7's ghost cells:	459	2181	0	277	2119	96	2835	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.5151         0.214232
    2          30.7973         0.575488
    3          21.7758         0.707067
    4          18.2545         0.838294
    5          17.2679         0.945952
    6          14.3892         0.833296
    7          11.6901         0.812422
    8          11.3145         0.967867
    9          10.1839         0.900078
   10          8.53898         0.838475
   11          8.45946         0.990688
   12          7.90513         0.934472
   13          6.62232         0.837724
   14          6.60438         0.997292
   15          6.41422         0.971206
   16          5.40774         0.843086
   17          5.29445         0.979051
   18          5.35397          1.01124
   19          4.63882         0.866426
   20           4.5003         0.970139
   21          4.46891         0.993026
   22          3.96189         0.886545
   23          3.89355         0.982749
   24          3.78776         0.972831
   25          3.40256         0.898303
   26          3.40591          1.00099
   27          3.25767         0.956474
   28          2.92163         0.896846
   29          2.93918          1.00601
   30          2.88963          0.98314
   31          2.72434         0.942801
   32          2.92534          1.07378
   33          3.13749          1.07252
   34          3.02109         0.962899
   35          2.76704         0.915908
   36          2.22698         0.804824
   37          1.77077         0.795144
   38           1.6422         0.927394
   39           1.4435         0.879003
   40          1.19255          0.82615
   41          1.07677         0.902912
   42         0.959624         0.891209
   43         0.816313          0.85066
   44         0.691393          0.84697
   45         0.587178         0.849268
   46         0.493795         0.840963
   47         0.405185         0.820554
   48         0.344614          0.85051
   49         0.284839         0.826546
   50         0.239487         0.840778
   51         0.206192         0.860976
   52         0.172309         0.835672
   53         0.145466         0.844216
   54         0.121023         0.831969
   55         0.100389         0.829503
   56        0.0839156         0.835903
   57        0.0669522         0.797851
   58        0.0557824         0.833168
   59        0.0441238         0.790999
   60        0.0360046          0.81599
   61        0.0289888         0.805141
   62        0.0223319         0.770364
=== rate=0.860397, T=20.048, TIT=0.323354, IT=62

 Elapsed time: 20.048
Rank 0: Matrix-vector product took 0.10668 seconds
Rank 1: Matrix-vector product took 0.108443 seconds
Rank 2: Matrix-vector product took 0.113046 seconds
Rank 3: Matrix-vector product took 0.109259 seconds
Rank 4: Matrix-vector product took 0.0889654 seconds
Rank 5: Matrix-vector product took 0.118076 seconds
Rank 6: Matrix-vector product took 0.104426 seconds
Rank 7: Matrix-vector product took 0.116587 seconds
Average time for Matrix-vector product is 0.108185 seconds

Rank 0: copyOwnerToAll took 0.00051392 seconds
Rank 1: copyOwnerToAll took 0.00054428 seconds
Rank 2: copyOwnerToAll took 0.0005331 seconds
Rank 3: copyOwnerToAll took 0.00057293 seconds
Rank 4: copyOwnerToAll took 0.00050897 seconds
Rank 5: copyOwnerToAll took 0.00060499 seconds
Rank 6: copyOwnerToAll took 0.00063646 seconds
Rank 7: copyOwnerToAll took 0.00059386 seconds
Average time for copyOwnertoAll is 0.000563564 seconds
