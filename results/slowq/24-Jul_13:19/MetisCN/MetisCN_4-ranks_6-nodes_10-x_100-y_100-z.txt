Number of cores/ranks per node is: 4
METIS partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 4197
Cell on rank 1 before loadbalancing: 4180
Cell on rank 2 before loadbalancing: 4170
Cell on rank 3 before loadbalancing: 4170
Cell on rank 4 before loadbalancing: 4180
Cell on rank 5 before loadbalancing: 4170
Cell on rank 6 before loadbalancing: 4200
Cell on rank 7 before loadbalancing: 4160
Cell on rank 8 before loadbalancing: 4190
Cell on rank 9 before loadbalancing: 4197
Cell on rank 10 before loadbalancing: 4128
Cell on rank 11 before loadbalancing: 4208
Cell on rank 12 before loadbalancing: 4160
Cell on rank 13 before loadbalancing: 4190
Cell on rank 14 before loadbalancing: 4150
Cell on rank 15 before loadbalancing: 4200
Cell on rank 16 before loadbalancing: 4130
Cell on rank 17 before loadbalancing: 4130
Cell on rank 18 before loadbalancing: 4150
Cell on rank 19 before loadbalancing: 4140
Cell on rank 20 before loadbalancing: 4130
Cell on rank 21 before loadbalancing: 4150
Cell on rank 22 before loadbalancing: 4160
Cell on rank 23 before loadbalancing: 4160
Edge-cut: 10023
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	0	250	90	0	0	0	0	0	160	0	130	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	0	0	240	0	0	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	250	240	0	120	0	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	90	0	120	0	130	120	0	0	0	0	0	270	0	0	40	90	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	140	0	220	0	0	0	0	0	0	0	0	0	240	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	160	130	120	220	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	0	0	140	220	290	70	0	0	0	0	0	0	0	0	0	0	0	130	0	
From rank 7 to: 	0	0	0	0	0	0	140	0	160	0	0	0	0	0	0	0	0	0	0	0	0	110	140	0	
From rank 8 to: 	0	0	0	0	0	0	220	160	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	160	0	0	0	0	0	290	0	0	0	90	141	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	70	0	0	90	0	270	0	0	110	0	0	0	0	0	0	0	200	40	
From rank 11 to: 	120	0	0	270	0	0	0	0	0	151	267	0	0	0	100	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	250	50	0	120	0	0	0	230	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	0	260	0	310	110	160	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	40	0	0	0	0	0	0	110	100	50	300	0	100	0	0	0	0	110	0	0	170	
From rank 15 to: 	0	0	0	90	230	0	0	0	0	0	0	0	0	110	100	0	130	190	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	120	160	0	130	0	280	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	190	280	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	270	0	120	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	270	0	330	90	0	70	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	230	0	100	0	0	0	0	330	0	0	0	140	
From rank 21 to: 	0	0	0	0	0	0	0	110	0	0	0	0	0	0	0	0	0	0	120	90	0	0	100	140	
From rank 22 to: 	0	0	0	0	0	0	130	150	0	0	200	0	0	0	0	0	0	0	0	0	0	110	0	230	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	50	0	0	0	170	0	0	0	0	70	130	140	230	0	

Edge-cut for node partition: 4655

loadb
After loadbalancing process 20 has 4580 cells.
After loadbalancing process 19 has 4570 cells.
After loadbalancing process 23 has 4800 cells.
After loadbalancing process 9 has 4540 cells.
After loadbalancing process 4 has 4878 cells.
After loadbalancing process 2 has 4820 cells.
After loadbalancing process 13 has 4710 cells.
After loadbalancing process 22 has 4780 cells.
After loadbalancing process 14 has 4980 cells.
After loadbalancing process 21 has 4910 cells.
After loadbalancing process 10 has 4900 cells.
After loadbalancing process 11 has 4930 cells.
After loadbalancing process 8 has 4810 cells.
After loadbalancing process 15 has 4950 cells.
After loadbalancing process 3 has 4600 cells.
After loadbalancing process 7 has 5050 cells.
After loadbalancing process 1 has 5030 cells.
After loadbalancing process 6 has 5130 cells.
After loadbalancing process 0 has 5030 cells.
After loadbalancing process 5 has 5116 cells.
After loadbalancing process 16 has 4827 cells.
After loadbalancing process 12 has 4908 cells.
After loadbalancing process 17 has 5050 cells.
After loadbalancing process 18 has 4710 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	0	0	0	0	270	40	90	0	0	0	0	0	0	0	0	90	0	0	0	0	120	130	120	
Rank 1's ghost cells:	0	0	160	0	0	0	310	110	260	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	160	0	280	0	0	0	130	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	280	0	0	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	141	0	0	0	0	0	0	90	0	0	0	160	290	0	0	0	0	0	0	
Rank 5's ghost cells:	270	0	0	0	151	0	100	0	0	0	0	0	267	0	0	0	120	0	0	0	0	0	0	0	
Rank 6's ghost cells:	40	300	0	0	0	100	0	100	50	0	0	110	110	0	0	170	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	90	110	130	190	0	0	100	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	
Rank 8's ghost cells:	0	250	120	0	0	0	50	0	0	0	0	230	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	0	0	270	0	0	120	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	270	0	330	0	90	0	70	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	100	0	230	0	330	0	0	0	0	140	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	90	270	110	0	0	0	0	0	0	0	200	40	0	70	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	120	90	0	0	0	100	140	0	0	110	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	200	110	0	230	0	130	150	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	170	0	0	0	70	130	50	140	230	0	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	90	0	0	0	160	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	0	
Rank 17's ghost cells:	0	0	0	0	290	0	0	0	0	0	0	0	70	0	130	0	0	0	140	220	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	110	140	0	0	140	0	160	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	160	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	160	
Rank 21's ghost cells:	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	0	0	240	0	0	130	
Rank 22's ghost cells:	140	0	0	0	0	0	0	240	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	220	
Rank 23's ghost cells:	120	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	130	220	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.4798         0.233883
    2           22.753         0.607075
    3          21.2241         0.932801
    4           18.295         0.861992
    5          8.92853         0.488032
    6          5.02836         0.563179
    7          3.61256         0.718438
    8          2.27439         0.629578
    9          1.15699         0.508703
   10         0.777366         0.671887
   11         0.576791         0.741981
   12         0.284254          0.49282
   13           0.2143         0.753904
   14         0.145842         0.680548
   15         0.076411         0.523931
   16        0.0550125         0.719955
   17        0.0361991         0.658017
   18        0.0207662         0.573666
   19         0.014757         0.710624
=== rate=0.613182, T=0.164358, TIT=0.00865043, IT=19

 Elapsed time: 0.164358
Rank 0: Matrix-vector product took 0.00295151 seconds
Rank 1: Matrix-vector product took 0.00295539 seconds
Rank 2: Matrix-vector product took 0.00284433 seconds
Rank 3: Matrix-vector product took 0.00269465 seconds
Rank 4: Matrix-vector product took 0.00287667 seconds
Rank 5: Matrix-vector product took 0.00299625 seconds
Rank 6: Matrix-vector product took 0.00301042 seconds
Rank 7: Matrix-vector product took 0.00296482 seconds
Rank 8: Matrix-vector product took 0.00281832 seconds
Rank 9: Matrix-vector product took 0.00266848 seconds
Rank 10: Matrix-vector product took 0.0028719 seconds
Rank 11: Matrix-vector product took 0.00289052 seconds
Rank 12: Matrix-vector product took 0.00287813 seconds
Rank 13: Matrix-vector product took 0.00277191 seconds
Rank 14: Matrix-vector product took 0.00292105 seconds
Rank 15: Matrix-vector product took 0.00290995 seconds
Rank 16: Matrix-vector product took 0.00282994 seconds
Rank 17: Matrix-vector product took 0.00296029 seconds
Rank 18: Matrix-vector product took 0.00277169 seconds
Rank 19: Matrix-vector product took 0.00269429 seconds
Rank 20: Matrix-vector product took 0.00269782 seconds
Rank 21: Matrix-vector product took 0.00287878 seconds
Rank 22: Matrix-vector product took 0.00281613 seconds
Rank 23: Matrix-vector product took 0.00281273 seconds
Average time for Matrix-vector product is 0.00285358 seconds

Rank 0: copyOwnerToAll took 0.000326244 seconds
Rank 1: copyOwnerToAll took 0.000350361 seconds
Rank 2: copyOwnerToAll took 0.000323744 seconds
Rank 3: copyOwnerToAll took 0.000347035 seconds
Rank 4: copyOwnerToAll took 0.000345111 seconds
Rank 5: copyOwnerToAll took 0.000329599 seconds
Rank 6: copyOwnerToAll took 0.000344528 seconds
Rank 7: copyOwnerToAll took 0.00029566 seconds
Rank 8: copyOwnerToAll took 0.000415712 seconds
Rank 9: copyOwnerToAll took 0.000260109 seconds
Rank 10: copyOwnerToAll took 0.000277654 seconds
Rank 11: copyOwnerToAll took 0.000411748 seconds
Rank 12: copyOwnerToAll took 0.00037548 seconds
Rank 13: copyOwnerToAll took 0.000244838 seconds
Rank 14: copyOwnerToAll took 0.000248665 seconds
Rank 15: copyOwnerToAll took 0.000374377 seconds
Rank 16: copyOwnerToAll took 0.000400659 seconds
Rank 17: copyOwnerToAll took 0.00035402 seconds
Rank 18: copyOwnerToAll took 0.000323085 seconds
Rank 19: copyOwnerToAll took 4.6603e-05 seconds
Rank 20: copyOwnerToAll took 4.0707e-05 seconds
Rank 21: copyOwnerToAll took 0.000250421 seconds
Rank 22: copyOwnerToAll took 0.000371494 seconds
Rank 23: copyOwnerToAll took 0.000239778 seconds
Average time for copyOwnertoAll is 0.000304068 seconds
