Number of cores/ranks per node is: 4
Scotch partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 4980
Cell on rank 1 before loadbalancing: 5049
Cell on rank 2 before loadbalancing: 5020
Cell on rank 3 before loadbalancing: 4951
Cell on rank 4 before loadbalancing: 4950
Cell on rank 5 before loadbalancing: 5020
Cell on rank 6 before loadbalancing: 4980
Cell on rank 7 before loadbalancing: 5030
Cell on rank 8 before loadbalancing: 5050
Cell on rank 9 before loadbalancing: 5020
Cell on rank 10 before loadbalancing: 5000
Cell on rank 11 before loadbalancing: 5050
Cell on rank 12 before loadbalancing: 4950
Cell on rank 13 before loadbalancing: 4954
Cell on rank 14 before loadbalancing: 4966
Cell on rank 15 before loadbalancing: 5050
Cell on rank 16 before loadbalancing: 4950
Cell on rank 17 before loadbalancing: 5000
Cell on rank 18 before loadbalancing: 5020
Cell on rank 19 before loadbalancing: 5010
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	140	0	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	140	0	73	140	0	0	0	160	120	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	73	0	167	140	0	0	0	230	0	0	116	0	0	104	0	40	0	0	0	
From rank 3 to: 	280	150	170	0	230	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	140	230	0	0	0	0	0	0	0	0	0	0	230	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	0	0	160	0	0	180	0	0	0	0	0	0	0	0	150	90	
From rank 6 to: 	0	0	0	0	0	160	0	170	0	100	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	160	0	0	0	0	170	0	90	160	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	110	230	0	0	0	0	100	0	250	0	0	0	0	0	0	150	0	0	50	
From rank 9 to: 	0	0	0	0	0	180	100	160	250	0	0	0	0	0	0	0	0	0	0	150	
From rank 10 to: 	0	0	0	0	0	0	0	0	0	0	0	224	190	56	0	200	0	0	0	0	
From rank 11 to: 	0	0	116	0	0	0	0	0	0	0	224	0	0	90	180	40	240	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	190	0	0	260	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	50	90	260	0	300	0	0	0	0	0	
From rank 14 to: 	0	0	104	0	230	0	0	0	0	0	0	180	0	300	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	200	40	0	0	0	0	160	210	0	60	
From rank 16 to: 	0	0	40	0	0	0	0	0	150	0	0	240	0	0	0	160	0	0	0	260	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	0	0	220	40	
From rank 18 to: 	0	0	0	0	0	150	0	0	0	0	0	0	0	0	0	0	0	220	0	250	
From rank 19 to: 	0	0	0	0	0	90	0	0	50	150	0	0	0	0	0	70	260	40	250	0	

Edge-cut for node partition: 3209

loadb
After loadbalancing process 0 has 5400 cells.
After loadbalancing process 17 has 5410 cells.
After loadbalancing process 5 has 5400 cells.
After loadbalancing process 16 has 5600 cells.
After loadbalancing process 18 has 5610 cells.
After loadbalancing process 14 has 5640 cells.
After loadbalancing process 3 has 5550 cells.
After loadbalancing process 6 has 5654 cells.
After loadbalancing process 19 has 5860 cells.
After loadbalancing process 1 has 5682 cells.
After loadbalancing process 13 has 5470 cells.
After loadbalancing process 4 has 5890 cells.
After loadbalancing process 2 has 5781 cells.
After loadbalancing process 12 has 5940 cells.
After loadbalancing process 7 has 5780 cells.
After loadbalancing process 8 has 5670 cells.
After loadbalancing process 9 has 5940 cells.
After loadbalancing process 15 has 5920 cells.
After loadbalancing process 10 has 5720 cells.
After loadbalancing process 11 has 5800 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	140	280	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	140	0	140	0	73	0	0	0	0	0	0	0	120	0	0	0	0	0	160	0	
Rank 2's ghost cells:	280	150	0	230	170	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	0	230	0	140	0	0	230	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	73	167	140	0	0	0	104	0	116	0	40	230	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	0	0	260	0	190	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	260	0	300	50	90	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	230	104	0	300	0	0	180	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	190	56	0	0	224	200	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	116	0	90	180	224	0	40	240	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	200	40	0	160	0	210	0	60	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	40	0	0	0	0	240	160	0	150	0	0	260	0	0	0	0	
Rank 12's ghost cells:	0	110	0	0	230	0	0	0	0	0	0	150	0	0	0	50	0	0	100	250	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	210	0	0	0	220	40	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	220	0	250	150	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	70	260	50	40	250	0	90	0	0	150	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	150	90	0	160	0	180	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	160	0	170	100	
Rank 18's ghost cells:	0	160	0	0	0	0	0	0	0	0	0	0	90	0	0	0	0	170	0	160	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	250	0	0	150	180	100	160	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          36.6898         0.228954
    2          22.1822         0.604587
    3          20.8247         0.938804
    4          17.3286         0.832115
    5          8.45138         0.487713
    6          4.89357         0.579026
    7          3.33236         0.680968
    8          2.25511         0.676731
    9          1.19717         0.530871
   10         0.745871         0.623026
   11           0.5529         0.741281
   12         0.263374          0.47635
   13         0.184739         0.701431
   14         0.131041         0.709332
   15         0.069459         0.530055
   16        0.0474395         0.682985
   17        0.0314385         0.662708
   18        0.0175383         0.557859
   19        0.0128195         0.730943
=== rate=0.608656, T=0.198396, TIT=0.0104419, IT=19

 Elapsed time: 0.198396
Rank 0: Matrix-vector product took 0.00318546 seconds
Rank 1: Matrix-vector product took 0.00335962 seconds
Rank 2: Matrix-vector product took 0.00339416 seconds
Rank 3: Matrix-vector product took 0.00327486 seconds
Rank 4: Matrix-vector product took 0.00347477 seconds
Rank 5: Matrix-vector product took 0.00319256 seconds
Rank 6: Matrix-vector product took 0.00333116 seconds
Rank 7: Matrix-vector product took 0.00339787 seconds
Rank 8: Matrix-vector product took 0.00333986 seconds
Rank 9: Matrix-vector product took 0.00349042 seconds
Rank 10: Matrix-vector product took 0.00337256 seconds
Rank 11: Matrix-vector product took 0.00341868 seconds
Rank 12: Matrix-vector product took 0.00348863 seconds
Rank 13: Matrix-vector product took 0.00321308 seconds
Rank 14: Matrix-vector product took 0.00332507 seconds
Rank 15: Matrix-vector product took 0.00348374 seconds
Rank 16: Matrix-vector product took 0.00329529 seconds
Rank 17: Matrix-vector product took 0.00318149 seconds
Rank 18: Matrix-vector product took 0.00328513 seconds
Rank 19: Matrix-vector product took 0.00344749 seconds
Average time for Matrix-vector product is 0.0033476 seconds

Rank 0: copyOwnerToAll took 3.8391e-05 seconds
Rank 1: copyOwnerToAll took 0.000367814 seconds
Rank 2: copyOwnerToAll took 0.000390442 seconds
Rank 3: copyOwnerToAll took 0.00036953 seconds
Rank 4: copyOwnerToAll took 0.00034337 seconds
Rank 5: copyOwnerToAll took 0.000241631 seconds
Rank 6: copyOwnerToAll took 0.000339805 seconds
Rank 7: copyOwnerToAll took 0.000334221 seconds
Rank 8: copyOwnerToAll took 0.000344234 seconds
Rank 9: copyOwnerToAll took 0.000341416 seconds
Rank 10: copyOwnerToAll took 0.000339197 seconds
Rank 11: copyOwnerToAll took 0.000340012 seconds
Rank 12: copyOwnerToAll took 0.000329879 seconds
Rank 13: copyOwnerToAll took 0.000243243 seconds
Rank 14: copyOwnerToAll took 0.000168478 seconds
Rank 15: copyOwnerToAll took 0.000333988 seconds
Rank 16: copyOwnerToAll took 0.000326642 seconds
Rank 17: copyOwnerToAll took 5.0543e-05 seconds
Rank 18: copyOwnerToAll took 0.000284463 seconds
Rank 19: copyOwnerToAll took 0.000341865 seconds
Average time for copyOwnertoAll is 0.000293458 seconds
