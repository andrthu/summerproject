Number of cores/ranks per node is: 4
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 41670
Cell on rank 1 before loadbalancing: 41669
Cell on rank 2 before loadbalancing: 41669
Cell on rank 3 before loadbalancing: 41692
Cell on rank 4 before loadbalancing: 41683
Cell on rank 5 before loadbalancing: 41688
Cell on rank 6 before loadbalancing: 41680
Cell on rank 7 before loadbalancing: 41677
Cell on rank 8 before loadbalancing: 41679
Cell on rank 9 before loadbalancing: 41678
Cell on rank 10 before loadbalancing: 41679
Cell on rank 11 before loadbalancing: 41679
Cell on rank 12 before loadbalancing: 41697
Cell on rank 13 before loadbalancing: 41697
Cell on rank 14 before loadbalancing: 41697
Cell on rank 15 before loadbalancing: 42078
Cell on rank 16 before loadbalancing: 41316
Cell on rank 17 before loadbalancing: 41697
Cell on rank 18 before loadbalancing: 41562
Cell on rank 19 before loadbalancing: 41561
Cell on rank 20 before loadbalancing: 41715
Cell on rank 21 before loadbalancing: 41612
Cell on rank 22 before loadbalancing: 41613
Cell on rank 23 before loadbalancing: 41612
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	1583	607	0	0	0	1485	0	3	0	0	0	0	0	0	0	0	0	934	0	0	0	0	123	
From rank 1 to: 	1592	0	1936	680	0	596	220	0	0	0	0	0	0	0	0	0	0	0	715	473	0	0	0	237	
From rank 2 to: 	610	1930	0	540	560	734	340	0	1514	0	0	339	0	20	0	0	0	24	74	174	0	0	0	387	
From rank 3 to: 	0	674	534	0	1401	1028	0	0	0	0	0	91	0	0	0	623	430	328	0	259	0	0	0	1	
From rank 4 to: 	0	0	566	1388	0	1489	0	0	112	697	1066	354	75	0	0	0	397	351	0	0	0	0	0	0	
From rank 5 to: 	0	616	748	1016	1487	0	0	0	88	185	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	1478	204	332	0	0	0	0	1304	1010	0	0	0	0	0	0	0	0	0	127	0	0	82	0	753	
From rank 7 to: 	0	0	0	0	0	0	1302	0	1332	0	0	425	0	0	0	0	0	0	0	0	0	855	0	0	
From rank 8 to: 	3	0	1513	0	107	88	1014	1357	0	703	0	1807	0	0	0	0	0	0	0	0	0	64	0	89	
From rank 9 to: 	0	0	0	0	698	185	0	0	702	0	1867	991	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	1066	0	0	0	0	1895	0	807	1160	0	0	0	0	44	0	0	0	0	0	0	
From rank 11 to: 	0	0	336	91	368	0	0	422	1820	984	802	0	662	204	543	0	0	161	0	0	0	161	0	197	
From rank 12 to: 	0	0	0	0	75	0	0	0	0	0	1162	670	0	1779	272	0	605	542	0	0	0	0	0	0	
From rank 13 to: 	0	0	20	0	0	0	0	0	0	0	0	205	1778	0	1613	0	484	1184	0	0	0	0	0	296	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	549	272	1620	0	0	0	416	0	0	0	1009	756	604	
From rank 15 to: 	0	0	0	623	0	0	0	0	0	0	0	0	0	0	0	0	1478	767	0	1082	0	0	0	0	
From rank 16 to: 	0	0	0	476	408	0	0	0	0	0	0	0	593	504	0	1469	0	1661	0	0	0	0	0	0	
From rank 17 to: 	0	0	26	324	351	0	0	0	0	0	44	155	554	1176	416	758	1659	0	0	747	103	0	504	308	
From rank 18 to: 	942	746	74	0	0	0	135	0	0	0	0	0	0	0	0	0	0	0	0	1216	1085	0	0	1193	
From rank 19 to: 	0	473	168	251	0	0	0	0	0	0	0	0	0	0	0	1082	0	758	1210	0	1157	0	271	395	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	103	1084	1161	0	0	1089	260	
From rank 21 to: 	0	0	0	0	0	0	81	850	70	0	0	151	0	0	1001	0	0	0	0	0	0	0	1295	998	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	753	0	0	510	0	278	1089	1306	0	1442	
From rank 23 to: 	118	220	381	1	0	0	754	0	86	0	0	196	0	295	625	0	0	312	1207	395	280	990	1442	0	

Edge-cut for node partition: 36874

loadb
After loadbalancing process 1 has 46028 cells.
After loadbalancing process 4 has 45412 cells.
After loadbalancing process 10 has 46427 cells.
After loadbalancing process 6 has 46991 cells.
After loadbalancing process 5 has 46058 cells.
After loadbalancing process 19 has 45828 cells.
After loadbalancing process 14 has 45591 cells.
After loadbalancing process 18 has 48178 cells.
After loadbalancing process 8 has 46802 cells.
After loadbalancing process 7 has 48914 cells.
After loadbalancing process 9 has 47277 cells.
After loadbalancing process 11 has 48822 cells.
After loadbalancing process 16 has 48915 cells.
After loadbalancing process 23 has 46923 cells.
After loadbalancing process 2 has 46953 cells.
After loadbalancing process 0 has 46405 cells.
After loadbalancing process 21 has 46651 cells.
After loadbalancing process 20 has 46121 cells.
After loadbalancing process 15 has 48424 cells.
After loadbalancing process 22 has 48430 cells.
After loadbalancing process 13 has 46970 cells.
After loadbalancing process 17 has 47061 cells.
After loadbalancing process 3 has 47326 cells.
After loadbalancing process 12 has 48118 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	0	934	0	0	0	0	123	0	0	0	0	1583	1485	0	3	607	0	0	0	0	0	0	0	
Rank 1's ghost cells:	0	0	0	1082	0	0	0	0	0	0	1478	767	0	0	0	0	0	623	0	0	0	0	0	0	
Rank 2's ghost cells:	942	0	0	1216	1085	0	0	1193	0	0	0	0	746	135	0	0	74	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	1082	1210	0	1157	0	271	395	0	0	0	758	473	0	0	0	168	251	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	1084	1161	0	0	1089	260	0	0	0	103	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	0	0	1295	998	0	0	0	0	0	81	850	70	0	0	0	0	0	0	151	1001	
Rank 6's ghost cells:	0	0	0	278	1089	1306	0	1442	0	0	0	510	0	0	0	0	0	0	0	0	0	0	0	753	
Rank 7's ghost cells:	118	0	1207	395	280	990	1442	0	0	295	0	312	220	754	0	86	381	1	0	0	0	0	196	625	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	1779	605	542	0	0	0	0	0	0	75	0	0	1162	670	272	
Rank 9's ghost cells:	0	0	0	0	0	0	0	296	1778	0	484	1184	0	0	0	0	20	0	0	0	0	0	205	1613	
Rank 10's ghost cells:	0	1469	0	0	0	0	0	0	593	504	0	1661	0	0	0	0	0	476	408	0	0	0	0	0	
Rank 11's ghost cells:	0	758	0	747	103	0	504	308	554	1176	1659	0	0	0	0	0	26	324	351	0	0	44	155	416	
Rank 12's ghost cells:	1592	0	715	473	0	0	0	237	0	0	0	0	0	220	0	0	1936	680	0	596	0	0	0	0	
Rank 13's ghost cells:	1478	0	127	0	0	82	0	753	0	0	0	0	204	0	1304	1010	332	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	855	0	0	0	0	0	0	0	1302	0	1332	0	0	0	0	0	0	425	0	
Rank 15's ghost cells:	3	0	0	0	0	64	0	89	0	0	0	0	0	1014	1357	0	1513	0	107	88	703	0	1807	0	
Rank 16's ghost cells:	610	0	74	174	0	0	0	387	0	20	0	24	1930	340	0	1514	0	540	560	734	0	0	339	0	
Rank 17's ghost cells:	0	623	0	259	0	0	0	1	0	0	430	328	674	0	0	0	534	0	1401	1028	0	0	91	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	75	0	397	351	0	0	0	112	566	1388	0	1489	697	1066	354	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	616	0	0	88	748	1016	1487	0	185	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	702	0	0	698	185	0	1867	991	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	1160	0	0	44	0	0	0	0	0	0	1066	0	1895	0	807	0	
Rank 22's ghost cells:	0	0	0	0	0	161	0	197	662	204	0	161	0	0	422	1820	336	91	368	0	984	802	0	543	
Rank 23's ghost cells:	0	0	0	0	0	1009	756	604	272	1620	0	416	0	0	0	0	0	0	0	0	0	0	549	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          55.4355          0.22192
    2          32.3402         0.583385
    3           23.286         0.720033
    4          20.6534         0.886945
    5          18.3453         0.888246
    6          14.5019         0.790498
    7          13.1849          0.90918
    8          12.4485         0.944147
    9          10.1718         0.817116
   10            9.655          0.94919
   11          9.25254         0.958316
   12          7.81348         0.844469
   13          7.60623         0.973475
   14          7.31679         0.961947
   15           6.2435         0.853311
   16          6.12987         0.981801
   17          5.97002         0.973922
   18          5.26435         0.881798
   19          5.15295         0.978839
   20           4.9939         0.969134
   21           4.5012         0.901341
   22           4.4994         0.999599
   23          4.26493          0.94789
   24           3.9085         0.916427
   25          3.91964          1.00285
   26           3.6496         0.931106
   27          3.37876         0.925791
   28          3.39147          1.00376
   29          3.12818         0.922366
   30          2.95044         0.943181
   31          2.99056           1.0136
   32          2.87053         0.959864
   33          2.90805          1.01307
   34          3.12551          1.07478
   35          3.10468         0.993334
   36          3.03035          0.97606
   37          2.72487         0.899194
   38          2.14376         0.786738
   39          1.75506         0.818683
   40            1.475         0.840429
   41           1.2483         0.846304
   42          1.14564         0.917757
   43         0.998402         0.871482
   44         0.832813         0.834146
   45         0.738863          0.88719
   46          0.63606         0.860864
   47         0.527772         0.829751
   48         0.447569         0.848036
   49          0.37083         0.828541
   50         0.308457         0.831801
   51         0.268015         0.868892
   52         0.225838         0.842631
   53         0.192699         0.853262
   54         0.163703         0.849527
   55         0.134717         0.822936
   56         0.115658         0.858521
   57          0.09563         0.826836
   58        0.0776119         0.811586
   59        0.0662319         0.853373
   60        0.0542847         0.819615
   61        0.0442567          0.81527
   62        0.0358208         0.809387
   63        0.0280117         0.781996
   64        0.0221919         0.792237
=== rate=0.864364, T=5.19325, TIT=0.0811446, IT=64

 Elapsed time: 5.19325
Rank 0: Matrix-vector product took 0.0283244 seconds
Rank 1: Matrix-vector product took 0.0280097 seconds
Rank 2: Matrix-vector product took 0.0285145 seconds
Rank 3: Matrix-vector product took 0.0287009 seconds
Rank 4: Matrix-vector product took 0.0277477 seconds
Rank 5: Matrix-vector product took 0.0279993 seconds
Rank 6: Matrix-vector product took 0.0284914 seconds
Rank 7: Matrix-vector product took 0.0296537 seconds
Rank 8: Matrix-vector product took 0.0284405 seconds
Rank 9: Matrix-vector product took 0.0286565 seconds
Rank 10: Matrix-vector product took 0.0281962 seconds
Rank 11: Matrix-vector product took 0.0297673 seconds
Rank 12: Matrix-vector product took 0.0292547 seconds
Rank 13: Matrix-vector product took 0.0285615 seconds
Rank 14: Matrix-vector product took 0.0277749 seconds
Rank 15: Matrix-vector product took 0.0293552 seconds
Rank 16: Matrix-vector product took 0.0298559 seconds
Rank 17: Matrix-vector product took 0.0286087 seconds
Rank 18: Matrix-vector product took 0.0292151 seconds
Rank 19: Matrix-vector product took 0.0278325 seconds
Rank 20: Matrix-vector product took 0.0279821 seconds
Rank 21: Matrix-vector product took 0.0283453 seconds
Rank 22: Matrix-vector product took 0.0294483 seconds
Rank 23: Matrix-vector product took 0.0284606 seconds
Average time for Matrix-vector product is 0.0286332 seconds

Rank 0: copyOwnerToAll took 0.00120653 seconds
Rank 1: copyOwnerToAll took 0.00110122 seconds
Rank 2: copyOwnerToAll took 0.00122571 seconds
Rank 3: copyOwnerToAll took 0.00130688 seconds
Rank 4: copyOwnerToAll took 0.00105833 seconds
Rank 5: copyOwnerToAll took 0.00109831 seconds
Rank 6: copyOwnerToAll took 0.00106765 seconds
Rank 7: copyOwnerToAll took 0.00118609 seconds
Rank 8: copyOwnerToAll took 0.00119253 seconds
Rank 9: copyOwnerToAll took 0.00116834 seconds
Rank 10: copyOwnerToAll took 0.00119957 seconds
Rank 11: copyOwnerToAll took 0.00121074 seconds
Rank 12: copyOwnerToAll took 0.00134517 seconds
Rank 13: copyOwnerToAll took 0.00137255 seconds
Rank 14: copyOwnerToAll took 0.00134222 seconds
Rank 15: copyOwnerToAll took 0.00135522 seconds
Rank 16: copyOwnerToAll took 0.00139564 seconds
Rank 17: copyOwnerToAll took 0.0012111 seconds
Rank 18: copyOwnerToAll took 0.00122699 seconds
Rank 19: copyOwnerToAll took 0.00122356 seconds
Rank 20: copyOwnerToAll took 0.00121148 seconds
Rank 21: copyOwnerToAll took 0.00108635 seconds
Rank 22: copyOwnerToAll took 0.00137263 seconds
Rank 23: copyOwnerToAll took 0.00110596 seconds
Average time for copyOwnertoAll is 0.00121962 seconds
