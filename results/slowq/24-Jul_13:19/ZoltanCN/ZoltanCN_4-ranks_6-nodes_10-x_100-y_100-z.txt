Number of cores/ranks per node is: 4
Zoltan partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 4175
Cell on rank 1 before loadbalancing: 4175
Cell on rank 2 before loadbalancing: 4066
Cell on rank 3 before loadbalancing: 4191
Cell on rank 4 before loadbalancing: 4191
Cell on rank 5 before loadbalancing: 4191
Cell on rank 6 before loadbalancing: 4164
Cell on rank 7 before loadbalancing: 4165
Cell on rank 8 before loadbalancing: 4165
Cell on rank 9 before loadbalancing: 4166
Cell on rank 10 before loadbalancing: 4165
Cell on rank 11 before loadbalancing: 4165
Cell on rank 12 before loadbalancing: 4161
Cell on rank 13 before loadbalancing: 4160
Cell on rank 14 before loadbalancing: 4161
Cell on rank 15 before loadbalancing: 4176
Cell on rank 16 before loadbalancing: 4176
Cell on rank 17 before loadbalancing: 4176
Cell on rank 18 before loadbalancing: 4096
Cell on rank 19 before loadbalancing: 4187
Cell on rank 20 before loadbalancing: 4141
Cell on rank 21 before loadbalancing: 4193
Cell on rank 22 before loadbalancing: 4198
Cell on rank 23 before loadbalancing: 4196
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
From rank 0 to: 	0	326	48	0	0	167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	334	0	313	0	0	275	0	0	0	0	0	0	0	94	18	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	45	323	0	0	0	0	0	0	0	0	0	0	0	180	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	0	0	0	285	174	0	130	54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	294	0	245	0	0	197	0	0	147	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	157	270	0	164	238	0	0	0	0	0	0	60	0	0	120	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	0	0	305	165	162	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	130	0	0	303	0	154	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	48	197	0	162	153	0	32	116	134	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	162	0	24	0	284	0	0	0	0	0	0	0	0	0	170	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	116	285	0	231	0	0	0	0	0	0	0	0	126	26	0	80	
From rank 11 to: 	0	0	0	0	147	50	0	0	140	0	236	0	0	0	190	0	0	0	0	0	0	107	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	209	192	179	0	151	0	0	0	92	0	0	
From rank 13 to: 	0	94	180	0	0	0	0	0	0	0	0	0	202	0	116	150	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	22	0	0	0	120	0	0	0	0	0	190	196	118	0	0	0	0	0	0	0	150	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	169	150	0	0	250	76	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	0	193	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	153	0	0	66	193	0	0	0	0	60	240	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	290	290	0	0	153	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	290	0	0	0	61	226	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	170	126	0	0	0	0	0	0	0	290	0	0	0	0	162	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	23	107	92	0	140	0	0	60	0	0	0	0	190	237	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	240	0	54	0	190	0	185	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	81	0	0	0	0	0	0	0	153	219	161	236	183	0	

Edge-cut for node partition: 4065

loadb
After loadbalancing process 12 has 4829 cells.
After loadbalancing process 0 has 4716 cells.
After loadbalancing process 13 has 4764 cells.
After loadbalancing process 8 has 4796 cells.
After loadbalancing process 9 has 4752 cells.
After loadbalancing process 14 has 4889 cells.
After loadbalancing process 17 has 4619 cells.
After loadbalancing process 23 has 4821 cells.
After loadbalancing process 2 has 4614 cells.
After loadbalancing process 4 has 4834 cells.
After loadbalancing process 21 has 4902 cells.
After loadbalancing process 1 has 5209 cells.
After loadbalancing process 3 has 5200 cells.
After loadbalancing process 19 has 4867 cells.
After loadbalancing process 15 has 5229 cells.
After loadbalancing process 5 has 5074 cells.
After loadbalancing process 10 has 5007 cells.
After loadbalancing process 20 has 4984 cells.
After loadbalancing process 11 has 4806 cells.
After loadbalancing process 18 has 4888 cells.
After loadbalancing process 6 has 5035 cells.
After loadbalancing process 16 has 5029 cells.
After loadbalancing process 7 has 5042 cells.
After loadbalancing process 22 has 4957 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	
Rank 0's ghost cells:	0	326	48	167	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	334	0	313	275	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	94	18	0	
Rank 2's ghost cells:	45	323	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	180	0	0	
Rank 3's ghost cells:	157	270	0	0	164	238	60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	120	0	
Rank 4's ghost cells:	0	0	0	174	0	285	0	0	0	130	54	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	245	294	0	147	0	0	0	197	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	50	0	147	0	107	0	0	140	0	0	0	0	0	236	0	0	0	0	0	190	0	
Rank 7's ghost cells:	0	0	0	0	0	0	107	0	0	0	0	0	0	0	0	237	23	0	60	190	92	0	140	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	305	165	162	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	130	0	0	0	303	0	154	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	48	197	134	0	162	153	0	32	0	0	0	0	116	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	162	0	24	0	0	0	170	0	284	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	290	290	153	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	290	0	0	226	0	0	0	61	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	170	290	0	0	162	126	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	236	0	0	0	0	153	219	161	0	81	0	0	183	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	231	26	0	0	116	285	0	0	126	80	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	193	0	0	0	0	250	
Rank 18's ghost cells:	0	0	0	0	0	0	0	60	0	0	0	0	0	0	0	0	0	193	0	240	153	0	0	66	
Rank 19's ghost cells:	0	0	0	0	0	0	0	190	0	0	0	0	0	54	0	185	0	0	240	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	92	0	0	0	0	0	0	0	0	0	0	151	0	0	209	192	179	
Rank 21's ghost cells:	0	94	180	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	202	0	116	150	
Rank 22's ghost cells:	0	22	0	120	0	0	190	150	0	0	0	0	0	0	0	0	0	0	0	0	196	118	0	0	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	250	76	0	169	150	0	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.3852         0.233293
    2          22.8662         0.611638
    3          21.4492         0.938029
    4          19.0775         0.889428
    5          9.39459         0.492444
    6          5.46682         0.581912
    7          3.98901         0.729676
    8          2.58635         0.648368
    9          1.36329         0.527111
   10         0.934026         0.685126
   11         0.629979         0.674477
   12         0.341498         0.542078
   13         0.257871         0.755118
   14         0.160711         0.623223
   15        0.0956513         0.595176
   16         0.066309         0.693236
   17        0.0413873         0.624158
   18        0.0263957         0.637773
   19        0.0176965         0.670432
   20       0.00994911         0.562208
=== rate=0.616097, T=0.184402, TIT=0.00922008, IT=20

 Elapsed time: 0.184402
Rank 0: Matrix-vector product took 0.00276387 seconds
Rank 1: Matrix-vector product took 0.00304564 seconds
Rank 2: Matrix-vector product took 0.00271814 seconds
Rank 3: Matrix-vector product took 0.00303497 seconds
Rank 4: Matrix-vector product took 0.0028377 seconds
Rank 5: Matrix-vector product took 0.00297744 seconds
Rank 6: Matrix-vector product took 0.00294243 seconds
Rank 7: Matrix-vector product took 0.0029676 seconds
Rank 8: Matrix-vector product took 0.00281393 seconds
Rank 9: Matrix-vector product took 0.00278264 seconds
Rank 10: Matrix-vector product took 0.00294372 seconds
Rank 11: Matrix-vector product took 0.00282424 seconds
Rank 12: Matrix-vector product took 0.00283772 seconds
Rank 13: Matrix-vector product took 0.00277779 seconds
Rank 14: Matrix-vector product took 0.0028701 seconds
Rank 15: Matrix-vector product took 0.00306627 seconds
Rank 16: Matrix-vector product took 0.00294511 seconds
Rank 17: Matrix-vector product took 0.00286271 seconds
Rank 18: Matrix-vector product took 0.00286931 seconds
Rank 19: Matrix-vector product took 0.00285593 seconds
Rank 20: Matrix-vector product took 0.0029215 seconds
Rank 21: Matrix-vector product took 0.00287498 seconds
Rank 22: Matrix-vector product took 0.00291936 seconds
Rank 23: Matrix-vector product took 0.00282813 seconds
Average time for Matrix-vector product is 0.00288672 seconds

Rank 0: copyOwnerToAll took 5.0638e-05 seconds
Rank 1: copyOwnerToAll took 0.000242104 seconds
Rank 2: copyOwnerToAll took 0.000242748 seconds
Rank 3: copyOwnerToAll took 0.00029904 seconds
Rank 4: copyOwnerToAll took 0.000171795 seconds
Rank 5: copyOwnerToAll took 0.000236348 seconds
Rank 6: copyOwnerToAll took 0.000284801 seconds
Rank 7: copyOwnerToAll took 0.00029969 seconds
Rank 8: copyOwnerToAll took 5.2139e-05 seconds
Rank 9: copyOwnerToAll took 0.000208075 seconds
Rank 10: copyOwnerToAll took 0.000374273 seconds
Rank 11: copyOwnerToAll took 0.000305897 seconds
Rank 12: copyOwnerToAll took 5.0263e-05 seconds
Rank 13: copyOwnerToAll took 0.000231852 seconds
Rank 14: copyOwnerToAll took 0.000305138 seconds
Rank 15: copyOwnerToAll took 0.000306022 seconds
Rank 16: copyOwnerToAll took 0.000407744 seconds
Rank 17: copyOwnerToAll took 0.000252924 seconds
Rank 18: copyOwnerToAll took 0.000377808 seconds
Rank 19: copyOwnerToAll took 0.000399983 seconds
Rank 20: copyOwnerToAll took 0.000383172 seconds
Rank 21: copyOwnerToAll took 0.000213462 seconds
Rank 22: copyOwnerToAll took 0.000377844 seconds
Rank 23: copyOwnerToAll took 0.00030601 seconds
Average time for copyOwnertoAll is 0.000265824 seconds
