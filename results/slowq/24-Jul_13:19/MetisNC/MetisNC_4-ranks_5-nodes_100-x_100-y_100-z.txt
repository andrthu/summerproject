Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 200004
Cell on rank 1 before loadbalancing: 200000
Cell on rank 2 before loadbalancing: 200002
Cell on rank 3 before loadbalancing: 199996
Cell on rank 4 before loadbalancing: 199998
Edge-cut: 30646
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	2149	180	2244	1860	411	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	2149	0	1964	305	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	169	1964	0	2090	0	0	0	0	1101	1248	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	2249	288	2090	0	0	1597	0	0	265	510	0	0	0	0	0	0	0	0	0	1770	
From rank 4 to: 	1875	0	0	0	0	2494	201	1702	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	411	0	0	1589	2508	0	1752	291	0	0	0	0	0	0	0	0	0	0	1557	898	
From rank 6 to: 	0	0	0	0	199	1753	0	2730	0	0	0	0	1436	1260	0	0	0	0	221	0	
From rank 7 to: 	0	0	0	0	1693	295	2728	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	1089	265	0	0	0	0	0	1183	877	1403	0	0	0	0	364	0	0	765	
From rank 9 to: 	0	0	1251	522	0	0	0	0	1185	0	927	650	0	0	0	0	397	0	0	1132	
From rank 10 to: 	0	0	0	0	0	0	0	0	901	959	0	2294	0	0	0	0	1090	2026	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	1403	650	2315	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	1455	0	0	0	0	0	0	981	1424	0	136	0	1645	0	
From rank 13 to: 	0	0	0	0	0	0	1268	0	0	0	0	0	980	0	192	1570	107	0	1248	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	1423	180	0	962	566	1041	3	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	1562	969	0	652	956	35	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	364	397	1056	0	136	107	573	647	0	2663	1856	1882	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	2025	0	0	0	1041	958	2670	0	0	0	
From rank 18 to: 	0	0	0	0	0	1564	219	0	0	0	0	0	1664	1245	3	35	1878	0	0	2537	
From rank 19 to: 	0	0	0	1753	0	900	0	0	764	1132	0	0	0	0	0	0	1882	0	2540	0	
loadb
After loadbalancing process 11 has 54362 cells.
After loadbalancing process 1 has 54421 cells.
After loadbalancing process 2 has 56571 cells.
After loadbalancing process 7 has 54719 cells.
After loadbalancing process 4 has 56271 cells.
After loadbalancing process 15 has 54168 cells.
After loadbalancing process 6 has 57601 cells.
After loadbalancing process 0 has 56849 cells.
After loadbalancing process 10 has 57271 cells.
After loadbalancing process 12 has 55646 cells.
After loadbalancing process 14 has 54172 cells.
After loadbalancing process 3 has 58766 cells.
After loadbalancing process 13 has 55365 cells.
After loadbalancing process 8 has 55949 cells.
After loadbalancing process 9 has 56068 cells.
After loadbalancing process 19 has 58972 cells.
After loadbalancing process 5 has 59002 cells.
After loadbalancing process 18 has 59147 cells.
After loadbalancing process 17 has 56690 cells.
After loadbalancing process 16 has 59680 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	2149	180	2244	1860	411	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	2149	0	1964	305	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	169	1964	0	2090	0	0	0	0	1101	1248	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	2249	288	2090	0	0	1597	0	0	265	510	0	0	0	0	0	0	0	0	0	1770	
Rank 4's ghost cells:	1875	0	0	0	0	2494	201	1702	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	411	0	0	1589	2508	0	1752	291	0	0	0	0	0	0	0	0	0	0	1557	898	
Rank 6's ghost cells:	0	0	0	0	199	1753	0	2730	0	0	0	0	1436	1260	0	0	0	0	221	0	
Rank 7's ghost cells:	0	0	0	0	1693	295	2728	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	1089	265	0	0	0	0	0	1183	877	1403	0	0	0	0	364	0	0	765	
Rank 9's ghost cells:	0	0	1251	522	0	0	0	0	1185	0	927	650	0	0	0	0	397	0	0	1132	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	901	959	0	2294	0	0	0	0	1090	2026	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	1403	650	2315	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	1455	0	0	0	0	0	0	981	1424	0	136	0	1645	0	
Rank 13's ghost cells:	0	0	0	0	0	0	1268	0	0	0	0	0	980	0	192	1570	107	0	1248	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	1423	180	0	962	566	1041	3	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	1562	969	0	652	956	35	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	364	397	1056	0	136	107	573	647	0	2663	1856	1882	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	2025	0	0	0	1041	958	2670	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	1564	219	0	0	0	0	0	1664	1245	3	35	1878	0	0	2537	
Rank 19's ghost cells:	0	0	0	1753	0	900	0	0	764	1132	0	0	0	0	0	0	1882	0	2540	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1           55.201         0.220981
    2          32.1352         0.582149
    3          22.6969         0.706294
    4          19.2908         0.849932
    5          17.4993         0.907131
    6          14.2249         0.812883
    7          12.1175         0.851851
    8          11.7093         0.966313
    9          10.0356         0.857062
   10          8.65803         0.862733
   11          8.64139         0.998078
   12          7.89558         0.913693
   13          6.76085         0.856284
   14          6.78438          1.00348
   15          6.44952         0.950643
   16          5.64464         0.875203
   17           5.6032         0.992658
   18          5.37191         0.958723
   19          4.77742         0.889333
   20          4.72601          0.98924
   21          4.45934         0.943574
   22          4.10553         0.920658
   23           4.1033         0.999459
   24          3.81949         0.930832
   25          3.55627         0.931086
   26          3.54419         0.996601
   27          3.27881         0.925124
   28           3.0571         0.932381
   29           3.0861          1.00948
   30          2.90773         0.942204
   31          2.80351         0.964157
   32           2.8938          1.03221
   33          2.90946          1.00541
   34          3.02858          1.04094
   35          3.14277           1.0377
   36          2.80253         0.891736
   37          2.29562         0.819125
   38          1.76718         0.769803
   39          1.33262         0.754098
   40          1.13457         0.851383
   41          1.03702         0.914021
   42         0.928497         0.895348
   43         0.810014         0.872393
   44         0.680178         0.839712
   45         0.578862         0.851044
   46         0.504765         0.871995
   47         0.416601         0.825338
   48         0.346831         0.832525
   49         0.288188         0.830917
   50         0.235942          0.81871
   51         0.203126         0.860914
   52         0.174192         0.857555
   53         0.145336         0.834348
   54         0.122423         0.842341
   55         0.102928         0.840756
   56         0.084564         0.821586
   57        0.0686371         0.811659
   58        0.0573031         0.834871
   59        0.0461477         0.805325
   60        0.0366991         0.795254
   61         0.029534         0.804762
   62        0.0228955         0.775223
=== rate=0.860743, T=6.03462, TIT=0.0973326, IT=62

 Elapsed time: 6.03462
Rank 0: Matrix-vector product took 0.0345406 seconds
Rank 1: Matrix-vector product took 0.033051 seconds
Rank 2: Matrix-vector product took 0.0343889 seconds
Rank 3: Matrix-vector product took 0.035632 seconds
Rank 4: Matrix-vector product took 0.0341607 seconds
Rank 5: Matrix-vector product took 0.0357794 seconds
Rank 6: Matrix-vector product took 0.034926 seconds
Rank 7: Matrix-vector product took 0.0332242 seconds
Rank 8: Matrix-vector product took 0.034052 seconds
Rank 9: Matrix-vector product took 0.0341846 seconds
Rank 10: Matrix-vector product took 0.0347147 seconds
Rank 11: Matrix-vector product took 0.033019 seconds
Rank 12: Matrix-vector product took 0.0339263 seconds
Rank 13: Matrix-vector product took 0.0337635 seconds
Rank 14: Matrix-vector product took 0.033026 seconds
Rank 15: Matrix-vector product took 0.0330672 seconds
Rank 16: Matrix-vector product took 0.0361666 seconds
Rank 17: Matrix-vector product took 0.0344598 seconds
Rank 18: Matrix-vector product took 0.035923 seconds
Rank 19: Matrix-vector product took 0.0357972 seconds
Average time for Matrix-vector product is 0.0343901 seconds

Rank 0: copyOwnerToAll took 0.00124011 seconds
Rank 1: copyOwnerToAll took 0.000273355 seconds
Rank 2: copyOwnerToAll took 0.000946267 seconds
Rank 3: copyOwnerToAll took 0.00139058 seconds
Rank 4: copyOwnerToAll took 0.000889364 seconds
Rank 5: copyOwnerToAll took 0.00138521 seconds
Rank 6: copyOwnerToAll took 0.00126718 seconds
Rank 7: copyOwnerToAll took 0.000351628 seconds
Rank 8: copyOwnerToAll took 0.0012271 seconds
Rank 9: copyOwnerToAll took 0.00136747 seconds
Rank 10: copyOwnerToAll took 0.00136688 seconds
Rank 11: copyOwnerToAll took 0.000272308 seconds
Rank 12: copyOwnerToAll took 0.0013677 seconds
Rank 13: copyOwnerToAll took 0.00137293 seconds
Rank 14: copyOwnerToAll took 0.00139309 seconds
Rank 15: copyOwnerToAll took 0.00141328 seconds
Rank 16: copyOwnerToAll took 0.00149954 seconds
Rank 17: copyOwnerToAll took 0.00145649 seconds
Rank 18: copyOwnerToAll took 0.00150416 seconds
Rank 19: copyOwnerToAll took 0.00149075 seconds
Average time for copyOwnertoAll is 0.00117377 seconds
