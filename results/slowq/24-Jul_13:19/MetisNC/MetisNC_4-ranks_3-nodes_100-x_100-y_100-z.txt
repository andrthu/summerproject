Number of cores/ranks per node is: 4
Metis Node cell partitioner
 Cell on rank 0 before loadbalancing: 333331
Cell on rank 1 before loadbalancing: 333344
Cell on rank 2 before loadbalancing: 333325
Edge-cut: 19019
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	
From rank 0 to: 	0	3666	0	3466	2342	207	0	0	0	0	0	0	
From rank 1 to: 	3666	0	0	0	88	2041	0	0	0	0	0	0	
From rank 2 to: 	0	0	0	3035	0	0	0	0	210	2670	0	0	
From rank 3 to: 	3466	0	3033	0	477	0	0	0	1952	110	0	0	
From rank 4 to: 	2344	108	0	483	0	2940	2385	324	2965	0	0	0	
From rank 5 to: 	208	2026	0	0	2928	0	177	2743	0	0	0	0	
From rank 6 to: 	0	0	0	0	2390	172	0	3252	390	0	3234	0	
From rank 7 to: 	0	0	0	0	324	2727	3265	0	0	0	0	0	
From rank 8 to: 	0	0	209	1990	2964	0	346	0	0	3240	2178	643	
From rank 9 to: 	0	0	2670	106	0	0	0	0	3238	0	0	2151	
From rank 10 to: 	0	0	0	0	0	0	3236	0	2178	0	0	3276	
From rank 11 to: 	0	0	0	0	0	0	0	0	651	2150	3275	0	
loadb
After loadbalancing process 7 has 89661 cells.
After loadbalancing process 11 has 89411 cells.
After loadbalancing process 2 has 89250 cells.
After loadbalancing process 9 has 91496 cells.
After loadbalancing process 6 has 92759 cells.
After loadbalancing process 1 has 89124 cells.
After loadbalancing process 10 has 92003 cells.
After loadbalancing process 5 has 91426 cells.
After loadbalancing process 0 has 93020 cells.
After loadbalancing process 3 has 92366 cells.
After loadbalancing process 4 has 94883 cells.
After loadbalancing process 8 has 94916 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	
Rank 0's ghost cells:	0	3666	0	3466	2342	207	0	0	0	0	0	0	
Rank 1's ghost cells:	3666	0	0	0	88	2041	0	0	0	0	0	0	
Rank 2's ghost cells:	0	0	0	3035	0	0	0	0	210	2670	0	0	
Rank 3's ghost cells:	3466	0	3033	0	477	0	0	0	1952	110	0	0	
Rank 4's ghost cells:	2344	108	0	483	0	2940	2385	324	2965	0	0	0	
Rank 5's ghost cells:	208	2026	0	0	2928	0	177	2743	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	2390	172	0	3252	390	0	3234	0	
Rank 7's ghost cells:	0	0	0	0	324	2727	3265	0	0	0	0	0	
Rank 8's ghost cells:	0	0	209	1990	2964	0	346	0	0	3240	2178	643	
Rank 9's ghost cells:	0	0	2670	106	0	0	0	0	3238	0	0	2151	
Rank 10's ghost cells:	0	0	0	0	0	0	3236	0	2178	0	0	3276	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	651	2150	3275	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          54.3461         0.217558
    2          31.4701         0.579068
    3          22.1246         0.703037
    4          18.4017         0.831731
    5          16.8618         0.916314
    6          14.0678         0.834298
    7          11.6069         0.825072
    8          11.0876         0.955259
    9          10.0771          0.90886
   10          8.36895         0.830494
   11          7.98806         0.954488
   12          7.93114         0.992874
   13          6.68393         0.842745
   14          6.14406         0.919229
   15          6.35922          1.03502
   16          5.79079         0.910613
   17          5.19913         0.897827
   18          5.24747           1.0093
   19          4.84673         0.923631
   20          4.40385         0.908623
   21          4.40589          1.00046
   22          4.10132         0.930871
   23          3.81109         0.929236
   24          3.78799         0.993938
   25          3.47557         0.917524
   26          3.28326         0.944666
   27          3.29646          1.00402
   28          3.01139         0.913523
   29          2.86419         0.951119
   30          2.86797          1.00132
   31          2.74897         0.958507
   32           2.8359          1.03162
   33          3.05985          1.07897
   34          3.06431          1.00146
   35          2.94604         0.961405
   36           2.5101         0.852024
   37          1.87325         0.746287
   38          1.45079         0.774478
   39          1.18785         0.818763
   40          1.03022         0.867296
   41         0.971856         0.943346
   42         0.838836         0.863128
   43         0.680617         0.811383
   44         0.599239         0.880435
   45         0.522609         0.872121
   46         0.423836            0.811
   47         0.353306         0.833591
   48         0.294282          0.83294
   49         0.242443         0.823845
   50         0.207377         0.855366
   51         0.173622         0.837225
   52           0.1455         0.838032
   53         0.126976         0.872688
   54         0.102326         0.805865
   55        0.0862297         0.842698
   56        0.0725075         0.840865
   57        0.0576279         0.794785
   58        0.0487223         0.845464
   59        0.0391825         0.804201
   60        0.0314386         0.802365
   61        0.0255427         0.812462
   62        0.0201882         0.790372
=== rate=0.858998, T=9.55154, TIT=0.154057, IT=62

 Elapsed time: 9.55154
Rank 0: Matrix-vector product took 0.0567585 seconds
Rank 1: Matrix-vector product took 0.0544696 seconds
Rank 2: Matrix-vector product took 0.0544852 seconds
Rank 3: Matrix-vector product took 0.056401 seconds
Rank 4: Matrix-vector product took 0.0578761 seconds
Rank 5: Matrix-vector product took 0.0558589 seconds
Rank 6: Matrix-vector product took 0.0567609 seconds
Rank 7: Matrix-vector product took 0.0547319 seconds
Rank 8: Matrix-vector product took 0.0579918 seconds
Rank 9: Matrix-vector product took 0.0558931 seconds
Rank 10: Matrix-vector product took 0.0561726 seconds
Rank 11: Matrix-vector product took 0.0545394 seconds
Average time for Matrix-vector product is 0.0559949 seconds

Rank 0: copyOwnerToAll took 0.00149317 seconds
Rank 1: copyOwnerToAll took 0.00117581 seconds
Rank 2: copyOwnerToAll took 0.000980856 seconds
Rank 3: copyOwnerToAll took 0.00103667 seconds
Rank 4: copyOwnerToAll took 0.00164409 seconds
Rank 5: copyOwnerToAll took 0.00117994 seconds
Rank 6: copyOwnerToAll took 0.00115432 seconds
Rank 7: copyOwnerToAll took 0.000430958 seconds
Rank 8: copyOwnerToAll took 0.00151056 seconds
Rank 9: copyOwnerToAll took 0.00101623 seconds
Rank 10: copyOwnerToAll took 0.0013249 seconds
Rank 11: copyOwnerToAll took 0.000430713 seconds
Average time for copyOwnertoAll is 0.00111485 seconds
