Number of cores/ranks per node is: 4
Scotch partitioner
Cell on rank 0 before loadbalancing: 12420
Cell on rank 1 before loadbalancing: 12460
Cell on rank 2 before loadbalancing: 12540
Cell on rank 3 before loadbalancing: 12380
Cell on rank 4 before loadbalancing: 12570
Cell on rank 5 before loadbalancing: 12530
Cell on rank 6 before loadbalancing: 12610
Cell on rank 7 before loadbalancing: 12490
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	470	0	0	0	0	250	0	
From rank 1 to: 	470	0	0	520	0	0	230	0	
From rank 2 to: 	0	0	0	470	0	260	0	0	
From rank 3 to: 	0	520	470	0	0	260	20	0	
From rank 4 to: 	0	0	0	0	0	470	0	230	
From rank 5 to: 	0	0	260	260	470	0	180	60	
From rank 6 to: 	250	230	0	10	0	180	0	490	
From rank 7 to: 	0	0	0	0	230	70	490	0	
loadb
After loadbalancing process 4 has 13270 cells.
After loadbalancing process 7 has 13280 cells.
After loadbalancing process 5 has 13760 cells.
After loadbalancing process 0 has 13140 cells.
After loadbalancing process 6 has 13770 cells.
After loadbalancing process 3 has 13650 cells.
After loadbalancing process 2 has 13270 cells.
After loadbalancing process 1 has 13680 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	470	0	0	0	0	250	0	
Rank 1's ghost cells:	470	0	0	520	0	0	230	0	
Rank 2's ghost cells:	0	0	0	470	0	260	0	0	
Rank 3's ghost cells:	0	520	470	0	0	260	20	0	
Rank 4's ghost cells:	0	0	0	0	0	470	0	230	
Rank 5's ghost cells:	0	0	260	260	470	0	180	60	
Rank 6's ghost cells:	250	230	0	10	0	180	0	490	
Rank 7's ghost cells:	0	0	0	0	230	70	490	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          34.8093         0.217219
    2          20.9393         0.601542
    3           19.887         0.949746
    4          13.2804         0.667793
    5          6.68339         0.503253
    6          4.54903         0.680646
    7          2.60664         0.573011
    8          1.70907          0.65566
    9          1.07969         0.631738
   10         0.607523         0.562685
   11         0.454661         0.748385
   12         0.238287         0.524098
   13         0.156837         0.658186
   14        0.0973571         0.620753
   15        0.0619937         0.636766
   16        0.0412649          0.66563
   17        0.0262984         0.637307
   18        0.0162158         0.616608
   19        0.0109893         0.677692
=== rate=0.603742, T=0.435013, TIT=0.0228954, IT=19

 Elapsed time: 0.435013
Rank 0: Matrix-vector product took 0.00784565 seconds
Rank 1: Matrix-vector product took 0.0081658 seconds
Rank 2: Matrix-vector product took 0.0079268 seconds
Rank 3: Matrix-vector product took 0.00814152 seconds
Rank 4: Matrix-vector product took 0.00791382 seconds
Rank 5: Matrix-vector product took 0.00820985 seconds
Rank 6: Matrix-vector product took 0.00821251 seconds
Rank 7: Matrix-vector product took 0.00790388 seconds
Average time for Matrix-vector product is 0.00803998 seconds

Rank 0: copyOwnerToAll took 0.000218299 seconds
Rank 1: copyOwnerToAll took 0.000253959 seconds
Rank 2: copyOwnerToAll took 0.000253076 seconds
Rank 3: copyOwnerToAll took 0.000383022 seconds
Rank 4: copyOwnerToAll took 6.5628e-05 seconds
Rank 5: copyOwnerToAll took 0.000241033 seconds
Rank 6: copyOwnerToAll took 0.000325155 seconds
Rank 7: copyOwnerToAll took 7.8949e-05 seconds
Average time for copyOwnertoAll is 0.00022739 seconds
