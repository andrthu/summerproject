Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 33500
Cell on rank 1 before loadbalancing: 33180
Cell on rank 2 before loadbalancing: 33320
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	
From rank 0 to: 	0	352	0	0	0	0	210	38	0	0	0	0	
From rank 1 to: 	352	0	340	0	0	0	0	202	0	50	0	0	
From rank 2 to: 	0	340	0	330	0	0	0	0	70	210	0	0	
From rank 3 to: 	0	0	330	0	0	0	0	0	240	0	0	0	
From rank 4 to: 	0	0	0	0	0	330	0	220	0	10	0	330	
From rank 5 to: 	0	0	0	0	330	0	220	70	0	0	0	0	
From rank 6 to: 	210	0	0	0	0	230	0	350	0	0	0	0	
From rank 7 to: 	38	202	0	0	220	70	350	0	0	300	0	0	
From rank 8 to: 	0	0	70	240	0	0	0	0	0	330	210	50	
From rank 9 to: 	0	40	210	0	10	0	0	300	330	0	0	240	
From rank 10 to: 	0	0	0	0	0	0	0	0	220	0	0	360	
From rank 11 to: 	0	0	0	0	330	0	0	0	50	240	360	0	
loadb
After loadbalancing process 10 has 8910 cells.
After loadbalancing process 5 has 8850 cells.
After loadbalancing process 3 has 8940 cells.
After loadbalancing process 8 has 9160 cells.
After loadbalancing process 6 has 9080 cells.
After loadbalancing process 2 has 9250 cells.
After loadbalancing process 4 has 9180 cells.
After loadbalancing process 0 has 9055 cells.
After loadbalancing process 11 has 9320 cells.
After loadbalancing process 1 has 9319 cells.
After loadbalancing process 7 has 9550 cells.
After loadbalancing process 9 has 9520 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	
Rank 0's ghost cells:	0	352	0	0	0	0	210	38	0	0	0	0	
Rank 1's ghost cells:	352	0	340	0	0	0	0	202	0	50	0	0	
Rank 2's ghost cells:	0	340	0	330	0	0	0	0	70	210	0	0	
Rank 3's ghost cells:	0	0	330	0	0	0	0	0	240	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	330	0	220	0	10	0	330	
Rank 5's ghost cells:	0	0	0	0	330	0	220	70	0	0	0	0	
Rank 6's ghost cells:	210	0	0	0	0	230	0	350	0	0	0	0	
Rank 7's ghost cells:	38	202	0	0	220	70	350	0	0	300	0	0	
Rank 8's ghost cells:	0	0	70	240	0	0	0	0	0	330	210	50	
Rank 9's ghost cells:	0	40	210	0	10	0	0	300	330	0	0	240	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	220	0	0	360	
Rank 11's ghost cells:	0	0	0	0	330	0	0	0	50	240	360	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          35.2263         0.219821
    2          21.2692         0.603788
    3          20.1232         0.946116
    4          14.3504         0.713129
    5          7.03699         0.490369
    6          4.57451         0.650066
    7          2.78762          0.60938
    8          1.83771         0.659242
    9          1.12175         0.610405
   10         0.658793         0.587291
   11         0.496589         0.753786
   12         0.252769         0.509009
   13         0.170424         0.674229
   14         0.106146         0.622836
   15        0.0634884         0.598123
   16        0.0399558          0.62934
   17        0.0249834         0.625276
   18        0.0153044         0.612583
=== rate=0.597954, T=0.290276, TIT=0.0161264, IT=18

 Elapsed time: 0.290276
Rank 0: Matrix-vector product took 0.00538175 seconds
Rank 1: Matrix-vector product took 0.00553322 seconds
Rank 2: Matrix-vector product took 0.00549344 seconds
Rank 3: Matrix-vector product took 0.00531989 seconds
Rank 4: Matrix-vector product took 0.00544046 seconds
Rank 5: Matrix-vector product took 0.00526073 seconds
Rank 6: Matrix-vector product took 0.00539039 seconds
Rank 7: Matrix-vector product took 0.0056754 seconds
Rank 8: Matrix-vector product took 0.00544111 seconds
Rank 9: Matrix-vector product took 0.00566041 seconds
Rank 10: Matrix-vector product took 0.00530104 seconds
Rank 11: Matrix-vector product took 0.0055377 seconds
Average time for Matrix-vector product is 0.00545296 seconds

Rank 0: copyOwnerToAll took 0.000331861 seconds
Rank 1: copyOwnerToAll took 0.000389916 seconds
Rank 2: copyOwnerToAll took 0.00039138 seconds
Rank 3: copyOwnerToAll took 0.000233753 seconds
Rank 4: copyOwnerToAll took 0.00029921 seconds
Rank 5: copyOwnerToAll took 5.8178e-05 seconds
Rank 6: copyOwnerToAll took 0.000247395 seconds
Rank 7: copyOwnerToAll took 0.000381966 seconds
Rank 8: copyOwnerToAll took 0.000325042 seconds
Rank 9: copyOwnerToAll took 0.000320441 seconds
Rank 10: copyOwnerToAll took 5.7156e-05 seconds
Rank 11: copyOwnerToAll took 0.000221724 seconds
Average time for copyOwnertoAll is 0.000271502 seconds
