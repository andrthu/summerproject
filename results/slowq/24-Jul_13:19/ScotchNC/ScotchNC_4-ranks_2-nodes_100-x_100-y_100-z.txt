Number of cores/ranks per node is: 4
Scotch Node cell partitioner
 Cell on rank 0 before loadbalancing: 497728
Cell on rank 1 before loadbalancing: 502272
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2403	2406	200	0	2196	334	0	
From rank 1 to: 	2400	0	95	2462	2314	352	0	39	
From rank 2 to: 	2405	81	0	2744	0	0	2200	78	
From rank 3 to: 	206	2463	2743	0	114	36	74	2563	
From rank 4 to: 	0	2316	0	114	0	2452	0	2337	
From rank 5 to: 	2196	347	0	37	2453	0	2584	340	
From rank 6 to: 	336	0	2227	78	0	2600	0	2391	
From rank 7 to: 	0	41	78	2562	2336	343	2391	0	
loadb
After loadbalancing process 4 has 132604 cells.
After loadbalancing process 6 has 133171 cells.
After loadbalancing process 1 has 132094 cells.
After loadbalancing process 2 has 132587 cells.
After loadbalancing process 5 has 134643 cells.
After loadbalancing process 7 has 132413 cells.
After loadbalancing process 0 has 131545 cells.
After loadbalancing process 3 has 132410 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2403	2406	200	0	2196	334	0	
Rank 1's ghost cells:	2400	0	95	2462	2314	352	0	39	
Rank 2's ghost cells:	2405	81	0	2744	0	0	2200	78	
Rank 3's ghost cells:	206	2463	2743	0	114	36	74	2563	
Rank 4's ghost cells:	0	2316	0	114	0	2452	0	2337	
Rank 5's ghost cells:	2196	347	0	37	2453	0	2584	340	
Rank 6's ghost cells:	336	0	2227	78	0	2600	0	2391	
Rank 7's ghost cells:	0	41	78	2562	2336	343	2391	0	
=== CGSolver
 Iter          Defect            Rate
    0            249.8
    1          53.2114         0.213016
    2          30.6198         0.575437
    3          21.5487         0.703749
    4          17.7002         0.821408
    5          16.4295         0.928208
    6          14.0982         0.858101
    7          11.3742         0.806786
    8          10.7613         0.946111
    9          10.1453         0.942766
   10          8.38415         0.826405
   11          8.00543         0.954828
   12          7.88519         0.984981
   13           6.5293         0.828045
   14          6.17615         0.945913
   15          6.35563          1.02906
   16          5.43916         0.855802
   17          5.01511         0.922038
   18          5.17975          1.03283
   19          4.60141         0.888347
   20          4.23756         0.920927
   21          4.32949          1.02169
   22          3.93756         0.909473
   23          3.67473          0.93325
   24          3.69588          1.00576
   25          3.36047         0.909248
   26          3.18106         0.946611
   27          3.18966           1.0027
   28          2.87819         0.902349
   29          2.75309         0.956537
   30          2.85136          1.03569
   31           2.7635         0.969185
   32          2.84391           1.0291
   33          3.11141          1.09406
   34          2.94492         0.946493
   35          2.53187          0.85974
   36          2.11053         0.833587
   37          1.71949         0.814719
   38          1.55363          0.90354
   39          1.37138         0.882695
   40          1.10249         0.803923
   41          1.01739         0.922814
   42         0.932557         0.916618
   43         0.707547         0.758718
   44         0.612781         0.866064
   45         0.567408         0.925956
   46          0.43811         0.772125
   47         0.371915         0.848908
   48         0.324923         0.873648
   49         0.254637         0.783684
   50          0.22711         0.891896
   51         0.188646         0.830639
   52          0.15016         0.795989
   53         0.134925         0.898537
   54         0.105358         0.780867
   55        0.0876214         0.831652
   56        0.0739159         0.843583
   57        0.0569259         0.770145
   58        0.0471104         0.827573
   59        0.0368802         0.782847
   60        0.0291944         0.791602
   61        0.0219209         0.750859
=== rate=0.858018, T=13.3318, TIT=0.218554, IT=61

 Elapsed time: 13.3318
Rank 0: Matrix-vector product took 0.0808421 seconds
Rank 1: Matrix-vector product took 0.0812091 seconds
Rank 2: Matrix-vector product took 0.081519 seconds
Rank 3: Matrix-vector product took 0.0812743 seconds
Rank 4: Matrix-vector product took 0.0815575 seconds
Rank 5: Matrix-vector product took 0.0827377 seconds
Rank 6: Matrix-vector product took 0.0818737 seconds
Rank 7: Matrix-vector product took 0.0814204 seconds
Average time for Matrix-vector product is 0.0815542 seconds

Rank 0: copyOwnerToAll took 0.00100073 seconds
Rank 1: copyOwnerToAll took 0.00107223 seconds
Rank 2: copyOwnerToAll took 0.00099032 seconds
Rank 3: copyOwnerToAll took 0.00130525 seconds
Rank 4: copyOwnerToAll took 0.000939608 seconds
Rank 5: copyOwnerToAll took 0.00120903 seconds
Rank 6: copyOwnerToAll took 0.00120497 seconds
Rank 7: copyOwnerToAll took 0.000965078 seconds
Average time for copyOwnertoAll is 0.0010859 seconds
