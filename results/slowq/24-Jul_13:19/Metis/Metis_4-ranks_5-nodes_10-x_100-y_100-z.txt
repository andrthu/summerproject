Number of cores/ranks per node is: 4
METIS partitioner
Cell on rank 0 before loadbalancing: 5024
Cell on rank 1 before loadbalancing: 4990
Cell on rank 2 before loadbalancing: 5050
Cell on rank 3 before loadbalancing: 4979
Cell on rank 4 before loadbalancing: 4951
Cell on rank 5 before loadbalancing: 4996
Cell on rank 6 before loadbalancing: 4990
Cell on rank 7 before loadbalancing: 5012
Cell on rank 8 before loadbalancing: 4980
Cell on rank 9 before loadbalancing: 5040
Cell on rank 10 before loadbalancing: 4980
Cell on rank 11 before loadbalancing: 5008
Cell on rank 12 before loadbalancing: 5020
Cell on rank 13 before loadbalancing: 5000
Cell on rank 14 before loadbalancing: 4990
Cell on rank 15 before loadbalancing: 5020
Cell on rank 16 before loadbalancing: 5000
Cell on rank 17 before loadbalancing: 4970
Cell on rank 18 before loadbalancing: 5020
Cell on rank 19 before loadbalancing: 4980
Edge-cut: 9309
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	287	70	110	0	20	0	317	0	0	0	0	0	0	0	0	160	0	0	0	
From rank 1 to: 	287	0	0	290	0	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	70	0	0	158	147	0	0	0	0	0	0	0	0	0	0	190	100	0	0	0	
From rank 3 to: 	120	280	168	0	265	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	147	273	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	20	170	0	0	0	0	260	183	60	70	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	260	0	0	0	160	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	327	0	0	0	0	193	0	0	293	0	0	40	0	0	0	0	60	90	0	0	
From rank 8 to: 	0	0	0	0	0	60	0	293	0	220	0	157	170	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	70	160	0	220	0	0	0	210	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	0	0	0	150	0	40	210	0	0	80	150	0	
From rank 11 to: 	0	0	0	0	0	0	0	40	147	0	150	0	110	0	110	0	0	347	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	170	210	0	110	0	110	230	0	0	0	0	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	40	0	110	0	300	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	210	120	240	310	0	0	0	0	0	0	
From rank 15 to: 	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	50	210	
From rank 16 to: 	160	0	100	0	0	0	0	60	0	0	0	0	0	0	0	230	0	210	0	150	
From rank 17 to: 	0	0	0	0	0	0	0	93	0	0	80	347	0	0	0	0	220	0	70	180	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	50	0	70	0	310	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	150	190	310	0	
loadb
After loadbalancing process 3 has 5812 cells.
After loadbalancing process 12 has 5850 cells.
After loadbalancing process 4 has 5371 cells.
After loadbalancing process 2 has 5715 cells.
After loadbalancing process 15 has 5700 cells.
After loadbalancing process 19 has 5840 cells.
After loadbalancing process 13 has 5450 cells.
After loadbalancing process 18 has 5600 cells.
After loadbalancing process 16 has 5910 cells.
After loadbalancing process 1 has 5740 cells.
After loadbalancing process 0 has 5988 cells.
After loadbalancing process 6 has 5410 cells.
After loadbalancing process 5 has 5759 cells.
After loadbalancing process 8 has 5880 cells.
After loadbalancing process 10 has 5610 cells.
After loadbalancing process 17 has 5960 cells.
After loadbalancing process 9 has 5700 cells.
After loadbalancing process 14 has 5870 cells.
After loadbalancing process 11 has 5912 cells.
After loadbalancing process 7 has 6015 cells.
			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	287	70	110	0	20	0	317	0	0	0	0	0	0	0	0	160	0	0	0	
Rank 1's ghost cells:	287	0	0	290	0	173	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	70	0	0	158	147	0	0	0	0	0	0	0	0	0	0	190	100	0	0	0	
Rank 3's ghost cells:	120	280	168	0	265	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	147	273	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	20	170	0	0	0	0	260	183	60	70	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	0	260	0	0	0	160	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	327	0	0	0	0	193	0	0	293	0	0	40	0	0	0	0	60	90	0	0	
Rank 8's ghost cells:	0	0	0	0	0	60	0	293	0	220	0	157	170	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	70	160	0	220	0	0	0	210	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	150	0	40	210	0	0	80	150	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	40	147	0	150	0	110	0	110	0	0	347	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	170	210	0	110	0	110	230	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	0	0	40	0	110	0	300	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	210	120	240	310	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	190	0	0	0	0	0	0	0	0	0	0	0	0	0	230	0	50	210	
Rank 16's ghost cells:	160	0	100	0	0	0	0	60	0	0	0	0	0	0	0	230	0	210	0	150	
Rank 17's ghost cells:	0	0	0	0	0	0	0	93	0	0	80	347	0	0	0	0	220	0	70	180	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	150	0	0	0	0	50	0	70	0	310	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	210	150	190	310	0	
=== CGSolver
 Iter          Defect            Rate
    0           160.25
    1          37.2276          0.23231
    2          22.5275          0.60513
    3          21.1036         0.936791
    4           17.839         0.845308
    5          8.75188         0.490603
    6          4.98696         0.569815
    7          3.52888         0.707623
    8          2.28965         0.648831
    9          1.18655         0.518223
   10         0.787058         0.663317
   11         0.582724         0.740382
   12         0.278967          0.47873
   13         0.210141         0.753281
   14         0.146145         0.695462
   15        0.0744933         0.509723
   16        0.0538499         0.722883
   17        0.0345405         0.641421
   18        0.0200932          0.58173
   19         0.014708         0.731987
=== rate=0.613075, T=0.193255, TIT=0.0101713, IT=19

 Elapsed time: 0.193255
Rank 0: Matrix-vector product took 0.00352797 seconds
Rank 1: Matrix-vector product took 0.00337051 seconds
Rank 2: Matrix-vector product took 0.00335348 seconds
Rank 3: Matrix-vector product took 0.00340364 seconds
Rank 4: Matrix-vector product took 0.00317499 seconds
Rank 5: Matrix-vector product took 0.00338653 seconds
Rank 6: Matrix-vector product took 0.00319175 seconds
Rank 7: Matrix-vector product took 0.00353537 seconds
Rank 8: Matrix-vector product took 0.0034709 seconds
Rank 9: Matrix-vector product took 0.00336666 seconds
Rank 10: Matrix-vector product took 0.00329448 seconds
Rank 11: Matrix-vector product took 0.00348664 seconds
Rank 12: Matrix-vector product took 0.00344333 seconds
Rank 13: Matrix-vector product took 0.00321334 seconds
Rank 14: Matrix-vector product took 0.00345161 seconds
Rank 15: Matrix-vector product took 0.00334894 seconds
Rank 16: Matrix-vector product took 0.0034869 seconds
Rank 17: Matrix-vector product took 0.00350825 seconds
Rank 18: Matrix-vector product took 0.00329595 seconds
Rank 19: Matrix-vector product took 0.00344502 seconds
Average time for Matrix-vector product is 0.00338781 seconds

Rank 0: copyOwnerToAll took 0.000406127 seconds
Rank 1: copyOwnerToAll took 0.00019811 seconds
Rank 2: copyOwnerToAll took 0.000410031 seconds
Rank 3: copyOwnerToAll took 0.000248382 seconds
Rank 4: copyOwnerToAll took 0.000372828 seconds
Rank 5: copyOwnerToAll took 0.00045274 seconds
Rank 6: copyOwnerToAll took 0.000444598 seconds
Rank 7: copyOwnerToAll took 0.000440451 seconds
Rank 8: copyOwnerToAll took 0.00038253 seconds
Rank 9: copyOwnerToAll took 0.000383785 seconds
Rank 10: copyOwnerToAll took 0.000364125 seconds
Rank 11: copyOwnerToAll took 0.000385116 seconds
Rank 12: copyOwnerToAll took 0.000451656 seconds
Rank 13: copyOwnerToAll took 0.000447784 seconds
Rank 14: copyOwnerToAll took 0.000435565 seconds
Rank 15: copyOwnerToAll took 0.000271256 seconds
Rank 16: copyOwnerToAll took 0.000315812 seconds
Rank 17: copyOwnerToAll took 0.000388572 seconds
Rank 18: copyOwnerToAll took 0.000344408 seconds
Rank 19: copyOwnerToAll took 0.000349418 seconds
Average time for copyOwnertoAll is 0.000374665 seconds
