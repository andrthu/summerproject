Number of nodes is: 4
Number of cores/ranks per node is: 4
Metis Uniform NodeCell
Cell on rank 0 before loadbalancing: 11059
Cell on rank 1 before loadbalancing: 11195
Cell on rank 2 before loadbalancing: 11090
Cell on rank 3 before loadbalancing: 11087
Edge-cut: 1352


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
From rank 0 to: 	0	300	62	229	0	0	18	6	0	0	0	0	0	0	0	0	
From rank 1 to: 	306	0	0	0	0	0	21	15	0	0	8	288	0	0	0	0	
From rank 2 to: 	62	0	0	246	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	237	0	243	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	185	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	0	0	0	0	182	0	210	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	18	21	0	0	0	215	0	229	0	0	109	0	0	0	0	0	
From rank 7 to: 	14	24	0	0	0	0	236	0	0	0	146	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	292	226	119	143	97	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	277	0	99	145	264	0	0	0	
From rank 10 to: 	0	8	0	0	0	0	115	139	214	97	0	174	0	0	0	0	
From rank 11 to: 	0	293	0	0	0	0	0	0	96	141	161	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	147	265	0	0	0	260	66	188	
From rank 13 to: 	0	0	0	0	0	0	0	0	98	0	0	0	266	0	227	32	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	69	218	0	337	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	188	41	343	0	

After loadbalancing process 0 has 3384 cells.
After loadbalancing process 1 has 3398 cells.
After loadbalancing process 2 has 3073 cells.
After loadbalancing process 3 has 3245 cells.
After loadbalancing process 4 has 2963 cells.
After loadbalancing process 5 has 3188 cells.
After loadbalancing process 6 has 3405 cells.
After loadbalancing process 7 has 3228 cells.
After loadbalancing process 8 has 3646 cells.
After loadbalancing process 9 has 3541 cells.
After loadbalancing process 10 has 3513 cells.
After loadbalancing process 11 has 3490 cells.
After loadbalancing process 12 has 3712 cells.
After loadbalancing process 13 has 3398 cells.
After loadbalancing process 14 has 3384 cells.
After loadbalancing process 15 has 3338 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00196203 seconds
Rank 1: Matrix-vector product took 0.00200057 seconds
Rank 2: Matrix-vector product took 0.00174485 seconds
Rank 3: Matrix-vector product took 0.00185494 seconds
Rank 4: Matrix-vector product took 0.00167102 seconds
Rank 5: Matrix-vector product took 0.00187784 seconds
Rank 6: Matrix-vector product took 0.00201917 seconds
Rank 7: Matrix-vector product took 0.0018993 seconds
Rank 8: Matrix-vector product took 0.00208366 seconds
Rank 9: Matrix-vector product took 0.00211311 seconds
Rank 10: Matrix-vector product took 0.00208133 seconds
Rank 11: Matrix-vector product took 0.00209761 seconds
Rank 12: Matrix-vector product took 0.00216215 seconds
Rank 13: Matrix-vector product took 0.00191422 seconds
Rank 14: Matrix-vector product took 0.00196018 seconds
Rank 15: Matrix-vector product took 0.0019371 seconds
Average time for Matrix-vector product is 0.00196119 seconds

Rank 0: copyOwnerToAll took 0.000238262 seconds
Rank 1: copyOwnerToAll took 0.000238262 seconds
Rank 2: copyOwnerToAll took 0.000238262 seconds
Rank 3: copyOwnerToAll took 0.000238262 seconds
Rank 4: copyOwnerToAll took 0.000238262 seconds
Rank 5: copyOwnerToAll took 0.000238262 seconds
Rank 6: copyOwnerToAll took 0.000238262 seconds
Rank 7: copyOwnerToAll took 0.000238262 seconds
Rank 8: copyOwnerToAll took 0.000238262 seconds
Rank 9: copyOwnerToAll took 0.000238262 seconds
Rank 10: copyOwnerToAll took 0.000238262 seconds
Rank 11: copyOwnerToAll took 0.000238262 seconds
Rank 12: copyOwnerToAll took 0.000238262 seconds
Rank 13: copyOwnerToAll took 0.000238262 seconds
Rank 14: copyOwnerToAll took 0.000238262 seconds
Rank 15: copyOwnerToAll took 0.000238262 seconds
Average time for copyOwnertoAll is 0.000295366 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	
Rank 0's ghost cells:	0	300	62	229	0	0	18	6	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	306	0	0	0	0	0	21	15	0	0	8	288	0	0	0	0	
Rank 2's ghost cells:	62	0	0	246	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	237	0	243	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	185	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	0	0	182	0	210	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	18	21	0	0	0	215	0	229	0	0	109	0	0	0	0	0	
Rank 7's ghost cells:	14	24	0	0	0	0	236	0	0	0	146	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	292	226	119	143	97	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	277	0	99	145	264	0	0	0	
Rank 10's ghost cells:	0	8	0	0	0	0	115	139	214	97	0	174	0	0	0	0	
Rank 11's ghost cells:	0	293	0	0	0	0	0	0	96	141	161	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	147	265	0	0	0	260	66	188	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	98	0	0	0	266	0	227	32	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	69	218	0	337	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	188	41	343	0	
