Number of nodes is: 5
Number of cores/ranks per node is: 4
Metis Uniform NodeCell
Cell on rank 0 before loadbalancing: 71793
Cell on rank 1 before loadbalancing: 70906
Cell on rank 2 before loadbalancing: 70908
Cell on rank 3 before loadbalancing: 70891
Cell on rank 4 before loadbalancing: 70909
Edge-cut: -2147471259


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	1136	0	0	0	232	0	0	880	0	0	142	0	0	0	0	0	0	0	0	
From rank 1 to: 	1131	0	0	831	0	0	0	0	78	0	6	727	0	0	0	0	0	0	0	0	
From rank 2 to: 	0	0	0	907	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	0	820	900	0	0	0	0	0	0	0	0	313	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	721	290	720	2	481	0	0	259	664	0	0	0	0	0	0	
From rank 5 to: 	197	0	0	0	738	0	507	276	1228	14	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	255	501	0	543	97	311	0	0	141	8	0	0	0	204	0	38	
From rank 7 to: 	0	0	0	0	713	272	582	0	0	0	0	0	448	222	0	0	0	0	0	600	
From rank 8 to: 	891	77	0	0	2	1314	123	0	0	267	193	229	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	469	19	313	0	318	0	704	417	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	6	0	0	0	0	0	0	197	701	0	580	0	0	728	144	0	0	0	0	
From rank 11 to: 	157	811	0	314	0	0	0	0	227	417	581	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	259	0	144	493	0	0	0	0	0	623	0	0	0	96	875	738	
From rank 13 to: 	0	0	0	0	687	0	32	274	0	0	0	0	624	0	0	677	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	730	0	0	0	0	702	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	144	0	0	647	687	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	850	1161	6	
From rank 17 to: 	0	0	0	0	0	0	206	0	0	0	0	0	96	0	0	0	834	0	149	603	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	874	0	0	0	1157	157	0	716	
From rank 19 to: 	0	0	0	0	0	0	34	600	0	0	0	0	744	0	0	0	15	629	719	0	

After loadbalancing process 0 has 20334 cells.
After loadbalancing process 1 has 20717 cells.
After loadbalancing process 2 has 18856 cells.
After loadbalancing process 3 has 19989 cells.
After loadbalancing process 4 has 20832 cells.
After loadbalancing process 5 has 20658 cells.
After loadbalancing process 6 has 19912 cells.
After loadbalancing process 7 has 20536 cells.
After loadbalancing process 8 has 20939 cells.
After loadbalancing process 9 has 19926 cells.
After loadbalancing process 10 has 20044 cells.
After loadbalancing process 11 has 20198 cells.
After loadbalancing process 12 has 21092 cells.
After loadbalancing process 13 has 19907 cells.
After loadbalancing process 14 has 19236 cells.
After loadbalancing process 15 has 19088 cells.
After loadbalancing process 16 has 19743 cells.
After loadbalancing process 17 has 19620 cells.
After loadbalancing process 18 has 20631 cells.
After loadbalancing process 19 has 20465 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.0119456 seconds
Rank 1: Matrix-vector product took 0.0121458 seconds
Rank 2: Matrix-vector product took 0.0108664 seconds
Rank 3: Matrix-vector product took 0.0118462 seconds
Rank 4: Matrix-vector product took 0.012354 seconds
Rank 5: Matrix-vector product took 0.012383 seconds
Rank 6: Matrix-vector product took 0.0116233 seconds
Rank 7: Matrix-vector product took 0.0121424 seconds
Rank 8: Matrix-vector product took 0.0123644 seconds
Rank 9: Matrix-vector product took 0.0119878 seconds
Rank 10: Matrix-vector product took 0.0118699 seconds
Rank 11: Matrix-vector product took 0.0118111 seconds
Rank 12: Matrix-vector product took 0.0124094 seconds
Rank 13: Matrix-vector product took 0.0115035 seconds
Rank 14: Matrix-vector product took 0.0116141 seconds
Rank 15: Matrix-vector product took 0.0110906 seconds
Rank 16: Matrix-vector product took 0.0115036 seconds
Rank 17: Matrix-vector product took 0.0113215 seconds
Rank 18: Matrix-vector product took 0.0122666 seconds
Rank 19: Matrix-vector product took 0.0121684 seconds
Average time for Matrix-vector product is 0.0118609 seconds

Rank 0: copyOwnerToAll took 0.000552348 seconds
Rank 1: copyOwnerToAll took 0.000552348 seconds
Rank 2: copyOwnerToAll took 0.000552348 seconds
Rank 3: copyOwnerToAll took 0.000552348 seconds
Rank 4: copyOwnerToAll took 0.000552348 seconds
Rank 5: copyOwnerToAll took 0.000552348 seconds
Rank 6: copyOwnerToAll took 0.000552348 seconds
Rank 7: copyOwnerToAll took 0.000552348 seconds
Rank 8: copyOwnerToAll took 0.000552348 seconds
Rank 9: copyOwnerToAll took 0.000552348 seconds
Rank 10: copyOwnerToAll took 0.000552348 seconds
Rank 11: copyOwnerToAll took 0.000552348 seconds
Rank 12: copyOwnerToAll took 0.000552348 seconds
Rank 13: copyOwnerToAll took 0.000552348 seconds
Rank 14: copyOwnerToAll took 0.000552348 seconds
Rank 15: copyOwnerToAll took 0.000552348 seconds
Rank 16: copyOwnerToAll took 0.000552348 seconds
Rank 17: copyOwnerToAll took 0.000552348 seconds
Rank 18: copyOwnerToAll took 0.000552348 seconds
Rank 19: copyOwnerToAll took 0.000552348 seconds
Average time for copyOwnertoAll is 0.000572312 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	1136	0	0	0	232	0	0	880	0	0	142	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	1131	0	0	831	0	0	0	0	78	0	6	727	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	0	0	0	907	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	0	820	900	0	0	0	0	0	0	0	0	313	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	721	290	720	2	481	0	0	259	664	0	0	0	0	0	0	
Rank 5's ghost cells:	197	0	0	0	738	0	507	276	1228	14	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	255	501	0	543	97	311	0	0	141	8	0	0	0	204	0	38	
Rank 7's ghost cells:	0	0	0	0	713	272	582	0	0	0	0	0	448	222	0	0	0	0	0	600	
Rank 8's ghost cells:	891	77	0	0	2	1314	123	0	0	267	193	229	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	469	19	313	0	318	0	704	417	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	6	0	0	0	0	0	0	197	701	0	580	0	0	728	144	0	0	0	0	
Rank 11's ghost cells:	157	811	0	314	0	0	0	0	227	417	581	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	259	0	144	493	0	0	0	0	0	623	0	0	0	96	875	738	
Rank 13's ghost cells:	0	0	0	0	687	0	32	274	0	0	0	0	624	0	0	677	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	730	0	0	0	0	702	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	144	0	0	647	687	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	850	1161	6	
Rank 17's ghost cells:	0	0	0	0	0	0	206	0	0	0	0	0	96	0	0	0	834	0	149	603	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	874	0	0	0	1157	157	0	716	
Rank 19's ghost cells:	0	0	0	0	0	0	34	600	0	0	0	0	744	0	0	0	15	629	719	0	
