Number of nodes is: 2
Number of cores/ranks per node is: 4
Metis Uniform CellNode
Cell on rank 0 before loadbalancing: 44305
Cell on rank 1 before loadbalancing: 44309
Cell on rank 2 before loadbalancing: 44587
Cell on rank 3 before loadbalancing: 44550
Cell on rank 4 before loadbalancing: 44740
Cell on rank 5 before loadbalancing: 44311
Cell on rank 6 before loadbalancing: 44308
Cell on rank 7 before loadbalancing: 44297
Edge-cut: -2147462979


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
From rank 0 to: 	0	2100	262	715	0	928	81	0	
From rank 1 to: 	2098	0	0	2120	158	293	0	0	
From rank 2 to: 	249	0	0	1660	0	1487	973	0	
From rank 3 to: 	694	2059	1676	0	867	1384	31	4	
From rank 4 to: 	0	139	0	833	0	230	16	80	
From rank 5 to: 	959	264	1645	1405	231	0	504	834	
From rank 6 to: 	81	0	985	18	16	529	0	1278	
From rank 7 to: 	0	0	0	4	82	849	1275	0	

Edge-cut for node partition: 4977


After loadbalancing process 0 has 48391 cells.
After loadbalancing process 1 has 48978 cells.
After loadbalancing process 2 has 51265 cells.
After loadbalancing process 3 has 46038 cells.
After loadbalancing process 4 has 48956 cells.
After loadbalancing process 5 has 50153 cells.
After loadbalancing process 6 has 47215 cells.
After loadbalancing process 7 has 46507 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.0281043 seconds
Rank 1: Matrix-vector product took 0.0291823 seconds
Rank 2: Matrix-vector product took 0.0301123 seconds
Rank 3: Matrix-vector product took 0.0270844 seconds
Rank 4: Matrix-vector product took 0.0294776 seconds
Rank 5: Matrix-vector product took 0.0293854 seconds
Rank 6: Matrix-vector product took 0.0282531 seconds
Rank 7: Matrix-vector product took 0.0278524 seconds
Average time for Matrix-vector product is 0.0286815 seconds

Rank 0: copyOwnerToAll took 0.00058508 seconds
Rank 1: copyOwnerToAll took 0.00058508 seconds
Rank 2: copyOwnerToAll took 0.00058508 seconds
Rank 3: copyOwnerToAll took 0.00058508 seconds
Rank 4: copyOwnerToAll took 0.00058508 seconds
Rank 5: copyOwnerToAll took 0.00058508 seconds
Rank 6: copyOwnerToAll took 0.00058508 seconds
Rank 7: copyOwnerToAll took 0.00058508 seconds
Average time for copyOwnertoAll is 0.000684142 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	
Rank 0's ghost cells:	0	2100	715	0	262	928	81	0	
Rank 1's ghost cells:	2098	0	2120	158	0	293	0	0	
Rank 2's ghost cells:	694	2059	0	867	1676	1384	31	4	
Rank 3's ghost cells:	0	139	833	0	0	230	16	80	
Rank 4's ghost cells:	249	0	1660	0	0	1487	973	0	
Rank 5's ghost cells:	959	264	1405	231	1645	0	504	834	
Rank 6's ghost cells:	81	0	18	16	985	529	0	1278	
Rank 7's ghost cells:	0	0	4	82	0	849	1275	0	
