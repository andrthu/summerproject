Number of nodes is: 7
Number of cores/ranks per node is: 4
cpGrid partitioner


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
From rank 0 to: 	0	29	167	102	85	181	0	0	0	11	12	0	0	0	103	45	121	39	0	0	0	0	24	28	0	6	6	0	
From rank 1 to: 	21	0	79	190	196	64	19	0	0	3	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	188	90	0	161	386	517	41	0	0	37	42	0	72	0	0	6	4	0	36	0	0	0	0	1	35	0	0	21	
From rank 3 to: 	125	229	172	0	261	3	47	0	0	42	78	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	93	217	334	222	0	267	173	0	0	0	34	0	0	0	0	0	33	0	0	0	0	0	0	0	5	0	0	0	
From rank 5 to: 	185	69	416	4	228	0	168	0	0	0	22	0	0	0	0	1	60	0	16	0	0	0	0	22	27	0	0	0	
From rank 6 to: 	0	20	23	36	163	139	0	0	0	0	35	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	0	0	0	368	9	2	206	163	135	0	0	0	0	21	8	0	0	0	77	58	0	17	6	
From rank 8 to: 	0	0	0	0	0	0	0	336	0	14	7	0	45	33	0	0	0	0	0	0	0	0	0	0	2	0	0	0	
From rank 9 to: 	19	9	35	80	0	0	0	25	14	0	131	44	0	36	0	0	0	0	0	0	0	0	20	0	18	0	0	0	
From rank 10 to: 	12	10	44	202	35	23	42	8	16	126	0	0	0	2	0	0	42	0	0	0	0	0	0	0	15	22	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	172	0	17	0	0	99	249	0	0	0	0	0	0	0	0	0	0	132	0	37	52	
From rank 12 to: 	0	0	15	0	0	0	0	111	44	0	0	98	0	139	0	0	0	0	1	0	0	0	0	44	59	0	30	57	
From rank 13 to: 	0	0	0	0	0	0	0	121	33	17	2	252	117	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	257	119	14	12	16	95	27	82	14	0	33	21	0	
From rank 15 to: 	48	0	6	0	0	1	0	0	0	0	0	0	0	0	252	0	79	252	0	185	25	82	55	380	1	13	2	18	
From rank 16 to: 	124	0	6	0	33	51	0	0	0	0	19	0	0	0	123	84	0	266	12	0	0	0	0	32	15	29	0	48	
From rank 17 to: 	46	0	0	0	0	0	0	0	0	0	0	0	0	0	15	278	276	0	20	76	0	0	6	135	0	99	1	100	
From rank 18 to: 	0	0	34	0	0	21	0	23	0	0	0	0	4	0	5	0	12	13	0	117	0	19	0	739	7	15	2	98	
From rank 19 to: 	0	0	0	0	0	0	0	8	0	0	0	0	0	0	16	187	0	74	120	0	114	146	84	80	89	3	16	22	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	101	27	0	0	0	123	0	170	100	0	0	3	6	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	25	79	0	0	19	124	167	0	218	210	85	0	0	0	
From rank 22 to: 	21	0	0	0	0	0	0	0	0	22	0	0	0	0	78	38	0	6	0	96	89	303	0	73	8	12	52	21	
From rank 23 to: 	53	0	1	0	0	26	0	85	0	0	0	0	44	0	9	400	41	156	746	76	0	225	57	0	43	41	13	133	
From rank 24 to: 	0	0	15	0	5	49	0	48	3	17	12	171	74	0	0	1	33	0	7	91	0	105	2	45	0	34	96	25	
From rank 25 to: 	7	0	0	0	0	0	0	0	0	0	21	0	0	0	35	16	43	94	14	2	3	0	14	40	33	0	158	158	
From rank 26 to: 	6	0	0	0	0	0	0	8	0	0	0	25	33	0	22	3	0	2	2	23	7	0	42	11	83	155	0	192	
From rank 27 to: 	0	0	6	0	0	0	0	6	0	0	0	52	58	0	0	18	29	94	37	22	0	0	19	107	21	123	148	0	

Cell on rank 0 before loadbalancing: 1652
Cell on rank 1 before loadbalancing: 1663
Cell on rank 2 before loadbalancing: 1648
Cell on rank 3 before loadbalancing: 1653
Cell on rank 4 before loadbalancing: 1647
Cell on rank 5 before loadbalancing: 1614
Cell on rank 6 before loadbalancing: 1610
Cell on rank 7 before loadbalancing: 1523
Cell on rank 8 before loadbalancing: 1666
Cell on rank 9 before loadbalancing: 1665
Cell on rank 10 before loadbalancing: 1216
Cell on rank 11 before loadbalancing: 1642
Cell on rank 12 before loadbalancing: 1649
Cell on rank 13 before loadbalancing: 1555
Cell on rank 14 before loadbalancing: 1632
Cell on rank 15 before loadbalancing: 1633
Cell on rank 16 before loadbalancing: 1661
Cell on rank 17 before loadbalancing: 1657
Cell on rank 18 before loadbalancing: 1536
Cell on rank 19 before loadbalancing: 1662
Cell on rank 20 before loadbalancing: 1467
Cell on rank 21 before loadbalancing: 1636
Cell on rank 22 before loadbalancing: 1444
Cell on rank 23 before loadbalancing: 1579
Cell on rank 24 before loadbalancing: 1238
Cell on rank 25 before loadbalancing: 1665
Cell on rank 26 before loadbalancing: 1565
Cell on rank 27 before loadbalancing: 1653

Edge-cut for node partition: 10412


After loadbalancing process 0 has 2103 cells.
After loadbalancing process 1 has 2400 cells.
After loadbalancing process 2 has 2097 cells.
After loadbalancing process 3 has 2645 cells.
After loadbalancing process 4 has 2611 cells.
After loadbalancing process 5 has 2026 cells.
After loadbalancing process 6 has 2096 cells.
After loadbalancing process 7 has 2503 cells.
After loadbalancing process 8 has 2241 cells.
After loadbalancing process 9 has 3025 cells.
After loadbalancing process 10 has 2832 cells.
After loadbalancing process 11 has 3728 cells.
After loadbalancing process 12 has 1997 cells.
After loadbalancing process 13 has 2563 cells.
After loadbalancing process 14 has 2071 cells.
After loadbalancing process 15 has 2179 cells.
After loadbalancing process 16 has 2593 cells.
After loadbalancing process 17 has 2247 cells.
After loadbalancing process 18 has 2621 cells.
After loadbalancing process 19 has 2263 cells.
After loadbalancing process 20 has 3285 cells.
After loadbalancing process 21 has 2436 cells.
After loadbalancing process 22 has 2303 cells.
After loadbalancing process 23 has 2393 cells.
After loadbalancing process 24 has 2610 cells.
After loadbalancing process 25 has 1815 cells.
After loadbalancing process 26 has 3032 cells.
After loadbalancing process 27 has 2709 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00119653 seconds
Rank 1: Matrix-vector product took 0.00144434 seconds
Rank 2: Matrix-vector product took 0.00124952 seconds
Rank 3: Matrix-vector product took 0.0014638 seconds
Rank 4: Matrix-vector product took 0.00153226 seconds
Rank 5: Matrix-vector product took 0.00112582 seconds
Rank 6: Matrix-vector product took 0.00111224 seconds
Rank 7: Matrix-vector product took 0.00150044 seconds
Rank 8: Matrix-vector product took 0.00132055 seconds
Rank 9: Matrix-vector product took 0.00174296 seconds
Rank 10: Matrix-vector product took 0.00157617 seconds
Rank 11: Matrix-vector product took 0.0021235 seconds
Rank 12: Matrix-vector product took 0.00115797 seconds
Rank 13: Matrix-vector product took 0.00148643 seconds
Rank 14: Matrix-vector product took 0.00115715 seconds
Rank 15: Matrix-vector product took 0.00120273 seconds
Rank 16: Matrix-vector product took 0.00142235 seconds
Rank 17: Matrix-vector product took 0.00131026 seconds
Rank 18: Matrix-vector product took 0.00148946 seconds
Rank 19: Matrix-vector product took 0.00126632 seconds
Rank 20: Matrix-vector product took 0.00182071 seconds
Rank 21: Matrix-vector product took 0.00141557 seconds
Rank 22: Matrix-vector product took 0.00127226 seconds
Rank 23: Matrix-vector product took 0.00141431 seconds
Rank 24: Matrix-vector product took 0.00156152 seconds
Rank 25: Matrix-vector product took 0.000966941 seconds
Rank 26: Matrix-vector product took 0.00172329 seconds
Rank 27: Matrix-vector product took 0.00157302 seconds
Average time for Matrix-vector product is 0.0014153 seconds

Rank 0: copyOwnerToAll took 0.000491566 seconds
Rank 1: copyOwnerToAll took 0.000491566 seconds
Rank 2: copyOwnerToAll took 0.000491566 seconds
Rank 3: copyOwnerToAll took 0.000491566 seconds
Rank 4: copyOwnerToAll took 0.000491566 seconds
Rank 5: copyOwnerToAll took 0.000491566 seconds
Rank 6: copyOwnerToAll took 0.000491566 seconds
Rank 7: copyOwnerToAll took 0.000491566 seconds
Rank 8: copyOwnerToAll took 0.000491566 seconds
Rank 9: copyOwnerToAll took 0.000491566 seconds
Rank 10: copyOwnerToAll took 0.000491566 seconds
Rank 11: copyOwnerToAll took 0.000491566 seconds
Rank 12: copyOwnerToAll took 0.000491566 seconds
Rank 13: copyOwnerToAll took 0.000491566 seconds
Rank 14: copyOwnerToAll took 0.000491566 seconds
Rank 15: copyOwnerToAll took 0.000491566 seconds
Rank 16: copyOwnerToAll took 0.000491566 seconds
Rank 17: copyOwnerToAll took 0.000491566 seconds
Rank 18: copyOwnerToAll took 0.000491566 seconds
Rank 19: copyOwnerToAll took 0.000491566 seconds
Rank 20: copyOwnerToAll took 0.000491566 seconds
Rank 21: copyOwnerToAll took 0.000491566 seconds
Rank 22: copyOwnerToAll took 0.000491566 seconds
Rank 23: copyOwnerToAll took 0.000491566 seconds
Rank 24: copyOwnerToAll took 0.000491566 seconds
Rank 25: copyOwnerToAll took 0.000491566 seconds
Rank 26: copyOwnerToAll took 0.000491566 seconds
Rank 27: copyOwnerToAll took 0.000491566 seconds
Average time for copyOwnertoAll is 0.000518787 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
Rank 0's ghost cells:	0	0	33	0	0	0	14	0	0	0	0	0	0	0	2	0	336	45	0	0	0	0	0	0	0	7	0	0	
Rank 1's ghost cells:	0	0	249	0	0	0	17	0	0	0	0	0	0	0	132	37	172	99	0	0	0	0	0	52	0	0	0	0	
Rank 2's ghost cells:	33	252	0	0	0	0	17	0	0	0	0	0	0	0	0	0	121	117	0	0	0	0	0	0	0	2	0	0	
Rank 3's ghost cells:	0	0	0	0	0	0	0	12	0	0	21	739	0	19	7	2	23	4	117	0	34	5	15	98	0	0	0	13	
Rank 4's ghost cells:	0	0	0	0	0	0	11	121	29	85	181	28	0	0	0	6	0	0	0	24	167	103	6	0	102	12	45	39	
Rank 5's ghost cells:	0	0	0	0	0	0	0	0	20	163	139	0	0	0	0	0	0	0	0	0	23	0	0	0	36	35	0	0	
Rank 6's ghost cells:	14	44	36	0	19	0	0	0	9	0	0	0	0	0	18	0	25	0	0	20	35	0	0	0	80	131	0	0	
Rank 7's ghost cells:	0	0	0	12	124	0	0	0	0	33	51	32	0	0	15	0	0	0	0	0	6	123	29	48	0	19	84	266	
Rank 8's ghost cells:	0	0	0	0	21	19	3	0	0	196	64	0	0	0	0	0	0	0	0	0	79	0	0	0	190	6	0	0	
Rank 9's ghost cells:	0	0	0	0	93	173	0	33	217	0	267	0	0	0	5	0	0	0	0	0	334	0	0	0	222	34	0	0	
Rank 10's ghost cells:	0	0	0	16	185	168	0	60	69	228	0	22	0	0	27	0	0	0	0	0	416	0	0	0	4	22	1	0	
Rank 11's ghost cells:	0	0	0	746	53	0	0	41	0	0	26	0	0	225	43	13	85	44	76	57	1	9	41	133	0	0	400	156	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	170	0	6	0	0	123	100	0	101	3	0	0	0	27	0	
Rank 13's ghost cells:	0	0	0	19	0	0	0	0	0	0	0	210	167	0	85	0	0	0	124	218	0	25	0	0	0	0	79	0	
Rank 14's ghost cells:	3	171	0	7	0	0	17	33	0	5	49	45	0	105	0	96	48	74	91	2	15	0	34	25	0	12	1	0	
Rank 15's ghost cells:	0	25	0	2	6	0	0	0	0	0	0	11	7	0	83	0	8	33	23	42	0	22	155	192	0	0	3	2	
Rank 16's ghost cells:	368	206	135	21	0	0	9	0	0	0	0	77	0	0	58	17	0	163	8	0	0	0	0	6	0	2	0	0	
Rank 17's ghost cells:	44	98	139	1	0	0	0	0	0	0	0	44	0	0	59	30	111	0	0	0	15	0	0	57	0	0	0	0	
Rank 18's ghost cells:	0	0	0	120	0	0	0	0	0	0	0	80	114	146	89	16	8	0	0	84	0	16	3	22	0	0	187	74	
Rank 19's ghost cells:	0	0	0	0	21	0	22	0	0	0	0	73	89	303	8	52	0	0	96	0	0	78	12	21	0	0	38	6	
Rank 20's ghost cells:	0	0	0	36	188	41	37	4	90	386	517	1	0	0	35	0	0	72	0	0	0	0	0	21	161	42	6	0	
Rank 21's ghost cells:	0	0	0	12	114	0	0	119	0	0	0	14	95	27	0	21	0	0	16	82	0	0	33	0	0	0	257	14	
Rank 22's ghost cells:	0	0	0	14	7	0	0	43	0	0	0	40	3	0	33	158	0	0	2	14	0	35	0	158	0	21	16	94	
Rank 23's ghost cells:	0	52	0	37	0	0	0	29	0	0	0	107	0	0	21	148	6	58	22	19	6	0	123	0	0	0	18	94	
Rank 24's ghost cells:	0	0	0	0	125	47	42	0	229	261	3	0	0	0	0	0	0	0	0	0	172	0	0	0	0	78	0	0	
Rank 25's ghost cells:	16	0	2	0	12	42	126	42	10	35	23	0	0	0	15	0	8	0	0	0	44	0	22	0	202	0	0	0	
Rank 26's ghost cells:	0	0	0	0	48	0	0	79	0	0	1	380	25	82	1	2	0	0	185	55	6	252	13	18	0	0	0	252	
Rank 27's ghost cells:	0	0	0	20	46	0	0	276	0	0	0	135	0	0	0	1	0	0	76	6	0	15	99	100	0	0	278	0	
