Number of nodes is: 7
Number of cores/ranks per node is: 4
Metis norne Node cell partitioner
 Cell on rank 0 before loadbalancing: 6357
Cell on rank 1 before loadbalancing: 6321
Cell on rank 2 before loadbalancing: 6305
Cell on rank 3 before loadbalancing: 6388
Cell on rank 4 before loadbalancing: 6325
Cell on rank 5 before loadbalancing: 6324
Cell on rank 6 before loadbalancing: 6411
Edge-cut: 71672


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
From rank 0 to: 	0	178	36	136	0	78	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	179	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	36	0	0	181	0	0	0	0	0	0	0	0	199	13	9	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	130	0	181	0	0	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	0	0	0	0	0	99	57	144	0	181	0	0	0	0	0	0	0	0	0	0	62	71	58	0	0	0	0	0	
From rank 5 to: 	91	0	0	36	109	0	0	127	74	6	0	0	0	0	0	0	0	0	0	0	91	56	19	0	0	0	34	12	
From rank 6 to: 	0	0	0	0	72	0	0	218	9	135	204	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	117	115	215	0	0	37	0	0	0	0	0	0	0	0	0	0	133	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	72	10	0	0	133	54	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	181	6	135	30	123	0	39	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	204	0	54	49	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	100	131	192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	201	0	0	0	0	0	0	0	0	0	0	187	111	152	34	0	0	0	0	0	0	0	0	0	0	0	
From rank 13 to: 	0	0	20	0	0	0	0	0	0	0	0	0	192	0	0	116	12	0	0	29	0	0	0	0	0	0	0	0	
From rank 14 to: 	0	0	9	0	0	0	0	0	0	0	0	0	108	0	0	198	42	99	0	0	0	9	22	97	0	0	164	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	161	114	188	0	0	0	0	0	0	0	0	0	0	0	66	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	27	11	37	0	0	160	95	142	0	0	0	18	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	0	167	0	72	0	0	0	69	104	0	0	14	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	72	0	155	0	0	0	0	32	33	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	141	0	155	0	0	0	0	0	172	56	0	0	
From rank 20 to: 	0	0	0	0	58	78	0	132	0	0	0	0	0	0	0	0	0	0	0	0	0	168	150	88	0	0	0	0	
From rank 21 to: 	0	0	0	0	72	52	0	0	0	0	0	0	0	0	9	0	0	0	0	0	164	0	75	12	0	0	228	0	
From rank 22 to: 	0	0	0	0	63	21	0	0	0	0	0	0	0	0	22	0	0	73	0	0	141	73	0	175	0	0	79	31	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	15	106	0	0	86	12	168	0	0	0	49	13	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	173	0	0	0	0	0	153	0	110	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	56	0	0	0	0	157	0	0	51	
From rank 26 to: 	0	0	0	0	0	33	0	0	0	0	0	0	0	0	161	68	0	13	0	0	0	225	73	44	0	0	0	25	
From rank 27 to: 	0	0	0	0	0	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	27	13	115	32	26	0	

After loadbalancing process 0 has 2007 cells.
After loadbalancing process 1 has 1752 cells.
After loadbalancing process 2 has 2038 cells.
After loadbalancing process 3 has 1952 cells.
After loadbalancing process 4 has 2251 cells.
After loadbalancing process 5 has 2219 cells.
After loadbalancing process 6 has 2222 cells.
After loadbalancing process 7 has 2211 cells.
After loadbalancing process 8 has 1927 cells.
After loadbalancing process 9 has 2233 cells.
After loadbalancing process 10 has 2069 cells.
After loadbalancing process 11 has 1984 cells.
After loadbalancing process 12 has 2284 cells.
After loadbalancing process 13 has 1966 cells.
After loadbalancing process 14 has 2348 cells.
After loadbalancing process 15 has 2121 cells.
After loadbalancing process 16 has 2082 cells.
After loadbalancing process 17 has 2100 cells.
After loadbalancing process 18 has 1969 cells.
After loadbalancing process 19 has 2126 cells.
After loadbalancing process 20 has 2249 cells.
After loadbalancing process 21 has 2184 cells.
After loadbalancing process 22 has 2274 cells.
After loadbalancing process 23 has 2132 cells.
After loadbalancing process 24 has 2056 cells.
After loadbalancing process 25 has 1885 cells.
After loadbalancing process 26 has 2257 cells.
After loadbalancing process 27 has 1843 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00113472 seconds
Rank 1: Matrix-vector product took 0.000948881 seconds
Rank 2: Matrix-vector product took 0.00120963 seconds
Rank 3: Matrix-vector product took 0.00113869 seconds
Rank 4: Matrix-vector product took 0.00127988 seconds
Rank 5: Matrix-vector product took 0.00119654 seconds
Rank 6: Matrix-vector product took 0.00129159 seconds
Rank 7: Matrix-vector product took 0.00127838 seconds
Rank 8: Matrix-vector product took 0.00105924 seconds
Rank 9: Matrix-vector product took 0.00130314 seconds
Rank 10: Matrix-vector product took 0.00116856 seconds
Rank 11: Matrix-vector product took 0.00110241 seconds
Rank 12: Matrix-vector product took 0.0013252 seconds
Rank 13: Matrix-vector product took 0.00162569 seconds
Rank 14: Matrix-vector product took 0.00134691 seconds
Rank 15: Matrix-vector product took 0.00179078 seconds
Rank 16: Matrix-vector product took 0.00121523 seconds
Rank 17: Matrix-vector product took 0.00121533 seconds
Rank 18: Matrix-vector product took 0.00108802 seconds
Rank 19: Matrix-vector product took 0.00121222 seconds
Rank 20: Matrix-vector product took 0.0013096 seconds
Rank 21: Matrix-vector product took 0.00126415 seconds
Rank 22: Matrix-vector product took 0.00132542 seconds
Rank 23: Matrix-vector product took 0.0012541 seconds
Rank 24: Matrix-vector product took 0.00121255 seconds
Rank 25: Matrix-vector product took 0.0010091 seconds
Rank 26: Matrix-vector product took 0.00129463 seconds
Rank 27: Matrix-vector product took 0.00100518 seconds
Average time for Matrix-vector product is 0.00123592 seconds

Rank 0: copyOwnerToAll took 0.000328112 seconds
Rank 1: copyOwnerToAll took 0.000328112 seconds
Rank 2: copyOwnerToAll took 0.000328112 seconds
Rank 3: copyOwnerToAll took 0.000328112 seconds
Rank 4: copyOwnerToAll took 0.000328112 seconds
Rank 5: copyOwnerToAll took 0.000328112 seconds
Rank 6: copyOwnerToAll took 0.000328112 seconds
Rank 7: copyOwnerToAll took 0.000328112 seconds
Rank 8: copyOwnerToAll took 0.000328112 seconds
Rank 9: copyOwnerToAll took 0.000328112 seconds
Rank 10: copyOwnerToAll took 0.000328112 seconds
Rank 11: copyOwnerToAll took 0.000328112 seconds
Rank 12: copyOwnerToAll took 0.000328112 seconds
Rank 13: copyOwnerToAll took 0.000328112 seconds
Rank 14: copyOwnerToAll took 0.000328112 seconds
Rank 15: copyOwnerToAll took 0.000328112 seconds
Rank 16: copyOwnerToAll took 0.000328112 seconds
Rank 17: copyOwnerToAll took 0.000328112 seconds
Rank 18: copyOwnerToAll took 0.000328112 seconds
Rank 19: copyOwnerToAll took 0.000328112 seconds
Rank 20: copyOwnerToAll took 0.000328112 seconds
Rank 21: copyOwnerToAll took 0.000328112 seconds
Rank 22: copyOwnerToAll took 0.000328112 seconds
Rank 23: copyOwnerToAll took 0.000328112 seconds
Rank 24: copyOwnerToAll took 0.000328112 seconds
Rank 25: copyOwnerToAll took 0.000328112 seconds
Rank 26: copyOwnerToAll took 0.000328112 seconds
Rank 27: copyOwnerToAll took 0.000328112 seconds
Average time for copyOwnertoAll is 0.000402994 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
Rank 0's ghost cells:	0	178	36	136	0	78	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	179	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	36	0	0	181	0	0	0	0	0	0	0	0	199	13	9	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	130	0	181	0	0	36	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	0	0	0	99	57	144	0	181	0	0	0	0	0	0	0	0	0	0	62	71	58	0	0	0	0	0	
Rank 5's ghost cells:	91	0	0	36	109	0	0	127	74	6	0	0	0	0	0	0	0	0	0	0	91	56	19	0	0	0	34	12	
Rank 6's ghost cells:	0	0	0	0	72	0	0	218	9	135	204	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	117	115	215	0	0	37	0	0	0	0	0	0	0	0	0	0	133	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	72	10	0	0	133	54	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	181	6	135	30	123	0	39	130	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	204	0	54	49	0	175	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	100	131	192	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	201	0	0	0	0	0	0	0	0	0	0	187	111	152	34	0	0	0	0	0	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	20	0	0	0	0	0	0	0	0	0	192	0	0	116	12	0	0	29	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	9	0	0	0	0	0	0	0	0	0	108	0	0	198	42	99	0	0	0	9	22	97	0	0	164	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	161	114	188	0	0	0	0	0	0	0	0	0	0	0	66	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	27	11	37	0	0	160	95	142	0	0	0	18	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	93	0	167	0	72	0	0	0	69	104	0	0	14	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	96	72	0	155	0	0	0	0	32	33	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	31	0	0	141	0	155	0	0	0	0	0	172	56	0	0	
Rank 20's ghost cells:	0	0	0	0	58	78	0	132	0	0	0	0	0	0	0	0	0	0	0	0	0	168	150	88	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	72	52	0	0	0	0	0	0	0	0	9	0	0	0	0	0	164	0	75	12	0	0	228	0	
Rank 22's ghost cells:	0	0	0	0	63	21	0	0	0	0	0	0	0	0	22	0	0	73	0	0	141	73	0	175	0	0	79	31	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	102	0	15	106	0	0	86	12	168	0	0	0	49	13	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	173	0	0	0	0	0	153	0	110	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	35	56	0	0	0	0	157	0	0	51	
Rank 26's ghost cells:	0	0	0	0	0	33	0	0	0	0	0	0	0	0	161	68	0	13	0	0	0	225	73	44	0	0	0	25	
Rank 27's ghost cells:	0	0	0	0	0	12	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	27	13	115	32	26	0	
