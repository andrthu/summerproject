Number of nodes is: 7
Number of cores/ranks per node is: 4
METIS partitioner
Cell on rank 0 before loadbalancing: 12796
Cell on rank 1 before loadbalancing: 12722
Cell on rank 2 before loadbalancing: 12716
Cell on rank 3 before loadbalancing: 12720
Cell on rank 4 before loadbalancing: 12723
Cell on rank 5 before loadbalancing: 12720
Cell on rank 6 before loadbalancing: 12706
Cell on rank 7 before loadbalancing: 12716
Cell on rank 8 before loadbalancing: 12705
Cell on rank 9 before loadbalancing: 12715
Cell on rank 10 before loadbalancing: 12622
Cell on rank 11 before loadbalancing: 12648
Cell on rank 12 before loadbalancing: 12647
Cell on rank 13 before loadbalancing: 12566
Cell on rank 14 before loadbalancing: 12647
Cell on rank 15 before loadbalancing: 12660
Cell on rank 16 before loadbalancing: 12661
Cell on rank 17 before loadbalancing: 12645
Cell on rank 18 before loadbalancing: 12648
Cell on rank 19 before loadbalancing: 12691
Cell on rank 20 before loadbalancing: 12651
Cell on rank 21 before loadbalancing: 12666
Cell on rank 22 before loadbalancing: 12663
Cell on rank 23 before loadbalancing: 12666
Cell on rank 24 before loadbalancing: 12791
Cell on rank 25 before loadbalancing: 12791
Cell on rank 26 before loadbalancing: 12790
Cell on rank 27 before loadbalancing: 12715
Edge-cut: 32745


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
From rank 0 to: 	0	562	463	560	58	99	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	556	0	1117	0	0	1123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	482	1116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 3 to: 	562	0	0	0	160	569	379	0	657	0	0	0	0	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 4 to: 	58	0	0	161	0	0	0	0	14	0	0	0	0	233	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	127	1121	0	570	0	0	1058	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	389	0	1058	0	566	641	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	0	0	567	0	406	538	208	508	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	657	12	0	640	407	0	1119	0	0	0	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	530	1119	0	0	286	730	223	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	208	0	0	0	400	503	13	0	0	0	21	0	0	40	409	437	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	513	0	284	404	0	731	0	0	0	0	689	0	0	197	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	0	0	0	0	0	0	743	487	740	0	454	0	0	0	0	0	0	176	194	0	0	0	0	0	0	
From rank 13 to: 	0	0	0	54	233	0	0	0	45	246	11	0	465	0	0	0	0	0	0	0	0	234	0	0	0	0	0	0	
From rank 14 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	576	0	0	0	731	0	0	0	0	0	0	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	574	0	593	0	0	144	0	0	0	0	0	0	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	601	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	23	698	0	0	0	0	0	0	801	0	408	0	13	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	794	0	311	318	0	114	0	0	0	0	3	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	725	144	0	0	305	0	629	0	0	0	0	0	0	0	
From rank 20 to: 	0	0	0	0	0	0	0	0	0	0	46	196	176	0	0	0	0	394	316	629	0	0	68	0	0	0	0	0	
From rank 21 to: 	0	0	0	0	0	0	0	0	0	0	416	0	190	235	0	0	0	0	0	0	0	0	270	673	0	0	58	0	
From rank 22 to: 	0	0	0	0	0	0	0	0	0	0	437	0	0	0	0	0	0	12	112	0	68	274	0	613	0	0	0	400	
From rank 23 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	675	621	0	0	0	625	81	
From rank 24 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	791	401	605	
From rank 25 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	786	0	0	0	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	0	636	397	0	0	576	
From rank 27 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	400	83	608	0	574	0	

After loadbalancing process 0 has 14538 cells.
After loadbalancing process 1 has 15518 cells.
After loadbalancing process 2 has 14314 cells.
After loadbalancing process 3 has 15105 cells.
After loadbalancing process 4 has 13189 cells.
After loadbalancing process 5 has 15596 cells.
After loadbalancing process 6 has 15360 cells.
After loadbalancing process 7 has 14943 cells.
After loadbalancing process 8 has 15592 cells.
After loadbalancing process 9 has 15603 cells.
After loadbalancing process 10 has 14653 cells.
After loadbalancing process 11 has 15466 cells.
After loadbalancing process 12 has 15441 cells.
After loadbalancing process 13 has 13854 cells.
After loadbalancing process 14 has 13954 cells.
After loadbalancing process 15 has 13971 cells.
After loadbalancing process 16 has 13262 cells.
After loadbalancing process 17 has 14588 cells.
After loadbalancing process 18 has 14188 cells.
After loadbalancing process 19 has 14494 cells.
After loadbalancing process 20 has 14476 cells.
After loadbalancing process 21 has 14508 cells.
After loadbalancing process 22 has 14579 cells.
After loadbalancing process 23 has 14668 cells.
After loadbalancing process 24 has 14588 cells.
After loadbalancing process 25 has 13577 cells.
After loadbalancing process 26 has 14457 cells.
After loadbalancing process 27 has 14383 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00852954 seconds
Rank 1: Matrix-vector product took 0.00915051 seconds
Rank 2: Matrix-vector product took 0.00828944 seconds
Rank 3: Matrix-vector product took 0.00886104 seconds
Rank 4: Matrix-vector product took 0.00754989 seconds
Rank 5: Matrix-vector product took 0.00924343 seconds
Rank 6: Matrix-vector product took 0.0090424 seconds
Rank 7: Matrix-vector product took 0.00883221 seconds
Rank 8: Matrix-vector product took 0.00915115 seconds
Rank 9: Matrix-vector product took 0.00923071 seconds
Rank 10: Matrix-vector product took 0.00874545 seconds
Rank 11: Matrix-vector product took 0.00923449 seconds
Rank 12: Matrix-vector product took 0.0092092 seconds
Rank 13: Matrix-vector product took 0.00803379 seconds
Rank 14: Matrix-vector product took 0.00828263 seconds
Rank 15: Matrix-vector product took 0.00807148 seconds
Rank 16: Matrix-vector product took 0.00768955 seconds
Rank 17: Matrix-vector product took 0.00863166 seconds
Rank 18: Matrix-vector product took 0.00875418 seconds
Rank 19: Matrix-vector product took 0.00865196 seconds
Rank 20: Matrix-vector product took 0.00849008 seconds
Rank 21: Matrix-vector product took 0.00849369 seconds
Rank 22: Matrix-vector product took 0.00866821 seconds
Rank 23: Matrix-vector product took 0.00865773 seconds
Rank 24: Matrix-vector product took 0.00857958 seconds
Rank 25: Matrix-vector product took 0.00779194 seconds
Rank 26: Matrix-vector product took 0.00842598 seconds
Rank 27: Matrix-vector product took 0.00834274 seconds
Average time for Matrix-vector product is 0.00859409 seconds

Rank 0: copyOwnerToAll took 0.000661254 seconds
Rank 1: copyOwnerToAll took 0.000661254 seconds
Rank 2: copyOwnerToAll took 0.000661254 seconds
Rank 3: copyOwnerToAll took 0.000661254 seconds
Rank 4: copyOwnerToAll took 0.000661254 seconds
Rank 5: copyOwnerToAll took 0.000661254 seconds
Rank 6: copyOwnerToAll took 0.000661254 seconds
Rank 7: copyOwnerToAll took 0.000661254 seconds
Rank 8: copyOwnerToAll took 0.000661254 seconds
Rank 9: copyOwnerToAll took 0.000661254 seconds
Rank 10: copyOwnerToAll took 0.000661254 seconds
Rank 11: copyOwnerToAll took 0.000661254 seconds
Rank 12: copyOwnerToAll took 0.000661254 seconds
Rank 13: copyOwnerToAll took 0.000661254 seconds
Rank 14: copyOwnerToAll took 0.000661254 seconds
Rank 15: copyOwnerToAll took 0.000661254 seconds
Rank 16: copyOwnerToAll took 0.000661254 seconds
Rank 17: copyOwnerToAll took 0.000661254 seconds
Rank 18: copyOwnerToAll took 0.000661254 seconds
Rank 19: copyOwnerToAll took 0.000661254 seconds
Rank 20: copyOwnerToAll took 0.000661254 seconds
Rank 21: copyOwnerToAll took 0.000661254 seconds
Rank 22: copyOwnerToAll took 0.000661254 seconds
Rank 23: copyOwnerToAll took 0.000661254 seconds
Rank 24: copyOwnerToAll took 0.000661254 seconds
Rank 25: copyOwnerToAll took 0.000661254 seconds
Rank 26: copyOwnerToAll took 0.000661254 seconds
Rank 27: copyOwnerToAll took 0.000661254 seconds
Average time for copyOwnertoAll is 0.000675774 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
Rank 0's ghost cells:	0	562	463	560	58	99	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	556	0	1117	0	0	1123	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	482	1116	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	562	0	0	0	160	569	379	0	657	0	0	0	0	58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	58	0	0	161	0	0	0	0	14	0	0	0	0	233	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	127	1121	0	570	0	0	1058	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	389	0	1058	0	566	641	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	567	0	406	538	208	508	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	657	12	0	640	407	0	1119	0	0	0	52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	530	1119	0	0	286	730	223	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	208	0	0	0	400	503	13	0	0	0	21	0	0	40	409	437	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	513	0	284	404	0	731	0	0	0	0	689	0	0	197	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	0	0	0	0	0	0	743	487	740	0	454	0	0	0	0	0	0	176	194	0	0	0	0	0	0	
Rank 13's ghost cells:	0	0	0	54	233	0	0	0	45	246	11	0	465	0	0	0	0	0	0	0	0	234	0	0	0	0	0	0	
Rank 14's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	576	0	0	0	731	0	0	0	0	0	0	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	574	0	593	0	0	144	0	0	0	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	601	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	23	698	0	0	0	0	0	0	801	0	408	0	13	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	794	0	311	318	0	114	0	0	0	0	3	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	725	144	0	0	305	0	629	0	0	0	0	0	0	0	
Rank 20's ghost cells:	0	0	0	0	0	0	0	0	0	0	46	196	176	0	0	0	0	394	316	629	0	0	68	0	0	0	0	0	
Rank 21's ghost cells:	0	0	0	0	0	0	0	0	0	0	416	0	190	235	0	0	0	0	0	0	0	0	270	673	0	0	58	0	
Rank 22's ghost cells:	0	0	0	0	0	0	0	0	0	0	437	0	0	0	0	0	0	12	112	0	68	274	0	613	0	0	0	400	
Rank 23's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	675	621	0	0	0	625	81	
Rank 24's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	791	401	605	
Rank 25's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	786	0	0	0	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	0	636	397	0	0	576	
Rank 27's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	400	83	608	0	574	0	
