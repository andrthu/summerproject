Number of nodes is: 7
Number of cores/ranks per node is: 4
Metis Uniform
Cell on rank 0 before loadbalancing: 12607
Cell on rank 1 before loadbalancing: 12675
Cell on rank 2 before loadbalancing: 12672
Cell on rank 3 before loadbalancing: 12731
Cell on rank 4 before loadbalancing: 12713
Cell on rank 5 before loadbalancing: 12678
Cell on rank 6 before loadbalancing: 12686
Cell on rank 7 before loadbalancing: 12658
Cell on rank 8 before loadbalancing: 12671
Cell on rank 9 before loadbalancing: 12664
Cell on rank 10 before loadbalancing: 12718
Cell on rank 11 before loadbalancing: 12717
Cell on rank 12 before loadbalancing: 12643
Cell on rank 13 before loadbalancing: 12650
Cell on rank 14 before loadbalancing: 12691
Cell on rank 15 before loadbalancing: 12765
Cell on rank 16 before loadbalancing: 12566
Cell on rank 17 before loadbalancing: 12756
Cell on rank 18 before loadbalancing: 12742
Cell on rank 19 before loadbalancing: 12759
Cell on rank 20 before loadbalancing: 12669
Cell on rank 21 before loadbalancing: 12729
Cell on rank 22 before loadbalancing: 12722
Cell on rank 23 before loadbalancing: 12756
Cell on rank 24 before loadbalancing: 12655
Cell on rank 25 before loadbalancing: 12615
Cell on rank 26 before loadbalancing: 12820
Cell on rank 27 before loadbalancing: 12679
Edge-cut: 39587


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
From rank 0 to: 	0	698	43	0	52	385	0	0	0	0	502	0	239	20	384	0	0	0	0	0	0	0	0	0	33	29	0	3	
From rank 1 to: 	690	0	676	0	0	0	0	0	0	0	50	0	303	0	123	0	0	0	0	0	185	0	0	0	58	116	0	35	
From rank 2 to: 	43	661	0	0	0	0	0	0	0	0	0	0	347	0	196	0	0	0	0	0	163	0	0	0	0	174	0	206	
From rank 3 to: 	0	0	0	0	821	550	201	0	0	0	0	0	0	0	695	0	0	0	0	0	716	234	0	114	0	6	0	0	
From rank 4 to: 	52	0	0	825	0	275	129	0	0	0	0	0	0	0	45	0	0	0	0	0	472	385	0	0	0	269	0	0	
From rank 5 to: 	384	0	0	554	293	0	560	0	0	0	161	0	356	0	164	0	0	0	0	0	0	0	0	0	0	50	0	0	
From rank 6 to: 	0	0	0	201	130	570	0	0	0	0	0	0	221	297	280	0	42	0	0	0	365	203	391	291	0	42	0	0	
From rank 7 to: 	0	0	0	0	0	0	0	0	0	585	0	751	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	718	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	579	725	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	492	50	0	0	0	161	0	0	0	0	0	894	353	375	0	0	0	0	0	0	0	0	0	0	0	80	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	754	0	142	893	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	251	293	338	0	0	376	241	0	0	0	376	0	0	461	0	0	0	0	0	0	0	0	0	0	230	524	0	268	
From rank 13 to: 	27	0	0	0	0	0	298	0	0	0	390	0	450	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 14 to: 	396	133	172	687	49	159	280	0	0	0	0	0	0	0	0	0	0	0	0	0	350	61	0	109	0	80	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	341	8	808	354	0	500	422	0	0	5	0	0	
From rank 16 to: 	0	0	0	0	0	0	42	0	0	0	0	0	0	0	0	357	0	6	53	195	300	14	368	94	0	156	0	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	24	0	884	780	0	0	0	0	0	0	0	0	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	780	49	880	0	458	0	0	0	0	0	0	0	0	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	362	190	780	424	0	367	0	51	297	0	0	0	0	
From rank 20 to: 	0	176	167	714	519	0	378	0	0	0	0	0	0	0	415	0	300	0	0	383	0	234	0	588	0	245	0	0	
From rank 21 to: 	0	0	0	232	385	0	194	0	0	0	0	0	0	0	48	498	12	0	0	0	200	0	833	202	0	38	0	0	
From rank 22 to: 	0	0	0	0	0	0	398	0	0	0	0	0	0	0	0	455	370	0	0	51	0	883	0	865	0	158	0	0	
From rank 23 to: 	0	0	0	114	0	0	280	0	0	0	0	0	0	0	97	0	108	0	0	297	562	201	839	0	0	0	0	0	
From rank 24 to: 	33	58	0	0	0	0	0	0	0	0	0	0	233	0	0	0	0	0	0	0	0	0	0	0	0	675	98	861	
From rank 25 to: 	34	121	176	6	261	62	51	0	0	0	80	0	518	0	117	5	162	0	0	0	235	35	159	0	692	0	48	240	
From rank 26 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	98	48	0	641	
From rank 27 to: 	3	40	210	0	0	0	0	0	0	0	0	0	304	0	0	0	0	0	0	0	0	0	0	0	850	237	670	0	

After loadbalancing process 0 has 14995 cells.
After loadbalancing process 1 has 14911 cells.
After loadbalancing process 2 has 14462 cells.
After loadbalancing process 3 has 16068 cells.
After loadbalancing process 4 has 15165 cells.
After loadbalancing process 5 has 15200 cells.
After loadbalancing process 6 has 15719 cells.
After loadbalancing process 7 has 13994 cells.
After loadbalancing process 8 has 13389 cells.
After loadbalancing process 9 has 14108 cells.
After loadbalancing process 10 has 15123 cells.
After loadbalancing process 11 has 14506 cells.
After loadbalancing process 12 has 16001 cells.
After loadbalancing process 13 has 13815 cells.
After loadbalancing process 14 has 15167 cells.
After loadbalancing process 15 has 15203 cells.
After loadbalancing process 16 has 14151 cells.
After loadbalancing process 17 has 14447 cells.
After loadbalancing process 18 has 14909 cells.
After loadbalancing process 19 has 15230 cells.
After loadbalancing process 20 has 16788 cells.
After loadbalancing process 21 has 15371 cells.
After loadbalancing process 22 has 15902 cells.
After loadbalancing process 23 has 15254 cells.
After loadbalancing process 24 has 14613 cells.
After loadbalancing process 25 has 15617 cells.
After loadbalancing process 26 has 13607 cells.
After loadbalancing process 27 has 14993 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00878583 seconds
Rank 1: Matrix-vector product took 0.00886742 seconds
Rank 2: Matrix-vector product took 0.00845838 seconds
Rank 3: Matrix-vector product took 0.00947558 seconds
Rank 4: Matrix-vector product took 0.00903851 seconds
Rank 5: Matrix-vector product took 0.0091262 seconds
Rank 6: Matrix-vector product took 0.00909603 seconds
Rank 7: Matrix-vector product took 0.00830735 seconds
Rank 8: Matrix-vector product took 0.00762678 seconds
Rank 9: Matrix-vector product took 0.00822694 seconds
Rank 10: Matrix-vector product took 0.00885859 seconds
Rank 11: Matrix-vector product took 0.0086731 seconds
Rank 12: Matrix-vector product took 0.00923019 seconds
Rank 13: Matrix-vector product took 0.00814852 seconds
Rank 14: Matrix-vector product took 0.00897206 seconds
Rank 15: Matrix-vector product took 0.00903859 seconds
Rank 16: Matrix-vector product took 0.00805583 seconds
Rank 17: Matrix-vector product took 0.00833728 seconds
Rank 18: Matrix-vector product took 0.00871598 seconds
Rank 19: Matrix-vector product took 0.00902882 seconds
Rank 20: Matrix-vector product took 0.00959975 seconds
Rank 21: Matrix-vector product took 0.00906228 seconds
Rank 22: Matrix-vector product took 0.00923749 seconds
Rank 23: Matrix-vector product took 0.0089656 seconds
Rank 24: Matrix-vector product took 0.00857297 seconds
Rank 25: Matrix-vector product took 0.00903497 seconds
Rank 26: Matrix-vector product took 0.00778801 seconds
Rank 27: Matrix-vector product took 0.00854052 seconds
Average time for Matrix-vector product is 0.00874534 seconds

Rank 0: copyOwnerToAll took 0.000772579 seconds
Rank 1: copyOwnerToAll took 0.000772579 seconds
Rank 2: copyOwnerToAll took 0.000772579 seconds
Rank 3: copyOwnerToAll took 0.000772579 seconds
Rank 4: copyOwnerToAll took 0.000772579 seconds
Rank 5: copyOwnerToAll took 0.000772579 seconds
Rank 6: copyOwnerToAll took 0.000772579 seconds
Rank 7: copyOwnerToAll took 0.000772579 seconds
Rank 8: copyOwnerToAll took 0.000772579 seconds
Rank 9: copyOwnerToAll took 0.000772579 seconds
Rank 10: copyOwnerToAll took 0.000772579 seconds
Rank 11: copyOwnerToAll took 0.000772579 seconds
Rank 12: copyOwnerToAll took 0.000772579 seconds
Rank 13: copyOwnerToAll took 0.000772579 seconds
Rank 14: copyOwnerToAll took 0.000772579 seconds
Rank 15: copyOwnerToAll took 0.000772579 seconds
Rank 16: copyOwnerToAll took 0.000772579 seconds
Rank 17: copyOwnerToAll took 0.000772579 seconds
Rank 18: copyOwnerToAll took 0.000772579 seconds
Rank 19: copyOwnerToAll took 0.000772579 seconds
Rank 20: copyOwnerToAll took 0.000772579 seconds
Rank 21: copyOwnerToAll took 0.000772579 seconds
Rank 22: copyOwnerToAll took 0.000772579 seconds
Rank 23: copyOwnerToAll took 0.000772579 seconds
Rank 24: copyOwnerToAll took 0.000772579 seconds
Rank 25: copyOwnerToAll took 0.000772579 seconds
Rank 26: copyOwnerToAll took 0.000772579 seconds
Rank 27: copyOwnerToAll took 0.000772579 seconds
Average time for copyOwnertoAll is 0.000832854 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	Rank 20	Rank 21	Rank 22	Rank 23	Rank 24	Rank 25	Rank 26	Rank 27	
Rank 0's ghost cells:	0	698	43	0	52	385	0	0	0	0	502	0	239	20	384	0	0	0	0	0	0	0	0	0	33	29	0	3	
Rank 1's ghost cells:	690	0	676	0	0	0	0	0	0	0	50	0	303	0	123	0	0	0	0	0	185	0	0	0	58	116	0	35	
Rank 2's ghost cells:	43	661	0	0	0	0	0	0	0	0	0	0	347	0	196	0	0	0	0	0	163	0	0	0	0	174	0	206	
Rank 3's ghost cells:	0	0	0	0	821	550	201	0	0	0	0	0	0	0	695	0	0	0	0	0	716	234	0	114	0	6	0	0	
Rank 4's ghost cells:	52	0	0	825	0	275	129	0	0	0	0	0	0	0	45	0	0	0	0	0	472	385	0	0	0	269	0	0	
Rank 5's ghost cells:	384	0	0	554	293	0	560	0	0	0	161	0	356	0	164	0	0	0	0	0	0	0	0	0	0	50	0	0	
Rank 6's ghost cells:	0	0	0	201	130	570	0	0	0	0	0	0	221	297	280	0	42	0	0	0	365	203	391	291	0	42	0	0	
Rank 7's ghost cells:	0	0	0	0	0	0	0	0	0	585	0	751	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	718	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	579	725	0	0	140	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 10's ghost cells:	492	50	0	0	0	161	0	0	0	0	0	894	353	375	0	0	0	0	0	0	0	0	0	0	0	80	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	754	0	142	893	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	251	293	338	0	0	376	241	0	0	0	376	0	0	461	0	0	0	0	0	0	0	0	0	0	230	524	0	268	
Rank 13's ghost cells:	27	0	0	0	0	0	298	0	0	0	390	0	450	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 14's ghost cells:	396	133	172	687	49	159	280	0	0	0	0	0	0	0	0	0	0	0	0	0	350	61	0	109	0	80	0	0	
Rank 15's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	341	8	808	354	0	500	422	0	0	5	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	42	0	0	0	0	0	0	0	0	357	0	6	53	195	300	14	368	94	0	156	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	24	0	884	780	0	0	0	0	0	0	0	0	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	780	49	880	0	458	0	0	0	0	0	0	0	0	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	362	190	780	424	0	367	0	51	297	0	0	0	0	
Rank 20's ghost cells:	0	176	167	714	519	0	378	0	0	0	0	0	0	0	415	0	300	0	0	383	0	234	0	588	0	245	0	0	
Rank 21's ghost cells:	0	0	0	232	385	0	194	0	0	0	0	0	0	0	48	498	12	0	0	0	200	0	833	202	0	38	0	0	
Rank 22's ghost cells:	0	0	0	0	0	0	398	0	0	0	0	0	0	0	0	455	370	0	0	51	0	883	0	865	0	158	0	0	
Rank 23's ghost cells:	0	0	0	114	0	0	280	0	0	0	0	0	0	0	97	0	108	0	0	297	562	201	839	0	0	0	0	0	
Rank 24's ghost cells:	33	58	0	0	0	0	0	0	0	0	0	0	233	0	0	0	0	0	0	0	0	0	0	0	0	675	98	861	
Rank 25's ghost cells:	34	121	176	6	261	62	51	0	0	0	80	0	518	0	117	5	162	0	0	0	235	35	159	0	692	0	48	240	
Rank 26's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	98	48	0	641	
Rank 27's ghost cells:	3	40	210	0	0	0	0	0	0	0	0	0	304	0	0	0	0	0	0	0	0	0	0	0	850	237	670	0	
