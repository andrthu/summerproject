Number of nodes is: 5
Number of cores/ranks per node is: 4
Metis Node Cell
 Cell on rank 0 before loadbalancing: 8896
Cell on rank 1 before loadbalancing: 8893
Cell on rank 2 before loadbalancing: 8861
Cell on rank 3 before loadbalancing: 8892
Cell on rank 4 before loadbalancing: 8889
Edge-cut: 2096


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	248	214	77	201	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	229	0	39	193	170	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	224	41	0	152	0	73	0	0	0	0	0	0	0	36	0	126	0	0	0	0	
From rank 3 to: 	77	189	156	0	0	0	0	0	0	0	0	0	130	124	91	48	0	0	0	0	
From rank 4 to: 	200	165	0	0	0	148	115	220	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 5 to: 	76	5	71	0	150	0	119	34	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	115	116	0	232	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	0	0	0	0	235	32	227	0	0	0	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	0	0	0	0	0	0	0	0	0	207	60	174	50	0	0	0	25	0	0	0	
From rank 9 to: 	0	0	0	0	0	0	0	0	197	0	0	124	144	104	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	60	0	0	177	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	175	123	181	0	0	0	0	0	0	0	0	0	
From rank 12 to: 	0	0	0	128	0	0	0	0	47	148	0	0	0	200	26	60	15	0	0	0	
From rank 13 to: 	0	0	36	129	0	0	0	0	0	106	0	0	207	0	11	112	0	0	0	0	
From rank 14 to: 	0	0	0	89	0	0	0	0	0	0	0	0	34	12	0	256	197	0	0	0	
From rank 15 to: 	0	0	125	53	0	0	0	0	0	0	0	0	68	110	259	0	50	68	0	0	
From rank 16 to: 	0	0	0	0	0	0	0	0	35	0	0	0	15	0	197	50	0	173	16	0	
From rank 17 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	159	0	166	36	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	167	0	185	
From rank 19 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	186	0	

After loadbalancing process 0 has 3016 cells.
After loadbalancing process 1 has 2875 cells.
After loadbalancing process 2 has 2859 cells.
After loadbalancing process 3 has 3061 cells.
After loadbalancing process 4 has 3091 cells.
After loadbalancing process 5 has 2656 cells.
After loadbalancing process 6 has 2669 cells.
After loadbalancing process 7 has 2737 cells.
After loadbalancing process 8 has 2726 cells.
After loadbalancing process 9 has 2781 cells.
After loadbalancing process 10 has 2473 cells.
After loadbalancing process 11 has 2682 cells.
After loadbalancing process 12 has 2868 cells.
After loadbalancing process 13 has 2812 cells.
After loadbalancing process 14 has 2808 cells.
After loadbalancing process 15 has 2950 cells.
After loadbalancing process 16 has 2690 cells.
After loadbalancing process 17 has 2631 cells.
After loadbalancing process 18 has 2611 cells.
After loadbalancing process 19 has 2464 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00176181 seconds
Rank 1: Matrix-vector product took 0.00168611 seconds
Rank 2: Matrix-vector product took 0.00161628 seconds
Rank 3: Matrix-vector product took 0.00182528 seconds
Rank 4: Matrix-vector product took 0.0018191 seconds
Rank 5: Matrix-vector product took 0.00148142 seconds
Rank 6: Matrix-vector product took 0.00152565 seconds
Rank 7: Matrix-vector product took 0.00158917 seconds
Rank 8: Matrix-vector product took 0.00159469 seconds
Rank 9: Matrix-vector product took 0.00160369 seconds
Rank 10: Matrix-vector product took 0.00136497 seconds
Rank 11: Matrix-vector product took 0.00153404 seconds
Rank 12: Matrix-vector product took 0.0017179 seconds
Rank 13: Matrix-vector product took 0.00163837 seconds
Rank 14: Matrix-vector product took 0.0016727 seconds
Rank 15: Matrix-vector product took 0.00170141 seconds
Rank 16: Matrix-vector product took 0.00157432 seconds
Rank 17: Matrix-vector product took 0.00155902 seconds
Rank 18: Matrix-vector product took 0.00152811 seconds
Rank 19: Matrix-vector product took 0.00134724 seconds
Average time for Matrix-vector product is 0.00160706 seconds

Rank 0: copyOwnerToAll took 0.000259118 seconds
Rank 1: copyOwnerToAll took 0.000259118 seconds
Rank 2: copyOwnerToAll took 0.000259118 seconds
Rank 3: copyOwnerToAll took 0.000259118 seconds
Rank 4: copyOwnerToAll took 0.000259118 seconds
Rank 5: copyOwnerToAll took 0.000259118 seconds
Rank 6: copyOwnerToAll took 0.000259118 seconds
Rank 7: copyOwnerToAll took 0.000259118 seconds
Rank 8: copyOwnerToAll took 0.000259118 seconds
Rank 9: copyOwnerToAll took 0.000259118 seconds
Rank 10: copyOwnerToAll took 0.000259118 seconds
Rank 11: copyOwnerToAll took 0.000259118 seconds
Rank 12: copyOwnerToAll took 0.000259118 seconds
Rank 13: copyOwnerToAll took 0.000259118 seconds
Rank 14: copyOwnerToAll took 0.000259118 seconds
Rank 15: copyOwnerToAll took 0.000259118 seconds
Rank 16: copyOwnerToAll took 0.000259118 seconds
Rank 17: copyOwnerToAll took 0.000259118 seconds
Rank 18: copyOwnerToAll took 0.000259118 seconds
Rank 19: copyOwnerToAll took 0.000259118 seconds
Average time for copyOwnertoAll is 0.000330755 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	248	214	77	201	72	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	229	0	39	193	170	5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	224	41	0	152	0	73	0	0	0	0	0	0	0	36	0	126	0	0	0	0	
Rank 3's ghost cells:	77	189	156	0	0	0	0	0	0	0	0	0	130	124	91	48	0	0	0	0	
Rank 4's ghost cells:	200	165	0	0	0	148	115	220	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 5's ghost cells:	76	5	71	0	150	0	119	34	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	115	116	0	232	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 7's ghost cells:	0	0	0	0	235	32	227	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	0	0	0	0	0	207	60	174	50	0	0	0	25	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	197	0	0	124	144	104	0	0	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	60	0	0	177	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	175	123	181	0	0	0	0	0	0	0	0	0	
Rank 12's ghost cells:	0	0	0	128	0	0	0	0	47	148	0	0	0	200	26	60	15	0	0	0	
Rank 13's ghost cells:	0	0	36	129	0	0	0	0	0	106	0	0	207	0	11	112	0	0	0	0	
Rank 14's ghost cells:	0	0	0	89	0	0	0	0	0	0	0	0	34	12	0	256	197	0	0	0	
Rank 15's ghost cells:	0	0	125	53	0	0	0	0	0	0	0	0	68	110	259	0	50	68	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	35	0	0	0	15	0	197	50	0	173	16	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	70	159	0	166	36	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	16	167	0	185	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	186	0	
