Number of nodes is: 5
Number of cores/ranks per node is: 4
METIS norne  partitioner with metis node partitioner
Cell on rank 0 before loadbalancing: 2219
Cell on rank 1 before loadbalancing: 2220
Cell on rank 2 before loadbalancing: 2231
Cell on rank 3 before loadbalancing: 2207
Cell on rank 4 before loadbalancing: 2211
Cell on rank 5 before loadbalancing: 2242
Cell on rank 6 before loadbalancing: 2228
Cell on rank 7 before loadbalancing: 2199
Cell on rank 8 before loadbalancing: 2220
Cell on rank 9 before loadbalancing: 2201
Cell on rank 10 before loadbalancing: 2229
Cell on rank 11 before loadbalancing: 2241
Cell on rank 12 before loadbalancing: 2203
Cell on rank 13 before loadbalancing: 2199
Cell on rank 14 before loadbalancing: 2202
Cell on rank 15 before loadbalancing: 2243
Cell on rank 16 before loadbalancing: 2207
Cell on rank 17 before loadbalancing: 2243
Cell on rank 18 before loadbalancing: 2243
Cell on rank 19 before loadbalancing: 2243
Edge-cut: 7050


			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
From rank 0 to: 	0	195	56	37	215	0	0	114	133	0	0	0	0	0	0	0	0	0	0	0	
From rank 1 to: 	190	0	30	186	0	0	0	8	32	260	0	0	0	0	0	0	0	0	0	0	
From rank 2 to: 	57	30	0	150	123	0	0	0	0	0	0	0	237	0	47	0	0	11	0	0	
From rank 3 to: 	30	190	151	0	150	0	0	0	0	0	0	0	14	0	0	0	0	153	0	4	
From rank 4 to: 	216	0	134	149	0	0	0	0	0	0	0	0	0	0	0	0	0	72	0	63	
From rank 5 to: 	0	0	0	0	0	0	228	0	56	216	0	0	0	0	0	0	0	0	0	0	
From rank 6 to: 	0	0	0	0	0	237	0	59	159	12	0	0	0	0	0	0	0	0	0	0	
From rank 7 to: 	113	11	0	0	0	0	67	0	208	7	0	0	0	0	0	0	0	0	0	0	
From rank 8 to: 	132	32	0	0	0	55	159	183	0	234	0	0	0	0	0	0	0	0	0	0	
From rank 9 to: 	0	260	0	0	0	212	12	7	235	0	0	0	0	0	0	0	0	0	0	0	
From rank 10 to: 	0	0	0	0	0	0	0	0	0	0	0	168	0	0	0	0	0	0	0	0	
From rank 11 to: 	0	0	0	0	0	0	0	0	0	0	166	0	0	100	145	0	0	0	0	0	
From rank 12 to: 	0	0	244	18	0	0	0	0	0	0	0	0	0	202	93	0	0	57	4	0	
From rank 13 to: 	0	0	0	0	0	0	0	0	0	0	0	112	216	0	213	0	0	44	46	0	
From rank 14 to: 	0	0	47	0	0	0	0	0	0	0	0	139	93	221	0	0	0	18	0	0	
From rank 15 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	201	0	212	61	
From rank 16 to: 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	202	0	0	0	0	
From rank 17 to: 	0	0	8	152	72	0	0	0	0	0	0	0	43	33	18	0	0	0	135	110	
From rank 18 to: 	0	0	0	0	0	0	0	0	0	0	0	0	4	40	0	219	0	135	0	125	
From rank 19 to: 	0	0	0	3	62	0	0	0	0	0	0	0	0	0	0	59	0	115	131	0	

Edge-cut for node partition: 2542


After loadbalancing process 0 has 2742 cells.
After loadbalancing process 1 has 2695 cells.
After loadbalancing process 2 has 3015 cells.
After loadbalancing process 3 has 2927 cells.
After loadbalancing process 4 has 2969 cells.
After loadbalancing process 5 has 2926 cells.
After loadbalancing process 6 has 2845 cells.
After loadbalancing process 7 has 2605 cells.
After loadbalancing process 8 has 2899 cells.
After loadbalancing process 9 has 2717 cells.
After loadbalancing process 10 has 2409 cells.
After loadbalancing process 11 has 2766 cells.
After loadbalancing process 12 has 2886 cells.
After loadbalancing process 13 has 2821 cells.
After loadbalancing process 14 has 2814 cells.
After loadbalancing process 15 has 2613 cells.
After loadbalancing process 16 has 2397 cells.
After loadbalancing process 17 has 2652 cells.
After loadbalancing process 18 has 2830 cells.
After loadbalancing process 19 has 2720 cells.


=== rate=0, T=0, TIT=0, IT=0

 Elapsed time: 0
Rank 0: Matrix-vector product took 0.00160673 seconds
Rank 1: Matrix-vector product took 0.00154003 seconds
Rank 2: Matrix-vector product took 0.00176459 seconds
Rank 3: Matrix-vector product took 0.00171841 seconds
Rank 4: Matrix-vector product took 0.00170615 seconds
Rank 5: Matrix-vector product took 0.00172107 seconds
Rank 6: Matrix-vector product took 0.00166107 seconds
Rank 7: Matrix-vector product took 0.00142907 seconds
Rank 8: Matrix-vector product took 0.00172965 seconds
Rank 9: Matrix-vector product took 0.00157077 seconds
Rank 10: Matrix-vector product took 0.00134081 seconds
Rank 11: Matrix-vector product took 0.00160443 seconds
Rank 12: Matrix-vector product took 0.00173305 seconds
Rank 13: Matrix-vector product took 0.00162806 seconds
Rank 14: Matrix-vector product took 0.00168835 seconds
Rank 15: Matrix-vector product took 0.00147782 seconds
Rank 16: Matrix-vector product took 0.00132717 seconds
Rank 17: Matrix-vector product took 0.0015281 seconds
Rank 18: Matrix-vector product took 0.00166197 seconds
Rank 19: Matrix-vector product took 0.00160634 seconds
Average time for Matrix-vector product is 0.00160218 seconds

Rank 0: copyOwnerToAll took 0.000282135 seconds
Rank 1: copyOwnerToAll took 0.000282135 seconds
Rank 2: copyOwnerToAll took 0.000282135 seconds
Rank 3: copyOwnerToAll took 0.000282135 seconds
Rank 4: copyOwnerToAll took 0.000282135 seconds
Rank 5: copyOwnerToAll took 0.000282135 seconds
Rank 6: copyOwnerToAll took 0.000282135 seconds
Rank 7: copyOwnerToAll took 0.000282135 seconds
Rank 8: copyOwnerToAll took 0.000282135 seconds
Rank 9: copyOwnerToAll took 0.000282135 seconds
Rank 10: copyOwnerToAll took 0.000282135 seconds
Rank 11: copyOwnerToAll took 0.000282135 seconds
Rank 12: copyOwnerToAll took 0.000282135 seconds
Rank 13: copyOwnerToAll took 0.000282135 seconds
Rank 14: copyOwnerToAll took 0.000282135 seconds
Rank 15: copyOwnerToAll took 0.000282135 seconds
Rank 16: copyOwnerToAll took 0.000282135 seconds
Rank 17: copyOwnerToAll took 0.000282135 seconds
Rank 18: copyOwnerToAll took 0.000282135 seconds
Rank 19: copyOwnerToAll took 0.000282135 seconds
Average time for copyOwnertoAll is 0.000390793 seconds



			Rank 0	Rank 1	Rank 2	Rank 3	Rank 4	Rank 5	Rank 6	Rank 7	Rank 8	Rank 9	Rank 10	Rank 11	Rank 12	Rank 13	Rank 14	Rank 15	Rank 16	Rank 17	Rank 18	Rank 19	
Rank 0's ghost cells:	0	228	56	216	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 1's ghost cells:	237	0	159	12	0	0	0	59	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 2's ghost cells:	55	159	0	234	132	32	0	183	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 3's ghost cells:	212	12	235	0	0	260	0	7	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 4's ghost cells:	0	0	133	0	0	195	215	114	37	0	0	0	56	0	0	0	0	0	0	0	
Rank 5's ghost cells:	0	0	32	260	190	0	0	8	186	0	0	0	30	0	0	0	0	0	0	0	
Rank 6's ghost cells:	0	0	0	0	216	0	0	0	149	0	0	0	134	0	72	63	0	0	0	0	
Rank 7's ghost cells:	0	67	208	7	113	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
Rank 8's ghost cells:	0	0	0	0	30	190	150	0	0	0	0	0	151	14	153	4	0	0	0	0	
Rank 9's ghost cells:	0	0	0	0	0	0	0	0	0	0	201	212	0	0	0	61	0	0	0	0	
Rank 10's ghost cells:	0	0	0	0	0	0	0	0	0	202	0	0	0	0	0	0	0	0	0	0	
Rank 11's ghost cells:	0	0	0	0	0	0	0	0	0	219	0	0	0	4	135	125	0	0	40	0	
Rank 12's ghost cells:	0	0	0	0	57	30	123	0	150	0	0	0	0	237	11	0	0	0	0	47	
Rank 13's ghost cells:	0	0	0	0	0	0	0	0	18	0	0	4	244	0	57	0	0	0	202	93	
Rank 14's ghost cells:	0	0	0	0	0	0	72	0	152	0	0	135	8	43	0	110	0	0	33	18	
Rank 15's ghost cells:	0	0	0	0	0	0	62	0	3	59	0	131	0	0	115	0	0	0	0	0	
Rank 16's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	168	0	0	
Rank 17's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	166	0	100	145	
Rank 18's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	46	0	216	44	0	0	112	0	213	
Rank 19's ghost cells:	0	0	0	0	0	0	0	0	0	0	0	0	47	93	18	0	0	139	221	0	
