/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_READINI_HEADER_INCLUDED
#define OPM_READINI_HEADER_INCLUDED

#endif // OPM_READINI_HEADER_INCLUDED

class DictRead
{
public:
    std::map<int,std::string> dict;

    void read_file_and_update(char* innit)
    {
	std::ifstream myfile {innit};
    
	std::string b;
	int a;
	while (myfile >> a >> b)
	{
	    dict[a] = b;
      
	}
    }
    void write_param()
    {
	for (int i = 0; i < dict.size(); ++i)
	{
	    std::cout << i << ": " << dict[i] << " ";
	}
	std::cout << std::endl;
    }

    DictRead ()
    {
	dict[0]  = std::string("0.0001"); // solver tolerance
	dict[1]  = std::string("200");    // max linear Iteration
	dict[2]  = std::string("1");      // Partition lib 0=metis,1=scotch,2=zoltan
	dict[3]  = std::string("10");     // x dimension
	dict[4]  = std::string("10");     // y dimension
	dict[5]  = std::string("3");      // z dimension
	dict[6]  = std::string("1");      // 1 for node partition
	dict[7]  = std::string("1");      // Num nodes           
    }
};
