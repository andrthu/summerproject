/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_READECLIPSECASE_HEADER_INCLUDED
#define OPM_READECLIPSECASE_HEADER_INCLUDED

#endif // OPM_READECLIPSECASE_HEADER_INCLUDED

#include <opm/common/utility/parameters/ParameterGroup.hpp>
#include <opm/parser/eclipse/Parser/Parser.hpp>
#include <opm/parser/eclipse/Parser/ParseContext.hpp>
#include <opm/parser/eclipse/Deck/Deck.hpp>
#include <opm/parser/eclipse/EclipseState/EclipseState.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Schedule.hpp>
#include <opm/parser/eclipse/Units/Units.hpp>
#include <opm/parser/eclipse/EclipseState/checkDeck.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/Fault.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/FaultFace.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/FaultCollection.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Well/Well.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/TimeMap.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Well/WellProductionProperties.hpp>

#include <opm/autodiff/createGlobalCellArray.hpp>
#include <opm/material/fluidmatrixinteractions/EclMaterialLawManager.hpp>
#include <opm/core/props/satfunc/RelpermDiagnostics.hpp>
#include <opm/autodiff/ParallelOverlappingILU0.hpp>


#include <opm/parser/eclipse/EclipseState/Grid/TransMult.hpp>
#include <opm/parser/eclipse/Parser/ErrorGuard.hpp>
#include <opm/grid/cpgrid/CartesianIndexMapper.hpp>

#include <opm/grid/common/ZoltanGraphFunctions.hpp>

class ReadProperties {
private:
    Opm::Parser parser_;
    Opm::ParameterGroup param_;
    
    std::shared_ptr<Opm::Deck> deck_;
    std::shared_ptr<Opm::EclipseState> eclipse_state_;
    std::shared_ptr<Opm::Schedule> schedule_;

    std::vector<double> trans_;
    std::vector<double> perm_;
    
    std::vector<Dune::FieldMatrix<double,3,3>> perm2_;
    void calcPerm_(const Dune::CpGrid& grid)
    {
	const auto& props = eclipse_state_->get3DProperties();
	unsigned numCells = grid.numCells();
	perm2_.resize(numCells);

	const std::vector<double>& permxData = props.getDoubleGridProperty("PERMX").getData();
	//std::vector<double> permyData(permxData);
	const std::vector<double>& permyData = props.getDoubleGridProperty("PERMY").getData();
	const std::vector<double>& permzData = props.getDoubleGridProperty("PERMZ").getData();

	//Dune::CartesianIndexMapper<Dune::CpGrid> cartmap(grid);
	std::vector<int> compressedToCartesianIdx;
	Opm::createGlobalCellArray(grid, compressedToCartesianIdx);

	for (unsigned dofIdx = 0; dofIdx < numCells; ++ dofIdx) {
	    unsigned cartesianElemIdx = compressedToCartesianIdx[dofIdx];
	    perm2_[dofIdx] = 0.0;
	    perm2_[dofIdx][0][0] = permxData[cartesianElemIdx];
	    perm2_[dofIdx][1][1] = permyData[cartesianElemIdx];
	    perm2_[dofIdx][2][2] = permzData[cartesianElemIdx];
	}
    }
    //multNTG(grid, *eclipse_state_, ntg, porv, actnum);
    void multNTG(const Dune::CpGrid& grid, Opm::EclipseState& eclState, 
		 std::vector<double>& ntg, const std::vector<double>& porv, 
		 const std::vector<int>& actnum)
    {
	
	auto eclgrid = eclState.getInputGrid();
	const int * cartdims = Opm::UgGridHelpers::cartDims(grid);
	
	auto gid = grid.globalIdSet();
	auto gridView = grid.leafGridView();
	
	//const int * globCell = Opm::UgGridHelpers::globalCell(grid);
	const int * globCell =Opm::UgGridHelpers::globalCell(grid);
	
	for (int cellItr = 0; cellItr < grid.numCells(); ++cellItr)
	{
	    int cell = globCell[cellItr];//gid.id(e);

	    const int nx = cartdims[0];
	    const int ny = cartdims[1];
	    const double volume = eclgrid.getCellVolume(cell);

	    ntg[cell] *= volume;
	    double totalCellVolume = volume;
	    
	    int upperCell = cell - nx*ny;
	    while(upperCell > 0  && actnum[upperCell] > 0 && porv[upperCell] < eclgrid.getMinpvVector()[upperCell])
	    {
		const double upperVolume = eclgrid.getCellVolume(upperCell);
		totalCellVolume += upperVolume;
		ntg[cell] += ntg[upperCell]*upperVolume;
		
		upperCell -= nx*ny;
	    }
	    ntg[cell]/= totalCellVolume;
	}
    }

    void calcTrans_(const Dune::CpGrid& grid, Opm::EclipseState& eclState, std::vector<double> ntg)
    {
	//auto eclgrid = getES();
	auto eclGrid = eclState.getInputGrid();
	auto gridView = grid.leafGridView();

	const auto & mult = eclipse_state_->getTransMult();
	trans_.resize(grid.numFaces(), 0.0);
       
	auto cell2Face = Opm::UgGridHelpers::cell2Faces(grid);
	
	std::vector<double> htran(grid.numFaces(), 0.0);
	std::vector<double> vmult(grid.numFaces(), 1.0);
	

	const int * globCell = Opm::UgGridHelpers::globalCell(grid);
	auto faceCells = Opm::UgGridHelpers::faceCells(grid);
	//auto faceCells = Opm::AutoDiffGrid::faceCells(grid);

	for (int cell = 0; cell < grid.numCells(); ++cell)
	{
	    int cellForMult = globCell[cell];
	    auto cellCenter = eclGrid.getCellCenter(cellForMult);
	    
	    const auto faceRow = grid.cellFaceRow(cell);
	    auto faceRange = cell2Face[cell];

	    for (auto it=faceRange.begin(); it!=faceRange.end();++it)
	    {
		const int tag=grid.faceTag(it);
		Opm::FaceDir::DirEnum faceDir;
		if (tag == 0)
		{
		    faceDir = Opm::FaceDir::XMinus;
		}
		else if (tag == 1)
		{
		    faceDir = Opm::FaceDir::XPlus;
		}
		else if (tag == 2)
		{
		    faceDir = Opm::FaceDir::YMinus;
		}
		else if (tag == 3)
		{
		    faceDir = Opm::FaceDir::YPlus;
		}
		else if (tag == 4)
		{
		    faceDir = Opm::FaceDir::ZMinus;
		}
		else if (tag == 5)
		{
		    faceDir = Opm::FaceDir::ZPlus;
		}
		int face = *it;
		int d = std::floor(tag/2);
		
		const auto& faceCenter = Opm::UgGridHelpers::faceCenterEcl(grid,cell,tag);
		const auto& faceNormal = Opm::UgGridHelpers::faceAreaNormalEcl(grid,face);
		
		double faceArea = grid.faceArea(face);
		
		const int faceCell0 = grid.faceCell(face, 0);
		const int faceCell1 = grid.faceCell(face, 1);
		
		bool inside = faceCell0==cell;
		
		const int neighbor = faceCell0==cell ? faceCell1 : faceCell0;
		
		if (true)//neighbor!=-1)
		{
		    const int nab = neighbor;
		
		    double val  = 0.0;
		    double dist = 0.0;
		    for (int k = 0; k < 3; ++k)
		    {
			const double Ci = faceCenter[k]-cellCenter[k];
			dist += Ci*Ci;
			val += Ci*faceNormal[k];
		    }
		    //long double T_in = perm[cell*9+d]*std::abs(val)/dist;
		    long double T_in = perm2_[cell][d][d]*std::abs(val)/dist;
		    if (tag < 4)
		    {
			T_in *= ntg[cellForMult];
		    }
		    if (T_in != 0)
			htran[face] += 1.0/T_in;
		    else
		    {
			vmult[face] = 0;
		    }
		    
		    vmult[face]*= mult.getMultiplier(cellForMult, faceDir);
		    
		    const int cellIn = faceCells(face,0);
		    const int cellOut = faceCells(face,1);
		    
		    int globIn = globCell[cellIn];
		    int globOut = globCell[cellOut];
		    
		    if (inside && nab != -1)
		    {
			vmult[face] *= mult.getRegionMultiplier(cellForMult,globCell[nab],faceDir);
		    }
		}
	    }
	}
	for (unsigned i = 0; i < grid.numFaces(); ++i) {
	    if (htran[i]!=0)
		trans_[i] = vmult[i] * 1.0/htran[i];
	}
    }

public:

    const double *  getTransmissibility()
    {
	return trans_.data();
	//return geology_->transmissibility().data();
    }

    std::vector<double> getTransVec()
    {
	return trans_;
    }

    const double * getPermeability()
    {
	return perm_.data();
	//return fluid_props_->permeability();//.data();
    }

    Opm::Schedule getSchedule()
    {
	return *schedule_;
    }
    
    std::vector<const Opm::Well*> getWells()
    {
	return schedule_->getWells();
    }
    
    std::vector<double> getNTG()
    {
	return eclipse_state_->get3DProperties().getDoubleGridProperty("NTG").getData();
    }
    const std::vector<double> getPORV()
    {
	return eclipse_state_->get3DProperties().getDoubleGridProperty("PORV").getData();
    }

    const std::vector<int> getACTNUM()
    {
	return eclipse_state_->get3DProperties().getIntGridProperty("ACTNUM").getData();
    }
  
    Opm::EclipseState getES()
    {
	return *eclipse_state_;
    }

    Opm::TransMult getTm()
    {
	return eclipse_state_->getTransMult();
    }

    Opm::EclipseGrid getEclGrid()
    {
	return eclipse_state_->getInputGrid();
    }
  
    void init_grid(Dune::CpGrid& grid, char * deck_name, bool isRoot)
    {	
	const double * trans;
	if(!isRoot)
	{
	    return;
	}
  
	//Create Parameter
	int argc=0;
	char ** argv = 0;

	param_= Opm::ParameterGroup(argc,argv,false,false);

	//Create Parser
	Opm::ParseContext parseContext( { {Opm::ParseContext::PARSE_RANDOM_SLASH,Opm::InputError::IGNORE},
		    {Opm::ParseContext::PARSE_MISSING_DIMS_KEYWORD,Opm::InputError::WARN},
			{Opm::ParseContext::SUMMARY_UNKNOWN_WELL,Opm::InputError::WARN},
			    {Opm::ParseContext::SUMMARY_UNKNOWN_GROUP,Opm::InputError::WARN}} );

	Opm::ErrorGuard errorGuard;
	//create and read input deck with parser
	deck_ = std::make_shared<Opm::Deck>(parser_.parseFile(deck_name, parseContext, errorGuard));
	
	Opm::checkDeck(*deck_,parser_,parseContext,errorGuard);

	//create Eclipse state
	eclipse_state_.reset(new Opm::EclipseState(*deck_, parseContext, errorGuard));
	
	//Create Scedule, includes wells
	schedule_.reset(new Opm::Schedule(*deck_,*eclipse_state_,
					  parseContext, errorGuard));
  
	//get some propertes from eclipse state
	const std::vector<double>& porv = eclipse_state_->get3DProperties().getDoubleGridProperty("PORV").getData();
	
	//get grid from deck.
	grid.processEclipseFormat(eclipse_state_->getInputGrid(), false, false, 
				  false, porv, eclipse_state_->getInputNNC());

	for (int k = 0; k < grid.numCells(); ++k)
	{
	    std::array<double,3> dims = eclipse_state_->getInputGrid().getCellDims(k);

	}
	//compressed array for material law
	std::vector<int> compressedToCartesianIdx;
	Opm::createGlobalCellArray(grid, compressedToCartesianIdx);

	std::vector<double> ntg_ = getNTG();
	std::vector<double> ntg(ntg_.size());
	for (unsigned j = 0; j < ntg_.size(); ++j)
	    ntg[j] = ntg_[j];
	const auto actnum = eclipse_state_->get3DProperties().getIntGridProperty("ACTNUM").getData();
	
	calcPerm_(grid);
	multNTG(grid, *eclipse_state_, ntg, porv, actnum);
	calcTrans_(grid, *eclipse_state_, ntg);
    }
};
