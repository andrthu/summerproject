/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_DISPLAYPARTITION_HEADER_INCLUDED
#define OPM_DISPLAYPARTITION_HEADER_INCLUDED

#endif // OPM_DISPLAYPARTITION_HEADER_INCLUDED

#include <dune/grid/io/file/vtk/vtkwriter.hh>

template<class Grid, class Vec,class CC>
void display_partition_VTK(Grid& grid, Vec& cell_part, const CC& cc)
{
    if (cc.size() > 1)
	grid.switchToGlobalView();

    typedef typename Grid::LeafGridView GridView;
    GridView gv = grid.leafGridView();

    // Create VTK writer class
    Dune::VTKWriter<GridView> vtkWriter(gv);

    // Add partition result
    vtkWriter.addCellData(cell_part, "part");
    
    if (cc.rank() == 0) {

	// Define file name
	std::string name = "partition";
	name += std::to_string(cc.size());
	// Write to file
	vtkWriter.write(name);
    }
}
