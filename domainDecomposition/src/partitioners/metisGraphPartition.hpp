/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_METISGRAPHPARTITION_HEADER_INCLUDED
#define OPM_METISGRAPHPARTITION_HEADER_INCLUDED

#endif // OPM_METISGRAPHPARTITION_HEADER_INCLUDED

//Method counting edges in graph
template<class Grid>
idx_t numMETISGraphEdges(const Grid& grid)
{
    auto gv = grid.leafGridView();
    auto lid = grid.localIdSet();
    idx_t numE = 0;

    // Add code for counting edges
    //iterative
    for (auto && e: elements(gv)){
      for (auto && face: intersections(gv,e)){
	if (face.neighbor()){
	numE++;
	}
      }
    }
    
    return numE;
}
 
// Set up graph using CSR data structure.
template<class Grid>
void buildMETISGraph(const Grid& grid, idx_t* xadj, idx_t* yadj)
{
  int auxy=0; //i-th neighbor
  int auxx=0; 
  auto lid = grid.localIdSet();
  auto gv=grid.leafGridView();
    // add code for building CSR graph format.
  for (auto &&e: elements(gv)){
    xadj[auxx]=auxy;
    auxx++;
    for (auto &&face: intersections(gv,e)){
      if( face.neighbor()){
	int id_Nab=lid.id(face.outside());
	yadj[auxy]=id_Nab;
	auxy++;
      }
    }


  }
  xadj[auxx]=auxy;
}

void set_options(idx_t* options, int tol)
{

    METIS_SetDefaultOptions(options);
    
    // *** Imbalance tolerance in %%. Default is 30%%. Set to 300%%
    options[METIS_OPTION_UFACTOR] = 10;

    // *** Coarsening method
    options[METIS_OPTION_CTYPE] = METIS_CTYPE_RM;
    //options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM;
    
    
    // *** Partitioning method for coarsest graph
    options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_RANDOM;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_EDGE;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_NODE;
    
    
    // *** Refinement method
    options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_GREEDY;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_SEP2SIDED;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_SEP1SIDED;
    
}
 
template<class Grid>
void CpGrid_METIS_partition(const Grid& grid, int mpi_size, std::vector<int>& cell_part, int tol = 300)
{
    // Set number of vertices and edges in graph
  idx_t numVertex = grid.numCells();
  idx_t numEdges = numMETISGraphEdges(grid);

  // Define vectors used to represent graph in CSR format
  idx_t xadj[numVertex + 1];
  idx_t yadj[numEdges];
  // Build graph
  buildMETISGraph(grid, xadj, yadj);
    
  // Set options for partitioning function
  idx_t balCon = 1;
  idx_t nparts = mpi_size;
  idx_t options[METIS_NOPTIONS];
  set_options(options, tol);
  
  // Define output arguments for partitioning
  idx_t objval;
  idx_t part[numVertex];

  // Partitin graph
  if (mpi_size>0)
    METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, NULL, &nparts,
			NULL, NULL, options, &objval, part);
  // Count and print number of cells per subgraph after partitioning
  std::vector<int> numCellsPerPart(nparts, 0);
  for (idx_t i = 0; i < numVertex; ++i)
    {
      //std::cout<<part[i] << std::endl;
	numCellsPerPart[part[i]]++;
	cell_part[i] = part[i];
    }
  for (int k = 0; k < nparts; ++k)
    std::cout << "Cell on rank " << k << " before loadbalancing: " << numCellsPerPart[k] << std::endl;
  
  // Print edge-cut of partitioning.
  std::cout << "Edge-cut: " << objval << std::endl;
}
