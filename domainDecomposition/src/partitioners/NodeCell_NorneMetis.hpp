#ifndef OPM_NODECELL_NORNEMETIS_HEADER_INCLUDED
#define OPM_NODECELL_NORNEMETIS_HEADER_INCLUDED

#endif //OPM_NODECELL_NORNEMETIS_HEADER_INCLUDED

template<class Grid, class W, class T>
idx_t numMETISGraphEdgesNodesvN(const Grid& grid, std::vector<int> &node_part, int node, const W &wells, const T &trans)
{
    auto gv = grid.leafGridView();
    auto lid = grid.localIdSet();
    idx_t numE = 0;

    // Add code for counting edges
    //iterative
    for (auto && e: elements(gv)){
      int id_e = lid.id(e);
      if (node_part[id_e] == node){
	auto nabSet = wells[id_e];
	numE +=nabSet.size();
	for (auto && face: intersections(gv,e)){
	  if (face.neighbor()){
	    if (node_part[lid.id(face.outside())] == node){ 
	      numE++;
	    }
	  }
	}
      }
    }
    return numE;
}

template<class Grid, class W, class T>
void Build_graphvN(const Grid &grid, std::vector<int> &node_part, idx_t * xadj, idx_t* yadj, idx_t* wadj,int node, std::vector<int> &g2l, std::vector<int> &l2g, const W &wells, const T &trans){

  auto lid = grid.localIdSet();
  auto gv = grid.leafGridView();
  
  // initializing g2l and l2g
  int i = 0;
  for (auto &&e : elements(gv)){

    if (node_part[lid.id(e)] == node){
      g2l[lid.id(e)] = i;
      l2g[i] = lid.id(e);
      i++;
    }
    if (i > l2g.size()) break;
  }



  int auxy = 0;
  int auxx = 0;
  for (auto &&e: elements(gv)){
    int id_e = lid.id(e);
    if (node_part[id_e] == node){
      
      xadj[auxx] = auxy;
      auxx++;
      
      for (auto &&face : intersections(gv,e)){
	if (face.neighbor()){
	  if (node_part[lid.id(face.outside())] == node){
	    int id_Nab = lid.id(face.outside());
	    yadj[auxy] = g2l[id_Nab];
	    long int normalizer = std::pow(10,20);
	    if ( std::log2(normalizer*trans[id_Nab]) < 1){
	      wadj[auxy] = 1;
	    }
	    else if(trans[id_Nab] == 0){
	      wadj[auxy] = 0;
	    }
	    else {
	      wadj[auxy] = int(std::log2(normalizer*trans[id_Nab]));
	    }
	    auxy++;

	  }
	}
      }

      
      auto nabSet = wells[id_e];
      // could check if one cells well neighbors are in the same node, but we assume they already are in the same node
      if (nabSet.size() != 0){
	for (auto &&nw: nabSet){

	  yadj[auxy] = g2l[nw];
	  wadj[auxy] = int(std::pow(2,22));
	  auxy++;
	}
	
      }
    }
  }
  xadj[auxx]=auxy;
}



template<class Grid, class W, class T>
void NodeCell_NorneMetis(const Grid &grid, std::vector<int> &node_part, std::vector<int> &cell_part, const W &wells, const T &trans, int nodes, int mpi_size){
  
  CpGrid_METIS_partitionvN(grid, nodes, node_part, wells, trans);


  std::vector<int> numCellsPerPart(grid.numCells(), 0);
  for (idx_t i = 0; i <grid.numCells(); ++i)
    {
      //std::cout<<part[i] << std::endl;
	numCellsPerPart[node_part[i]]++;
    }

  idx_t temp = mpi_size;
  idx_t npartsvec[nodes];
  for (idx_t i = 0; i < nodes ; i++){
    if (i == nodes - 1){
      npartsvec[i] = temp;
    }
    else{
	 npartsvec[i] = std::ceil((float)mpi_size/(float)nodes);
	 temp = temp - npartsvec[i];
    }
  }
  
  // make graph for each node
  for (int no = 0; no < nodes; no++){

    // Set number of vertices and edges in graph
    idx_t numVertex = numCellsPerPart[no];
    idx_t numEdges = numMETISGraphEdgesNodesvN(grid,node_part,no, wells, trans);
 
    std::vector<int> g2l(grid.numCells());
    std::vector<int> l2g(numVertex);
    // Define vectors used to represent graph in CSR format
    idx_t xadj[numVertex + 1];
    idx_t yadj[numEdges];
    idx_t wadj[numEdges];
    // Build graph
    Build_graphvN(grid, node_part, xadj, yadj,wadj, no, g2l,l2g,wells,trans);
    idx_t balCon = 1;
    idx_t nparts = npartsvec[no];
    idx_t options[METIS_NOPTIONS];
    set_options(options, 300);
    idx_t objval;
    idx_t part[numVertex];

    if (mpi_size > 0)
      METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, NULL, &nparts,
			  NULL, NULL, options, &objval, part);

    // merging Cp i and Cp j into Cp global cell  part

    for (int i = 0; i < numVertex; i ++){

      cell_part[l2g[i]] =  no*npartsvec[0] + part[i];
      
    }

    
   
  }
}
