/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_SCOTCHNORNEPARTITION_HEADER_INCLUDED
#define OPM_SCOTCHNORNEPARTITION_HEADER_INCLUDED

#endif // OPM_SCOTCHNORNEPARTITION_HEADER_INCLUDED

template<class Grid, class W, class T>
void buildScotchGraphvN(const Grid& grid, SCOTCH_Num* verttab, SCOTCH_Num* edgetab, SCOTCH_Num* vendtab, SCOTCH_Num *edlotab, const W &wells, const T &trans)
{
  auto gv=grid.leafGridView();
  SCOTCH_Num auxedge,auxvert,id_nab, auxw, id_e;
  auxedge = 0;
  auxvert = 0;
  auxw = 0;
  auto lid=grid.localIdSet();
  for (auto &&e: elements(gv)){
    verttab[auxvert]=auxedge;
    id_e=lid.id(e);
    for (auto &&face: intersections(gv,e)){
      if (face.neighbor()){
	id_nab=lid.id(face.outside());
	edgetab[auxedge]=id_nab;
	auxedge++;

	long int normalizer = std::pow(10,20);

	if (std::log2(normalizer*trans[id_nab]) < 1){
	  edlotab[auxw] = 1;
	}
	else{
	  edlotab[auxw] = int(std::log2(normalizer*trans[id_nab]));
	}
	/*if (wells[id_e].find(id_nab)!=wells[id_e].end()){
	  edlotab[auxw] = std::pow(2,28);
	  }*/
	auxw++;
      }

      
    }

    vendtab[auxvert]=auxedge;
    auxvert++;
  }
  verttab[auxvert]=id_nab;
}


template<class Grid, class W, class T>
void CpGrid_Scotch_partitionvN(const Grid& grid, int mpi_size, std::vector<int>& cell_part, const W &wells, const T &trans, std::string tol)
{
    const SCOTCH_Num baseval = 0;
    SCOTCH_Num vertnbr = grid.numCells();
    SCOTCH_Num edgenbr = Scotch_numEdges(grid);

    SCOTCH_Num verttab[vertnbr+1];
    SCOTCH_Num vendtab[vertnbr];
    SCOTCH_Num edgetab[edgenbr];

    
    SCOTCH_Num* velotab = NULL;
    SCOTCH_Num* vlbltab = NULL;
    SCOTCH_Num edlotab[edgenbr];

    buildScotchGraphvN(grid, verttab, edgetab, vendtab, edlotab, wells, trans);
    for (int i = 0; i < edgenbr; i++){
      std::cout << "id: " << i << "vekt: " << edlotab[i] << "\n";
    }

    std::cout << "hei\n";
    SCOTCH_Graph graph;

    //Build Scotch graph structure.
    SCOTCH_graphInit(&graph);
    SCOTCH_graphBuild(&graph, baseval, vertnbr, verttab, vendtab, velotab, vlbltab, edgenbr, edgetab, edlotab);
    
    SCOTCH_Num npart = mpi_size;
    SCOTCH_Num part[vertnbr];

    SCOTCH_Strat opt;
    setScotchOptions(opt,tol);
    std::cout << "hei\n"; 
    //Partition graph.
    SCOTCH_graphPart(&graph, npart, &opt, part);
    std::cout << "hei\n";
    SCOTCH_graphExit(&graph);
    SCOTCH_stratExit(&opt);

    std::vector<int> numCellsPerPart(npart, 0);
    for (int i =0; i < grid.numCells(); ++i) {
	cell_part[i] = part[i];
	numCellsPerPart[part[i]]++;
    }

    for (int k = 0; k < npart; ++k)
	std::cout << "Cell on rank " << k << " before loadbalancing: " << numCellsPerPart[k] << std::endl;
}
