#ifndef OPM_NODE_PARTITION_HEADER_INCLUDED
#define OPM_NODE_PARTITION_HEADER_INCLUDED

#endif //OPM_NODE_PARTITION_HEADER_INCLUDED

void set_options(idx_t* options)
{

    METIS_SetDefaultOptions(options);
    
    // *** Imbalance tolerance in %%. Default is 30%%. Set to 300%%
    options[METIS_OPTION_UFACTOR] = 1;

    // *** Coarsening method
    options[METIS_OPTION_CTYPE] = METIS_CTYPE_RM;
    //options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM;
    
    
    // *** Partitioning method for coarsest graph
    options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_RANDOM;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_EDGE;
    //options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_NODE;
    
    
    // *** Refinement method
    options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_GREEDY;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_SEP2SIDED;
    //options[METIS_OPTION_RTYPE] = METIS_RTYPE_SEP1SIDED;
    
}



idx_t numMETISNodeGraphEdges(const std::vector<int> &CommTab){

  idx_t numE = 0;
  
  for (int i = 0; i < CommTab.size(); i++){

    if (CommTab[i] > 0){

      numE++;
    }
  }
  return numE;
}


void buildMETISGraph(const std::vector<int> &CommTab, idx_t *xadj, idx_t *yadj, idx_t *wadj, int &nRanks){

  int auxx = 0;
  int auxy = 0;
  int auxw = 0;

  for (int rank = 0; rank < nRanks; rank++){
    
    xadj[auxx] = auxy;
    auxx++;
    for (int j = nRanks*rank; j<nRanks*rank+nRanks; j++){

      
      if (CommTab[j] > 0) {

	wadj[auxw] = CommTab[j];

	yadj[auxy] = j%nRanks;
	auxw++;
	auxy++;
      }
    }
  }
  xadj[auxx]=auxy;
}




void repartition(std::vector<int> &node_part, std::vector<int> &cell_part,  const idx_t &nparts){ 

  std::vector<int> new_ranks(node_part.size());
  int aux = 0;
  for (int j = 0; j < nparts; j++){
    for (int i = 0; i < node_part.size(); i++){

      if (node_part[i] == j) {

	new_ranks[i] = aux;
	aux++;
      }
      
    }
  }

   for (int kk = 0; kk < cell_part.size(); kk++){
    int old_rank  = cell_part[kk];
    int new_rank  = new_ranks[old_rank];
    cell_part[kk] = new_rank;
    //    std::cout << "old: " << old_rank << " new: " << new_rank <<"\t";
  }

  //std::cout << "new ranks: \t";
  //for (auto &r: new_ranks){
  // std::cout << r << "\t ";
  // }
  //std::cout << "\n\n";
}

void CellNode_Partition(const std::vector<int> &CommTab, int mpi_size, std::vector<int> &node_part, std::vector<int> &cell_part, int nodes){

  //set number of vertices and edges in graph
  int nRanks = mpi_size; //or mpi_size
  idx_t numVertex = nRanks;
  idx_t numEdges = numMETISNodeGraphEdges(CommTab);

  // Define vectors used to represent graph in CSR format
  idx_t xadj[numVertex +1];
  idx_t yadj[numEdges];
  idx_t wadj[numEdges];

  // Build graph
  buildMETISGraph(CommTab, xadj, yadj, wadj, nRanks);

  // set options for partitioning function
  idx_t balCon = 1;
  idx_t nparts = nodes;
  idx_t options[METIS_NOPTIONS];
  set_options(options);


  // Define output arguments for partitioning
  idx_t objval;
  idx_t part[numVertex];

  
  // Partition graph
  if (nparts > 0){
    METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, wadj, &nparts, NULL,
			NULL, options, &objval, part);
  }

  // Count and print number of cells per subgraph after partitioning
  std::vector<int> numCellsPerPart(nparts,0);
  for (idx_t i = 0; i < numVertex; i++){
    //std::cout << "Vertex "<< i << " to: " << part[i] << "\n";
    numCellsPerPart[part[i]]++;
    node_part[i] = part[i];
  }
  for (int k = 0; k < nparts; k++){
    //std::cout << "Cell on rank " << k << " :" << numCellsPerPart[k] << "\n";
  }

  std::cout << "\nEdge-cut for node partition: " << objval << "\n\n";


  //  std::cout << "\n Repartitioning: \n";
  repartition(node_part, cell_part, nparts);

} 
