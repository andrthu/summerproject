/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_ZOLTANGRAPHPARTITION_HEADER_INCLUDED
#define OPM_ZOLTANGRAPHPARTITION_HEADER_INCLUDED

#endif // OPM_ZOLTANGRAPHPARTITION_HEADER_INCLUDED

#undef HAVE_MPI
#include <zoltan.h>
#undef HAVE_MPI
#define HAVE_MPI 1

int getNullNumCells(void* cpGridPointer, int* err)
{
    (void) cpGridPointer;
    *err = ZOLTAN_OK;
    return 0;
}

void getNullVertexList(void* cpGridPointer, int numGlobalIdEntries,
                       int numLocalIdEntries, ZOLTAN_ID_PTR gids,
                       ZOLTAN_ID_PTR lids, int wgtDim,
                       float *objWgts, int *err)
{
    (void) cpGridPointer; (void) numGlobalIdEntries;
    (void) numLocalIdEntries; (void) gids; (void) lids; (void) objWgts;
    (void) wgtDim;
    // We do nothing as we pretend to not have any grid cells.
    *err = ZOLTAN_OK;
}

void getNullNumEdgesList(void *cpGridPointer, int sizeGID, int sizeLID,
                           int numCells,
                           ZOLTAN_ID_PTR globalID, ZOLTAN_ID_PTR localID,
                           int *numEdges, int *err)
{
    (void) sizeGID; (void) sizeLID; (void) numCells; (void) globalID;
    (void) localID; (void) numEdges; (void) cpGridPointer;
    // Pretend that there are no edges
    numEdges = 0;
    *err = ZOLTAN_OK;
}

void getNullEdgeList(void *cpGridPointer, int sizeGID, int sizeLID,
		     int numCells, ZOLTAN_ID_PTR globalID, 
		     ZOLTAN_ID_PTR localID,
		     int *numEdges,
		     ZOLTAN_ID_PTR nborGID, int *nborProc,
		     int wgtDim, float *ewgts, int *err)
{
    (void) cpGridPointer; (void) sizeGID; (void) sizeLID; (void) numCells;
    (void) globalID; (void) localID; (void) numEdges; (void) nborGID;
    (void) nborProc; (void) wgtDim; (void) ewgts;
    *err = ZOLTAN_OK;
}

/// Function setting number of vertices in graph
int getCpGridNumCells(void* cpGridPointer, int* err)
{
    const Dune::CpGrid&  grid = *static_cast<const Dune::CpGrid*>(cpGridPointer);
    
    *err = ZOLTAN_OK;
    return grid.numCells();
}

/// Function setting up a list of vertices in graph
void getCpGridVertexList(void* cpGridPointer, int numGlobalIdEntries,
                       int numLocalIdEntries, ZOLTAN_ID_PTR gids,
                       ZOLTAN_ID_PTR lids, int wgtDim,
                       float *objWgts, int *err)
{
    (void) objWgts; (void) wgtDim;
    
    const Dune::CpGrid&  grid = *static_cast<const Dune::CpGrid*>(cpGridPointer);
    /// Insert code here
    auto lid=grid.localIdSet();
    auto gid=grid.globalIdSet();
    auto gv= grid.leafGridView();
    int aux=0;
    for (auto &&e: elements(gv)){
      lids[aux]=lid.id(e);//=e
      gids[aux]=gid.id(e);//=e
      aux++;
      
    }
    *err = ZOLTAN_OK;
}

/// Function for setting number of edges in graph
void getCpGridNumEdgesList(void *cpGridPointer, int sizeGID, int sizeLID,
                           int numCells,
                           ZOLTAN_ID_PTR globalID, ZOLTAN_ID_PTR localID,
                           int *numEdges, int *err)
{
    (void) globalID;
    
    const Dune::CpGrid&  grid = *static_cast<const Dune::CpGrid*>(cpGridPointer);
    auto gv=grid.leafGridView();
    int aux=0;
    for (auto &&e: elements(gv)){
      int count_nab=0;
      for (auto &&face: intersections(gv,e)){
	if (face.neighbor()){
	  count_nab++;
	}
      }
      numEdges[aux]=count_nab;
      aux++;
    }
    
    *err = ZOLTAN_OK;
}

// Function for setting up lists edges
template<class W, class T>
void getCpGridEdgeList(void *cpGridPointer, int sizeGID, int sizeLID,
		       int numCells, ZOLTAN_ID_PTR globalID, 
		       ZOLTAN_ID_PTR localID,
		       int *numEdges,
		       ZOLTAN_ID_PTR nborGID, int *nborProc,
		       int wgtDim, float *ewgts, int *err,
		       const W & wells, const T &trans)
{
    (void) globalID;
    
    const Dune::CpGrid&  grid = *static_cast<const Dune::CpGrid*>(cpGridPointer);
    auto gv = grid.leafGridView();
    int aux = 0;
    int auxw = 0;
    auto lid = grid.localIdSet();
    for (auto &&e: elements(gv)){

      int id_e = lid.id(e);
      
      for (auto && face:intersections(gv,e)){
	if (face.neighbor()){
	int id_nab = lid.id(face.outside());
	nborGID[aux] = id_nab;
	nborProc[aux] = 0;
	aux++;
	}
      }

    }
    
    *err = ZOLTAN_OK;
}



template<class Grid>
void setCpGridZoltanGraphFunctions(Zoltan_Struct *zz, const Grid& grid, bool pretendNull)
{
    Grid *gridPointer = const_cast<Grid*>(&grid);
    
    if (pretendNull) {
	Zoltan_Set_Num_Obj_Fn(zz, getNullNumCells, gridPointer);
        Zoltan_Set_Obj_List_Fn(zz, getNullVertexList, gridPointer);
        Zoltan_Set_Num_Edges_Multi_Fn(zz, getNullNumEdgesList, gridPointer);
        Zoltan_Set_Edge_List_Multi_Fn(zz, getNullEdgeList, gridPointer);
    }
    
    else {
	Zoltan_Set_Num_Obj_Fn(zz, getCpGridNumCells, gridPointer);
        Zoltan_Set_Obj_List_Fn(zz, getCpGridVertexList, gridPointer);
        Zoltan_Set_Num_Edges_Multi_Fn(zz, getCpGridNumEdgesList, gridPointer);
        Zoltan_Set_Edge_List_Multi_Fn(zz, getCpGridEdgeList, gridPointer);
    }
    
}

template<class Grid, class C, class W, class T>
void CpGrid_Zoltan_partition(const Grid& grid, const C& cc, int mpi_size, int rank, std::vector<int>& cell_part, const W &wells, const T &trans, std::string tol)
{
    int rc = ZOLTAN_OK - 1;
    float ver = 0;
    struct Zoltan_Struct *zz;
    int changes, numGidEntries, numLidEntries, numImport, numExport;
    ZOLTAN_ID_PTR importGlobalGids, importLocalGids, exportGlobalGids, exportLocalGids;
    int *importProcs, *importToPart, *exportProcs, *exportToPart;
    int argc = 0;
    char** argv = 0 ;

    rc = Zoltan_Initialize(argc, argv, &ver);
    zz = Zoltan_Create(cc);

    Zoltan_Set_Param(zz, "IMBALANCE_TOL", "1.01");
    Zoltan_Set_Param(zz, "SEED", "123456789");
    Zoltan_Set_Param(zz, "DEBUG_LEVEL", "0");
    Zoltan_Set_Param(zz, "LB_METHOD", "GRAPH");
    Zoltan_Set_Param(zz, "LB_APPROACH", "PARTITION");
    Zoltan_Set_Param(zz, "NUM_GID_ENTRIES", "1");
    Zoltan_Set_Param(zz, "NUM_LID_ENTRIES", "1");
    Zoltan_Set_Param(zz, "RETURN_LISTS", "ALL");
    Zoltan_Set_Param(zz, "CHECK_GRAPH", "2");
    Zoltan_Set_Param(zz, "EDGE_WEIGHT_DIM","1");
    Zoltan_Set_Param(zz, "OBJ_WEIGHT_DIM", "0");
    Zoltan_Set_Param(zz, "PHG_EDGE_SIZE_THRESHOLD", ".35");

    setCpGridZoltanGraphFunctions(zz, grid, rank!=0);

    rc = Zoltan_LB_Partition(zz, /* input (all remaining fields are output) */
                             &changes,        
                             &numGidEntries,  
                             &numLidEntries,  
                             &numImport,      
                             &importGlobalGids,  
                             &importLocalGids,   
                             &importProcs,    
                             &importToPart,   
                             &numExport,      /* Number of vertices I must send to other processes*/
                             &exportGlobalGids,  
                             &exportLocalGids,   /* Local IDs of the vertices I must send */
                             &exportProcs,
                             &exportToPart);  /* Partition to which each vertex will belong */

    std::vector<int> numCellsPerPart(mpi_size,0);
    numCellsPerPart[0]=grid.numCells();
    for ( int i=0; i < numExport; ++i )
    {
        cell_part[exportLocalGids[i]] = exportProcs[i];
	//std::cout<<cell_part[exportLocalGids[i]] << " " << exportLocalGids[i]<< " \n";
	numCellsPerPart[cell_part[exportLocalGids[i]]]++;
	if (cell_part[exportLocalGids[i]] != 0)
	  numCellsPerPart[0]--;
    }

    
    
    //std::cout<<mpi_size<<std::endl;

    if (rank == 0) {
      
	for (int k=0;k<mpi_size;k++){
	  std::cout<<"Cell on rank " << k << " before loadbalancing: " << numCellsPerPart[k] << std::endl;
	}
    }

    //cc.broadcast(&cell_part[0], cell_part.size(), 0);
    
    Zoltan_LB_Free_Part(&exportGlobalGids, &exportLocalGids, &exportProcs, &exportToPart);
    Zoltan_LB_Free_Part(&importGlobalGids, &importLocalGids, &importProcs, &importToPart);
    Zoltan_Destroy(&zz);
}
