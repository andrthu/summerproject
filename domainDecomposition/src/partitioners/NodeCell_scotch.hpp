#ifndef OPM_NODECELL_SCOTCH_HEADER_INCLUDED
#define OPM_NODECELL_SCOTCH_HEADER_INCLUDED

#endif //OPM_NODECELL_SCOTCH_HEADER_INCLUDED


template<class Grid>
SCOTCH_Num numSCOTCHGraphEdgesNodes(const Grid &grid, std::vector<int> &node_part, int node){

  auto gv = grid.leafGridView();
  auto lid = grid.localIdSet();
  SCOTCH_Num numE = 0;
  
  // Add code for counting edges
  //iterative
  for (auto && e: elements(gv)){
    if (node_part[lid.id(e)] == node){
      for (auto && face: intersections(gv,e)){
	if (face.neighbor()){
	  if (node_part[lid.id(face.outside())] == node){ 
	    numE++;
	  }
	}
      }
    }
  }
  return numE;
    
}


template<class Grid>
void BuildScotch_graph(const Grid &grid, std::vector<int> &node_part, SCOTCH_Num *vertTab, SCOTCH_Num *vendTab, SCOTCH_Num* edgeTab, int node, std::vector<int> &g2l, std::vector<int> &l2g){
  

  
  auto lid = grid.localIdSet();
  auto gv = grid.leafGridView();
  
  // initializing g2l and l2g
  int i = 0;
  for (auto &&e : elements(gv)){

    if (node_part[lid.id(e)] == node){
      g2l[lid.id(e)] = i;
      l2g[i] = lid.id(e);
      i++;
    }
    if (i > l2g.size()) break;
  }

  SCOTCH_Num auxedge, auxvert;
  auxedge = 0;
  auxvert = 0;
  for (auto &&e: elements(gv)){
    
    if (node_part[lid.id(e)] == node){
      
      vertTab[auxvert] = auxedge;
      
      for (auto &&face : intersections(gv,e)){
	if (face.neighbor()){
	  if (node_part[lid.id(face.outside())] == node){
	    int id_nab = lid.id(face.outside());
	    edgeTab[auxedge] = g2l[id_nab];
	    auxedge++;
	  }
	}
      }
      vendTab[auxvert]=auxedge;
      auxvert++;
    }
  }
  vertTab[auxvert]=auxedge;
}




template<class Grid>
void NodeCell_Scotch(const Grid &grid, std::vector<int> &node_part, std::vector<int> &cell_part, int nodes, int mpi_size){

  CpGrid_Scotch_partition(grid, nodes , node_part, "1.01");

  std::vector<int> numCellsPerPart(grid.numCells(), 0);
  for (SCOTCH_Num i = 0; i <grid.numCells(); ++i)
    {
      //std::cout<<part[i] << std::endl;
      numCellsPerPart[node_part[i]]++;
    }
  
  
  SCOTCH_Num temp = mpi_size;
  SCOTCH_Num npartsvec[nodes];
  for (SCOTCH_Num i = 0; i < nodes ; i++){
    if (i == nodes - 1){
      npartsvec[i] = temp;
    }
    else{
      npartsvec[i] = std::ceil((float)mpi_size/(float)nodes);
      temp = temp - npartsvec[i];
    }
  }
  // make graph for each node
  for (int no = 0; no < nodes; no++){
    const SCOTCH_Num baseval = 0;
    SCOTCH_Num numVertex = numCellsPerPart[no];
    SCOTCH_Num numEdges = numSCOTCHGraphEdgesNodes(grid, node_part, no);

    SCOTCH_Num vertTab[numVertex + 1];
    SCOTCH_Num vendTab[numVertex];
    SCOTCH_Num edgeTab[numEdges];


    std::vector<int> g2l(grid.numCells());
    std::vector<int> l2g(numVertex);

    
    BuildScotch_graph(grid, node_part, vertTab, vendTab, edgeTab, no, g2l, l2g);
    
    SCOTCH_Num* velotab = NULL;
    SCOTCH_Num* vlbltab = NULL;
    SCOTCH_Num* edlotab = NULL;
    
    SCOTCH_Graph graph;
    
    //Build Scotch graph structure.
    SCOTCH_graphInit(&graph);
    SCOTCH_graphBuild(&graph, baseval, numVertex, vertTab, vendTab, velotab, vlbltab, numEdges, edgeTab, edlotab);

    SCOTCH_Num npart = npartsvec[no];
    SCOTCH_Num part[numVertex];

    SCOTCH_Strat opt;
    setScotchOptions(opt,"0.01");

    
    //Partition graph.
    SCOTCH_graphPart(&graph, npart, &opt, part);
    
    SCOTCH_graphExit(&graph);
    SCOTCH_stratExit(&opt);

    for (int i = 0; i < numVertex; i ++){
      
      cell_part[l2g[i]] =  no*npartsvec[0] + part[i];

    }

  }


  //end
}
