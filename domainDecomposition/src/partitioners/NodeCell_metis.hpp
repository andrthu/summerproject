#ifndef OPM_NODECELL_METIS_HEADER_INCLUDED
#define OPM_NODECELL_METIS_HEADER_INCLUDED

#endif //OPM_NODECELL_METIS_HEADER_INCLUDED

template<class Grid>
idx_t numMETISGraphEdgesNodes(const Grid& grid, std::vector<int> &node_part, int node)
{
    auto gv = grid.leafGridView();
    auto lid = grid.localIdSet();
    idx_t numE = 0;

    // Add code for counting edges
    //iterative
    for (auto && e: elements(gv)){
      if (node_part[lid.id(e)] == node){
	for (auto && face: intersections(gv,e)){
	  if (face.neighbor()){
	    if (node_part[lid.id(face.outside())] == node){ 
	      numE++;
	    }
	  }
	}
      }
    }
    return numE;
}

template<class Grid>
void Build_graph(const Grid &grid, std::vector<int> &node_part, idx_t * xadj, idx_t* yadj, int node, std::vector<int> &g2l, std::vector<int> &l2g){

  auto lid = grid.localIdSet();
  auto gv = grid.leafGridView();
  
  // initializing g2l and l2g
  int i = 0;
  for (auto &&e : elements(gv)){

    if (node_part[lid.id(e)] == node){
      g2l[lid.id(e)] = i;
      l2g[i] = lid.id(e);
      i++;
    }
    if (i > l2g.size()) break;
  }



  int auxy = 0;
  int auxx = 0;
  for (auto &&e: elements(gv)){
    
    if (node_part[lid.id(e)] == node){
      
      xadj[auxx] = auxy;
      auxx++;
      
      for (auto &&face : intersections(gv,e)){
	if (face.neighbor()){
	  if (node_part[lid.id(face.outside())] == node){
	    int id_nab = lid.id(face.outside());
	    yadj[auxy] = g2l[id_nab];
	    auxy++;
	  }
	}
      }
    }
  }
  xadj[auxx]=auxy;
}



template<class Grid>
void NodeCell_metis(const Grid &grid, std::vector<int> &node_part, std::vector<int> &cell_part, int nodes, int mpi_size){
  
  CpGrid_METIS_partition(grid, nodes, node_part, 300);


  std::vector<int> numCellsPerPart(grid.numCells(), 0);
  for (idx_t i = 0; i <grid.numCells(); ++i)
    {
      //std::cout<<part[i] << std::endl;
	numCellsPerPart[node_part[i]]++;
    }

  idx_t temp = mpi_size;
  idx_t npartsvec[nodes];
  for (idx_t i = 0; i < nodes ; i++){
    if (i == nodes - 1){
      npartsvec[i] = temp;
    }
    else{
	 npartsvec[i] = std::ceil((float)mpi_size/(float)nodes);
	 temp = temp - npartsvec[i];
    }
  }
  
  // make graph for each node
  for (int no = 0; no < nodes; no++){

    // Set number of vertices and edges in graph
    idx_t numVertex = numCellsPerPart[no];
    idx_t numEdges = numMETISGraphEdgesNodes(grid,node_part,no);

    std::vector<int> g2l(grid.numCells());
    std::vector<int> l2g(numVertex);
    // Define vectors used to represent graph in CSR format
    idx_t xadj[numVertex + 1];
    idx_t yadj[numEdges];

    // Build graph
    Build_graph(grid, node_part, xadj, yadj, no, g2l,l2g);
    idx_t balCon = 1;
    idx_t nparts = npartsvec[no];
    idx_t options[METIS_NOPTIONS];
    set_options(options, 300);
    idx_t objval;
    idx_t part[numVertex];

    if (mpi_size > 0)
      METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, NULL, &nparts,
			  NULL, NULL, options, &objval, part);

    // merging Cp i and Cp j into Cp global

    for (int i = 0; i < numVertex; i ++){

      cell_part[l2g[i]] =  no*npartsvec[0] + part[i];
      
    }

    
   
  }
}
