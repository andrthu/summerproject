#ifndef OPM_BAD_PARTITION_HEADER_INCLUDED
#define OPM_BAD_PARTITION_HEADER_INCLUDED

#endif //OPM_BAD_PARTITION_HEADER_INCLUDED





void bad_repartition(std::vector<int> &node_part, std::vector<int> &cell_part,  const idx_t &nparts, int mpi_size){ 

  std::vector<int> new_ranks(node_part.size());
  int aux = 0;

  for (int j = 0; j < nparts; j++){
    for (int i = 0; i < node_part.size(); i++){

      if (node_part[i] == j) {

	new_ranks[i] = aux;
	aux++;
      }
    }
  }
  for (int j = 0; j < node_part.size(); j++){

    if ((j+1)%4 == 0 and ((j+1) < node_part.size())){
      int temp = new_ranks[(j+1)];
      new_ranks[(j+1)] = new_ranks[(j+1)/2];
      new_ranks[(j+1)/2] = temp;
    }
  }
  for (int kk = 0; kk < cell_part.size(); kk++){
    int old_rank  = cell_part[kk];
    int new_rank  = new_ranks[old_rank];
    cell_part[kk] = new_rank;
    //std::cout << "old: " << old_rank << " new: " << new_rank <<"\t";
  }

  
}

void bad_partition(const std::vector<int> &CommTab, int mpi_size, std::vector<int> &node_part, std::vector<int> &cell_part, int nodes){

  //set number of vertices and edges in graph
  int nRanks = mpi_size; //or mpi_size
  idx_t numVertex = nRanks;
  idx_t numEdges = numMETISNodeGraphEdges(CommTab);

  // Define vectors used to represent graph in CSR format
  idx_t xadj[numVertex +1];
  idx_t yadj[numEdges];
  idx_t wadj[numEdges];

  // Build graph
  buildMETISGraph(CommTab, xadj, yadj, wadj, nRanks);

  // set options for partitioning function
  idx_t balCon = 1;
  idx_t nparts = nodes;
  idx_t options[METIS_NOPTIONS];
  set_options(options);


  // Define output arguments for partitioning
  idx_t objval;
  idx_t part[numVertex];

  
  // Partition graph
  if (nparts > 0){
    METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, wadj, &nparts, NULL,
			NULL, options, &objval, part);
  }

  // Count and print number of cells per subgraph after partitioning
  std::vector<int> numCellsPerPart(nparts,0);
  for (idx_t i = 0; i < numVertex; i++){
    //std::cout << "Vertex "<< i << " to: " << part[i] << "\n";
    numCellsPerPart[part[i]]++;
    node_part[i] = part[i];
  }
  for (int k = 0; k < nparts; k++){
    //std::cout << "Cell on rank " << k << " :" << numCellsPerPart[k] << "\n";
  }

  std::cout << "\nEdge-cut for node partition: " << objval << "\n\n";


  //  std::cout << "\n Repartitioning: \n";
  bad_repartition(node_part, cell_part, nparts, mpi_size);

} 
