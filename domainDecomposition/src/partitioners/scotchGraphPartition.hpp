/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_SCOTCHGRAPHPARTITION_HEADER_INCLUDED
#define OPM_SCOTCHGRAPHPARTITION_HEADER_INCLUDED

#endif // OPM_SCOTCHGRAPHPARTITION_HEADER_INCLUDED

template<class Grid>
SCOTCH_Num Scotch_numEdges(const Grid& grid)
{
  SCOTCH_Num numEdges = 0;
  auto gv = grid.leafGridView();
  auto lid = grid.localIdSet();
  // Add code for counting edges
  //iterative
  for (auto && e: elements(gv)){
    for (auto && face: intersections(gv,e)){
      if (face.neighbor()){
	numEdges++;
      }
    }
  }
  return numEdges;
}

template<class Grid>
void buildScotchGraph(const Grid& grid, SCOTCH_Num* verttab, SCOTCH_Num* edgetab, SCOTCH_Num* vendtab)
{
  auto gv=grid.leafGridView();
  SCOTCH_Num auxedge,auxvert,id_nab;
  auxedge=0;
  auxvert=0;
  auto lid=grid.localIdSet();
  for (auto &&e: elements(gv)){
    verttab[auxvert]=auxedge;

    for (auto &&face: intersections(gv,e)){
      if (face.neighbor()){
	id_nab=lid.id(face.outside());
	edgetab[auxedge]=id_nab;
	auxedge++;
	
      }
    }

    vendtab[auxvert]=auxedge;
    auxvert++;
  }
  verttab[auxvert]=id_nab;
}

void setScotchOptions(SCOTCH_Strat& opt , std::string tol)
{
    SCOTCH_stratInit(&opt); 
    SCOTCH_stratGraphMap(&opt,"m{asc=b{width=3,bnd=d{pass=40,dif=1,rem=0}f{move=80,pass=-1,bal=0.01},org=f{move=80,pass=-1,bal=0.01}},low=r{job=t,bal=0.1,map=t,poli=S,sep=(m{asc=b{bnd=f{move=120,pass=-1,bal=0.1,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8}|m{asc=b{bnd=f{move=120,pass=-1,bal=0.01,type=b},org=f{move=120,pass=-1,bal=0.01,type=b},width=3},low=h{pass=10}f{move=120,pass=-1,bal=0.01,type=b},vert=120,rat=0.8})},vert=10000,rat=0.8,type=h}" );
    //SCOTCH_stratGraphMap(&opt, "m{asc=f{bal=0.1,move=120,pass=-1},low=b{width=3,bnd=d{pass=40,dif=1,rem=0}},vert=120,rat=1,type=h}");

}

template<class Grid>
void CpGrid_Scotch_partition(const Grid& grid, int mpi_size, std::vector<int>& cell_part,std::string tol)
{
    const SCOTCH_Num baseval = 0;
    SCOTCH_Num vertnbr = grid.numCells();
    SCOTCH_Num edgenbr = Scotch_numEdges(grid);

    SCOTCH_Num verttab[vertnbr+1];
    SCOTCH_Num vendtab[vertnbr];
    SCOTCH_Num edgetab[edgenbr];
    buildScotchGraph(grid, verttab, edgetab, vendtab);
    
    SCOTCH_Num* velotab = NULL;
    SCOTCH_Num* vlbltab = NULL;
    SCOTCH_Num* edlotab = NULL;

    SCOTCH_Graph graph;

    //Build Scotch graph structure.
    SCOTCH_graphInit(&graph);
    SCOTCH_graphBuild(&graph, baseval, vertnbr, verttab, vendtab, velotab, vlbltab, edgenbr, edgetab, edlotab);

    SCOTCH_Num npart = mpi_size;
    SCOTCH_Num part[vertnbr];

    SCOTCH_Strat opt;
    setScotchOptions(opt,tol);

    //Partition graph.
    SCOTCH_graphPart(&graph, npart, &opt, part);
    
    SCOTCH_graphExit(&graph);
    SCOTCH_stratExit(&opt);

    std::vector<int> numCellsPerPart(npart, 0);
    for (int i =0; i < grid.numCells(); ++i) {
	cell_part[i] = part[i];
	numCellsPerPart[part[i]]++;
    }

    for (int k = 0; k < npart; ++k)
	std::cout << "Cell on rank " << k << " before loadbalancing: " << numCellsPerPart[k] << std::endl;
}
