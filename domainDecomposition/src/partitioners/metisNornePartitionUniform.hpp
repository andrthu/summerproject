/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_METISNORNEPARTITIONUNIFORM_HEADER_INCLUDED
#define OPM_METISNORNEPARTITIONUNIFORM_HEADER_INCLUDED

#endif // OPM_METISNORNEPARTITIONUNIFORM_HEADER_INCLUDED

template<class Grid, class W, class T>
void buildMETISGraphvUniform(const Grid& grid, idx_t* xadj, idx_t* yadj, idx_t *wadj, const W &wells, const T &trans)
{
  int auxy = 0; 
  int auxx = 0;
  auto lid = grid.localIdSet();
  auto gv = grid.leafGridView();
  for (auto &&e: elements(gv)){

    xadj[auxx]=auxy;
    auxx++;
    int id_e = lid.id(e);
    for (auto &&face: intersections(gv,e)){
      if( face.neighbor()){
	
	int id_Nab=lid.id(face.outside());
	yadj[auxy]=id_Nab;
	long int normalizer = std::pow(10,20);
	//	std::cout<<"\t test";
	wadj[auxy] = 1;
	auxy++;
      }
    }
    auto nabSet = wells[id_e];
    if (nabSet.size() != 0){
      for (auto && nW: nabSet){
	
	yadj[auxy] = nW;
	wadj[auxy] = int(std::pow(2,22));
	auxy++;
      }
    }
  }
  xadj[auxx]=auxy;
}
template<class Grid, class W, class T>
void CpGrid_METIS_partitionvUniform(const Grid& grid, int mpi_size, std::vector<int>& cell_part, const W &wells, const T &trans, int tol = 300)
{
    // Set number of vertices and edges in graph
  idx_t numVertex = grid.numCells();
  idx_t numEdges =  numEdgeMETISGraphvN(grid, wells, trans);
  // Define vectors used to represent graph in CSR format
  idx_t xadj[numVertex + 1];
  idx_t yadj[numEdges];
  idx_t wadj[numEdges];
  // Build graph
  buildMETISGraphvUniform(grid, xadj, yadj, wadj, wells, trans);
  // Set options for partitioning function
  idx_t balCon = 1;
  idx_t nparts = mpi_size;
  idx_t options[METIS_NOPTIONS];
  set_options(options,tol);
  
  // Define output arguments for partitioning
  idx_t objval;
  idx_t part[numVertex];
  // Partitin graph
  
  if (mpi_size>0)
    METIS_PartGraphKway(&numVertex, &balCon, xadj, yadj, NULL, NULL, wadj, &nparts,
			NULL, NULL, options, &objval, part);

  // Count and print number of cells per subgraph after partitioning
  std::vector<int> numCellsPerPart(nparts, 0);
  for (idx_t i = 0; i < numVertex; ++i)
    {
      //std::cout<<part[i] << std::endl;
	numCellsPerPart[part[i]]++;
	cell_part[i] = part[i];
    }
  for (int k = 0; k < nparts; ++k)
    std::cout << "Cell on rank " << k << " before loadbalancing: " << numCellsPerPart[k] << std::endl;
  
  // Print edge-cut of partitioning.
  std::cout << "Edge-cut: " << objval << std::endl;
}
