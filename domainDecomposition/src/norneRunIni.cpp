#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <string>
#include <set>
#include <chrono>



#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <dune/geometry/referenceelements.hh>

#include <mpi.h>
#include <scotch.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include "io/readIni.hpp"
#include "io/readEclipseCase.hpp"
#include "io/displayPartition.hpp"
#include "partitioners/scotchGraphPartition.hpp"
#include "partitioners/scotchNornePartition.hpp"
#include "partitioners/zoltanGraphPartition.hpp"
#include <metis.h>
#include "partitioners/metisGraphPartition.hpp" 
#include "partitioners/metisNornePartition.hpp"
#include "partitioners/metisNornePartitionUniform.hpp"
#include "partitioners/CellNode_Partition.hpp"
#include "partitioners/bad_partition.hpp"
#include "partitioners/NodeCell_metis.hpp"
#include "partitioners/NodeCell_NorneMetis.hpp"
#include "partitioners/NodeCell_NorneMetisUniform.hpp"
#include "partitioners/NodeCell_scotch.hpp"
using namespace Dune;
#include "overlappingOperations/parallelInfo.hpp"
#include "overlappingOperations/parallelSolver.hpp"
#include "assemble/buildPoisson.hpp"
#include "assemble/initSparseMatrix.hpp" 
#include "assemble/norneSolve.hpp"
#include "communication/ghost_cells_after.hpp"
#include "communication/ghost_cells_before.hpp"



template<class G, class WC>
void init_connections(const G& grid, WC& wc, std::vector<const Opm::Well*> wells)
{
    const auto& cpgdim = grid.logicalCartesianSize();
    
    std::vector<int> cartesian_to_compressed(cpgdim[0]*cpgdim[1]*cpgdim[2], -1);
    for( int i=0; i < grid.numCells(); ++i )
    {
        cartesian_to_compressed[grid.globalCell()[i]] = i;
    }
    
    wc.init(wells, cpgdim, cartesian_to_compressed);
}


int main(int argc, char ** argv) {

  // Define dictionary for reading from ini
  DictRead    dr;
 
  // Reading from ini file
  if (argc > 1)
    dr.read_file_and_update(argv[1]);
  

  // Store data from dictionary
  int partition_lib     = std::stoi(dr.dict[0]); // 0 metis, 1 metisCN, 2 metisNC, 3-5 metisnorne, 6-7 cp GRid
  std::string tol       = dr.dict[1]; // imbalance tol 
  int num_nodes         = std::stoi(dr.dict[2]); 


  bool cont = false;

  
  // Set MPI commuicator types
  Dune::MPIHelper::instance(argc, argv);
  typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
  typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;

  // Dune Block vector format type
  typedef Dune::FieldVector<double,1> BlockVec;
  typedef Dune::BlockVector<BlockVec> Vec;

  // Dune Block Matrix and CSR sparse Matrix format types
  typedef Dune::FieldMatrix<double,1,1> BlockMat;
  typedef Dune::BCRSMatrix<BlockMat> Mat;

  Dune::CpGrid grid;
  ReadProperties readProperties;
  if (argc > 2) readProperties.init_grid(grid, argv[2], true);

  const auto wells = readProperties.getWells();
  auto trans = readProperties.getTransVec();
  std::vector<double> dummy = {0,0,0};
  int edgWgtMet = 1; //0=uniform, 1=trans, 2=log(trans)
  


  // Define MPI comunicator
  CollectiveCommunication cc(MPI_COMM_WORLD);
  
  // Get rank of processor and size of communicator (number of ranks).
  int rank = cc.rank();
  int size = cc.size();

  // Matrix and vectors for mvp 
  Mat         A;
  Vec         rhs,x;
 
  // Initialize vector for cells rank and for node part
  std::vector<int> cell_part(grid.numCells(), rank);
  std::vector<int> cell_rank(cc.size(), 0);
  std::vector<int> node_part(grid.numCells(), rank);



  // Well connections
  Dune::cpgrid::WellConnections wc;
  init_connections(grid, wc, wells);
  int d;
  std::vector<std::set<int>> comm_Well(grid.numCells());
  for (const auto & well : wc){
    for (auto perf = well.begin(); perf!=well.end(); ++perf){
      d=*perf;
      for (auto perfy= well.begin(); perfy!=well.end(); ++perfy){

	comm_Well[*perf].insert(*perfy);
      }
    }

  }

  

    
  if (rank == 0){
    std::cout << "Number of cores/ranks per node is: " << size/num_nodes << "\n";
    }
  std::vector<int> cell_partN;
  // deciding partitioner
  switch (partition_lib){
  case 0: //METIS
    if (rank==0){
      std::cout << "METIS partitioner" << std::endl;
      CpGrid_METIS_partition(grid, size, cell_part);
    }
    break;
  case 1: // Cell Node partition with metis cell partition and then metis node partition
    if (rank==0){
      std::cout << "METIS norne  partitioner with metis node partitioner" << std::endl;
      CpGrid_METIS_partition(grid, size, cell_part);
    }
    break;
  case 2: // Node cell partition with metis
    if (rank == 0){
      std::cout << "Metis norne Node cell partitioner\n "; 
      NodeCell_metis(grid, node_part, cell_part, num_nodes, size);
    }
    break;    
  case 3: //METIS norne
    if (rank==0){
      std::cout << "METIS partitioner" << std::endl;
      CpGrid_METIS_partitionvN(grid, size, cell_part, comm_Well, trans );
    }
    break;
  case 4: //METIS norne; cell node
    if (rank==0){
      std::cout << "METIS partitioner" << std::endl;
      NodeCell_NorneMetis(grid, node_part, cell_part, comm_Well, trans, num_nodes, size);
    }
    break;
  case 5: // Node cell partition with metis norne
    if (rank == 0){
      std::cout << "Metis norne Node cell partitioner\n "; 
      CpGrid_METIS_partition(grid, size, cell_part);
    }
    break;
    
  case 6:
    if (rank == 0){
      std::cout << "Metis Uniform\n";
      CpGrid_METIS_partitionvUniform(grid, size, cell_part, comm_Well, trans);
    }
    break;
  case 7:
    if (rank == 0){
      std::cout << "Metis Uniform CellNode\n";
      CpGrid_METIS_partitionvUniform(grid, size, cell_part, comm_Well, trans);
    }
    break;
  case 8:
    if (rank == 0){
      std::cout << "Metis Uniform NodeCell\n";
      NodeCell_NorneMetisUniform(grid, node_part, cell_part, comm_Well, trans, num_nodes, size);
    }
    break;
  case 9: //cpGrid partitionen
    if (rank == 0)std::cout << "cpGrid partitioner\n";
    cell_partN = grid.getCellPartitionLoadBalance(&wells, trans.data(), 1, edgWgtMet);
    cell_part = cell_partN;
    break;
  case 10: //cpGrid partitioner and then later metis Node partition
    if (rank == 0)std::cout << "cpGrid partitioner\n";
    cell_partN = grid.getCellPartitionLoadBalance(&wells, trans.data(), 1, edgWgtMet);
    cell_part = cell_partN;
    break;
  }    
  
  
  
  // Broadcast information from rank 0 to the other ranks
  cc.broadcast(&cell_part[0], cell_part.size(), 0);

  if (rank == 0) std::cout << "\n\n";
  std::vector<int> CommTab(cc.size()*cc.size(), 0);
  ghost_cells_beforeLoadBalance(grid,cc,cell_part,CommTab);

  // Cell Node partitioner: Node partitioning
  if (partition_lib == 1 | partition_lib == 4 | partition_lib == 7 | partition_lib == 9) {
    if (rank == 0){
      CellNode_Partition(CommTab, cc.size(), cell_rank, cell_part, num_nodes);
      // Broadcast information from rank 0 to the other ranks
    }
    cc.broadcast(&cell_part[0], cell_part.size(), 0); 
  }
  if (partition_lib == 8){
    if (rank == 0)  bad_partition(CommTab, cc.size(), cell_rank, cell_part, num_nodes);
    cc.broadcast(&cell_part[0], cell_part.size(), 0); 
  }


  // Load balance
  grid.loadBalanceWithCellPartition(cell_part);



  // print loadbalancing results

  int resultsLoadbalancing[size];
  cc.barrier();
  int numCells=grid.numCells();
  cc.gather(&numCells,resultsLoadbalancing,1,0);
  if (rank == 0){
    std::cout<<"\n";
    for (int kk = 0; kk < size; kk++){

      std::cout << "After loadbalancing process " << kk << " has " << resultsLoadbalancing[kk] << " cells.\n"; 
    }
  }
  
  // Calculate where one rank's ghost cells belong to

  std::vector<int> comm_ghost(cc.size(),0); 
  ghost_cells_afterLoadBalance(grid,cc,cell_part,comm_ghost);
  

  // Calculate time spent on spMV and CopyOwnerToAll
  if (rank == 0) std::cout <<"\n\n";
  solve(grid,cc,A,rhs,x);


  
  /*  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  cc.barrier();
  std::cout << "NodeName: "<< rank << ": " << processor_name << std::endl;
  */



  // print comm tab
  if (rank == 0){
    std::cout << "\n\n\n";}

  std::vector<int> comm_ghost_reciever(cc.size()*cc.size());
  cc.gather(&comm_ghost[0], &comm_ghost_reciever[0], size, 0);

  
  if (cc.rank()==0){
    std::cout << "\t\t\t";
    for (int k=0;k<size;k++){
      std::cout<< "Rank " << k << "\t";
    }
    
    for (int r=0;r<size;r++){
      std::cout<< "\nRank " << r << "'s ghost cells:\t";
      
      for (int gc=r*size;gc<size*size;gc++){
	std::cout << comm_ghost_reciever[gc] << "\t";
	if (gc==r*size+size-1) break;
      }
    }
    std::cout<<"\n";
    


    display_partition_VTK(grid, cell_part, cc); 
  }

  return 0;
}
 
