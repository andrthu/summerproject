
#ifndef OPM_BUILDPOISSON_HEADER_INCLUDED
#define OPM_BUILDPOISSON_HEADER_INCLUDED

#endif //OPM_BUILDPOISSON_HEADER_INCLUDED



template<class Grid,class Mat, class Vec>
void buildPoisson(Grid &grid, Mat &T, Vec &rhs){

  rhs.resize (grid.numCells ());
  const int K=1;
  auto gridView = grid.leafGridView ();
  const int size=grid.numCells();
  for (auto && e : elements(gridView)) {
      //storing information about e's geometry
      auto e_geometry=e.geometry();
      auto e_center=e_geometry.center();
      double e_Volume=e_geometry.volume();
      auto lid = grid.localIdSet();
      int id_e=lid.id(e);
      if (e.partitionType()==InteriorEntity){
      for (auto && face : intersections(gridView, e)) {
	
	// check if e has a neighbor (meaning not on grid boundary). 
	    if (face.neighbor()) {


	      // e is current cell and n is its neighbor, f is face aka the intersection
	      // between the two cells
	      auto neighbor=face.outside();
	      auto n_geometry=neighbor.geometry();
	      auto n_center=n_geometry.center();
    	      auto f_geometry=face.geometry();
	      auto f_center=f_geometry.center();
	      double norm_ef=(f_center-e_center).two_norm();
	      double norm_nf=(f_center-n_center).two_norm();
	      //getting local center for normal vector
	      //dim =3 -> 3-1=2 : <double,dim-1>==<double,2>
	      const auto& intersectRE=ReferenceElements<double,2>::general(face.type());
	      auto localFaceCenter=intersectRE.position(0,0);
	      auto normal=face.unitOuterNormal(localFaceCenter);
	      
		
	      double A_f=f_geometry.volume();

	      double T_en=A_f*K*(f_center-e_center).dot(normal)/norm_ef;
	      double T_ne=A_f*K*(f_center-n_center).dot(-normal)/norm_nf;


	      int id_n=lid.id(neighbor);
	      T[id_e][id_n]=-1/((1/T_en)+(1/T_ne));
	      T[id_e][id_e]+=1/((1/T_en)+(1/T_ne));
	      //std::cout << T[id_e][id_n] << "\n";
	      
	}
	    else  {
	      
	      auto f_geometry=face.geometry();
	      auto f_center=f_geometry.center();
	      double norm_ef=(f_center-e_center).two_norm();
	      
	      //getting local center for normal vector

	      //dim=3
	      const auto& intersectRE=ReferenceElements<double,2>::general(face.type());
	      auto localFaceCenter=intersectRE.position(0,0);
	      auto normal=face.unitOuterNormal(localFaceCenter);
	      
		
	      double A_f=f_geometry.volume();
	      double T_en=A_f*K*(f_center-e_center).dot(normal)/norm_ef;

	      T[id_e][id_e] += T_en;
	      rhs[id_e]+=1*T_en;	      
	    }
      }}
      else{
	T[id_e][id_e]=1;
      }

   }

}
