
#ifndef OPM_SOLVE_HEADER_INCLUDED
#define OPM_SOLVE_HEADER_INCLUDED

#endif //OPM_SOLVE_HEADER_INCLUDED

/*
#include "overlappingOperations/parallelSolver.hpp"
#include "assemble/buildPoisson.hpp"
#include "assemble/initSparseMatrix.hpp"
#include "overlappingOperations/parallelInfo.hpp"
*/

template<class Grid, class CollectiveCommunication,class Mat, class Vec>
void solve(Grid &grid, CollectiveCommunication &cc,Mat &A, Vec &rhs,Vec &x){

  Vec y(grid.numCells());
  y=0;
  
  auto info =getParallelInfo(grid);
  initSparseMatrix(grid,A);
  //buildPoisson(grid,A,rhs);
  A = 1;
  rhs.resize (grid.numCells ());
  rhs = 0;
  parallelSolve(A,rhs,x,y,cc,info);
}
