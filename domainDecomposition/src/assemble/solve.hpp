
#ifndef OPM_SOLVE_HEADER_INCLUDED
#define OPM_SOLVE_HEADER_INCLUDED

#endif //OPM_SOLVE_HEADER_INCLUDED

/*
#include "overlappingOperations/parallelSolver.hpp"
#include "assemble/buildPoisson.hpp"
#include "assemble/initSparseMatrix.hpp"
#include "overlappingOperations/parallelInfo.hpp"
*/

template<class Grid, class CollectiveCommunication,class Mat, class Vec>
void solve(Grid &grid, CollectiveCommunication &cc,Mat &A, Vec &rhs,Vec &x){

  Vec y(grid.numCells());
  y=0;
  
  auto info =getParallelInfo(grid);
  initSparseMatrix(grid,A);
  buildPoisson(grid,A,rhs);
  parallelSolve(A,rhs,x,y,cc,info);

  double norms [cc.size ()];

  double norm = x.two_norm2()/x.size();
  cc.gather (&norm, norms, 1, 0);
  /*
  if (cc.rank () == 0) {
    for (int k =0; k < cc.size (); ++k)
      std::cout<< "\n norm of x: "<< norms [k]<<std::endl;
  }
  
  */
}
