#ifndef OPM_INITSPARSEMATRIX_HEADER_INCLUDED
#define OPM_INITSPARSEMATRIX_HEADER_INCLUDED

#endif //OPM_INITSPARSEMATRIX_HEADER_INCLUDED


template<class Grid, class Mat>
void initSparseMatrix(Grid &grid, Mat &M){

  auto gridView=grid.leafGridView();
  const int size=grid.numCells();
  typedef Dune::MatrixIndexSet AdjecencyPattern;
  AdjecencyPattern adjDiag;
  adjDiag.resize(size,size);
  auto lid = grid.localIdSet();




  for (auto && e : elements(gridView)) {

      int id_e=lid.id(e);
      adjDiag.add(id_e,id_e);
      //loop over faces of cell e
      for (auto && face : intersections(gridView, e)) {

	    // check if e has a neighbor (meaning not on grid boundary). 
	    if (face.neighbor()) {

		// Get cell index of neighbouring cell
		int cellNab = lid.id(face.outside());
		//std::cout<<"id"<< id_e << "id nab"<<cellNab<<"\n";
		adjDiag.add(id_e,cellNab);
	       
	    }
      }

   }

    adjDiag.exportIdx(M);


}
