
#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>



#include <mpi.h>
#include <metis.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include "io/displayPartition.hpp"
#include "partitioners/metisGraphPartition.hpp"
//#include "partitioners/NodeCell_metis.hpp"

#include "io/readIni.hpp"
#include "io/readEclipseCase.hpp"





int main(int argc, char ** argv)
{
    // Set MPI commuicator types
    Dune::MPIHelper::instance(argc, argv);
    typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
    typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;

    ReadProperties readProperties;

    Dune::CpGrid grid;

    if (argc > 1)
	readProperties.init_grid(grid, argv[1], true);
    
    
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    // Define MPI comunicator
    CollectiveCommunication cc(MPI_COMM_WORLD);
    // Get rank of processor and size of communicator (number of ranks).
    int rank = cc.rank();
    int size = cc.size();


    const auto wells = readProperties.getWells();

    auto trans = readProperties.getTransVec();
    std::vector<double> dummy = {0,0,0};
    int edgWgtMet = 1; //0=uniform, 1=trans, 2=log(trans)
    //grid.loadBalance(&wells, trans.data(), edgWgtMet, 0, 1, dummy, 1);


    std::vector<int> cell_part = grid.getCellPartitionLoadBalance(&wells, trans.data(), 1, edgWgtMet);
    grid.loadBalanceWithCellPartition(cell_part);

    
    // Distribute result of partitioning to all ranks
    cc.broadcast(&cell_part[0], cell_part.size(), 0);

    // Set up parallel grid. 
    grid.loadBalanceWithCellPartition(cell_part);

    // Write out rank of each cell to vtk format
    display_partition_VTK(grid, cell_part, cc);

    return 0;
}
