// discretization of poisson's equation using TPFA

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>


#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/operators.hh>


#include <dune/common/function.hh>

#include <mpi.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>
using namespace Dune;


#include "assemble/initSparseMatrix.hpp"
#include "assemble/buildPoisson.hpp"


template<class Mat>
void print_matrix(Mat& m)
{
    int N = m.N();
    int M = m.M();
    
    for (int i = 0; i < N; ++i) {
	for (int j = 0; j < M; ++j) {
	    if (m.exists(i, j)) {
		std::cout << m[i][j] << " ";
	    }
	    else {
		std::cout << "0 ";
	    }
	}
	std::cout << std::endl;
    }
}

int main(int argc, char ** argv)
{
  int K=1;
    Dune::MPIHelper::instance(argc, argv);
    //====== How to create a trivial Corner-point grid ===========
    const std::array<int,3> dims = {10,3,3};
    const std::array<double,3> cellsize = {0.1,0.1,0.1};
    Dune::CpGrid grid;
    grid.createCartesian(dims, cellsize);
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    
    //const int dim = GridView::dimension == 2;
    auto lid = grid.localIdSet();

    // Dune Block Vector and Vector types
    typedef Dune::FieldVector<double,1> BlockVec;
    typedef Dune::BlockVector<BlockVec> Vec;

    // Dune Block Matrix and CSR sparse Matrix format types
    typedef Dune::FieldMatrix<double,1,1> BlockMat;
    typedef Dune::BCRSMatrix<BlockMat> Mat;

    // Type for class that defines an adjecency pattern used to set up sparse matrix
    //typedef Dune::MatrixIndexSet AdjecencyPattern;
 
    //AdjecencyPattern adjDiag;
    const int size=grid.numCells();
    std::cout<<size<<std::endl;
    // Ap=q
    // q=vector?
    Vec rhs(size);
    
    //adjDiag.resize(size,size);
    // adding non-zero elements on the diagonals
    /* for (auto && e : elements(gridView)) {

      int id_e=lid.id(e);
      adjDiag.add(id_e,id_e);
      //loop over faces of cell e
      for (auto && face : intersections(gridView, e)) {

	    // check if e has a neighbor (meaning not on grid boundary). 
	    if (face.neighbor()) {

		// Get cell index of neighbouring cell
		int cellNab = lid.id(face.outside());
		//std::cout<<"id"<< id_e << "id nab"<<cellNab<<"\n";
		adjDiag.add(id_e,cellNab);
	       
	    }
      }

   }

    // initializing the two matrices T and A
    */
    Mat T;
    // adjDiag.exportIdx(T);
    
    // filling T with the corresponding transmissibilities values
    /*    for (auto && e : elements(gridView)) {
      //storing information about e's geometry
      auto e_geometry=e.geometry();
      auto e_center=e_geometry.center();
      double e_Volume=e_geometry.volume();
      int id_e=lid.id(e);
      for (auto && face : intersections(gridView, e)) {

	    // check if e has a neighbor (meaning not on grid boundary). 
	    if (face.neighbor()) {


	      // e is current cell and n is its neighbor, f is face aka the intersection
	      // between the two cells
	      auto neighbor=face.outside();
	      auto n_geometry=neighbor.geometry();
	      auto n_center=n_geometry.center();
    	      auto f_geometry=face.geometry();
	      auto f_center=f_geometry.center();
	      double norm_ef=(f_center-e_center).two_norm();
	      double norm_nf=(f_center-n_center).two_norm();
	      //getting local center for normal vector
	      
	      const auto& intersectRE=ReferenceElements<double,dim-1>::general(face.type());
	      auto localFaceCenter=intersectRE.position(0,0);
	      auto normal=face.unitOuterNormal(localFaceCenter);
	      
		
	      double A_f=f_geometry.volume();

	      double T_en=A_f*K*(f_center-e_center).dot(normal)/norm_ef;
	      double T_ne=A_f*K*(f_center-n_center).dot(-normal)/norm_nf;


	      int id_n=lid.id(neighbor);
	      T[id_e][id_n]=-1/((1/T_en)+(1/T_ne));
	      T[id_e][id_e]+=1/((1/T_en)+(1/T_ne));
	      //std::cout << T[id_e][id_n] << "\n";
	      
	}
	    else  {
	      
	      auto f_geometry=face.geometry();
	      auto f_center=f_geometry.center();
	      double norm_ef=(f_center-e_center).two_norm();
	      
	      //getting local center for normal vector
	      
	      const auto& intersectRE=ReferenceElements<double,dim-1>::general(face.type());
	      auto localFaceCenter=intersectRE.position(0,0);
	      auto normal=face.unitOuterNormal(localFaceCenter);
	      
		
	      double A_f=f_geometry.volume();
	      double T_en=A_f*K*(f_center-e_center).dot(normal)/norm_ef;

	      T[id_e][id_e] += T_en;
	      rhs[id_e]+=T_en;	      
	    }
      }

   }
    */
    initSparseMatrix(grid,T);
    buildPoisson(grid,T,rhs);
    
    std::cout <<"A\n" << std::endl;
    // print_matrix(T);

    MatrixAdapter<Mat,Vec,Vec> op (T);

    SeqILU0<Mat,Vec,Vec> ilu(T,0.99);
    CGSolver<Vec> cg (op,ilu,1e-8,100,2);
    InverseOperatorResult r;

    Vec x(size);
    x=0;
    cg.apply(x,rhs,r);
    for (auto && temp:x){
      std::cout<<temp <<"\t";}
    //solve Ap=q
}
