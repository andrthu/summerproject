// Testing DisplayPartition
/*

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>


#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/operators.hh>


#include <dune/common/function.hh>

#include <mpi.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>
using namespace Dune;
#include "io/displayPartition.hpp"
int main(int argc, char **argv){
    const std::array<int,3> dims = {10,3,3};
    const std::array<double,3> cellsize = {0.1,0.1,0.1};
    Dune::CpGrid grid;
    grid.createCartesian(dims, cellsize);
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    
    //const int dim = GridView::dimension == 2;
    auto lid = grid.localIdSet();
    
    // Dune Block Vector and Vector types
    typedef Dune::FieldVector<double,1> BlockVec;
    typedef Dune::BlockVector<BlockVec> Vec;

    // Dune Block Matrix and CSR sparse Matrix format types
    typedef Dune::FieldMatrix<double,1,1> BlockMat;
    typedef Dune::BCRSMatrix<BlockMat> Mat;
    int cc=5;
    vector<int> cell_part;
    cell_part.push_back(3);

    display_partition_VTK(grid,cell_part,cc);



}
*/
