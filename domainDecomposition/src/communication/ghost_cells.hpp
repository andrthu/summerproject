template<class Grid, class CollectiveCommunication>
void ghost_cells_afterLoadBalance(Grid &grid, CollectiveCommunication &cc, std::vector<int> &cell_part){


  auto gid    = grid.globalIdSet();
  auto gv     = grid.leafGridView();
  int size    = cc.size();

  std::vector<int> comm_ghost(cc.size(),0);

  std::vector<int> comm_ghost_reciever(cc.size()*cc.size());

  for (auto&& e: elements(gv)){

    int id_e              = gid.id(e);
    int current_rank      = cc.rank();
    int origin            = cell_part[id_e];

    if (current_rank!=origin) comm_ghost[origin]++;

  }

  cc.gather(&comm_ghost[0], &comm_ghost_reciever[0], size, 0);


  if (cc.rank()==0){
    std::cout << "\t\t\t";
    for (int k=0;k<size;k++){
      std::cout<< "Rank " << k << "\t";
    }
    
    for (int r=0;r<size;r++){
      std::cout<< "\nRank " << r << "'s ghost cells:\t";
      
      for (int gc=r*size;gc<size*size;gc++){
	std::cout << comm_ghost_reciever[gc] << "\t";
	if (gc==r*size+size-1) break;
      }
    }
    std::cout<<"\n";
    


  }

  // for cc.gatherv
/*
    int resclen[ size];
    int start [size];
    int sta =0;
    for (int kk =0; kk<size;++kk) {
      start[kk] = sta;
      resclen[kk] = size;
      sta += size;
    }
// cc.gatherv (&comm_ghost[0], size, &comm_ghost2[0], resclen, start, 0);
*/

}



