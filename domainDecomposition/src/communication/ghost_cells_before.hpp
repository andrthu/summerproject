template<class Grid, class CollectiveCommunication>
void ghost_cells_beforeLoadBalance(Grid &grid, CollectiveCommunication &cc, std::vector<int> &cell_part,std::vector<int> & ghost_cells_reciever){


  auto gid                 = grid.globalIdSet();
  auto gv                  = grid.leafGridView();
  int size                 = cc.size();
  //  std::set<int>::iterator    it;
  std::set<int>              id_ghost_cells;
  std::vector<int>           ghost_cells(size,0);
  
  for (auto && e: elements(gv)){

    int id_e = gid.id(e);  

    for (auto && face: intersections(gv,e)){

      if (face.neighbor()) {

	int id_neighbor = gid.id(face.outside());
	
	if (id_ghost_cells.find(id_neighbor) == id_ghost_cells.end() &  cc.rank() == cell_part[id_e]){

	  if (cell_part[id_e]!=cell_part[id_neighbor]){

	    id_ghost_cells.insert(id_neighbor);
	    ghost_cells[cell_part[id_neighbor]]++;
	  }
	}	
      }      
    } 
  }
  

  //  std::cout << "checkpoint\n";

  cc.gather(&ghost_cells[0],&ghost_cells_reciever[0],size,0);

  
  if (cc.rank()==0){
    // std::cout << 
    std::cout << "\t\t\t";

    for (int k=0;k<size;k++){

      std::cout<< "Rank " << k << "\t";
    }
    
    for (int r=0;r<size;r++){

      std::cout<< "\nFrom rank " << r << " to: \t";
      
      for (int gc=r*size;gc<r*size+size;gc++){

	std::cout << ghost_cells_reciever[gc] << "\t";


      }
    }
    std::cout<<"\n";
  }
}
