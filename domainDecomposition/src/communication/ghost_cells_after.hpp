template<class Grid, class CollectiveCommunication>
void ghost_cells_afterLoadBalance(Grid &grid, CollectiveCommunication &cc, std::vector<int> &cell_part, std::vector<int> &comm_ghost){


  auto gid    = grid.globalIdSet();
  auto gv     = grid.leafGridView();
  int size    = cc.size();





  for (auto&& e: elements(gv)){
    int id_e              = gid.id(e);
    int current_rank      = cc.rank();
    int origin            = cell_part[id_e];

    if (current_rank!=origin) comm_ghost[origin]++;

  }
}


// for cc.gatherv
/*
    int resclen[ size];
    int start [size];
    int sta =0;
    for (int kk =0; kk<size;++kk) {
      start[kk] = sta;
      resclen[kk] = size;
      sta += size;
    }
// cc.gatherv (&comm_ghost[0], size, &comm_ghost2[0], resclen, start, 0);
*/
