#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <string>
#include <set>
#include <chrono>



#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <dune/geometry/referenceelements.hh>

#include <mpi.h>
#include <scotch.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include "io/readIni.hpp"
#include "io/displayPartition.hpp"
#include "partitioners/scotchGraphPartition.hpp"
#include "partitioners/zoltanGraphPartition.hpp"
#include <metis.h>
#include "partitioners/metisGraphPartition.hpp"
#include "partitioners/CellNode_Partition.hpp"
#include "partitioners/bad_partition.hpp"
#include "partitioners/NodeCell_metis.hpp"
#include "partitioners/NodeCell_scotch.hpp"
using namespace Dune;
#include "overlappingOperations/parallelInfo.hpp"
#include "overlappingOperations/parallelSolver.hpp"
#include "assemble/buildPoisson.hpp"
#include "assemble/initSparseMatrix.hpp" 
#include "assemble/solve.hpp"
#include "communication/ghost_cells_after.hpp"
#include "communication/ghost_cells_before.hpp"

int main(int argc, char ** argv) {

  // Define dictionary for reading from ini
  DictRead    dr;
 
  // Reading from ini file
  if (argc > 1)
    dr.read_file_and_update(argv[1]);
  

  // Store data from dictionary
  int partition_lib     = std::stoi(dr.dict[2]); // 0 metis, 1 scotch, 2 zoltan, 3-5 CellNode, 6 NodeCell_metis, 7 NodeCell_scotch
  int xx                = std::stoi(dr.dict[3]);
  int yy                = std::stoi(dr.dict[4]);
  int zz                = std::stoi(dr.dict[5]);
  std::string tol       = dr.dict[6]; // imbalance tol 
  int num_nodes         = std::stoi(dr.dict[7]); 


  bool cont = false;

  
  // Set MPI commuicator types
  Dune::MPIHelper::instance(argc, argv);
  typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
  typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;

  // Dune Block vector format type
  typedef Dune::FieldVector<double,1> BlockVec;
  typedef Dune::BlockVector<BlockVec> Vec;

  // Dune Block Matrix and CSR sparse Matrix format types
  typedef Dune::FieldMatrix<double,1,1> BlockMat;
  typedef Dune::BCRSMatrix<BlockMat> Mat;
    
  //====== How to create a trivial Corner-point grid ===========
  const std::array<int,3> dims = {xx,yy,zz};
  const std::array<double,3> cellsize = {1.0,1.0,1.0};
  Dune::CpGrid grid;
  grid.createCartesian(dims, cellsize);
  typedef Dune::CpGrid::LeafGridView GridView;
  GridView gridView = grid.leafGridView();
  //=============================================================
  
  // Define MPI comunicator
  CollectiveCommunication cc(MPI_COMM_WORLD);
  
  // Get rank of processor and size of communicator (number of ranks).
  int rank = cc.rank();
  int size = cc.size();

  // Matrix and vectors for mvp 
  Mat         A;
  Vec         rhs,x;

  // Initialize vector for cells rank and for node part
  std::vector<int> cell_part(grid.numCells(), rank);
  std::vector<int> cell_rank(cc.size(), 0);
  std::vector<int> node_part(grid.numCells(), rank);

  if (rank == 0){
    std::cout << "Number of cores/ranks per node is: " << size/num_nodes << "\n";
    }
  
  // deciding partitioner
  switch (partition_lib){
    
    
  case 0: //METIS
    if (rank==0){
      std::cout << "METIS partitioner" << std::endl;
      CpGrid_METIS_partition(grid, size, cell_part);
    }
    break;
    
  case 1: //SCOTCH
    if (rank==0){
      std::cout << "Scotch partitioner" << std::endl;
      CpGrid_Scotch_partition(grid, size, cell_part,tol);
    }
    break;
    
  case 2 : //zoltan
    if (rank == 0)
      std::cout << "Zoltan partitioner" << std::endl;
    CpGrid_Zoltan_partition(grid, cc, size, rank, cell_part,tol);
    break;
    
  case 3: // Cell Node partition with metis cell partition and then metis node partition
    if (rank==0){
      std::cout << "METIS partitioner with metis node partitioner" << std::endl;
      CpGrid_METIS_partition(grid, size, cell_part);
    }
    break;
  case 4: // Cell node partition with scotch cell partition and then metis node partition
    if (rank==0){
      std::cout << "Scotch partitioner with metis node partitioner" << std::endl;
      CpGrid_Scotch_partition(grid, size, cell_part,tol);
    }
    break;
  case 5: // cell node partition with zoltan cell partition and then metis node partition
    if (rank == 0)
      std::cout << "Zoltan partitioner with metis node partitioner" << std::endl;
    CpGrid_Zoltan_partition(grid, cc, size, rank, cell_part,tol);
    break;  

  case 6: // Node cell partition with metis
    if (rank == 0){
      std::cout << "Metis Node cell partitioner\n "; 
      NodeCell_metis(grid, node_part, cell_part, num_nodes, size);
    }
    break;
  case 7: // Node cell partition with scotch
    if (rank == 0){
      std::cout << "Scotch Node cell partitioner\n ";
      NodeCell_Scotch(grid, node_part, cell_part, num_nodes, size);
    }
    break;
  case 8: // bad partition
    if (rank == 0){
      std::cout <<"Bad partitioner\n ";
      CpGrid_METIS_partition(grid, size, cell_part);
    }
  }
  
  
  
  // Broadcast information from rank 0 to the other ranks
  cc.broadcast(&cell_part[0], cell_part.size(), 0);

  if (rank == 0) std::cout << "\n\n";
  std::vector<int> CommTab(cc.size()*cc.size(), 0);
  ghost_cells_beforeLoadBalance(grid,cc,cell_part,CommTab);

  // Cell Node partitioner: Node partitioning
  if (partition_lib == 3 | partition_lib == 4 | partition_lib == 5) {
    if (rank == 0){
      CellNode_Partition(CommTab, cc.size(), cell_rank, cell_part, num_nodes);
      // Broadcast information from rank 0 to the other ranks
    }
    cc.broadcast(&cell_part[0], cell_part.size(), 0); 
  }
  if (partition_lib == 8){
    if (rank == 0)  bad_partition(CommTab, cc.size(), cell_rank, cell_part, num_nodes);
    cc.broadcast(&cell_part[0], cell_part.size(), 0); 
  }


  // Load balance
  if (rank == 0) std::cout << "\n\n";
  grid.loadBalanceWithCellPartition(cell_part);

  // Calculate where one rank's ghost cells belong to

  std::vector<int> comm_ghost(cc.size(),0); 
  ghost_cells_afterLoadBalance(grid,cc,cell_part,comm_ghost);
  

  // Solve Poisson's equation and calculate time spent
  if (rank == 0) std::cout <<"\n\n";
  solve(grid,cc,A,rhs,x);


  /*
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  cc.barrier();
  std::cout << "NodeName: "<< rank << ": " << processor_name << std::endl;
  */



  // print comm tab
  if (rank == 0){
    std::cout << "\n\n\n";}

  std::vector<int> comm_ghost_reciever(cc.size()*cc.size());
  cc.gather(&comm_ghost[0], &comm_ghost_reciever[0], size, 0);

  
  if (cc.rank()==0){
    std::cout << "\t\t\t";
    for (int k=0;k<size;k++){
      std::cout<< "Rank " << k << "\t";
    }
    
    for (int r=0;r<size;r++){
      std::cout<< "\nRank " << r << "'s ghost cells:\t";
      
      for (int gc=r*size;gc<size*size;gc++){
	std::cout << comm_ghost_reciever[gc] << "\t";
	if (gc==r*size+size-1) break;
      }
    }
    std::cout<<"\n";
    



  }

  return 0;
}
 
