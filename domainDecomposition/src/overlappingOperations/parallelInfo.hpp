/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_PARALLELINFO_HEADER_INCLUDED
#define OPM_PARALLELINFO_HEADER_INCLUDED

#endif //OPM_PARALLELINFO_HEADER_INCLUDED

template<class G>
Dune::IndexInfoFromGrid<int,int> getParallelInfo(const G& grid)
{
    auto pid = grid.getCellIndexSet();
    const auto& rid = grid.getCellRemoteIndices();

    Dune::IndexInfoFromGrid<int,int> info;

    for (auto && i : pid) {
	info.addLocalIndex(std::tuple<int,int,int>(i.global(), i.local().local(), i.local().attribute()));
    }
  
    for (auto && r : rid) {
	int rank = r.first;
	
	for(auto && rr : *r.second.first) {
	    auto glob = rr.localIndexPair().global();
	    auto ra = rr.attribute();
	    auto la = rr.localIndexPair().local().attribute();
	    info.addRemoteIndex(std::tuple<int,int,int>(rank,glob,ra));
	}
    }
    return info;
}

