/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPM_PARALLELSOLVER_HEADER_INCLUDED
#define OPM_PARALLELSOLVER_HEADER_INCLUDED

#endif // OPM_PARALLELSOLVER_HEADER_INCLUDED
#include <dune/common/timer.hh>
#include <algorithm>
#include <iterator>
template<class Mat, class Vec, class CC, class PI>
void parallelSolve(Mat& A, Vec& rhs, Vec& x, Vec&y, const CC& cc, PI info)
{
    typedef Dune::OwnerOverlapCopyCommunication<int,int>        Comm;
    typedef Dune::OverlappingSchwarzScalarProduct<Vec,Comm>     ScalarProduct; 
    typedef Dune::OverlappingSchwarzOperator<Mat,Vec,Vec,Comm>  Operator;
    typedef Dune::SeqILU0<Mat,Vec,Vec>                          ILU; 
    typedef Dune::BlockPreconditioner<Vec,Vec,Comm,ILU>         Pre;
    typedef Dune::CGSolver<Vec>                                 Solver;
    typedef Dune::InverseOperatorResult                         Stat;

    int linItr = 200;
    double tol = 1e-4;

    int rank = cc.rank();
    int verbose = 0;
    if (rank==0)
	verbose = 2;

    Comm           comm(info, cc);
    ScalarProduct  sp(comm);
    Operator       linOp(A, comm);
    ILU            SILU(A, 0.99);
    Pre            BJILU(SILU, comm);
    Solver         cg(linOp, sp, BJILU, tol, linItr, verbose);
    Stat           statistics;

    x.resize(A.N());
    x = 0;
    
    cg.apply(x, rhs, statistics);

    if (cc.rank()==0){
      std::cout<< "\n Elapsed time: "<< statistics.elapsed <<std::endl;
 
    }
    
    // calculating time spent on matrix-vector product
    Dune::Timer timer;
    double  mv_times[cc.size()];
    cc.barrier();
    timer.reset();
    timer.start();
    A.mv(x,y); // y= Ax
    double mv_time =  timer.stop();
    
    cc.gather (&mv_time, mv_times, 1, 0);
    
    
    if (cc.rank() == 0) {
      double avg_time_mv = 0;
      for (int k = 0; k < cc.size (); k++){
	std::cout<<"Rank "<<k<< ": Matrix-vector product took " << mv_times[k] << " seconds\n";
	avg_time_mv+=mv_times[k];
      }
      std::cout << "Average time for Matrix-vector product is " << avg_time_mv/cc.size() << " seconds\n\n";    
    }
    int loops = 50;
    double tmax[loops];
    for (int i = 0; i < loops; i++){

      cc.barrier();
      timer.reset();
      timer.start();
      comm.copyOwnerToAll(x,x);
      double duration = timer.stop();
      double t = cc.max(duration);
      if (rank == 0){
	tmax[i] = t;
      }
    }

    if (cc.rank() == 0){
      double min_time123 = 2.0;
      double avg_time123 = 0.0;
      for (auto &&ti : tmax){
	if (ti < min_time123) min_time123 = ti;
	avg_time123+=ti;
      }
      for (int kk = 0; kk < cc.size();kk++){
	std::cout << "Rank " << kk << ": copyOwnerToAll took " << min_time123 << " seconds\n";  
      }
      std::cout << "Average time for copyOwnertoAll is " << avg_time123/loops << " seconds\n";
    }
    
    // calculating time for communication
    /*    double  copyOwnertoAll_times[cc.size()];
    cc.barrier();
    timer.reset();
    timer.start();
    comm.copyOwnerToAll(x,x);
    
    double duration = timer.stop();
    cc.gather (&duration, copyOwnertoAll_times, 1, 0);
    
    if (cc.rank() == 0) {
      double avg_time_cc = 0;
      for (int k = 0; k < cc.size (); k++){

	std::cout<<"Rank "<<k<< ": copyOwnerToAll took " << copyOwnertoAll_times[k] << " seconds\n";
 	avg_time_cc+=copyOwnertoAll_times[k];
      }
    std::cout << "Average time for copyOwnertoAll is " << avg_time_cc/cc.size() << " seconds\n";
    
    }*/
}
