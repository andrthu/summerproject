/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>


#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>

template<class Mat>
void print_matrix(Mat& m)
{
    int N = m.N();
    int M = m.M();
    
    for (int i = 0; i < N; ++i) {
	for (int j = 0; j < M; ++j) {
	    if (m.exists(i, j)) {
		std::cout << m[i][j] << " ";
	    }
	    else {
		std::cout << "0 ";
	    }
	}
	std::cout << std::endl;
    }
}

int main(int argc, char ** argv)
{
    const int size = 5;

    // Dune Block Vector and Vector types
    typedef Dune::FieldVector<double,1> BlockVec;
    typedef Dune::BlockVector<BlockVec> Vec;

    // Dune Block Matrix and CSR sparse Matrix format types
    typedef Dune::FieldMatrix<double,1,1> BlockMat;
    typedef Dune::BCRSMatrix<BlockMat> Mat;

    // Type for class that defines an adjecency pattern used to set up sparse matrix
    typedef Dune::MatrixIndexSet AdjecencyPattern;
    
    // Create Vector of size 5, and set it to 1
    Vec x(size);
    x=1;

    // Set up adjecency pattern of diagonal matrix D
    AdjecencyPattern adjDiag;
    //set matrix dimension
    adjDiag.resize(size,size);

    // Add non-zero elements on the diagonal in pattern 
    for (int idx = 0; idx < size; ++idx)
	adjDiag.add(idx, idx);

    // Create Diagonal matrix using adjecency pattern
    Mat D;
    adjDiag.exportIdx(D);

    //Set non-zero elements of D to 0.5
    D=0.5;

    Vec y(size);
    y=0;
    
    // Matrix-vector products:
    D.mv(x, y); // y = Ax
    //D.umv(x, y); // y += Ax
    //D.mmv(x, y); // y -= Ax
    //D.usmv(alpha, x, y); // y += alpha * Ax


    // Adjecency pattern for non-diagonal matrix A
    AdjecencyPattern adjA = adjDiag;
    for (int idx = 0; idx < size - 1; ++idx)
	adjA.add(idx, idx+1);

    // Set A matrix
    Mat A;
    adjA.exportIdx(A);
    A=1;
    for (int idx = 0; idx < size - 1; ++idx)
	A[idx][idx+1] = -1;

    // Inner product <x,y>
    double alpha = x.dot(y);
    A.usmv(-alpha,x,y);

    // Print matrices
    std::cout << "\nD:"<<std::endl;
    print_matrix(D);
    std::cout << "\nA:"<<std::endl;
    print_matrix(A);

    // print y = x-ADx:
    std::cout << "\ny = Dx - <x,Dx>Ax:"<<std::endl;
    for (int idx = 0; idx < size; ++idx)
	std::cout << y[idx] << std::endl;
    
    return 0;
}
