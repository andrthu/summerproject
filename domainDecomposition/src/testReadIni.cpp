/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>


#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>


#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include "io/readIni.hpp"
#include "io/readEclipseCase.hpp"

template<class G, class WC>
void init_connections(const G& grid, WC& wc, std::vector<const Opm::Well*> wells)
{
    const auto& cpgdim = grid.logicalCartesianSize();
    
    std::vector<int> cartesian_to_compressed(cpgdim[0]*cpgdim[1]*cpgdim[2], -1);
    for( int i=0; i < grid.numCells(); ++i )
    {
        cartesian_to_compressed[grid.globalCell()[i]] = i;
    }
    
    wc.init(wells, cpgdim, cartesian_to_compressed);
}


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    DictRead dr;
    
    if (argc > 1)
	dr.read_file_and_update(argv[1]);

    dr.write_param();

    ReadProperties readProperties;

    Dune::CpGrid grid;

    if (argc > 2)
	readProperties.init_grid(grid, argv[2], true);

    const auto wells = readProperties.getWells();
    auto trans = readProperties.getTransVec();
    std::vector<double> dummy = {0,0,0};
    int edgWgtMet = 1; //0=uniform, 1=trans, 2=log(trans)
    //grid.loadBalance(&wells, trans.data(), edgWgtMet, 0, 1, dummy, 1);
    

    std::vector<int> cell_part = grid.getCellPartitionLoadBalance(&wells, trans.data(), 1, edgWgtMet);
    grid.loadBalanceWithCellPartition(cell_part);

    Dune::cpgrid::WellConnections wc;
    init_connections(grid, wc, wells);

    std::cout << "\nWell connections\n";  
    int wellNum = 1;
    for ( const auto& well : wc ) {
	std::cout << "Well " << wellNum << ": ";
	for ( auto perf = well.begin(); perf != well.end(); ++perf)
	    std::cout << *perf << " ";
	std::cout << std::endl;
	wellNum++;
    }
    
    return 0;

}

