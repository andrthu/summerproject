/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>


#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>



#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>

#include <dune/common/function.hh>

#include <mpi.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>


#include "io/readIni.hpp"
#include "io/readEclipseCase.hpp"

int main(int argc, char ** argv)
{
    
    Dune::MPIHelper::instance(argc, argv);
    /*    
    //====== How to create a trivial Corner-point grid ===========
    const std::array<int,3> dims = {10,10,3};
    const std::array<double,3> cellsize = {1.0,1.0,1.0};
    Dune::CpGrid grid;
    grid.createCartesian(dims, cellsize);
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    //=============================================================
    */
    //partition the grid

    ReadProperties readProperties;

    Dune::CpGrid grid;

    if (argc > 1)
	readProperties.init_grid(grid, argv[1], true);

    std::cout << grid.numCells() << std::endl;
    grid.loadBalance();
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();

    std::cout << "\n" << "Number of cells in corner-point grid is: " << grid.numCells() << std::endl;
    
    // Class capable of mapping cells to their index. 
    auto lid = grid.localIdSet();
    // lid and gid only different when grid is partitioned and parallelized.
    auto gid = grid.globalIdSet();
    
    // Loop over grid cells version 1:
    for (auto && e : elements(gridView)) {

	int numFace = 0;
	//loop over faces of cell e
	for (auto && face : intersections(gridView, e)) {

	    // check if has a neighbor (meaning not on grid boundary). 
	    if (face.neighbor()) {
		numFace++;

		// Get cell index of neighbouring cell
		int cellNab = lid.id(face.outside());
	    }
	}
	std::cout << "Cell " << lid.id(e) <<" has "<< numFace<< " faces" << std::endl;

	// Other cell information:
	// e.volume();
	// e.center();
	// e.partitionType();
    }

    //loop over grid cells version 2:
    for (int cell = 0; cell < grid.numCells(); ++cell) {

	//loop over faces of cell cell 
	for (int loc_face = 0; loc_face < grid.numCellFaces(cell); ++ loc_face) {

	    // Global index of local face
	    int face = grid.cellFace(cell, loc_face);

	    // Two cell indecies of global face 
	    int faceCell0 = grid.faceCell(face, 0);
	    int faceCell1 = grid.faceCell(face, 1);

	    // Find index of neighbor cell of cell, connected through face. 
	    int nabCell = faceCell0 == cell ? faceCell1 : faceCell0;
	}
    }
    
    return 0;
}
