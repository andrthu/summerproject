/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>



#include <mpi.h>
#include <metis.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include "io/displayPartition.hpp"
#include "partitioners/metisGraphPartition.hpp"
//#include "partitioners/NodeCell_metis.hpp"

#include "io/readIni.hpp"
#include "io/readEclipseCase.hpp"





int main(int argc, char ** argv)
{
    // Set MPI commuicator types
    Dune::MPIHelper::instance(argc, argv);
    typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
    typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;
    

    ReadProperties readProperties;

    Dune::CpGrid grid;

    if (argc > 1)
	readProperties.init_grid(grid, argv[1], true);
    else {
	
	//====== How to create a trivial Corner-point grid ===========
	const std::array<int,3> dims = {10,10,3};
	const std::array<double,3> cellsize = {1.0,1.0,1.0};
	grid.createCartesian(dims, cellsize);
	//=============================================================
    }
    
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    // Define MPI comunicator
    CollectiveCommunication cc(MPI_COMM_WORLD);
    // Get rank of processor and size of communicator (number of ranks).
    int rank = cc.rank();
    int size = cc.size();
    int tol=300;

    // Partition grid using METIS on rank 0.
    std::vector<int> node_part(grid.numCells(), rank);
    std::vector<int> cell_part(grid.numCells(), rank);
    if (rank == 0) {
      CpGrid_METIS_partition(grid, size, cell_part,tol);
      //      NodeCell_metis(grid,node_part, cell_part,3, size);
    }


    
    // Distribute result of partitioning to all ranks
    cc.broadcast(&cell_part[0], cell_part.size(), 0);

    // Set up parallel grid. 
    grid.loadBalanceWithCellPartition(cell_part);

    // Write out rank of each cell to vtk format
    display_partition_VTK(grid, cell_part, cc);


    
    return 0;
}
