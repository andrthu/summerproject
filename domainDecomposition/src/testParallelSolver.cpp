/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <mpi.h>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/cpgrid/CpGridData.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>


#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <dune/geometry/referenceelements.hh>


using namespace Dune;
#include "overlappingOperations/parallelInfo.hpp"
#include "overlappingOperations/parallelSolver.hpp"
#include "assemble/buildPoisson.hpp"
#include "assemble/initSparseMatrix.hpp"
#include "io/displayPartition.hpp"
#include "assemble/solve.hpp"
int main(int argc, char ** argv)
{

    // Set MPI commuicator types
    Dune::MPIHelper::instance(argc, argv);
    typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
    typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;

    // Dune Block Vector and Vector types
    typedef Dune::FieldVector<double,1> BlockVec;
    typedef Dune::BlockVector<BlockVec> Vec;

    // Dune Block Matrix and CSR sparse Matrix format types
    typedef Dune::FieldMatrix<double,1,1> BlockMat;
    typedef Dune::BCRSMatrix<BlockMat> Mat;
    
    //====== How to create a trivial Corner-point grid ===========
    const std::array<int,3> dims = {10,10,3};
    const std::array<double,3> cellsize = {1.0,1.0,1.0};
    Dune::CpGrid grid;
    grid.createCartesian(dims, cellsize);
    typedef Dune::CpGrid::LeafGridView GridView;
    GridView gridView = grid.leafGridView();
    //=============================================================

    // Define MPI comunicator
    CollectiveCommunication cc(MPI_COMM_WORLD);
    
    Mat A;
    Vec rhs,x;
    grid.loadBalance();
    solve(grid,cc,A,rhs,x);
    /*
    auto info = getParallelInfo(grid);

    
    initSparseMatrix(grid,A);
    buildPoisson(grid,A,rhs);
    parallelSolve(A, rhs, x, cc, info);
    //std::cout<< "\n norm of x: "<< x.two_norm2()/x.size()<<std::endl;
    */
    return 0;
}
