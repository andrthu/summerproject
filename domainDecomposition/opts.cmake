#set(CMAKE_C_COMPILER "/cm/local/apps/gcc/8.2.0/bin/gcc" CACHE STRING "compiler")
#set(CMAKE_CXX_COMPILER "/cm/local/apps/gcc/8.2.0/bin/g++" CACHE STRING "compiler")
#set(CMAKE_CXX_FLAGS  "-O2 -mavx -funroll-loops -fomit-frame-pointer" CACHE STRING "SET CXX flags manually")
set(CMAKE_CXX_FLAGS  "-O2 -mavx -funroll-loops -pg" CACHE STRING "SET CXX flags manually")
