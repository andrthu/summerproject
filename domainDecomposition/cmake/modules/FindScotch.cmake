# -*-cmake-*-
#
# Try to find the libSCOTCH graph partioning library
#
# Once done, this will define:
#
#  SCOTCH_FOUND         - system has the libSCOTCH graph partioning library
#  HAVE_SCOTCH          - like SCOTCH_FOUND, but for the inclusion in config.h
#  SCOTCH_INCLUDE_DIRS  - incude paths to use libSCOTCH
#  SCOTCH_LIBRARIES     - Link these to use libSCOTCH

set(SCOTCH_SEARCH_PATH "/usr" "/usr/local" "/opt" "/opt/local" "$HOME/usr/local" "${PROJECT_SOURCE_DIR}/../../usr/local" "/home/andreast/usr/local")
set(SCOTCH_NO_DEFAULT_PATH "")
if(SCOTCH_ROOT)
  set(SCOTCH_SEARCH_PATH "${SCOTCH_ROOT}")
  set(SCOTCH_NO_DEFAULT_PATH "NO_DEFAULT_PATH")
endif()

# search for files which implements this module
find_path (SCOTCH_INCLUDE_DIRS
  NAMES "scotch.h"
  PATHS ${SCOTCH_SEARCH_PATH}
  PATH_SUFFIXES "include" "SCOTCHLib" "include/scotch"
  ${SCOTCH_NO_DEFAULT_PATH})

# only search in architecture-relevant directory
if (CMAKE_SIZEOF_VOID_P)
  math (EXPR _BITS "8 * ${CMAKE_SIZEOF_VOID_P}")
endif (CMAKE_SIZEOF_VOID_P)


set(SCOTCH_libs_to_find "scotch;scotcherrexit;scotcherr;scotchmetis")
set(SCOTCH_LIBRARIES "")

foreach(scotch_lib ${SCOTCH_libs_to_find})
  set(SCOTCH_${scotch_lib}_LIBRARY "SCOTCH_${scotch_lib}_LIBRARY-NOTFOUND")
  
  find_library(SCOTCH_${scotch_lib}_LIBRARY
    NAMES ${scotch_lib}
    PATHS ${SCOTCH_SEARCH_PATH}
    PATH_SUFFIXES "lib/.libs" "lib" "lib${_BITS}" "lib/${CMAKE_LIBRARY_ARCHITECTURE}"
    ${SCOTCH_NO_DEFAULT_PATH})
  
  list(APPEND SCOTCH_LIBRARIES "${SCOTCH_${scotch_lib}_LIBRARY}")
  
endforeach()

if (FALSE)
  find_library (SCOTCH_LIBRARIES
    NAMES "scotch"
    PATHS ${SCOTCH_SEARCH_PATH}
    PATH_SUFFIXES "lib/.libs" "lib" "lib${_BITS}" "lib/${CMAKE_LIBRARY_ARCHITECTURE}"
    ${SCOTCH_NO_DEFAULT_PATH})

  find_library (SCOTCH_LIBRARIES
    NAMES "scotcherrexit"
    PATHS ${SCOTCH_SEARCH_PATH}
    PATH_SUFFIXES "lib/.libs" "lib" "lib${_BITS}" "lib/${CMAKE_LIBRARY_ARCHITECTURE}"
    ${SCOTCH_NO_DEFAULT_PATH})
endif()


set (SCOTCH_FOUND FALSE)
if (SCOTCH_INCLUDE_DIRS OR SCOTCH_LIBRARIES)
  set(SCOTCH_FOUND TRUE)
  set(HAVE_SCOTCH TRUE)
endif()

# print a message to indicate status of this package
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args(SCOTCH
  DEFAULT_MSG
  SCOTCH_LIBRARIES
  SCOTCH_INCLUDE_DIRS
  )
