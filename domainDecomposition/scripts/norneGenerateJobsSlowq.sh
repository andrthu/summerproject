#!/bin/bash
tol=0.01
partitioners=("Metis" "Metis-CN" "Metis-NC" "Metis-WT" "Metis-WT-CN" "Metis-WT-NC" "Metis-W" "Metis-W-CN" "Metis-W-NC" "CpGrid" "CpGrid-CN" )
parts="0 1 2 3 4 5 6 7 8 9 10"
q="slowq"
namefolder=`date +"%d-%b_%H:%M"`
path=../../results/norne/$q/$namefolder
mkdir $path
sizes=("small" "big")
norne_paths=("../../norne_reservoir_grids/norne/NORNE_ATW2013.DATA" "../../norne_reservoir_grids/NORNE_ATW2013_222.DATA")
for part in $parts
do
    mkdir $path/${partitioners[part]} 
    for s in {0,1}
    do
	for node in {2..8}
	do
	    for i in {1,2,3,4}
	    do
		ranks=$i
		total_ranks=$(($ranks*$node))
		job_name=${partitioners[part]}_$ranks-ranks_$node-nodes_${sizes[i]}-size
		ini_file=../iniFiles/$job_name.ini
		host_file=../hostFiles/$job_name
		result_file=$path/${partitioners[part]}/$job_name.txt
		
		bash writeToIni.sh $part $tol $node $ini_file
		bash writeHOSTFILEslowq.sh $host_file $node $ranks 
		bash slowqNorneWriteSBATCH.sh $job_name.sh $node $total_ranks $q $ini_file $result_file $host_file ${norne_paths[i]}
		sbatch $job_name.sh 
		rm $job_name.sh 
	    done
	done
    done
done
