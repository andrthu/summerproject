#!/bin/bash


#SBATCH -p defq
#SBATCH -N 1 # number of nodes
#SBATCH -n 64
#SBATCH --mem 100 # hvor mye minne
#SBATCH -t 00-00:50 # time
#SBATCH -o slurm-%N-%j.out
#SBATCH --exclusive


#char processor_name[MPI_MAX_PROCESSOR_NAME];
# int name len;
# MPI_Get_processor_name(processor_name, &name_len)


#S1 numbeof cores
#S2 path to .ini file
#S3 path to .txt resutl file

mpirun  -np $1 ../build/src/runIni $2 >> $3
