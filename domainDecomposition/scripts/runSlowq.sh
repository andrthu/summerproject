#!/bin/bash

#SBATCH -p defq
#SBATCH -N 1 # number of nodes
#SBATCH -n 8
#SBATCH --mem 100 # hvor mye minne
#SBATCH -t 00-00:50 # time
#SBATCH -o ../../junk/slurm-%N-%j.out
#SBATCH --exclusive

#S1 number of cores
#S2 path to .ini file
#S3 path to .txt resutl file

mpirun -np $1 ../build/src/runIni $2 >> $3
