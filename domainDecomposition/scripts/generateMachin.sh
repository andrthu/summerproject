#!/bin/bash
tol=0.01
it=100
y=100
z=100
partitioners=("Metis" "Scotch" "Zoltan")  #"MetisCN" "ScotchCN" "ZoltanCN" "MetisNC" "ScotchNC
parts="0 1 2" # 3 4 5 6 7 8
for part in $parts
do
    for x in {10,100}
    do
	ranks=$((2**6))
	job_name=${partitioners[part]}_$ranks-ranks_$x-x_$y-y_$z-z
	ini_file=../iniFiles/$job_name.ini
	result_file=../../results/2node/${partitioners[part]}/$job_name.txt		
	cp machintest.sh $job_name.scp;
	bash writeToIni.sh $tol $it $part $x $y $z 2  $ini_file
	sbatch $job_name.scp $ranks $ini_file $result_file
	rm $job_name.scp    
    done
done

