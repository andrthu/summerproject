#!/bin/bash
tol=0.01
it=100
y=100
z=100
partitioners=("Metis" "Scotch" "Zoltan" "MetisCN" "ScotchCN" "ZoltanCN" "MetisNC" "ScotchNC")
parts="0 1 2"
node=1
q="slowq"
namefolder=`date +"%d-%b_%H:%M"`
path=../../results/1node/$q/$namefolder
mkdir $path
for part in $parts
do
    mkdir $path/${partitioners[part]}
    for ranks in {1,2,3,4}
    do
	for x in {10,100,1000}
	do
	    #ranks=$((2**2))
	    job_name=${partitioners[part]}_$ranks-ranks_$node-nodes_$x-x_$y-y_$z-z
	    ini_file=../iniFiles/$job_name.ini
	    host_file=../hostFiles/$job_name
	    result_file=$path/${partitioners[part]}/$job_name.txt
	    
	    bash writeToIni.sh $tol $it $part $x $y $z $node $ini_file
	    bash writeHOSTFILEslowq.sh $host_file $node $ranks 
	    bash writeSBATCHslowq.sh $job_name.sh $node $ranks $q $ini_file $result_file $host_file
	    sbatch $job_name.sh $ranks $ini_file $result_file
	    rm $job_name.sh
	done
    done
done

