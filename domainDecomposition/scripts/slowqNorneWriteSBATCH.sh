#!/bin/bash
path=$1
> $path
q=$4 # slowq or defq
numranks=$3
numnodes=$2
pathini=$5
pathresult=$6
pathhost=$7
pathgrid=$8
pref="n041"
for ((i=2; i<numnodes+1; i++))
do
    pref="$pref,n04$i"
done
echo "#!/bin/bash" >> $path 
echo "#SBATCH -p $q" >> $path
echo "#SBATCH -N $numnodes " >> $path
echo "#SBATCH -n $numranks " >> $path
echo "#SBATCH -w $pref" >> $path
echo "#SBATCH --mem 100" >> $path
echo "#SBATCH -t 00-00:60" >> $path
echo "#SBATCH -o ../../junk/slurm-%N-%j.out" >> $path
echo "#SBATCH --exclusive" >> $path
echo "mpirun -np $numranks --hostfile $pathhost ../build/src/norneRunIni $pathini $pathgrid  >> $pathresult" >>$path 
