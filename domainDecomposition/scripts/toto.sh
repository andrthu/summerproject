#!/bin/bash
#SBATCH -p defq
#SBATCH -N 4 
#SBATCH -n 16 
#SBATCH -w n001,n002,n003,n004
#SBATCH --mem 100
#SBATCH -t 00-00:60
#SBATCH -o ../../junk/slurm-%N-%j.out
#SBATCH --exclusive
mpirun -np 16 --hostfile host ../build/src/norneRunIni ../iniFiles/norne.ini ../../norne_reservoir_grids/NORNE_ATW2013_222.DATA  >> eurasia.txt
