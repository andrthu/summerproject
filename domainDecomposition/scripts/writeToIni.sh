#!/bin/bash

if test $# -eq 8
then
    > $8 # clearing .ini file
    echo "1 $2" >> $8 #max it
    echo "2 $3" >> $8 # 0 METIS, 1 SCOTCH, 2 ZOLTAN
    echo "3 $4" >> $8 # x dimension
    echo "4 $5" >> $8 # y dimension
    echo "5 $6" >> $8 # z dimension
    echo "6 $1" >> $8 # imbalance tolerance
    echo "7 $7" >> $8 # num nodes
fi


