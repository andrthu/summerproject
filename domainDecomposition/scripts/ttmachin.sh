#!/bin/bash
#SBATCH -p slowq
#SBATCH -N 8 
#SBATCH -n 32 
#SBATCH -w n041,n042,n043,n044,n045,n046,n047,n048
#SBATCH --mem 100
#SBATCH -t 00-00:60
#SBATCH -o ../../junk/slurm-%N-%j.out
#SBATCH --exclusive
mpirun -np 32 --hostfile host991 ../build/src/runIni europe.ini >> eurasia.txt
