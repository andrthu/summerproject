#!/bin/bash
tol=0.01
it=100
y=100
z=100
partitioners=("Metis" "Scotch" "Zoltan")
parts="0 1 2"


namefolder=`date +"%d-%b_%H:%M"`
path=../../results/1node/$namefolder
mkdir $path

for part in $parts
do
    mkdir $path/${partitioners[part]}
    for i in {0..6}
    do
	for x in {10,100,100}
	do
	    ranks=$((2**$i))
	    job_name=${partitioners[part]}_$ranks-ranks_$x-x_$y-y_$z-z
	    ini_file=../iniFiles/$job_name.ini
	    
	    result_file=../../results/1node/${partitioners[part]}/$job_name.txt
	    bash writeToIni.sh $tol $it $part $x $y $z 1  $ini_file
	    bash writeSBATCH_1NODE.sh $job_name.sh 1 $ranks defq $ini_file $result_file 
	    sbatch $job_name.sh $ranks $ini_file $result_file
	    rm $job_name.sh 
	done
    done
done

