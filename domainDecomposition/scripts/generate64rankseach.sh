#!/bin/bash
tol=0.01
it=100
y=100
z=100
partitioners=("Metis" "Scotch" "Zoltan" "MetisCN" "ScotchCN" "ZoltanCN" "MetisNC" "ScotchNC" "Bad")
parts="5 4 3 7 6 2 1 0 8"
q="defq"
namefolder=`date +"%d-%b_%H:%M"`
path=../../results/defq/$namefolder
mkdir $path

for part in $parts
do
    mkdir $path/${partitioners[part]}
    for x in {10,100,1000}
    do
	for node in {1,2,3,4}
	do
      	    ranks=$(($node*2**6))
	    job_name=${partitioners[part]}_$ranks-ranks_$node-nodes_$x-x_$y-y_$z-z
	    ini_file=../iniFiles/$job_name.ini
	    host_file=../hostFiles/$job_name
	    result_file=$path/${partitioners[part]}/$job_name.txt
	    
	    bash writeToIni.sh $tol $it $part $x $y $z $node $ini_file
	    bash writeHOSTFILE.sh $host_file $node $ranks 
	    bash writeSBATCH.sh $job_name.sh $node $ranks $q $ini_file $result_file $host_file
	    sbatch $job_name.sh $ranks $ini_file $result_file
	    rm $job_name.sh 
	done
    done
    
done
