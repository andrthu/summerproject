#!/bin/bash
>$1 # clearing hostfile
numnodes=$2
numranks=$3
ranksoneachnode=$(($numranks / $numnodes))
for ((i=1; i<numnodes+1; i++))
do
    echo "n00$i:$ranksoneachnode" >>$1
done
